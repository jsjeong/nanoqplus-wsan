/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#ifdef __MSPGCC__
#include <legacymsp430.h>
#else
#include <signal.h>
#endif

#include "platform.h"
#include "critical_section.h"

#ifdef UART_M
#include "uart.h"
#endif

#ifdef LED_M
#include "led.h"
#endif

#ifdef CC2420_M
#include "cc2420-interface.h"
#include "cc2420.h"
#endif

#ifdef SENSOR_LIGHT_M
#include "sensor_light.h"
#endif

#ifdef UART_M
int putchar(int c)
{
    if (c == '\n')
        nos_uart_putc(STDIO, '\r');
    nos_uart_putc(STDIO, c);
    return 0;
}
#endif //UART_M

void nos_platform_init()
{
#ifdef UART_M
    nos_uart_init(STDIO);
#endif

#ifdef LED_M
    nos_led_init();
#endif

#ifdef CC2420_M
    cc2420_interface_init(ID_UBI_MSP430_CC2420);
    cc2420_init(ID_UBI_MSP430_CC2420);
#endif

#ifdef SENSOR_LIGHT_M
    nos_light_init();
#endif
}

#ifdef CC2420_M
wakeup interrupt (PORT1_VECTOR) port1_isr(void)
{
    NOS_ENTER_ISR();
    cc2420_intr_handler(ID_UBI_MSP430_CC2420);
    NOS_EXIT_ISR();
}
#endif
