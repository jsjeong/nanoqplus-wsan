// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2420 - Ubi-MSP430 interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 6. 16.
 */

#include "kconf.h"

#ifdef CC2420_M
#include "cc2420-interface.h"

#ifdef __MSPGCC__
#include <msp430.h>
#elif defined __ICC430__
#include "io430.h"
#endif

#include "arch.h"
#include "critical_section.h"
#include "wpan-dev.h"
#include "platform.h"

#define REG(port, type) P##port##type

#define POUT(port)      REG(port, OUT)
#define PDIR(port)      REG(port, DIR)
#define PIN(port)       REG(port, IN)
#define PSEL(port)      REG(port, SEL)
#define PIE(port)       REG(port, IE)
#define PIFG(port)      REG(port, IFG)
#define PIES(port)      REG(port, IES)

#define VREG_EN_PORT   4
#define VREG_EN_PIN    5 //P4.5 - VREG_EN
#define FIFOP_PORT     1
#define FIFOP_PIN      0 //P1.0 - FIFOP
#define FIFO_PORT      1
#define FIFO_PIN       3 //P1.3 - FIFO
#define CCA_PORT       1
#define CCA_PIN        4 //P1.4 - CCA
#define SFD_PORT       4
#define SFD_PIN        1 //P4.1 - SFD
#define RESETN_PORT    4
#define RESETN_PIN     6 //P4.6 - RESET_N

#define MOSI_PORT      3
#define MOSI_PIN       1 //P3.1 - MOSI
#define MISO_PORT      3
#define MISO_PIN       2 //P3.2 - MISO
#define CLK_PORT       3
#define CLK_PIN        3 //P3.3 - CLK
#define CSN_PORT       4
#define CSN_PIN        2 //P4.2 - CS_N

#define UART_TXD_PORT  3
#define UART_TXD_PIN   4 //P3.4 - UART0 Tx
#define UART_RXD_PORT  3
#define UART_RXD_PIN   5 //P3.5 - UART0 Rx

extern WPAN_DEV wpan_dev[WPAN_DEV_CNT];

void cc2420_interface_init(UINT8 id)
{
    if (id == ID_UBI_MSP430_CC2420)
    {
        // Initialize GPIO outputs.
        _BIT_CLR(PSEL(VREG_EN_PORT), VREG_EN_PIN);
        _BIT_SET(PDIR(VREG_EN_PORT), VREG_EN_PIN);
        _BIT_CLR(PSEL(RESETN_PORT), RESETN_PIN);
        _BIT_SET(PDIR(RESETN_PORT), RESETN_PIN);

        // Initialize GPIO inputs.
        _BIT_CLR(PSEL(FIFOP_PORT), FIFOP_PIN);
        _BIT_CLR(PDIR(FIFOP_PORT), FIFOP_PIN);
        _BIT_CLR(PSEL(FIFO_PORT), FIFO_PIN);
        _BIT_CLR(PDIR(FIFO_PORT), FIFO_PIN);
        _BIT_CLR(PSEL(CCA_PORT), CCA_PIN);
        _BIT_CLR(PDIR(CCA_PORT), CCA_PIN);
        _BIT_CLR(PSEL(SFD_PORT), SFD_PIN);
        _BIT_CLR(PDIR(SFD_PORT), SFD_PIN);

        // Initialize SPI pins.
        _BIT_CLR(PSEL(UART_TXD_PORT), UART_TXD_PIN);
        _BIT_CLR(PSEL(UART_RXD_PORT), UART_RXD_PIN);
        _BIT_SET(PSEL(MOSI_PORT), MOSI_PIN);
        _BIT_SET(PDIR(MOSI_PORT), MOSI_PIN);
        _BIT_SET(PSEL(MISO_PORT), MISO_PIN);
        _BIT_CLR(PDIR(MISO_PORT), MISO_PIN);
        _BIT_SET(PSEL(CLK_PORT), CLK_PIN);
        _BIT_SET(PDIR(CLK_PORT), CLK_PIN);
        _BIT_CLR(PSEL(CSN_PORT), CSN_PIN);
        _BIT_SET(PDIR(CSN_PORT), CSN_PIN);

        // Initialize SPI.
        _BIT_SET(POUT(CSN_PORT), CSN_PIN);

        U0CTL = CHAR + SYNC + MM + SWRST;
        U0TCTL = CKPH + SSEL1 + STC;
        U0CTL &= ~0x20;
        U0BR0 = 0x02;
        U0BR1 = 0;
        U0MCTL = 0;
        ME1 &= ~(UTXE0 | URXE0);
        ME1 |= USPIE0;
        U0CTL &= ~SWRST;
        IFG1 &= ~(UTXIFG0 | URXIFG0);
        IE1 &= ~(UTXIE0 | URXIE0);

        // Initialize FIFOP interrupt.
        _BIT_CLR(PIES(FIFOP_PORT), FIFOP_PIN);
    }
}

ERROR_T cc2420_request_cs(UINT8 id)
{
    if (id == ID_UBI_MSP430_CC2420)
    {
        _BIT_CLR(POUT(CSN_PORT), CSN_PIN);
        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_release_cs(UINT8 id)
{
    if (id == ID_UBI_MSP430_CC2420)
    {
        _BIT_SET(POUT(CSN_PORT), CSN_PIN);
        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_spi(UINT8 id, UINT8 txdata, UINT8 *rxdata)
{
    UINT8 rxd;

    if (id == ID_UBI_MSP430_CC2420)
    {
        U0TXBUF = txdata;
        while ((IFG1 & URXIFG0) == 0);
        rxd = U0RXBUF;

        if (rxdata != NULL)
            *rxdata = rxd;

        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_on(UINT8 id)
{
    if (id == ID_UBI_MSP430_CC2420)
    {
        _BIT_SET(POUT(VREG_EN_PORT), VREG_EN_PIN);
        nos_delay_ms(2);

        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_off(UINT8 id)
{
    _BIT_CLR(POUT(VREG_EN_PORT), VREG_EN_PIN);
    return ERROR_SUCCESS;
}

ERROR_T cc2420_reset(UINT8 id)
{
    if (id == ID_UBI_MSP430_CC2420)
    {
        _BIT_CLR(POUT(RESETN_PORT), RESETN_PIN);
        nos_delay_us(1);
        _BIT_SET(POUT(RESETN_PORT), RESETN_PIN);
        nos_delay_us(5);

        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_enable_fifop_intr(UINT8 id, BOOL enable)
{
    if (id == ID_UBI_MSP430_CC2420)
    {
        if (enable)
            _BIT_SET(PIE(FIFOP_PORT), FIFOP_PIN);
        else
            _BIT_CLR(PIE(FIFOP_PORT), FIFOP_PIN);

        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_clear_fifop_intr(UINT8 id)
{
    if (id == ID_UBI_MSP430_CC2420)
    {
        _BIT_CLR(PIFG(FIFOP_PORT), FIFOP_PIN);
        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_check_fifop_intr(UINT8 id, BOOL *flag_is_set)
{
    if (id == ID_UBI_MSP430_CC2420)
    {
        if (flag_is_set == NULL)
            return ERROR_INVALID_ARGS;

        *flag_is_set = (_IS_SET(PIFG(FIFOP_PORT), FIFOP_PIN)) ? TRUE : FALSE;
        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_get_pin_state(UINT8 id, UINT8 pin, BOOL *pin_is_set)
{
    if (id == ID_UBI_MSP430_CC2420)
    {
        if (pin_is_set == NULL)
            return ERROR_INVALID_ARGS;

        if (pin == CC2420_FIFOP)
        {
            *pin_is_set = (_IS_SET(PIN(FIFOP_PORT), FIFOP_PIN)) ? TRUE : FALSE;
        }
        else if (pin == CC2420_FIFO)
        {
            *pin_is_set = (_IS_SET(PIN(FIFO_PORT), FIFO_PIN)) ? TRUE : FALSE;
        }
        else if (pin == CC2420_SFD)
        {
            *pin_is_set = (_IS_SET(PIN(SFD_PORT), SFD_PIN)) ? TRUE : FALSE;
        }
        else if (pin == CC2420_CCA)
        {
            *pin_is_set = (_IS_SET(PIN(CCA_PORT), CCA_PIN)) ? TRUE : FALSE;
        }
        else
        {
            return ERROR_INVALID_ARGS;
        }

        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

#endif //CC2420_M
