// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Battery Monitor for Tmote-Sky
 * 
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 19.
 */

/*
 * $Id$
 * $LastChangedDate$
 */

#include "bat-monitor.h"

#ifdef BATTERY_MONITOR_M
#include "pwr_mgr.h"
#include "critical_section.h"
#include <stdio.h>
#include "user_timer.h"

#ifdef COOJA_SIM_M
#ifdef POWER_SOURCE_ENERGY_SCAVENGER
#define CHARGE_RATE ((UINT16)((UINT32)CONFIG_ENERGY_SCAVENGER_RECHARGE_RATE * 0xffffL / 100 / 60))
#endif
UINT16 bat_residual = 0xffff;
#endif

#ifdef COOJA_SIM_M
#ifdef POWER_SOURCE_ENERGY_SCAVENGER
void nos_battery_charge(void *args)
{
    if (0xffff - bat_residual > CHARGE_RATE)
    {
        //printf("bat_residual: %u -> %u\n", bat_residual, bat_residual + CHARGE_RATE);
        bat_residual += CHARGE_RATE;
    }
    else
    {
        bat_residual = 0xffff;
    }
}
#endif

void nos_battery_discharge(UINT16 x)
{
#if defined(POWER_SOURCE_BATTERY) || defined(POWER_SOURCE_ENERGY_SCAVENGER)
    if (bat_residual > x)
    {
        bat_residual -= x;
    }
    else
    {
        bat_residual = 0;
        printf("Out of power\n");
        while(1)
        {
            // Stop the operation.
            NOS_ENTER_CRITICAL_SECTION();
            nos_mcu_sleep();
            NOS_EXIT_CRITICAL_SECTION();
        }
    }
#endif //POWER_SOURCE_BATTERY || POWER_SOURCE_ENERGY_SCAVENGER
}
#endif //COOJA_SIM_M

UINT8 nos_battery_read_level(void)
{
#ifdef COOJA_SIM_M
    return (UINT32) bat_residual * 100 / 65535; //Percentage
#else
    return 0;
#endif
}

void nos_battery_mon_init(void)
{
#if defined POWER_SOURCE_ENERGY_SCAVENGER && defined COOJA_SIM_M
    printf("Charge rate: %u/sec\n", CHARGE_RATE);
    nos_user_timer_create_sec(nos_battery_charge, NULL, 1, TRUE);
#endif
}

#endif //BATTERY_MONITOR_M
