// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#ifndef PLATFORM_H
#define PLATFORM_H

#include "kconf.h"
#include <avr/io.h>
#include "uart.h"

#define _SYSTEM_CLOCK 8000000ul

enum
{
    STDIO = ATMEGA128_UART1,
};

/* ############################# RF Modem Devices ########################### */
#ifdef IEEE_802_15_4_DEV_M
enum
{
    ID_NANO24_CC2420 = 0,
    WPAN_DEV_CNT = 1,
};
#endif //IEEE_802_15_4_DEV_M
/* ########################################################################## */

/* LEDs */
#define LED1_PORT                   D
#define LED1_PIN                    7 // PD.7 - Output : Red LED
#define LED2_PORT                   E
#define LED2_PIN                    0 // PE.0 - Output : Green LED
#define LED3_PORT                   E
#define LED3_PIN                    1 // PE.1 - Output : Yellow LED

/* UART pins */
#define UART1_RXD_PORT              D
#define UART1_RXD_PIN               2 // RXD1 - Input:  UART1 RXD
#define UART1_TXD_PORT              D
#define UART1_TXD_PIN               3 // TXD1 - Output: UART1 TXD

/* Battery voltage */
#define BATTERY_VOLTAGE_ADC_CHANNEL_PORT    F
#define BATTERY_VOLTAGE_ADC_CHANNEL_PIN     6 //ADC6 - adc channel 6

/* OCTACOMM Ultrasonic sensor board */
#define OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PORT    A
#define OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PIN     0 // PA.0 - US Sensor Power Switch
#define OCTACOMM_ULTRASONIC_SENSOR_INTR_PORT        E
#define OCTACOMM_ULTRASONIC_SENSOR_INTR_PIN         7 // INT7 - Falling Edge Interrupt
#define OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PORT       F
#define OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PIN        5 // PF.5 - Output

/* OCTACOMM Environment sensor board */
#define OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PORT               C
#define OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PIN                0 // PC.0 - Gas Sensor Power Switch
#define OCTACOMM_ENV_SENSOR_GAS_ADC_CHANNEL_PORT            F
#define OCTACOMM_ENV_SENSOR_GAS_ADC_CHANNEL_PIN             2 //ADC2 - adc channel 2
#define OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PORT       A
#define OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PIN        0 // PA.0 - Temp Sensor Power Switch
#define OCTACOMM_ENV_SENSOR_TEMPERATURE_ADC_CHANNEL_PORT  	F
#define OCTACOMM_ENV_SENSOR_TEMPERATURE_ADC_CHANNEL_PIN     0 //ADC0 - adc channel 0
#define OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PORT             A
#define OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PIN              1 // PA.1 - Light Sensor Power Switch
#define OCTACOMM_ENV_SENSOR_LIGHT_ADC_CHANNEL_PORT          F
#define OCTACOMM_ENV_SENSOR_LIGHT_ADC_CHANNEL_PIN           1 //ADC1 - adc channel 1
#define OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PORT          A
#define OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PIN           2 // PA.2 - Humidity Sensor Power Switch
#define OCTACOMM_ENV_SENSOR_HUMIDITY_CLK_PORT   		    E
#define OCTACOMM_ENV_SENSOR_HUMIDITY_CLK_PIN    		    6 // T3 - Input : humidiy Clock Source

/* OCTACOMM AC DC Actuators (relays) board */
#define OCTACOMM_RELAY1_PORT    F
#define OCTACOMM_RELAY1_PIN     7 // PF.7 - Actuator1 (relay)
#define OCTACOMM_RELAY2_PORT    A
#define OCTACOMM_RELAY2_PIN     3 // PA.3 - Actuator0 (relay)
#define OCTACOMM_RELAY3_PORT    C
#define OCTACOMM_RELAY3_PIN     3 // PC.2 - Actuator3 (relay)
#define OCTACOMM_RELAY4_PORT    C
#define OCTACOMM_RELAY4_PIN     4 // PC.3 - Actuator4 (relay)

/* External flash memory */
#define FLASH_SO_PORT           B
#define FLASH_SO_PIN            6 // PB.6 - External flash memory SO
#define FLASH_SI_PORT           B
#define FLASH_SI_PIN            7 // PB.7 - External flash memory SI
#define FLASH_CLK_PORT          E
#define FLASH_CLK_PIN           4 // PE.4 - External flash memory CLK
#define FLASH_CSN_PORT          E
#define FLASH_CSN_PIN           5 // PE.5 - External flash memory CSN

/* Octacomm PIR sensor board */
#define OCTACOMM_PIR_SENSOR_POWER_SW_PORT       C
#define OCTACOMM_PIR_SENSOR_POWER_SW_PIN        0 // PC.0 - Same for GAS, PIR Sensor Power Switch
#define OCTACOMM_PIR_SENSOR_INTR_PORT           E
#define OCTACOMM_PIR_SENSOR_INTR_PIN            7 // INT7 - Falling Edge Interrupt


/* Octacomm interface board */
#define INTERFACE_SW3_PORT          E
#define INTERFACE_SW3_PIN           6 // INT6 - Interface board sw3
#define INTERFACE_SW2_PORT          E
#define INTERFACE_SW2_PIN           7 // INT7 - Interface board sw2

void nos_platform_init(void);

#endif // ~PLATFORM_H
