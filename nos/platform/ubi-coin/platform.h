// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#ifndef PLATFORM_H
#define PLATFORM_H

#include "kconf.h"
#include "uart.h"

enum
{
    STDIO = MSP430F1611_USART1,
};

enum wpan_dev_id
{
    ID_UBI_COIN_CC2420 = 0,
    WPAN_DEV_CNT = 1,
};

/* LED */
#define LED1_PORT                   4
#define LED1_PIN                    2 // P4.2

/*********************** PORT Definition ******************************/
// Port 2
#define PORT_USER_BUTTON            2
#define PIN_USER_BUTTON             1 // P2.1 - Left side push-button is connected to interrupt pin. 
#define IE_USER_BUTTON              P2IE
#define IES_USER_BUTTON             P2IES
#define IFG_USER_BUTTON             P2IFG
//#define BSL_RX     2
//#define ONEWIRE_SERIAL_ID  3 //P2.3 - 1-Wire : serial id check

// Port 3

// Port 4
#define LIGHT_SENSOR_POWER_SW_PORT  4
#define LIGHT_SENSOR_POWER_SW_PIN   3 // P4.3 - LIGHT Sensor power
#define PORT_PIEZO_PWM              4
#define PIN_PIEZO_PWM               4 // P4.4 - Sound

// Port 6
#define LIGHT_SENSOR_ADC_CHANNEL_PORT   6
#define LIGHT_SENSOR_ADC_CHANNEL_PIN    0 // P6.0 - ADC 0 IN

void nos_platform_init(void);

#endif // ~PLATFORM_H
