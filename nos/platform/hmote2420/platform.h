// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Sangcheol Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#ifndef PLATFORM_H
#define PLATFORM_H

#include "kconf.h"
#include "uart.h"

enum
{
    STDIO = MSP430F1611_USART1,
};

enum wpan_dev_id
{
    ID_HMOTE2420_CC2420 = 0,
    WPAN_DEV_CNT = 1,
};

/*********************** PORT Definition ******************************/

/* LEDs */
#define LED1_PORT                   5
#define LED1_PIN                    4 // P5.4 - Output : Green LED
#define LED2_PORT                   5
#define LED2_PIN                    5 // P5.5 - Output : Red LED
#define LED3_PORT                   5
#define LED3_PIN                    6 // P5.6 - Output : Blue LED

/* Sensirion SHT1x pins */
#define SHTXX_SCL_PORT              1
#define SHTXX_SCL_PIN               7
#define SHTXX_SDA_PORT              2
#define SHTXX_SDA_PIN               0

/* Microphone pins */
#define MIC_SENSOR_POWER_SW_PORT    4
#define MIC_SENSOR_POWER_SW_PIN     3 // P4.3 - Input: MIC Sensor
#define MIC_SENSOR_ADC_CHANNEL_PORT 6
#define MIC_SENSOR_ADC_CHANNEL_PIN  2 // P6.2 - MIC channel

/* Light sensor pins */
#define LIGHT_SENSOR_POWER_SW_PORT      2
#define LIGHT_SENSOR_POWER_SW_PIN       1 // P2.1 - Illuminance intensity Sensor
#define LIGHT_SENSOR_ADC_CHANNEL_PORT   6
#define LIGHT_SENSOR_ADC_CHANNEL_PIN    4 // P6.4 - light channel

void nos_platform_init(void);

#endif // PLATFORM_H
