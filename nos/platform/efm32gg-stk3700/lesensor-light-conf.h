/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief LESENSE based light sensor
 * @date 2012. 9. 8.
 * @author Jongsoo Jeong (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef LIGHT_SENSOR_CONF_H_
#define LIGHT_SENSOR_CONF_H_

#include "kconf.h"

#ifdef LESENSOR_LIGHT_M

#define LIGHT_EXCITE_PORT               D
#define LIGHT_EXCITE_PIN                6
#define LIGHT_EXCITE_LESENSE_ALTEX_ID   0
#define LIGHT_SENSE_PORT                C
#define LIGHT_SENSE_PIN                 6
#define LIGHT_SENSE_LESENSE_CHANNEL     6

#include "common.h"
#include "em_lesense.h"

void nos_lesensor_light_config(LESENSE_Init_TypeDef *lesense,
        LESENSE_ChDesc_TypeDef *ch,
        LESENSE_AltExDesc_TypeDef *altex);

#endif /* LESENSOR_LIGHT_M */
#endif /* LIGHT_SENSOR_CONF_H_ */
