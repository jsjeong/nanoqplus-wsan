/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief EFM32GG-STK3700 (EFM32-GiantGecko Starter Kit)
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 7. 18.
 */

#include "platform.h"
#include "critical_section.h"

#ifdef LED_M
#include "led.h"
#endif /* LED_M */

#ifdef UART_M
#include "uart.h"
#endif /* UART_M */

#ifdef EMLCD_M
#include "emlcd.h"
#endif /* EMLCD_M */

#ifdef BUTTON_M
#include "button.h"
#endif /* USER_BUTTON_M */

#ifdef EM_LESENSOR_M
#include "lesense.h"

#ifdef LESENSOR_LIGHT_M
#include "lesensor-light.h"
#endif /* LESENSOR_LIGHT_M */
#endif /* EM_LESENSOR_M */

void setupSWO(void)
{
    uint32_t *dwt_ctrl = (uint32_t *) 0xE0001000;
    uint32_t *tpiu_prescaler = (uint32_t *) 0xE0040010;
    uint32_t *tpiu_protocol = (uint32_t *) 0xE00400F0;

    CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;
    /* Enable Serial wire output pin */
    GPIO->ROUTE |= GPIO_ROUTE_SWOPEN;
#if defined(_EFM32_GIANT_FAMILY)
    /* Set location 0 */
    GPIO->ROUTE = (GPIO->ROUTE & ~(_GPIO_ROUTE_SWLOCATION_MASK)) | GPIO_ROUTE_SWLOCATION_LOC0;

    /* Enable output on pin - GPIO Port F, Pin 2 */
    GPIO->P[5].MODEL &= ~(_GPIO_P_MODEL_MODE2_MASK);
    GPIO->P[5].MODEL |= GPIO_P_MODEL_MODE2_PUSHPULL;
#else
    /* Set location 1 */
    GPIO->ROUTE = (GPIO->ROUTE & ~(_GPIO_ROUTE_SWLOCATION_MASK)) | GPIO_ROUTE_SWLOCATION_LOC1;
    /* Enable output on pin */
    GPIO->P[2].MODEH &= ~(_GPIO_P_MODEH_MODE15_MASK);
    GPIO->P[2].MODEH |= GPIO_P_MODEH_MODE15_PUSHPULL;
#endif
    /* Enable debug clock AUXHFRCO */
    CMU->OSCENCMD = CMU_OSCENCMD_AUXHFRCOEN;

    while(!(CMU->STATUS & CMU_STATUS_AUXHFRCORDY));

    /* Enable trace in core debug */
    CoreDebug->DHCSR |= 1;
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

    /* Enable PC and IRQ sampling output */
    *dwt_ctrl = 0x400113FF;
    /* Set TPIU prescaler to 16. */
    *tpiu_prescaler = 0xf;
    /* Set protocol to NRZ */
    *tpiu_protocol = 2;
    /* Unlock ITM and output data */
    ITM->LAR = 0xC5ACCE55;
    ITM->TCR = 0x10009;
}

void nos_platform_init()
{
#ifdef LED_M
    nos_led_init();
#endif /* LED_M */

#ifdef EFM32GG_USART1_FOR_UART_M
    nos_uart_init(EFM32GG_USART1);
#endif
#ifdef EFM32GG_LEUART0_FOR_UART_M
    nos_uart_init(EFM32GG_LEUART0);
#endif

#ifdef EMLCD_M
    nos_emlcd_init();
#endif /* EMLCD_M */

#ifdef BUTTON_M
    NVIC_SetPriority(GPIO_EVEN_IRQn, 6);
    NVIC_SetPriority(GPIO_ODD_IRQn, 6);
    NVIC_EnableIRQ(GPIO_EVEN_IRQn);
    NVIC_EnableIRQ(GPIO_ODD_IRQn);

    nos_button_init();
#endif /* BUTTON_M */

#ifdef EM_LESENSOR_M
    nos_lesense_init();
#endif /* EM_LESENSOR_M */
}

void GPIO_EVEN_IRQHandler(void)
{
    NOS_ENTER_ISR();
#ifdef BUTTON_M
    if (GPIO->IF & (1 << UIF_PB1_PIN))
    {
        NOS_GPIO_CLEAR_INTR(UIF_PB1_PORT, UIF_PB1_PIN);
        if (nos_button_callback != NULL)
            nos_button_callback(PB1);
    }
#endif /* USER_BUTTON_M */
    NOS_EXIT_ISR();
}

void GPIO_ODD_IRQHandler(void)
{
    NOS_ENTER_ISR();
#ifdef BUTTON_M
    if (GPIO->IF & (1 << UIF_PB0_PIN))
    {
        NOS_GPIO_CLEAR_INTR(UIF_PB0_PORT, UIF_PB0_PIN);
        if (nos_button_callback != NULL)
            nos_button_callback(PB0);
    }
#endif /* USER_BUTTON_M */
    NOS_EXIT_ISR();
}

void HardFault_Handler(void)
{
    while(1)
    {
        nos_led_on(0);
        nos_led_on(1);
        nos_delay_ms(500);
        nos_led_off(0);
        nos_led_off(1);
        nos_delay_ms(500);
    }
}

#ifdef EM_LESENSOR_M
void LESENSE_IRQHandler(void)
{
    NOS_ENTER_ISR();
#ifdef LESENSOR_LIGHT_M
    if (LESENSE_IntGet() & (1 << LIGHT_SENSE_LESENSE_CHANNEL))
    {
        LESENSE_IntClear(1 << LIGHT_SENSE_LESENSE_CHANNEL);
        nos_lesense_light_event_handler(LIGHT_SENSE_LESENSE_CHANNEL);
    }
#endif /* LESENSOR_LIGHT_M */
    NOS_EXIT_ISR();
}
#endif /* EM_LESENSOR_M */
