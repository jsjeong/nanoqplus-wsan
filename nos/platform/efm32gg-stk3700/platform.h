// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief EFM32GG-STK3700 (EFM32-GiantGecko Starter Kit)
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 7. 17.
 */

#ifndef PLATFORM_H_
#define PLATFORM_H_

#include "kconf.h"

#include "arch.h"
#include "gpio.h"

#define HFCORECLK 48000000

/* UART port numbers */

#if defined STDIO_EFM32GG_STK3700_USART1
#define STDIO   EFM32GG_USART1
#elif defined STDIO_EFM32GG_STK3700_LEUART0
#define STDIO   EFM32GG_LEUART0
#else
#undef STDIO
#endif

/* USART1 pins */
#define USART1_TXD_PORT     gpioPortD
#define USART1_TXD_PIN      0
#define USART1_RXD_PORT     gpioPortD
#define USART1_RXD_PIN      1
#define USART1_LOCATION     1

/* LEUART0 pins */
#define LEUART0_TXD_PORT    gpioPortD
#define LEUART0_TXD_PIN     4
#define LEUART0_RXD_PORT    gpioPortD
#define LEUART0_RXD_PIN     5
#define LEUART0_LOCATION    0

/* LEDs */
#define UIF_LED0_PORT       E
#define UIF_LED0_PIN        2
#define UIF_LED1_PORT       E
#define UIF_LED1_PIN        3

/* Push buttons */
#define UIF_PB0_PORT        B
#define UIF_PB0_PIN         9
#define UIF_PB1_PORT        B
#define UIF_PB1_PIN         10

/* 160 segment EnergyMicro LCD */
#define ENERGYMICRO_ENERGYMICRO_LCD_SEG_39_PORT     A
#define ENERGYMICRO_LCD_SEG_39_PIN      11
#define ENERGYMICRO_LCD_SEG_38_PORT     A
#define ENERGYMICRO_LCD_SEG_38_PIN      10
#define ENERGYMICRO_LCD_SEG_37_PORT     A
#define ENERGYMICRO_LCD_SEG_37_PIN      9
#define ENERGYMICRO_LCD_SEG_36_PORT     A
#define ENERGYMICRO_LCD_SEG_36_PIN      8
#define ENERGYMICRO_LCD_SEG_35_PORT     A
#define ENERGYMICRO_LCD_SEG_35_PIN      7
#define ENERGYMICRO_LCD_SEG_34_PORT     B
#define ENERGYMICRO_LCD_SEG_34_PIN      2
#define ENERGYMICRO_LCD_SEG_33_PORT     B
#define ENERGYMICRO_LCD_SEG_33_PIN      1
#define ENERGYMICRO_LCD_SEG_32_PORT     B
#define ENERGYMICRO_LCD_SEG_32_PIN      0
#define ENERGYMICRO_LCD_SEG_31_PORT     D
#define ENERGYMICRO_LCD_SEG_31_PIN      12
#define ENERGYMICRO_LCD_SEG_30_PORT     D
#define ENERGYMICRO_LCD_SEG_30_PIN      11
#define ENERGYMICRO_LCD_SEG_29_PORT     D
#define ENERGYMICRO_LCD_SEG_29_PIN      10
#define ENERGYMICRO_LCD_SEG_28_PORT     D
#define ENERGYMICRO_LCD_SEG_28_PIN      9

#define ENERGYMICRO_LCD_SEG_19_PORT     A
#define ENERGYMICRO_LCD_SEG_19_PIN      6
#define ENERGYMICRO_LCD_SEG_18_PORT     A
#define ENERGYMICRO_LCD_SEG_18_PIN      5
#define ENERGYMICRO_LCD_SEG_17_PORT     A
#define ENERGYMICRO_LCD_SEG_17_PIN      4
#define ENERGYMICRO_LCD_SEG_16_PORT     A
#define ENERGYMICRO_LCD_SEG_16_PIN      3
#define ENERGYMICRO_LCD_SEG_15_PORT     A
#define ENERGYMICRO_LCD_SEG_15_PIN      2
#define ENERGYMICRO_LCD_SEG_14_PORT     A
#define ENERGYMICRO_LCD_SEG_14_PIN      1
#define ENERGYMICRO_LCD_SEG_13_PORT     A
#define ENERGYMICRO_LCD_SEG_13_PIN      0
#define ENERGYMICRO_LCD_SEG_12_PORT     A
#define ENERGYMICRO_LCD_SEG_12_PIN      15

#define ENERGYMICRO_LCD_COM_7_PORT      B
#define ENERGYMICRO_LCD_COM_7_PIN       6
#define ENERGYMICRO_LCD_COM_6_PORT      B
#define ENERGYMICRO_LCD_COM_6_PIN       5
#define ENERGYMICRO_LCD_COM_5_PORT      B
#define ENERGYMICRO_LCD_COM_5_PIN       4
#define ENERGYMICRO_LCD_COM_4_PORT      B
#define ENERGYMICRO_LCD_COM_4_PIN       3
#define ENERGYMICRO_LCD_COM_3_PORT      E
#define ENERGYMICRO_LCD_COM_3_PIN       7
#define ENERGYMICRO_LCD_COM_2_PORT      E
#define ENERGYMICRO_LCD_COM_2_PIN       6
#define ENERGYMICRO_LCD_COM_1_PORT      E
#define ENERGYMICRO_LCD_COM_1_PIN       5
#define ENERGYMICRO_LCD_COM_0_PORT      E
#define ENERGYMICRO_LCD_COM_0_PIN       4

/* Capacitive touch slide */
#define UIF_TOUCH0_PORT     C
#define UIF_TOUCH0_PIN      8
#define UIF_TOUCH1_PORT     C
#define UIF_TOUCH1_PIN      9
#define UIF_TOUCH2_PORT     C
#define UIF_TOUCH2_PIN      10
#define UIF_TOUCH3_PORT     C
#define UIF_TOUCH3_PIN      11

/* Ambient light sensor */
#include "lesensor-light-conf.h"

/* LC sensor */
#define DAC_LC_EXCITE_PORT  B
#define DAC_LC_EXCITE_PIN   12
#define LES_LC_SENSE_PORT   C
#define LES_LC_SENSE_PIN    7

/* NAND256W3A flash connected to the external bus interface */
#define NAND256W3A_PWR_EN_PORT  B
#define NAND256W3A_PWR_EN_PIN   15
#define NAND256W3A_IO7_PORT     E
#define NAND256W3A_IO7_PIN      15
#define NAND256W3A_IO6_PORT     E
#define NAND256W3A_IO6_PIN      14
#define NAND256W3A_IO5_PORT     E
#define NAND256W3A_IO5_PIN      13
#define NAND256W3A_IO4_PORT     E
#define NAND256W3A_IO4_PIN      12
#define NAND256W3A_IO3_PORT     E
#define NAND256W3A_IO3_PIN      11
#define NAND256W3A_IO2_PORT     E
#define NAND256W3A_IO2_PIN      10
#define NAND256W3A_IO1_PORT     E
#define NAND256W3A_IO1_PIN      9
#define NAND256W3A_IO0_PORT     E
#define NAND256W3A_IO0_PIN      8
#define NAND256W3A_ALE_PORT     C
#define NAND256W3A_ALE_PIN      1
#define NAND256W3A_CLE_PORT     C
#define NAND256W3A_CLE_PIN      2
#define NAND256W3A_WEN_PORT     F
#define NAND256W3A_WEN_PIN      8
#define NAND256W3A_REN_PORT     F
#define NAND256W3A_REN_PIN      9
#define NAND256W3A_WPN_PORT     D
#define NAND256W3A_WPN_PIN      13
#define NAND256W3A_CEN_PORT     D
#define NAND256W3A_CEN_PIN      14
#define NAND256W3A_RBN_PORT     D
#define NAND256W3A_RBN_PIN      15

void nos_platform_init(void);

#endif /* PLATFORM_H_ */
