/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief LESENSE based light sensor
 * @date 2012. 9. 8.
 * @author Jongsoo Jeong (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "lesensor-light-conf.h"

#ifdef LESENSOR_LIGHT_M
#include "gpio.h"

void nos_lesensor_light_config(LESENSE_Init_TypeDef *lesense,
        LESENSE_ChDesc_TypeDef *ch,
        LESENSE_AltExDesc_TypeDef *altex)
{
    lesense->coreCtrl.scanStart = lesenseScanStartPeriodic;
    lesense->coreCtrl.prsSel = lesensePRSCh0;
    lesense->coreCtrl.scanConfSel = lesenseScanConfDirMap;
    lesense->coreCtrl.invACMP0 = false;
    lesense->coreCtrl.invACMP1 = false;
    lesense->coreCtrl.dualSample = false;
    lesense->coreCtrl.storeScanRes = false;
    lesense->coreCtrl.bufOverWr = true;
    lesense->coreCtrl.bufTrigLevel = lesenseBufTrigHalf;
    lesense->coreCtrl.wakeupOnDMA = lesenseDMAWakeUpDisable;
    lesense->coreCtrl.biasMode = lesenseBiasModeDutyCycle;
    lesense->coreCtrl.debugRun = false;

    lesense->timeCtrl.startDelay = 0U;

    lesense->perCtrl.dacCh0Data = lesenseDACIfData;
    lesense->perCtrl.dacCh0ConvMode = lesenseDACConvModeDisable;
    lesense->perCtrl.dacCh0OutMode = lesenseDACOutModeDisable;
    lesense->perCtrl.dacCh1Data = lesenseDACIfData;
    lesense->perCtrl.dacCh1ConvMode = lesenseDACConvModeDisable;
    lesense->perCtrl.dacCh1OutMode = lesenseDACOutModeDisable;
    lesense->perCtrl.dacPresc = 0U;
    lesense->perCtrl.dacRef = lesenseDACRefBandGap;
    lesense->perCtrl.acmp0Mode = lesenseACMPModeMuxThres;
    lesense->perCtrl.acmp1Mode = lesenseACMPModeDisable;
    lesense->perCtrl.warmupMode = lesenseWarmupModeNormal;

    lesense->decCtrl.decInput = lesenseDecInputSensorSt;
    lesense->decCtrl.initState = 0U;
    lesense->decCtrl.chkState = false;
    lesense->decCtrl.intMap = true;
    lesense->decCtrl.hystPRS0 = false;
    lesense->decCtrl.hystPRS1 = false;
    lesense->decCtrl.hystPRS2 = false;
    lesense->decCtrl.hystIRQ = false;
    lesense->decCtrl.prsCount = true;
    lesense->decCtrl.prsChSel0 = lesensePRSCh0;
    lesense->decCtrl.prsChSel1 = lesensePRSCh1;
    lesense->decCtrl.prsChSel2 = lesensePRSCh2;
    lesense->decCtrl.prsChSel3 = lesensePRSCh3;

    ch->enaScanCh = true;
    ch->enaPin = false;
    ch->enaInt = true;
    ch->chPinExMode = lesenseChPinExHigh;
    ch->chPinIdleMode = lesenseChPinIdleDis;
    ch->useAltEx = true;
    ch->shiftRes = false;
    ch->invRes = false;
    ch->storeCntRes = true;
    ch->exClk = lesenseClkLF;
    ch->sampleClk = lesenseClkLF;
    ch->exTime = 0x01U;
    ch->sampleDelay = 0x01U;
    ch->measDelay = 0x00U;
    ch->acmpThres = 0x38U;
    ch->sampleMode = lesenseSampleModeACMP;
    ch->intMode = lesenseSetIntNegEdge;
    ch->cntThres = 0x0000U;
    ch->compMode = lesenseCompModeLess;

    altex->enablePin = true;
    altex->idleConf = lesenseAltExPinIdleDis;
    altex->alwaysEx = true;

    /* Initialize the 2 GPIO pins of the light sensor setup. */
    NOS_GPIO_INIT_OUT(LIGHT_EXCITE_PORT, LIGHT_EXCITE_PIN);
    NOS_GPIO_INIT_PERIPHERAL(LIGHT_SENSE_PORT, LIGHT_SENSE_PIN);
}

#endif /* LESENSOR_LIGHT_M */
