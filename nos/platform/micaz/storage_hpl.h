/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @brief Hardware Presentation Layer for storage device (ext. flash memory)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef STORAGE_HPL_H
#define STORAGE_HPL_H

#include "nos_common.h"
#include "platform.h"
#include "arch.h"
#include "gpio.h"

#ifdef STORAGE_M

// Storage Device Declaration
#define AT45DB_M

void flash_spi_tx(UINT8 a);
void flash_spi_rx(UINT8 *a);
void flash_wait_until_ready(UINT8 ms_maxtime);

#define NOS_STORAGE_INIT() \
    do { \
        NOS_GPIO_INIT_OUT(FLASH_CSN_PORT, FLASH_CSN_PIN); \
        NOS_GPIO_ON(FLASH_CSN_PORT, FLASH_CSN_PIN); \
        NOS_GPIO_INIT_OUT(FLASH_CLK_PORT, FLASH_CLK_PIN); \
        NOS_GPIO_ON(FLASH_CLK_PORT, FLASH_CLK_PIN); \
        NOS_GPIO_INIT_OUT(FLASH_SI_PORT, FLASH_SI_PIN); \
        NOS_GPIO_ON(FLASH_SI_PORT, FLASH_SI_PIN); \
        NOS_GPIO_INIT_IN(FLASH_SO_PORT, FLASH_SO_PIN); \
    } while(0)

#define FLASH_SPI_ENABLE()  \
    do { \
        NOS_GPIO_OFF(FLASH_CLK_PORT, FLASH_CLK_PIN); \
        NOS_GPIO_OFF(FLASH_CSN_PORT, FLASH_CSN_PIN); \
        NOS_NOP(); /* for t_css (~CS Setup Time) 250 ns */\
    } while(0)

#define FLASH_SPI_DISABLE() \
    do {    \
        NOS_GPIO_OFF(FLASH_CLK_PORT, FLASH_CLK_PIN); \
        NOS_NOP(); /* for t_csh (~CS Hold Time) 250 ns */\
        NOS_GPIO_ON(FLASH_CSN_PORT, FLASH_CSN_PIN); \
    } while(0)

#define FLASH_SPI_TX(a)                     flash_spi_tx(a)
#define FLASH_SPI_RX(pa)                    flash_spi_rx(pa)
#define FLASH_WAIT_UNTIL_READY(ms_maxtime)  flash_wait_until_ready(ms_maxtime)

#endif //STORAGE_M
#endif //~STORAGE_HPL_H
