// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#ifndef PLATFORM_H
#define PLATFORM_H

#include <avr/io.h>
#include "kconf.h"
#include "uart.h"

#define _SYSTEM_CLOCK 7372800ul

enum
{
    STDIO = ATMEGA128_UART0,
};

/* ############################# RF Modem Devices ########################### */
enum
{
    ID_MICAZ_CC2420 = 0,
    WPAN_DEV_CNT = 1,
};
/* ########################################################################## */

#ifdef STORAGE_M
#endif //STORAGE_M

/* LEDs */
#define LED1_PORT                   A
#define LED1_PIN                    2 // PA.2 - Output : Red LED
#define LED2_PORT                   A
#define LED2_PIN                    1 // PA.1 - Output : Green LED
#define LED3_PORT                   A
#define LED3_PIN                    0 // PA.0 - Output : Yellow LED

/* External flash memory pins */
#define FLASH_CSN_PORT              A
#define FLASH_CSN_PIN               3 // PA.3 - Output : Flash Chip Select (FLASH_CS_N)
#define FLASH_SO_PORT               D
#define FLASH_SO_PIN                2 // PD.2 - Input:  USART1 RXD
#define FLASH_SI_PORT               D
#define FLASH_SI_PIN                3 // PD.3 - Output: USART1 TXD
#define FLASH_CLK_PORT              D
#define FLASH_CLK_PIN               5 // PD.5 - Output: USART1 CLK

/* MTS300 Sensor board pins */
#define MTS300_LIGHT_SENSOR_POWER_SW_PORT       E
#define MTS300_LIGHT_SENSOR_POWER_SW_PIN        5 // PE.5 - Light Sensor Power Switch
#define MTS300_LIGHT_SENSOR_ADC_CHANNEL_PORT    F
#define MTS300_LIGHT_SENSOR_ADC_CHANNEL_PIN     1 // ADC1 - adc channel 1

void nos_platform_init(void);

#endif // ~PLATFORM_H
