/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @brief Hardware Presentation Layer for storage device (ext. flash memory)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "storage_hpl.h"

#include "arch.h"

#ifdef STORAGE_M

#define FALLING_TICK()    \
    do { \
        NOS_GPIO_ON(FLASH_CLK_PORT, FLASH_CLK_PIN); \
        NOS_GPIO_OFF(FLASH_CLK_PORT, FLASH_CLK_PIN); \
    } while(0)

#define RISING_TICK() \
    do { \
        NOS_GPIO_OFF(FLASH_CLK_PORT, FLASH_CLK_PIN); \
        NOS_GPIO_ON(FLASH_CLK_PORT, FLASH_CLK_PIN); \
    } while(0)

void flash_spi_tx(UINT8 a) {
    NOS_GPIO_OFF(FLASH_CLK_PORT, FLASH_CLK_PIN);
    if (_IS_SET(a, 7)) NOS_GPIO_ON(FLASH_SI_PORT, FLASH_SI_PIN);
    else NOS_GPIO_OFF(FLASH_SI_PORT, FLASH_SI_PIN);
    FALLING_TICK();
    if (_IS_SET(a, 6)) NOS_GPIO_ON(FLASH_SI_PORT, FLASH_SI_PIN);
    else NOS_GPIO_OFF(FLASH_SI_PORT, FLASH_SI_PIN);
    FALLING_TICK();
    if (_IS_SET(a, 5)) NOS_GPIO_ON(FLASH_SI_PORT, FLASH_SI_PIN);
    else NOS_GPIO_OFF(FLASH_SI_PORT, FLASH_SI_PIN);
    FALLING_TICK();
    if (_IS_SET(a, 4)) NOS_GPIO_ON(FLASH_SI_PORT, FLASH_SI_PIN);
    else NOS_GPIO_OFF(FLASH_SI_PORT, FLASH_SI_PIN);
    FALLING_TICK();
    if (_IS_SET(a, 3)) NOS_GPIO_ON(FLASH_SI_PORT, FLASH_SI_PIN);
    else NOS_GPIO_OFF(FLASH_SI_PORT, FLASH_SI_PIN);
    FALLING_TICK();
    if (_IS_SET(a, 2)) NOS_GPIO_ON(FLASH_SI_PORT, FLASH_SI_PIN);
    else NOS_GPIO_OFF(FLASH_SI_PORT, FLASH_SI_PIN);
    FALLING_TICK();
    if (_IS_SET(a, 1)) NOS_GPIO_ON(FLASH_SI_PORT, FLASH_SI_PIN);
    else NOS_GPIO_OFF(FLASH_SI_PORT, FLASH_SI_PIN);
    FALLING_TICK();
    if (_IS_SET(a, 0)) NOS_GPIO_ON(FLASH_SI_PORT, FLASH_SI_PIN);
    else NOS_GPIO_OFF(FLASH_SI_PORT, FLASH_SI_PIN);
    FALLING_TICK();
}

void flash_spi_rx(UINT8 *a) {
    RISING_TICK();
    if (NOS_GPIO_READ(FLASH_SO_PORT, FLASH_SO_PIN)) _BIT_SET(*a, 7);
    else _BIT_CLR(*a, 7);
    RISING_TICK();
    if (NOS_GPIO_READ(FLASH_SO_PORT, FLASH_SO_PIN)) _BIT_SET(*a, 6);
    else _BIT_CLR(*a, 6);
    RISING_TICK();
    if (NOS_GPIO_READ(FLASH_SO_PORT, FLASH_SO_PIN)) _BIT_SET(*a, 5);
    else _BIT_CLR(*a, 5);
    RISING_TICK();
    if (NOS_GPIO_READ(FLASH_SO_PORT, FLASH_SO_PIN)) _BIT_SET(*a, 4);
    else _BIT_CLR(*a, 4);
    RISING_TICK();
    if (NOS_GPIO_READ(FLASH_SO_PORT, FLASH_SO_PIN)) _BIT_SET(*a, 3);
    else _BIT_CLR(*a, 3);
    RISING_TICK();
    if (NOS_GPIO_READ(FLASH_SO_PORT, FLASH_SO_PIN)) _BIT_SET(*a, 2);
    else _BIT_CLR(*a, 2);
    RISING_TICK();
    if (NOS_GPIO_READ(FLASH_SO_PORT, FLASH_SO_PIN)) _BIT_SET(*a, 1);
    else _BIT_CLR(*a, 1);
    RISING_TICK();
    if (NOS_GPIO_READ(FLASH_SO_PORT, FLASH_SO_PIN)) _BIT_SET(*a, 0);
    else _BIT_CLR(*a, 0);
    NOS_GPIO_OFF(FLASH_CLK_PORT, FLASH_CLK_PIN);
}

void flash_wait_until_ready(UINT8 ms_maxtime) {
    /* If the platform uses RDY/~BUSY pin to check the chip's status,
     * polling should be implemented. However, this platform does not
     * use that pin. So it just wait maximum time specified in the data sheet. */
    nos_delay_ms(ms_maxtime);
}

#endif //STORAGE_M
