// For Emacs: -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#ifndef PLATFORM_H
#define PLATFORM_H

#include "kconf.h"
#include "uart.h"

enum
{
    STDIO = MSP430F1611_USART1,
};

enum wpan_dev_id
{
    ID_UBEE430_CC2420 = 0,
    WPAN_DEV_CNT = 1,
};

enum storage_dev_id
{
    ID_UBEE430_M25P80 = 0,
    STORAGE_DEV_CNT = 1,
};

/* LEDs */
#define LED1_PORT                   5
#define LED1_PIN                    4 // P5.4 - Output : Red LED
#define LED2_PORT                   5
#define LED2_PIN                    5 // P5.5 - Output : Green LED
#define LED3_PORT                   5
#define LED3_PIN                    6 // P5.6 - Output : Blue LED

/*********************** PORT Definition ******************************/
/* SHT1x pins */
#define SHTXX_SDA_PORT              1
#define SHTXX_SDA_PIN               5
#define SHTXX_SCL_PORT              1
#define SHTXX_SCL_PIN               6
#define SHTXX_POWER_PORT            1
#define SHTXX_POWER_PIN             7

/* DS28DG02 */
#define PORT_DS28DG02_SCK           3
#define PIN_DS28DG02_SCK            0 //SPI
#define PORT_DS28DQ02_CSZ           4
#define PIN_DS28DG02_CSZ            3 //SPI
#define PORT_DS28DG02_ALMZ          2
#define PIN_DS28DG02_ALMZ           3
#define PORT_DS28DG02_RSTZ          2
#define PIN_DS28DG02_RSTZ           4
#define PORT_DS28DG02_SI            2
#define PIN_DS28DG02_SI             0 //SPI
#define PORT_DS28DG02_SO            2
#define PIN_DS28DG02_SO             1 //SPI

/* User switches */
#define PORT_USER_SWITCH0           2
#define PIN_USER_SWITCH0            7
#define PORT_USER_SWITCH1           2
#define PIN_USER_SWITCH1            6

#define PORT_BSLRX                  2
#define PIN_BSLRX                   2
#define PORT_BSLTX                  1
#define PIN_BSLTX                   1

#define PORT_ROSC                   2
#define PIN_ROSC                    5

#define PORT_SERIAL_FLASH_CS        4
#define PIN_SERIAL_FLASH_CS         4
#define PORT_SERIAL_FLASH_HOLD      4
#define PIN_SERIAL_FLASH_HOLD       7

#define PORT_DOOR_OPEN_CHECK_SENSOR 5
#define PIN_DOOR_OPEN_CHECK_SENSOR  7

#define PIR_SENSOR_ADC_CHANNEL_PORT 6
#define PIR_SENSOR_ADC_CHANNEL_PIN  0
#define LIGHT_SENSOR_ADC_CHANNEL_PORT   6
#define LIGHT_SENSOR_ADC_CHANNEL_PIN    5
//#define ADC_BAT_MONITOR_CHANNEL 7  // default by msp430
//#define ADC_INTERGRATED_TEMP_CHANNEL 10 // default by msp430. defined in arch/msp430f1611/temp_diode.h

void nos_platform_init(void);

#endif
