/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "user_switch.h"

#ifdef USER_SWITCH_M
#include "platform.h"

void (*nos_user_switch_cb)(void);

void nos_user_switch_init(void)
{
    // set as INPUT
    _BIT_CLR(USER_SWITCH_DDR, USER_SWITCH0);
    _BIT_CLR(USER_SWITCH_DDR, USER_SWITCH1);

    // Interrupt will be requested at the rising edge (low-to-high transition)
    _BIT_CLR(P2IES, USER_SWITCH0);
    _BIT_CLR(P2IES, USER_SWITCH1);

    ENABLE_USER_SWITCH_INTR();
    nos_user_switch_cb = NULL;
}

void nos_user_switch_set_cb(void (*func)(void))
{
    nos_user_switch_cb = func;
}

UINT8 nos_user_switch_get_state(void)
{
    UINT8 switch_state = 0;
    if (_IS_SET(USER_SWITCH_PIN, USER_SWITCH0))
        switch_state |= 0x01;
    if (_IS_SET(USER_SWITCH_PIN, USER_SWITCH1))
        switch_state |= 0x02;
    return switch_state;
}

#endif
