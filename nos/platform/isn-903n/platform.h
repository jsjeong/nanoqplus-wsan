// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 6. 15.
 */

#ifndef PLATFORM_H
#define PLATFORM_H
#include <avr/io.h>
#include "kconf.h"
#include "arch.h"
#include "gpio.h"
#include "intr.h"
#include "uart.h"

#ifdef IR_LCD_M
#include "irlcd.h"
#endif //IR_LCD_M

#ifdef STDIO_ISN_903N_USART0
#define STDIO      ATMEGA1284P_USART0
#define PPP_PORT   ATMEGA1284P_USART1

#elif defined STDIO_ISN_903N_USART1
#define STDIO      ATMEGA1284P_USART1
#define PPP_PORT   ATMEGA1284P_USART0

#elif defined STDIO_ISN_903N_PPP_UDP_51515
#define STDIO      254
#define PPP_PORT   ATMEGA1284P_USART1

#else
#define STDIO      ATMEGA1284P_NULL
#define PPP_PORT   ATMEGA1284P_USART1
#endif

#ifdef IR_LCD_M
#define IRLCD      ATMEGA1284P_USART1
#endif

enum wpan_dev_id
{
    ID_ISN_903N_RF212 = 0,
};

#define WPAN_DEV_CNT 1

// High-level sensor IDs that used by user applications
enum sensor_dev_id
{
    ID_ISN_903N_BH1600 = 0,
    ID_ISN_903N_ELT_S100 = 1,
    ID_ISN_903N_MICS5132 = 2,
    ID_ISN_903N_SHT11_HUMIDITY = 3,
    ID_ISN_903N_SHT11_TEMPERATURE = 4,
};

#define SENSOR_DEV_CNT 5

enum default_sensor
{
    DEFAULT_LIGHT = ID_ISN_903N_BH1600,
    DEFAULT_CO2 = ID_ISN_903N_ELT_S100,
    DEFAULT_CO = ID_ISN_903N_MICS5132,
    DEFAULT_HUM = ID_ISN_903N_SHT11_HUMIDITY,
    DEFAULT_TEMP = ID_ISN_903N_SHT11_TEMPERATURE,
};

/*
 * Low-level device IDs that used by each device's interface implementation.
 * (only for devices that provide multiple sensing values per device)
 */
enum shtxx_dev_id
{
    ID_ISN_903N_SHT11 = 0,
};

#define SHTXX_DEV_CNT 1

/* LEDs */
#define D1_PORT             A
#define D1_PIN              5
#define D2_PORT             A
#define D2_PIN              4
#define D3_PORT             D
#define D3_PIN              7

/* Buttons */
#define BUTTON1_PORT		A
#define BUTTON1_PIN			6	// push = clear(0), printed as SW3, PCINT6
#define BUTTON2_PORT		A
#define BUTTON2_PIN			7	// push = clear(0), printed as SW4, PCINT7
#define BUTTON3_PORT		B
#define BUTTON3_PIN			0	// push = clear(0), printed as SW5, PCINT8
#define BUTTON4_PORT		B
#define BUTTON4_PIN			2	// push = clear(0), printed as SW6, PCINT10, INT2

/*AT86RF212 modem*/
#define AT86RF212_SPI_CSN_PORT		B	// chip select. active low
#define AT86RF212_SPI_CSN_PIN		4
#define AT86RF212_IRQ_PORT			D	// interrupt request signal. input
#define AT86RF212_IRQ_PIN			6
#define AT86RF212_SLP_TR_PORT		B	// control signal
#define AT86RF212_SLP_TR_PIN		3
#define AT86RF212_RSTN_PORT			B	// reset. active low
#define AT86RF212_RSTN_PIN			1

/* PIR sensor */
#define PIR_SENSOR_PORT			A
#define PIR_SENSOR_PIN			2	//PA2. input. interrupts will trigger even if pins are configured as outputs.(can be used as SW interrupts)

#ifdef IEEE_802_15_4_DEV_M
/* #define ENABLE_RF_INTR()	ENABLE_TIMER1_CAPT_vect() */
/* #define DISABLE_RF_INTR()	DISABLE_TIMER1_CAPT_vect() */
/* #define CLEAR_RF_INTR()		CLEAR_TIMER1_CAPT_vect() */
#define ENABLE_RF_INTR()      ENABLE_PCINT3_vect()
#define DISABLE_RF_INTR()     DISABLE_PCINT3_vect()
#define CLEAR_RF_INTR()       CLEAR_PCINT3_vect()

void nos_rf_intr_init(void);
#endif

void nos_platform_init(void);

#endif // ~PLATFORM_H
