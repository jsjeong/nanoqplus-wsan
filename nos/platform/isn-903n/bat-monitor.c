// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Battery Monitor for iSN-903N
 * 
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 2. 19.
 */

/*
 * $Id$
 * $LastChangedDate$
 */

#include "bat-monitor.h"

#ifdef BATTERY_MONITOR_M
#include "pwr_mgr.h"

UINT8 nos_battery_read_level(void)
{
    UINT16 supply_voltage;
    
    /*
      Currently, the only way to measure battery life time is to estimate by
      using its supply voltage. Experimentally, iSN-903N mote seems not work
      under a certain level of supply voltage less than 1.8V. So we assume that
      voltage value under 1.8V is fully discharged (0%), and 3.3V, typical two
      AA batteries voltage, is fully charged (100%).
     */

    supply_voltage = nos_mcu_get_supply_voltage();

    if (supply_voltage > 3300)
    {
        return 100;
    }
    else if (supply_voltage < 1700)
    {
        return 0;
    }
    else
    {
        return (supply_voltage - 1700) / 16;
    }
}

void nos_battery_mon_init(void)
{
    // Nothing to do.
}

#endif //BATTERY_MONITOR_M
