// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * SHTxx sensor I2C-like GPIO interface implementation
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 11. 19.
 */

#include "kconf.h"
#ifdef SHTXX_M
#include "shtxx-interface.h"
#include "platform.h"
#include <avr/io.h>

#define SDA_PORT              D
#define SDA_PIN               0
#define SCL_PORT              D
#define SCL_PIN               1

void shtxx_interface_init(UINT8 chip_id)
{
    if (chip_id == ID_ISN_903N_SHT11)
    {
        _BIT_SET(DDRD, SDA_PIN);
        _BIT_SET(DDRD, SCL_PIN);
    }
}

void shtxx_set_scl(UINT8 chip_id, BOOL high)
{
    if (chip_id == ID_ISN_903N_SHT11)
    {
        if (high)
            _BIT_SET(PORTD, SCL_PIN);
        else
            _BIT_CLR(PORTD, SCL_PIN);
    }    
}

void shtxx_set_sda(UINT8 chip_id, BOOL high)
{
    if (chip_id == ID_ISN_903N_SHT11)
    {
        if (high)
            _BIT_SET(PORTD, SDA_PIN);
        else
            _BIT_CLR(PORTD, SDA_PIN);
    }
}

BOOL shtxx_get_sda(UINT8 chip_id)
{
    if (chip_id == ID_ISN_903N_SHT11)
    {
        return _IS_SET(PIND, SDA_PIN) ? TRUE : FALSE;
    }
    
    return FALSE;
}

void shtxx_set_in_sda(UINT8 chip_id)
{
    if (chip_id == ID_ISN_903N_SHT11)
    {
        _BIT_CLR(DDRD, SDA_PIN);
        _BIT_SET(PORTD, SDA_PIN); //pull-up
    }
}

void shtxx_set_out_sda(UINT8 chip_id)
{
    if (chip_id == ID_ISN_903N_SHT11)
    {
        _BIT_SET(DDRD, SDA_PIN);
    }
}

void shtxx_switch(UINT8 chip_id, BOOL on)
{
    //Nothing to do.
}

#endif //SHTXX_M
