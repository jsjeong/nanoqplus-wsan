/*
 * Copyright (c) 2006-2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * IR LCD Driver
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 11.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "irlcd.h"

#ifdef IR_LCD_M

#ifndef ATMEGA1284P_ASYNC_USART1_BR_38400
#error "USART1's baudrate should be 38400."
#endif //~ATMEGA1284P_ASYNC_USART1_BR_38400
#include "platform.h"
#include "uart.h"
#include "led.h"
#include "taskq.h"
#include <string.h>

enum
{
    COMMAND_READY = 1,
    COMMAND_INCOMING = 2,
    COMMAND_STARTED = 3,
    COMMAND_INCOMING_0 = 4,
};

static unsigned char serialBuffer[100];
static unsigned char serialIndex = 0;
static unsigned char serialLength = 0;
static unsigned char commandStatus = COMMAND_READY;

void check_command_packet(void *args)
{
    if (commandStatus != COMMAND_STARTED)
        return;

    switch (serialBuffer[0])
    {
    case 'C':
        if (serialBuffer[1] == 1)
            nos_led_on(1);
        else if (serialBuffer[1] == 0)
            nos_led_off(1);

        //lcdOnOffResponse_toRF(serialBuffer[1]);
        break;

    case 'M':
        //lcdMeterResponse_toRF(&serialBuffer[1]);
        break;

    case 'I':
        //lcdIrResponse_toRF(serialBuffer[1]);
        break;

    default:
        break;
    }

    commandStatus = COMMAND_READY;
    serialIndex = 0;
}

void irlcd_recv(UINT8 port, char c)
{
    if (serialIndex > 100)
    {
        commandStatus = COMMAND_READY;
        serialIndex = 0;
        return;
    }

    if (c == 0xE0 && commandStatus == COMMAND_READY)
    {
        commandStatus = COMMAND_INCOMING_0;
    }
    else if (commandStatus == COMMAND_INCOMING_0)
    {
        serialLength = c;
        commandStatus = COMMAND_INCOMING;
    }
    else if (c == 0xE1 && commandStatus == COMMAND_INCOMING)
    {
        if (serialIndex < serialLength)
        {
            serialBuffer[serialIndex] = c;
            serialIndex++;
        }
        else
        {
            commandStatus = COMMAND_STARTED;
            nos_taskq_reg(check_command_packet, NULL);
        }
    }
    else if (commandStatus == COMMAND_INCOMING)
    {
        serialBuffer[serialIndex] = c;
        serialIndex++;
        if (serialIndex > serialLength)
        {
            commandStatus = COMMAND_READY;
            serialIndex = 0;
        }
    }
    else
    {
        // COMMAND_READY or COMMAND_STARTED and serial data is not C0.
    }
}

void irlcd_init(void)
{
    nos_uart_set_getc_callback(IRLCD, irlcd_recv);
}

void irlcd_ir_request(UINT8 *d)
{
    unsigned char i;
    unsigned char packet[9] = {0xE0, 6 , 'I', 0x00, 0x00, 0x00, 0x00, 0x00, 0xE1};

    memcpy(&packet[3], d, 5);
    for(i = 0; i < 9; i++) nos_uart_putc(IRLCD, packet[i]);
}

void irlcd_on_off_request(UINT8 cmd)
{
    unsigned char i;
    unsigned char packet[5] = {0xE0, 2 , 'C', 0x00, 0xE1};

    packet[3] = cmd;
    for(i = 0; i < 5; i++) nos_uart_putc(IRLCD, packet[i]);
}

void irlcd_meter_request(void)
{
    unsigned char i;
    unsigned char packet[4] = {0xE0, 1, 'M', 0xE1};

    for(i = 0; i < 4; i++) nos_uart_putc(IRLCD, packet[i]);
}

#endif //IR_LCD_M
