// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "platform.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include "critical_section.h"
#include "gpio.h"

#ifdef UART_M
#include <stdio.h>
#include "uart.h"
#endif

#ifdef LED_M
#include "led.h"
#endif

#ifdef BUTTON_M
#include "button.h"
#endif

#ifdef AT86RF212_M
#include "rf212-interface.h"
#include "rf212.h"
#endif

#ifdef PIR_SENSOR_M
#include "pir_sensor.h"
#endif

#ifdef BH1600_M
#include "bh1600-interface.h"
#include "bh1600.h"
#endif

#ifdef ELT_S100_M
#include "elt_s100-interface.h"
#include "elt_s100.h"
#endif

#ifdef MICS5132_M
#include "mics5132-interface.h"
#include "mics5132.h"
#endif

#ifdef SHTXX_M
#include "shtxx-interface.h"
#include "sensor_shtxx.h"
#endif

#ifdef IR_LCD_M
#include "irlcd.h"
#endif

#ifdef GLOSSY_M
#include "glossy-interface.h"
#endif

#ifdef STDIO_ISN_903N_PPP_UDP_51515
#include "ppp-os.h"
#include "ip6-interface.h"
#include "udp.h"

char stdoutbuf[200];
UINT8 stdoutbuf_idx = 0;
static void udp_stdout(char c)
{
    UINT8 ppp_inf;
    IP6_ADDRESS opp;

    stdoutbuf[stdoutbuf_idx++] = c;
    if (stdoutbuf_idx == sizeof(stdoutbuf) || c == '\n')
    {
        ppp_inf = pppos_get_ip6_inf(PPP_PORT);
        if (ppp_inf == 255)
        {
            stdoutbuf_idx = 0;
            return;
        }

        nos_ip6_interface_get_neigh_addr(IP6_ADDRTYPE_LINKLOCAL, ppp_inf, 0, &opp);
        nos_udp_sendto(ppp_inf, 51515, &opp, 51515, stdoutbuf, stdoutbuf_idx);
        stdoutbuf_idx = 0;
    }
}
#endif

static int platform_putchar(char c, FILE *stream)
{
    if (c == '\n')
        platform_putchar('\r', stream);

        nos_led_toggle(2);
#if (defined STDIO_ISN_903N_USART0 || defined STDIO_ISN_903N_USART1)
    nos_uart_putc(STDIO, c);
    
#elif defined STDIO_ISN_903N_PPP_UDP_51515
    udp_stdout(c);
#endif
    return 0;
}

static FILE platform_stdout =
    FDEV_SETUP_STREAM(platform_putchar, NULL, _FDEV_SETUP_WRITE);

void nos_platform_init(void)
{
#ifdef LED_M
    nos_led_init();
#endif

#ifdef ATMEGA1284P_ASYNC_USART0
	nos_uart_init(ATMEGA1284P_USART0);
#endif
#ifdef ATMEGA1284P_ASYNC_USART1
    nos_uart_init(ATMEGA1284P_USART1);
#endif

    stdout = &platform_stdout;

#ifdef BUTTON_M
	nos_button_init();
#endif

    /* AT86RF212 radio initialization */
#ifdef AT86RF212_M
    rf212_interface_init(ID_ISN_903N_RF212);
#ifdef ATMEGA1284P_CORECLK_16MHZ
    rf212_init(ID_ISN_903N_RF212, RF212_CLKM_16MHZ);
#else
    rf212_init(ID_ISN_903N_RF212, RF212_CLKM_1MHZ);
#endif //ATMEGA1284P_CORECLK_16MHZ

#ifdef ISN_903N_RF212_INIT_MODE_BPSK_20
    rf212_set_mode(ID_ISN_903N_RF212, RF212_BPSK_20KBPS);
#elif defined ISN_903N_RF212_INIT_MODE_BPSK_40
    rf212_set_mode(ID_ISN_903N_RF212, RF212_BPSK_40KBPS);
#elif defined ISN_903N_RF212_INIT_MODE_OQPSK_SIN_RC_100
    rf212_set_mode(ID_ISN_903N_RF212, RF212_OQPSK_100KBPS);
#elif defined ISN_903N_RF212_INIT_MODE_OQPSK_SIN_250
    rf212_set_mode(ID_ISN_903N_RF212, RF212_OQPSK_250KBPS_SIN);
#elif defined ISN_903N_RF212_INIT_MODE_OQPSK_RC_250
    rf212_set_mode(ID_ISN_903N_RF212, RF212_OQPSK_250KBPS_RC_0_8);
#endif
    
    rf212_set_channel(ID_ISN_903N_RF212, CONFIG_ISN_903N_RF212_INIT_CH);
#endif //AT86RF212_M

#ifdef PIR_SENSOR_M
	pir_sensor_init();
#endif

#ifdef SHTXX_M
    shtxx_interface_init(ID_ISN_903N_SHT11);
	shtxx_init(ID_ISN_903N_SHT11,
               ID_ISN_903N_SHT11_TEMPERATURE,
               ID_ISN_903N_SHT11_HUMIDITY);
#endif

#ifdef BH1600_M
    bh1600_interface_init(ID_ISN_903N_BH1600);
	bh1600_init(ID_ISN_903N_BH1600);
#endif

#ifdef ELT_S100_M
    elt_s100_interface_init(ID_ISN_903N_ELT_S100);
	elt_s100_init(ID_ISN_903N_ELT_S100);
#endif

#ifdef MICS5132_M
    mics5132_interface_init(ID_ISN_903N_MICS5132);
	mics5132_init(ID_ISN_903N_MICS5132);
#endif

#ifdef IR_LCD_M
	irlcd_init();
#endif

#ifdef GLOSSY_M
    glossy_init_delay_timer(ID_ISN_903N_RF212);
#endif
}

#if defined(BUTTON_M) || defined(PIR_SENSOR_M)
ISR(PCINT0_vect)
{
#ifdef GLOSSY_M
    if (NOS_GPIO_READ(BUTTON1_PORT, BUTTON1_PIN))
        nos_button_callback(SW3);
#else
    
    NOS_ENTER_ISR();
#ifdef BUTTON_M
	if (nos_button_callback != NULL)
	{
		// run only if button is being pushed.
		if (!NOS_GPIO_READ(BUTTON1_PORT,BUTTON1_PIN))
		{
			while (!NOS_GPIO_READ(BUTTON1_PORT,BUTTON1_PIN));
			nos_button_callback(SW3);
			NOS_EXIT_ISR();
			return;
		}
		else if (!NOS_GPIO_READ(BUTTON2_PORT,BUTTON2_PIN))
		{
			while (!NOS_GPIO_READ(BUTTON2_PORT,BUTTON2_PIN));
			nos_button_callback(SW4);
			NOS_EXIT_ISR();
			return;
		}
	}
#endif

#ifdef PIR_SENSOR_M
	if (pir_sensor_callback != NULL)
	{
		if (!NOS_GPIO_READ(PIR_SENSOR_PORT,PIR_SENSOR_PIN))
		{
			pir_sensor_callback();
		}
	}
#endif
	NOS_EXIT_ISR();
#endif
}
#endif

#ifdef BUTTON_M
ISR(PCINT1_vect)
{
    NOS_ENTER_ISR();
	if (nos_button_callback != NULL)
	{
		// run only if button is being pushed.
		if (!NOS_GPIO_READ(BUTTON3_PORT,BUTTON3_PIN))
		{
			while (!NOS_GPIO_READ(BUTTON3_PORT,BUTTON3_PIN));
			nos_button_callback(SW5);
		}
		if (!NOS_GPIO_READ(BUTTON4_PORT,BUTTON4_PIN))
		{
			while (!NOS_GPIO_READ(BUTTON4_PORT,BUTTON4_PIN));
			nos_button_callback(SW6);
		}
	}
    NOS_EXIT_ISR();
}
#endif

#ifdef AT86RF212_M
ISR(TIMER1_CAPT_vect)
{
    NOS_ENTER_ISR();
    rf212_interrupt_handler(ID_ISN_903N_RF212);
    NOS_EXIT_ISR();
}
#endif
