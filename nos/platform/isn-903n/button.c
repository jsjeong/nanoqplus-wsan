// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "button.h"

#ifdef BUTTON_M
#include "platform.h"
#include "gpio.h"

// Callback function for user button. Argument is used to distinguish user buttons
void (*nos_button_callback)(UINT8);

void nos_button_init(void)
{
    // Interrupts will trigger even if pins are configured as outputs.(can be used as SW interrupts)
    /*NOS_GPIO_SET_IN(BUTTON1_PORT, BUTTON1_PIN);
	NOS_GPIO_SET_IN(BUTTON2_PORT, BUTTON2_PIN);
	NOS_GPIO_SET_IN(BUTTON3_PORT, BUTTON3_PIN);
	NOS_GPIO_SET_IN(BUTTON4_PORT, BUTTON4_PIN);*/

	// Pin change interrupt mask (rising and falling)
	_BIT_SET(PCMSK0, PCINT6);
	_BIT_SET(PCMSK0, PCINT7);
	_BIT_SET(PCMSK1, PCINT8);
	_BIT_SET(PCMSK1, PCINT10);

	// enable interrupt
    ENABLE_USER_BUTTON_INTR();

	// default callback
    nos_button_callback = NULL;
}

void nos_button_set_callback(void (*func)(UINT8))
{
    nos_button_callback = func;
}

#endif
