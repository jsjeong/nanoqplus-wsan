// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Interface between iSN-903N and BH1600.
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 3.
 */

#include "kconf.h"

#ifdef BH1600_M
#include "bh1600-interface.h"
#include "platform.h"
#include "gpio.h"
#include "adc.h"

#define ILLUM_SENSOR_ADC_CH     1
#define ILLUM_SENSOR_ADC_REFV   ADC_AVCC
#define ILLUM_SENSOR_ADC_PRES   ADC_PRESCALER_2
#define ILLUM_SENSOR_PORT       A
#define ILLUM_SENSOR_PIN        1

void bh1600_interface_init(UINT8 id)
{
    if (id == ID_ISN_903N_BH1600)
    {
        NOS_GPIO_INIT_IN(ILLUM_SENSOR_PORT, ILLUM_SENSOR_PIN);
        NOS_GPIO_OFF(ILLUM_SENSOR_PORT, ILLUM_SENSOR_PIN);
    }
}

/**
 * @return Analog-to-Digital converted value in unit of mV.
 */
UINT16 bh1600_interface_read_adc(UINT8 id, BOOL *h_gain, UINT16 *output_resistor)
{
    UINT32 mv;

    if (id == ID_ISN_903N_BH1600)
    {
        UINT16 reading;

        //'h_gain' may be different according to GCx jumper settings.
        *h_gain = FALSE;
        *output_resistor = 6800;
        reading = nos_adc_convert(ILLUM_SENSOR_ADC_CH,
                                  ILLUM_SENSOR_ADC_REFV,
                                  ILLUM_SENSOR_ADC_PRES, 1);
        
        mv = (UINT32) reading * 3300L / 1024L;
        return mv;
    }

    return 0xffff;
}

void bh1600_interface_switch(UINT8 id, BOOL turn_on)
{
    // Nothing to do. There is no way to turn the sensor on/off.
}

#endif // BH1600_M
