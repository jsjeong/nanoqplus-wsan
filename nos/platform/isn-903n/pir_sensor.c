/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */


#include "pir_sensor.h"

#ifdef PIR_SENSOR_M
#include "platform.h"
#include "gpio.h"

// Callback function for user button. Argument is used to distinguish user buttons
void (*pir_sensor_callback)(void);

void pir_sensor_init(void)
{
    // Interrupts will trigger even if pins are configured as outputs.(can be used as SW interrupts. --> output+ <PORT or PIN> reg change)
    //NOS_GPIO_SET_OUT(PIR_SENSOR_PORT, PIR_SENSOR_PIN); //default: in

	// Pin change interrupt mask (rising and falling. no other option.)
	_BIT_SET(PCMSK0, PCINT2);

	// enable interrupt
    ENABLE_PIR_SENSOR_INTR();

	// default callback
    pir_sensor_callback = NULL;
}

void pir_sensor_set_callback(void (*func)(void))
{
    pir_sensor_callback = func;
}

UINT8 pir_sensor_read(void)
{
	return !NOS_GPIO_READ(PIR_SENSOR_PORT,PIR_SENSOR_PIN);
}


#endif
