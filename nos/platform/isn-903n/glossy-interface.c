// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Implementation of Glossy interface for iSN-903N
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 8.
 */

#include "kconf.h"

#ifdef GLOSSY_M
#include "glossy-interface.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include "intr.h"
#include <stdio.h>
#include "critical_section.h"

void glossy_start_delay_timer(UINT8 dev_id, UINT16 delay_ticks)
{
    CLEAR_TIMER1_COMPA_vect();
        
    OCR1AH = (delay_ticks >> 8);
    OCR1AL = (delay_ticks & 0xFF);
        
    TCCR1B |= (1 << CS00);
}

UINT16 glossy_get_ticks(UINT8 dev_id)
{
    UINT16 cnt;
    
    cnt = TCNT1L;
    cnt += (TCNT1H << 8);
    return cnt;
}

void glossy_stop_timer(UINT8 dev_id)
{
    TCCR1B &= ~((1 << CS02) + (1 << CS01) + (1 << CS00));
}

void glossy_reset_timer(UINT8 dev_id)
{
    CLEAR_TIMER1_COMPA_vect();
    TCNT1H = 0;
    TCNT1L = 0;
}

void glossy_wait_until_delay_done(UINT8 dev_id)
{
    while (!IS_PENDING_TIMER1_COMPA_vect());
}

void glossy_init_delay_timer(UINT8 dev_id)
{
    CLEAR_TIMER1_OVF_vect();
    CLEAR_TIMER1_COMPA_vect();
    CLEAR_TIMER1_COMPB_vect();

    DISABLE_TIMER1_OVF_vect();
    ENABLE_TIMER1_COMPA_vect();
    DISABLE_TIMER1_COMPB_vect();
}

#endif //GLOSSY_M
