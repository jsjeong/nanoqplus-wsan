/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Platform-dependent LED functions
 * @author Jongsoo Jeong (ETRI)
 */

/*
 * $LastChangedDate: 2012-09-07 17:09:28 +0900 (금, 07 9 2012) $
 * $Id: led.c 950 2012-09-07 08:09:28Z haekim $
 */

#include "led.h"

#ifdef LED_M
#include "platform.h"
#include "gpio.h"

void nos_led_init(void)
{
    NOS_GPIO_INIT_OUT(D1_PORT, D1_PIN);
    NOS_GPIO_INIT_OUT(D2_PORT, D2_PIN);
    NOS_GPIO_INIT_OUT(D3_PORT, D3_PIN);

    nos_led_off(LED_D1);
    nos_led_off(LED_D2);
    nos_led_off(LED_D3);
}

void nos_led_on(UINT8 n)
{
    if (n == LED_D1)
    {
        NOS_GPIO_OFF(D1_PORT, D1_PIN);
    }
    else if (n == LED_D2)
    {
        NOS_GPIO_OFF(D2_PORT, D2_PIN);
    }
    else if (n == LED_D3)
    {
        NOS_GPIO_ON(D3_PORT, D3_PIN);
    }
}

void nos_led_off(UINT8 n)
{
    if (n == LED_D1)
    {
        NOS_GPIO_ON(D1_PORT, D1_PIN);
    }
    else if (n == LED_D2)
    {
        NOS_GPIO_ON(D2_PORT, D2_PIN);
    }
    else if (n == LED_D3)
    {
        NOS_GPIO_OFF(D3_PORT, D3_PIN);
    }
}

void nos_led_toggle(UINT8 n)
{
    if (n == LED_D1)
    {
        NOS_GPIO_TOGGLE(D1_PORT, D1_PIN);
    }
    else if (n == LED_D2)
    {
        NOS_GPIO_TOGGLE(D2_PORT, D2_PIN);
    }
    else if (n == LED_D3)
    {
        NOS_GPIO_TOGGLE(D3_PORT, D3_PIN);
    }
}

#endif //LED_M
