// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Mango-EToI platform
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 2.
 */

#ifndef PLATFORM_H_
#define PLATFORM_H_

#include "kconf.h"
#include "stm32f10x.h"
#include "arch.h"
#include "uart.h"

#ifdef LED_M
#include "led.h"
#endif

#ifdef BUTTON_M
#include "button.h"
#endif

#define SYSCLK 72000000
#define HCLK   72000000
#define PCLK1  36000000
#define PCLK2  72000000
#define ADCCLK 36000000

#ifdef UART_M
#define USART1_TX_PORT    GPIOA
#define USART1_TX_PIN     9
#define USART1_RX_PORT    GPIOA
#define USART1_RX_PIN     10

#define USART2_TX_PORT    GPIOA
#define USART2_TX_PIN     2
#define USART2_RX_PORT    GPIOA
#define USART2_RX_PIN     3

enum
{
#ifdef STDIO_MANGO_ETOI_USART1
    STDIO = STM32F10X_USART1,
#elif defined STDIO_MANGO_ETOI_USART2
    STDIO = STM32F10X_USART2,
#endif
};
#endif

#if (defined MANGO_ETOI_NET1_CC2520 ||          \
     defined MANGO_ETOI_NET1_RF212 ||           \
     defined MANGO_ETOI_NET1_CC1120) &&         \
    (defined MANGO_ETOI_NET2_CC2520 ||          \
     defined MANGO_ETOI_NET2_RF212 ||           \
     defined MANGO_ETOI_NET2_CC1120)
/* Both slot 1 and 2 are for WPAN devices. */
enum wpan_dev_id
{
    ID_MANGO_ETOI_NET1 = 0,
    ID_MANGO_ETOI_NET2 = 1,
};
#define WPAN_DEV_CNT 2

#elif (defined MANGO_ETOI_NET1_CC2520 ||        \
       defined MANGO_ETOI_NET1_RF212 ||         \
       defined MANGO_ETOI_NET1_CC1120)
/* Only slot 1 is for WPAN devices. */
enum wpan_dev_id
{
    ID_MANGO_ETOI_NET1 = 0,
};
#define WPAN_DEV_CNT 1

#elif (defined MANGO_ETOI_NET2_CC2520 ||        \
       defined MANGO_ETOI_NET2_RF212 ||         \
       defined MANGO_ETOI_NET2_CC1120)
/* Only slot 2 is for WPAN devices. */
enum wpan_dev_id
{
    ID_MANGO_ETOI_NET2 = 0,
};
#define WPAN_DEV_CNT 1

#else
/* No slot is for WPAN devices. */
#define WPAN_DEV_CNT 0
#endif

#if (defined MANGO_ETOI_NET1_ENC28J60 &&        \
     defined MANGO_ETOI_NET2_ENC28J60)
/* Both slot 1 and 2 are for ethernet devices. */
enum eth_dev_id
{
    ID_MANGO_ETOI_NET1 = 0,
    ID_MANGO_ETOI_NET2 = 1,
};
#define ETH_DEV_CNT 2

#elif (defined MANGO_ETOI_NET1_ENC28J60)
/* Only slot 1 is for ethernet devices. */
enum eth_dev_id
{
    ID_MANGO_ETOI_NET1 = 0,
};
#define ETH_DEV_CNT 1

#elif (defined MANGO_ETOI_NET2_ENC28J60)
/* Only slot 2 is for ethernet devices. */
enum eth_dev_id
{
    ID_MANGO_ETOI_NET2 = 0,
};
#define ETH_DEV_CNT 1

#else
/* No slot is for ethernet devices. */
#define ETH_DEV_CNT 0
#endif

#ifdef LMC1623_M
enum clcd_id
{
    ID_MANGO_ETOI_CLCD = 0,
};
#define CLCD_DEV_CNT 1
#define LMC1623_DEV_CNT 1
#endif

#define SPI1_PORT     GPIOA
#define SPI1_nCE      GPIO_Pin_4
#define SPI1_SCK      GPIO_Pin_5
#define SPI1_MISO     GPIO_Pin_6
#define SPI1_MOSI     GPIO_Pin_7

#define SPI2_PORT     GPIOB
#define SPI2_nCS      GPIO_Pin_12
#define SPI2_SCK      GPIO_Pin_13
#define SPI2_MISO     GPIO_Pin_14
#define SPI2_MOSI     GPIO_Pin_15

#define SPI_IO_PORT   GPIOD
#define SPI_IO0       GPIO_Pin_0
#define SPI_IO1       GPIO_Pin_1
#define SPI_IO2       GPIO_Pin_2
#define SPI_IO3       GPIO_Pin_3
#define SPI_IO4       GPIO_Pin_4
#define SPI_IO5       GPIO_Pin_5
#define SPI_IO6       GPIO_Pin_6
#define SPI_IO7       GPIO_Pin_7
#define SPI_IO8       GPIO_Pin_8
#define SPI_IO9       GPIO_Pin_9
#define SPI_IO10      GPIO_Pin_10
#define SPI_IO11      GPIO_Pin_11
#define SPI_IO12      GPIO_Pin_12
#define SPI_IO13      GPIO_Pin_13
#define SPI_IO14      GPIO_Pin_14
#define SPI_IO15      GPIO_Pin_15

void nos_platform_init(void);

#endif //PLATFORM_H_
