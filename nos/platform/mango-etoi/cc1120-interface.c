// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC1120 interface implementation for Mango-EToI
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 6.
 */

#include "kconf.h"

#ifdef CC1120_M
#include "cc1120-interface.h"
#include "cc1120-const.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_exti.h"
#include "platform.h"
#include "critical_section.h"

#define NET1_GPIO0          SPI_IO_PORT, SPI_IO5
#define NET1_GPIO2          SPI_IO_PORT, SPI_IO6
#define NET1_GPIO3          SPI_IO_PORT, SPI_IO4
#define NET1_RESET          SPI_IO_PORT, SPI_IO7
#define NET1_CSN            SPI1_PORT, SPI1_nCE
#define NET1_MISO           SPI1_PORT, SPI1_MISO

#define NET2_GPIO0          SPI_IO_PORT, SPI_IO13
#define NET2_GPIO1          SPI_IO_PORT, SPI_IO14
#define NET2_GPIO2          SPI_IO_PORT, SPI_IO12
#define NET2_RESET          SPI_IO_PORT, SPI_IO15
#define NET2_CSN            SPI2_PORT, SPI2_nCS
#define NET2_MISO           SPI2_PORT, SPI2_MISO

void cc1120_interface_init(UINT8 dev_id)
{
    GPIO_InitTypeDef g;
    SPI_InitTypeDef s;

#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |
                               RCC_APB2Periph_GPIOD |
                               RCC_APB2Periph_AFIO |
                               RCC_APB2Periph_SPI1,
                               ENABLE);

        // Initialize output pins.
        g.GPIO_Pin = SPI_IO7;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI_IO_PORT, &g);

        GPIO_ResetBits(NET1_RESET);

        // Initialize input pins.
        g.GPIO_Pin = (SPI_IO4 | SPI_IO5 | SPI_IO6);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_IPD;
        GPIO_Init(SPI_IO_PORT, &g);

        // Initialize SPI1 pins.
        g.GPIO_Pin = SPI1_nCE;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI1_PORT, &g);

        g.GPIO_Pin = (SPI1_SCK | SPI1_MOSI | SPI1_MISO);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(SPI1_PORT, &g);
        
        GPIO_SetBits(NET1_CSN);
        
        // Initialize SPI1.
        s.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
        s.SPI_Mode = SPI_Mode_Master;
        s.SPI_DataSize = SPI_DataSize_8b;
        s.SPI_CPOL = SPI_CPOL_Low;
        s.SPI_CPHA = SPI_CPHA_1Edge;
        s.SPI_NSS = SPI_NSS_Soft;
        s.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;   // 72MHz / 16 = 4.5MHz
        s.SPI_FirstBit = SPI_FirstBit_MSB;
        s.SPI_CRCPolynomial = 7;
        SPI_Init(SPI1, &s);
        SPI_Cmd(SPI1, ENABLE);

        cc1120_reset(dev_id);
        return;
    }
#endif
    
#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB |
                               RCC_APB2Periph_GPIOD |
                               RCC_APB2Periph_AFIO,
                               ENABLE);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

        // Initialize output pins.
        g.GPIO_Pin = SPI_IO14;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI_IO_PORT, &g);

        GPIO_ResetBits(NET2_RESET);

        // Initialize input pins.
        g.GPIO_Pin = (SPI_IO11 | SPI_IO12 | SPI_IO13);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_IPD;
        GPIO_Init(SPI_IO_PORT, &g);

        // Initialize SPI2 pins.
        g.GPIO_Pin = SPI2_nCE;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI2_PORT, &g);

        g.GPIO_Pin = (SPI2_SCK | SPI2_MOSI | SPI2_MISO);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(SPI2_PORT, &g);
        
        GPIO_SetBits(NET2_CSN);
        
        // Initialize SPI2.
        s.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
        s.SPI_Mode = SPI_Mode_Master;
        s.SPI_DataSize = SPI_DataSize_8b;
        s.SPI_CPOL = SPI_CPOL_Low;
        s.SPI_CPHA = SPI_CPHA_1Edge;
        s.SPI_NSS = SPI_NSS_Soft;
        s.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;   // 72MHz / 16 = 4.5MHz
        s.SPI_FirstBit = SPI_FirstBit_MSB;
        s.SPI_CRCPolynomial = 7;
        SPI_Init(SPI2, &s);
        SPI_Cmd(SPI2, ENABLE);

        cc1120_reset(dev_id);
        return;
    }
#endif
}

ERROR_T cc1120_request_cs(UINT8 dev_id)
{
    UINT16 msec;

#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        GPIO_ResetBits(NET1_CSN);

        // Wait until SO pin is low upto 10 seconds.
        for (msec = 0; msec < 10000; msec++)
        {
            if (GPIO_ReadInputDataBit(NET1_MISO) == RESET)
                break;
            
            nos_delay_ms(1);
        }
        goto out;
    }
#endif
    
#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        GPIO_ResetBits(NET2_CSN);

        // Wait until SO pin is low upto 10 seconds.
        for (msec = 0; msec < 10000; msec++)
        {
            if (GPIO_ReadInputDataBit(NET2_MISO) == RESET)
                break;
            
            nos_delay_ms(1);
        }
        goto out;
    }
#endif
    
    return ERROR_NOT_FOUND;

out:
    if (msec >= 10000)
    {
        return ERROR_FAIL;
    }
    else
    {
        return ERROR_SUCCESS;
    }
}

ERROR_T cc1120_release_cs(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        GPIO_SetBits(NET1_CSN);
        return ERROR_SUCCESS;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        GPIO_SetBits(NET2_CSN);
        return ERROR_SUCCESS;
    }
#endif

    return ERROR_NOT_FOUND;
}

ERROR_T cc1120_spi(UINT8 dev_id, UINT8 txdata, UINT8 *rxdata)
{
    UINT8 r;
    
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        SPI_I2S_SendData(SPI1, txdata);
        while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
        r = SPI_I2S_ReceiveData(SPI1);
        
        if (rxdata)
            *rxdata = r;

        return ERROR_SUCCESS;
    }
#endif
    
#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        SPI_I2S_SendData(SPI2, txdata);
        while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET);
        r = SPI_I2S_ReceiveData(SPI2);
        
        if (rxdata)
            *rxdata = r;

        return ERROR_SUCCESS;
    }
#endif
    
    return ERROR_NOT_FOUND;
}

ERROR_T cc1120_on(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        //TODO
        return ERROR_NOT_SUPPORTED;
    }
#endif
    
#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        //TODO
        return ERROR_NOT_SUPPORTED;
    }
#endif

    return ERROR_NOT_FOUND;
}

ERROR_T cc1120_off(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        //TODO
        return ERROR_NOT_SUPPORTED;
    }
#endif
    
#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        //TODO
        return ERROR_NOT_SUPPORTED;
    }
#endif

    return ERROR_NOT_FOUND;
}

ERROR_T cc1120_reset(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        GPIO_SetBits(NET1_RESET);
        nos_delay_ms(0);
        GPIO_ResetBits(NET1_RESET);
        return ERROR_SUCCESS;
    }
#endif
    
#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        GPIO_SetBits(NET2_RESET);
        nos_delay_ms(0);
        GPIO_ResetBits(NET2_RESET);
        return ERROR_SUCCESS;
    }
#endif
    
    return ERROR_NOT_FOUND;
}

ERROR_T cc1120_enable_intr(UINT8 dev_id, BOOL enable)
{
    EXTI_InitTypeDef e;

#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource0);
        e.EXTI_Line = EXTI_Line0;
        e.EXTI_Mode = EXTI_Mode_Interrupt;
        e.EXTI_Trigger = EXTI_Trigger_Falling;
        e.EXTI_LineCmd = (enable) ? ENABLE : DISABLE;
        EXTI_Init(&e);
        return ERROR_SUCCESS;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource8);
        e.EXTI_Line = EXTI_Line8;
        e.EXTI_Mode = EXTI_Mode_Interrupt;
        e.EXTI_Trigger = EXTI_Trigger_Falling;
        e.EXTI_LineCmd = (enable) ? ENABLE : DISABLE;
        EXTI_Init(&e);
        return ERROR_SUCCESS;
    }
#endif

    return ERROR_NOT_FOUND;
}

ERROR_T cc1120_clear_intr(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        EXTI_ClearFlag(EXTI_Line0);
        return ERROR_SUCCESS;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        EXTI_ClearFlag(EXTI_Line8);
        return ERROR_SUCCESS;
    }
#endif
    
    return ERROR_NOT_FOUND;
}

BOOL cc1120_get_intr(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        return (EXTI_GetITStatus(EXTI_Line0) != RESET);
    }
#endif

#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        return (EXTI_GetITStatus(EXTI_Line8) != RESET);
    }
#endif

    return FALSE;
}

ERROR_T cc1120_prepare_tx(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        //Nothing to do.
        return ERROR_SUCCESS;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        //Nothing to do.
        return ERROR_SUCCESS;
    }
#endif
    
    return ERROR_NOT_FOUND;
}

ERROR_T cc1120_prepare_rx(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        //Nothing to do.
        return ERROR_SUCCESS;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        //Nothing to do.
        return ERROR_SUCCESS;
    }
#endif
    
    return ERROR_NOT_FOUND;
}

ERROR_T cc1120_get_pin_state(UINT8 dev_id, enum cc1120_pin pin, BOOL *pin_is_set)
{
    if (!pin_is_set)
        return ERROR_INVALID_ARGS;
    
#ifdef MANGO_ETOI_NET1_CC1120
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        if (pin == CC1120_PIN_GPIO0)
        {
            *pin_is_set = GPIO_ReadInputDataBit(NET1_GPIO0);
        }
        else if (pin == CC1120_PIN_GPIO2)
        {
            *pin_is_set = GPIO_ReadInputDataBit(NET1_GPIO2);
        }
        else if (pin == CC1120_PIN_GPIO3)
        {
            *pin_is_set = GPIO_ReadInputDataBit(NET1_GPIO3);
        }
        else
        {
            return ERROR_INVALID_ARGS;
        }
        return ERROR_SUCCESS;
    }
#endif
    
#ifdef MANGO_ETOI_NET2_CC1120
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        if (pin == CC1120_PIN_GPIO0)
        {
            *pin_is_set = GPIO_ReadInputDataBit(NET2_GPIO0);
        }
        else if (pin == CC1120_PIN_GPIO2)
        {
            *pin_is_set = GPIO_ReadInputDataBit(NET2_GPIO2);
        }
        else if (pin == CC1120_PIN_GPIO3)
        {
            *pin_is_set = GPIO_ReadInputDataBit(NET2_GPIO3);
        }
        else
        {
            return ERROR_INVALID_ARGS;
        }
        return ERROR_SUCCESS;
    }
#endif
    
    return ERROR_NOT_FOUND;
}

#endif //CC1120_M
