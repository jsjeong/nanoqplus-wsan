// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ENC28J60 interface implementation for Mango-EToI
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 10.
 */

#include "kconf.h"

#ifdef ENC28J60_M
#include "enc28j60-interface.h"
#include "platform.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_exti.h"
#include "critical_section.h"

#define NET1_INTN           SPI_IO_PORT, SPI_IO6
#define NET1_RESET          SPI_IO_PORT, SPI_IO7
#define NET1_CSN            SPI1_PORT, SPI1_nCE

#define NET2_INTN           SPI_IO_PORT, SPI_IO14
#define NET2_RESET          SPI_IO_PORT, SPI_IO15
#define NET2_CSN            SPI2_PORT, SPI2_nCS

void enc28j60_request_cs(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        NVIC_DisableIRQ(EXTI9_5_IRQn);
        GPIO_ResetBits(NET1_CSN);
        return;
    }
#endif

#ifdef MANGO_ETOI_NET2_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        NVIC_DisableIRQ(EXTI15_10_IRQn);
        GPIO_ResetBits(NET2_CSN);
        return;
    }
#endif
}

void enc28j60_release_cs(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        GPIO_SetBits(NET1_CSN);
        NVIC_EnableIRQ(EXTI9_5_IRQn);
        return;
    }
#endif

#ifdef MANGO_ETOI_NET2_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        GPIO_SetBits(NET2_CSN);
        NVIC_EnableIRQ(EXTI15_10_IRQn);
        return;
    }
#endif
}

UINT8 enc28j60_spi(UINT8 dev_id, UINT8 txd)
{
#ifdef MANGO_ETOI_NET1_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        SPI_I2S_SendData(SPI1, txd);
        while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
        return SPI_I2S_ReceiveData(SPI1);
    }
#endif

#ifdef MANGO_ETOI_NET2_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        SPI_I2S_SendData(SPI2, txd);
        while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET);
        return SPI_I2S_ReceiveData(SPI2);
    }
#endif

    return 0;
}

void enc28j60_interface_init(UINT8 dev_id)
{
    GPIO_InitTypeDef g;
    SPI_InitTypeDef s;
    EXTI_InitTypeDef e;

#ifdef MANGO_ETOI_NET1_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |
                               RCC_APB2Periph_GPIOD |
                               RCC_APB2Periph_AFIO |
                               RCC_APB2Periph_SPI1,
                               ENABLE);

        // Initialize output pins.
        g.GPIO_Pin = SPI_IO7;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI_IO_PORT, &g);

        GPIO_ResetBits(NET1_RESET);

        // Initialize input pins.
        g.GPIO_Pin = SPI_IO6;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_IPU;
        GPIO_Init(SPI_IO_PORT, &g);

        // Initialize SPI1 pins.
        g.GPIO_Pin = SPI1_nCE;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI1_PORT, &g);

        g.GPIO_Pin = (SPI1_SCK | SPI1_MOSI | SPI1_MISO);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(SPI1_PORT, &g);
        
        GPIO_SetBits(NET1_CSN);
        
        // Initialize SPI1.
        s.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
        s.SPI_Mode = SPI_Mode_Master;
        s.SPI_DataSize = SPI_DataSize_8b;
        s.SPI_CPOL = SPI_CPOL_Low;
        s.SPI_CPHA = SPI_CPHA_1Edge;
        s.SPI_NSS = SPI_NSS_Soft;
        s.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;   // 72MHz / 8 = 9MHz
        s.SPI_FirstBit = SPI_FirstBit_MSB;
        s.SPI_CRCPolynomial = 7;
        SPI_Init(SPI1, &s);
        SPI_Cmd(SPI1, ENABLE);

        GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource6);
        e.EXTI_Line = EXTI_Line6;
        e.EXTI_Mode = EXTI_Mode_Interrupt;
        e.EXTI_Trigger = EXTI_Trigger_Falling;
        e.EXTI_LineCmd = ENABLE;
        EXTI_Init(&e);
        
        GPIO_SetBits(NET1_RESET);
        nos_delay_ms(0);
        GPIO_ResetBits(NET1_RESET);
        return;
    }
#endif

#ifdef MANGO_ETOI_NET2_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB |
                               RCC_APB2Periph_GPIOD |
                               RCC_APB2Periph_AFIO,
                               ENABLE);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

        // Initialize output pins.
        g.GPIO_Pin = SPI_IO15;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI_IO_PORT, &g);

        GPIO_ResetBits(NET2_RESET);

        // Initialize input pins.
        g.GPIO_Pin = SPI_IO14;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_IPU;
        GPIO_Init(SPI_IO_PORT, &g);

        // Initialize SPI2 pins.
        g.GPIO_Pin = (SPI2_SCK | SPI2_MISO | SPI2_MOSI);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(SPI2_PORT, &g);
        
        g.GPIO_Pin = SPI2_nCS;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI2_PORT, &g);

        GPIO_SetBits(NET2_CSN);
        
        // Initialize SPI2.
        s.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
        s.SPI_Mode = SPI_Mode_Master;
        s.SPI_DataSize = SPI_DataSize_8b;
        s.SPI_CPOL = SPI_CPOL_Low;
        s.SPI_CPHA = SPI_CPHA_1Edge;
        s.SPI_NSS = SPI_NSS_Soft;
        s.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;   // 72MHz / 8 = 9MHz
        s.SPI_FirstBit = SPI_FirstBit_MSB;
        s.SPI_CRCPolynomial = 7;
        SPI_Init(SPI2, &s);
        SPI_Cmd(SPI2, ENABLE);

        GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource14);
        e.EXTI_Line = EXTI_Line14;
        e.EXTI_Mode = EXTI_Mode_Interrupt;
        e.EXTI_Trigger = EXTI_Trigger_Falling;
        e.EXTI_LineCmd = ENABLE;
        EXTI_Init(&e);
        
        GPIO_SetBits(NET2_RESET);
        nos_delay_ms(0);
        GPIO_ResetBits(NET2_RESET);
        return;
    }
#endif
}

BOOL enc28j60_intn_pin_is_set(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET1)
        return (GPIO_ReadInputDataBit(NET1_INTN)) ? TRUE : FALSE;
#endif

#ifdef MANGO_ETOI_NET2_ENC28J60
    if (dev_id == ID_MANGO_ETOI_NET2)
        return (GPIO_ReadInputDataBit(NET2_INTN)) ? TRUE : FALSE;
#endif

    return FALSE;
}

#endif //ENC28J60_M
