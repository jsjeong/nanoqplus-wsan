// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Mango-EToI platform
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 4.
 */

#include <stdio.h>
#include "platform.h"
#include "critical_section.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_rcc.h"

#ifdef UART_M
#include "uart.h"
#ifdef __GNUC__
UINT8 iobuf[3];
#endif
#endif

#ifdef LED_M
#include "led.h"
#endif

#ifdef BUTTON_M
#include "button.h"
#endif

#if defined LIB_UTC_CLOCK_M || defined KERNEL_M
#include "rtc.h"
#endif

#ifdef CC2520_M
#include "cc2520-interface.h"
#include "cc2520.h"
#endif

#ifdef AT86RF212_M
#include "rf212-interface.h"
#include "rf212.h"
#endif

#ifdef CC1120_M
#include "cc1120-interface.h"
#include "cc1120.h"
#endif

#ifdef ENC28J60_M
#include "enc28j60-interface.h"
#include "enc28j60.h"
#endif

#ifdef CLCD_M
#include "clcd.h"
#endif

#ifdef LMC1623_M
#include "lmc1623-interface.h"
#include "lmc1623.h"
struct lmc1623_state lmc1623[LMC1623_DEV_CNT];
#endif

void nos_platform_init(void)
{
    NVIC_InitTypeDef n;

    /*
     * Configure preemption priority groups. 0~3 for preemtion group. 0~3 for
     * priority. 0:highest, 3:lowest. Refer <misc.h>.
     */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

#ifdef LED_M	
    nos_led_init();
#endif

#ifdef STM32F10X_USART1_FOR_ASYNC
    nos_uart_init(STM32F10X_USART1);
    n.NVIC_IRQChannel = USART1_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 1;
    n.NVIC_IRQChannelSubPriority = 0;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);
#endif
#ifdef STM32F10X_USART2_FOR_ASYNC
    nos_uart_init(STM32F10X_USART2);
    n.NVIC_IRQChannel = USART2_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 1;
    n.NVIC_IRQChannelSubPriority = 0;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);
#endif
#ifdef LMC1623_M
    lmc1623_interface_init(ID_MANGO_ETOI_CLCD);
    lmc1623_init(ID_MANGO_ETOI_CLCD, &lmc1623[0]);
    clcd_set_line(ID_MANGO_ETOI_CLCD, 0, "ETRI NanoQplus  ");
    clcd_set_line(ID_MANGO_ETOI_CLCD, 1, " on Mango-EToI  ");
#endif

 #if defined UART_M && defined __GNUC__
    // I/O buffer initialization not to use buffering
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
#endif

#ifdef BUTTON_M
    nos_button_init();
#endif

    GPIO_ResetBits(GPIOC, GPIO_Pin_0);

    // Initialize chip on network interface 1.
#if defined MANGO_ETOI_NET1_CC2520
    cc2520_interface_init(ID_MANGO_ETOI_NET1);
    cc2520_init(ID_MANGO_ETOI_NET1, CONFIG_MANGO_ETOI_NET1_CC2520_INIT_CH);
    
#elif defined MANGO_ETOI_NET1_RF212
    rf212_interface_init(ID_MANGO_ETOI_NET1);
    rf212_init(ID_MANGO_ETOI_NET1, RF212_CLKM_NOCLK);
    
#ifdef MANGO_ETOI_NET1_RF212_INIT_MODE_BPSK_20
    rf212_set_mode(ID_MANGO_ETOI_NET1,
                   RF212_BPSK_20KBPS);
#elif defined MANGO_ETOI_NET1_RF212_INIT_MODE_BPSK_40
    rf212_set_mode(ID_MANGO_ETOI_NET1,
                   RF212_BPSK_40KBPS);
#elif defined MANGO_ETOI_NET1_RF212_INIT_MODE_OQPSK_SIN_RC_100
    rf212_set_mode(ID_MANGO_ETOI_NET1,
                   RF212_OQPSK_100KBPS);
#elif defined MANGO_ETOI_NET1_RF212_INIT_MODE_OQPSK_SIN_250
    rf212_set_mode(ID_MANGO_ETOI_NET1,
                   RF212_OQPSK_250KBPS_SIN);
#elif defined MANGO_ETOI_NET1_RF212_INIT_MODE_OQPSK_RC_250
    rf212_set_mode(ID_MANGO_ETOI_NET1,
                   RF212_OQPSK_250KBPS_RC_0_8);
#endif
    rf212_set_channel(ID_MANGO_ETOI_NET1,
                      CONFIG_MANGO_ETOI_NET1_RF212_INIT_CH);
    
#elif defined MANGO_ETOI_NET1_CC1120
    cc1120_interface_init(ID_MANGO_ETOI_NET1);
    cc1120_init(ID_MANGO_ETOI_NET1);
    
#elif defined MANGO_ETOI_NET1_ENC28J60
    enc28j60_interface_init(ID_MANGO_ETOI_NET1);
    enc28j60_init(ID_MANGO_ETOI_NET1);
#endif

    // Intialize chip on network interface 2.
#if defined MANGO_ETOI_NET2_CC2520
    cc2520_interface_init(ID_MANGO_ETOI_NET2);
    cc2520_init(ID_MANGO_ETOI_NET2, CONFIG_MANGO_ETOI_NET2_CC2520_INIT_CH);
    
#elif defined MANGO_ETOI_NET2_RF212
    rf212_interface_init(ID_MANGO_ETOI_NET2);
    rf212_init(ID_MANGO_ETOI_NET2, RF212_CLKM_NOCLK);
    
#ifdef MANGO_ETOI_NET2_RF212_INIT_MODE_BPSK_20
    rf212_set_mode(ID_MANGO_ETOI_NET2,
                   RF212_BPSK_20KBPS);
#elif defined MANGO_ETOI_NET2_RF212_INIT_MODE_BPSK_40
    rf212_set_mode(ID_MANGO_ETOI_NET2,
                   RF212_BPSK_40KBPS);
#elif defined MANGO_ETOI_NET2_RF212_INIT_MODE_OQPSK_SIN_RC_100
    rf212_set_mode(ID_MANGO_ETOI_NET2,
                   RF212_OQPSK_100KBPS);
#elif defined MANGO_ETOI_NET2_RF212_INIT_MODE_OQPSK_SIN_250
    rf212_set_mode(ID_MANGO_ETOI_NET2,
                   RF212_OQPSK_250KBPS_SIN);
#elif defined MANGO_ETOI_NET2_RF212_INIT_MODE_OQPSK_RC_250
    rf212_set_mode(ID_MANGO_ETOI_NET2,
                   RF212_OQPSK_250KBPS_RC_0_8);
#endif
    rf212_set_channel(ID_MANGO_ETOI_NET2,
                      CONFIG_MANGO_ETOI_NET2_RF212_INIT_CH);

#elif defined MANGO_ETOI_NET2_CC1120
    cc1120_interface_init(ID_MANGO_ETOI_NET2);
    cc1120_init(ID_MANGO_ETOI_NET2);
    
#elif defined MANGO_ETOI_NET2_ENC28J60
    enc28j60_interface_init(ID_MANGO_ETOI_NET2);
    enc28j60_init(ID_MANGO_ETOI_NET2);
#endif

#if (defined MANGO_ETOI_NET1_CC2520 ||           \
     defined MANGO_ETOI_NET1_RF212 ||            \
     defined MANGO_ETOI_NET1_CC1120)
    n.NVIC_IRQChannel = EXTI0_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 2;
    n.NVIC_IRQChannelSubPriority = 0;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);
#endif

#if (defined MANGO_ETOI_NET2_CC2520 ||          \
     defined MANGO_ETOI_NET2_RF212 ||           \
     defined MANGO_ETOI_NET2_CC1120 ||          \
     defined MANGO_ETOI_NET1_ENC28J60)
    n.NVIC_IRQChannel = EXTI9_5_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 2;
    n.NVIC_IRQChannelSubPriority = 0;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);
#endif

#if defined BUTTON_M ||                         \
    defined MANGO_ETOI_NET2_ENC28J60
    n.NVIC_IRQChannel = EXTI15_10_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 2;
    n.NVIC_IRQChannelSubPriority = 0;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);
#endif
    
#if defined LIB_UTC_CLOCK_M || defined KERNEL_M
    nos_rtc_init();
    n.NVIC_IRQChannel = RTCAlarm_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 0;
    n.NVIC_IRQChannelSubPriority = 0;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);	
#endif
}
