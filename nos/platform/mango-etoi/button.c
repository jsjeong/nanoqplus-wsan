// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Buttons
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 4.
 */

#include "button.h"

#ifdef BUTTON_M
#include <stm32f10x_gpio.h>
#include "critical_section.h"

#define KEY1_PORT            GPIOC
#define KEY1_PIN             GPIO_Pin_11

#define KEY2_PORT            GPIOC
#define KEY2_PIN             GPIO_Pin_12

#define KEY3_PORT            GPIOC
#define KEY3_PIN             GPIO_Pin_13

static void (*callback)(UINT8) = NULL;

#ifdef KERNEL_M
#include "taskq.h"
static UINT8 intr_bitmap = 0;
#endif

void nos_button_init(void)
{
    GPIO_InitTypeDef g;
    EXTI_InitTypeDef e;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    g.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13;
    g.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOC, &g);

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource11);
    e.EXTI_Line    = EXTI_Line11;
    e.EXTI_Mode    = EXTI_Mode_Interrupt;
    e.EXTI_Trigger = EXTI_Trigger_Falling;
    e.EXTI_LineCmd = ENABLE;
    EXTI_Init(&e);

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource12);
    e.EXTI_Line    = EXTI_Line12;
    e.EXTI_Mode    = EXTI_Mode_Interrupt;
    e.EXTI_Trigger = EXTI_Trigger_Falling;
    e.EXTI_LineCmd = ENABLE;
    EXTI_Init(&e);

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource13);
    e.EXTI_Line    = EXTI_Line13;
    e.EXTI_Mode    = EXTI_Mode_Interrupt;
    e.EXTI_Trigger = EXTI_Trigger_Falling;
    e.EXTI_LineCmd = ENABLE;
    EXTI_Init(&e);
}

void nos_button_set_callback(void (*func)(UINT8))
{
    callback = func;
}

#ifdef KERNEL_M
static void notify(void *args)
{
    NOS_ENTER_CRITICAL_SECTION();
    while (intr_bitmap != 0)
    {
        if (_IS_SET(intr_bitmap, KEY1))
        {
            _BIT_CLR(intr_bitmap, KEY1);
            NOS_EXIT_CRITICAL_SECTION();
            
            callback(KEY1);

            NOS_ENTER_CRITICAL_SECTION();
        }

        if (_IS_SET(intr_bitmap, KEY2))
        {
            _BIT_CLR(intr_bitmap, KEY2);
            NOS_EXIT_CRITICAL_SECTION();

            callback(KEY2);

            NOS_ENTER_CRITICAL_SECTION();
        }

        if (_IS_SET(intr_bitmap, KEY3))
        {
            _BIT_CLR(intr_bitmap, KEY3);
            NOS_EXIT_CRITICAL_SECTION();

            callback(KEY3);

            NOS_ENTER_CRITICAL_SECTION();
        }
    }
    NOS_EXIT_CRITICAL_SECTION();
}
#endif

void button_pressed(UINT8 btn)
{
    if (callback != NULL)
    {
#ifdef KERNEL_M
        if (intr_bitmap == 0)
            nos_taskq_reg(notify, NULL);
        _BIT_SET(intr_bitmap, btn);
#else
        callback(btn);
#endif
    }
}
#endif //BUTTON_M
