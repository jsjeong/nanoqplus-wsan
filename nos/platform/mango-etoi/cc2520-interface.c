// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2520 interface for Mango-EToI
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 5.
 */

#include "kconf.h"
#ifdef CC2520_M
#include "cc2520-interface.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "platform.h"
#include "critical_section.h"
#include "cc2520.h"

#define NET1_GPIO0          SPI_IO_PORT, SPI_IO0
#define NET1_GPIO1          SPI_IO_PORT, SPI_IO1
#define NET1_GPIO2          SPI_IO_PORT, SPI_IO2
#define NET1_GPIO3          SPI_IO_PORT, SPI_IO3
#define NET1_GPIO4          SPI_IO_PORT, SPI_IO4
#define NET1_GPIO5          SPI_IO_PORT, SPI_IO5
#define NET1_VREG_EN        SPI_IO_PORT, SPI_IO6
#define NET1_RESET          SPI_IO_PORT, SPI_IO7
#define NET1_CSN            SPI1_PORT, SPI1_nCE

#define NET2_GPIO0          SPI_IO_PORT, SPI_IO8
#define NET2_GPIO1          SPI_IO_PORT, SPI_IO9
#define NET2_GPIO2          SPI_IO_PORT, SPI_IO10
#define NET2_GPIO3          SPI_IO_PORT, SPI_IO11
#define NET2_GPIO4          SPI_IO_PORT, SPI_IO12
#define NET2_GPIO5          SPI_IO_PORT, SPI_IO13
#define NET2_VREG_EN        SPI_IO_PORT, SPI_IO14
#define NET2_RESET          SPI_IO_PORT, SPI_IO15
#define NET2_CSN            SPI2_PORT, SPI2_nCS

void cc2520_interface_init(UINT8 dev_id)
{
    GPIO_InitTypeDef g;
    SPI_InitTypeDef s;

#ifdef MANGO_ETOI_NET1_CC2520
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |
                               RCC_APB2Periph_GPIOD |
                               RCC_APB2Periph_AFIO |
                               RCC_APB2Periph_SPI1,
                               ENABLE);

        // Initialize output pins.
        g.GPIO_Pin = (SPI_IO6 | SPI_IO7);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI_IO_PORT, &g);

        cc2520_set_pin(dev_id, CC2520_PIN_RESET, FALSE);
        cc2520_set_pin(dev_id, CC2520_PIN_VREG_EN, FALSE);

        // Initialize input pins.
        g.GPIO_Pin = (SPI_IO0 | SPI_IO1 | SPI_IO2 | SPI_IO3 | SPI_IO4 | SPI_IO5);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        GPIO_Init(SPI_IO_PORT, &g);

        // Initialize SPI1 pins.
        g.GPIO_Pin = SPI1_nCE;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI1_PORT, &g);

        g.GPIO_Pin = (SPI1_SCK | SPI1_MOSI | SPI1_MISO);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(SPI1_PORT, &g);
        
        GPIO_SetBits(NET1_CSN);
        
        // Initialize SPI1.
        s.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
        s.SPI_Mode = SPI_Mode_Master;
        s.SPI_DataSize = SPI_DataSize_8b;
        s.SPI_CPOL = SPI_CPOL_Low;
        s.SPI_CPHA = SPI_CPHA_1Edge;
        s.SPI_NSS = SPI_NSS_Soft;
        s.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;   // 72MHz / 16 = 4.5MHz
        s.SPI_FirstBit = SPI_FirstBit_MSB;
        s.SPI_CRCPolynomial = 7;
        SPI_Init(SPI1, &s);
        SPI_Cmd(SPI1, ENABLE);

        // Interrupt should be enabled after CC2520 GPIO pins configuration.
        cc2520_rx_done_intr_flag_enable(dev_id, FALSE);
        return;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC2520
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB |
                               RCC_APB2Periph_GPIOD |
                               RCC_APB2Periph_AFIO,
                               ENABLE);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

        // Initialize output pins.
        g.GPIO_Pin = (SPI_IO14 | SPI_IO15);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI_IO_PORT, &g);

        cc2520_set_pin(dev_id, CC2520_PIN_RESET, FALSE);
        cc2520_set_pin(dev_id, CC2520_PIN_VREG_EN, FALSE);

        // Initialize input pins.
        g.GPIO_Pin = (SPI_IO8 | SPI_IO9 | SPI_IO10 | SPI_IO11 | SPI_IO12 | SPI_IO13);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_IN_FLOATING;
        GPIO_Init(SPI_IO_PORT, &g);

        // Initialize SPI2 pins.
        g.GPIO_Pin = (SPI2_SCK | SPI2_MISO | SPI2_MOSI);
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_AF_PP;
        GPIO_Init(SPI2_PORT, &g);
        
        g.GPIO_Pin = SPI2_nCS;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(SPI2_PORT, &g);

        GPIO_SetBits(NET2_CSN);
        
        // Initialize SPI2.
        s.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
        s.SPI_Mode = SPI_Mode_Master;
        s.SPI_DataSize = SPI_DataSize_8b;
        s.SPI_CPOL = SPI_CPOL_Low;
        s.SPI_CPHA = SPI_CPHA_1Edge;
        s.SPI_NSS = SPI_NSS_Soft;
        s.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;   // 72MHz / 16 = 4.5MHz
        s.SPI_FirstBit = SPI_FirstBit_MSB;
        s.SPI_CRCPolynomial = 7;
        SPI_Init(SPI2, &s);
        SPI_Cmd(SPI2, ENABLE);

        // Interrupt should be enabled after CC2520 GPIO pins configuration.
        cc2520_rx_done_intr_flag_enable(dev_id, FALSE);
        return;
    }
#endif
}

void cc2520_request_cs(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC2520
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        GPIO_ResetBits(NET1_CSN);
        return;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC2520
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        GPIO_ResetBits(NET2_CSN);
        return;
    }
#endif
}

void cc2520_release_cs(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC2520
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        GPIO_SetBits(NET1_CSN);
        return;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC2520
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        GPIO_SetBits(NET2_CSN);
        return;
    }
#endif
}

UINT8 cc2520_spi(UINT8 dev_id, UINT8 txd)
{
#ifdef MANGO_ETOI_NET1_CC2520
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        SPI_I2S_SendData(SPI1, txd);
        while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
        return SPI_I2S_ReceiveData(SPI1);
    }
#endif

#ifdef MANGO_ETOI_NET2_CC2520
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        SPI_I2S_SendData(SPI2, txd);
        while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET);
        return SPI_I2S_ReceiveData(SPI2);
    }
#endif

    return 0;
}

BOOL cc2520_pin_is_set(UINT8 dev_id, enum cc2520_pin pin)
{
#ifdef MANGO_ETOI_NET1_CC2520
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        if (pin == CC2520_PIN_GPIO0)
        {
            return GPIO_ReadInputDataBit(NET1_GPIO0);
        }
        else if (pin == CC2520_PIN_GPIO1)
        {
            return GPIO_ReadInputDataBit(NET1_GPIO1);
        }
        else if (pin == CC2520_PIN_GPIO2)
        {
            return GPIO_ReadInputDataBit(NET1_GPIO2);
        }
        else if (pin == CC2520_PIN_RESET)
        {
            return GPIO_ReadInputDataBit(NET1_RESET);
        }
        else if (pin == CC2520_PIN_VREG_EN)
        {
            return GPIO_ReadInputDataBit(NET1_VREG_EN);
        }
        else if (pin == CC2520_PIN_SPI_MISO)
        {
            return GPIO_ReadInputDataBit(SPI1_PORT, SPI1_MISO);
        }
    }
#endif

#ifdef MANGO_ETOI_NET2_CC2520
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        if (pin == CC2520_PIN_GPIO0)
        {
            return GPIO_ReadInputDataBit(NET2_GPIO0);
        }
        else if (pin == CC2520_PIN_GPIO1)
        {
            return GPIO_ReadInputDataBit(NET2_GPIO1);
        }
        else if (pin == CC2520_PIN_GPIO2)
        {
            return GPIO_ReadInputDataBit(NET2_GPIO2);
        }
        else if (pin == CC2520_PIN_RESET)
        {
            return GPIO_ReadInputDataBit(NET2_RESET);
        }
        else if (pin == CC2520_PIN_VREG_EN)
        {
            return GPIO_ReadInputDataBit(NET2_VREG_EN);
        }
        else if (pin == CC2520_PIN_SPI_MISO)
        {
            return GPIO_ReadInputDataBit(SPI2_PORT, SPI2_MISO);
        }
    }
#endif

    return FALSE;
}

void cc2520_set_pin(UINT8 dev_id, enum cc2520_pin pin, BOOL set)
{
#ifdef MANGO_ETOI_NET1_CC2520
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        if (pin == CC2520_PIN_GPIO0)
        {
            if (set) GPIO_SetBits(NET1_GPIO0);
            else GPIO_ResetBits(NET1_GPIO0);
        }
        else if (pin == CC2520_PIN_GPIO1)
        {
            if (set) GPIO_SetBits(NET1_GPIO1);
            else GPIO_ResetBits(NET1_GPIO1);
        }
        else if (pin == CC2520_PIN_GPIO2)
        {
            if (set) GPIO_SetBits(NET1_GPIO2);
            else GPIO_ResetBits(NET1_GPIO2);
        }
        else if (pin == CC2520_PIN_RESET)
        {
            if (set) GPIO_ResetBits(NET1_RESET);
            else GPIO_SetBits(NET1_RESET);
        }
        else if (pin == CC2520_PIN_VREG_EN)
        {
            if (set) GPIO_SetBits(NET1_VREG_EN);
            else GPIO_ResetBits(NET1_VREG_EN);
        }
    }
#endif

#ifdef MANGO_ETOI_NET2_CC2520
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        if (pin == CC2520_PIN_GPIO0)
        {
            if (set) GPIO_SetBits(NET2_GPIO0);
            else GPIO_ResetBits(NET2_GPIO0);
        }
        else if (pin == CC2520_PIN_GPIO1)
        {
            if (set) GPIO_SetBits(NET2_GPIO1);
            else GPIO_ResetBits(NET2_GPIO1);
        }
        else if (pin == CC2520_PIN_GPIO2)
        {
            if (set) GPIO_SetBits(NET2_GPIO2);
            else GPIO_ResetBits(NET2_GPIO2);
        }
        else if (pin == CC2520_PIN_RESET)
        {
            if (set) GPIO_ResetBits(NET2_RESET);
            else GPIO_SetBits(NET2_RESET);
        }
        else if (pin == CC2520_PIN_VREG_EN)
        {
            if (set) GPIO_SetBits(NET2_VREG_EN);
            else GPIO_ResetBits(NET2_VREG_EN);
        }
    }
#endif
}

BOOL cc2520_rx_done_pin_is_set(UINT8 dev_id)
{
    return cc2520_pin_is_set(dev_id, CC2520_PIN_GPIO0);
}

BOOL cc2520_rx_done_intr_flag_is_set(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC2520
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        return (EXTI_GetITStatus(EXTI_Line0) != RESET);
    }
#endif

#ifdef MANGO_ETOI_NET2_CC2520
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        return (EXTI_GetITStatus(EXTI_Line8) != RESET);
    }
#endif

    return FALSE;
}

void cc2520_rx_done_intr_flag_clear(UINT8 dev_id)
{
#ifdef MANGO_ETOI_NET1_CC2520
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        EXTI_ClearFlag(EXTI_Line0);
        return;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC2520
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        EXTI_ClearFlag(EXTI_Line8);
        return;
    }
#endif
}

void cc2520_rx_done_intr_flag_enable(UINT8 dev_id, BOOL enable)
{
    EXTI_InitTypeDef e;

#ifdef MANGO_ETOI_NET1_CC2520
    if (dev_id == ID_MANGO_ETOI_NET1)
    {
        GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource0);
        e.EXTI_Line = EXTI_Line0;
        e.EXTI_Mode = EXTI_Mode_Interrupt;
        e.EXTI_Trigger = EXTI_Trigger_Rising;
        e.EXTI_LineCmd = (enable) ? ENABLE : DISABLE;
        EXTI_Init(&e);
        return;
    }
#endif

#ifdef MANGO_ETOI_NET2_CC2520
    if (dev_id == ID_MANGO_ETOI_NET2)
    {
        GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource8);
        e.EXTI_Line = EXTI_Line8;
        e.EXTI_Mode = EXTI_Mode_Interrupt;
        e.EXTI_Trigger = EXTI_Trigger_Rising;
        e.EXTI_LineCmd = (enable) ? ENABLE : DISABLE;
        EXTI_Init(&e);
        return;
    }
#endif
}

BOOL cc2520_tx_done_pin_is_set(UINT8 dev_id)
{
    return cc2520_pin_is_set(dev_id, CC2520_PIN_GPIO1);
}

BOOL cc2520_sampled_cca_pin_is_set(UINT8 dev_id)
{
    return cc2520_pin_is_set(dev_id, CC2520_PIN_GPIO2);
}
#endif //CC2520_M
