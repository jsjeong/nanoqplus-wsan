// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LMC1623 16x2 character LCD interface implementation
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 4.
 */

#include "kconf.h"

#ifdef LMC1623_M
#include "lmc1623-interface.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

#define RS_PIN      GPIOE, GPIO_Pin_0
#define NRW_PIN     GPIOE, GPIO_Pin_1
#define ENABLE_PIN  GPIOE, GPIO_Pin_2
#define D0_PIN      GPIOE, GPIO_Pin_3
#define D1_PIN      GPIOE, GPIO_Pin_4
#define D2_PIN      GPIOE, GPIO_Pin_5
#define D3_PIN      GPIOE, GPIO_Pin_6
#define D4_PIN      GPIOE, GPIO_Pin_7
#define D5_PIN      GPIOE, GPIO_Pin_8
#define D6_PIN      GPIOE, GPIO_Pin_9
#define D7_PIN      GPIOE, GPIO_Pin_10

void lmc1623_interface_init(UINT8 dev_id)
{
    GPIO_InitTypeDef g;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
    
    g.GPIO_Pin = (GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 |
                  GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 |
                  GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10);
    g.GPIO_Speed = GPIO_Speed_50MHz;
    g.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOE, &g);
}

void lmc1623_enable(UINT8 dev_id)
{
    GPIO_SetBits(ENABLE_PIN);
}

void lmc1623_disable(UINT8 dev_id)
{
    GPIO_ResetBits(ENABLE_PIN);
}

void lmc1623_rs_data(UINT8 dev_id)
{
    GPIO_SetBits(RS_PIN);
}

void lmc1623_rs_control(UINT8 dev_id)
{
    GPIO_ResetBits(RS_PIN);
}

void lmc1623_read(UINT8 dev_id)
{
    GPIO_SetBits(NRW_PIN);
}

void lmc1623_write(UINT8 dev_id)
{
    GPIO_ResetBits(NRW_PIN);
}

void lmc1623_write_byte(UINT8 dev_id, UINT8 d)
{
    if (d & 0x01) GPIO_SetBits(D0_PIN);
    else GPIO_ResetBits(D0_PIN);
    
    if (d & 0x02) GPIO_SetBits(D1_PIN);
    else GPIO_ResetBits(D1_PIN);
    
    if (d & 0x04) GPIO_SetBits(D2_PIN);
    else GPIO_ResetBits(D2_PIN);
    
    if (d & 0x08) GPIO_SetBits(D3_PIN);
    else GPIO_ResetBits(D3_PIN);
    
    if (d & 0x10) GPIO_SetBits(D4_PIN);
    else GPIO_ResetBits(D4_PIN);
    
    if (d & 0x20) GPIO_SetBits(D5_PIN);
    else GPIO_ResetBits(D5_PIN);
    
    if (d & 0x40) GPIO_SetBits(D6_PIN);
    else GPIO_ResetBits(D6_PIN);
    
    if (d & 0x80) GPIO_SetBits(D7_PIN);
    else GPIO_ResetBits(D7_PIN);
}

#endif //LMC1623_M
