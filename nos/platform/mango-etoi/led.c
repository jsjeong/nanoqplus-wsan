// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LEDs
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 4.
 */

#include "led.h"
#ifdef LED_M
#include "platform.h"

#define LED1_PIN        GPIOC, GPIO_Pin_8
#define LED2_PIN        GPIOC, GPIO_Pin_9
#define LED3_PIN        GPIOC, GPIO_Pin_10

void nos_led_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    nos_led_off(LED1);
    nos_led_off(LED2);
    nos_led_off(LED3);
}

void nos_led_on(UINT8 n)
{
    if (n == LED1)
        GPIO_ResetBits(LED1_PIN);
    else if (n == LED2)
        GPIO_ResetBits(LED2_PIN);
    else if (n == LED3)
        GPIO_ResetBits(LED3_PIN);
}

void nos_led_off(UINT8 n)
{
    if (n == LED1) 
        GPIO_SetBits(LED1_PIN);
    else if (n == LED2)
        GPIO_SetBits(LED2_PIN);
    else if (n == LED3)
        GPIO_SetBits(LED3_PIN);
}

void nos_led_toggle(UINT8 n)
{
    if (n == LED1)
        GPIO_WriteBit(LED1_PIN, (BitAction) !GPIO_ReadOutputDataBit(LED1_PIN));
    else if (n == LED2)
        GPIO_WriteBit(LED2_PIN, (BitAction) !GPIO_ReadOutputDataBit(LED2_PIN));
    else if (n == LED3)
        GPIO_WriteBit(LED3_PIN, (BitAction) !GPIO_ReadOutputDataBit(LED3_PIN));
}

BOOL nos_led_get_status(UINT8 n)
{
    if (n == LED1)
        return (GPIO_ReadOutputDataBit(LED1_PIN)) ? FALSE : TRUE;
    else if (n == LED2)
        return (GPIO_ReadOutputDataBit(LED2_PIN)) ? FALSE : TRUE;
    else if (n == LED3)
        return (GPIO_ReadOutputDataBit(LED3_PIN)) ? FALSE : TRUE;

    return FALSE;
}

#endif //LED_M
