// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief LEDs (red, yellow, blue)
 * @date 2013. 1. 18.
 * @author Haeyong Kim (ETRI)
 */


#include "led.h"
#ifdef LED_M
#include "platform.h"

/* LEDs */
#define LED1_PORT       GPIOB
#define LED1_PIN        GPIO_Pin_9	//red
#define LED2_PORT       GPIOB
#define LED2_PIN        GPIO_Pin_5	//yellow
#define LED3_PORT       GPIOB
#define LED3_PIN        GPIO_Pin_8	//blue

void nos_led_init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    GPIO_InitStructure.GPIO_Pin = LED3_PIN | LED1_PIN | LED2_PIN ;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    nos_led_off(RLED);
    nos_led_off(YLED);
    nos_led_off(BLED);
}

void nos_led_on(UINT8 n)
{
    if (n == RLED)
        GPIO_ResetBits(LED1_PORT, LED1_PIN);
    else if (n == YLED)
        GPIO_ResetBits(LED2_PORT, LED2_PIN);
    else if (n == BLED)
        GPIO_ResetBits(LED3_PORT, LED3_PIN);
}

void nos_led_off(UINT8 n)
{
    if (n == RLED) 
        GPIO_SetBits(LED1_PORT, LED1_PIN);
    else if (n == YLED)
        GPIO_SetBits(LED2_PORT, LED2_PIN);
    else if (n == BLED)
        GPIO_SetBits(LED3_PORT, LED3_PIN);
}

void nos_led_toggle(UINT8 n)
{
    if (n == RLED)
    {
        GPIO_WriteBit(LED1_PORT, LED1_PIN,
                      (BitAction) !GPIO_ReadOutputDataBit(LED1_PORT, LED1_PIN));
    }
    else if (n == YLED)
    {
        GPIO_WriteBit(LED2_PORT, LED2_PIN,
                      (BitAction) !GPIO_ReadOutputDataBit(LED2_PORT, LED2_PIN));
    }
    else if (n == BLED)
    {
        GPIO_WriteBit(LED3_PORT, LED3_PIN,
                      (BitAction) !GPIO_ReadOutputDataBit(LED3_PORT, LED3_PIN));
    }
}

#endif //LED_M
