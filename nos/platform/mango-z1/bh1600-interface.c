// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CX Light Sensor (BH1600) board on Mango-Z1
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 11. 15.
 */

#include "kconf.h"

#ifdef BH1600_M
#include "bh1600-interface.h"
#include "platform.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_dma.h"

#define GC1        GPIOC, GPIO_Pin_0
#define GC2        GPIOC, GPIO_Pin_1
#define LIGHT_OUT  GPIOC, GPIO_Pin_4

#define ADC1_DR_ADDR ((UINT32) 0x4001244C)
__IO UINT16 adc_converted_value;

void bh1600_interface_init(UINT8 id)
{
    GPIO_InitTypeDef g;
    DMA_InitTypeDef d;
    ADC_InitTypeDef a;
    
    if (id == ID_MANGO_Z1_BH1600)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

        g.GPIO_Pin = GPIO_Pin_4;
        g.GPIO_Mode = GPIO_Mode_AIN;
        GPIO_Init(GPIOC, &g);

        g.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
        g.GPIO_Speed = GPIO_Speed_50MHz;
        g.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_Init(GPIOC, &g);

        bh1600_interface_switch(ID_MANGO_Z1_BH1600, TRUE);

        DMA_DeInit(DMA1_Channel1);
        d.DMA_PeripheralBaseAddr = ADC1_DR_ADDR;
        d.DMA_MemoryBaseAddr = (UINT32) &adc_converted_value;
        d.DMA_DIR = DMA_DIR_PeripheralSRC;
        d.DMA_BufferSize = 1;
        d.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
        d.DMA_MemoryInc = DMA_MemoryInc_Disable;
        d.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
        d.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
        d.DMA_Mode = DMA_Mode_Circular;
        d.DMA_Priority = DMA_Priority_High;
        d.DMA_M2M = DMA_M2M_Disable;
        DMA_Init(DMA1_Channel1, &d);
        DMA_Cmd(DMA1_Channel1, ENABLE);

        a.ADC_Mode = ADC_Mode_Independent;
        a.ADC_ScanConvMode = DISABLE;
        a.ADC_ContinuousConvMode = ENABLE;
        a.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
        a.ADC_DataAlign = ADC_DataAlign_Right;
        a.ADC_NbrOfChannel = 1;
        ADC_Init(ADC1, &a);
        ADC_RegularChannelConfig(ADC1, ADC_Channel_14, 1, ADC_SampleTime_55Cycles5);
        ADC_DMACmd(ADC1, ENABLE);
        ADC_Cmd(ADC1, ENABLE);

        ADC_ResetCalibration(ADC1);
        while(ADC_GetResetCalibrationStatus(ADC1));

        ADC_StartCalibration(ADC1);
        while(ADC_GetCalibrationStatus(ADC1));

        ADC_SoftwareStartConvCmd(ADC1, ENABLE);
    }
}

UINT16 bh1600_interface_read_adc(UINT8 id, BOOL *h_gain, UINT16 *output_resistor)
{
    if (id == ID_MANGO_Z1_BH1600)
    {
        *h_gain = TRUE;
        *output_resistor = 4700; //4.7Kohm
        return adc_converted_value;
    }
    else
    {
        return 0;
    }
}

void bh1600_interface_switch(UINT8 id, BOOL turn_on)
{
    if (id == ID_MANGO_Z1_BH1600)
    {
        if (turn_on)
        {
            //H-Gain mode
            GPIO_SetBits(GC1);
            GPIO_ResetBits(GC2);
        }
        else
        {
            //Shutdown mode
            GPIO_ResetBits(GC1);
            GPIO_ResetBits(GC2);
        }
    }
}

#endif //BH1600_M
