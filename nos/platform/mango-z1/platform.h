// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Mango-Z1 platform
 *
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 17.
 */


#ifndef PLATFORM_H_
#define PLATFORM_H_

#include "kconf.h"
#include "stm32f10x.h"
#include "arch.h"
#include "uart.h"

#ifdef LED_M
#include "led.h"
#endif

#ifdef BUTTON_M
#include "button.h"
#endif

#define SYSCLK 72000000
#define HCLK   72000000
#define PCLK1  36000000
#define PCLK2  72000000
#define ADCCLK 36000000

#ifdef UART_M
#define USART1_TX_PORT    GPIOA
#define USART1_TX_PIN     9
#define USART1_RX_PORT    GPIOA
#define USART1_RX_PIN     10

enum
{
    STDIO = STM32F10X_USART1,
};
#endif

enum wpan_dev_id
{
    ID_MANGO_Z1_CC2520 = 0,
};
#define WPAN_DEV_CNT 1

enum light_sensor_dev_id
{
    ID_MANGO_Z1_BH1600 = 0,
};
#define SENSOR_DEV_CNT 1

void nos_platform_init(void);

#endif /* PLATFORM_H_ */
