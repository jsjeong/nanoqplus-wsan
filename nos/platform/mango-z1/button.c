// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Buttons (WKUP, USER)
 * @date 2013. 1. 18.
 * @author Haeyong Kim (ETRI)
 */

#include "button.h"

#ifdef BUTTON_M
#include <stm32f10x_gpio.h>

#define BUTTON_WKUP_PORT     GPIOA	
#define BUTTON_WKUP_PIN      GPIO_Pin_0	
#define BUTTON_WKUP_PORTSRC	 GPIO_PortSourceGPIOA
#define BUTTON_WKUP_PINSRC   GPIO_PinSource0

#define BUTTON_USER_PORT     GPIOA
#define BUTTON_USER_PIN      GPIO_Pin_1
#define BUTTON_USER_PORTSRC  GPIO_PortSourceGPIOA
#define BUTTON_USER_PINSRC   GPIO_PinSource1

static void (*pressed)(UINT8) = NULL;

#ifdef KERNEL_M
#include "taskq.h"
#include "critical_section.h"
static UINT8 intr_bitmap = 0;
#endif

void nos_button_init(void)
{
    GPIO_InitTypeDef g;
    EXTI_InitTypeDef e;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    g.GPIO_Pin = BUTTON_WKUP_PIN| BUTTON_USER_PIN;
    g.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &g);

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);
    e.EXTI_Line    = EXTI_Line0;
    e.EXTI_Mode    = EXTI_Mode_Interrupt;
    e.EXTI_Trigger = EXTI_Trigger_Falling;
    e.EXTI_LineCmd = ENABLE;
    EXTI_Init(&e);

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource1);
    e.EXTI_Line    = EXTI_Line1;
    e.EXTI_Mode    = EXTI_Mode_Interrupt;
    e.EXTI_Trigger = EXTI_Trigger_Falling;
    e.EXTI_LineCmd = ENABLE;
    EXTI_Init(&e);
}

void nos_button_set_callback(void (*func)(UINT8))
{
    pressed = func;
}

#ifdef KERNEL_M
static void notify(void *args)
{
    NOS_ENTER_CRITICAL_SECTION();
    while (intr_bitmap != 0)
    {
        if (_IS_SET(intr_bitmap, BUTTON_WKUP))
        {
            _BIT_CLR(intr_bitmap, BUTTON_WKUP);
            NOS_EXIT_CRITICAL_SECTION();
            
            pressed(BUTTON_WKUP);

            NOS_ENTER_CRITICAL_SECTION();
        }

        if (_IS_SET(intr_bitmap, BUTTON_USER))
        {
            _BIT_CLR(intr_bitmap, BUTTON_USER);
            NOS_EXIT_CRITICAL_SECTION();

            pressed(BUTTON_USER);

            NOS_ENTER_CRITICAL_SECTION();
        }
    }
    NOS_EXIT_CRITICAL_SECTION();
}
#endif

void nos_button_exe(UINT8 btn)
{
    if (pressed != NULL)
    {
#ifdef KERNEL_M
        if (intr_bitmap == 0)
            nos_taskq_reg(notify, NULL);
        _BIT_SET(intr_bitmap, btn);
#else
        pressed(btn);
#endif
    }
}

#endif

