// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Mango-Z1's IRQ handlers
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "platform.h"
#include "critical_section.h"
#include "rtc.h"
#include <stdio.h>

#ifdef BUTTON_M
#include "button.h"
#endif

#ifdef CC2520_M
#include "cc2520.h"
#endif

#ifdef KERNEL_M
#include "hal_sched.h"
#include "sched.h"
#endif

#ifdef LIB_UTC_CLOCK_M
#include "utc-clock.h"
#endif

void HardFault_Handler(void)
{
    /*
     * Get the appropriate stack pointer, depending on our mode,
     * and use it as the parameter to the C handler. This function
     * will never return
     */
    __asm("TST   LR, #4");
    __asm("ITE   EQ");
    __asm("MRSEQ R0, MSP");
    __asm("MRSNE R0, PSP");
    __asm("B HardFault_HandlerC");
}

void HardFault_HandlerC(uint32_t *stack_pointer)
{
    uint32_t stacked_r0;
    uint32_t stacked_r1;
    uint32_t stacked_r2;
    uint32_t stacked_r3;
    uint32_t stacked_r12;
    uint32_t stacked_lr;
    uint32_t stacked_pc;
    uint32_t stacked_psr;

    stacked_r0 = ((uint32_t) stack_pointer[0]);
    stacked_r1 = ((uint32_t) stack_pointer[1]);
    stacked_r2 = ((uint32_t) stack_pointer[2]);
    stacked_r3 = ((uint32_t) stack_pointer[3]);

    stacked_r12 = ((uint32_t) stack_pointer[4]);
    stacked_lr =  ((uint32_t) stack_pointer[5]);
    stacked_pc =  ((uint32_t) stack_pointer[6]);
    stacked_psr = ((uint32_t) stack_pointer[7]);

    printf("\n\n[HardFault]\n");
    printf("R0        = %08x\n", (unsigned int)stacked_r0);
    printf("R1        = %08x\n", (unsigned int)stacked_r1);
    printf("R2        = %08x\n", (unsigned int)stacked_r2);
    printf("R3        = %08x\n", (unsigned int)stacked_r3);
    printf("R12       = %08x\n", (unsigned int)stacked_r12);
    printf("LR [R14]  = %08x - Subroutine Call return address\n", (unsigned int)stacked_lr);
    printf("PC [R15]  = %08x - Program Counter\n", (unsigned int)stacked_pc);
    printf("PSR       = %08x\n", (unsigned int)stacked_psr);
    printf("BFAR      = %08x - Bus Fault SR/Address causing bus fault\n",
           (unsigned int) (*((volatile uint32_t *)(0xE000ED38))));
    printf("CFSR      = %08x - Config. Fault SR\n",
           (unsigned int) (*((volatile uint32_t *)(0xE000ED28))));
    if((*((volatile uint32_t *)(0xE000ED28)))&(1<<25))
    {
        printf("  :UsageFault->DivByZero\n");
    }
    if((*((volatile uint32_t *)(0xE000ED28)))&(1<<24))
    {
        printf("  :UsageFault->Unaligned access\n");
    }
    if((*((volatile uint32_t *)(0xE000ED28)))&(1<<18))
    {
        printf("  :UsageFault->Integrity check error\n");
    }
    if((*((volatile uint32_t *)(0xE000ED28)))&(1<<0))
    {
        printf("  :MemFault->Data access violation\n");
    }
    if((*((volatile uint32_t *)(0xE000ED28)))&(1<<0))
    {
        printf("  :MemFault->Instruction access violation\n");
    }
    printf("HFSR      = %08x - Hard Fault SR\n",
           (unsigned int)(*((volatile uint32_t *)(0xE000ED2C))));
    if((*((volatile uint32_t *)(0xE000ED2C)))&(1UL<<1))
    {
        printf("  :VECTBL, Failed vector fetch\n");
    }
    if((*((volatile uint32_t *)(0xE000ED2C)))&(1UL<<30))
    {
        printf("  :FORCED, Bus fault/Memory management fault/usage fault\n");
    }
    if((*((volatile uint32_t *)(0xE000ED2C)))&(1UL<<31))
    {
        printf("  :DEBUGEVT, Hard fault triggered by debug event\n");
    }
    printf("DFSR      = %08x - Debug Fault SR\n", (unsigned int)(*((volatile uint32_t *)(0xE000ED30))));
    printf("MMAR      = %08x - Memory Manage Address R\n", (unsigned int)(*((volatile uint32_t *)(0xE000ED34))));
    printf("AFSR      = %08x - Auxilirary Fault SR\n", (unsigned int)(*((volatile uint32_t *)(0xE000ED3C))));
    printf("SCB->SHCSR= %08x - System Handler Control and State R (exception)\n", (unsigned int)SCB->SHCSR);

    while(1);
}

#ifdef BUTTON_M
void EXTI0_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if(EXTI_GetFlagStatus(EXTI_Line0) != RESET)
    {
        nos_button_exe(BUTTON_WKUP);
        EXTI_ClearFlag(EXTI_Line0);
    }
    NOS_EXIT_ISR();
}

void EXTI1_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if(EXTI_GetFlagStatus(EXTI_Line1) != RESET)
    {
        nos_button_exe(BUTTON_USER);
        EXTI_ClearFlag(EXTI_Line1);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef CC2520_M
void EXTI9_5_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if(EXTI_GetITStatus(EXTI_Line8) != RESET)
    {
        EXTI_ClearFlag(EXTI_Line8);
        cc2520_isr(ID_MANGO_Z1_CC2520);
    }
    NOS_EXIT_ISR();
}
#endif

void RTCAlarm_IRQHandler(void)
{
#ifdef KERNEL_M
    static UINT32 next_cnt = TICKS_SCHED;
    static BOOL expire_next = FALSE;
    static BOOL expire_after_ovf = FALSE;
    UINT32 current_cnt;
#endif
    static UINT32 next_sec = TICKS_SEC;
    
    if (RTC_GetITStatus(RTC_IT_ALR) != RESET)
    {
        EXTI_ClearITPendingBit(EXTI_Line17);

        if (PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
        {
            PWR_ClearFlag(PWR_FLAG_WU);
        }
        RTC_WaitForLastTask();
        
        RTC_ClearITPendingBit(RTC_IT_ALR);
        RTC_WaitForLastTask();

#ifdef KERNEL_M
        current_cnt = next_cnt;
        next_cnt = RTC_GetCounter() + TICKS_SCHED;
        RTC_SetAlarm(next_cnt);
        RTC_WaitForLastTask();
        
        nos_sched_handler();

        if (_sched_enable_bit)
        {
            NOS_CTX_SW_PENDING_SET();
        }

        if (expire_next)
        {
            nos_utc_increment();
            expire_next = FALSE;
            next_sec += 32768;
        }

        if (expire_after_ovf &&
            (next_cnt < current_cnt))
        {
            //Counter overflow will not be occured.
            expire_after_ovf = FALSE;
        }
        
        if (expire_after_ovf == FALSE && next_cnt >= next_sec)
        {
            expire_next = TRUE;
        }
        
#else
        next_sec += TICKS_SEC;
        RTC_SetAlarm(next_sec);
        RTC_WaitForLastTask();
        nos_utc_increment();
#endif
    }
}

#ifdef KERNEL_M
void SysTick_Handler(void)
{
    nos_sched_handler();
    
    if (_sched_enable_bit)
    {
        NOS_CTX_SW_PENDING_SET();
    }
}

void PendSV_Handler(void)
{
    NOS_CTX_SW_PENDING_CLEAR();
    NOS_DISABLE_GLOBAL_INTERRUPT();
    nos_ctx_sw_handler();	// may return here with global interrupt SET
    NOS_ENABLE_GLOBAL_INTERRUPT();
}
#endif
