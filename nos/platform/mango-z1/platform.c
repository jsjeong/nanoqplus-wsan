// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Mango-Z1 platform
 *
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 17.
 */

#include <stdio.h>
#include "platform.h"
#include "critical_section.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_rcc.h"

#ifdef UART_M
#include "uart.h"
#endif

#ifdef LED_M
#include "led.h"
#endif

#ifdef BUTTON_M
#include "button.h"
#endif

#if defined LIB_UTC_CLOCK_M || defined KERNEL_M
#include "rtc.h"
#endif

#ifdef CC2520_M
#include "cc2520-interface.h"
#include "cc2520.h"
#endif

#ifdef BH1600_M
#include "bh1600-interface.h"
#include "bh1600.h"
#endif

static void nos_isr_init(void)
{
    NVIC_InitTypeDef n;

#ifdef  VECT_TAB_RAM
    /* Set the Vector Table base location at 0x20000000 */ 
    NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0); 
#else  /* VECT_TAB_FLASH  */
    /* Set the Vector Table base location at 0x08000000 */ 
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);   
#endif

    /*
     * Configure preemption priority groups. 0~3 for preemtion group. 0~3 for
     * priority. 0:highest, 3:lowest. Refer <misc.h>.
     */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

#if defined LIB_UTC_CLOCK_M || defined KERNEL_M
    n.NVIC_IRQChannel = RTCAlarm_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 0;
    n.NVIC_IRQChannelSubPriority = 0;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);	
#endif

#ifdef STM32F10X_USART1_FOR_ASYNC
    n.NVIC_IRQChannel = USART1_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 1;
    n.NVIC_IRQChannelSubPriority = 0;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);
#endif

#ifdef BUTTON_M
    // Button WKUP
    n.NVIC_IRQChannel = EXTI0_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 2;
    n.NVIC_IRQChannelSubPriority = 3;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);

    // Button USER
    n.NVIC_IRQChannel = EXTI1_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 2;
    n.NVIC_IRQChannelSubPriority = 3;
    n.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&n);
#endif

#ifdef CC2520_M
    n.NVIC_IRQChannel                   = EXTI9_5_IRQn;
    n.NVIC_IRQChannelPreemptionPriority = 2;
    n.NVIC_IRQChannelSubPriority        = 0;
    n.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init(&n);
#endif
}

void nos_platform_init(void)
{
    // Enable External Interrupt, Event control clocks
    RCC_APB2PeriphClockCmd(	RCC_APB2Periph_AFIO, ENABLE);

#ifdef STM32F10X_USART1_FOR_ASYNC
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1, ENABLE);
    nos_uart_init(STM32F10X_USART1);
#endif

    /* { */
    /*     RCC_ClocksTypeDef ck; */
    
    /*     RCC_GetClocksFreq(&ck); */
    /*     printf("SYSCLK:%u, HCLK:%u, PCLK1:%u, PCLK2:%u, ADCCLK:%u\n", */
    /*            ck.SYSCLK_Frequency, ck.HCLK_Frequency, */
    /*            ck.PCLK1_Frequency, ck.PCLK2_Frequency, */
    /*            ck.ADCCLK_Frequency); */
    /* } */
    
#ifdef LED_M	
    nos_led_init();
#endif
    
#ifdef BUTTON_M
    nos_button_init();
#endif

#ifdef CC2520_M
    cc2520_interface_init(ID_MANGO_Z1_CC2520);
    cc2520_init(ID_MANGO_Z1_CC2520, CONFIG_MANGO_Z1_CC2520_INIT_CH);
#endif

#ifdef BH1600_M
    bh1600_interface_init(ID_MANGO_Z1_BH1600);
    bh1600_init(ID_MANGO_Z1_BH1600);
#endif

#if defined LIB_UTC_CLOCK_M || defined KERNEL_M
    nos_rtc_init();
#endif
    
    nos_isr_init();
}
