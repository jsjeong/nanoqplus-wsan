// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2520 interface for Mango-Z1
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 15.
 */

#include "kconf.h"
#ifdef CC2520_M
#include "cc2520-interface.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_gpio.h"

#define RESET_PORT      GPIOC
#define RESET_PIN       GPIO_Pin_6
#define VREG_EN_PORT    GPIOC
#define VREG_EN_PIN     GPIO_Pin_7

#define GPIO0_PORT      GPIOC
#define GPIO0_PIN       GPIO_Pin_8
#define GPIO1_PORT      GPIOC
#define GPIO1_PIN       GPIO_Pin_9
#define GPIO2_PORT      GPIOC
#define GPIO2_PIN       GPIO_Pin_10
#define GPIO3_PORT      GPIOC
#define GPIO3_PIN       GPIO_Pin_11
#define GPIO4_PORT      GPIOC
#define GPIO4_PIN       GPIO_Pin_12
#define GPIO5_PORT      GPIOD
#define GPIO5_PIN       GPIO_Pin_2

#define SPI_COMMON_PORT GPIOA
#define SPI_CSN_PIN     GPIO_Pin_4
#define SPI_CLK_PIN     GPIO_Pin_5
#define SPI_MISO_PIN    GPIO_Pin_6
#define SPI_MOSI_PIN    GPIO_Pin_7

#define CC2520_RESET           RESET_PORT, RESET_PIN
#define CC2520_VREG_EN         VREG_EN_PORT, VREG_EN_PIN
#define CC2520_GPIO0           GPIO0_PORT, GPIO0_PIN
#define CC2520_GPIO1           GPIO1_PORT, GPIO1_PIN
#define CC2520_GPIO2           GPIO2_PORT, GPIO2_PIN
#define CC2520_GPIO3           GPIO3_PORT, GPIO3_PIN
#define CC2520_GPIO4           GPIO4_PORT, GPIO4_PIN
#define CC2520_GPIO5           GPIO5_PORT, GPIO5_PIN
#define CC2520_SPI_CSN         SPI_COMMON_PORT, SPI_CSN_PIN
#define CC2520_SPI_MISO        SPI_COMMON_PORT, SPI_MISO_PIN

void cc2520_interface_init(UINT8 dev_id)
{
    GPIO_InitTypeDef g;
    SPI_InitTypeDef s;

    // Enable clocks.
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |
                           RCC_APB2Periph_GPIOC |
                           RCC_APB2Periph_GPIOD |
                           RCC_APB2Periph_SPI1,
                           ENABLE);
    
    //Initialize GPIOs.
    g.GPIO_Pin = (RESET_PIN | VREG_EN_PIN);
    g.GPIO_Speed = GPIO_Speed_50MHz;
    g.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(RESET_PORT, &g);
    cc2520_set_pin(dev_id, CC2520_PIN_RESET, TRUE);
    cc2520_set_pin(dev_id, CC2520_PIN_VREG_EN, TRUE);
    
    g.GPIO_Pin = (GPIO0_PIN | GPIO1_PIN | GPIO2_PIN | GPIO3_PIN | GPIO4_PIN);
    g.GPIO_Speed = GPIO_Speed_50MHz;
    g.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOC, &g);
    
    //Initialize SPI1.
    g.GPIO_Pin = (SPI_CLK_PIN | SPI_MISO_PIN | SPI_MOSI_PIN);
    g.GPIO_Speed = GPIO_Speed_50MHz;
    g.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &g);

    g.GPIO_Pin = SPI_CSN_PIN;
    g.GPIO_Speed = GPIO_Speed_50MHz;
    g.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_Init(SPI_COMMON_PORT, &g);

    s.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    s.SPI_Mode = SPI_Mode_Master;
    s.SPI_DataSize = SPI_DataSize_8b;
    s.SPI_CPOL = SPI_CPOL_Low;
    s.SPI_CPHA = SPI_CPHA_1Edge;
    s.SPI_NSS = SPI_NSS_Soft;
    s.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;   // 72MHz / 16 = 4.5MHz
    s.SPI_FirstBit = SPI_FirstBit_MSB;
    s.SPI_CRCPolynomial = 7;
    SPI_Init(SPI1, &s);
    SPI_Cmd(SPI1, ENABLE);

    // Interrupt should be enabled after CC2520 GPIO pins configuration.
    cc2520_rx_done_intr_flag_enable(dev_id, FALSE);
}

void cc2520_request_cs(UINT8 dev_id)
{
    GPIO_ResetBits(CC2520_SPI_CSN);
}

void cc2520_release_cs(UINT8 dev_id)
{
    GPIO_SetBits(CC2520_SPI_CSN);
}

UINT8 cc2520_spi(UINT8 dev_id, UINT8 txd)
{
    SPI_I2S_SendData(SPI1, txd);
    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
    return SPI_I2S_ReceiveData(SPI1);
}

BOOL cc2520_pin_is_set(UINT8 dev_id, enum cc2520_pin pin)
{
    if (pin == CC2520_PIN_GPIO0)
    {
        return GPIO_ReadInputDataBit(CC2520_GPIO0);
    }
    else if (pin == CC2520_PIN_GPIO1)
    {
        return GPIO_ReadInputDataBit(CC2520_GPIO1);
    }
    else if (pin == CC2520_PIN_GPIO2)
    {
        return GPIO_ReadInputDataBit(CC2520_GPIO2);
    }
    else if (pin == CC2520_PIN_RESET)
    {
        return GPIO_ReadInputDataBit(CC2520_RESET);
    }
    else if (pin == CC2520_PIN_VREG_EN)
    {
        return GPIO_ReadInputDataBit(CC2520_VREG_EN);
    }
    else if (pin == CC2520_PIN_SPI_MISO)
    {
        return GPIO_ReadInputDataBit(CC2520_SPI_MISO);
    }
    else
    {
        return FALSE;
    }
}

void cc2520_set_pin(UINT8 dev_id, enum cc2520_pin pin, BOOL set)
{
    if (pin == CC2520_PIN_GPIO0)
    {
        if (set) GPIO_SetBits(CC2520_GPIO0);
        else GPIO_ResetBits(CC2520_GPIO0);
    }
    else if (pin == CC2520_PIN_GPIO1)
    {
        if (set) GPIO_SetBits(CC2520_GPIO1);
        else GPIO_ResetBits(CC2520_GPIO1);
    }
    else if (pin == CC2520_PIN_GPIO2)
    {
        if (set) GPIO_SetBits(CC2520_GPIO2);
        else GPIO_ResetBits(CC2520_GPIO2);
    }
    else if (pin == CC2520_PIN_RESET)
    {
        if (set) GPIO_SetBits(CC2520_RESET);
        else GPIO_ResetBits(CC2520_RESET);
    }
    else if (pin == CC2520_PIN_VREG_EN)
    {
        if (set) GPIO_SetBits(CC2520_VREG_EN);
        else GPIO_ResetBits(CC2520_VREG_EN);
    }
}

BOOL cc2520_rx_done_pin_is_set(UINT8 dev_id)
{
    return cc2520_pin_is_set(dev_id, CC2520_PIN_GPIO0);
}

BOOL cc2520_rx_done_intr_flag_is_set(UINT8 dev_id)
{
    return (EXTI_GetITStatus(EXTI_Line8) != RESET);
}

void cc2520_rx_done_intr_flag_clear(UINT8 dev_id)
{
    EXTI_ClearFlag(EXTI_Line8);
}

void cc2520_rx_done_intr_flag_enable(UINT8 dev_id, BOOL enable)
{
    EXTI_InitTypeDef e;

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource8);
    e.EXTI_Line    = EXTI_Line8;
    e.EXTI_Mode    = EXTI_Mode_Interrupt;
    e.EXTI_Trigger = EXTI_Trigger_Rising;
    e.EXTI_LineCmd = (enable) ? ENABLE : DISABLE;
    EXTI_Init(&e);    
}

BOOL cc2520_tx_done_pin_is_set(UINT8 dev_id)
{
    return cc2520_pin_is_set(dev_id, CC2520_PIN_GPIO1);
}

BOOL cc2520_sampled_cca_pin_is_set(UINT8 dev_id)
{
    return cc2520_pin_is_set(dev_id, CC2520_PIN_GPIO2);
}

#endif //CC2520_M
