// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 12. 12.
 */

#include "platform.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include "critical_section.h"

#ifdef UART_M
#include "uart.h"
#include <stdio.h>
#endif

#ifdef LED_M
#include "led.h"
#endif

#ifdef AT86RF230_M
#include "rf230-interface.h"
#include "rf230.h"
#endif

#ifdef UART_M
static int uart_putchar(char c, FILE *stream);
static FILE platform_stdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);

static int uart_putchar(char c, FILE *stream)
{
    if (c == '\n')
        uart_putchar('\r', stream);
    nos_uart_putc(STDIO, c);
    return 0;
}
#endif //UART_M

void nos_platform_init()
{
#ifdef LED_M
    nos_led_init();
#endif

#ifdef UART_M
    nos_uart_init(STDIO);
    stdout = &platform_stdout;
#endif

#ifdef AT86RF230_M
    rf230_interface_init(ID_IRIS_RF230);
    rf230_init(ID_IRIS_RF230, RF230_CLKM_1MHZ); //1MHz: default CLKM frequency.
#endif
}

#ifdef AT86RF230_M
ISR(TIMER1_CAPT_vect)
{
    NOS_ENTER_ISR();
    rf230_interrupt_handler(ID_IRIS_RF230);
    NOS_EXIT_ISR();
}
#endif

