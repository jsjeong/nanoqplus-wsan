// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Implementation of Glossy interface for IRIS. Timer3 is used to delay before
 * forwarding Glossy packets.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 11.
 */

#include "kconf.h"

#ifdef GLOSSY_M
#include "glossy-interface.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "critical_section.h"

void glossy_start_delay_timer(UINT8 dev_id, UINT16 delay_ticks)
{
    TIFR3 |= (1 << OCF3A);
    
    OCR3AH = (delay_ticks >> 8);
    OCR3AL = (delay_ticks & 0xFF);
        
    TCCR3B |= (1 << CS00);
}

UINT16 glossy_get_ticks(UINT8 dev_id)
{
    UINT16 cnt;
    
    cnt = TCNT3L;
    cnt += (TCNT3H << 8);
    return cnt;
}

void glossy_stop_timer(UINT8 dev_id)
{
    TCCR3B &= ~((1 << CS02) | (1 << CS01) | (1 << CS00));
}

void glossy_reset_timer(UINT8 dev_id)
{
    // Clear an interrupt flag and counter.
    TIFR3 |= (1 << OCF3A);
    
    TCNT3H = 0;
    TCNT3L = 0;
}

void glossy_wait_until_delay_done(UINT8 dev_id)
{
    while ((TIFR3 & (1 << OCF3A)) == 0);
    TCCR3B &= ~((1 << CS02) | (1 << CS01) | (1 << CS00));
}

void glossy_init_delay_timer(UINT8 dev_id)
{
    // Clear Timer3's all interrupt flags.
    TIFR3 &= ~((1 << ICF3) |
               (1 << OCF3C) |
               (1 << OCF3B) |
               (1 << OCF3A) |
               (1 << TOV3));

    // Enable only a output compare A match interrupt. Disable others.
    TIMSK3 &= ~((1 << ICIE3) |
                (1 << OCIE3C) |
                (1 << OCIE3B) |
                (1 << TOIE3));
    TIMSK3 |= (1 << OCIE3A);
}

#endif //GLOSSY_M
