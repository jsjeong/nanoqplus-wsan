// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * AT86RF230 interface implementation
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 10.
 */

#include "kconf.h"

#ifdef AT86RF230_M

#include "rf230-interface.h"
#include "arch.h"
#include "intr.h"
#include <avr/io.h>
#include "platform.h"

#define SPI_CSN       0
#define SPI_CSN_PORT  PORTB
#define SPI_CSN_DDR   DDRB
#define SPI_CSN_PIN   PINB

#define SLP_TR        7
#define SLP_TR_PORT   PORTB
#define SLP_TR_DDR    DDRB
#define SLP_TR_PIN    PINB

#define RSTN          6
#define RSTN_PORT     PORTA
#define RSTN_DDR      DDRA
#define RSTN_PIN      PINA

#define IRQ           4
#define IRQ_PORT      PORTD
#define IRQ_DDR       DDRD
#define IRQ_PIN       PIND

/**
 * Implementation of rf212_interface_init() defined in rf212-interface.h.
 */
void rf230_interface_init(UINT8 dev_id)
{
    if (dev_id == ID_IRIS_RF230)
    {
        // SPI
        _BIT_SET(DDRB, 1); // Clock
        _BIT_SET(PORTB, 1); // pull up clock
        
        _BIT_SET(DDRB, 2); // MOSI
        
        _BIT_CLR(DDRB, 3); // MISO
        
        _BIT_SET(SPI_CSN_DDR, SPI_CSN);
        _BIT_SET(SPI_CSN_PORT, SPI_CSN); // pull up slave select
        
        SPCR = (1 << SPE) | (1 << MSTR);
        SPSR = (1 << SPI2X);

        // Pins
        _BIT_CLR(SPI_CSN_PORT, SPI_CSN);
        _BIT_SET(SLP_TR_DDR, SLP_TR);
        _BIT_SET(RSTN_DDR, RSTN);
        
        // Interrupt (ICP1)
        _BIT_CLR(IRQ_DDR, IRQ);
        _BIT_CLR(IRQ_PORT, IRQ);
        _BIT_SET(TCCR1B, ICES1);
        _BIT_SET(TIMSK1, ICIE1);
        
        rf230_clear_mcu_irq(dev_id);
        rf230_enable_mcu_irq(dev_id, TRUE);
    }
}

/**
 * Implementation of rf230_request_cs() defined in rf212-interface.h.
 */
ERROR_T rf230_request_cs(UINT8 dev_id)
{
    if (dev_id == ID_IRIS_RF230)
    {
        //nos_delay_us(1);
        _BIT_CLR(SPI_CSN_PORT, SPI_CSN);
        //rf230_set_pin(dev_id, RF230_PIN_SPI_CSN, FALSE);
        return ERROR_SUCCESS;
    }
    else
    {
        return ERROR_NOT_FOUND;
    }
}

/**
 * Implementation of rf230_release_cs() defined in rf212-interface.h.
 */
ERROR_T rf230_release_cs(UINT8 dev_id)
{
    if (dev_id == ID_IRIS_RF230)
    {
        _BIT_SET(SPI_CSN_PORT, SPI_CSN);
//        rf230_set_pin(dev_id, RF230_PIN_SPI_CSN, TRUE);
        return ERROR_SUCCESS;
    }
    else
    {
        return ERROR_NOT_FOUND;
    }
}

/**
 * Implementation of rf230_spi() defined in rf230-interface.h.
 */
ERROR_T rf230_spi(UINT8 dev_id, UINT8 txdata, UINT8 *rxdata)
{
    UINT8 r;

    if (dev_id == ID_IRIS_RF230)
    {
        SPDR = txdata;
        while (!(SPSR & (1 << SPIF)));
        r = SPDR;

        if (rxdata) *rxdata = r;
        return ERROR_SUCCESS;
    }
    else
    {
        return ERROR_NOT_FOUND;
    }
}

/**
 * Implementation of rf230_set_pin() defined in rf230-interface.h.
 */
ERROR_T rf230_set_pin(UINT8 dev_id, enum rf230_pin pin, BOOL state)
{
    if (pin == RF230_PIN_RESETN)
    {
        if (dev_id == ID_IRIS_RF230)
        {
            if (state == TRUE) _BIT_SET(RSTN_PORT, RSTN);
            else _BIT_CLR(RSTN_PORT, RSTN);
            return ERROR_SUCCESS;
        }
        else
        {
            return ERROR_NOT_FOUND;
        }
    }
    else if (pin == RF230_PIN_SLP_TR)
    {
        if (dev_id == ID_IRIS_RF230)
        {
            if (state == TRUE) _BIT_SET(SLP_TR_PORT, SLP_TR);
            else _BIT_CLR(SLP_TR_PORT, SLP_TR);
            return ERROR_SUCCESS;
        }
        else
        {
            return ERROR_NOT_FOUND;
        }
    }
    else if (pin == RF230_PIN_IRQ)
    {
        if (dev_id == ID_IRIS_RF230)
        {
            if (state == TRUE) _BIT_SET(IRQ_PORT, IRQ);
            else _BIT_CLR(IRQ_PORT, IRQ);
            return ERROR_SUCCESS;
        }
        else
        {
            return ERROR_NOT_FOUND;
        }
    }
    else if (pin == RF230_PIN_SPI_CSN)
    {
        if (dev_id == ID_IRIS_RF230)
        {
            if (state == TRUE) _BIT_SET(SPI_CSN_PORT, SPI_CSN);
            else _BIT_CLR(SPI_CSN_PORT, SPI_CSN);
            return ERROR_SUCCESS;
        }
        else
        {
            return ERROR_NOT_FOUND;
        }
    }
    else
    {
        return ERROR_INVALID_ARGS;
    }
}

/**
 * Implementation of rf230_get_pin() defined in rf212-interface.h.
 */
ERROR_T rf230_get_pin(UINT8 dev_id, enum rf230_pin pin, BOOL *state)
{
    if (state == NULL)
        return ERROR_INVALID_ARGS;

    if (pin == RF230_PIN_RESETN)
    {
        if (dev_id == ID_IRIS_RF230)
        {
            *state = (_IS_SET(RSTN_PIN, RSTN)) ? TRUE : FALSE;
            return ERROR_SUCCESS;
        }
        else
        {
            return ERROR_NOT_FOUND;
        }
    }
    else if (pin == RF230_PIN_SLP_TR)
    {
        if (dev_id == ID_IRIS_RF230)
        {
            *state = (_IS_SET(SLP_TR_PIN, SLP_TR)) ? TRUE : FALSE;
            return ERROR_SUCCESS;
        }
        else
        {
            return ERROR_NOT_FOUND;
        }
    }
    else if (pin == RF230_PIN_IRQ)
    {
        if (dev_id == ID_IRIS_RF230)
        {
            *state = (_IS_SET(IRQ_PIN, IRQ)) ? TRUE : FALSE;
            return ERROR_SUCCESS;
        }
        else
        {
            return ERROR_NOT_FOUND;
        }
    }
    else if (pin == RF230_PIN_SPI_CSN)
    {
        if (dev_id == ID_IRIS_RF230)
        {
            *state = (_IS_SET(SPI_CSN_PIN, SPI_CSN)) ? TRUE : FALSE;
            return ERROR_SUCCESS;
        }
        else
        {
            return ERROR_NOT_FOUND;
        }
    }
    else
    {
        return ERROR_INVALID_ARGS;
    }
}

/**
 * Implementation of rf230_enable_mcu_irq() defined in rf230-interface.h.
 */
ERROR_T rf230_enable_mcu_irq(UINT8 dev_id, BOOL enable)
{
    if (dev_id == ID_IRIS_RF230)
    {
        if (enable)
        {
            _BIT_SET(TIMSK1, ICIE1);
        }
        else
        {
            _BIT_CLR(TIMSK1, ICIE1);
        }
        return ERROR_SUCCESS;
    }
    else
    {
        return ERROR_NOT_FOUND;
    }
}

/**
 * Implementation of rf212_clear_mcu_irq() defined in rf212-interface.h.
 */
ERROR_T rf230_clear_mcu_irq(UINT8 dev_id)
{
    if (dev_id == ID_IRIS_RF230)
    {
        _BIT_SET(TIFR1, ICF1);
        return ERROR_SUCCESS;
    }
    else
    {
        return ERROR_NOT_FOUND;
    }
}

#endif //AT86RF230_M
