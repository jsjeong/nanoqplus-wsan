// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * eZ430-Chronos watch platform
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 30.
 */

#ifdef __MSPGCC__
#include <legacymsp430.h>
#else
#include <signal.h>
#endif

#include "platform.h"
#include "critical_section.h"
#include "arch.h"

#ifdef UART_M
#include "uart.h"
#endif

#ifdef CHRONOS_RADIO
#endif

#ifdef UART_M
int putchar(int c)
{
    if (c == '\n')
        nos_uart_putc(STDIO, '\r');
    nos_uart_putc(STDIO, c);
    return 0;
}
#endif //UART_M

void nos_platform_init()
{
    cc430_set_vcore(3);

    // Set global high power request enable
    PMMCTL0_H = 0xA5;
    PMMCTL0_L |= PMMHPMRE;
    PMMCTL0_H = 0x00;
    
    // Clock initialization
    
    // 32kHz ACLK.
    P5SEL |= (1 << 1) + (1 << 0); //P5.0 (XIN) and P5.1 (XOUT)
    UCSCTL6 &= ~XT1OFF;           // XT1 on.
    UCSCTL6 |= XCAP_3;            // Internal load cap

    UCSCTL3 = SELREF__XT1CLK;     // Select XT1 as FLL reference.
    UCSCTL4 = SELA__XT1CLK | SELS__DCOCLKDIV | SELM__DCOCLKDIV;

    // 12MHz CPU clock
    _BIS_SR(SCG0);                // Disable the FLL.
    UCSCTL0 = 0x0000;             // Set the lowest possible DCOx, MODx.
    UCSCTL1 = DCORSEL_5;          // Select suitable range.
    UCSCTL2 = FLLD_1 + 0x16E;     // Set DCO multiplier.
    _BIC_SR(SCG0);                // Enable the FLL.

    /*
     * Worst-case settling time for the DCO when the DCO range bits have been
     * changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
     * UG for optimization.
     * 32 x 32 x 12 MHz / 32,768 Hz = 375000 = MCLK cycles for DCO to settle
     */
    __delay_cycles(375000);

    /*
     * Loop until XT1 & DCO stabilizes, use do-while to insure that body is
     * executed at least once.
     */
    do
    {
        UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
        SFRIFG1 &= ~OFIFG;      // Clear fault flags
    }
    while ((SFRIFG1 & OFIFG));

    NOS_ENTER_CRITICAL_SECTION();
    // Get write-access to port mapping registers:
    PMAPPWD = 0x02D52;

    // Disable write-access to port mapping registers:
    PMAPPWD = 0;
    NOS_EXIT_CRITICAL_SECTION();
    
#ifdef CHRONOS_UART_A0
    nos_uart_init(CC430_UCA0);
#endif

}

#ifdef CHRONOS_RADIO
wakeup interrupt (PORT1_VECTOR) port1_isr(void)
{
    NOS_ENTER_ISR();
    NOS_EXIT_ISR();
}
#endif
