// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#ifndef PLATFORM_H
#define PLATFORM_H
#include <avr/io.h>
#include "kconf.h"
#include "uart.h"

#define _SYSTEM_CLOCK 7372800ul

enum
{
    STDIO = ATMEGA128_UART0,
};

/* ############################# RF Modem Devices ########################### */
#ifdef IEEE_802_15_4_DEV_M
enum
{
    ID_ZIGBEX_CC2420 = 0,
    WPAN_DEV_CNT = 1,
};
#endif //IEEE_802_15_4_DEV_M
/* ########################################################################## */

/* LEDs */
#define LED1_PORT                           A
#define LED1_PIN                            2 // PA.2 - Output : Red LED
#define LED2_PORT                           A
#define LED2_PIN                            1 // PA.1 - Output : Green LED
#define LED3_PORT                           A
#define LED3_PIN                            0 // PA.0 - Output : Yellow LED

/* SHT1x pins */
#define SHTXX_SCL_PORT                      D
#define SHTXX_SCL_PIN                       0
#define SHTXX_SDA_PORT                      D
#define SHTXX_SDA_PIN                       1

#ifdef ZIGBEX2_LUMINANCE_M
#define LIGHT_SENSOR_POWER_SW_PORT          B
#define LIGHT_SENSOR_POWER_SW_PIN           6 // for Zigbex2
#endif //~ZIGBEX2_LUMINANCE_M

#define PIR_SENSOR_POWER_SW_PORT            C
#define PIR_SENSOR_POWER_SW_PIN             4
#define PIR_SENSOR_ADC_CHANNEL_PORT         F
#define PIR_SENSOR_ADC_CHANNEL_PIN          3

#define PORT_DOOR_OPEN_CHECK_SENSOR         C
#define PIN_DOOR_OPEN_CHECK_SENSOR          4

#ifdef ZIGBEX1_LUMINANCE_M
#define LIGHT_SENSOR_POWER_SW_PORT          E
#define LIGHT_SENSOR_POWER_SW_PIN           4 // for Zigbex1.x
#endif //~ZIGBEX1_LUMINANCE_M

#define LIGHT_SENSOR_ADC_CHANNEL_PIN        0
#define ADC_PHOTO_CHANNEL                   1
#define ADC_DOOR_OPEN_CHECK_CHANNEL         3


void nos_platform_init(void);

#endif // ~PLATFORM_H

