// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2420 - OCX-Z interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 6. 16.
 */

#include "kconf.h"

#ifdef CC2420_M
#include "cc2420-interface.h"

#include <avr/io.h>
#include "arch.h"
#include "critical_section.h"
#include "wpan-dev.h"
#include "platform.h"

#define REG(port, type) type##port

#define PORT(port)     REG(port, PORT)
#define DDR(port)      REG(port, DDR)
#define PIN(port)      REG(port, PIN)

#define VREG_EN_PORT   B
#define VREG_EN_PIN    5 //PB5 - VREG_EN
#define FIFOP_PORT     D
#define FIFOP_PIN      0 //PD0(INT0) - FIFOP
#define FIFO_PORT      D
#define FIFO_PIN       1 //PD1 - FIFO
#define CCA_PORT       D
#define CCA_PIN        6 //PD6 - CCA
#define SFD_PORT       D
#define SFD_PIN        4 //PD4(ICP1) - SFD
#define RESETN_PORT    B
#define RESETN_PIN     4 //PB4 - RESET_N

#define MOSI_PORT      B
#define MOSI_PIN       2 //PB2(MOSI) - SPI Out
#define MISO_PORT      B
#define MISO_PIN       3 //PB3(MISO) - SPI In
#define CLK_PORT       B
#define CLK_PIN        1 //PB1(SCK) - SPI Clock
#define CSN_PORT       B
#define CSN_PIN        0 //PB0 - CS_N

extern WPAN_DEV wpan_dev[WPAN_DEV_CNT];

void cc2420_interface_init(UINT8 id)
{
    if (id == ID_OCX_Z_CC2420)
    {
        // Initialize GPIO outputs.
        _BIT_SET(DDR(VREG_EN_PORT), VREG_EN_PIN);
        _BIT_SET(DDR(RESETN_PORT), RESETN_PIN);

        // Initialize GPIO inputs.
        _BIT_CLR(DDR(FIFOP_PORT), FIFOP_PIN);
        _BIT_CLR(DDR(FIFO_PORT), FIFO_PIN);
        _BIT_CLR(DDR(CCA_PORT), CCA_PIN);
        _BIT_CLR(DDR(SFD_PORT), SFD_PIN);

        // Initialize SPI pins.
        _BIT_SET(DDR(MOSI_PORT), MOSI_PIN);
        _BIT_CLR(DDR(MISO_PORT), MISO_PIN);
        _BIT_SET(DDR(CLK_PORT), CLK_PIN);
        _BIT_SET(DDR(CSN_PORT), CSN_PIN);

        // Initialize SPI.
        _BIT_SET(PORT(CSN_PORT), CSN_PIN);

        SPCR = ((0 << SPIE) |
                (1 << SPE) |                  // Enable SPI
                (0 << DORD) |                 // MSB first
                (1 << MSTR) |                 // Master mode
                (0 << CPOL) |                 // Idle low
                (0 << CPHA) |                 // Sample at rising edge
                (0 << SPR1) | (0 << SPR0));   // SCK frequency: f_osc / 2
        SPSR = (1 << SPI2X);

        // Initialize FIFOP interrupt (rising edge)
        EICRA |= (1 << ISC01) + (1 << ISC00);
    }
}

ERROR_T cc2420_request_cs(UINT8 id)
{
    if (id == ID_OCX_Z_CC2420)
    {
        _BIT_CLR(PORT(CSN_PORT), CSN_PIN);
        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_release_cs(UINT8 id)
{ 
    if (id == ID_OCX_Z_CC2420)
    {
        _BIT_SET(PORT(CSN_PORT), CSN_PIN);
        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_spi(UINT8 id, UINT8 txdata, UINT8 *rxdata)
{
    UINT8 rxd;

    if (id == ID_OCX_Z_CC2420)
    {
        SPDR = txdata;
        while (!_IS_SET(SPSR, SPIF));
        rxd = SPDR;

        if (rxdata != NULL)
            *rxdata = rxd;

        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_on(UINT8 id)
{
    if (id == ID_OCX_Z_CC2420)
    {
        _BIT_SET(PORT(VREG_EN_PORT), VREG_EN_PIN);
        nos_delay_ms(2);

        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_off(UINT8 id)
{
    if (id == ID_OCX_Z_CC2420)
    {
        _BIT_CLR(PORT(VREG_EN_PORT), VREG_EN_PIN);
        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_reset(UINT8 id)
{
    if (id == ID_OCX_Z_CC2420)
    {
        _BIT_CLR(PORT(RESETN_PORT), RESETN_PIN);
        nos_delay_us(1);
        _BIT_SET(PORT(RESETN_PORT), RESETN_PIN);
        nos_delay_us(5);

        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_enable_fifop_intr(UINT8 id, BOOL enable)
{
    if (id == ID_OCX_Z_CC2420)
    {
        if (enable)
            _BIT_SET(EIMSK, INT0);
        else
            _BIT_CLR(EIMSK, INT0);

        return ERROR_SUCCESS;
    }
    
    return ERROR_NOT_FOUND;
}


ERROR_T cc2420_clear_fifop_intr(UINT8 id)
{
    if (id == ID_OCX_Z_CC2420)
    {
        _BIT_SET(EIFR, INTF0);
        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_check_fifop_intr(UINT8 id, BOOL *flag_is_set)
{
    if (id == ID_OCX_Z_CC2420)
    {
        if (flag_is_set == NULL)
            return ERROR_INVALID_ARGS;

        *flag_is_set = (_IS_SET(EIFR, INTF6)) ? TRUE : FALSE;
        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

ERROR_T cc2420_get_pin_state(UINT8 id, UINT8 pin, BOOL *pin_is_set)
{
    if (id == ID_OCX_Z_CC2420)
    {
        if (pin_is_set == NULL)
            return ERROR_INVALID_ARGS;

        if (pin == CC2420_FIFOP)
        {
            *pin_is_set = (_IS_SET(PIN(FIFOP_PORT), FIFOP_PIN)) ? TRUE : FALSE;
        }
        else if (pin == CC2420_FIFO)
        {
            *pin_is_set = (_IS_SET(PIN(FIFO_PORT), FIFO_PIN)) ? TRUE : FALSE;
        }
        else if (pin == CC2420_SFD)
        {
            *pin_is_set = (_IS_SET(PIN(SFD_PORT), SFD_PIN)) ? TRUE : FALSE;
        }
        else if (pin == CC2420_CCA)
        {
            *pin_is_set = (_IS_SET(PIN(CCA_PORT), CCA_PIN)) ? TRUE : FALSE;
        }
        else
        {
            return ERROR_INVALID_ARGS;
        }

        return ERROR_SUCCESS;
    }

    return ERROR_NOT_FOUND;
}

#endif //CC2420_M
