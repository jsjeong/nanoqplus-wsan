// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#ifndef PLATFORM_H
#define PLATFORM_H
#include <avr/io.h>
#include "kconf.h"
#include "uart.h"

#define _SYSTEM_CLOCK 8000000ul

enum
{
    STDIO = ATMEGA128_UART1,
};

/* ############################# RF Modem Devices ########################### */
#ifdef IEEE_802_15_4_DEV_M
enum
{
    ID_OCX_Z_CC2420 = 0,
    WPAN_DEV_CNT = 1,
};
#endif //IEEE_802_15_4_DEV_M
/* ########################################################################## */

/* Light sensor */
#define LIGHT_SENSOR_POWER_SW_PORT  E
#define LIGHT_SENSOR_POWER_SW_PIN   0 // PE.0 - Lumination Sensor Power Switch
#define LIGHT_SENSOR_ADC_CHANNEL_PORT   F
#define LIGHT_SENSOR_ADC_CHANNEL_PIN    0 //ADC0 - adc channel 0

/* Sensirion SHTxx Temperature & Humidity sensor */
#define SHTXX_SDA_PORT      D
#define SHTXX_SDA_PIN       2
#define SHTXX_SCL_PORT      D
#define SHTXX_SCL_PIN       3
#define SHTXX_POWER_SW_PORT E
#define SHTXX_POWER_SW_PIN  1 // PE.1 - SHT15 (temp, humidity) sensor power switch

// Do not use UART module to use SHT15 sensor
#define UART1_RXD_PIN 2 // RXD1 - Input:  UART1 RXD
#define UART1_TXD_PIN 3 // TXD1 - Output: UART1 TXD

/* Octacomm Interface Board */
#define INTERFACE_SW3   6 // INT6 - Interface board sw3
#define INTERFACE_SW2   7 // INT7 - Interface board sw2

void nos_platform_init(void);

#endif // ~PLATFORM_H

