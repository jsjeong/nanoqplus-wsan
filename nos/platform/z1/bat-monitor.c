// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Battery Monitor for Tmote-Sky
 * 
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 19.
 */

/*
 * $Id$
 * $LastChangedDate$
 */

#include "bat-monitor.h"

#ifdef BATTERY_MONITOR_M
#include "pwr_mgr.h"
#include "critical_section.h"
#ifdef COOJA_SIM_M
#include <stdio.h>

UINT16 bat_residual = 0xffff;

void nos_battery_decrement_level(UINT16 x)
{
    if (bat_residual > x)
    {
        bat_residual -= x;
        //printf("Decrementing %u, Remaining:%u/65535\n", x, bat_residual);
    }
    else
    {
        bat_residual = 0;
        printf("Out of power\n");
        while(1)
        {
            // Stop the operation.
            NOS_ENTER_CRITICAL_SECTION();
            nos_mcu_sleep();
            NOS_EXIT_CRITICAL_SECTION();
        }
    }
}
#endif //COOJA_SIM_M

UINT16 nos_battery_read_level(void)
{
#ifdef COOJA_SIM_M
    return bat_residual;
#else //~COOJA_SIM_M
    return 0;
#endif //~COOJA_SIM_M
}

#endif //BATTERY_MONITOR_M
