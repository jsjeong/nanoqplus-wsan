// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Z1-dependent LED implementation
 *
 * @date 2013. 1. 30.
 * @author Jongsoo Jeong (ETRI)
 */

#include "led.h"

#ifdef LED_M
#include "platform.h"

#ifdef __ICC430__
#include <io430.h>
#endif

#define LED1_PORT 5
#define LED1_PIN  4
#define LED2_PORT 5
#define LED2_PIN  5
#define LED3_PORT 5
#define LED3_PIN  6

void nos_led_init(void)
{
    P5SEL &= ~((1 << LED1_PIN) | (1 << LED2_PIN) | (1 << LED3_PIN));
    P5OUT |= ((1 << LED1_PIN) | (1 << LED2_PIN) | (1 << LED3_PIN));
    
    nos_led_off(LED1);
    nos_led_off(LED2);
    nos_led_off(LED3);
}

void nos_led_on(UINT8 n)
{
    if (n == LED1)
        P5OUT &= ~(1 << LED1_PIN);
    else if (n == LED2)
        P5OUT &= ~(1 << LED2_PIN);
    else if (n == LED3)
        P5OUT &= ~(1 << LED3_PIN);
}

void nos_led_off(UINT8 n)
{
    if (n == LED1)
        P5OUT |= (1 << LED1_PIN);
    else if (n == LED2)
        P5OUT |= (1 << LED2_PIN);
    else if (n == LED3)
        P5OUT |= (1 << LED3_PIN);
}

void nos_led_toggle(UINT8 n)
{
    if (n == LED1)
        P5OUT ^= (1 << LED1_PIN);
    else if (n == LED2)
        P5OUT ^= (1 << LED2_PIN);
    else if (n == LED3)
        P5OUT ^= (1 << LED3_PIN);
}

#endif //LED_M
