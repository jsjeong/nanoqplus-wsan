/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Implementation of the KEP relay & ADC for power meter.
 * @author Jeehoon Lee (UST-ETRI)
 * @date 2012. 1. 13.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "kep.h"

#ifdef KEP_M
#include "platform.h"
#include "gpio.h"
#include "adc.h"

#ifdef RELAY_M
void kep_relay_init()
{
    NOS_GPIO_INIT_OUT(KEP_POWER_RELAY_PORT, KEP_POWER_RELAY_PIN);
    kep_relay_on(0);
}

void kep_relay_on(UINT8 id)
{
    NOS_GPIO_ON(KEP_POWER_RELAY_PORT, KEP_POWER_RELAY_PIN);
}

void kep_relay_off(UINT8 id)
{
    NOS_GPIO_OFF(KEP_POWER_RELAY_PORT, KEP_POWER_RELAY_PIN);
}

void kep_relay_toggle(UINT8 id)
{
    NOS_GPIO_TOGGLE(KEP_POWER_RELAY_PORT, KEP_POWER_RELAY_PIN);
}

BOOL kep_relay_get_state(UINT8 id)
{
    return NOS_GPIO_READ(KEP_POWER_RELAY_PORT, KEP_POWER_RELAY_PIN);
}
#endif /* RELAY_M */

#ifdef POWERMETER_M
void kep_powermeter_init()
{
    NOS_GPIO_INIT_PERIPHERAL(KEP_POWERMETER_ADC_PORT, KEP_POWERMETER_ADC_PIN);
}

UINT16 kep_powermeter_get_state()
{
    UINT16 data=0x0;
    data = nos_adc_convert(KEP_POWERMETER_ADC_PIN);
    data = 0xfff - data;    //revert data
    data = ((UINT32)data * 10000)/4095; // Unit: mA
    return data;
}
#endif /* POWERMETER_M */

#endif /* KEP_M */

