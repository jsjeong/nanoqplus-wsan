// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Power Manager for CC430
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 30.
 */

#include "pwr_mgr.h"

static MSP430_POWER_MODE current_pm;

void nos_mcu_set_power_mode(MSP430_POWER_MODE mode)
{
    if (mode == MSP430_POWER_MODE_ACTIVE)
    {
        // Called at the ISRs.
    }
    else if (mode == MSP430_POWER_MODE_LPM0)
    {
        LPM0;
    }
    current_pm = mode;
}

MSP430_POWER_MODE nos_mcu_get_current_power_mode(void)
{
    return current_pm;
}

void nos_mcu_sleep(void)
{
#ifdef MCU_AUTO_SLEEP_M
    nos_set_power_mode(MSP430_POWER_MODE_LPM0);
#endif
}

void cc430_set_vcore(UINT8 level)
{
    UINT8 current_level;

    do
    {
        // 0x03: (1 << PMMCOREV1) + (1 << PMMCOREV0)
        current_level = PMMCTL0_L & 0x03;

        PMMCTL0_H = 0xA5; //Unlock
        
        if (current_level < level)
        {
            // Increment Vcore step by step.
            current_level++;

            // Set SVS/M high side to new level.
            SVSMHCTL = (SVSHE + SVSHRVL0 * current_level +
                        SVMHE + SVSMHRRL0 * current_level);

            // Set SVM new Level.
            SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * current_level;                    
            while ((PMMIFG & SVSMLDLYIFG) == 0);   // Wait till SVM is settled.

            // Set VCore to current_level.
            PMMCTL0_L = PMMCOREV0 * current_level;

            // Clear already set flags.
            PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);

            // Wait till level is reached.
            if ((PMMIFG & SVMLIFG))
                while ((PMMIFG & SVMLVLRIFG) == 0);

            // Set SVS/M Low side to new level.
            SVSMLCTL = (SVSLE + SVSLRVL0 * level +
                        SVMLE + SVSMLRRL0 * level);
        }
        else if (current_level > level)
        {
            // Decrement Vcore step by step.
            current_level--;

            // Set SVS/M Low side to new level.
            SVSMLCTL = (SVSLE + SVSLRVL0 * current_level +
                        SVMLE + SVSMLRRL0 * current_level);
            while ((PMMIFG & SVSMLDLYIFG) == 0);   // Wait till SVM is settled.

            // Set VCore to 1.85 V for Max speed.
            PMMCTL0_L = (level * PMMCOREV0);
        }
        
        PMMCTL0_H = 0x00; //Lock
    }
    while (current_level != level);
}
