// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * MSP430F5438 Interrupts
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 29.
 */

#ifndef INTR_H
#define INTR_H

#ifdef __MSPGCC__
#include <legacymsp430.h>
#elif defined __ICC430__
#include "io430.h"
#endif

#include "arch.h"

#include "nos_common.h"
#include "arch.h"

#define ENABLE_TIMERA_OVF_vect()        do { TACTL |= TAIE; } while (0)
#define DISABLE_TIMERA_OVF_vect()       do { TACTL &= ~TAIE; } while (0)
#define CLEAR_TIMERA_OVF_vect()         do { TACTL &= ~TAIFG; } while (0)

#define ENABLE_TIMERA_CCR0_vect()       do { TACCTL0 |= CCIE; } while (0)
#define DISABLE_TIMERA_CCR0_vect()      do { TACCTL0 &= ~CCIE; } while (0)
#define CLEAR_TIMERA_CCR0_vect()        do { TACCTL0 &= ~CCIFG; } while (0)
#define SET_TIMERA_CCR0_vect()          do { TACCTL0 |= CCIFG; } while (0)

#define ENABLE_TIMERA_CCR1_vect()       do { TACCTL1 |= CCIE; } while (0)
#define DISABLE_TIMERA_CCR1_vect()      do { TACCTL1 &= ~CCIE; } while (0)
#define CLEAR_TIMERA_CCR1_vect()        do { TACCTL1 &= ~CCIFG; } while (0)

#define ENABLE_TIMERA_CCR2_vect()       do { TACCTL2 |= CCIE; } while (0)
#define DISABLE_TIMERA_CCR2_vect()      do { TACCTL2 &= ~CCIE; } while (0)
#define CLEAR_TIMERA_CCR2_vect()        do { TACCTL2 &= ~CCIFG; } while (0)
#define TIMERA_CCR2_vect_IS_SET()       (TACCTL2 & CCIFG)

#define ENABLE_TIMERB_OVF_vect()        do { TBCTL |= TBIE; } while (0)
#define DISABLE_TIMERB_OVF_vect()       do { TBCTL &= ~TBIE; } while (0)
#define CLEAR_TIMERB_OVF_vect()         do { TBCTL &= ~TBIFG; } while (0)
#define TIMERB_OVF_vect_IS_SET()        (TBCTL & TBIFG)

#define ENABLE_TIMERB_CCR0_vect()       do { TBCCTL0 |= CCIE; } while (0)
#define DISABLE_TIMERB_CCR0_vect()      do { TBCCTL0 &= ~CCIE; } while (0)
#define CLEAR_TIMERB_CCR0_vect()        do { TBCCTL0 &= ~CCIFG; } while (0)
#define TIMERB_CCR0_vect_IS_SET()       (TBCCTL0 & CCIFG)

#define ENABLE_TIMERB_CCR2_vect()       do { TBCCTL2 |= CCIE; } while (0)
#define DISABLE_TIMERB_CCR2_vect()      do { TBCCTL2 &= ~CCIE; } while (0)
#define CLEAR_TIMERB_CCR2_vect()        do { TBCCTL2 &= ~CCIFG; } while (0)
#define TIMERB_CCR2_vect_IS_SET()       (TBCCTL2 & CCIFG)

#define ENABLE_USART0_RX_vect()         do { IE1 |= URXIE0; } while (0)
#define DISABLE_USART0_RX_vect()        do { IE1 &= ~URXIE0; } while (0)
#define USART0_RX_vect_IS_SET()         (IE1 & URXIE0)
#define ENABLE_USART0_TX_vect()         do { IE1 |= UTXIE0; } while (0)
#define DISABLE_USART0_TX_vect()        do { IE1 &= ~UTXIE0; } while (0)

#define ENABLE_USART1_RX_vect()         do { IE2 |= URXIE1; } while (0)
#define DISABLE_USART1_RX_vect()        do { IE2 &= ~URXIE1; } while (0)
#define USART1_RX_vect_IS_SET()         (IE2 & URXIE1)
#define ENABLE_USART1_TX_vect()         do { IE2 |= UTXIE1; } while (0)
#define DISABLE_USART1_TX_vect()        do { IE2 &= ~UTXIE1; } while (0)

#endif	// MSP430_INTR_H
