// -*- c-basic-offset:4; c-file-style:"bsd"; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2014
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart-common.h
 * @author Jongsoo Jeong (ETRI)
 * @date 2014. 3. 12.
 * @brief Common UART functions
 */

#ifndef UART_COMMON_H
#define UART_COMMON_H

#include "nos_common.h"

/**
 * @brief Initialize UART ports.
 *
 * It is called by nos_platform_init().
 *
 * @param[in] port_num  Port number to be initialized
 */
void nos_uart_init(UINT8 port_num);

/**
 * @brief Send a character to an UART port.
 *
 * @param[in] port_num  Port number to send to
 * @param[in] byte      A character to be sent
 */
void nos_uart_putc(UINT8 port_num, char byte);

/**
 * @brief Send a string to an UART port.
 *
 * @param[in] port_num  Port number to send to
 * @param[in] str       A string to be sent
 */
void nos_uart_puts(UINT8 port_num, const char *str);

/**
 * @brief Receive a string input from users.
 *
 * @param[in] port_num  Port number to receive a string from
 * @param[out] str      Input string
 * @param[in] str_size  Maximum size of @p str
 */
void nos_uart_gets(UINT8 port_num, char *str, UINT8 str_size);

/**
 * @brief Set a callback function to be called when a character is received.
 *
 * @param[in] port_num  Port number to receive characters
 * @param[in] func      Callback function pointer
 */
void nos_uart_set_getc_callback(UINT8 port_num, void (*func)(UINT8, char));

/**
 * @brief Get a callback function pointer already set.
 *
 * @param[in] port_num  Port number where callback function is set
 * @return A callback function pointer
 */
void (*nos_uart_get_getc_callback(UINT8 port_num))(UINT8, char);

/**
 * @brief Enable an UART Rx interrupt.
 *
 * @param[in] port_num  Target port number
 */
void nos_uart_enable_rx_intr(UINT8 port_num);

/**
 * @brief Disable an UART Rx interrupt.
 *
 * @param[in] port_num  Target port number
 */
void nos_uart_disable_rx_intr(UINT8 port_num);

/**
 * @brief Check whether an UART Rx interrupt is enabled or not.
 *
 * @param[in] port_num  Target port number
 * @return TRUE when the interrupt is enabled. Otherwise, FALSE.
 */
BOOL nos_uart_rx_intr_is_set(UINT8 port_num);

#endif //UART_COMMON_H
