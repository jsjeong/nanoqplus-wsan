// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#include "uart.h"

#ifndef STM32W108CB

#ifdef UART_INPUT_M
#include "platform.h"
#include "arch.h"
#include "critical_section.h"

static volatile UINT8 char_cnt;
static volatile char *rx_str;

#ifdef THREAD_M
static UINT8 running = FALSE;
#endif

static UINT8 max_str_size;
extern volatile char uart_rx_char;

#ifdef KERNEL_M
extern UINT8 nos_thread_sleep(UINT16 ticks);
#endif

static void nos_uart_rx_bottom_half(UINT8 port, char rx_char)
{
    if (rx_char == _CR) // carriage return (ENTER Key)
    {
        rx_str[char_cnt] = '\0';
        nos_uart_putc(port, '\n');
        nos_uart_putc(port, _CR);
        nos_uart_disable_rx_intr(port);
    }
    else if (rx_char == _BS) // back space
    {
        if ( char_cnt > 0)
        {
            nos_uart_putc(port, _BS);
            nos_uart_putc(port, ' ');
            nos_uart_putc(port, _BS);
            char_cnt--;
        }
    }
    else
    {
        // The last allocated memory is reserved for termination character '\0'.
        if ( char_cnt < max_str_size-1 )
        {
            rx_str[char_cnt++] = rx_char;
            nos_uart_putc(port, rx_char);				
        }
        else
        {
            rx_str[char_cnt-1] = rx_char;
            nos_uart_putc(port, _BS);
            nos_uart_putc(port,rx_char);
        }
    }
}


void nos_uart_gets(UINT8 port_num, char *str, UINT8 str_size)
{
    // saving
    void (*temp_callback)(UINT8, char);
    volatile BOOL temp_uart_rx_intr;

    if (str_size < 2)
    {
        nos_uart_puts(port_num, "\n\rERROR : The string size must be bigger than '2'. One slot  is for termination character.\n\r");
        return;
    }

#ifdef THREAD_M
    // prohibit re-entrance
    while (running)
    {
        nos_thread_sleep(1);		//sleep 1 tick if other thread is using this function now.
    }
#endif

    NOS_ENTER_CRITICAL_SECTION();
#ifdef THREAD_M
    running = TRUE;
#endif
    char_cnt = 0;
    uart_rx_char = '\0';
    rx_str = (volatile char *)str;
    max_str_size = str_size;

    temp_callback = nos_uart_get_getc_callback(port_num);
    nos_uart_set_getc_callback(port_num, nos_uart_rx_bottom_half);

    if (nos_uart_rx_intr_is_set(port_num))
    {
        temp_uart_rx_intr = TRUE;
    }
    else
    {
        temp_uart_rx_intr = FALSE;
        nos_uart_enable_rx_intr(port_num);
    }

    NOS_EXIT_CRITICAL_SECTION();

    // receives string
    while (uart_rx_char != _CR);

    NOS_ENTER_CRITICAL_SECTION();
    // restoring

    nos_uart_set_getc_callback(port_num, temp_callback);
    if (temp_uart_rx_intr)
    {
        nos_uart_enable_rx_intr(port_num);
    }

#ifdef THREAD_M
    running = FALSE;
#endif
    NOS_EXIT_CRITICAL_SECTION();
}

#endif /* UART_INPUT_M */
#endif
