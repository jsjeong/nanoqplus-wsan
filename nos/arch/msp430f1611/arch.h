// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 * @author Haeyong Kim (ETRI)
 */

#ifndef ARCH_H
#define ARCH_H
#include "kconf.h"

#include "nos_common.h"

/* Endianess */
#define BYTE_ORDER_LITTLE_ENDIAN 1
#undef BYTE_ORDER_BIG_ENDIAN
#define BIT_ORDER_LITTLE_ENDIAN 1
#undef BIT_ORDER_BIG_ENDIAN

#define RAMEND 0x38FE

// USART0
#define SIMO0           1 // P3.1 - Output: SIMO (Slave Input Master Output 0)
#define SOMI0           2 // P3.2 - Input: SOMI (Slave Output Master Input 0)
#define UCLK0           3 // P3.3 - Output: SCK (UCLK0)
#define UTXD0           4 // P3.4 - Output: UART0 TXD
#define URXD0           5 // P3.5 - Input:  UART0 RXD

// USART1
#define SIMO1           1 // P5.1 - Output: SIMO (Slave Input Master Output 1)
#define SOMI1           2 // P5.2 - Input: SOMI (Slave Output Master Input 1)
#define UCLK1           3 // P5.3 - Output: SCK (UCLK1)
#define UTXD1           6 // P3.6 - Output: UART1 TXD
#define URXD1           7 // P3.7 - Input:  UART1 RXD

#define NOS_NOP() __asm__ volatile ("nop\n\t"::)
#define NOS_RESET()  do{ WDTCTL = 0x0000; }while(0)

void nos_arch_init(void);

#endif // ARCH_H
