// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Heap abstraction for MSP430F2617.
 *
 * @author Junkeun Song (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 30.
 *
 * @note This code is imported from msp430-libc-20120716. It overrides stdlib's
 *       malloc() and free().
 */

#include "heap.h"
#include "critical_section.h"
#include <stdlib.h>
#include <sys/crtld.h>
#include "arch.h"

#ifdef KERNEL_M
#include "hal_thread.h"
#endif

#define XSIZE(x)      ((*x) >> 1)
#define FREE_P(x)     (!((*x) & 1))
#define MARK_BUSY(x)  ((*x) |= 1)
#define MARK_FREE(x)  ((*x) &= 0xfffe) 

void *malloc (UINT16 size) 
{
    static char once = 0;
    UINT16 *heap_bottom;
    UINT16 kk = (UINT16) __noinit_end;        /* this will possibly introduce */
    UINT16 *heap_top = (UINT16 *)((kk+1)&~1); /* 1 byte hole between .bss and heap */
    INT8 f = 0;

    NOS_ENTER_CRITICAL_SECTION();

    if (!once) 
    {
        once = 1; 
        *heap_top = 0xfffe; 
    }
#ifdef KERNEL_M
    heap_bottom = (UINT16 *) (RAMEND - SYSTEM_STACK_SIZE);
#else
    heap_bottom = __read_stack_pointer();
    heap_bottom -= 20;
#endif
    size = (size+1) >> 1; /* round to 2 */

    do
    {
        UINT16 xsize = XSIZE (heap_top);
        UINT16 *heap_next = &heap_top[xsize + 1]; 
        if ((xsize<<1)+2 == 0) 
        { 
            f = 1; 
        }

        if (FREE_P (heap_top)) 
        {
            if (f) 
            { 
                xsize = heap_bottom - heap_top - 1; 
            } 
            else if (FREE_P(heap_next)) 
            { 
                *heap_top = ( (XSIZE(heap_next)<<1) + 2 == 0
                              ? 0xfffe
                              : (xsize + XSIZE(heap_next) + 1)<<1);
                continue; 
            } 

            if (xsize >= size) 
            {
                if (heap_top + size + 1 > heap_bottom)
                    break;
                if (f) 
                    heap_top[size + 1] = 0xfffe; 
                else if (xsize != size) 
                    heap_top[size + 1] = (xsize - size - 1) << 1; 
                *heap_top = size << 1; 
                MARK_BUSY (heap_top); 
                NOS_EXIT_CRITICAL_SECTION();
                return heap_top+1;
            } 
        }
        heap_top += xsize + 1;
    } 
    while (!f);
    NOS_EXIT_CRITICAL_SECTION();
    return NULL; 
} 

void free(void *p)
{
    UINT16 *t;
    
    NOS_ENTER_CRITICAL_SECTION();
    t = (UINT16 *)p - 1;
    MARK_FREE (t);
    NOS_EXIT_CRITICAL_SECTION();	
} 
