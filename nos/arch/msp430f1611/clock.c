// c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#ifdef __MSPGCC__
#include <msp430.h>
#elif defined __ICC430__
#include "io430.h"
#endif

#include "clock.h"
#include "critical_section.h"

//--- The clock speed unit is kHz. (1k = 1024) ---//
// Endurable the minimum of DCO clock for UART communication.
#define DCOCLK_MIN				3968
// Endurable the maxiimum of DCO clock for UART communication.
#define DCOCLK_MAX				4224

// TimerB ticks for calibration
#define TBCLK_CALIB_INTERVAL	2

void nos_set_dco_calib(UINT16 dco_calib);



// Set MCLK, SMCLK, ACLK from LFXT1 osc and DCO osc
// MCLK = 4194304Hz (4096kHz), SMCLK = 2097152Hz (2048kHz), ACLK = 32768Hz (32kHz)
// Acutally, DCO is slightly faster than 4096kHz
void nos_clock_init()
{
    // Adjust DCO clock to 4096kHz by the precise calibration.
    nos_adj_dco_clock();

    //XT2OFF[7] -XT2 oscillator. 0:on, 1:off
    //XTS[6] - LFXT1 mode select. 0:LF mode, 1:HF mode
    //DIVA[5-4] - Divider for ACLK. 00:/1, 01:/2, 10:/4, 11:/8 
    //RSEL[2-0] - Resistor select for DCO. 8 different steps. 7 gives the highest frequency to DCO. Do not change!!
    BCSCTL1 |= XT2OFF;	// XT2 off
    BCSCTL1 &= ~XTS;	// LF mode for LFXT1, ACLK : 32kHz

    // SELM[7-6] - Select MCLK. 00 or 01:DCOCLK, 10:XT2CLK, 11:LFXT1CLK
    // DIVM[5-4] - Divider for MCLK. 00:/1, 01:/2, 10:/4, 11:/8 
    // SELS[3] - Select SMCLK. 0:DCOCLK, 1:XT2CLK
    // DIVS[2-1] - Divider for SMCLK. 00:/1, 01:/2, 10:/4, 11:/8 
    // DCOR[0] - DCO resistor select. 0:internal, 1:external. Do not change!
    BCSCTL2 = DIVS0; // MCLK : 4096kHz, SMCLK : 2048kHz, default:0x00

    // Oscillator fault interrupt disable
    IE1 &= ~OFIE;
}	

//Adjusts DCO clock into "DCOCLK=4,194,304Hz". DCO clock needs the precise calibration because it uses RC oscillator
void nos_adj_dco_clock()
{
    UINT16 reg_tactl, reg_tbctl, reg_bcsctl1, reg_bcsctl2, reg_tbccr0;
    UINT16 dco_calib, step, smclk_start=0, smclk_end=0;
    UINT8 loop=3;

    NOS_ENTER_CRITICAL_SECTION();
    // Back up registers 
    reg_tactl = TACTL;
    reg_tbctl = TBCTL;
    reg_bcsctl1 = BCSCTL1;
    reg_bcsctl2 = BCSCTL2;
    reg_tbccr0 = TBCCR0;

    //XT2OFF[7] -XT2 oscillator. 0:on, 1:off
    //XTS[6] - LFXT1 mode select. 0:LF mode, 1:HF mode
    //DIVA[5-4] - Divider for ACLK. 00:/1, 01:/2, 10:/4, 11:/8 
    //RSEL[2-0] - Resistor select for DCO. 8 different steps. 7 gives the highest frequency to DCO.
    BCSCTL1 &= ~XTS;	//LF mode select.. default : 0x84

    // SELM[7-6] - Select MCLK. 00 or 01:DCOCLK, 10:XT2CLK, 11:LFXT1CLK
    // DIVM[5-4] - Divider for MCLK. 00:/1, 01:/2, 10:/4, 11:/8 
    // SELS[3] - Select SMCLK. 0:DCOCLK, 1:XT2CLK
    // DIVS[2-1] - Divider for SMCLK. 00:/1, 01:/2, 10:/4, 11:/8 
    // DCOR[0] - DCO resistor select. 0:internal, 1:external
    BCSCTL2 = 0x00; // default:0x00

    // TASSEL[9-8] - Timer A source select. 01:ACLK, 10:SMCLK
    // ID[7-6] - Input divider. 00:/1, 01:/2, 10:/4, 11/8
    // MC[5-4] - Mode control. 00:stop, 01:up, 10:continuous, 11:up/down
    // TACLR[2] - Timer A clear. Setting this bit resets TAR, the clock divider, the count direction (up/down mode)
    // TAIE[1] - Timer A interrupt enable
    // TAIFG[0] - Timer A interrupt flag
    // Timer will be initialized again at timer.c
    TACTL = TASSEL1 | MC1;	// SMCLK, continuous mode.  default : 0x0000
    TBCTL = TBSSEL0 | MC1;	// ACLK, conitnuous mode.  default : 0x0000



    //DCO calibration (SMCLK/1 = Timer A) using LFXT1 oscillator(ACLK/1 = Timer B).
    // RSEL=0, DCO=0, MOD=0
    // 1~3rd step : RSEL (+4) (+2) (+1) --> Selects RSEL value
    // 4~6th step : DCO (+4) (+2) (+1) --> Selects DCO value
    // 7~11th step : MOD (+16) (+8) (+4) (+2) (+1) --> Selects MOD value
    for (dco_calib=0, step=0x0800; step != 0; step >>= 1 )
    {
        // Starts test from RSEL0, DCO0, MOD0 to RSEL7, DCO7, MOD0
        nos_set_dco_calib(dco_calib | step);

        // When the timer clock is asynchronous to the CPU clock,
        // any read from TAR should occur while the timer is not operationg or the results may be unpredictable.
        // Alternatively, the timer may be read multiple times while operating,
        // and a majority vote taken in software to determine the correct reading.
        // Any write to TAR will take effect immediately.
        while (--loop)
        {
            // Counts TimerA ticks during TimerB calibration interval
            smclk_start = smclk_end;
            TBCCR0 = TBR + TBCLK_CALIB_INTERVAL;	// Sets next Timer B interrupt
            // CCIFG[0] - Capture/compare interrupt flag
            TBCCTL0 &= ~CCIFG;	// clear pending interrupt
            while ( !(TBCCTL0 & CCIFG) );	// Wait next Timer B interrupt pending
            smclk_end = TAR;
        }
        // apply the step value if current DCO clock is lower than DCOCLK(4096kHz)
        // Here, TACLK/TBCLK = SMCLK/ACLK = DCOosc/LFXT1osc =4096kHz/32kHz =128		
        if (smclk_end -smclk_start <= 128*TBCLK_CALIB_INTERVAL)
            dco_calib |= step;
    }
    // If DCO is 7, sets MOD  to 0 because it is useless.
    if ((dco_calib & 0x00e0) == 0x00e0)
        dco_calib &= ~0x001f;	// sets MOD to 0
    nos_set_dco_calib(dco_calib);


    // Restore registers
    TACTL = reg_tactl;
    TBCTL = reg_tbctl;
    BCSCTL1 |= reg_bcsctl1 & (~0x07);	// 0~2bit of BCSCTL1 is changed after calibration. 
    BCSCTL2 = reg_bcsctl2;
    TBCCR0 = reg_tbccr0;

    // clear pending interrupt
    TBCCTL0 &= ~CCIFG;
    NOS_EXIT_CRITICAL_SECTION();
}


//Writes DCO clock into registers using UINT16 ( 00000aaabbbccccc -> aaa:RSEL bbb:DCO ccccc:MOD)
void nos_set_dco_calib(UINT16 dco_calib)
{
    // RSEL[2-0] select.
    BCSCTL1 = (BCSCTL1 & ~0x07) | ((dco_calib >> 8) & 0x07);
    //DCO[7-5] and MOD[4-0] select
    DCOCTL = dco_calib & 0xff;
}


/*
// This function adjust DCO clock to current MOD+1 or current MOD-1.
UINT16 dco_calib;
void nos_fast_adj_dco_clock()
{
NOS_ENTER_CRITICAL_SECTION();
UINT16 reg_tactl, reg_tbctl, reg_bcsctl1, reg_bcsctl2, reg_tbccr0;
UINT16 smclk_start=0, smclk_end=0;
UINT8 loop=3;

// Back up registers 
reg_tactl = TACTL;
reg_tbctl = TBCTL;
reg_bcsctl1 = BCSCTL1;
reg_bcsctl2 = BCSCTL2;
reg_tbccr0 = TBCCR0;

// TASSEL[9-8] - Timer A source select. 01:ACLK, 10:SMCLK
// ID[7-6] - Input divider. 00:/1, 01:/2, 10:/4, 11/8
// MC[5-4] - Mode control. 00:stop, 01:up, 10:continuous, 11:up/down
// TACLR[2] - Timer A clear. Setting this bit resets TAR, the clock divider, the count direction (up/down mode)
// TAIE[1] - Timer A interrupt enable
// TAIFG[0] - Timer A interrupt flag
TACTL = TASSEL1 | MC1;	// SMCLK, continuous mode.  default : 0x0000
TBCTL = TBSSEL0 | MC1;	// ACLK, conitnuous mode.  default : 0x0000

//XT2OFF[7] -XT2 oscillator. 0:on, 1:off
//XTS[6] - LFXT1 mode select. 0:LF mode, 1:HF mode
//DIVA[5-4] - Divider for ACLK. 00:/1, 01:/2, 10:/4, 11:/8 
//RSEL[2-0] - Resistor select for DCO. 8 different steps. 7 gives the highest frequency to DCO.
BCSCTL1 &= ~XTS;	//LF mode select.. default : 0x84

// SELM[7-6] - Select MCLK. 00 or 01:DCOCLK, 10:XT2CLK, 11:LFXT1CLK
// DIVM[5-4] - Divider for MCLK. 00:/1, 01:/2, 10:/4, 11:/8 
// SELS[3] - Select SMCLK. 0:DCOCLK, 1:XT2CLK
// DIVS[2-1] - Divider for SMCLK. 00:/1, 01:/2, 10:/4, 11:/8 
// DCOR[0] - DCO resistor select. 0:internal, 1:external
BCSCTL2 = 0x00; // default:0x00

while (--loop)
{
smclk_start = smclk_end;
TBCCR0 = TBR + TBCLK_CALIB_INTERVAL;	// Sets next Timer B interrupt
// CCIFG[0] - Capture/compare interrupt flag
TBCCTL0 &= ~CCIFG;	// clear pending interrupt
while ( !(TBCCTL0 & CCIFG) );	// Wait next Timer B interrupt pending
smclk_end = TAR;
}

// Here, TACLK/TBCLK = SMCLK/ACLK = DCOosc/LFXT1osc =4096kHz/32kHz =128
// If DCOosc is lower than 3968kHz, speeds it up.
if (smclk_end -smclk_start <= 124*TBCLK_CALIB_INTERVAL)	// Here, 124 = DCOCLK_MIN(3968kHz)/TBCLK(32kHz)
{
nos_set_dco_calib(++dco_calib);
}
// If DCOosc is higher than 4224kHz, slows it down.
else if  (smclk_end -smclk_start >= 132*TBCLK_CALIB_INTERVAL)	// Here, 132 = DCOCLK_MAX(4224kHz)/TBCLK(32kHz)
{
nos_set_dco_calib(--dco_calib);
}

// Restore registers
TACTL = reg_tactl;
TBCTL = reg_tbctl;
BCSCTL1 |= reg_bcsctl1 & (~0x07);	// 0~2bit of BCSCTL1 is changed after calibration. 
BCSCTL2 = reg_bcsctl2;
TBCCR0 = reg_tbccr0;

// clear pending interrupt
TBCCTL0 &= ~CCIFG;
NOS_EXIT_CRITICAL_SECTION();
}
*/

