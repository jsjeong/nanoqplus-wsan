// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 */

#include "hal_thread.h"

#ifdef THREAD_M
#include "arch.h"
#include "sched.h"
#include "thread.h"

UINT8 kernel_thread_stack[SYSTEM_STACK_SIZE];

// initialize stack
// This is necessary because a context should be placed in the stack when context switching.
// After context switching call, the thread with this stack will be executed starting from the function pointer (func)
STACK_PTR nos_tcb_stack_init(void (*func)(void), STACK_PTR fos, UINT16 stack_size)
{
    STACK_PTR  sptr; // stack pointer
    sptr = fos + ((stack_size >> 1) - 1); // stack pointer(16bit) indicates the bottom of stack

    // Initially we pushes the starting address of the task
    // As soon as context switching, this thread will go jump to the start address (function pointer) of this function.
    *(--sptr) = (UINT16) func;            // PC to be restored

    // Insert fills 0x00, which is not used anywhere, into the stack.
    *(--sptr) = (UINT16) 0x0000;         // r4 = 0x00; push r4
    *(--sptr) = (UINT16) 0x0000;         // r5 = 0x00; push r5
    *(--sptr) = (UINT16) 0x0000;         // r6 = 0x00; push r6
    *(--sptr) = (UINT16) 0x0000;         // r7 = 0x00; push r7
    *(--sptr) = (UINT16) 0x0000;         // r8 = 0x00; push r8
    *(--sptr) = (UINT16) 0x0000;         // r9 = 0x00; push r9
    *(--sptr) = (UINT16) 0x0000;         // r10 = 0x00; push r10
    *(--sptr) = (UINT16) 0x0000;         // r11 = 0x00; push r11
    *(--sptr) = (UINT16) 0x0000;         // r12 = 0x00; push r12
    *(--sptr) = (UINT16) 0x0000;         // r13 = 0x00; push r13
    *(--sptr) = (UINT16) 0x0000;         // r14 = 0x00; push r14
    *(--sptr) = (UINT16) 0x0000;         // r15 = 0x00; push r15

    // SREG = 0x0008; enables GIE (Global Interrupt Enable) for context switching
    *(--sptr)	= (UINT16) 0x0008;         // SREG ; push SREG

    return ((STACK_PTR) sptr);      // eos
}

__attribute__ ((naked)) void nos_context_switch_core(void)
{
    // Save current thread state
    NOS_THREAD_SAVE_STATE();
    __SAVE_SP(tcb[_rtid]->sptr);

    _rtid = _next_rtid;
    // the selected thread will run soon after restoring context
    tcb[_rtid]->state = RUNNING_STATE;

    //Load new thread state
    __LOAD_SP(tcb[_rtid]->sptr);
    NOS_THREAD_LOAD_STATE();

    //NOS_RETURN(); // __asm__ volatile ("ret\n\t")
}

// just for 'EXIT_STATE' thread
void nos_context_load_core(void)
{
    tcb[_rtid]->state = RUNNING_STATE;
    // Load new thread state
    // EXC_RETURN+Global Interrupt Enable
	__LOAD_SP(tcb[_rtid]->sptr);
	NOS_THREAD_LOAD_STATE();
	//NOS_THREAD_LOAD_STATE_WITHOUT_EXEC_RETURN();
}

#endif /* THREAD_M */
