// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 */

#ifndef HAL_THREAD_H
#define HAL_THREAD_H
#include "kconf.h"
#ifdef KERNEL_M

#include "nos_common.h"

typedef UINT16   STACK_ENTRY;    // The stack is 2 byte-contiguous array
typedef UINT16   *STACK_PTR;     // stack pointer (16 bit wide)

#define DEFAULT_STACK_SIZE 300
#define SYSTEM_STACK_SIZE  300

#define SYSTEM_STACK_START_ADDR ((STACK_PTR) (RAMEND - SYSTEM_STACK_SIZE + 1))

#ifdef THREAD_M
// push general purpose register
#define __PUSH_GPRS()                           \
    __asm__ volatile (                          \
        "push   r4\n\t"                         \
        "push   r5\n\t"                         \
        "push   r6\n\t"                         \
        "push   r7\n\t"                         \
        "push   r8\n\t"                         \
        "push   r9\n\t"                         \
        "push   r10\n\t"                        \
        "push   r11\n\t"                        \
        "push   r12\n\t"                        \
        "push   r13\n\t"                        \
        "push   r14\n\t"                        \
        "push   r15\n\t"                        \
        )

// push status register
#define __PUSH_SREG()                           \
    __asm__ volatile (                          \
        "push   r2\n\t"                         \
        )

// the current stack pointer value is assigned to 16-bit 'addr' variable
#define __SAVE_SP(addr)                         \
    __asm__ volatile (                          \
        "mov r1, %0\n\t"                        \
        : "=r" ((UINT16) addr):                 \
        )

// pop general purpose registers
#define __POP_GPRS()                            \
    __asm__ volatile (                          \
        "pop    r15\n\t"                        \
        "pop    r14\n\t"                        \
        "pop    r13\n\t"                        \
        "pop    r12\n\t"                        \
        "pop    r11\n\t"                        \
        "pop    r10\n\t"                        \
        "pop    r9\n\t"                         \
        "pop    r8\n\t"                         \
        "pop    r7\n\t"                         \
        "pop    r6\n\t"                         \
        "pop    r5\n\t"                         \
        "pop    r4\n\t"                         \
        )

// pop status register
#define __POP_SREG()                            \
    __asm__ volatile (                          \
        "pop    r2\n\t"                         \
        )

// 16-bit 'addr' value is stored into stack pointer register.
// Thus, the system stack pointer is changed (context restored).
#define __LOAD_SP(addr)                         \
    __asm__ volatile (                          \
        "mov %0, r1\n\t"                        \
        :: "r" ((UINT16) addr)                  \
        )

#define NOS_THREAD_SAVE_STATE()                 \
    do {                                        \
        __PUSH_GPRS();                          \
        __PUSH_SREG();                          \
    } while(0)

#define NOS_THREAD_LOAD_STATE()                 \
    do {                                        \
        __POP_SREG();                           \
        __POP_GPRS();                           \
    } while(0)

#define NOS_RETURN() __asm__ volatile ("ret\n\t")

STACK_PTR nos_tcb_stack_init(void (*func)(void), STACK_PTR fos, UINT16 stack_size);
__attribute__ ((naked)) void nos_context_switch_core(void);
void nos_context_load_core(void);

#endif /* THREAD_M */
#endif /* KERNEL_M */
#endif /* HAL_THREAD_H */
