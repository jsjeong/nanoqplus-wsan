/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 */

#ifdef __MSPGCC__
#include <msp430.h>
#elif defined __ICC430__
#include "io430.h"
#endif

#include "adc.h"

#ifdef ADC_M
#include "critical_section.h"


void nos_adc_init()
{
    ADC12CTL0 = ADC12CTL1 = 0;

    /** must be ENC = 0 **/
    ADC12CTL0 |= ADC12ON;	//ADC core enable. (consumes power)
    ADC12CTL0 |= REFON;	// internal reference voltage on
    //ADC12CTL0 |= ~REF2_5V;	// reference generator voltage 2.5V (or 1.5V). REFON must be set.
    //ADC12CTL1 |= CSTARTADD0; // conversion memory register start adress[0~3] (ADC12MEM0~ADC12MEM15)

    ADC12CTL1 |= ADC12SSEL1;	// ADC12CLK = MCLK. ADC12 clock source - 00:ADC12OSC (consumes power), 01:ACLK, 10:MCLK, 11:SMCLK
    ADC12CTL1 |= ADC12DIV0;	// ADC12 clock divider[0~2]	 - divide by 1~8

    ADC12CTL1 |= SHP;	//ADC12 Pulse Sample Mode (or Extended Sample mode)
    ADC12CTL0 |= SHT01 | SHT00;	// 32 ADC12CLK cycles sampling. SHT1x, SHT0x - sample and hold time. only for when SHP =1.  
    //ADC12CTL0 |= MSC;	// Multiple sample and conversion. only for sequence or repeated modes.(ref. CONSEQx)

    //ADC12CTL1 |= SHS0 | SHS1;	// Sample-and-hold source - 00:ADC12SC bit, 01:TA1, 10:TB0
    /*******************/

    //ADC12CTL1 |= CONSEQ0 | CONSEQ1;	// ADC12 conversion sequence select (00:single channel and conversion, 01:sequence channel, 10:repeat single channel, 11:repeat sequence of channels)
    //ADC12CTL0 |= ENC;	// ADC12 Enable Conversion 
    //ADC12CTL0 |= ADC12SC;	// ADC12 start conversion
}


UINT16 nos_adc_convert(UINT8 adc_channel)
{
    UINT16 adc_result;
    NOS_ENTER_CRITICAL_SECTION();

    /** must be ENC = 0 **/
    ADC12CTL0 &= ~ENC;
    if (adc_channel == 0)
    {
        ADC12MCTL0 = INCH_0;
        ADC12MCTL0 |= SREF_1; // Select voltage reference. (VR+ = VREF+ and VR- = AVSS)
    }
    else if (adc_channel == 1)
    {
        ADC12MCTL0 = INCH_1;
    }
    else if (adc_channel == 2)
    {
        ADC12MCTL0 = INCH_2;
    }
    else if (adc_channel == 3)
    {
        ADC12MCTL0 = INCH_3;
    }
    else if (adc_channel == 4)
    {
        ADC12MCTL0 = INCH_4;
    }
    else if (adc_channel == 5)
    {
        ADC12MCTL0 = INCH_5;
    }
    else if (adc_channel == 6)
    {
        ADC12MCTL0 = INCH_6;
    }
    else if (adc_channel == 7)
    {
        ADC12MCTL0 = INCH_7;
    }
    else if (adc_channel == 10)
    {
        ADC12MCTL0 = INCH_10; // temperature diode
    }
    /*******************/

    ADC12CTL0 |= ADC12SC + ENC;	// SHI = 1
    ADC12CTL0 &= ~ADC12SC;	// SHI = 0
    // wait until the conversion completes; ADC12BUSY?
    while (ADC12CTL1 & ADC12BUSY);
    ADC12CTL0 &= ~ENC;

    // 12 bit data mode 
    adc_result = ADC12MEM0;
    NOS_EXIT_CRITICAL_SECTION();
    return(adc_result);
}
#endif // ADC_M
