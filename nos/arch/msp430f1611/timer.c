/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#include "timer.h"
#include "critical_section.h"
#include "platform.h"
#include "uart.h"

#ifdef UART_M
// TimerA interrupt period for DCO calibration. Here, overflow interrupt.
#define TA_CALIB_TICKS			65536
// Endurable the minmum of TimerB ticks while TimerA interrupt period
// TB_MIN_TICKS = TA_CALIB_TICKS * DCOCLK_MIN/DCOCLK * TBCLK/TACLK = 992
#define TB_MIN_TICKS			992
// Endurable the maximum of TimerB ticks while TimerA interrupt period
// TB_MAX_TICKS=TA_CALIB_TICKS * DCOCLK_MAX/DCOCLK * TBCLK/TACLK = 1056
#define TB_MAX_TICKS			1056
UINT16 prev_tbr;
#endif

#ifdef TIMECHK_M
#include "time_check.h"
extern void nos_timechk_init(void);
extern void nos_ust_time_ticks(void);
#endif

//Do not change anything.
void nos_timer_init()
{
    // TASSEL[9-8] - Timer A source select. 01:ACLK, 10:SMCLK
    // ID[7-6] - Input divider. 00:/1, 01:/2, 10:/4, 11/8
    // MC[5-4] - Mode control. 00:stop, 01:up, 10:continuous, 11:up/down
    // TACLR[2] - Timer A clear. Setting this bit resets TAR, the clock divider, the count direction (up direction sin up/down mode)
    // TAIE[1] - Timer A interrupt enable
    // TAIFG[0] - Timer A interrupt flag

    // Initialize TimerA.
    TACTL = TASSEL1| MC1;// SMCLK, continuous mode.  default : 0x0000
    // Initialize TimerB
    TBCTL = TBSSEL0 | MC1;	// ACLK, conitnuous mode.  default : 0x0000

#ifdef UART_M
    CLEAR_TIMERA_OVF_vect();
    ENABLE_TIMERA_OVF_vect();
#endif
#ifdef TIMECHK_M
    nos_timechk_init();
    CLEAR_TIMERB_OVF_vect();
    ENABLE_TIMERB_OVF_vect();
#endif
}

wakeup interrupt (TIMERA1_VECTOR) TAIV_ISR(void)
{
    UINT8 taiv;

    NOS_ENTER_ISR();
    taiv = TAIV;
    switch (taiv)
    {
        // Default.
        default:
            break;
    }
    NOS_EXIT_ISR();
}

/*
wakeup interrupt (TIMERB1_VECTOR) TBIV_ISR(void)
{
    NOS_ENTER_ISR();
    switch (TBIV)
    {
        //Capture/compare1 interrupt
        //case 0x0002:
        //	break;
        //case 0x0004:
        //	break;
        //case 0x0006:
        //	break;
        //case 0x0008:
        //	break;
        //case 0x000a:
        //	break;
        //case 0x000c:
        //	break;

#ifdef TIMECHK_M
        //Timer overflow interrupt
        case 0x000e:
            nos_ust_time_ticks();
            break;
#endif
        default:
            break;
    }


    NOS_EXIT_ISR();
}
*/

