// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_init.c
 * @author Haeyong Kim (ETRI)
 * @author Sangcheol Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "kconf.h"
#ifdef UART_M

#include "uart.h"
#include "platform.h"
#include "arch.h"
#include "clock.h"

#ifdef MSP430F1611_UART0_M

#if defined MSP430F1611_UART0_BR_9600
#define UART0_BR 9600L
#elif defined MSP430F1611_UART0_BR_19200
#define UART0_BR 19200L
#elif defined MSP430F1611_UART0_BR_38400
#define UART0_BR 38400L
#elif defined MSP430F1611_UART0_BR_57600
#define UART0_BR 57600L
#elif defined MSP430F1611_UART0_BR_115200
#define UART0_BR 115200L
#elif defined MSP430F1611_UART0_BR_230400
#define UART0_BR 230400L
#elif defined MSP430F1611_UART0_BR_380400
#define UART0_BR 380400L
#elif defined MSP430F1611_UART0_BR_460800
#define UART0_BR 460800L
#else
#error "MSP430F1611 UART0 unknown baud rate."
#endif

#endif //MSP430F1611_UART0_M

#ifdef MSP430F1611_UART1_M

#if defined MSP430F1611_UART1_BR_9600
#define UART1_BR 9600L
#elif defined MSP430F1611_UART1_BR_19200
#define UART1_BR 19200L
#elif defined MSP430F1611_UART1_BR_38400
#define UART1_BR 38400L
#elif defined MSP430F1611_UART1_BR_57600
#define UART1_BR 57600L
#elif defined MSP430F1611_UART1_BR_115200
#define UART1_BR 115200L
#elif defined MSP430F1611_UART1_BR_230400
#define UART1_BR 230400L
#elif defined MSP430F1611_UART1_BR_380400
#define UART1_BR 380400L
#elif defined MSP430F1611_UART1_BR_460800
#define UART1_BR 460800L
#else
#error "MSP430F1611 UART1 unknown baud rate."
#endif

#endif //MSP430F1611_UART1_M

//If you want to use 921600 baudrate, You must set SMCLK to 2048kHz or 4096kHz.

void nos_uart_init(UINT8 port_num)
{
#ifdef MSP430F1611_UART0_M
    if (port_num == MSP430F1611_USART0)
    {
        U0BR0 = (UINT16) (SMCLK * 1024L / UART0_BR) & 0xFF;
        U0BR1 = (UINT16) (SMCLK * 1024L / UART0_BR) >> 8;        

        //Use UART0 peripheral not genral I/O for P3.4, P3.5
        P3SEL |= (1 << 4) | (1 << 5);

        //USART control register init
        //Idle-line multiprocessor protocol, UART mode, no feedback,
        // Parity disabled, 1 stop bit, 8-bit data, 1 stop bit
        U0CTL = SWRST;	//USART logic held in reset state.
        U0CTL |= CHAR;  // 8-bit data.

        // USART receive control
        U0RCTL = 0x00;

        // Clock source select
        U0TCTL = SSEL1 | SSEL0 | TXEPT;	//SMCLK, U1TXBUF is empty.

        // module enable
        ME1 &= ~USPIE0;   // USART SPI module disable
        ME1 |= (UTXE0 | URXE0);   // USART UART module enable

        // interrupt flag initialize
        IFG1 |= UTXIFG0; // TX buffer is empty.
        IFG1 &= ~URXIFG0; // RX buffer is empty

        // USART reset released for operation
        U0CTL &= ~SWRST;

        return;
    }
#endif //MSP430F1611_UART0_M

#ifdef MSP430F1611_UART1_M
    if (port_num == MSP430F1611_USART1)
    {
        U1BR0 = (UINT16) (SMCLK * 1024L / UART1_BR) & 0xFF;
        U1BR1 = (UINT16) (SMCLK * 1024L / UART1_BR) >> 8;

        //Use UART1 peripheral not genral I/O for P3.6, P3.7
        P3SEL |= (1 << 6) | (1 << 7);

        //USART control register init
        //Idle-line multiprocessor protocol, UART mode, no feedback,
        // Parity disabled, 1 stop bit, 8-bit data, 1 stop bit
        U1CTL = SWRST;	//USART logic held in reset state.
        U1CTL |= CHAR;  // 8-bit data.

        // USART receive control
        U1RCTL = 0x00;

        // Clock source select
        U1TCTL = SSEL1 | SSEL0 | TXEPT;	//SMCLK, U1TXBUF is empty.

        // module enable
        ME2 &= ~USPIE1;   // USART SPI module disable
        ME2 |= (UTXE1 | URXE1);   // USART UART module enable

        // interrupt flag initialize
        IFG2 |= UTXIFG1; // TX buffer is empty.
        IFG2 &= ~URXIFG1; // RX buffer is empty

        // USART reset released for operation
        U1CTL &= ~SWRST;
        return;
    }
#endif //MSP430F1611_UART1_M
}

#endif // UART_M
