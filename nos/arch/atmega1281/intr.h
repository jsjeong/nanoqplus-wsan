// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 12. 12.
 */

#ifndef INTR_H
#define INTR_H

#include <avr/io.h>
#include "nos_common.h"

#define ENABLE_USART0_RX_vect()         do { _BIT_SET(UCSR0B, RXCIE0); } while (0)
#define DISABLE_USART0_RX_vect()        do { _BIT_CLR(UCSR0B, RXCIE0); } while (0)
#define USART0_RX_vect_IS_SET()         _IS_SET(UCSR0B, RXCIE0)

#define ENABLE_USART0_TX_vect()         do { _BIT_SET(UCSR0B, TXCIE0); } while (0)
#define DISABLE_USART0_TX_vect()        do { _BIT_CLR(UCSR0B, TXCIE0); } while (0)

#define ENABLE_USART0_UDRE_vect()       do { _BIT_SET(UCSR0B, UDRIE0); } while (0)
#define DISABLE_USART0_UDRE_vect()      do { _BIT_CLR(UCSR0B, UDRIE0); } while (0)

#define ENABLE_USART1_RX_vect()         do { _BIT_SET(UCSR1B, RXCIE1); } while (0)
#define DISABLE_USART1_RX_vect()        do { _BIT_CLR(UCSR1B, RXCIE1); } while (0)
#define USART1_RX_vect_IS_SET()         _IS_SET(UCSR1B, RXCIE1)

#define ENABLE_USART1_TX_vect()         do { _BIT_SET(UCSR1B, TXCIE1); } while (0)
#define DISABLE_USART1_TX_vect()        do { _BIT_CLR(UCSR1B, TXCIE1); } while (0)

#define ENABLE_USART1_UDRE_vect()       do { _BIT_SET(UCSR1B, UDRIE1); } while (0)
#define DISABLE_USART1_UDRE_vect()      do { _BIT_CLR(UCSR1B, UDRIE1); } while (0)

/* Pin Change Interrupt */
#define ENABLE_PCINT0_vect()	do { _BIT_SET(PCICR, PCIE0); } while(0) // PCINT0~7
#define DISABLE_PCINT0_vect()	do { _BIT_CLR(PCICR, PCIE0); } while(0) // PCINT0~7

#define ENABLE_PCINT1_vect()	do { _BIT_SET(PCICR, PCIE1); } while(0) // PCINT8~15
#define DISABLE_PCINT1_vect()	do { _BIT_CLR(PCICR, PCIE1); } while(0) // PCINT0~7

#define ENABLE_PCINT2_vect()	do { _BIT_SET(PCICR, PCIE2); } while(0) // PCINT16~23
#define DISABLE_PCINT2_vect()	do { _BIT_CLR(PCICR, PCIE2); } while(0) // PCINT0~7

#define ENABLE_PCINT3_vect()	do { _BIT_SET(PCICR, PCIE3); } while(0) // PCINT24~31
#define DISABLE_PCINT3_vect()	do { _BIT_CLR(PCICR, PCIE3); } while(0) // PCINT0~7

/* Timer/Counter2 Interrupts */
#define ENABLE_TIMER2_OVF_vect()        _BIT_SET(TIMSK2, TOIE2)
#define DISABLE_TIMER2_OVF_vect()       _BIT_CLR(TIMSK2, TOIE2)
#define CLEAR_TIMER2_OVF_vect()         _BIT_SET(TIFR2, TOV2)

#define ENABLE_TIMER2_COMPA_vect()      _BIT_SET(TIMSK2, OCIE2A)
#define DISABLE_TIMER2_COMPA_vect()     _BIT_CLR(TIMSK2, OCIE2A)
#define CLEAR_TIMER2_COMPA_vect()       _BIT_SET(TIFR2, OCF2A)

#define ENABLE_TIMER2_COMPB_vect()      _BIT_SET(TIMSK2, OCIE2B)
#define DISABLE_TIMER2_COMPB_vect()     _BIT_CLR(TIMSK2, OCIE2B)
#define CLEAR_TIMER2_COMPB_vect()       _BIT_SET(TIFR2, OCF2B)
#define IS_PENDING_TIMER2_COMPB_vect()	_IS_SET(TIFR2, OCF2B)

/* Timer/Counter1 Interrupts */
#define ENABLE_TIMER1_CAPT_vect()       _BIT_SET(TIMSK1, ICIE1)
#define DISABLE_TIMER1_CAPT_vect()      _BIT_CLR(TIMSK1, ICIE1)
#define CLEAR_TIMER1_CAPT_vect()        _BIT_SET(TIFR1, ICF1)

#endif	// ~INTR_H
