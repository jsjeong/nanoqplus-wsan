/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @note This code is imported from timer.c of ATmega1284P.
 */

/*
 * $LastChangedDate: 2012-12-12 08:49:28 +0900 (2012-12-12, 수) $
 * $Id: timer.c 1062 2012-12-11 23:49:28Z jsjeong $
 */

#include <avr/io.h>
#include <avr/interrupt.h>

#include "timer.h"
#include "critical_section.h"
#include "platform.h"

#ifdef TIMECHK_M
#include "time_check.h"
UINT8 local_clock_count_H;
extern void nos_timechk_init(void);
extern void nos_ust_time_ticks(void);

UINT16 nos_get_us_counter()
{
    UINT16 counter;
    NOS_ENTER_CRITICAL_SECTION();
    counter = (UINT16)TCNT0 + (UINT16)(local_clock_count_H<<8);
    NOS_EXIT_CRITICAL_SECTION();
    return counter;
}
#endif

void nos_timer_init()
{
#ifdef TIMECHK_M
    // Setting for local clock
    ASSR = (1 << AS0); // user asynchronous clock : 32.768 KHz 
    TCCR0 = (1 << CS00); // prescaler : no prescaling;
    local_clock_count_H=0;
    nos_timechk_init();

    CLEAR_TIMER0_OVF_vect();
    ENABLE_TIMER0_OVF_vect();
#endif
}

#ifdef TIMECHK_M
// 32768KHz, each TCNT0 count means 30.517578125us, 256ticks --> interrupt every 7.8125ms  (*128 = 1sec)
ISR(TIMER0_OVF_vect)
{
    NOS_ENTER_ISR();
    if (++local_clock_count_H == 0)
    {
        //Local clock callback will be called every 2 sec.
        nos_ust_time_ticks();
    }
    NOS_EXIT_ISR();
}
#endif
