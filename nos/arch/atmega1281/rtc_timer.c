// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 12. 12.
 *
 * @note This code is imported from rtc_timer.c of ATmega1284P.
 */

#include "rtc_timer.h"

#if defined(LIB_UTC_CLOCK_M) || defined(KERNEL_M)
#include <avr/interrupt.h>
#include <avr/io.h>

#ifdef LIB_UTC_CLOCK_M
#include "utc-clock.h"
#endif

#include "intr.h"
#include "critical_section.h"

void nos_rtc_timer_init(void)
{
	/*** Asynchronous operation of T/C2 ***/
	//1. Disable the Timer2 interrupts
	//DISABLE_TIMER2_OVF_vect();
	//DISABLE_TIMER2_COMPA_vect();
	//DISABLE_TIMER2_COMPB_vect();

	//2. Selcet clock source 
	//_BIT_CLR(ASSR, EXCLK);	// 0:32KHz XTAL, 1:TOSC1
	_BIT_SET(ASSR, AS2);	//Clocked from a XOSC connected to TOSC1. (p.161)

	//3. operation mode
	//Normal mode, TOP=0xFF (p.158)
	//_BIT_CLR(TCCR2A, WGM20);
	//_BIT_CLR(TCCR2A, WGM21);
	//_BIT_CLR(TCCR2B, WGM22);

	//4. Wait for TCN2UB, OCR2xUB, and TCR2xUB.
  while(ASSR & ((1 << TCN2UB) |
                (1 << OCR2AUB) |
                (1 << OCR2BUB) |
                (1 << TCR2AUB) |
                (1 << TCR2BUB)));

	//5. Clear interrupt flags
	CLEAR_TIMER2_OVF_vect();
	CLEAR_TIMER2_COMPA_vect();
	CLEAR_TIMER2_COMPB_vect();

	//6. Enable interrupts
#ifdef LIB_UTC_CLOCK_M
	ENABLE_TIMER2_OVF_vect();
#endif

	// 7. Start timer (prescale: 32768Hz/128, p.160)
	_BIT_SET(TCCR2B, CS20);
	_BIT_CLR(TCCR2B, CS21);
	_BIT_SET(TCCR2B, CS22);
}



#ifdef LIB_UTC_CLOCK_M

/**
 * Handles UTC_IRQ (current UTC time)
 */
ISR(TIMER2_OVF_vect, ISR_BLOCK)
{
    NOS_ENTER_ISR();
    nos_utc_increment();
    NOS_EXIT_ISR();
}

#endif //LIB_UTC_CLOCK_M
#endif //LIB_UTC_CLOCK_M || KERNEL_M

