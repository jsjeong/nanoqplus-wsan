// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_getc.c
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 12. 12.
 */

#include "kconf.h"
#ifdef UART_M

#include "uart.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include "critical_section.h"
#include "intr.h"

volatile char uart_rx_char;
void (*nos_uart0_rx_callback)(char) = NULL;
void (*nos_uart1_rx_callback)(char) = NULL;

void nos_uart_set_getc_callback(UINT8 port_num, void (*func)(char))
{
    if (port_num == 0)
    {
        nos_uart0_rx_callback = func;
    }
    else
    {
        nos_uart1_rx_callback = func;
    }
}

void (*nos_uart_get_getc_callback(UINT8 port_num))(char)
{
    switch (port_num)
    {
    case 0:
        return nos_uart0_rx_callback;

    case 1:
        return nos_uart1_rx_callback;

    default:
        return NULL;
    }
}

void nos_uart_enable_rx_intr(UINT8 port_num)
{
    if (port_num == ATMEGA1281_USART0)
    {
        ENABLE_USART0_RX_vect();
        return;
    }

    if (port_num == ATMEGA1281_USART1)
    {
        ENABLE_USART1_RX_vect();
        return;
    }
}

void nos_uart_disable_rx_intr(UINT8 port_num)
{
    if (port_num == ATMEGA1281_USART0)
    {
        DISABLE_USART0_RX_vect();
    }

    if (port_num == ATMEGA1281_USART1)
    {
        DISABLE_USART0_RX_vect();
    }
}


ISR(USART0_RX_vect)
{
    NOS_ENTER_ISR();
    uart_rx_char = UDR0;
    if (nos_uart0_rx_callback)
    {
        nos_uart0_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}

ISR(USART1_RX_vect)
{
    NOS_ENTER_ISR();
    uart_rx_char = UDR1;
    if (nos_uart1_rx_callback)
    {
        nos_uart1_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}

BOOL nos_uart_rx_intr_is_set(UINT8 port_num)
{
    switch (port_num)
    {
    case 0:
        return USART0_RX_vect_IS_SET();

    case 1:
        return USART1_RX_vect_IS_SET();

    default:
        return FALSE;
    }
}

#endif // UART_M 
