// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * malloc() and free() redefinitions for thread-safety
 *
 * @author Junkeun Song (ETRI)
 * @date 2012. 12. 12.
 */

#include "heap.h"        
#include <stdlib.h>
#include "critical_section.h"

struct __freelist
{
    UINT16 sz;
    struct __freelist *nx;
};

struct __freelist *__flp = NULL;
INT8 *__brkval = 0; // first location not yet allocated. use for making new chunk

extern char __heap_start;
extern char __heap_end;

//#define HEAP_DEBUG
#ifdef HEAP_DEBUG
#include <stdio.h>
#endif

void *malloc(UINT16 len)
{
    struct __freelist *fp1, *fp2;
    INT8 *cp;
    UINT16 s, avail;

#ifdef HEAP_DEBUG
    printf("%s()-len:%u\n", __FUNCTION__, len);
#endif

    /* 
     * The minimum chunk size is the size of a pointer (plus the size of the "sz"
     * field, but we don't need to account for this), otherwise we could not
     * possibly fit a freelist entry into the chunk later.
     */

    if (len < sizeof(struct __freelist) - sizeof(UINT16))
        len = sizeof(struct __freelist) - sizeof(UINT16);

    NOS_ENTER_CRITICAL_SECTION();

    /*
     * Step 1: Walk the free list and try finding a chunk that would match 
     * exactly. If we found one, we are done. While walking, note down the size
     * of the largest chunk we found that would still fit the request -- we need
     * it for step 2.
     */

    for (s = 0, fp1 = __flp, fp2 = 0; fp1; fp2 = fp1, fp1 = fp1->nx)
    {
        // Exact match of the free list entries
        if (fp1->sz == len)
        {
            // Found it. Disconnect the chunk from the freelist, and return it.
            if (fp2)
                fp2->nx = fp1->nx;
            else
                __flp = fp1->nx;

            NOS_EXIT_CRITICAL_SECTION();

#ifdef HEAP_DEBUG
            printf("%s()-mem:0x%x, (1)\n", __FUNCTION__, (MEMADDR_T) &(fp1->nx));
#endif
            return &(fp1->nx);
        }

        // More larger free list entry than requested
        if (fp1->sz > len)
        {
            if (s == 0 || fp1->sz < s)
                s = fp1->sz;
        }
    }

    /*
     * Step 2: If we found a chunk on the freelist that would fit (but was too
     * large), look it up again and use it, since it is our closest match now.
     * Since the freelist entry needs to be split into two entries then, watch
     * out that the difference between the requested size and the size of the 
     * chunk found is large enough for another freelist entry; if not, just 
     * enlarge the request size to what we have found, and use the entire chunk.
     */

    if (s)
    {
        if (s - len < sizeof(struct __freelist))
            len = s;

        for (fp1 = __flp, fp2 = 0; fp1; fp2 = fp1, fp1 = fp1->nx)
        {
            if (fp1->sz == s)
            {
                if (len == s) // Use entire chunk; same as above
                {
                    if (fp2)
                        fp2->nx = fp1->nx;
                    else
                        __flp = fp1->nx;

                    NOS_EXIT_CRITICAL_SECTION();
#ifdef HEAP_DEBUG
                    printf("%s()-mem:0x%x (2)\n",
                           __FUNCTION__, (MEMADDR_T) &(fp1->nx));
#endif
                    return &(fp1->nx);
                }

                /*
                 * Split them up. Note that we leave the first part as the new
                 * (smaller) freelist entry, and return the upper portion to the
                 * caller. This saves us the work to fix up the freelist chain;
                 * we just need to fixup the size of the current entry, and note
                 * down the size of the new chunk before returning it to the
                 * caller.
                 */

                cp = (INT8 *)fp1;
                s -= len;
                cp += s;
                fp2 = (struct __freelist *)cp;
                fp2->sz = len;
                fp1->sz = s - sizeof(UINT16);

                NOS_EXIT_CRITICAL_SECTION();
#ifdef HEAP_DEBUG
                printf("%s()-mem:0x%x (3)\n",
                       __FUNCTION__, (MEMADDR_T) &(fp2->nx));
#endif
                return &(fp2->nx);
            }
        }
    }

    /* Step 3: If the request could not be satisfied from a freelist entry, just
     * prepare a new chunk. This means we need to obtain more memory first.
     * The largest address just not allocated so far is remembered in the brkval
     * variable. Under Unix, the "break value" was the end of the data segment as
     * dynamically requested from the operating system. Since we don't have an
     * operating system, just make sure * that we don't collide with the stack.
     */

    if (__brkval == 0)
        __brkval = (INT8 *) &__heap_start;

    cp = (INT8 *) &__heap_end;

    avail = cp - __brkval; // currently available memory space for a new chunk

    // Both tests below are needed to catch the case len >= 0xfffe.
    if (avail >= len && avail >= len + sizeof(UINT16))
    {
        fp1 = (struct __freelist *)__brkval;
        __brkval += len + sizeof(UINT16);
        fp1->sz = len;

        NOS_EXIT_CRITICAL_SECTION();
#ifdef HEAP_DEBUG
        printf("%s()-mem:0x%x (4)\n",
               __FUNCTION__, (MEMADDR_T) &(fp1->nx));
#endif
        return &(fp1->nx);
    }

    /* Step 4 : There's no help, just fail. :-/ */

    NOS_EXIT_CRITICAL_SECTION();

#ifdef HEAP_DEBUG
    printf("%s()-mem:0x0 (5)\n", __FUNCTION__);
#endif
    return 0;
}

void free(void *p)
{
    struct __freelist *fp1, *fp2, *fpnew;
    INT8 *cp1, *cp2, *cpnew;

#ifdef HEAP_DEBUG
    printf("%s()-mem:0x%x\n", __FUNCTION__, (MEMADDR_T) p);
#endif

    // ISO C says free(NULL) must be a no-op
    if (p == 0) return;

    cpnew = p;
    cpnew -= sizeof(size_t);
    fpnew = (struct __freelist *)cpnew;
    fpnew->nx = 0;

    // If there's no freelist yet, our entry will be the only one on it.
    if (__flp == 0)
    {
        __flp = fpnew;
        return;
    }

    NOS_ENTER_CRITICAL_SECTION();

    /*
     * Now, find the position where our new entry belongs onto the freelist.
     * Try to aggregate the chunk with adjacent chunks if possible.
     */

    for (fp1 = __flp, fp2 = 0; fp1; fp2 = fp1, fp1 = fp1->nx)
    {
        if (fp1 < fpnew) continue;

        cp1 = (INT8 *) fp1;
        fpnew->nx = fp1;

        if ((INT8 *)&(fpnew->nx) + fpnew->sz == cp1)
        {
            // upper chunk adjacent, assimilate it.
            fpnew->sz += fp1->sz + sizeof(UINT16);
            fpnew->nx = fp1->nx;
        }

        if (fp2 == 0)
        {
            // new head of freelist
            __flp = fpnew;
            NOS_EXIT_CRITICAL_SECTION();

            return;
        }

        break;
    }

    /* 
     * Note that we get here either if we hit the "break" above, or if we fell
     * off the end of the loop. The latter means we've got a new topmost chunk.
     * Either way, try aggregating with the lower chunk if possible.
     */

    fp2->nx = fpnew;
    cp2 = (INT8 *) &(fp2->nx);
    if (cp2 + fp2->sz == cpnew) // lower junk adjacent, merge
    {
        fp2->sz += fpnew->sz + sizeof(UINT16);
        fp2->nx = fpnew->nx;
    }

    NOS_EXIT_CRITICAL_SECTION();
}
