// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_init.c
 * UART initialization for STM32F2xx.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 2. 27.
 */

#include "kconf.h"
#ifdef UART_M

#include "uart.h"
#include "stm32f2xx_usart.h"
#include "stm32f2xx_gpio.h"
#include "platform.h"

#ifdef STM32F2XX_USART1_AS_UART_M
#ifdef STM32F2XX_USART1_BR_300
#define USART1_BR                           300
#elif defined STM32F2XX_USART1_BR_600
#define USART1_BR                           600
#elif defined STM32F2XX_USART1_BR_1200
#define USART1_BR                           1200
#elif defined STM32F2XX_USART1_BR_1800
#define USART1_BR                           1800
#elif defined STM32F2XX_USART1_BR_2400
#define USART1_BR                           2400
#elif defined STM32F2XX_USART1_BR_4800
#define USART1_BR                           4800
#elif defined STM32F2XX_USART1_BR_9600
#define USART1_BR                           9600
#elif defined STM32F2XX_USART1_BR_19200
#define USART1_BR                           19200
#elif defined STM32F2XX_USART1_BR_38400
#define USART1_BR                           38400
#elif defined STM32F2XX_USART1_BR_57600
#define USART1_BR                           57600
#elif defined STM32F2XX_USART1_BR_115200
#define USART1_BR                           115200
#elif defined STM32F2XX_USART1_BR_230400
#define USART1_BR                           230400
#elif defined STM32F2XX_USART1_BR_380400
#define USART1_BR                           380400
#elif defined STM32F2XX_USART1_BR_460800
#define USART1_BR                           460800
#elif defined STM32F2XX_USART1_BR_500000
#define USART1_BR                           500000
#elif defined STM32F2XX_USART1_BR_576000
#define USART1_BR                           576000
#elif defined STM32F2XX_USART1_BR_921600
#define USART1_BR                           921600
#elif defined STM32F2XX_USART1_BR_1000000
#define USART1_BR                           1000000
#elif defined STM32F2XX_USART1_BR_1152000
#define USART1_BR                           1152000
#elif defined STM32F2XX_USART1_BR_2000000
#define USART1_BR                           2000000
#elif defined STM32F2XX_USART1_BR_2500000
#define USART1_BR                           2500000
#elif defined STM32F2XX_USART1_BR_3000000
#define USART1_BR                           3000000
#elif defined STM32F2XX_USART1_BR_3500000
#define USART1_BR                           3500000
#elif defined STM32F2XX_USART1_BR_4000000
#define USART1_BR                           4000000
#else
#error "Unsupported baud rate for USART1"
#endif /* For USART1_BR */

#if defined STM32F2XX_USART1_DATABITS_8
#define USART1_DATABITS                     USART_WordLength_8b
#elif defined STM32F2XX_USART1_DATABITS_9
#define USART1_DATABITS                     USART_WordLength_9b
#else
#error "Unsupported databits for USART1"
#endif /* For USART1_DATABITS */

#if defined STM32F2XX_USART1_PARITY_NONE
#define USART1_PARITY                       USART_Parity_No
#elif defined STM32F2XX_USART1_PARITY_EVEN
#define USART1_PARITY                       USART_Parity_Even
#elif defined STM32F2XX_USART1_PARITY_ODD
#define USART1_PARITY                       USART_Parity_Odd
#else
#error "Unsupported parity mode for USART1"
#endif /* For USART1_PARITY */

#if defined STM32F2XX_USART1_STOPBITS_HALF
#define USART1_STOPBITS                     USART_StopBits_0_5
#elif defined STM32F2XX_USART1_STOPBITS_ONE
#define USART1_STOPBITS                     USART_StopBits_1
#elif defined STM32F2XX_USART1_STOPBITS_ONEANDAHALF
#define USART1_STOPBITS                     USART_StopBits_1_5
#elif defined STM32F2XX_USART1_STOPBITS_TWO
#define USART1_STOPBITS                     USART_StopBits_2
#else
#error
#endif /* For USART1_STOPBITS */
#endif /* STM32F2XX_USART1_AS_UART_M */

#ifdef STM32F2XX_USART2_AS_UART_M
#ifdef STM32F2XX_USART2_BR_300
#define USART2_BR                           300
#elif defined STM32F2XX_USART2_BR_600
#define USART2_BR                           600
#elif defined STM32F2XX_USART2_BR_1200
#define USART2_BR                           1200
#elif defined STM32F2XX_USART2_BR_1800
#define USART2_BR                           1800
#elif defined STM32F2XX_USART2_BR_2400
#define USART2_BR                           2400
#elif defined STM32F2XX_USART2_BR_4800
#define USART2_BR                           4800
#elif defined STM32F2XX_USART2_BR_9600
#define USART2_BR                           9600
#elif defined STM32F2XX_USART2_BR_19200
#define USART2_BR                           19200
#elif defined STM32F2XX_USART2_BR_38400
#define USART2_BR                           38400
#elif defined STM32F2XX_USART2_BR_57600
#define USART2_BR                           57600
#elif defined STM32F2XX_USART2_BR_115200
#define USART2_BR                           115200
#elif defined STM32F2XX_USART2_BR_230400
#define USART2_BR                           230400
#elif defined STM32F2XX_USART2_BR_380400
#define USART2_BR                           380400
#elif defined STM32F2XX_USART2_BR_460800
#define USART2_BR                           460800
#elif defined STM32F2XX_USART2_BR_500000
#define USART2_BR                           500000
#elif defined STM32F2XX_USART2_BR_576000
#define USART2_BR                           576000
#elif defined STM32F2XX_USART2_BR_921600
#define USART2_BR                           921600
#elif defined STM32F2XX_USART2_BR_1000000
#define USART2_BR                           1000000
#elif defined STM32F2XX_USART2_BR_1152000
#define USART2_BR                           1152000
#elif defined STM32F2XX_USART2_BR_2000000
#define USART2_BR                           2000000
#elif defined STM32F2XX_USART2_BR_2500000
#define USART2_BR                           2500000
#elif defined STM32F2XX_USART2_BR_3000000
#define USART2_BR                           3000000
#elif defined STM32F2XX_USART2_BR_3500000
#define USART2_BR                           3500000
#elif defined STM32F2XX_USART2_BR_4000000
#define USART2_BR                           4000000
#else
#error "Unsupported baud rate for USART2"
#endif /* For USART2_BR */

#if defined STM32F2XX_USART2_DATABITS_8
#define USART2_DATABITS                     USART_WordLength_8b
#elif defined STM32F2XX_USART2_DATABITS_9
#define USART2_DATABITS                     USART_WordLength_9b
#else
#error "Unsupported databits for USART2"
#endif /* For USART2_DATABITS */

#if defined STM32F2XX_USART2_PARITY_NONE
#define USART2_PARITY                       USART_Parity_No
#elif defined STM32F2XX_USART2_PARITY_EVEN
#define USART2_PARITY                       USART_Parity_Even
#elif defined STM32F2XX_USART2_PARITY_ODD
#define USART2_PARITY                       USART_Parity_Odd
#else
#error "Unsupported parity mode for USART2"
#endif /* For USART2_PARITY */

#if defined STM32F2XX_USART2_STOPBITS_HALF
#define USART2_STOPBITS                     USART_StopBits_0_5
#elif defined STM32F2XX_USART2_STOPBITS_ONE
#define USART2_STOPBITS                     USART_StopBits_1
#elif defined STM32F2XX_USART2_STOPBITS_ONEANDAHALF
#define USART2_STOPBITS                     USART_StopBits_1_5
#elif defined STM32F2XX_USART2_STOPBITS_TWO
#define USART2_STOPBITS                     USART_StopBits_2
#else
#error " Unsupported stopbits for USART2"
#endif /* For USART2_STOPBITS */
#endif /* STM32F2XX_USART2_AS_UART_M */

#ifdef STM32F2XX_USART3_AS_UART_M
#ifdef STM32F2XX_USART3_BR_300
#define USART3_BR                           300
#elif defined STM32F2XX_USART3_BR_600
#define USART3_BR                           600
#elif defined STM32F2XX_USART3_BR_1200
#define USART3_BR                           1200
#elif defined STM32F2XX_USART3_BR_1800
#define USART3_BR                           1800
#elif defined STM32F2XX_USART3_BR_2400
#define USART3_BR                           2400
#elif defined STM32F2XX_USART3_BR_4800
#define USART3_BR                           4800
#elif defined STM32F2XX_USART3_BR_9600
#define USART3_BR                           9600
#elif defined STM32F2XX_USART3_BR_19200
#define USART3_BR                           19200
#elif defined STM32F2XX_USART3_BR_38400
#define USART3_BR                           38400
#elif defined STM32F2XX_USART3_BR_57600
#define USART3_BR                           57600
#elif defined STM32F2XX_USART3_BR_115200
#define USART3_BR                           115200
#elif defined STM32F2XX_USART3_BR_230400
#define USART3_BR                           230400
#elif defined STM32F2XX_USART3_BR_380400
#define USART3_BR                           380400
#elif defined STM32F2XX_USART3_BR_460800
#define USART3_BR                           460800
#elif defined STM32F2XX_USART3_BR_500000
#define USART3_BR                           500000
#elif defined STM32F2XX_USART3_BR_576000
#define USART3_BR                           576000
#elif defined STM32F2XX_USART3_BR_921600
#define USART3_BR                           921600
#elif defined STM32F2XX_USART3_BR_1000000
#define USART3_BR                           1000000
#elif defined STM32F2XX_USART3_BR_1152000
#define USART3_BR                           1152000
#elif defined STM32F2XX_USART3_BR_2000000
#define USART3_BR                           2000000
#elif defined STM32F2XX_USART3_BR_2500000
#define USART3_BR                           2500000
#elif defined STM32F2XX_USART3_BR_3000000
#define USART3_BR                           3000000
#elif defined STM32F2XX_USART3_BR_3500000
#define USART3_BR                           3500000
#elif defined STM32F2XX_USART3_BR_4000000
#define USART3_BR                           4000000
#else
#error "Unsupported baud rate for USART3"
#endif /* For USART3_BR */

#if defined STM32F2XX_USART3_DATABITS_8
#define USART3_DATABITS                     USART_WordLength_8b
#elif defined STM32F2XX_USART3_DATABITS_9
#define USART3_DATABITS                     USART_WordLength_9b
#else
#error "Unsupported databits for USART3"
#endif /* For USART3_DATABITS */

#if defined STM32F2XX_USART3_PARITY_NONE
#define USART3_PARITY                       USART_Parity_No
#elif defined STM32F2XX_USART3_PARITY_EVEN
#define USART3_PARITY                       USART_Parity_Even
#elif defined STM32F2XX_USART3_PARITY_ODD
#define USART3_PARITY                       USART_Parity_Odd
#else
#error "Unsupported parity mode for USART3"
#endif /* For USART3_PARITY */

#if defined STM32F2XX_USART3_STOPBITS_HALF
#define USART3_STOPBITS                     USART_StopBits_0_5
#elif defined STM32F2XX_USART3_STOPBITS_ONE
#define USART3_STOPBITS                     USART_StopBits_1
#elif defined STM32F2XX_USART3_STOPBITS_ONEANDAHALF
#define USART3_STOPBITS                     USART_StopBits_1_5
#elif defined STM32F2XX_USART3_STOPBITS_TWO
#define USART3_STOPBITS                     USART_StopBits_2
#else
#error "Unsupported stopbits for USART3"
#endif /* For USART3_STOPBITS */
#endif /* STM32F2XX_USART3_AS_UART_M */

#ifdef STM32F2XX_UART4_AS_UART_M
#ifdef STM32F2XX_UART4_BR_300
#define UART4_BR                            300
#elif defined STM32F2XX_UART4_BR_600
#define UART4_BR                            600
#elif defined STM32F2XX_UART4_BR_1200
#define UART4_BR                            1200
#elif defined STM32F2XX_UART4_BR_1800
#define UART4_BR                            1800
#elif defined STM32F2XX_UART4_BR_2400
#define UART4_BR                            2400
#elif defined STM32F2XX_UART4_BR_4800
#define UART4_BR                            4800
#elif defined STM32F2XX_UART4_BR_9600
#define UART4_BR                            9600
#elif defined STM32F2XX_UART4_BR_19200
#define UART4_BR                            19200
#elif defined STM32F2XX_UART4_BR_38400
#define UART4_BR                            38400
#elif defined STM32F2XX_UART4_BR_57600
#define UART4_BR                            57600
#elif defined STM32F2XX_UART4_BR_115200
#define UART4_BR                            115200
#elif defined STM32F2XX_UART4_BR_230400
#define UART4_BR                            230400
#elif defined STM32F2XX_UART4_BR_380400
#define UART4_BR                            380400
#elif defined STM32F2XX_UART4_BR_460800
#define UART4_BR                            460800
#elif defined STM32F2XX_UART4_BR_500000
#define UART4_BR                            500000
#elif defined STM32F2XX_UART4_BR_576000
#define UART4_BR                            576000
#elif defined STM32F2XX_UART4_BR_921600
#define UART4_BR                            921600
#elif defined STM32F2XX_UART4_BR_1000000
#define UART4_BR                            1000000
#elif defined STM32F2XX_UART4_BR_1152000
#define UART4_BR                            1152000
#elif defined STM32F2XX_UART4_BR_2000000
#define UART4_BR                            2000000
#elif defined STM32F2XX_UART4_BR_2500000
#define UART4_BR                            2500000
#elif defined STM32F2XX_UART4_BR_3000000
#define UART4_BR                            3000000
#elif defined STM32F2XX_UART4_BR_3500000
#define UART4_BR                            3500000
#elif defined STM32F2XX_UART4_BR_4000000
#define UART4_BR                            4000000
#else
#error "Unsupported baud rate for UART4"
#endif /* For UART4_BR */

#if defined STM32F2XX_UART4_DATABITS_8
#define UART4_DATABITS                      USART_WordLength_8b
#elif defined STM32F2XX_UART4_DATABITS_9
#define UART4_DATABITS                      USART_WordLength_9b
#else
#error "Unsupported databits for UART4"
#endif /* For UART4_DATABITS */

#if defined STM32F2XX_UART4_PARITY_NONE
#define UART4_PARITY                        USART_Parity_No
#elif defined STM32F2XX_UART4_PARITY_EVEN
#define UART4_PARITY                        USART_Parity_Even
#elif defined STM32F2XX_UART4_PARITY_ODD
#define UART4_PARITY                        USART_Parity_Odd
#else
#error "Unsupported parity mode for UART4"
#endif /* For UART4_PARITY */

#if defined STM32F2XX_UART4_STOPBITS_HALF
#define UART4_STOPBITS                      USART_StopBits_0_5
#elif defined STM32F2XX_UART4_STOPBITS_ONE
#define UART4_STOPBITS                      USART_StopBits_1
#elif defined STM32F2XX_UART4_STOPBITS_ONEANDAHALF
#define UART4_STOPBITS                      USART_StopBits_1_5
#elif defined STM32F2XX_UART4_STOPBITS_TWO
#define UART4_STOPBITS                      USART_StopBits_2
#else
#error "Unsupported stopbits for UART4"
#endif /* For UART4_STOPBITS */
#endif /* STM32F2XX_UART4_AS_UART_M */

#ifdef STM32F2XX_UART5_AS_UART_M
#ifdef STM32F2XX_UART5_BR_300
#define UART5_BR                            300
#elif defined STM32F2XX_UART5_BR_600
#define UART5_BR                            600
#elif defined STM32F2XX_UART5_BR_1200
#define UART5_BR                            1200
#elif defined STM32F2XX_UART5_BR_1800
#define UART5_BR                            1800
#elif defined STM32F2XX_UART5_BR_2400
#define UART5_BR                            2400
#elif defined STM32F2XX_UART5_BR_4800
#define UART5_BR                            4800
#elif defined STM32F2XX_UART5_BR_9600
#define UART5_BR                            9600
#elif defined STM32F2XX_UART5_BR_19200
#define UART5_BR                            19200
#elif defined STM32F2XX_UART5_BR_38400
#define UART5_BR                            38400
#elif defined STM32F2XX_UART5_BR_57600
#define UART5_BR                            57600
#elif defined STM32F2XX_UART5_BR_115200
#define UART5_BR                            115200
#elif defined STM32F2XX_UART5_BR_230400
#define UART5_BR                            230400
#elif defined STM32F2XX_UART5_BR_380400
#define UART5_BR                            380400
#elif defined STM32F2XX_UART5_BR_460800
#define UART5_BR                            460800
#elif defined STM32F2XX_UART5_BR_500000
#define UART5_BR                            500000
#elif defined STM32F2XX_UART5_BR_576000
#define UART5_BR                            576000
#elif defined STM32F2XX_UART5_BR_921600
#define UART5_BR                            921600
#elif defined STM32F2XX_UART5_BR_1000000
#define UART5_BR                            1000000
#elif defined STM32F2XX_UART5_BR_1152000
#define UART5_BR                            1152000
#elif defined STM32F2XX_UART5_BR_2000000
#define UART5_BR                            2000000
#elif defined STM32F2XX_UART5_BR_2500000
#define UART5_BR                            2500000
#elif defined STM32F2XX_UART5_BR_3000000
#define UART5_BR                            3000000
#elif defined STM32F2XX_UART5_BR_3500000
#define UART5_BR                            3500000
#elif defined STM32F2XX_UART5_BR_4000000
#define UART5_BR                            4000000
#else
#error "Unsupported baud rate for UART5"
#endif /* For UART5_BR */

#if defined STM32F2XX_UART5_DATABITS_8
#define UART5_DATABITS                      USART_WordLength_8b
#elif defined STM32F2XX_UART5_DATABITS_9
#define UART5_DATABITS                      USART_WordLength_9b
#else
#error "Unsupported databits for UART5"
#endif /* For UART5_DATABITS */

#if defined STM32F2XX_UART5_PARITY_NONE
#define UART5_PARITY                        USART_Parity_No
#elif defined STM32F2XX_UART5_PARITY_EVEN
#define UART5_PARITY                        USART_Parity_Even
#elif defined STM32F2XX_UART5_PARITY_ODD
#define UART5_PARITY                        USART_Parity_Odd
#else
#error "Unsupported parity mode for UART5"
#endif /* For UART5_PARITY */

#if defined STM32F2XX_UART5_STOPBITS_HALF
#define UART5_STOPBITS                      USART_StopBits_0_5
#elif defined STM32F2XX_UART5_STOPBITS_ONE
#define UART5_STOPBITS                      USART_StopBits_1
#elif defined STM32F2XX_UART5_STOPBITS_ONEANDAHALF
#define UART5_STOPBITS                      USART_StopBits_1_5
#elif defined STM32F2XX_UART5_STOPBITS_TWO
#define UART5_STOPBITS                      USART_StopBits_2
#else
#error "Unsupported stopbits for UART5"
#endif /* For UART5_STOPBITS */
#endif /* STM32F2XX_UART5_AS_UART_M */

#ifdef STM32F2XX_USART6_AS_UART_M
#ifdef STM32F2XX_USART6_BR_300
#define USART6_BR                          300
#elif defined STM32F2XX_USART6_BR_600
#define USART6_BR                          600
#elif defined STM32F2XX_USART6_BR_1200
#define USART6_BR                          1200
#elif defined STM32F2XX_USART6_BR_1800
#define USART6_BR                          1800
#elif defined STM32F2XX_USART6_BR_2400
#define USART6_BR                          2400
#elif defined STM32F2XX_USART6_BR_4800
#define USART6_BR                          4800
#elif defined STM32F2XX_USART6_BR_9600
#define USART6_BR                          9600
#elif defined STM32F2XX_USART6_BR_19200
#define USART6_BR                          19200
#elif defined STM32F2XX_USART6_BR_38400
#define USART6_BR                          38400
#elif defined STM32F2XX_USART6_BR_57600
#define USART6_BR                          57600
#elif defined STM32F2XX_USART6_BR_115200
#define USART6_BR                          115200
#elif defined STM32F2XX_USART6_BR_230400
#define USART6_BR                          230400
#elif defined STM32F2XX_USART6_BR_380400
#define USART6_BR                          380400
#elif defined STM32F2XX_USART6_BR_460800
#define USART6_BR                          460800
#elif defined STM32F2XX_USART6_BR_500000
#define USART6_BR                          500000
#elif defined STM32F2XX_USART6_BR_576000
#define USART6_BR                          576000
#elif defined STM32F2XX_USART6_BR_921600
#define USART6_BR                          921600
#elif defined STM32F2XX_USART6_BR_1000000
#define USART6_BR                          1000000
#elif defined STM32F2XX_USART6_BR_1152000
#define USART6_BR                          1152000
#elif defined STM32F2XX_USART6_BR_2000000
#define USART6_BR                          2000000
#elif defined STM32F2XX_USART6_BR_2500000
#define USART6_BR                          2500000
#elif defined STM32F2XX_USART6_BR_3000000
#define USART6_BR                          3000000
#elif defined STM32F2XX_USART6_BR_3500000
#define USART6_BR                          3500000
#elif defined STM32F2XX_USART6_BR_4000000
#define USART6_BR                          4000000
#else
#error "Unsupported baud rate for USART6"
#endif /* For USART6_BR */

#if defined STM32F2XX_USART6_DATABITS_8
#define USART6_DATABITS                    USART_WordLength_8b
#elif defined STM32F2XX_USART6_DATABITS_9
#define USART6_DATABITS                    USART_WordLength_9b
#else
#error "Unsupported databits for USART6"
#endif /* For USART6_DATABITS */

#if defined STM32F2XX_USART6_PARITY_NONE
#define USART6_PARITY                      USART_Parity_No
#elif defined STM32F2XX_USART6_PARITY_EVEN
#define USART6_PARITY                      USART_Parity_Even
#elif defined STM32F2XX_USART6_PARITY_ODD
#define USART6_PARITY                      USART_Parity_Odd
#else
#error "Unsupported parity mode for USART6"
#endif /* For USART6_PARITY */

#if defined STM32F2XX_USART6_STOPBITS_ONE
#define USART6_STOPBITS                    USART_StopBits_1
#elif defined STM32F2XX_USART6_STOPBITS_TWO
#define USART6_STOPBITS                    USART_StopBits_2
#else
#error "Unsupported stopbits for USART6"
#endif /* For USART6_STOPBITS */
#endif /* STM32F2XX_USART6_AS_UART_M */

void usart_init(GPIO_TypeDef *tx_port, uint32_t tx_pin, uint8_t tx_pinsrc,
                GPIO_TypeDef *rx_port, uint32_t rx_pin, uint8_t rx_pinsrc,
                uint8_t gpio_af,
                USART_TypeDef *port, uint32_t baudrate,
                uint16_t databits, uint16_t stopbits, uint16_t parity)
{
    USART_InitTypeDef usart_init;
    GPIO_InitTypeDef gpio_init;

    GPIO_PinAFConfig(tx_port, tx_pinsrc, gpio_af);

    gpio_init.GPIO_Speed = GPIO_Speed_50MHz;
    gpio_init.GPIO_Mode = GPIO_Mode_AF;
    gpio_init.GPIO_OType = GPIO_OType_PP;
    gpio_init.GPIO_PuPd = GPIO_PuPd_UP;
    //gpio_init.GPIO_PuPd =
    gpio_init.GPIO_Pin = tx_pin;
    GPIO_Init(tx_port, &gpio_init);

    GPIO_PinAFConfig(rx_port, rx_pinsrc, gpio_af);

    gpio_init.GPIO_Pin = rx_pin;
    gpio_init.GPIO_Mode = GPIO_Mode_AF;
    gpio_init.GPIO_OType = GPIO_OType_OD;
    GPIO_Init(rx_port, &gpio_init);

    usart_init.USART_WordLength = databits;
    usart_init.USART_StopBits = stopbits;
    usart_init.USART_Parity = parity;
    usart_init.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    usart_init.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    usart_init.USART_BaudRate = baudrate;
    USART_Init(port, &usart_init);

    USART_Cmd(port, ENABLE);    
    USART_ITConfig(port, USART_IT_RXNE, ENABLE);
}

#define CONCAT(a,b) a ## b
#define GPIO_Pin(n) CONCAT(GPIO_Pin_, n)
#define GPIO_PinSource(n) CONCAT(GPIO_PinSource, n)

void nos_uart_init(UINT8 port_num)
{
#ifdef STM32F2XX_USART1_AS_UART_M
    if (port_num == STM32F2XX_USART1)
    {
        usart_init(USART1_TX_PORT, GPIO_Pin(USART1_TX_PIN), GPIO_PinSource(USART1_TX_PIN),
                   USART1_RX_PORT, GPIO_Pin(USART1_RX_PIN), GPIO_PinSource(USART1_RX_PIN),
                   GPIO_AF_USART1,
                   USART1, USART1_BR, 
                   USART1_DATABITS, USART1_STOPBITS, USART1_PARITY);
        return;
    }
#endif

#ifdef STM32F2XX_USART2_AS_UART_M    
    if (port_num == STM32F2XX_USART2)
    {
        usart_init(USART2_TX_PORT, GPIO_Pin(USART2_TX_PIN), GPIO_PinSource(USART2_TX_PIN),
                   USART2_RX_PORT, GPIO_Pin(USART2_RX_PIN), GPIO_PinSource(USART2_RX_PIN),
                   GPIO_AF_USART2,
                   USART2, USART2_BR, 
                   USART2_DATABITS, USART2_STOPBITS, USART2_PARITY);
        return;
    }
#endif

#ifdef STM32F2XX_USART3_AS_UART_M
    if (port_num == STM32F2XX_USART3)
    {
        usart_init(USART3_TX_PORT, GPIO_Pin(USART3_TX_PIN), GPIO_PinSource(USART3_TX_PIN),
                   USART3_RX_PORT, GPIO_Pin(USART3_RX_PIN), GPIO_PinSource(USART3_RX_PIN),
                   GPIO_AF_USART3,
                   USART3, USART3_BR, 
                   USART3_DATABITS, USART3_STOPBITS, USART3_PARITY);
        return;
    }
#endif

#ifdef STM32F2XX_UART4_AS_UART_M
    if (port_num == STM32F2XX_UART4)
    {
        usart_init(UART4_TX_PORT, GPIO_Pin(UART4_TX_PIN), GPIO_PinSource(UART4_TX_PIN),
                   UART4_RX_PORT, GPIO_Pin(UART4_RX_PIN), GPIO_PinSource(UART4_RX_PIN),
                   GPIO_AF_UART4,
                   UART4, UART4_BR, 
                   UART4_DATABITS, UART4_STOPBITS, UART4_PARITY);
        return;
    }
#endif

#ifdef STM32F2XX_UART5_AS_UART_M
    if (port_num == STM32F2XX_UART5)
    {
        usart_init(UART5_TX_PORT, GPIO_Pin(UART5_TX_PIN), GPIO_PinSource(UART5_TX_PIN),
                   UART5_RX_PORT, GPIO_Pin(UART5_RX_PIN), GPIO_PinSource(UART6_RX_PIN),
                   GPIO_AF_UART5,
                   UART5, UART5_BR, 
                   UART5_DATABITS, UART5_STOPBITS, UART5_PARITY);
        return;
    }
#endif

#ifdef STM32F2XX_USART6_AS_UART_M
    if (port_num == STM32F2XX_USART6)
    {
        usart_init(USART6_TX_PORT, GPIO_Pin(USART6_TX_PIN), GPIO_PinSource(USART6_TX_PIN),
                   USART6_RX_PORT, GPIO_Pin(USART6_RX_PIN), GPIO_PinSource(USART6_RX_PIN),
                   GPIO_AF_USART6,
                   USART6, USART6_BR, 
                   USART6_DATABITS, USART6_STOPBITS, USART6_PARITY);
        return;
    }
#endif
}

#endif	// UART_M
