// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Realtime counter
 *
 * @author Jongsoo Jeong (ETRI)
 * @author Haeyong Kim (ETRI)
 * @date 2013. 2. 22.
 */

#include "rtc.h"

#include "stm32f2xx_rcc.h"
#include "stm32f2xx_rtc.h"
#include "stm32f2xx_exti.h"

#ifdef __CC_ARM
#include "cyassl_MDK_ARM.h"
#endif

#ifdef KERNEL_M
#include "hal_sched.h"
#endif

#define INITMODE_TIMEOUT         ((uint32_t) 0xffffff)
#define RTC_INIT_MASK           ((uint32_t)0xFFFFFFFF)  

extern void nos_rtc_init_fail(void);

void nos_rtc_init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure; 
    EXTI_InitTypeDef EXTI_InitStructure;
    RTC_InitTypeDef RTC_InitStructure;
    RTC_TimeTypeDef RTC_Time ;
    RTC_DateTypeDef RTC_Date ;

    /* Enable the PWR clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

    /* Allow access to RTC */
    PWR_BackupAccessCmd(ENABLE);

    /* LSE used as RTC source clock */
    RCC_LSEConfig(RCC_LSE_ON);  
    while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET);
    RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
   
    /* Enable the RTC Clock */
    RCC_RTCCLKCmd(ENABLE);

    /* Wait for RTC APB registers synchronisation */
    RTC_WaitForSynchro();

    /* Calendar Configuration with LSE supposed at 32kHz */
    RTC_InitStructure.RTC_AsynchPrediv = 0x7F; /* 32kHz / (127 + 1) = 256Hz (ck_apre) */
    RTC_InitStructure.RTC_SynchPrediv = 0xFF;  /* 256Hz / (255 + 1) = 1Hz (ck_spre) */
    RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
    RTC_Init(&RTC_InitStructure);
    
    RTC_GetTime(RTC_Format_BIN, &RTC_Time);
    RTC_GetDate(RTC_Format_BIN, &RTC_Date);
    
#ifdef KERNEL_M
    /* EXTI configuration */
    EXTI_ClearITPendingBit(EXTI_Line22);
    EXTI_InitStructure.EXTI_Line = EXTI_Line22;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
  
    /* Enable the RTC Wakeup Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = RTC_WKUP_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* Configure the RTC WakeUp Clock source: RTCCLK/2 (16384Hz) */
    RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div2);
    
    /* When the wakeup function is enabled, the down-counting remains active in
       low power modes. In addition, when it reaches 0, the WUTF flag is set in
       the RTC_ISR register, and the wakeup counter is automatically reloaded
       with its reload value (RTC_WUTR register value). */
    RTC_SetWakeUpCounter(16384 * SCHED_TIMER_MS / 1000);

    /* Enable the RTC Wakeup Interrupt */
    RTC_ITConfig(RTC_IT_WUT, ENABLE);
  
    /* Enable Wakeup Counter */
    RTC_WakeUpCmd(ENABLE);

    if (RTC_GetWakeUpCounter() != (16384 * SCHED_TIMER_MS / 1000))
    {
        nos_rtc_init_fail();
    }

    RTC_ClearITPendingBit(RTC_IT_WUT);
    EXTI_ClearITPendingBit(EXTI_Line22);
#endif
}

#ifdef __CC_ARM
struct tm *Cyassl_MDK_gmtime(const time_t *c) 
{
    RTC_TimeTypeDef RTC_Time;
    RTC_DateTypeDef RTC_Date;
    static struct tm date; 

    RTC_GetTime(RTC_Format_BIN, &RTC_Time) ;
    RTC_GetDate(RTC_Format_BIN, &RTC_Date) ;

    date.tm_year = RTC_Date.RTC_Year + 100 ;
    date.tm_mon = RTC_Date.RTC_Month - 1 ;
    date.tm_mday = RTC_Date.RTC_Date ;
    date.tm_hour = RTC_Time.RTC_Hours ;
    date.tm_min = RTC_Time.RTC_Minutes ;
    date.tm_sec = RTC_Time.RTC_Seconds ;

    return(&date) ;
}
#endif
