// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_getc.c
 * UART input for STM32F2xx.
 *
 * @author Jongsoo Jeong (ETRI)
 * @author Haeyong Kim (ETRI)
 * @date 2013. 2. 22.
 */

#include "kconf.h"
#ifdef UART_M

#include "uart.h"
#include "stm32f2xx.h"
#include "stm32f2xx_usart.h"
#include "critical_section.h"

// Callback functions
#ifdef STM32F2XX_USART1_AS_UART_M
void (*nos_usart1_rx_callback)(char) =  NULL;
#endif
#ifdef STM32F2XX_USART2_AS_UART_M
void (*nos_usart2_rx_callback)(char) =  NULL;
#endif
#ifdef STM32F2XX_USART3_AS_UART_M
void (*nos_usart3_rx_callback)(char) =  NULL;
#endif
#ifdef STM32F2XX_UART4_AS_UART_M
void (*nos_uart4_rx_callback)(char) =  NULL;
#endif
#ifdef STM32F2XX_UART5_AS_UART_M
void (*nos_uart5_rx_callback)(char) =  NULL;
#endif
#ifdef STM32F2XX_USART6_AS_UART_M
void (*nos_usart6_rx_callback)(char) = NULL;
#endif

volatile char uart_rx_char;

void nos_uart_set_getc_callback(UINT8 port_num, void (*func)(char))
{
#ifdef STM32F2XX_USART1_AS_UART_M
    if (port_num == STM32F2XX_USART1)
    {
        nos_usart1_rx_callback = func;
        return;
    }
#endif

#ifdef STM32F2XX_USART2_AS_UART_M
    if (port_num == STM32F2XX_USART2)
    {
        nos_usart2_rx_callback = func;
        return;
    }
#endif

#ifdef STM32F2XX_USART3_AS_UART_M
    if (port_num == STM32F2XX_USART3)
    {
        nos_usart3_rx_callback = func;
        return;
    }
#endif

#ifdef STM32F2XX_UART4_AS_UART_M
    if (port_num == STM32F2XX_UART4)
    {
        nos_uart4_rx_callback = func;
        return;
    }
#endif

#ifdef STM32F2XX_UART5_AS_UART_M
    if (port_num == STM32F2XX_UART5)
    {
        nos_uart5_rx_callback = func;
        return;
    }
#endif

#ifdef STM32F2XX_USART6_AS_UART_M
    if (port_num == STM32F2XX_USART6)
    {
        nos_usart6_rx_callback = func;
        return;
    }
#endif
}

void (*nos_uart_get_getc_callback(UINT8 port_num))(char)
{
#ifdef STM32F2XX_USART1_AS_UART_M
    if (port_num == STM32F2XX_USART1)
        return nos_usart1_rx_callback;
#endif

#ifdef STM32F2XX_USART2_AS_UART_M
    if (port_num == STM32F2XX_USART2)
        return nos_usart2_rx_callback;
#endif

#ifdef STM32F2XX_USART3_AS_UART_M
    if (port_num == STM32F2XX_USART3)
        return nos_usart3_rx_callback;
#endif

#ifdef STM32F2XX_UART4_AS_UART_M
    if (port_num == STM32F2XX_UART4)
        return nos_uart4_rx_callback;
#endif

#ifdef STM32F2XX_UART5_AS_UART_M
    if (port_num == STM32F2XX_UART5)
        return nos_uart5_rx_callback;
#endif

#ifdef STM32F2XX_USART6_AS_UART_M
    if (port_num == STM32F2XX_USART6)
        return nos_usart6_rx_callback;
#endif

    return NULL;
}

void nos_uart_enable_rx_intr(UINT8 port_num)
{
#ifdef STM32F2XX_USART1_AS_UART_M
    if (port_num == STM32F2XX_USART1)
    {
        NVIC_EnableIRQ(USART1_IRQn);
        return;
    }
#endif
        
#ifdef STM32F2XX_USART2_AS_UART_M
    if (port_num == STM32F2XX_USART2)
    {
        NVIC_EnableIRQ(USART2_IRQn);
        return;
}
#endif

#ifdef STM32F2XX_USART3_AS_UART_M
    if (port_num == STM32F2XX_USART3)
    {
        NVIC_EnableIRQ(USART3_IRQn);
        return;
}
#endif

#ifdef STM32F2XX_UART4_AS_UART_M
    if (port_num == STM32F2XX_UART4)
    {
        NVIC_EnableIRQ(UART4_IRQn);
        return;
    }
#endif

#ifdef STM32F2XX_UART5_AS_UART_M
    if (port_num == STM32F2XX_UART5)
    {
        NVIC_EnableIRQ(UART5_IRQn);
        return;
    }
#endif
        
#ifdef STM32F2XX_USART6_AS_UART_M
    if (port_num == STM32F2XX_USART6)
    {
        NVIC_EnableIRQ(USART6_IRQn);
        return;
    }
#endif
        
}

void nos_uart_disable_rx_intr(UINT8 port_num)
{
#ifdef STM32F2XX_USART1_AS_UART_M
    if (port_num == STM32F2XX_USART1)
    {
        NVIC_DisableIRQ(USART1_IRQn);
        return;
    }
#endif
        
#ifdef STM32F2XX_USART2_AS_UART_M
    if (port_num == STM32F2XX_USART2)
    {
        NVIC_DisableIRQ(USART2_IRQn);
        return;
}
#endif
        
#ifdef STM32F2XX_USART3_AS_UART_M
    if (port_num == STM32F2XX_USART3)
    {
        NVIC_DisableIRQ(USART3_IRQn);
        return;
    }
#endif
        
#ifdef STM32F2XX_UART4_FOR_UART_ENABLE
    if (port_num == STM32F2XX_UART4)
    {
        NVIC_DisableIRQ(UART4_IRQn);
        return;
    }
#endif
        
#ifdef STM32F2XX_UART5_FOR_UART_ENABLE
    if (port_num == STM32F2XX_UART5)
    {
        NVIC_DisableIRQ(UART5_IRQn);
        return;
    }
#endif
        
#ifdef STM32F2XX_USART6_AS_UART_M
    if (port_num == STM32F2XX_USART6)
    {
        NVIC_DisableIRQ(USART6_IRQn);
        return;
    }
#endif
}

BOOL nos_uart_rx_intr_is_set(UINT8 port_num)
{
#ifdef STM32F2XX_USART1_AS_UART_M
    if (port_num == STM32F2XX_USART1)
    {
        return ((NVIC->ISER[((uint32_t)(USART1_IRQn) >> 5)] &
                 (1 << ((uint32_t)(USART1_IRQn) & 0x1F))) != 0);
    }
#endif

    #ifdef STM32F2XX_USART1_AS_UART_M
    if (port_num == STM32F2XX_USART2)
    {
        return ((NVIC->ISER[((uint32_t)(USART2_IRQn) >> 5)] &
                 (1 << ((uint32_t)(USART2_IRQn) & 0x1F))) != 0);
    }
#endif

#ifdef STM32F2XX_USART3_AS_UART_M
    if (port_num == STM32F2XX_USART3)
    {
        return ((NVIC->ISER[((uint32_t)(USART3_IRQn) >> 5)] &
                 (1 << ((uint32_t)(USART3_IRQn) & 0x1F))) != 0);
    }
#endif

#ifdef STM32F2XX_UART4_AS_UART_M
    if (port_num == STM32F2XX_UART4)
    {
        return ((NVIC->ISER[((uint32_t)(UART4_IRQn) >> 5)] &
                 (1 << ((uint32_t)(UART4_IRQn) & 0x1F))) != 0);
    }
#endif

#ifdef STM32F2XX_UART5_AS_UART_M
    if (port_num == STM32F2XX_UART5)
    {
        return ((NVIC->ISER[((uint32_t)(UART5_IRQn) >> 5)] &
                 (1 << ((uint32_t)(UART5_IRQn) & 0x1F))) != 0);
    }
#endif


#ifdef STM32F2XX_USART6_AS_UART_M
    if (port_num == STM32F2XX_USART6)
    {
        return ((NVIC->ISER[((uint32_t)(USART6_IRQn) >> 5)] &
                 (1 << ((uint32_t)(USART6_IRQn) & 0x1F))) != 0);
    }
#endif

    return FALSE;
}

#ifdef STM32F2XX_USART1_AS_UART_M
void USART1_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
    {
        uart_rx_char = (char) USART_ReceiveData(USART1);
        if (nos_usart1_rx_callback != NULL)
            nos_usart1_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef STM32F2XX_USART2_AS_UART_M
void USART2_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
    {
        uart_rx_char = (char) USART_ReceiveData(USART2);
        if (nos_usart2_rx_callback != NULL)
            nos_usart2_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef STM32F2XX_USART3_AS_UART_M
void USART3_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_GetITStatus(USART3, USART_IT_RXNE) == SET)
    {
        uart_rx_char = (char) USART_ReceiveData(USART3);
        if (nos_usart3_rx_callback != NULL)
            nos_usart3_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef STM32F2XX_UART4_AS_UART_M
void UART4_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_GetITStatus(UART4, USART_IT_RXNE) == SET)
    {
        uart_rx_char = (char) USART_ReceiveData(UART4);
        if (nos_uart4_rx_callback != NULL)
            nos_uart4_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef STM32F2XX_UART5_AS_UART_M
void UART5_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_GetITStatus(UART5, USART_IT_RXNE) == SET)
    {
        uart_rx_char = (char) USART_ReceiveData(UART5);
        if (nos_uart5_rx_callback != NULL)
            nos_uart5_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef STM32F2XX_USART6_AS_UART_M
void USART6_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_GetITStatus(USART6, USART_IT_RXNE) == SET)
    {
        uart_rx_char = (char) USART_ReceiveData(USART6);
        if (nos_usart6_rx_callback != NULL)
            nos_usart6_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#endif // UART_M
