/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief RTC and scheduling timer
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 9. 7.
 */

/*
 * $Id: spi.c 930 2012-09-03 05:47:09Z jsjeong $
 * $LastChangedDate: 2012-09-03 14:47:09 +0900 (월, 03 9월 2012) $
 */

#include "timer.h"
#if defined(KERNEL_M) || defined(RTC_M)

#include "em_cmu.h"
#include "em_rtc.h"

#ifdef RTC_M
#define MAX_CNT_VALUE CMU_ClockFreqGet(cmuClock_RTC)
#else
#define MAX_CNT_VALUE 0xFFFFFF
#endif /* RTC_M */

#ifdef KERNEL_M
#include "hal_sched.h"
extern void systick_handler(void);
#endif /* KERNEL_M */

#ifdef RTC_M
extern void rtc_handler(void);
#endif /* RTC_M */

void nos_timer_init(void)
{
    RTC_Init_TypeDef rtc_init;

    //CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_32);
    CMU_ClockEnable(cmuClock_RTC, true);

    NVIC_SetPriority(RTC_IRQn, 0);
    NVIC_EnableIRQ(RTC_IRQn);

    rtc_init.enable = false;
    rtc_init.debugRun = false;
#ifdef RTC_M
    rtc_init.comp0Top = true;
#else
    rtc_init.comp0Top = false;
#endif /* RTC_M */

    RTC_Init(&rtc_init);

#ifdef RTC_M
    RTC_CompareSet(0, MAX_CNT_VALUE);
    RTC_IntEnable(RTC_IF_COMP0);
    RTC_Enable(true);
#endif /* RTC_M */
}

void RTC_IRQHandler(void)
{
#ifdef KERNEL_M
    if (RTC_IntGet() & RTC_IF_COMP1)
    {
        RTC_IntClear(RTC_IF_COMP1);
        RTC_CompareSet(1, (RTC_CounterGet() + (CMU_ClockFreqGet(cmuClock_RTC) *
                SCHED_TIMER_MS / 1000)) % MAX_CNT_VALUE);
        systick_handler();
    }
#endif /* KERNEL_M */

#ifdef RTC_M
    if (RTC_IntGet() & RTC_IF_COMP0)
    {
        RTC_IntClear(RTC_IF_COMP0);
        rtc_handler();
    }
#endif /* RTC_M */
}

#endif /* KERNEL_M || RTC_M */
