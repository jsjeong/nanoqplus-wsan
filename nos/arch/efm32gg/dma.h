/**************************************************************************//**
* @file le_uart.h
* @brief LE_UART utility routine.
* @author NURI Telecom Co., Ltd.
* @version v1.0 (TripleK / 11.08.10)
******************************************************************************
* @section License
* <b>(C) Copyright 2011 by NURI Corporation, http://www.nuritelecom.com</b>
******************************************************************************
*
*
*****************************************************************************/
#ifndef __DMA_H__
#define __DMA_H__                      1

#include "kconf.h"
#ifdef DMA_M
#include "em_dma.h"

/* DMA control block, must be aligned to 256. */
#if defined (__ICCARM__)
#pragma data_alignment=256
extern DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMA_CHAN_COUNT * 2];
#elif defined (__CC_ARM)
extern DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMA_CHAN_COUNT * 2] __attribute__ ((aligned(256)));
#elif defined (__GNUC__)
extern DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMA_CHAN_COUNT * 2] __attribute__ ((aligned(256)));
#else
#error Undefined toolkit, need to define alignment
#endif

extern DMA_Init_TypeDef dmaInit;

void nos_dma_init(void);
#endif /* DMA_M */
#endif //__DMA_H__
