/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Low-Energy Sensor Interface (LESENSE) for EFM32GG.
 * @date 2012. 9. 8.
 * @author Jongsoo Jeong (ETRI)
 */

/*
 * $LastChangedDate: 2012-09-07 20:09:55 +0900 (금, 07 9월 2012) $
 * $Id: pwr_mgr.c 951 2012-09-07 11:09:55Z jsjeong $
 */


#include "lesense.h"

#ifdef EM_LESENSOR_M
#include "em_cmu.h"
#include "em_lesense.h"
#include "em_acmp.h"
#include "em_gpio.h"
#include <string.h>

#ifdef LESENSOR_LIGHT_M
#include "lesensor-light-conf.h"
#endif /* LESENSOR_LIGHT_M */

extern void nos_lesense_light_event_handler(UINT8 sensor_idx);

#define LESENSE_SET_CONF_DISABLED(ch)\
    do {\
        (ch).enaScanCh = false;\
        (ch).enaPin = false;\
        (ch).enaInt = false;\
        (ch).chPinExMode = lesenseChPinExDis;\
        (ch).chPinIdleMode = lesenseChPinIdleDis;\
        (ch).useAltEx = false;\
        (ch).shiftRes = false;\
        (ch).invRes = false;\
        (ch).storeCntRes = false;\
        (ch).exClk = lesenseClkLF;\
        (ch).sampleClk = lesenseClkLF;\
        (ch).exTime = 0x00U;\
        (ch).sampleDelay = 0x00U;\
        (ch).measDelay = 0x00U;\
        (ch).acmpThres = 0x00U;\
        (ch).sampleMode = lesenseSampleModeCounter;\
        (ch).intMode = lesenseSetIntNone;\
        (ch).cntThres = 0x0000U;\
        (ch).compMode = lesenseCompModeLess;\
    } while(0)

#define LESENSE_SET_ALTEX_DISABLED(ex)\
    do {\
    	(ex).enablePin = false;\
        (ex).idleConf = lesenseAltExPinIdleDis;\
        (ex).alwaysEx = false;\
    } while(0)

void nos_lesense_init(void)
{
    // Initial structures.
    ACMP_Init_TypeDef init_acmp;
    LESENSE_Init_TypeDef init_lesense;
    LESENSE_ChAll_TypeDef init_chs;
    LESENSE_ConfAltEx_TypeDef init_alt_ex;

    /* Enable clock for ACMP0. */
    CMU_ClockEnable(cmuClock_ACMP0, true);
    /* Enable clock for LESENSE. */
    CMU_ClockEnable(cmuClock_LESENSE, true);
    /* Enable clock divider for LESENSE. */
    CMU_ClockDivSet(cmuClock_LESENSE, cmuClkDiv_1);

    init_acmp.fullBias = false;
    init_acmp.halfBias = true;
    init_acmp.biasProg = 0x0;
    init_acmp.interruptOnFallingEdge = false;
    init_acmp.interruptOnRisingEdge = false;
    init_acmp.warmTime = acmpWarmTime512;
    init_acmp.hysteresisLevel = acmpHysteresisLevel5;
    init_acmp.inactiveValue = false;
    init_acmp.lowPowerReferenceEnabled = false;
    init_acmp.vddLevel = 0x00;
    init_acmp.enable = false;

    ACMP_Init(ACMP0, &init_acmp);
    ACMP_GPIOSetup(ACMP0, 0, false, false);
    ACMP_ChannelSet(ACMP0, acmpChannelVDD, acmpChannel0);

    init_lesense.coreCtrl.scanStart = lesenseScanStartPeriodic;
    init_lesense.coreCtrl.prsSel = lesensePRSCh0;
    init_lesense.coreCtrl.scanConfSel = lesenseScanConfDirMap;
    init_lesense.coreCtrl.invACMP0 = false;
    init_lesense.coreCtrl.invACMP1 = false;
    init_lesense.coreCtrl.dualSample = false;
    init_lesense.coreCtrl.storeScanRes = false;
    init_lesense.coreCtrl.bufOverWr = true;
    init_lesense.coreCtrl.bufTrigLevel = lesenseBufTrigHalf;
    init_lesense.coreCtrl.wakeupOnDMA = lesenseDMAWakeUpDisable;
    init_lesense.coreCtrl.biasMode = lesenseBiasModeDutyCycle;
    init_lesense.coreCtrl.debugRun = false;

    init_lesense.timeCtrl.startDelay = 0U;

    init_lesense.perCtrl.dacCh0Data = lesenseDACIfData;
    init_lesense.perCtrl.dacCh0ConvMode = lesenseDACConvModeDisable;
    init_lesense.perCtrl.dacCh0OutMode = lesenseDACOutModeDisable;
    init_lesense.perCtrl.dacCh1Data = lesenseDACIfData;
    init_lesense.perCtrl.dacCh1ConvMode = lesenseDACConvModeDisable;
    init_lesense.perCtrl.dacCh1OutMode = lesenseDACOutModeDisable;
    init_lesense.perCtrl.dacPresc = 0U;
    init_lesense.perCtrl.dacRef = lesenseDACRefBandGap;
    init_lesense.perCtrl.acmp0Mode = lesenseACMPModeMuxThres;
    init_lesense.perCtrl.acmp1Mode = lesenseACMPModeDisable;
    init_lesense.perCtrl.warmupMode = lesenseWarmupModeNormal;

    init_lesense.decCtrl.decInput = lesenseDecInputSensorSt;
    init_lesense.decCtrl.initState = 0U;
    init_lesense.decCtrl.chkState = false;
    init_lesense.decCtrl.intMap = true;
    init_lesense.decCtrl.hystPRS0 = false;
    init_lesense.decCtrl.hystPRS1 = false;
    init_lesense.decCtrl.hystPRS2 = false;
    init_lesense.decCtrl.hystIRQ = false;
    init_lesense.decCtrl.prsCount = true;
    init_lesense.decCtrl.prsChSel0 = lesensePRSCh0;
    init_lesense.decCtrl.prsChSel1 = lesensePRSCh1;
    init_lesense.decCtrl.prsChSel2 = lesensePRSCh2;
    init_lesense.decCtrl.prsChSel3 = lesensePRSCh3;

    /* Configure scan channels. */
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[0]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[1]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[2]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[3]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[4]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[5]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[6]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[7]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[8]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[9]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[10]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[11]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[12]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[13]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[14]);
    LESENSE_SET_CONF_DISABLED(init_chs.Ch[15]);

    /* Configure alternate excitation channels. */
    init_alt_ex.altExMap = lesenseAltExMapALTEX;
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[0]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[1]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[2]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[3]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[4]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[5]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[6]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[7]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[8]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[9]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[10]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[11]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[12]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[13]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[14]);
    LESENSE_SET_ALTEX_DISABLED(init_alt_ex.AltEx[15]);

#ifdef LESENSOR_LIGHT_M
    nos_lesensor_light_config(&init_lesense,
            &init_chs.Ch[LIGHT_SENSE_LESENSE_CHANNEL],
            &init_alt_ex.AltEx[LIGHT_EXCITE_LESENSE_ALTEX_ID]);
#endif /* LESENSOR_LIGHT_M */

    LESENSE_Init(&init_lesense, true);
    LESENSE_ChannelAllConfig(&init_chs);
    LESENSE_AltExConfig(&init_alt_ex);

    /* Set scan frequency (in Hz). */
    LESENSE_ScanFreqSet(0U, 20U);

    /* Set clock divisor for LF clock. */
    LESENSE_ClkDivSet(lesenseClkLF, lesenseClkDiv_1);

    LESENSE_ScanStart();
    NVIC_EnableIRQ(LESENSE_IRQn);
}

#endif /* EM_LESENSOR_M */
