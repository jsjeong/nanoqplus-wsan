// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_getc.c
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 2. 23.
 */

#include "kconf.h"
#ifdef UART_M

#include "uart.h"
#include "critical_section.h"
#include "em_usart.h"

#if defined EFM32GG_LEUART0_FOR_UART_M ||\
    defined EFM32GG_LEUART1_FOR_UART_M
#include "em_leuart.h"
#include "dma.h"
#endif

#include "led.h"

volatile char uart_rx_char;

#ifdef EFM32GG_USART0_FOR_UART_M
void (*nos_usart0_rx_callback)(UINT8, char) = NULL;
#endif
#ifdef EFM32GG_USART1_FOR_UART_M
void (*nos_usart1_rx_callback)(UINT8, char) = NULL;
#endif
#ifdef EFM32GG_USART2_FOR_UART_M
void (*nos_usart2_rx_callback)(UINT8, char) = NULL;
#endif
#ifdef EFM32GG_UART0_FOR_UART_M
void (*nos_uart0_rx_callback)(UINT8, char) = NULL;
#endif
#ifdef EFM32GG_UART1_FOR_UART_M
void (*nos_uart1_rx_callback)(UINT8, char) = NULL;
#endif
#ifdef EFM32GG_LEUART0_FOR_UART_M
void (*nos_leuart0_rx_callback)(UINT8, char) = NULL;
#endif
#ifdef EFM32GG_LEUART1_FOR_UART_M
void (*nos_leuart1_rx_callback)(UINT8, char) = NULL;
#endif

void (*dummy_rx_callback)(char) = NULL;

#ifdef EFM32GG_LEUART0_FOR_UART_M
char rxdata_leuart0;
#endif

#ifdef EFM32GG_LEUART1_FOR_UART_M
char rxdata_leuart1;
#endif

void nos_uart_set_getc_callback(UINT8 port_num, void (*func)(UINT8, char))
{
#ifdef EFM32GG_USART0_FOR_UART_M
    if (port_num == EFM32GG_USART0)
    {
        nos_usart0_rx_callback = func;
        return;
    }
#endif

#ifdef EFM32GG_USART1_FOR_UART_M
    if (port_num == EFM32GG_USART1)
    {
        nos_usart1_rx_callback = func;
        return;
    }
#endif

#ifdef EFM32GG_USART2_FOR_UART_M
    if (port_num == EFM32GG_USART2)
    {
        nos_usart2_rx_callback = func;
        return;
    }
#endif

#ifdef EFM32GG_UART0_FOR_UART_M
    if (port_num == EFM32GG_UART0)
    {
        nos_uart0_rx_callback = func;
        return;
    }
#endif

#ifdef EFM32GG_UART1_FOR_UART_M
    if (port_num == EFM32GG_UART1)
    {
        nos_uart1_rx_callback = func;
        return;
    }
#endif

#ifdef EFM32GG_LEUART0_FOR_UART_M
    if (port_num == EFM32GG_LEUART0)
    {
        nos_leuart0_rx_callback = func;
        return;
    }
#endif

#ifdef EFM32GG_LEUART1_FOR_UART_M
    if (port_num == EFM32GG_LEUART1)
    {
        nos_leuart1_rx_callback = func;
        return;
    }
#endif

}

void (*nos_uart_get_getc_callback(UINT8 port_num))(UINT8, char)
{
#ifdef EFM32GG_USART0_FOR_UART_M
    if (port_num == EFM32GG_USART0)
    {
        return nos_usart0_rx_callback;
    }
#endif

#ifdef EFM32GG_USART1_FOR_UART_M
    if (port_num == EFM32GG_USART1)
    {
        return nos_usart1_rx_callback;
    }
#endif

#ifdef EFM32GG_USART2_FOR_UART_M
    if (port_num == EFM32GG_USART2)
    {
        return nos_usart2_rx_callback;
    }
#endif

#ifdef EFM32GG_UART0_FOR_UART_M
    if (port_num == EFM32GG_UART0)
    {
        return nos_uart0_rx_callback;
    }
#endif

#ifdef EFM32GG_UART1_FOR_UART_M
    if (port_num == EFM32GG_UART1)
    {
        return nos_uart1_rx_callback;
    }
#endif

#ifdef EFM32GG_LEUART0_FOR_UART_M
    if (port_num == EFM32GG_LEUART0)
    {
        return nos_leuart0_rx_callback;
    }
#endif

#ifdef EFM32GG_LEUART1_FOR_UART_M
    if (port_num == EFM32GG_LEUART1)
    {
        return nos_leuart1_rx_callback;
    }
#endif

    return NULL;
}

void nos_uart_enable_rx_intr(UINT8 port_num)
{
#ifdef EFM32GG_USART0_FOR_UART_M
    if (port_num == EFM32GG_USART0)
    {
        NVIC_EnableIRQ(USART0_RX_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_USART1_FOR_UART_M
    if (port_num == EFM32GG_USART1)
    {
        NVIC_EnableIRQ(USART1_RX_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_USART2_FOR_UART_M
    if (port_num == EFM32GG_USART2)
    {
        NVIC_EnableIRQ(USART2_RX_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_UART0_FOR_UART_M
    if (port_num == EFM32GG_UART0)
    {
        NVIC_EnableIRQ(UART0_RX_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_UART1_FOR_UART_M
    if (port_num == EFM32GG_UART1)
    {
        NVIC_EnableIRQ(UART1_RX_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_LEUART0_FOR_UART_M
    if (port_num == EFM32GG_LEUART0)
    {
        NVIC_EnableIRQ(LEUART0_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_LEUART1_FOR_UART_M
    if (port_num == EFM32GG_LEUART1)
    {
        NVIC_EnableIRQ(LEUART1_IRQn);
        return;
    }
#endif
}

void nos_uart_disable_rx_intr(UINT8 port_num)
{
#ifdef EFM32GG_USART0_FOR_UART_M
    if (port_num == EFM32GG_USART0)
    {
        NVIC_DisableIRQ(USART0_RX_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_USART1_FOR_UART_M
    if (port_num == EFM32GG_USART1)
    {   
        NVIC_DisableIRQ(USART1_RX_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_USART2_FOR_UART_M
    if (port_num == EFM32GG_USART2)
    {
        NVIC_DisableIRQ(USART2_RX_IRQn);
        return;
    }
#endif
        
#ifdef EFM32GG_UART0_FOR_UART_M
    if (port_num == EFM32GG_UART0)
    {
        NVIC_DisableIRQ(UART0_RX_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_UART1_FOR_UART_M
    if (port_num == EFM32GG_UART1)
    {
        NVIC_DisableIRQ(UART1_RX_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_LEUART0_FOR_UART_M
    if (port_num == EFM32GG_LEUART0)
    {
        NVIC_DisableIRQ(LEUART0_IRQn);
        return;
    }
#endif

#ifdef EFM32GG_LEUART1_FOR_UART_M
    if (port_num == EFM32GG_LEUART1)
    {
        NVIC_DisableIRQ(LEUART1_IRQn);
        return;
    }
#endif
}


BOOL nos_uart_rx_intr_is_set(UINT8 port_num)
{
#ifdef EFM32GG_USART0_FOR_UART_M
    if (port_num == EFM32GG_USART0)
        return ((NVIC->ISER[((uint32_t)(USART0_RX_IRQn) >> 5)] &
                 (1 << ((uint32_t)(USART0_RX_IRQn)))) != 0);
#endif

#ifdef EFM32GG_USART1_FOR_UART_M
    if (port_num == EFM32GG_USART1)
        return ((NVIC->ISER[((uint32_t)(USART1_RX_IRQn) >> 5)] &
                 (1 << ((uint32_t)(USART1_RX_IRQn)))) != 0);
#endif

#ifdef EFM32GG_USART2_FOR_UART_M
    if (port_num == EFM32GG_USART2)
        return ((NVIC->ISER[((uint32_t)(USART2_RX_IRQn) >> 5)] &
                 (1 << ((uint32_t)(USART2_RX_IRQn)))) != 0);
#endif

#ifdef EFM32GG_UART0_FOR_UART_M
    if (port_num == EFM32GG_UART0)
        return ((NVIC->ISER[((uint32_t)(UART0_RX_IRQn) >> 5)] &
                 (1 << ((uint32_t)(UART0_RX_IRQn)))) != 0);
#endif

#ifdef EFM32GG_UART1_FOR_UART_M
    if (port_num == EFM32GG_UART1)
        return ((NVIC->ISER[((uint32_t)(UART1_RX_IRQn) >> 5)] &
                 (1 << ((uint32_t)(UART1_RX_IRQn)))) != 0);
#endif

#ifdef EFM32GG_LEUART0_FOR_UART_M
    if (port_num == EFM32GG_LEUART0)
        return ((NVIC->ISER[((uint32_t)(LEUART0_IRQn) >> 5)] &
                 (1 << ((uint32_t)(LEUART0_IRQn)))) != 0);
#endif

#ifdef EFM32GG_LEUART1_FOR_UART_M
    if (port_num == EFM32GG_LEUART1)
        return ((NVIC->ISER[((uint32_t)(LEUART1_IRQn) >> 5)] &
                 (1 << ((uint32_t)(LEUART1_IRQn)))) != 0);
#endif
    
    return FALSE;
}

#ifdef EFM32GG_USART0_FOR_UART_M
void USART0_RX_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_IntGet(USART0) & USART_IF_RXDATAV)
    {
        USART_IntClear(USART0, USART_IF_RXDATAV);
        uart_rx_char = USART_Rx(USART0);
        if (nos_usart0_rx_callback)
            nos_usart0_rx_callback(EFM32GG_USART0, uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef EFM32GG_USART1_FOR_UART_M
void USART1_RX_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_IntGet(USART1) & USART_IF_RXDATAV)
    {
        USART_IntClear(USART1, USART_IF_RXDATAV);
        uart_rx_char = USART_Rx(USART1);
        if (nos_usart1_rx_callback)
            nos_usart1_rx_callback(EFM32GG_USART1, uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef EFM32GG_USART2_FOR_UART_M
void USART2_RX_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_IntGet(USART2) & USART_IF_RXDATAV)
    {
        USART_IntClear(USART2, USART_IF_RXDATAV);
        uart_rx_char = USART_Rx(USART2);
        if (nos_usart2_rx_callback)
            nos_usart2_rx_callback(EFM32GG_USART2, uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef EFM32GG_UART0_FOR_UART_M
void UART0_RX_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_IntGet(UART0) & USART_IF_RXDATAV)
    {
        uart_rx_char = USART_Rx(UART0);
        if (nos_uart0_rx_callback)
            nos_uart0_rx_callback(EFM32GG_UART0, uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef EFM32GG_UART1_FOR_UART_M
void UART1_RX_IRQHandler(void)
{
    NOS_ENTER_ISR();
    if (USART_IntGet(UART1) & UART_IF_RXDATAV)
    {
        uart_rx_char = USART_Rx(UART1);
        if (nos_uart1_rx_callback)
            nos_uart1_rx_callback(EFM32GG_UART1, uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef EFM32GG_LEUART0_FOR_UART_M
void LEUART0_IRQHandler(void)
{
    UINT32 intflag;
    
    NOS_ENTER_ISR();
    intflag = LEUART_IntGet(LEUART0);
    LEUART_IntClear(LEUART0, intflag);

    uart_rx_char = rxdata_leuart0;

    /* Starting the transfer. Using Basic Mode */
    DMA_ActivateBasic(DMA_LE_UART0_RX,  /* Activate channel selected */
                      true,             /* Use primary descriptor */
                      false,            /* No DMA burst */
                      NULL,             /* Destination address */
                      NULL,             /* Source address*/
                      0);               /* Size of buffer minus1 */

    if (nos_leuart0_rx_callback)
        nos_leuart0_rx_callback(EFM32GG_LEUART0, uart_rx_char);

    NOS_EXIT_ISR();
}
#endif

#ifdef EFM32GG_LEUART1_FOR_UART_M
void LEUART1_IRQHandler(void)
{
    UINT32 intflag;
    
    NOS_ENTER_ISR();
    intflag = LEUART_IntGet(LEUART1);
    LEUART_IntClear(LEUART1, intflag);

    uart_rx_char = rxdata_leuart1;

    /* Starting the transfer. Using Basic Mode */
    DMA_ActivateBasic(DMA_LE_UART1_RX,  /* Activate channel selected */
                      true,             /* Use primary descriptor */
                      false,            /* No DMA burst */
                      NULL,             /* Destination address */
                      NULL,             /* Source address*/
                      0);               /* Size of buffer minus1 */

    if (nos_leuart1_rx_callback)
        nos_leuart1_rx_callback(EFM32GG_LEUART1, uart_rx_char);
    NOS_EXIT_ISR();
}
#endif

#endif // UART_M 
