// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief EFM32-accelerated AES (advanced encryption standard) utilities.
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 6. 4.
 */

#ifdef USE_EFM32GG_AES
#include <string.h>
#include "em_cmu.h"
#include "em_aes.h"
#include "heap.h"

BOOL nos_aes_cbc128(UINT8 *out, const UINT8 *in, unsigned int len,
        const UINT8 *iv, const UINT8 *key, BOOL encrypt)
{
    if (out == NULL || in == NULL || (len % 16) > 0 || key == NULL ||
            iv == NULL)
        return FALSE;

    if (encrypt)
    {
        AES_CBC128(out, in, len, key, iv, TRUE);
    }
    else
    {
        UINT8 dec_key[16];

        AES_DecryptKey128(dec_key, key);
        AES_CBC128(out, in, len, dec_key, iv, FALSE);
    }
    return TRUE;
}

BOOL nos_aes_cbcmac128(UINT8 *mic, const UINT8 *in, unsigned int len,
        const UINT8 *iv, const UINT8 *key)
{
    UINT8 *out;

    out = nos_malloc(len);
    if (mic == NULL || in == NULL || key == NULL || out == NULL ||
            iv == NULL || (len % 16) > 0)
        return FALSE;

    AES_CBC128(out, in, len, key, iv, TRUE);
    memcpy(mic, &out[len - 16], 16);

    nos_free(out);
    return TRUE;
}

BOOL nos_aes_ctr128(UINT8 *out, const UINT8 *in, unsigned int len,
        UINT8 *init_ctr, const UINT8 *key)
{
    if (out == NULL || in == NULL || (len % 16) > 0 || key == NULL)
        return FALSE;

    AES_CTR128(out, in, len, key, init_ctr, AES_CTRUpdate32Bit);
    return TRUE;
}

#endif /* USE_EFM32GG_AES */
