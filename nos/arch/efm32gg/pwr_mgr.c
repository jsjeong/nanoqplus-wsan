// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Power management for EFM32GG.
 * @date 2012. 7. 24.
 * @author Jongsoo Jeong (ETRI)
 */

#include "pwr_mgr.h"
#include "em_emu.h"
#include "em_usart.h"
#include "arch.h"
#include "uart.h"

#ifdef EMLCD_M
#include "emlcd.h"
#endif /* EMLCD_M */

static UINT8 latest_pm = 0;

#ifdef THREAD_M
#ifndef THREAD_EXT_M
extern UINT8 _created_tid_bit;
#else
extern UINT16 _created_tid_bit;
#endif /* THREAD_EXT_M */
#endif /* THREAD_M */

#ifdef USER_TIMER_M
extern UINT16 nos_user_timer_created_bit;
#endif /* USER_TIMER_M */

void nos_mcu_set_power_mode(UINT8 pm)
{
#ifdef EMLCD_M
    nos_emlcd_print_power_mode(pm);
#endif /* EMLCD_M */

    if (pm == NOS_POWER_MODE_IDLE)
    {
        // (EM0) called at IRQ handlers of LE peripherals.
    }
    else if (pm == NOS_POWER_MODE_SLEEP)
    {
        // (EM1)
        EMU_EnterEM1();
    }
    else if (pm == NOS_POWER_MODE_DEEPSLEEP)
    {
        // (EM2)
        EMU_EnterEM2(true);
    }
    else if (pm == NOS_POWER_MODE_STOP)
    {
        // (EM3)
        EMU_EnterEM3(true);
    }
    else if (pm == NOS_POWER_MODE_SHUTOFF)
    {
        // (EM4)
        EMU_EnterEM4();
    }
    latest_pm = pm;
}

UINT8 nos_mcu_get_current_power_mode(void)
{
    return latest_pm;
}

void nos_mcu_sleep(void)
{
#ifdef MCU_AUTO_SLEEP_M

#ifdef UART_M
#if defined(EFM32GG_USART0_FOR_UART_M) ||         \
    defined(EFM32GG_USART1_FOR_UART_M) ||         \
    defined(EFM32GG_USART2_FOR_UART_M) ||         \
    defined(EFM32GG_UART0_FOR_UART_M) ||          \
    defined(EFM32GG_UART1_FOR_UART_M)

    nos_mcu_set_power_mode(NOS_POWER_MODE_SLEEP);
    return;
#elif defined(EFM32GG_LEUART0_FOR_UART_M) ||    \
    defined(EFM32GG_LEUART1_FOR_UART_M)

    nos_mcu_set_power_mode(NOS_POWER_MODE_DEEPSLEEP);
    return;
#endif
#endif /* UART_M */

#ifdef RTC_M
    nos_mcu_set_power_mode(NOS_POWER_MODE_DEEPSLEEP);
    return;
#endif /* RTC_M */

#ifdef EMLCD_M
    nos_mcu_set_power_mode(NOS_POWER_MODE_DEEPSLEEP);
    return;
#endif /* EMLCD_M */

#ifdef USER_TIMER_M
    if (nos_user_timer_created_bit != 0)
    {
        nos_mcu_set_power_mode(NOS_POWER_MODE_DEEPSLEEP);
        return;
    }
#endif /* USER_TIMER_M */

#ifdef THREAD_M
    if (_created_tid_bit > 1)
    {
        nos_mcu_set_power_mode(NOS_POWER_MODE_DEEPSLEEP);
        return;
    }
#endif /* THREAD_M */

    nos_mcu_set_power_mode(NOS_POWER_MODE_STOP);

#endif /* MCU_AUTO_SLEEP_M */
}

