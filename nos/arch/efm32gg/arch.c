// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file arch.c
 *
 * Basic definitions related to MCU (EFM32GG ARM Cortex-M3)
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 2. 22.
 */

#include "arch.h"
#include "platform.h"
#include "critical_section.h"
#include "em_chip.h"
#include "em_wdog.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_emu.h"
#include "em_device.h"
#include "gpio.h"
#include "pwr_mgr.h"
#include "timer.h"

#ifdef RTC_M
#include "rtc.h"
#endif

#ifdef DMA_M
#include "dma.h"
#endif

#ifdef EFM32GG_STK3700
extern void setupSWO(void);
#endif

#ifndef HFCORECLK
#error "HFCORECLK shoud be defined in 'platform.h'."
#endif

#define LOOP_PER_USEC  (HFCORECLK / 4800000)

void nos_arch_init(void)
{
    CHIP_Init();

    CMU_OscillatorEnable(cmuOsc_HFXO, true, true);
    CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
    CMU_OscillatorEnable(cmuOsc_HFRCO, false, false);

    CMU_OscillatorEnable(cmuOsc_LFXO, true, true);
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);
    CMU_OscillatorEnable(cmuOsc_LFRCO, false, false);
    
    CMU_ClockEnable(cmuClock_CORELE, true);

#if defined(KERNEL_M) || defined(RTC_M)
    nos_timer_init();
#endif

    SystemCoreClockUpdate();
    
#ifdef EFM32GG_STK3700
    setupSWO();
#endif

    SCB->ICSR = SCB_ICSR_PENDSVCLR_Msk | SCB_ICSR_PENDSTCLR_Msk;
    SCB->VTOR = 0x00000000;
    SCB->AIRCR = (0x5fa << SCB_AIRCR_VECTKEY_Pos);
    SCB->SCR = 0x00000000;
    SCB->CCR = 0x00000000;
    SCB->SHP[0] = 0;
    SCB->SHP[1] = 0;
    SCB->SHP[2] = 0;
    SCB->SHP[3] = 0;
    SCB->SHP[4] = 0;
    SCB->SHP[5] = 0;
    SCB->SHP[6] = 0;
    SCB->SHP[7] = 0;
    SCB->SHP[8] = 0;
    SCB->SHP[9] = 0;
    SCB->SHP[10] = 0;
    SCB->SHP[11] = 0;
    SCB->SHCSR = 0x00000000;
    SCB->DFSR = 0xFFFFFFFF;

#if !defined(KERNEL_M) && defined(RTC_M)
    nos_rtc_init();
#endif

    CMU_ClockEnable(cmuClock_GPIO, true);
    GPIO_DriveModeSet(gpioPortA ,gpioDriveModeLow);
    GPIO_DriveModeSet(gpioPortB ,gpioDriveModeLow);
    GPIO_DriveModeSet(gpioPortC ,gpioDriveModeLow);
    GPIO_DriveModeSet(gpioPortD ,gpioDriveModeLow);
    GPIO_DriveModeSet(gpioPortE ,gpioDriveModeLow);
    GPIO_DriveModeSet(gpioPortF ,gpioDriveModeLow);

#ifdef DMA_M
    nos_dma_init();
#endif

#ifdef USE_EFM32GG_AES_M
    CMU_ClockEnable(cmuClock_AES, true);
#endif

    _nested_intr_cnt = 0;

    nos_mcu_set_power_mode(NOS_POWER_MODE_IDLE);
}

void nos_delay_us(UINT16 us)
{
    UINT32 loops = us * LOOP_PER_USEC;
    while(loops-- != 0);
}

void nos_delay_ms(UINT16 ms)
{
    UINT32 loops = ms * LOOP_PER_USEC * 1000;
    while(loops-- != 0);
}

UINT16 ntohs(UINT16 a)
{
    return (((a << 8) & 0xff00) | ((a >> 8) & 0xff));
}

UINT32 ntohl(UINT32 a)
{
    return (((a << 24) & 0xff000000) |
            ((a << 8) & 0xff0000) |
            ((a >> 8) & 0xff00) |
            ((a >> 24) & 0xff));
}
