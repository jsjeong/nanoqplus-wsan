/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief GPIO functions' wrapper.
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 2. 22.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "kconf.h"
#include "nos_common.h"
#include "em_gpio.h"

#define PORT(n)                         gpioPort##n
#define PIN(n)                          n

/* Default input port init */
#define NOS_GPIO_SET_IN(port, pin) \
    GPIO_PinModeSet(PORT(port), PIN(pin), gpioModeInput, 0)

#define NOS_GPIO_SET_IN_PULLUP(port, pin) \
    GPIO_PinModeSet(PORT(port), PIN(pin), gpioModeInputPull, 1)

#define NOS_GPIO_SET_IN_PULLDOWN(port, pin) \
    GPIO_PinModeSet(PORT(port), PIN(pin), gpioModeInputPull, 0)

#define NOS_GPIO_SET_OUT(port, pin) \
    GPIO_PinModeSet(PORT(port), PIN(pin), gpioModePushPull, 1)

#define NOS_GPIO_READ(port, pin)        GPIO_PinInGet(PORT(port), PIN(pin))

/*
 * TODO EFM32 MCUs have different methods to read pin value regard its direction.
 * So NOS_GPIO_READ_OUTPIN() and NOS_GPIO_READ_INPIN() macros are made to distinguish it.
 *
 * Other MCUs should have this macro to keep an uniform API.
 */
//
#define NOS_GPIO_READ_OUTPIN(port,pin)  GPIO_PinOutGet(PORT(port), PIN(pin))
#define NOS_GPIO_READ_INPIN(port,pin)   NOS_GPIO_READ(port,pin)
#define NOS_GPIO_ON(port, pin)          GPIO_PinOutSet(PORT(port), PIN(pin))
#define NOS_GPIO_OFF(port, pin)         GPIO_PinOutClear(PORT(port), PIN(pin))
#define NOS_GPIO_TOGGLE(port, pin)      GPIO_PinOutToggle(PORT(port), PIN(pin))

#define NOS_GPIO_INIT_IN(port, pin) \
    NOS_GPIO_SET_IN(port,pin)

#define NOS_GPIO_INIT_IN_PULLUP(port, pin) \
    NOS_GPIO_SET_IN_PULLUP(port, pin)

#define NOS_GPIO_INIT_IN_PULLDOWN(port, pin) \
    NOS_GPIO_SET_IN_PULLDOWN(port, pin)

#define NOS_GPIO_INIT_OUT(port, pin) \
    NOS_GPIO_SET_OUT(port,pin)

#define NOS_GPIO_INIT_PERIPHERAL(port, pin) \
    GPIO_PinModeSet(PORT(port), PIN(pin), gpioModeDisabled, 0)

#define NOS_GPIO_ENABLE_INTR_ON_RISING(port, pin) \
    GPIO_IntConfig(PORT(port), PIN(pin), true, false, true)
#define NOS_GPIO_DISABLE_INTR_ON_RISING(port, pin) \
    GPIO_IntConfig(PORT(port), PIN(pin), true, false, false)
#define NOS_GPIO_ENABLE_INTR_ON_FALLING(port, pin) \
    GPIO_IntConfig(PORT(port), PIN(pin), false, true, true)
#define NOS_GPIO_DISABLE_INTR_ON_FALLING(port, pin) \
    GPIO_IntConfig(PORT(port), PIN(pin), false, true, false)
#define NOS_GPIO_CLEAR_INTR(port, pin) \
    GPIO_IntClear(1 << PIN(pin))
#define NOS_GPIO_INTRFLAG_IS_SET(pin) \
    (GPIO->IF & (1 << pin))

void nos_gpio_init(void);
void nos_gpio_set_intr_handler(UINT8 pin, void (*func)(void));

#endif /* GPIO_H_ */
