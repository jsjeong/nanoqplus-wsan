// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_putc.c
 * @author Jongsoo Jeong (ETRI)
 * @date 2011.10.17
 */

#include "kconf.h"
#ifdef UART_M

#include "uart.h"
#include "em_usart.h"

#if defined(EFM32GG_LEUART0_FOR_UART_M) ||      \
    defined(EFM32GG_LEUART1_FOR_UART_M)
#include "em_leuart.h"
#include "dma.h"
#endif

void nos_uart_putc(UINT8 port_num, UINT8 character)
{
#ifdef EFM32GG_USART0_FOR_UART_M
    if (port_num == EFM32GG_USART0)
    {
        USART_Tx(USART0, character);
    }
#endif

#ifdef EFM32GG_USART1_FOR_UART_M
    if (port_num == EFM32GG_USART1)
    {
        USART_Tx(USART1, character);
    }
#endif

#ifdef EFM32GG_USART2_FOR_UART_M
    if (port_num == EFM32GG_USART2)
    {
        USART_Tx(USART2, character);
    }
#endif

#ifdef EFM32GG_UART0_FOR_UART_M
    if (port_num == EFM32GG_UART0)
    {
        USART_Tx(UART0, character); //FIXME UART0 is not defined.
    }
#endif

#ifdef EFM32GG_UART1_FOR_UART_M
    if (port_num == EFM32GG_UART1)
    {
        USART_Tx(UART1, character); //FIXME UART1 is not defined.
    }
#endif

#ifdef EFM32GG_LEUART0_FOR_UART_M
    if (port_num == EFM32GG_LEUART0)
    {
        dmaControlBlock[DMA_LE_UART0_TX].SRCEND = &character;
        /* (Re)starting the transfer. Using Basic Mode */
        DMA_ActivateBasic(DMA_LE_UART0_TX,            /* Activate channel selected */
                          true,                       /* Use primary descriptor */
                          false,                      /* No DMA burst */
                          NULL,                       /* Keep destination address */
                          NULL,                       /* Keep source address*/
                          0);                         /* Size of buffer minus1 */
        while(!(LEUART0->STATUS & LEUART_STATUS_TXC));
    }
#endif

#ifdef EFM32GG_LEUART1_FOR_UART_M
    if (port_num == EFM32GG_LEUART1)
    {
        dmaControlBlock[DMA_LE_UART1_TX].SRCEND = &character;
        /* (Re)starting the transfer. Using Basic Mode */
        DMA_ActivateBasic(DMA_LE_UART1_TX,            /* Activate channel selected */
                          true,                       /* Use primary descriptor */
                          false,                      /* No DMA burst */
                          NULL,                       /* Keep destination address */
                          NULL,                       /* Keep source address*/
                          0);                         /* Size of buffer minus1 */
        while(!(LEUART1->STATUS & LEUART_STATUS_TXC));
    }
#endif
}

#endif // UART_M
