// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_init.c
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 2. 23.
 */

#include "kconf.h"
#ifdef UART_M

#include "uart.h"
#include "em_cmu.h"
#include "em_usart.h"

#if defined EFM32GG_LEUART0_FOR_UART_M || \
    defined EFM32GG_LEUART1_FOR_UART_M
#include "em_leuart.h"
#include "dma.h"
#endif

#include "platform.h"
#include "em_gpio.h"

#ifdef EFM32GG_USART0_FOR_UART_M
#ifdef EFM32GG_USART0_BR_300
#define USART0_BR                           300
#elif defined EFM32GG_USART0_BR_600
#define USART0_BR                           600
#elif defined EFM32GG_USART0_BR_1200
#define USART0_BR                           1200
#elif defined EFM32GG_USART0_BR_1800
#define USART0_BR                           1800
#elif defined EFM32GG_USART0_BR_2400
#define USART0_BR                           2400
#elif defined EFM32GG_USART0_BR_4800
#define USART0_BR                           4800
#elif defined EFM32GG_USART0_BR_9600
#define USART0_BR                           9600
#elif defined EFM32GG_USART0_BR_19200
#define USART0_BR                           19200
#elif defined EFM32GG_USART0_BR_38400
#define USART0_BR                           38400
#elif defined EFM32GG_USART0_BR_57600
#define USART0_BR                           57600
#elif defined EFM32GG_USART0_BR_115200
#define USART0_BR                           115200
#elif defined EFM32GG_USART0_BR_230400
#define USART0_BR                           230400
#elif defined EFM32GG_USART0_BR_380400
#define USART0_BR                           380400
#elif defined EFM32GG_USART0_BR_460800
#define USART0_BR                           460800
#elif defined EFM32GG_USART0_BR_500000
#define USART0_BR                           500000
#elif defined EFM32GG_USART0_BR_576000
#define USART0_BR                           576000
#elif defined EFM32GG_USART0_BR_921600
#define USART0_BR                           921600
#elif defined EFM32GG_USART0_BR_1000000
#define USART0_BR                           1000000
#elif defined EFM32GG_USART0_BR_1152000
#define USART0_BR                           1152000
#elif defined EFM32GG_USART0_BR_2000000
#define USART0_BR                           2000000
#elif defined EFM32GG_USART0_BR_2500000
#define USART0_BR                           2500000
#elif defined EFM32GG_USART0_BR_3000000
#define USART0_BR                           3000000
#elif defined EFM32GG_USART0_BR_3500000
#define USART0_BR                           3500000
#elif defined EFM32GG_USART0_BR_4000000
#define USART0_BR                           4000000
#else
#error "Unsupported baud rate for USART0"
#endif /* For USART0_BR */

#if defined EFM32GG_USART0_DATABITS_4
#define USART0_DATABITS                     usartDatabits4
#elif defined EFM32GG_USART0_DATABITS_5
#define USART0_DATABITS                     usartDatabits5
#elif defined EFM32GG_USART0_DATABITS_6
#define USART0_DATABITS                     usartDatabits6
#elif defined EFM32GG_USART0_DATABITS_7
#define USART0_DATABITS                     usartDatabits7
#elif defined EFM32GG_USART0_DATABITS_8
#define USART0_DATABITS                     usartDatabits8
#elif defined EFM32GG_USART0_DATABITS_9
#define USART0_DATABITS                     usartDatabits9
#elif defined EFM32GG_USART0_DATABITS_10
#define USART0_DATABITS                     usartDatabits10
#elif defined EFM32GG_USART0_DATABITS_11
#define USART0_DATABITS                     usartDatabits11
#elif defined EFM32GG_USART0_DATABITS_12
#define USART0_DATABITS                     usartDatabits12
#elif defined EFM32GG_USART0_DATABITS_13
#define USART0_DATABITS                     usartDatabits13
#elif defined EFM32GG_USART0_DATABITS_14
#define USART0_DATABITS                     usartDatabits14
#elif defined EFM32GG_USART0_DATABITS_15
#define USART0_DATABITS                     usartDatabits15
#elif defined EFM32GG_USART0_DATABITS_16
#define USART0_DATABITS                     usartDatabits16
#else
#error "Unsupported databits for USART0"
#endif /* For USART0_DATABITS */

#if defined EFM32GG_USART0_PARITY_NONE
#define USART0_PARITY                       usartNoParity
#elif defined EFM32GG_USART0_PARITY_EVEN
#define USART0_PARITY                       usartEvenParity
#elif defined EFM32GG_USART0_PARITY_ODD
#define USART0_PARITY                       usartOddParity
#else
#error "Unsupported parity mode for USART0"
#endif /* For USART0_PARITY */

#if defined EFM32GG_USART0_STOPBITS_HALF
#define USART0_STOPBITS                     usartStopbits0p5
#elif defined EFM32GG_USART0_STOPBITS_ONE
#define USART0_STOPBITS                     usartStopbits1
#elif defined EFM32GG_USART0_STOPBITS_ONEANDAHALF
#define USART0_STOPBITS                     usartStopbits1p5
#elif defined EFM32GG_USART0_STOPBITS_TWO
#define USART0_STOPBITS                     usartStopbits2
#else
#error
#endif /* For USART0_STOPBITS */
#endif /* EFM32GG_USART0_FOR_UART_M */

#ifdef EFM32GG_USART1_FOR_UART_M
#ifdef EFM32GG_USART1_BR_300
#define USART1_BR                           300
#elif defined EFM32GG_USART1_BR_600
#define USART1_BR                           600
#elif defined EFM32GG_USART1_BR_1200
#define USART1_BR                           1200
#elif defined EFM32GG_USART1_BR_1800
#define USART1_BR                           1800
#elif defined EFM32GG_USART1_BR_2400
#define USART1_BR                           2400
#elif defined EFM32GG_USART1_BR_4800
#define USART1_BR                           4800
#elif defined EFM32GG_USART1_BR_9600
#define USART1_BR                           9600
#elif defined EFM32GG_USART1_BR_19200
#define USART1_BR                           19200
#elif defined EFM32GG_USART1_BR_38400
#define USART1_BR                           38400
#elif defined EFM32GG_USART1_BR_57600
#define USART1_BR                           57600
#elif defined EFM32GG_USART1_BR_115200
#define USART1_BR                           115200
#elif defined EFM32GG_USART1_BR_230400
#define USART1_BR                           230400
#elif defined EFM32GG_USART1_BR_380400
#define USART1_BR                           380400
#elif defined EFM32GG_USART1_BR_460800
#define USART1_BR                           460800
#elif defined EFM32GG_USART1_BR_500000
#define USART1_BR                           500000
#elif defined EFM32GG_USART1_BR_576000
#define USART1_BR                           576000
#elif defined EFM32GG_USART1_BR_921600
#define USART1_BR                           921600
#elif defined EFM32GG_USART1_BR_1000000
#define USART1_BR                           1000000
#elif defined EFM32GG_USART1_BR_1152000
#define USART1_BR                           1152000
#elif defined EFM32GG_USART1_BR_2000000
#define USART1_BR                           2000000
#elif defined EFM32GG_USART1_BR_2500000
#define USART1_BR                           2500000
#elif defined EFM32GG_USART1_BR_3000000
#define USART1_BR                           3000000
#elif defined EFM32GG_USART1_BR_3500000
#define USART1_BR                           3500000
#elif defined EFM32GG_USART1_BR_4000000
#define USART1_BR                           4000000
#else
#error "Unsupported baud rate for USART1"
#endif /* For USART1_BR */

#if defined EFM32GG_USART1_DATABITS_4
#define USART1_DATABITS                     usartDatabits4
#elif defined EFM32GG_USART1_DATABITS_5
#define USART1_DATABITS                     usartDatabits5
#elif defined EFM32GG_USART1_DATABITS_6
#define USART1_DATABITS                     usartDatabits6
#elif defined EFM32GG_USART1_DATABITS_7
#define USART1_DATABITS                     usartDatabits7
#elif defined EFM32GG_USART1_DATABITS_8
#define USART1_DATABITS                     usartDatabits8
#elif defined EFM32GG_USART1_DATABITS_9
#define USART1_DATABITS                     usartDatabits9
#elif defined EFM32GG_USART1_DATABITS_10
#define USART1_DATABITS                     usartDatabits10
#elif defined EFM32GG_USART1_DATABITS_11
#define USART1_DATABITS                     usartDatabits11
#elif defined EFM32GG_USART1_DATABITS_12
#define USART1_DATABITS                     usartDatabits12
#elif defined EFM32GG_USART1_DATABITS_13
#define USART1_DATABITS                     usartDatabits13
#elif defined EFM32GG_USART1_DATABITS_14
#define USART1_DATABITS                     usartDatabits14
#elif defined EFM32GG_USART1_DATABITS_15
#define USART1_DATABITS                     usartDatabits15
#elif defined EFM32GG_USART1_DATABITS_16
#define USART1_DATABITS                     usartDatabits16
#else
#error "Unsupported databits for USART1"
#endif /* For USART1_DATABITS */

#if defined EFM32GG_USART1_PARITY_NONE
#define USART1_PARITY                       usartNoParity
#elif defined EFM32GG_USART1_PARITY_EVEN
#define USART1_PARITY                       usartEvenParity
#elif defined EFM32GG_USART1_PARITY_ODD
#define USART1_PARITY                       usartOddParity
#else
#error "Unsupported parity mode for USART1"
#endif /* For USART1_PARITY */

#if defined EFM32GG_USART1_STOPBITS_HALF
#define USART1_STOPBITS                     usartStopbits0p5
#elif defined EFM32GG_USART1_STOPBITS_ONE
#define USART1_STOPBITS                     usartStopbits1
#elif defined EFM32GG_USART1_STOPBITS_ONEANDAHALF
#define USART1_STOPBITS                     usartStopbits1p5
#elif defined EFM32GG_USART1_STOPBITS_TWO
#define USART1_STOPBITS                     usartStopbits2
#else
#error " Unsupported stopbits for USART1"
#endif /* For USART1_STOPBITS */
#endif /* EFM32GG_USART1_FOR_UART_M */

#ifdef EFM32GG_USART2_FOR_UART_M
#ifdef EFM32GG_USART2_BR_300
#define USART2_BR                           300
#elif defined EFM32GG_USART2_BR_600
#define USART2_BR                           600
#elif defined EFM32GG_USART2_BR_1200
#define USART2_BR                           1200
#elif defined EFM32GG_USART2_BR_1800
#define USART2_BR                           1800
#elif defined EFM32GG_USART2_BR_2400
#define USART2_BR                           2400
#elif defined EFM32GG_USART2_BR_4800
#define USART2_BR                           4800
#elif defined EFM32GG_USART2_BR_9600
#define USART2_BR                           9600
#elif defined EFM32GG_USART2_BR_19200
#define USART2_BR                           19200
#elif defined EFM32GG_USART2_BR_38400
#define USART2_BR                           38400
#elif defined EFM32GG_USART2_BR_57600
#define USART2_BR                           57600
#elif defined EFM32GG_USART2_BR_115200
#define USART2_BR                           115200
#elif defined EFM32GG_USART2_BR_230400
#define USART2_BR                           230400
#elif defined EFM32GG_USART2_BR_380400
#define USART2_BR                           380400
#elif defined EFM32GG_USART2_BR_460800
#define USART2_BR                           460800
#elif defined EFM32GG_USART2_BR_500000
#define USART2_BR                           500000
#elif defined EFM32GG_USART2_BR_576000
#define USART2_BR                           576000
#elif defined EFM32GG_USART2_BR_921600
#define USART2_BR                           921600
#elif defined EFM32GG_USART2_BR_1000000
#define USART2_BR                           1000000
#elif defined EFM32GG_USART2_BR_1152000
#define USART2_BR                           1152000
#elif defined EFM32GG_USART2_BR_2000000
#define USART2_BR                           2000000
#elif defined EFM32GG_USART2_BR_2500000
#define USART2_BR                           2500000
#elif defined EFM32GG_USART2_BR_3000000
#define USART2_BR                           3000000
#elif defined EFM32GG_USART2_BR_3500000
#define USART2_BR                           3500000
#elif defined EFM32GG_USART2_BR_4000000
#define USART2_BR                           4000000
#else
#error "Unsupported baud rate for USART2"
#endif /* For USART2_BR */

#if defined EFM32GG_USART2_DATABITS_4
#define USART2_DATABITS                     usartDatabits4
#elif defined EFM32GG_USART2_DATABITS_5
#define USART2_DATABITS                     usartDatabits5
#elif defined EFM32GG_USART2_DATABITS_6
#define USART2_DATABITS                     usartDatabits6
#elif defined EFM32GG_USART2_DATABITS_7
#define USART2_DATABITS                     usartDatabits7
#elif defined EFM32GG_USART2_DATABITS_8
#define USART2_DATABITS                     usartDatabits8
#elif defined EFM32GG_USART2_DATABITS_9
#define USART2_DATABITS                     usartDatabits9
#elif defined EFM32GG_USART2_DATABITS_10
#define USART2_DATABITS                     usartDatabits10
#elif defined EFM32GG_USART2_DATABITS_11
#define USART2_DATABITS                     usartDatabits11
#elif defined EFM32GG_USART2_DATABITS_12
#define USART2_DATABITS                     usartDatabits12
#elif defined EFM32GG_USART2_DATABITS_13
#define USART2_DATABITS                     usartDatabits13
#elif defined EFM32GG_USART2_DATABITS_14
#define USART2_DATABITS                     usartDatabits14
#elif defined EFM32GG_USART2_DATABITS_15
#define USART2_DATABITS                     usartDatabits15
#elif defined EFM32GG_USART2_DATABITS_16
#define USART2_DATABITS                     usartDatabits16
#else
#error "Unsupported databits for USART2"
#endif /* For USART2_DATABITS */

#if defined EFM32GG_USART2_PARITY_NONE
#define USART2_PARITY                       usartNoParity
#elif defined EFM32GG_USART2_PARITY_EVEN
#define USART2_PARITY                       usartEvenParity
#elif defined EFM32GG_USART2_PARITY_ODD
#define USART2_PARITY                       usartOddParity
#else
#error "Unsupported parity mode for USART2"
#endif /* For USART2_PARITY */

#if defined EFM32GG_USART2_STOPBITS_HALF
#define USART2_STOPBITS                     usartStopbits0p5
#elif defined EFM32GG_USART2_STOPBITS_ONE
#define USART2_STOPBITS                     usartStopbits1
#elif defined EFM32GG_USART2_STOPBITS_ONEANDAHALF
#define USART2_STOPBITS                     usartStopbits1p5
#elif defined EFM32GG_USART2_STOPBITS_TWO
#define USART2_STOPBITS                     usartStopbits2
#else
#error "Unsupported stopbits for USART2"
#endif /* For USART2_STOPBITS */
#endif /* EFM32GG_USART2_FOR_UART_M */

#ifdef EFM32GG_UART0_FOR_UART_M
#ifdef EFM32GG_UART0_BR_300
#define UART0_BR                            300
#elif defined EFM32GG_UART0_BR_600
#define UART0_BR                            600
#elif defined EFM32GG_UART0_BR_1200
#define UART0_BR                            1200
#elif defined EFM32GG_UART0_BR_1800
#define UART0_BR                            1800
#elif defined EFM32GG_UART0_BR_2400
#define UART0_BR                            2400
#elif defined EFM32GG_UART0_BR_4800
#define UART0_BR                            4800
#elif defined EFM32GG_UART0_BR_9600
#define UART0_BR                            9600
#elif defined EFM32GG_UART0_BR_19200
#define UART0_BR                            19200
#elif defined EFM32GG_UART0_BR_38400
#define UART0_BR                            38400
#elif defined EFM32GG_UART0_BR_57600
#define UART0_BR                            57600
#elif defined EFM32GG_UART0_BR_115200
#define UART0_BR                            115200
#elif defined EFM32GG_UART0_BR_230400
#define UART0_BR                            230400
#elif defined EFM32GG_UART0_BR_380400
#define UART0_BR                            380400
#elif defined EFM32GG_UART0_BR_460800
#define UART0_BR                            460800
#elif defined EFM32GG_UART0_BR_500000
#define UART0_BR                            500000
#elif defined EFM32GG_UART0_BR_576000
#define UART0_BR                            576000
#elif defined EFM32GG_UART0_BR_921600
#define UART0_BR                            921600
#elif defined EFM32GG_UART0_BR_1000000
#define UART0_BR                            1000000
#elif defined EFM32GG_UART0_BR_1152000
#define UART0_BR                            1152000
#elif defined EFM32GG_UART0_BR_2000000
#define UART0_BR                            2000000
#elif defined EFM32GG_UART0_BR_2500000
#define UART0_BR                            2500000
#elif defined EFM32GG_UART0_BR_3000000
#define UART0_BR                            3000000
#elif defined EFM32GG_UART0_BR_3500000
#define UART0_BR                            3500000
#elif defined EFM32GG_UART0_BR_4000000
#define UART0_BR                            4000000
#else
#error "Unsupported baud rate for UART0"
#endif /* For UART0_BR */

#if defined EFM32GG_UART0_DATABITS_8
#define UART0_DATABITS                      usartDatabits8
#elif defined EFM32GG_UART0_DATABITS_9
#define UART0_DATABITS                      usartDatabits9
#else
#error "Unsupported databits for UART0"
#endif /* For UART0_DATABITS */

#if defined EFM32GG_UART0_PARITY_NONE
#define UART0_PARITY                        usartNoParity
#elif defined EFM32GG_UART0_PARITY_EVEN
#define UART0_PARITY                        usartEvenParity
#elif defined EFM32GG_UART0_PARITY_ODD
#define UART0_PARITY                        usartOddParity
#else
#error "Unsupported parity mode for UART0"
#endif /* For UART0_PARITY */

#if defined EFM32GG_UART0_STOPBITS_HALF
#define UART0_STOPBITS                      usartStopbits0p5
#elif defined EFM32GG_UART0_STOPBITS_ONE
#define UART0_STOPBITS                      usartStopbits1
#elif defined EFM32GG_UART0_STOPBITS_ONEANDAHALF
#define UART0_STOPBITS                      usartStopbits1p5
#elif defined EFM32GG_UART0_STOPBITS_TWO
#define UART0_STOPBITS                      usartStopbits2
#else
#error "Unsupported stopbits for UART0"
#endif /* For UART0_STOPBITS */
#endif /* EFM32GG_UART0_FOR_UART_M */

#ifdef EFM32GG_UART1_FOR_UART_M
#ifdef EFM32GG_UART1_BR_300
#define UART1_BR                            300
#elif defined EFM32GG_UART1_BR_600
#define UART1_BR                            600
#elif defined EFM32GG_UART1_BR_1200
#define UART1_BR                            1200
#elif defined EFM32GG_UART1_BR_1800
#define UART1_BR                            1800
#elif defined EFM32GG_UART1_BR_2400
#define UART1_BR                            2400
#elif defined EFM32GG_UART1_BR_4800
#define UART1_BR                            4800
#elif defined EFM32GG_UART1_BR_9600
#define UART1_BR                            9600
#elif defined EFM32GG_UART1_BR_19200
#define UART1_BR                            19200
#elif defined EFM32GG_UART1_BR_38400
#define UART1_BR                            38400
#elif defined EFM32GG_UART1_BR_57600
#define UART1_BR                            57600
#elif defined EFM32GG_UART1_BR_115200
#define UART1_BR                            115200
#elif defined EFM32GG_UART1_BR_230400
#define UART1_BR                            230400
#elif defined EFM32GG_UART1_BR_380400
#define UART1_BR                            380400
#elif defined EFM32GG_UART1_BR_460800
#define UART1_BR                            460800
#elif defined EFM32GG_UART1_BR_500000
#define UART1_BR                            500000
#elif defined EFM32GG_UART1_BR_576000
#define UART1_BR                            576000
#elif defined EFM32GG_UART1_BR_921600
#define UART1_BR                            921600
#elif defined EFM32GG_UART1_BR_1000000
#define UART1_BR                            1000000
#elif defined EFM32GG_UART1_BR_1152000
#define UART1_BR                            1152000
#elif defined EFM32GG_UART1_BR_2000000
#define UART1_BR                            2000000
#elif defined EFM32GG_UART1_BR_2500000
#define UART1_BR                            2500000
#elif defined EFM32GG_UART1_BR_3000000
#define UART1_BR                            3000000
#elif defined EFM32GG_UART1_BR_3500000
#define UART1_BR                            3500000
#elif defined EFM32GG_UART1_BR_4000000
#define UART1_BR                            4000000
#else
#error "Unsupported baud rate for UART1"
#endif /* For UART1_BR */

#if defined EFM32GG_UART1_DATABITS_8
#define UART1_DATABITS                      usartDatabits8
#elif defined EFM32GG_UART1_DATABITS_9
#define UART1_DATABITS                      usartDatabits9
#else
#error "Unsupported databits for UART1"
#endif /* For UART1_DATABITS */

#if defined EFM32GG_UART1_PARITY_NONE
#define UART1_PARITY                        usartNoParity
#elif defined EFM32GG_UART1_PARITY_EVEN
#define UART1_PARITY                        usartEvenParity
#elif defined EFM32GG_UART1_PARITY_ODD
#define UART1_PARITY                        usartOddParity
#else
#error "Unsupported parity mode for UART1"
#endif /* For UART1_PARITY */

#if defined EFM32GG_UART1_STOPBITS_HALF
#define UART1_STOPBITS                      usartStopbits0p5
#elif defined EFM32GG_UART1_STOPBITS_ONE
#define UART1_STOPBITS                      usartStopbits1
#elif defined EFM32GG_UART1_STOPBITS_ONEANDAHALF
#define UART1_STOPBITS                      usartStopbits1p5
#elif defined EFM32GG_UART1_STOPBITS_TWO
#define UART1_STOPBITS                      usartStopbits2
#else
#error "Unsupported stopbits for UART1"
#endif /* For UART1_STOPBITS */
#endif /* EFM32GG_UART1_FOR_UART_M */

#ifdef EFM32GG_LEUART0_FOR_UART_M
#ifdef EFM32GG_LEUART0_BR_300
#define LEUART0_BR                          300
#elif defined EFM32GG_LEUART0_BR_600
#define LEUART0_BR                          600
#elif defined EFM32GG_LEUART0_BR_1200
#define LEUART0_BR                          1200
#elif defined EFM32GG_LEUART0_BR_1800
#define LEUART0_BR                          1800
#elif defined EFM32GG_LEUART0_BR_2400
#define LEUART0_BR                          2400
#elif defined EFM32GG_LEUART0_BR_4800
#define LEUART0_BR                          4800
#elif defined EFM32GG_LEUART0_BR_9600
#define LEUART0_BR                          9600
#elif defined EFM32GG_LEUART0_BR_19200
#define LEUART0_BR                          19200
#elif defined EFM32GG_LEUART0_BR_38400
#define LEUART0_BR                          38400
#elif defined EFM32GG_LEUART0_BR_57600
#define LEUART0_BR                          57600
#elif defined EFM32GG_LEUART0_BR_115200
#define LEUART0_BR                          115200
#elif defined EFM32GG_LEUART0_BR_230400
#define LEUART0_BR                          230400
#elif defined EFM32GG_LEUART0_BR_380400
#define LEUART0_BR                          380400
#elif defined EFM32GG_LEUART0_BR_460800
#define LEUART0_BR                          460800
#elif defined EFM32GG_LEUART0_BR_500000
#define LEUART0_BR                          500000
#elif defined EFM32GG_LEUART0_BR_576000
#define LEUART0_BR                          576000
#elif defined EFM32GG_LEUART0_BR_921600
#define LEUART0_BR                          921600
#elif defined EFM32GG_LEUART0_BR_1000000
#define LEUART0_BR                          1000000
#elif defined EFM32GG_LEUART0_BR_1152000
#define LEUART0_BR                          1152000
#elif defined EFM32GG_LEUART0_BR_2000000
#define LEUART0_BR                          2000000
#elif defined EFM32GG_LEUART0_BR_2500000
#define LEUART0_BR                          2500000
#elif defined EFM32GG_LEUART0_BR_3000000
#define LEUART0_BR                          3000000
#elif defined EFM32GG_LEUART0_BR_3500000
#define LEUART0_BR                          3500000
#elif defined EFM32GG_LEUART0_BR_4000000
#define LEUART0_BR                          4000000
#else
#error "Unsupported baud rate for LEUART0"
#endif /* For LEUART0_BR */

#if defined EFM32GG_LEUART0_DATABITS_8
#define LEUART0_DATABITS                    leuartDatabits8
#elif defined EFM32GG_LEUART0_DATABITS_9
#define LEUART0_DATABITS                    leuartDatabits9
#else
#error "Unsupported databits for LEUART0"
#endif /* For LEUART0_DATABITS */

#if defined EFM32GG_LEUART0_PARITY_NONE
#define LEUART0_PARITY                      leuartNoParity
#elif defined EFM32GG_LEUART0_PARITY_EVEN
#define LEUART0_PARITY                      leuartEvenParity
#elif defined EFM32GG_LEUART0_PARITY_ODD
#define LEUART0_PARITY                      leuartOddParity
#else
#error "Unsupported parity mode for LEUART0"
#endif /* For LEUART0_PARITY */

#if defined EFM32GG_LEUART0_STOPBITS_ONE
#define LEUART0_STOPBITS                    leuartStopbits1
#elif defined EFM32GG_LEUART0_STOPBITS_TWO
#define LEUART0_STOPBITS                    leuartStopbits2
#else
#error "Unsupported stopbits for LEUART0"
#endif /* For LEUART0_STOPBITS */
#endif /* EFM32GG_LEUART0_FOR_UART_M */

#ifdef EFM32GG_LEUART1_FOR_UART_M
#ifdef EFM32GG_LEUART1_BR_300
#define LEUART1_BR                          300
#elif defined EFM32GG_LEUART1_BR_600
#define LEUART1_BR                          600
#elif defined EFM32GG_LEUART1_BR_1200
#define LEUART1_BR                          1200
#elif defined EFM32GG_LEUART1_BR_1800
#define LEUART1_BR                          1800
#elif defined EFM32GG_LEUART1_BR_2400
#define LEUART1_BR                          2400
#elif defined EFM32GG_LEUART1_BR_4800
#define LEUART1_BR                          4800
#elif defined EFM32GG_LEUART1_BR_9600
#define LEUART1_BR                          9600
#elif defined EFM32GG_LEUART1_BR_19200
#define LEUART1_BR                          19200
#elif defined EFM32GG_LEUART1_BR_38400
#define LEUART1_BR                          38400
#elif defined EFM32GG_LEUART1_BR_57600
#define LEUART1_BR                          57600
#elif defined EFM32GG_LEUART1_BR_115200
#define LEUART1_BR                          115200
#elif defined EFM32GG_LEUART1_BR_230400
#define LEUART1_BR                          230400
#elif defined EFM32GG_LEUART1_BR_380400
#define LEUART1_BR                          380400
#elif defined EFM32GG_LEUART1_BR_460800
#define LEUART1_BR                          460800
#elif defined EFM32GG_LEUART1_BR_500000
#define LEUART1_BR                          500000
#elif defined EFM32GG_LEUART1_BR_576000
#define LEUART1_BR                          576000
#elif defined EFM32GG_LEUART1_BR_921600
#define LEUART1_BR                          921600
#elif defined EFM32GG_LEUART1_BR_1000000
#define LEUART1_BR                          1000000
#elif defined EFM32GG_LEUART1_BR_1152000
#define LEUART1_BR                          1152000
#elif defined EFM32GG_LEUART1_BR_2000000
#define LEUART1_BR                          2000000
#elif defined EFM32GG_LEUART1_BR_2500000
#define LEUART1_BR                          2500000
#elif defined EFM32GG_LEUART1_BR_3000000
#define LEUART1_BR                          3000000
#elif defined EFM32GG_LEUART1_BR_3500000
#define LEUART1_BR                          3500000
#elif defined EFM32GG_LEUART1_BR_4000000
#define LEUART1_BR                          4000000
#else
#error "Unsupported baud rate for LEUART1"
#endif /* For LEUART1_BR */

#if defined EFM32GG_LEUART1_DATABITS_8
#define LEUART1_DATABITS                    leuartDatabits8
#elif defined EFM32GG_LEUART1_DATABITS_9
#define LEUART1_DATABITS                    leuartDatabits9
#else
#error "Unsupported databits for LEUART1"
#endif /* For LEUART1_DATABITS */

#if defined EFM32GG_LEUART1_PARITY_NONE
#define LEUART1_PARITY                      leuartNoParity
#elif defined EFM32GG_LEUART1_PARITY_EVEN
#define LEUART1_PARITY                      leuartEvenParity
#elif defined EFM32GG_LEUART1_PARITY_ODD
#define LEUART1_PARITY                      leuartOddParity
#else
#error "Unsupported parity mode for LEUART1"
#endif /* For LEUART1_PARITY */

#if defined EFM32GG_LEUART0_STOPBITS_ONE
#define LEUART1_STOPBITS                    leuartStopbits1
#elif defined EFM32GG_LEUART0_STOPBITS_TWO
#define LEUART1_STOPBITS                    leuartStopbits2
#else
#error "Unsupported stopbits for LEUART1"
#endif /* For LEUART1_STOPBITS */
#endif /* EFM32GG_LEUART1_FOR_UART_M */

#ifdef EFM32GG_LEUART0_FOR_UART_M
extern char rxdata_leuart0;
#endif

#ifdef EFM32GG_LEUART1_FOR_UART_M
extern char rxdata_leuart1;
#endif

#if defined EFM32GG_USART0_FOR_UART_M ||\
    defined EFM32GG_USART1_FOR_UART_M ||\
    defined EFM32GG_USART2_FOR_UART_M ||\
    defined EFM32GG_UART0_FOR_UART_M ||\
    defined EFM32GG_UART1_FOR_UART_M

void init_uart(USART_TypeDef *port, CMU_Clock_TypeDef clock, UINT8 location,
        IRQn_Type rxirqn, USART_InitAsync_TypeDef *init)
{
    CMU_ClockEnable(cmuClock_HFPER, true);
    CMU_ClockEnable(clock, true);

    init->enable = usartDisable;
    USART_InitAsync(port, init);

    /* Enable RX and TX pins and set location */
    port->ROUTE = USART_ROUTE_RXPEN | USART_ROUTE_TXPEN | (location << 8);

    /* Clear previous interrupts */
    USART_IntClear(port, USART_IF_RXDATAV);
    NVIC_ClearPendingIRQ(rxirqn);

    /* Enable interrupts */
    USART_IntEnable(port, USART_IF_RXDATAV);

    NVIC_SetPriority(rxirqn, 6);
    NVIC_EnableIRQ(rxirqn);

    /* Finally enable it */
    USART_Enable(port, usartEnable);
}
#endif

#if defined EFM32GG_LEUART0_FOR_UART_M || \
    defined EFM32GG_LEUART1_FOR_UART_M

void init_leuart(LEUART_TypeDef *port,
        CMU_Clock_TypeDef clock,
        LEUART_Init_TypeDef *init,
        UINT8 location,
        IRQn_Type rxirqn,
        char *dma_dst_addr,
        unsigned dma_ch_tx,
        DMA_CfgChannel_TypeDef *dma_chcfg_tx,
        DMA_CfgDescr_TypeDef *dma_chds_tx,
        unsigned dma_ch_rx,
        DMA_CfgChannel_TypeDef *dma_chcfg_rx,
        DMA_CfgDescr_TypeDef *dma_chds_rx)
{
#if ((LEUART0_BR > 9600) || (LEUART1_BR > 9600))
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_CORELEDIV2);
    CMU_ClockDivSet(clock, cmuClkDiv_2);
#else
    /* Start LFXO, and use LFXO for low-energy modules */
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);
#endif

    /* Enabling clocks, all other remain disabled */
    CMU_ClockEnable(cmuClock_CORELE, true); /* Enable CORELE clock */
    CMU_ClockEnable(clock, true);

    LEUART_Reset(port);
    LEUART_Init(port, init);

    port->ROUTE = LEUART_ROUTE_RXPEN | LEUART_ROUTE_TXPEN | (location << 8);

    LEUART_IntClear(port, LEUART_IF_RXDATAV);
    NVIC_ClearPendingIRQ(rxirqn);

    //LE UARTx RX Initializing DMA, Channel & Desriptor
    DMA_CfgChannel(dma_ch_rx, dma_chcfg_rx);
    DMA_CfgDescr(dma_ch_rx, true, dma_chds_rx);

    /* Starting the transfer. Using Basic Mode */
    DMA_ActivateBasic(dma_ch_rx, /* Activate channel selected */
            true, /* Use primary descriptor */
            false, /* No DMA burst */
            (void *) dma_dst_addr, /* Destination address */
            (void *) &port->RXDATA, /* Source address*/
            0); /* Size of buffer minus1 */

    //LE UARTx TX Initializing DMA, Channel & Desriptor
    DMA_CfgChannel(dma_ch_tx, dma_chcfg_tx);
    DMA_CfgDescr(dma_ch_tx, true, dma_chds_tx);

    /* Set new DMA destination address directly in the DMA descriptor */
    dmaControlBlock[dma_ch_tx].DSTEND = &port->TXDATA;

    /* Enable LEUART Signal Frame Interrupt */
    //      LEUART_IntEnable(LEUART0, LEUART_IEN_TXC);  // deleted by chkim on 9/1 since IRQHandler is only called by RXDATAV bit.
    LEUART_IntEnable(port, LEUART_IEN_RXDATAV);

    /* Enable LEUARTx interrupt vector */
    NVIC_SetPriority(rxirqn, 6);
    NVIC_EnableIRQ(rxirqn);

    /* Make sure the LEUART wakes up the DMA on RX data */
    port->CTRL = LEUART_CTRL_RXDMAWU;
}
#endif

void nos_uart_init(UINT8 port_num)
{
    
#if defined EFM32GG_USART0_FOR_UART_M ||\
    defined EFM32GG_USART1_FOR_UART_M ||\
    defined EFM32GG_USART2_FOR_UART_M ||\
    defined EFM32GG_UART0_FOR_UART_M ||\
    defined EFM32GG_UART1_FOR_UART_M
    USART_InitAsync_TypeDef async_init = USART_INITASYNC_DEFAULT;
#endif // local variables for USARTx and UARTx

#if defined EFM32GG_LEUART0_FOR_UART_M ||\
    defined EFM32GG_LEUART1_FOR_UART_M
    LEUART_Init_TypeDef leuart_init = LEUART_INIT_DEFAULT;
    DMA_CfgChannel_TypeDef dma_cfg_ch_tx, dma_cfg_ch_rx;
    DMA_CfgDescr_TypeDef dma_cfg_descr_tx, dma_cfg_descr_rx;
#endif // local variables for LEUARTx

    if (port_num == EFM32GG_USART0)
    {
#ifdef EFM32GG_USART0_FOR_UART_M
        GPIO_PinModeSet(USART0_TXD_PORT, USART0_TXD_PIN, gpioModePushPull, 1);
        GPIO_PinOutSet(USART0_TXD_PORT, USART0_TXD_PIN);
        GPIO_PinModeSet(USART0_RXD_PORT, USART0_RXD_PIN, gpioModeInputPull, 0);
        async_init.baudrate = USART0_BR;
        async_init.databits = USART0_DATABITS;
        async_init.parity = USART0_PARITY;
        async_init.stopbits = USART0_STOPBITS;
        init_uart(USART0, cmuClock_USART0, USART0_LOCATION, USART0_RX_IRQn,
                &async_init);
#else
        return;
#endif
    }
    else if (port_num == EFM32GG_USART1)
    {
#ifdef EFM32GG_USART1_FOR_UART_M
        GPIO_PinModeSet(USART1_TXD_PORT, USART1_TXD_PIN, gpioModePushPull, 1);
        GPIO_PinOutSet(USART1_TXD_PORT, USART1_TXD_PIN);
        GPIO_PinModeSet(USART1_RXD_PORT, USART1_RXD_PIN, gpioModeInputPull, 0);
        async_init.baudrate = USART1_BR;
        async_init.databits = USART1_DATABITS;
        async_init.parity = USART1_PARITY;
        async_init.stopbits = USART1_STOPBITS;
        init_uart(USART1, cmuClock_USART1, USART1_LOCATION, USART1_RX_IRQn,
                &async_init);
#else
        return;
#endif
    }
    else if (port_num == EFM32GG_USART2)
    {
#ifdef EFM32GG_USART2_FOR_UART_M
        GPIO_PinModeSet(USART2_TXD_PORT, USART2_TXD_PIN, gpioModePushPull, 1);
        GPIO_PinOutSet(USART2_TXD_PORT, USART2_TXD_PIN);
        GPIO_PinModeSet(USART2_RXD_PORT, USART2_RXD_PIN, gpioModeInputPull, 0);
        async_init.baudrate = USART2_BR;
        async_init.databits = USART2_DATABITS;
        async_init.parity = USART2_PARITY;
        async_init.stopbits = USART2_STOPBITS;
        init_uart(USART2, cmuClock_USART2, USART2_LOCATION, USART2_RX_IRQn,
                &async_init);
#else
        return;
#endif
    }
    else if (port_num == EFM32GG_UART0)
    {
#ifdef EFM32GG_UART0_FOR_UART_M
        GPIO_PinModeSet(UART0_TXD_PORT, UART0_TXD_PIN, gpioModePushPull, 1);
        GPIO_PinOutSet(UART0_TXD_PORT, UART0_TXD_PIN);
        GPIO_PinModeSet(UART0_RXD_PORT, UART0_RXD_PIN, gpioModeInputPull, 0);
        async_init.baudrate = UART0_BR;
        async_init.databits = UART0_DATABITS;
        async_init.parity = UART0_PARITY;
        async_init.stopbits = UART0_STOPBITS;
        init_uart(UART0, cmuClock_UART0, UART0_LOCATION, UART0_RX_IRQn,
                &async_init);
#else
        return;
#endif
    }
    else if (port_num == EFM32GG_UART1)
    {
#ifdef EFM32GG_UART1_FOR_UART_M
        GPIO_PinModeSet(UART1_TXD_PORT, UART1_TXD_PIN, gpioModePushPull, 1);
        GPIO_PinOutSet(UART1_TXD_PORT, UART1_TXD_PIN);
        GPIO_PinModeSet(UART1_RXD_PORT, UART1_RXD_PIN, gpioModeInputPull, 0);
        async_init.baudrate = UART1_BR;
        async_init.databits = UART1_DATABITS;
        async_init.parity = UART1_PARITY;
        async_init.stopbits = UART1_STOPBITS;
        init_uart(UART1, cmuClock_UART1, UART1_LOCATION, UART1_RX_IRQn,
                &async_init);
#else
        return;
#endif
    }
    else if (port_num == EFM32GG_LEUART0)
    {
#ifdef EFM32GG_LEUART0_FOR_UART_M
        GPIO_PinModeSet(LEUART0_TXD_PORT, LEUART0_TXD_PIN, gpioModePushPull, 1);
        GPIO_PinOutSet(LEUART0_TXD_PORT, LEUART0_TXD_PIN);
        GPIO_PinModeSet(LEUART0_RXD_PORT, LEUART0_RXD_PIN, gpioModeInputPull, 0);

        leuart_init.baudrate = LEUART0_BR;
        leuart_init.databits = LEUART0_DATABITS;
        leuart_init.parity = LEUART0_PARITY;
        leuart_init.stopbits = LEUART0_STOPBITS;

        /* Setting up channel */
        dma_cfg_ch_rx.highPri   = false;                    /* Normal priority */
        dma_cfg_ch_rx.enableInt = false;                    /* No interupt enabled for callback functions */
        dma_cfg_ch_rx.select    = DMAREQ_LEUART0_RXDATAV;   /* Set LEUART0 RX data avalible as source of DMA signals */
        dma_cfg_ch_rx.cb        = NULL;                     /* No callback funtion */

        /* Setting up channel descriptor */
        dma_cfg_descr_rx.dstInc  = dmaDataInc1;     /* Increment destination address by one byte */
        dma_cfg_descr_rx.srcInc  = dmaDataIncNone;  /* Do no increment source address  */
        dma_cfg_descr_rx.size    = dmaDataSize1;    /* Data size is one byte */
        dma_cfg_descr_rx.arbRate = dmaArbitrate1;   /* Rearbitrate for each byte recieved*/
        dma_cfg_descr_rx.hprot   = 0;               /* No read/write source protection */

        /* Setting up channel */
        dma_cfg_ch_tx.highPri = false;              /* Normal priority */
        dma_cfg_ch_tx.enableInt = false;            /* No interupt enabled for callback functions */
        dma_cfg_ch_tx.select = DMAREQ_LEUART0_TXBL; /* Set LEUART0 RX data avalible as source of DMA signals */
        dma_cfg_ch_tx.cb = NULL;                    /* No callback funtion */

        /* Setting up channel descriptor */
        dma_cfg_descr_tx.dstInc  = dmaDataIncNone;  /* Do not increment destination address */
        dma_cfg_descr_tx.srcInc  = dmaDataInc1;     /* Increment source address by one byte */
        dma_cfg_descr_tx.size    = dmaDataSize1;    /* Data size is one byte */
        dma_cfg_descr_tx.arbRate = dmaArbitrate1;   /* Rearbitrate for each byte recieved */
        dma_cfg_descr_tx.hprot   = 0;               /* No read/write source protection */

        init_leuart(LEUART0, cmuClock_LEUART0, &leuart_init, LEUART0_LOCATION,
                LEUART0_IRQn, &rxdata_leuart0,
                DMA_LE_UART0_TX, &dma_cfg_ch_tx, &dma_cfg_descr_tx,
                DMA_LE_UART0_RX, &dma_cfg_ch_rx, &dma_cfg_descr_rx);
#else
        return;
#endif
    }
    else if (port_num == EFM32GG_LEUART1)
    {
#ifdef EFM32GG_LEUART1_FOR_UART_M
        GPIO_PinModeSet(LEUART1_TXD_PORT, LEUART1_TXD_PIN, gpioModePushPull, 1);
        GPIO_PinOutSet(LEUART1_TXD_PORT, LEUART1_TXD_PIN);
        GPIO_PinModeSet(LEUART1_RXD_PORT, LEUART1_RXD_PIN, gpioModeInputPull, 0);

        leuart_init.baudrate = LEUART1_BR;
        leuart_init.databits = LEUART1_DATABITS;
        leuart_init.parity = LEUART1_PARITY;
        leuart_init.stopbits = LEUART1_STOPBITS;

        /* Setting up channel */
        dma_cfg_ch_rx.highPri   = false;                     /* Normal priority */
        dma_cfg_ch_rx.enableInt = false;                     /* No interupt enabled for callback functions */
        dma_cfg_ch_rx.select    = DMAREQ_LEUART1_RXDATAV;    /* Set LEUART0 RX data avalible as source of DMA signals */
        dma_cfg_ch_rx.cb        = NULL;                      /* No callback funtion */

        /* Setting up channel descriptor */
        dma_cfg_descr_rx.dstInc  = dmaDataInc1;       /* Increment destination address by one byte */
        dma_cfg_descr_rx.srcInc  = dmaDataIncNone;    /* Do no increment source address  */
        dma_cfg_descr_rx.size    = dmaDataSize1;      /* Data size is one byte */
        dma_cfg_descr_rx.arbRate = dmaArbitrate1;     /* Rearbitrate for each byte recieved*/
        dma_cfg_descr_rx.hprot   = 0;                 /* No read/write source protection */

        /* Setting up channel */
        dma_cfg_ch_tx.highPri   = false;                     /* Normal priority */
        dma_cfg_ch_tx.enableInt = false;                     /* No interupt enabled for callback functions */
        dma_cfg_ch_tx.select    = DMAREQ_LEUART1_TXBL;       /* Set LEUART0 RX data avalible as source of DMA signals */
        dma_cfg_ch_tx.cb        = NULL;                      /* No callback funtion */

        /* Setting up channel descriptor */
        dma_cfg_descr_tx.dstInc  = dmaDataIncNone;    /* Do not increment destination address */
        dma_cfg_descr_tx.srcInc  = dmaDataInc1;       /* Increment source address by one byte */
        dma_cfg_descr_tx.size    = dmaDataSize1;      /* Data size is one byte */
        dma_cfg_descr_tx.arbRate = dmaArbitrate1;     /* Rearbitrate for each byte recieved */
        dma_cfg_descr_tx.hprot   = 0;                 /* No read/write source protection */

        init_leuart(LEUART1, cmuClock_LEUART1, &leuart_init, LEUART1_LOCATION,
                LEUART1_IRQn, &rxdata_leuart1,
                DMA_LE_UART1_TX, &dma_cfg_ch_tx, &dma_cfg_descr_tx,
                DMA_LE_UART1_RX, &dma_cfg_ch_rx, &dma_cfg_descr_rx);
#else
        return;
#endif
    }
    else
    {
        return;
    }

    nos_uart_putc(port_num, 0); // To set USARTn->STATUS.TXC.
}

#endif	// UART_M
