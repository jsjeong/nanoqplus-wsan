/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Real Time Counter Module
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 2. 23.
 *
 * @note It is imported from sample code provided by Nuri Telecom Co., Ltd.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "rtc.h"

#ifdef RTC_M

#include <string.h>
#include "em_cmu.h"
#include "em_rtc.h"
#include "pwr_mgr.h"

const UINT8 ucaCalenderMonth[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

//__no_init __root _FULL_DATE_TIME sInternalRtc @ 0x20001000;
//__no_init __root UINT16          wReservedCrc @ 0x20001014;

_FULL_DATE_TIME sInternalRtc;
UINT16          wReservedCrc;

void nos_rtc_init(void)
{
}

BOOL nos_is_leap_year(UINT16 year)
{
    if (!(year % 4) || (year % 100) && !(year % 400))
        return TRUE;
    else
        return FALSE;
}

void nos_increase_fulltime_second(void *args)
{
    UINT8  maxDay;
    UINT16 tempYear;
    _FULL_DATE_TIME *utc = (_FULL_DATE_TIME *) args;

    utc->ucSecond = utc->ucSecond + 1;
    if(utc->ucSecond > 59)
    {
        utc->ucSecond = 0;
        utc->ucMinute = utc->ucMinute + 1;
        if(utc->ucMinute > 59)
        {
            utc->ucMinute = 0;
            utc->ucHour = utc->ucHour + 1;
            if(utc->ucHour > 23)
            {
                utc->ucHour = 0;
                utc->ucDay = utc->ucDay + 1;
                tempYear = utc->wYear;
                maxDay = ucaCalenderMonth[utc->ucMonth] + nos_is_leap_year(tempYear);
                if(utc->ucDay > maxDay)
                {
                    utc->ucDay= 1;
                    utc->ucMonth = utc->ucMonth + 1;
                    if(utc->ucMonth > 12)
                    {
                        utc->ucMonth = 1;
                        utc->wYear = utc->wYear + 1;
                    }
                }
            }
        }
    }
}

void nos_set_time(_DATE_TIME *datetime)
{
    NVIC_DisableIRQ(RTC_IRQn);
    sInternalRtc.wYear = datetime->wYear;
    sInternalRtc.ucMonth = datetime->ucMonth;
    sInternalRtc.ucDay = datetime->ucDay;
    sInternalRtc.ucHour = datetime->ucHour;
    sInternalRtc.ucMinute = datetime->ucMinute;
    sInternalRtc.ucSecond = datetime->ucSecond;
    NVIC_EnableIRQ(RTC_IRQn);
}

void nos_get_time(_DATE_TIME *datetime)
{
    NVIC_DisableIRQ(RTC_IRQn);
    datetime->wYear = sInternalRtc.wYear;
    datetime->ucMonth = sInternalRtc.ucMonth;
    datetime->ucDay = sInternalRtc.ucDay;
    datetime->ucHour = sInternalRtc.ucHour;
    datetime->ucMinute = sInternalRtc.ucMinute;
    datetime->ucSecond = sInternalRtc.ucSecond;
    NVIC_EnableIRQ(RTC_IRQn);
}

void nos_set_utc(_FULL_DATE_TIME *utc)
{
    NVIC_DisableIRQ(RTC_IRQn);
    memcpy(&sInternalRtc, utc, sizeof(_FULL_DATE_TIME));
    NVIC_EnableIRQ(RTC_IRQn);
}

void nos_get_utc(_FULL_DATE_TIME *utc)
{
    NVIC_DisableIRQ(RTC_IRQn);
    memcpy(utc, &sInternalRtc, sizeof(_FULL_DATE_TIME));
    NVIC_EnableIRQ(RTC_IRQn);
}

void rtc_handler(void)
{
    nos_mcu_set_power_mode(NOS_POWER_MODE_IDLE);

    //timerState = TIMER_ISR_ACTION_REQUIRED;

    nos_increase_fulltime_second(&sInternalRtc);
    //wReservedCrc = Calculate_ReservedSram_Crc();
}

#endif /* RTC_M */
