// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Real Time Counter Module
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 2. 23.
 *
 * @note It is imported from sample code provided by Nuri Telecom Co., Ltd.
 */

#ifndef RTC_H_
#define RTC_H_

#include "kconf.h"

#ifdef RTC_M
#include "nos_common.h"

#pragma pack(1)
struct  _full_date_time
{
    UINT16 wTimeZone;
    UINT16 wDst;
    UINT16 wYear;
    UINT8  ucMonth;
    UINT8  ucDay;
    UINT8  ucHour;
    UINT8  ucMinute;
    UINT8  ucSecond;
};
#pragma pack()
typedef struct  _full_date_time _FULL_DATE_TIME;

#pragma pack(1)
struct  _date_time
{
    UINT16 wYear;
    UINT8  ucMonth;
    UINT8  ucDay;
    UINT8  ucHour;
    UINT8  ucMinute;
    UINT8  ucSecond;
};
#pragma pack()
typedef struct  _date_time _DATE_TIME;

void nos_rtc_init(void);
BOOL nos_is_leap_year(UINT16 year);
void nos_increase_fulltime_second(void *args);
void nos_set_time(_DATE_TIME *datetime);
void nos_get_time(_DATE_TIME *datetime);
void nos_set_utc(_FULL_DATE_TIME *utc);
void nos_get_utc(_FULL_DATE_TIME *utc);

extern _FULL_DATE_TIME sInternalRtc;
extern UINT16          wReservedCrc;
#endif /* RTC_M */
#endif /* RTC_H_ */
