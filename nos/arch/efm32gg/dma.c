/**************************************************************************//**
* @file dma.c
* @brief UART utility routine.
* @author NURI Telecom Co., Ltd.
* @version v1.0 (TripleK / 12.04.17)
******************************************************************************
* @section License
* <b>(C) Copyright 2012 by NURI Corporation, http://www.nuritelecom.com</b>
******************************************************************************
*
*
*****************************************************************************/

//=============================================================================
//=============================================================================
// Includes
#include "dma.h"

#ifdef DMA_M
#include "em_cmu.h"
/* DMA control block, must be aligned to 256. */
#if defined (__ICCARM__)
#pragma data_alignment=256
DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMA_CHAN_COUNT * 2];
#elif defined (__CC_ARM)
DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMA_CHAN_COUNT * 2] __attribute__ ((aligned(256)));
#elif defined (__GNUC__)
DMA_DESCRIPTOR_TypeDef dmaControlBlock[DMA_CHAN_COUNT * 2] __attribute__ ((aligned(256)));
#else
#error Undefined toolkit, need to define alignment
#endif

void nos_dma_init(void)
{
    DMA_Init_TypeDef dmaInit;
    dmaInit.hprot = 0;
    dmaInit.controlBlock = dmaControlBlock;

    /* Initializing DMA, channel and desriptor */
    CMU_ClockEnable(cmuClock_DMA, true);        /* Enable DMA clock */
    DMA_Init(&dmaInit);
}

#endif /* DMA_M */
