// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief HAL for scheduler.
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 3. 26.
 */

#include "hal_sched.h"

#ifdef KERNEL_M

#include "arch.h"
#include "sched.h"
#include "hal_sched.h"
#include "pwr_mgr.h"
#include "critical_section.h"
#include "em_rtc.h"
#include "em_cmu.h"

void nos_sched_hal_init(void)
{
    /* Init PendSV */
    // Clear a pending interrupt.
    SCB->ICSR |= SCB_ICSR_PENDSVCLR_Msk;
    // Set the priority of PendSV to the lowest priority.
    NVIC_SetPriority(PendSV_IRQn, 7);
}

void nos_sched_timer_start(void)
{
    RTC_CompareSet(1, CMU_ClockFreqGet(cmuClock_RTC) * SCHED_TIMER_MS / 1000);
    RTC_IntEnable(RTC_IF_COMP1);
    RTC_Enable(true);
}

void systick_handler(void)
{
    NOS_WDOG_FEED();
    nos_mcu_set_power_mode(NOS_POWER_MODE_IDLE);

    nos_sched_handler();

    if (_sched_enable_bit)
    {
        NOS_CTX_SW_PENDING_SET();
    }
}

void PendSV_Handler(void)
{
    NOS_DISABLE_GLOBAL_INTERRUPT();
    nos_ctx_sw_handler();   // may return here with global interrupt SET
    NOS_ENABLE_GLOBAL_INTERRUPT();
}

#endif // KERNEL_M
