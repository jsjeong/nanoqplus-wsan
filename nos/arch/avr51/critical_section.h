// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Misun Yu (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 6. 15.
 */

#ifndef CRITICAL_SECTION_H
#define CRITICAL_SECTION_H
#include "nos_common.h"

extern volatile UINT8 _nested_intr_cnt;		// the number of cli() or sli() that is called

#define NOS_DISABLE_GLOBAL_INTERRUPT() 	__asm__ __volatile__ ("cli" ::)
#define NOS_ENABLE_GLOBAL_INTERRUPT() 	__asm__ __volatile__ ("sei" ::)

#define NOS_ENTER_CRITICAL_SECTION() \
    do { \
        NOS_DISABLE_GLOBAL_INTERRUPT(); \
        ++_nested_intr_cnt; \
    } while (0)

#define NOS_EXIT_CRITICAL_SECTION() \
    do { \
        --_nested_intr_cnt; \
        if (!_nested_intr_cnt) \
        NOS_ENABLE_GLOBAL_INTERRUPT(); \
    } while (0)

#define NOS_ENTER_ISR()		(++_nested_intr_cnt)
#define NOS_EXIT_ISR()		(--_nested_intr_cnt)

#define NOS_IS_CTX_SW_ALLOWABLE() (!_nested_intr_cnt)

#endif	// CRITICAL_SECTION_H
