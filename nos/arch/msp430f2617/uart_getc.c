// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * UART input for MSP430F2617.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 30.
 */

#include "uart.h"

#ifdef UART_INPUT_M

#if defined __MSPGCC__
#include <msp430.h>
#include <legacymsp430.h>
#elif defined __ICC430__
#include <io430.h>
#endif

#include "critical_section.h"

volatile char uart_rx_char;

#ifdef MSP430F2617_UCA0_ASYNC
void (*nos_uart0_rx_callback)(UINT8,char);
#endif

#ifdef MSP430F2617_UCA1_ASYNC
void (*nos_uart1_rx_callback)(UINT8,char);
#endif

void nos_uart_set_getc_callback(UINT8 port_num, void (*func)(UINT8, char))
{
#ifdef MSP430F2617_UCA0_ASYNC
    if (port_num == MSP430F2617_UCA0)
    {
        nos_uart0_rx_callback = func;
        return;
    }
#endif

#ifdef MSP430F2617_UCA1_ASYNC
    if (port_num == MSP430F2617_UCA1)
    {
        nos_uart1_rx_callback = func;
        return;
    }
#endif
}

void (*nos_uart_get_getc_callback(UINT8 port_num))(UINT8, char)
{
#ifdef MSP430F2617_UCA0_ASYNC
    if (port_num == MSP430F2617_UCA0)
    {
        return nos_uart0_rx_callback;
    }
#endif

#ifdef MSP430F2617_UCA1_ASYNC
    if (port_num == MSP430F2617_UCA1)
    {
        return nos_uart1_rx_callback;
    }
#endif
    
    return NULL;
}

void nos_uart_enable_rx_intr(UINT8 port_num)
{
#ifdef MSP430F2617_UCA0_ASYNC
    if (port_num == MSP430F2617_UCA0)
    {
        IE2 |= UCA0RXIE;
        return;
    }
#endif

#ifdef MSP430F2617_UCA1_ASYNC
    if (port_num == MSP430F2617_UCA1)
    {
        UC1IE |= UCA1RXIE;
        return;
    }
#endif
}

void nos_uart_disable_rx_intr(UINT8 port_num)
{
#ifdef MSP430F2617_UCA0_ASYNC
    if (port_num == MSP430F2617_UCA0)
    {
        IE2 &= ~UCA0RXIE;
        return;
    }
#endif

#ifdef MSP430F2617_UCA1_ASYNC
    if (port_num == MSP430F2617_UCA1)
    {
        UC1IE &= ~UCA1RXIE;
        return;
    }
#endif
}

BOOL nos_uart_rx_intr_is_set(UINT8 port_num)
{
#ifdef MSP430F2617_UCA0_ASYNC
    if (port_num == MSP430F2617_UCA0)
    {
        return (IFG2 & UCA0RXIFG) ? TRUE : FALSE;
    }
#endif

#ifdef MSP430F2617_UCA1_ASYNC
    if (port_num == MSP430F2617_UCA1)
    {
        return (UC1IFG & UCA1RXIFG) ? TRUE : FALSE;
    }
#endif
    
    return FALSE;
}

#ifdef MSP430F2617_UCA0_ASYNC
#ifdef __MSPGCC__
wakeup interrupt (USCIAB0RX_VECTOR) USCIAB0RX_ISR(void)
#elif defined __ICC430__
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCIAB0RX_ISR(void)
#endif
{
    NOS_ENTER_ISR();
    uart_rx_char = UCA0RXBUF;
    if (nos_uart0_rx_callback)
    {
        nos_uart0_rx_callback(MSP430F2617_UCA0, uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#ifdef MSP430F2617_UCA1_ASYNC
#ifdef __MSPGCC__
wakeup interrupt (USCIAB1RX_VECTOR) USCIAB1RX_ISR(void)
#elif defined __ICC430__
#pragma vector=USCIAB1RX_VECTOR
__interrupt void USCIAB1RX_ISR(void)
#endif
{
    NOS_ENTER_ISR();
    uart_rx_char = UCA1RXBUF;
    if (nos_uart1_rx_callback)
    {
        nos_uart1_rx_callback(MSP430F2617_UCA1, uart_rx_char);
    }
    NOS_EXIT_ISR();
}
#endif

#endif /* UART_INPUT_M */
