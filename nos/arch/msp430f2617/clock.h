// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * MSP430F2617 Clock Initialization
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 29.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef CLOCK_H
#define CLOCK_H

#include "nos_common.h"

// The clock speed unit is kHz. (1k = 1024)
#define DCOCLK          ((UINT16)4096)
#define SMCLK           2048
#define TACLK           SMCLK

#define LFXT1CLK        ((UINT16)32)
#define ACLK            ((UINT16)32)
#define TBCLK           ACLK

/* These are unstable. Do not use these.
#define SMCLK		( DCOCLK >> ((BCSCTL2&(DIVS1|DIVS0)) >> 1) )
#define ACLK			( LFXT1CLK >> ((BCSCTL1&(DIVA1|DIVA0)) >> 4) )
#define TACLK		( SMCLK >> ((TACTL&(ID1|ID0)) >> 6) )
#define TBCLK		( ACLK >>((TBCTL&(ID1|ID0)) >> 6) )
*/

void nos_clock_init(void);
void nos_adj_dco_clock(void);

#endif // CLOCK_H
