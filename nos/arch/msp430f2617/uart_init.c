// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * UART initialization for MSP430F2617.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 30.
 */

#include "kconf.h"

#ifdef UART_M

#if defined __ICC430__
#include <io430.h>
#endif

#include "uart.h"
#include "platform.h"
#include "arch.h"
#include "clock.h"

//TODO Simplify setting UCxBR and UCxMCTL to a single equation instead of 'ifdef' based case-by-case definitions.

#ifdef MSP430F2617_UCA0_ASYNC
#if defined MSP430F2617_UCA0_BR_9600
#define UCA0_BR 9600
#elif defined MSP430F2617_UCA0_BR_19200
#define UCA0_BR 19200
#elif defined MSP430F2617_UCA0_BR_38400
#define UCA0_BR 38400
#elif defined MSP430F2617_UCA0_BR_57600
#define UCA0_BR 57600
#elif defined MSP430F2617_UCA0_BR_115200
#define UCA0_BR 115200
#elif defined MSP430F2617_UCA0_BR_230400
#define UCA0_BR 230400
#elif defined MSP430F2617_UCA0_BR_380400
#define UCA0_BR 380400
#elif defined MSP430F2617_UCA0_BR_460800
#define UCA0_BR 460800
#endif
#endif //MSP430F2617_UCA0_ASYNC

#ifdef MSP430F2617_UCA1_ASYNC
#if defined MSP430F2617_UCA1_BR_9600
#define UCA1_BR 9600
#elif defined MSP430F2617_UCA1_BR_19200
#define UCA1_BR 19200
#elif defined MSP430F2617_UCA1_BR_38400
#define UCA1_BR 38400
#elif defined MSP430F2617_UCA1_BR_57600
#define UCA1_BR 57600
#elif defined MSP430F2617_UCA1_BR_115200
#define UCA1_BR 115200
#elif defined MSP430F2617_UCA1_BR_230400
#define UCA1_BR 230400
#elif defined MSP430F2617_UCA1_BR_380400
#define UCA1_BR 380400
#elif defined MSP430F2617_UCA1_BR_460800
#define UCA1_BR 460800
#endif
#endif //MSP430F2617_UCA0_ASYNC

/**
 * Low-frequency baud rate generation is used.
 *
 * UCBRx = SMCLK * 1024 / BR
 * UCBRSx = round((UCBRx - INT(UCBRx)) * 8)
 * UCBRFx = 0, UCOS16 = 0
 */

void nos_uart_init(UINT8 port_num)
{
#ifdef MSP430F2617_UCA0_ASYNC
    if (port_num == MSP430F2617_UCA0)
    {
        UCA0CTL1 = UCSWRST;  //Hold in reset state and clear other bits.
        
        UCA0CTL1 |= UCSSEL1; //Clock source: SMCLK
        UCA0CTL0 = 0;        // 8-N-1 UART

        //Use UCA0 peripheral not genral I/O for P3.4, P3.5.
        P3SEL |= (1 << 4) | (1 << 5);

#if defined MSP430F2617_UCA0_BR_9600

#if (SMCLK == 1024)
        UCA0BR0 = 0x6D;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_2;
#elif (SMCLK == 2048)
        UCA0BR0 = 0xDA;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_4;
#elif (SMCLK == 4096)
        UCA0BR0 = 0xB4;
        UCA0BR1 = 0x01;
        UCA0MCTL = UCBRS_7;
#endif

#elif defined MSP430F2617_UCA0_BR_19200

#if (SMCLK == 1024)
        UCA0BR0 = 0x36;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_5;
#elif (SMCLK == 2048)
        UCA0BR0 = 0x6D;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_2;
#elif (SMCLK == 4096)
        UCA0BR0 = 0xDA;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_4;
#endif

#elif defined MSP430F2617_UCA0_BR_38400

#if (SMCLK == 1024)
        UCA0BR0 = 0x1B;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_2;
#elif (SMCLK == 2048)
        UCA0BR0 = 0x36;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_5;
#elif (SMCLK == 4096)
        UCA0BR0 = 0x6D;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_2;
#endif

#elif defined MSP430F2617_UCA0_BR_57600

#if (SMCLK == 1024)
        UCA0BR0 = 0x12;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_2;
#elif (SMCLK == 2048)
        UCA0BR0 = 0x24;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_3;
#elif (SMCLK == 4096)
        UCA0BR0 = 0x48;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_7;
#endif

#elif defined MSP430F2617_UCA0_BR_115200

#if (SMCLK == 1024)
        UCA0BR0 = 0x09;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_1;
#elif (SMCLK == 2048)
        UCA0BR0 = 0x12;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_2;
#elif (SMCLK == 4096)
        UCA0BR0 = 0x24;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_3;
#endif

#elif defined MSP430F2617_UCA0_BR_230400

#if (SMCLK == 1024)
        UCA0BR0 = 0x04;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_4;
#elif (SMCLK == 2048)
        UCA0BR0 = 0x09;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_1;
#elif (SMCLK == 4096)
        UCA0BR0 = 0x12;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_2;
#endif

#elif defined MSP430F2617_UCA0_BR_380400

#if (SMCLK == 1024)
        UCA0BR0 = 0x02;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_6;
#elif (SMCLK == 2048)
        UCA0BR0 = 0x05;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_4;
#elif (SMCLK == 4096)
        UCA0BR0 = 0x0B;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_0;
#endif

#elif defined MSP430F2617_UCA0_BR_460800

#if (SMCLK == 1024)
        UCA0BR0 = 0x02;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_2;
#elif (SMCLK == 2048)
        UCA0BR0 = 0x04;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_4;
#elif (SMCLK == 4096)
        UCA0BR0 = 0x09;
        UCA0BR1 = 0x00;
        UCA0MCTL = UCBRS_1;
#endif

#endif //MSP430F2617_UCA0_BR_xxx

        // Interrupt flag initialization.
        IFG2 |= UCA1TXIFG;  // TX buffer is empty.
        IFG2 &= ~UCA1RXIFG; // RX buffer is empty.

        // USART reset released for operation
        UCA0CTL1 &= ~UCSWRST;

        return;
    }
#endif //MSP430F2617_UCA0_ASYNC

#ifdef MSP430F2617_UCA1_ASYNC
    if (port_num == MSP430F2617_UCA1)
    {
        UCA1CTL1 = UCSWRST;  //Hold in reset state and clear other bits.
        
        UCA1CTL1 |= UCSSEL1; //Clock source: SMCLK
        UCA1CTL0 = 0;        // 8-N-1 UART

        //Use UCA1 peripheral not genral I/O for P3.6, P3.7.
        P3SEL |= (1 << 6) | (1 << 7);

#if defined MSP430F2617_UCA1_BR_9600

#if (SMCLK == 1024)
        UCA1BR0 = 0x6D;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_2;
#elif (SMCLK == 2048)
        UCA1BR0 = 0xDA;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_4;
#elif (SMCLK == 4096)
        UCA1BR0 = 0xB4;
        UCA1BR1 = 0x01;
        UCA1MCTL = UCBRS_7;
#endif

#elif defined MSP430F2617_UCA1_BR_19200

#if (SMCLK == 1024)
        UCA1BR0 = 0x36;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_5;
#elif (SMCLK == 2048)
        UCA1BR0 = 0x6D;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_2;
#elif (SMCLK == 4096)
        UCA1BR0 = 0xDA;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_4;
#endif

#elif defined MSP430F2617_UCA1_BR_38400

#if (SMCLK == 1024)
        UCA1BR0 = 0x1B;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_2;
#elif (SMCLK == 2048)
        UCA1BR0 = 0x36;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_5;
#elif (SMCLK == 4096)
        UCA1BR0 = 0x6D;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_2;
#endif

#elif defined MSP430F2617_UCA1_BR_57600

#if (SMCLK == 1024)
        UCA1BR0 = 0x12;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_2;
#elif (SMCLK == 2048)
        UCA1BR0 = 0x24;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_3;
#elif (SMCLK == 4096)
        UCA1BR0 = 0x48;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_7;
#endif

#elif defined MSP430F2617_UCA1_BR_115200

#if (SMCLK == 1024)
        UCA1BR0 = 0x09;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_1;
#elif (SMCLK == 2048)
        UCA1BR0 = 0x12;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_2;
#elif (SMCLK == 4096)
        UCA1BR0 = 0x24;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_3;
#endif

#elif defined MSP430F2617_UCA1_BR_230400

#if (SMCLK == 1024)
        UCA1BR0 = 0x04;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_4;
#elif (SMCLK == 2048)
        UCA1BR0 = 0x09;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_1;
#elif (SMCLK == 4096)
        UCA1BR0 = 0x12;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_2;
#endif

#elif defined MSP430F2617_UCA1_BR_380400

#if (SMCLK == 1024)
        UCA1BR0 = 0x02;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_6;
#elif (SMCLK == 2048)
        UCA1BR0 = 0x05;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_4;
#elif (SMCLK == 4096)
        UCA1BR0 = 0x0B;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_0;
#endif

#elif defined MSP430F2617_UCA1_BR_460800

#if (SMCLK == 1024)
        UCA1BR0 = 0x02;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_2;
#elif (SMCLK == 2048)
        UCA1BR0 = 0x04;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_4;
#elif (SMCLK == 4096)
        UCA1BR0 = 0x09;
        UCA1BR1 = 0x00;
        UCA1MCTL = UCBRS_1;
#endif

#endif //MSP430F2617_UCA1_BR_xxxx

        // Interrupt flag initialization.
        UC1IFG |= UCA1TXIFG;  // TX buffer is empty.
        UC1IFG &= ~UCA1RXIFG; // RX buffer is empty.
        
        // Reset released for operation
        UCA1CTL1 &= ~UCSWRST;

        return;
    }
#endif //MSP430F2617_UCA1_ASYNC
}

#endif // UART_M
