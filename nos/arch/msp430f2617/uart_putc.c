// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_putc.c
 * UART output for MSP430F2617.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 30.
 */

#include "kconf.h"
#ifdef UART_M

#ifdef __MSPGCC__
#include <msp430.h>
#elif defined __ICC430__
#include "io430.h"
#endif

#include "uart.h"
#include <stdio.h>

void nos_uart_putc(UINT8 port_num, char byte)
{
#ifdef MSP430F2617_UCA0_ASYNC
    if (port_num == MSP430F2617_UCA0)
    {
        while (!(IFG2 & UCA0TXIFG)); // Wait until UCA0TXBUF is empty.
        UCA0TXBUF = byte;
        return;
    }
#endif

#ifdef MSP430F2617_UCA1_ASYNC
    if (port_num == MSP430F2617_UCA1)
    {
        while (!(UC1IFG & UCA1TXIFG)); // Wait until UCA1TXBUF is empty.
        UCA1TXBUF = byte;
        return;
    }
#endif
}

#endif // UART_M 
