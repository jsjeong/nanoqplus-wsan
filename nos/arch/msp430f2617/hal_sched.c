// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Hardware timer abstraction for scheduling in MSP430F2617.
 * 
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 29.
 */

#include "hal_sched.h"

#ifdef KERNEL_M

#ifdef __MSPGCC__
#include <msp430.h>
#include <legacymsp430.h>
#elif defined __ICC430__
#include "io430.h"
#endif

#include "critical_section.h"
#include "sched.h"
#include "pwr_mgr.h"

// SCHED_TICKS = 32768 (Hz) * SCHED_TIMER_MS (ms) / 1000 (sec)
#if defined SCHED_PERIOD_100
#define SCHED_TICKS             3276
#elif defined SCHED_PERIOD_32
#define SCHED_TICKS             1048
#elif defined SCHED_PERIOD_10
#define SCHED_TICKS             327
#elif defined SCHED_PERIOD_5
#define SCHED_TICKS             163
#else
#error "Scheduler time slice is not defined."
#endif

extern UINT8 nos_ctx_sw_intr_exists;

void nos_sched_hal_init(void)
{
    TBCCR0 = SCHED_TICKS;
}

void nos_sched_timer_start(void)
{
    TBR = 0;
    TBCCTL0 &= ~CCIFG; // Clear Timer_B CCR0 interrupt
    TBCCTL0 |= CCIE;   // Enable Timer_B CCR0 interrupt
}

#ifdef __MSPGCC__
wakeup interrupt (TIMERB0_VECTOR) TBCCR0_ISR(void)
#elif defined __ICC430__
#pragma vector=TIMERB0_VECTOR
__interrupt void Timer_B0_ISR(void)
#endif
{
    NOS_ENTER_ISR();
    TBCCR0 = TBR + SCHED_TICKS;
    nos_mcu_set_power_mode(MSP430_POWER_MODE_ACTIVE);
    nos_sched_handler();
    NOS_EXIT_ISR();
    
    nos_ctx_sw_handler();
}

#endif //KERNEL_M
