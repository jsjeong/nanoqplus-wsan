// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Basic definitions related to MCU
 * @author Haeyong Kim (ETRI)
 * @date 2012. 2. 22.
 */

#ifndef ARCH_H
#define ARCH_H
#include "kconf.h"

#include "nos_common.h"
#include "stm32f10x_gpio.h"

#define SYSTEM_STACK_SIZE        0x1000
#define DEFAULT_STACK_SIZE       0x400

#if (defined STM32F103CB || defined STM32F103RB)
#define RAMEND 0x20004FFF    /* 20KB */

#elif (defined STM32F101RC || defined STM32F103VC)
#define RAMEND 0x20007FFF    /* 48KB */

#elif (defined STM32F103RF)
#define RAMEND 0x20017FFF    /* 96KB */
#endif

#define SYSTEM_STACK_START_ADDR  (RAMEND - SYSTEM_STACK_SIZE + 1)

void nos_arch_init(void);

#define NOS_RESET()                             \
    do { \
        NVIC_SystemReset(); \
        while(1); \
    } while(0)

#endif // ~ARCH_H
