# -*- mode:Makefile; -*-
###############################################################################
# Toolchain configuration for STM32F10x                                       #
# Author: Jongsoo Jeong (ETRI)                                                #
###############################################################################
NOSDIR += $(NOS_HOME)/nos/arch/arm-cortex-m3
MCU_CLASS = arm-cortex-m3

PREDEFINES+=\
	USE_STDPERIPH_DRIVER\
	$(APP_PREDEFINES)

# CyaSSL related
PREDEFINES += SIZEOF_LONG_LONG=8

SUPPORT_DIR=$(NOS_HOME)/support/stm32/STM32F10x_StdPeriph_Lib_V3.5.0

SUPPORT_SRCDIR=\
	$(SUPPORT_DIR)/Libraries/STM32F10x_StdPeriph_Driver/src

INCLUDES=\
	$(SUPPORT_DIR)/Libraries/STM32F10x_StdPeriph_Driver/inc

ifeq ($(CONFIG_STM32F10X_BUILD_IAR),y)
	INCLUDES += $(SUPPORT_DIR)/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x
	USE_IAR_CMSIS = y

else ifeq ($(CONFIG_STM32F10X_BUILD_KEIL),y)
	INCLUDES += $(KEIL_HOME)/ARM/CMSIS/Include

	UV4_VENDOR=STMicroelectronics
	UV4_CPU=IRAM(0x20000000-0x20017FFF) IROM(0x8000000-0x80BFFFF) CLOCK(8000000) CPUTYPE("Cortex-M3")
	UV4_FLASH_DRIVER_DLL=UL2CM3(-O14 -S0 -C0 -N00("ARM Cortex-M3") -D00(1BA00477) -L00(4) -FO7 -FD20000000 -FC800 -FN1 -FF0STM32F10x_1024 -FS08000000 -FL0C0000)
	UV4_REGISTER_FILE=stm32f10x.h
	UV4_REGISTER_FILE_PATH=ST\\STM32F10x\\
	UV4_DB_REGISTER_FILE_PATH=ST\\STM32F10x\\
	UV4_ADS_CPU_TYPE="Cortex-M3"

#CyaSSL related
	PREDEFINES+=CYASSL_MDK_ARM KEIL_INTRINSICS
	INCLUDES += $(NOS_HOME)/support/src/MDK-ARM

else ifeq ($(CONFIG_STM32F10X_BUILD_GCC),y)
	SUPPORT_SRCDIR += $(SUPPORT_DIR)/Libraries/CMSIS/CM3/CoreSupport
	INCLUDES += $(SUPPORT_DIR)/Libraries/CMSIS/CM3/CoreSupport\
		$(SUPPORT_DIR)/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x

	CC = arm-none-eabi-gcc
	LD = arm-none-eabi-gcc
	OBJCOPY = arm-none-eabi-objcopy
	OBJDUMP = arm-none-eabi-objdump
	SIZE = arm-none-eabi-size

	CPFLAGS =\
		-c\
		-mcpu=cortex-m3\
		-mthumb\
		-mfix-cortex-m3-ldrd\
		-O1\
		-std=gnu99\
		-Wall\
		-Wa,-ahlms=$(notdir $(<:.c=.lst))\
		-DNOS

	ASFLAGS = -c -mcpu=cortex-m3 -mthumb -Wa,-ahlms=$(notdir $(<:.s=.lst))

	ifeq ($(OS),Cygwin)
		MAP = "$(shell cygpath -a -m "$(shell readlink -f "$(PWD)/$(TRG).map")")"
	else
		MAP = $(PWD)/$(TRG).map
	endif

	LDFLAGS =\
		-mcpu=cortex-m3\
		-mthumb\
		-Wl,-Map,$(MAP),--cref,--gc-sections\
		--specs=nano.specs

	ifeq ($(findstring CYGWIN, $(shell uname)),CYGWIN)
		LDFLAGS += -L"$(shell cygpath -a -m $(NOS_HOME)/lib/arm-cortex-m3/gcc)"
	else
		LDFLAGS += -L$(NOS_HOME)/lib/arm-cortex-m3/gcc
	endif

	OBJCOPYOPTS = -O ihex
	OBJDUMPOPTS = -h -S

endif

stdperiph_install:
	@if [ ! -d $(SUPPORT_DIR) ]; then\
		make -C $(NOS_HOME)/support/stm32 f10x;\
		echo;\
		echo "### NOTE ###";\
		echo "An additional package is installed for STM32F10x. Please, re-run 'make'.";\
		false;\
	fi
