// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief HAL for threads.
 * @author Haeyong Kim (ETRI)
 * @date 2013. 1. 22.
 */

#ifndef __HAL_THREAD_H__
#define __HAL_THREAD_H__
#include "kconf.h"
#ifdef KERNEL_M

#ifdef __ICCARM__
#include <intrinsics.h>
#endif

#include "nos_common.h"
#include "arch.h"

typedef UINT32 STACK_ENTRY;	// The stack is 4 byte-contiguous array
typedef UINT32 *STACK_PTR;	// stack pointer (32 bit wide)

#ifdef THREAD_M
struct arch_dep_states
{
    UINT32 delay_tick;
    UINT16 delay_timer_counter;
    BOOL delaying;
};


// preserve GPRS. Push R7-R4, R11-R8, respectively.
// R0-R3 and R12 are scratch registers in IAR compiler. Use them to 'PUSH' for R8-R11
// PRIMASK is stored for the first context switching for a new thread
#if defined __ICCARM__
#define NOS_THREAD_SAVE_STATE()                 \
    {                                           \
        __asm ("PUSH {LR}");                    \
        __asm ("MRS R0,PRIMASK\n"               \
               "PUSH {R0}");                    \
        __asm ("MOV R0,R8\n");                  \
        __asm ("MOV R1,R9\n");                  \
        __asm ("MOV R2,R10\n");                 \
        __asm ("MOV R3,R11\n");                 \
        __asm ("PUSH {R0-R7}");                 \
    }
#elif defined __GNUC__
#define NOS_THREAD_SAVE_STATE()                 \
    {                                                    \
        __asm volatile ("PUSH {LR}");                    \
        __asm volatile ("MRS R0,PRIMASK\n"               \
                        "PUSH {R0}");                    \
        __asm volatile ("MOV R0,R8\n");                  \
        __asm volatile ("MOV R1,R9\n");                  \
        __asm volatile ("MOV R2,R10\n");                 \
        __asm volatile ("MOV R3,R11\n");                 \
        __asm volatile ("PUSH {R0-R7}");                 \
    }
#endif

// POP R8-R11, R4-R7, PRIMASK, FAULTMASK, BASEPRI respectively
#if defined __ICCARM__
#define NOS_THREAD_LOAD_STATE()                 \
    {                                           \
        __asm ("POP {R0-R7}\n");                \
        __asm ("MOV R8,R0\n");                  \
        __asm ("MOV R9,R1\n");                  \
        __asm ("MOV R10,R2\n");                 \
        __asm ("MOV R11,R3");                   \
        __asm ("POP {R0}");                     \
        __asm ("MSR PRIMASK,R0");               \
        __asm ("POP {PC}");                     \
    }
#elif defined __GNUC__
#define NOS_THREAD_LOAD_STATE()                 \
    {                                                    \
        __asm volatile ("POP {R0-R7}\n");                \
        __asm volatile ("MOV R8,R0\n");                  \
        __asm volatile ("MOV R9,R1\n");                  \
        __asm volatile ("MOV R10,R2\n");                 \
        __asm volatile ("MOV R11,R3");                   \
        __asm volatile ("POP {R0}");                     \
        __asm volatile ("MSR PRIMASK,R0");               \
        __asm volatile ("POP {PC}");                     \
    }
#endif

#define __SAVE_SP(stk_ptr)                      \
    {                                           \
        stk_ptr = (STACK_PTR)__get_MSP();       \
    }

#define __LOAD_SP(stk_ptr)                      \
    {                                           \
        __set_MSP((UINT32) stk_ptr);            \
    }

STACK_PTR nos_tcb_stack_init(void (*func)(void), STACK_PTR fos, UINT16 stack_size);

#if defined __GNUC__
void nos_context_switch_core(void) __attribute__ ((naked));
#else
void nos_context_switch_core(void);
#endif

#if defined __GNUC__
void nos_context_load_core(void) __attribute__ ((naked));
#else
void nos_context_load_core(void);
#endif

#endif // THREAD_M
#endif // KERNEL_M
#endif // !__HAL_THREAD_H__
