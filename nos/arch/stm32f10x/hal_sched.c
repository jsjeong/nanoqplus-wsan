// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Scheduler HAL
 *
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 22.
 */

#include "hal_sched.h"

#ifdef KERNEL_M

/*
 * If the platform supports low-power RTC, RTC alarm tick is used for
 * scheduling. Otherwise, SysTick is used.
 */

#ifdef LIB_UTC_CLOCK_M

void nos_sched_hal_init(void)
{
    /* Disable SysTick */
    SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;

    /* Init PendSV */
    // Clear a pending interrupt.
    SCB->ICSR |= SCB_ICSR_PENDSVCLR_Msk;
    // Set the priority of PendSV to the lowest priority. STM32 uses 4bits for priority.
    NVIC_SetPriority(PendSV_IRQn, 15);	// means preemption priority group:3, subpriority 3
}

void nos_sched_timer_start(void)
{
    //Nothing to do
}

#else //~LIB_UTC_CLOCK_M

void nos_sched_hal_init(void)
{
    SysTick->LOAD = ((SystemCoreClock / 8000 * SCHED_TIMER_MS) &
                     SysTick_LOAD_RELOAD_Msk) - 1;
    SysTick->VAL = 0;
    SysTick->CTRL = SysTick_CTRL_TICKINT_Msk;
    SysTick->CTRL &= SysTick_CLKSource_HCLK_Div8;	// means external clock : sys/8, 9MHz

    /* Init PendSV */
    // Clear a pending interrupt.
    SCB->ICSR |= SCB_ICSR_PENDSVCLR_Msk;
    // Set the priority of PendSV to the lowest priority. STM32 uses 4bits for priority.
    NVIC_SetPriority(PendSV_IRQn, 15); // means preemption priority group:3, subpriority 3
}

void nos_sched_timer_start(void)
{
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
}
#endif

#endif // KERNEL_M
