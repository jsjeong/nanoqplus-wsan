// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file arch.c
 *
 * STM32F10x MCU Initialization
 *
 * @author Haeyong Kim (ETRI)
 * @date 2013. 1. 21.
 */

#include "arch.h"
#include "platform.h"

#ifndef SYSCLK
#error "SYSCLK shoud be defined in 'platform.h'."
#endif

#define LOOP_PER_USEC  (SYSCLK / 6500000)

volatile uint8_t _nested_intr_cnt = 0;

void nos_arch_init(void)
{
}

void nos_delay_us(UINT16 us)
{
    UINT32 loops = us * LOOP_PER_USEC;
    while(loops-- != 0);
}

void nos_delay_ms(UINT16 ms)
{
    UINT32 loops = ms * LOOP_PER_USEC * 1000;
    while(loops-- != 0);
}

#ifdef __GNUC__

// Definitions for newlib OS interface
#include <errno.h>
#include <sys/stat.h>

#undef errno
extern int errno;

extern int _write(int fd, char *ptr, int len);

/**
 * Minimal implementation
 */
int _close(int fd)
{
    return -1;
}

/**
 * Minimal implementation
 */
int _fstat(int fd, struct stat *st)
{
    st->st_mode = S_IFCHR;
    return 0;
}

/**
 * Minimal implementation
 */
int _isatty(int fd)
{
    return -1;
}

/**
 * Minimal implementation
 */
int _lseek(int fd, int ptr, int dir)
{
    return 0;
}

/**
 * Minimal implementation
 */
caddr_t _sbrk(int incr)
{
    extern char _end; /* Defined by the linker */
    static char *heap_end;
    char *prev_heap_end;
    nos_led_on(0);

    if (heap_end == 0)
    {
        heap_end = &_end;
    }
    prev_heap_end = heap_end;

    if (heap_end + incr > (char *) SYSTEM_STACK_START_ADDR)
    {
        _write(1, "Heap and stack collision\n", 25);
        while (1)
        {
            nos_led_on(2);
            nos_delay_ms(100);
            nos_led_off(2);
            nos_delay_ms(100);
        }
    }
    heap_end += incr;
    return (caddr_t) prev_heap_end;
}

/**
 * Minimal implementation
 */
int _stat(const char *filepath, struct stat *st)
{
    st->st_mode = S_IFCHR;
    return 0;
}
#endif //__GNUC__

UINT16 ntohs(UINT16 a)
{
    return (((a << 8) & 0xff00) | ((a >> 8) & 0xff));
}

UINT32 ntohl(UINT32 a)
{
    return (((a << 24) & 0xff000000) |
            ((a << 8) & 0xff0000) |
            ((a >> 8) & 0xff00) |
            ((a >> 24) & 0xff));
}
