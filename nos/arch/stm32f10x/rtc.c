// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Realtime counter
 *
 * @author Jongsoo Jeong (ETRI)
 * @author Haeyong Kim (ETRI)
 * @date 2013. 6. 3.
 */

#include "rtc.h"

#if defined LIB_UTC_CLOCK_M || defined KERNEL_M

#include "stm32f10x_rcc.h"
#include "stm32f10x_rtc.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_bkp.h"

#ifdef LIB_UTC_CLOCK_M
#include "utc-clock.h"
#endif

#ifdef KERNEL_M
#include "hal_sched.h"
#endif

void nos_rtc_init(void)
{
    EXTI_InitTypeDef et;

    EXTI_ClearITPendingBit(EXTI_Line17);
    et.EXTI_Line = EXTI_Line17;
    et.EXTI_Mode = EXTI_Mode_Interrupt;
    et.EXTI_Trigger = EXTI_Trigger_Rising;
    et.EXTI_LineCmd = ENABLE;
    EXTI_Init(&et);

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
    PWR_BackupAccessCmd(ENABLE);
    BKP_DeInit();

    RCC_LSEConfig(RCC_LSE_ON);
    while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET);
    RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

    RCC_RTCCLKCmd(ENABLE);
    RTC_WaitForSynchro();
    RTC_WaitForLastTask();

    RTC_SetPrescaler(0);
    RTC_WaitForLastTask();

    RTC_ITConfig(RTC_IT_ALR, ENABLE);
    RTC_WaitForLastTask();

#ifdef KERNEL_M
    RTC_SetAlarm(RTC_GetCounter() + TICKS_SCHED);
#else
    RTC_SetAlarm(TICKS_SEC);
#endif
    RTC_WaitForLastTask();
    
    RCC_ClearFlag();
    NVIC_ClearPendingIRQ(RTCAlarm_IRQn);
}

#endif //LIB_UTC_CLOCK_M || KERNEL_M
