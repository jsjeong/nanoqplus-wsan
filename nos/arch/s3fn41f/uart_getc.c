/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "uart.h"

#ifdef UART_M

volatile UINT8 uart0_rx_char;

volatile char uart_rx_char;
void (*nos_uart0_rx_callback)(char) = NULL;
void (*nos_uart1_rx_callback)(char) = NULL;
void (*nos_uart2_rx_callback)(char) = NULL;
void (*dummy_callback)(char) = NULL;

void nos_uart_set_getc_callback(UINT8 port_num, void (*func)(char))
{
    switch (port_num)
    {
    case 0:
        nos_uart0_rx_callback = func;
        break;

    case 1:
        nos_uart1_rx_callback = func;
        break;

    case 2:
        nos_uart2_rx_callback = func;
        break;
    }
}

void nos_uart_enable_rx_intr(UINT8 port_num)
{
    switch (port_num)
    {
    case 0:
        CSP_USARTConfigInterrupt(USART0, USART_RXRDY, ENABLE);
        break;

    case 1:
        CSP_USARTConfigInterrupt(USART1, USART_RXRDY, ENABLE);
        break;

    case 2:
        CSP_USARTConfigInterrupt(USART2, USART_RXRDY, ENABLE);
        break;
    }
}

void nos_uart_disable_rx_intr(UINT8 port_num)
{
    switch(port_num)
    {
    case 0:
        CSP_USARTConfigInterrupt(USART0, USART_RXRDY, DISABLE);
        break;

    case 1:
        CSP_USARTConfigInterrupt(USART1, USART_RXRDY, DISABLE);
        break;

    case 2:
        CSP_USARTConfigInterrupt(USART2, USART_RXRDY, DISABLE);
        break;
    }
}

void (*nos_uart_get_getc_callback(UINT8 port_num))(char)
{
    switch (port_num)
    {
    case 0:
        return nos_uart0_rx_callback;

    case 1:
        return nos_uart1_rx_callback;

    case 2:
        return nos_uart2_rx_callback;

    default:
        return dummy_callback;
    }
}



#endif // UART_M 
