/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef GPIO_H
#define GPIO_H

#include "kconf.h"
#include "nos_common.h"
#include "csp.h"

#define GROUP(n)                        GROUP##n
#define IOPORT(n)                       IO_Port##n
#define PORT(n)                         GPIO##n
#define PIN(n)                          GPIO_P##n

#define NOS_GPIO_SET_IN(port, pin)      CSP_GPIOInit(PORT(port), PIN(pin), IN)
#define NOS_GPIO_SET_OUT(port, pin)     CSP_GPIOInit(PORT(port), PIN(pin), OUT)

#define NOS_GPIO_READ(port, pin)        (!(CSP_GPIO_SET_PDSR(PORT(port)) & PIN(pin)) && (CSP_GPIO_GET_RISR(PORT(port)) & PIN(pin)))
#define NOS_GPIO_ON(port, pin)          CSP_GPIOSet(PORT(port), PIN(pin));
#define NOS_GPIO_OFF(port, pin)         CSP_GPIOClear(PORT(port), PIN(pin));
#define NOS_GPIO_TOGGLE(port, pin) \
    do { \
        if(PORT(port)->ODSR & PIN(pin)) \
        { \
            NOS_GPIO_OFF(port, pin); \
        } \
        else \
        { \
            NOS_GPIO_ON(port, pin); \
        } \
    } while(0)


#define NOS_GPIO_INIT_IN(port, pin) \
    do { \
        CSP_IOFunctionConfigure(GROUP(port), IOPORT(pin), IOCONF_F0); \
        NOS_GPIO_SET_IN(port, pin); \
    } while(0)

#define NOS_GPIO_INIT_OUT(port, pin) \
    do { \
        CSP_IOFunctionConfigure(GROUP(port), IOPORT(pin), IOCONF_F0); \
        NOS_GPIO_SET_OUT(port, pin); \
    } while(0)

#define NOS_GPIO_INIT_PERIPHERAL(port, pin)

#endif //~GPIO_H
