/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file arch.c
 * @brief Basic definitions related to MCU (S3FN41F ARM Cortex-M0)
 * @author Haeyong Kim (ETRI)
 * @date 2011.10.12
 */

#include "arch.h"
#include "csp.h"
#include "critical_section.h"

static void CORTEXM0_ConfigPLL(UINT16 u16PMSValue, UINT32 u32SysClkDivider)
{
	//UINT32 cnt = 0;
	
	/* IOCONF&GPIO and Flash Peripheral Clock Enable */
	CSP_CM_SET_PCSR(CM0, 0x60000000); 

	/* Flash Controller Clock Enable */
	CSP_IFC_SET_CEDR(IFC0, IFC_CLKEN); 
	if((CSP_CM_GET_SR(CM0) & CM_EMCLK) != CM_EMCLK)
	{
		CSP_CM_SET_ICR(CM0, CM_EMCLK);
		CSP_CM_SET_CSR(CM0, CM_EMCLK);
		while((CSP_CM_GET_RISR(CM0) & CM_EMCLK) != CM_EMCLK)
		{
			
		}
		
		CSP_CM_SET_ICR(CM0, CM_EMCLK);
	}
	
	/* Enable PLL FIN = (EMCLK)8MHz --> FOUT = 32MHz */  
	CSP_CM_SET_ICR(CM0, CM_PLL);
	CSP_CMPLLConfig(u16PMSValue);	// Refer to s3fn41f_conf.h
	CSP_CMPLLCmd(ENABLE);    
	
        //�����ÿ� �ּ� ó���ؾ� ������
        //while((CSP_CM_GET_RISR(CM0) & CM_PLL) != CM_PLL) {}
        nos_delay_ms(100);
        
        
	CSP_CM_SET_ICR(CM0, CM_PLL);      

	/* CORECLK = PLLCLK, high speed mode */
	if((CSP_IFC_GET_MR(IFC0) & IFC_FSMODE) != IFC_FSMODE)
	{
		CSP_IFC_SET_MR(IFC0, CSP_IFC_GET_MR(IFC0) | IFC_FSMODE);
	}
	
	/* SYSCLK Clock configuration */ 
	if((CSP_CM_GET_MR1(CM0) & CM_SYSCLK_MASK) != CM_SYSCLK_SEL(SysPLLCLK))
	{
		CSP_CM_SET_ICR(CM0, CM_STABLE);
		CSP_CMSysclkChange(SysPLLCLK);
		while((CSP_CM_GET_RISR(CM0) & CM_STABLE) != CM_STABLE)
		{
			
		}
		CSP_CM_SET_ICR(CM0, CM_STABLE);
	} 

	/* SYSCLK divider value setting */
	CSP_CMSysclkDivConfig(u32SysClkDivider); 
	//Delay(8); //The delay should be inserted.
	nos_delay_us(3);

	CSP_CMClockOutPinConfig(ENABLE);	//P0.14 & P1.21
	CSP_CMClockOutCmd(MdCORECLK); 	//You can check core clock defined with PLLCLK.

	/* Enable the clock for peripherals : Need to set '1' configuration to access/control a target peripheral */
	CSP_CMPclkCmd(0xEFD7FFFF, ENABLE);   

	/* Check the frequency for clocks(SYSCLK,PCLK,STCLK) consisting of a system */
	CSP_CMGetFrequency(&n41f); 	
}



void nos_arch_init()
{
	CSP_SCBInit();
	CSP_NVICInit();
	//CORTEXM0_WDTDisable();
	CSP_WDT_SET_OMR(WDT0, 0); // Disable WDT.

	CORTEXM0_ConfigPLL(PMS_FIN8PLL32, CM_SYSCLK_DIV2); // CORTEX_M0 PLL configuration
	nos_delay_ms(90);	// Delay for SM-LINK Programming flash memory.
	_nested_intr_cnt = 0;

	/* haekim temp Init WDT */
	//CORTEXM0_WDTInit(NVIC_PRI_SUPER);	// high priority(1), normal priority(2) for general interrupts
}

void nos_delay_us(UINT32 timeout_usec)
{
    if (timeout_usec)
    {
        do
        {
            NOS_NOP();
            NOS_NOP();
            NOS_NOP();
            NOS_NOP();
            NOS_NOP();
            NOS_NOP();
            NOS_NOP();
            NOS_NOP();
			NOS_NOP();
			NOS_NOP();
		}while(--timeout_usec);
    }
}

void nos_delay_ms(UINT32 timeout_msec)
{
    if (timeout_msec)
    {
        do
        {
            nos_delay_us(1000);
		}while (--timeout_msec);
    }
}

void NOS_RESET(void)
{
	CM0->SRR = CM_SWRST;	//Clock Manager Software Reset
	SCB0->AIRCR = (SCB_VECKEY | SCB_SYSRESETREQ);	// System Reset request
	while(1);
}

UINT16 ntohs(UINT16 a)
{
    return (((a << 8) & 0xff00) | ((a >> 8) & 0xff));
}

UINT32 ntohl(UINT32 a)
{
    return (((a << 24) & 0xff000000)
            | ((a << 8) & 0xff0000)
            | ((a >> 8) & 0xff00)
            | ((a >> 24) & 0xff));
}
