/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @date 2011.10.13
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "hal_sched.h"

#ifdef KERNEL_M
#include "arch.h"
#include "csp.h"
#include "sched.h"
#include "hal_sched.h"

void nos_sched_hal_init()
{
	/* Init PendSV */
	SCB0->ICSR |= SCB_PENDSVCLR;	// clear pending interrupt
	SCB0->SHPR3 &= 0xFF00FFFF;
	SCB0->SHPR3 |= ( ((U32_T)NVIC_PRI_LOW) << 16);	// [23:22]-priority for PendSV, The lowest priority

	/* Init Systic */
	CSP_SysTickDisable();
	CSP_SysTickClkConfig(SYSTICK_HCLK);				// processor clock (ref UG4.4.1)
	//CSP_SysTickReload(n41f.MAINCLK_Frequency/(1000/SCHED_TIMER_MS));		// max:0xffffff, mainclk:16MHz
	CSP_SysTickReload(n41f.MAINCLK_Frequency/958*SCHED_TIMER_MS);		// to slow down 'SysTick' a little bit....
	CSP_SysTickConfigInterrupt(NVIC_PRI_SUPER, ENABLE);	// The highest priority
}

void nos_sched_timer_start()
{
	CSP_SysTickEnable();
}

BOOL nos_sched_timer_is_set()
{
	return (SYSTICK0->CSR & SYSTICK_COUNTER_ENABLE)?TRUE:FALSE;
}


#endif // KERNEL_M
