/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 10. 17.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "uart.h"
#ifdef UART_M

#include "csp.h"

void nos_uart_putc(UINT8 port_num, UINT8 character)
{
    CSP_USART_T *p;

    if (port_num == 0)
	{
	    p = USART0;
	}
    else
    {
        return;
    }

    while (!(CSP_USART_GET_SR(p) & USART_TXRDY));
    CSP_USART_SET_THR(p, character);
}

/*
UINT8 APP_GetChar(UINT8* pu8Data)
{
	if(gu16Uart1RxWrIdx == gu16Uart1RxRdIdx)
	{
		return 0;
	}

	// UART0_RX_OFF; 
	CSP_USART_SET_IMSCR(USART0, CSP_USART_GET_IMSCR(USART0) & (~USART_RXRDY));
	
	*pu8Data = gau8uart1RxBuff[gu16Uart1RxRdIdx];
	gu16Uart1RxRdIdx = (gu16Uart1RxRdIdx + 1) & (SIZE_UART1RX_BUFF - 1);

	// UART0_RX_ON;
	CSP_USART_SET_IMSCR(USART0, CSP_USART_GET_IMSCR(USART0) | (USART_RXRDY));

	return 1;	
}*/

#endif // UART_M
