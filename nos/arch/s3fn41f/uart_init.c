/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @date 2011. 10. 16.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "uart.h"
#ifdef UART_M

#include "platform.h"	// CMGetAPBClk

#ifdef S3FN41F_UART_BR_9600
#define UART_BR 9600
#elif defined S3FN41F_UART_BR_19200
#define UART_BR 19200
#elif defined S3FN41F_UART_BR_38400
#define UART_BR 38400
#elif defined S3FN41F_UART_BR_57600
#define UART_BR 57600
#elif defined S3FN41F_UART_BR_115200
#define UART_BR 115200
#elif defined S3FN41F_UART_BR_230400
#define UART_BR 230400
#elif defined S3FN41F_UART_BR_380400
#define UART_BR 380400
#elif defined S3FN41F_UART_BR_460800
#define UART_BR 460800
#elif defined S3FN41F_UART_BR_921600
#define UART_BR 921600
#else
#error "Unsupported baud rate"
#endif

void nos_uart_init(UINT8 port_num)
{
	UINT32 			BaudRateGen, ComputeBaudRate;
	//NVIC_TypeDef 	NVICConfig;

	if (port_num == 0)
	{
		CSP_USARTPortInit(USART0, DISABLE);

		/* BRGR approximation : Baud rate = APB clock/(BRGR value * 16) */
		BaudRateGen = (UINT32)(CMGetAPBClk / (UART_BR << 4));
		/* Calculate nearest baud rate */
		ComputeBaudRate = ((CMGetAPBClk * 100) / (UART_BR << 4));
		if((ComputeBaudRate % 100) > 50)
		{
			BaudRateGen++;
		}
		/* Init. USART */
		CSP_USARTInit(	USART0, 
						(USART_NBSTOP_1 | USART_CLKS_MCKI | USART_CHRL_8 | USART_ASYNC | USART_PAR_NO | USART_CHMODE_NORMAL),
						BaudRateGen,	 /* Baud Rate  */
						0, 			 	 /* Time Out   */
						1 );			 	 /* Time Guard */
		/* Configure interrupt */
		CSP_USARTConfigInterrupt(USART0, USART_RXRDY, ENABLE);
		CSP_NVICEnableInterrupt(NVIC_USART0, NVIC_PRI_HIGH);
		
		/* Configure interrupt mode in NVIC module */
		/*NVICConfig.NVIC_IRQChannel = NVIC_USART0;
		NVICConfig.NVIC_IRQChannelPriority = NVIC_PRIORITY_1;	//Do not change. fixed by NanoQplus
		NVICConfig.NVIC_IRQChannelCmd = ENABLE;
		CSP_NVICConfigInterrupt(&NVICConfig);*/

		// USART DMA enable
		//USART0->DMACR = USART_TXDMAE | USART_RXDMAE;
		// Enable USART TX and RX 
		CSP_USARTEnable(USART0, (USART_RXEN | USART_TXEN));
	}
}



#endif	// UART_M
