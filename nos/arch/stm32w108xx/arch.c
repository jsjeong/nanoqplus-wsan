// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file arch.c
 * @author Haeyong Kim (ETRI)
 * @date 2013. 2. 28.
 */
   
#include "arch.h"
#include "stm32w108xx.h"

#ifdef __IAR_SYSTEMS_ICC__
#include "iar.h"
#endif

#include "hal.h"
//#include "memmap.h"

volatile int nos_hal_initialized = FALSE;
volatile uint8_t _nested_intr_cnt;

/*
  After 'RESET', 
  SystemInit() or __low_level_init()
  __iar_program_start();
  will be executed by Startup code befor nos_init(). 
*/
void nos_arch_init()
{
    /*** __low_level_init() ***/
#if 0
    PWR->VREGCR = 0x00000307;
    CoreDebug->DEMCR &= ~CoreDebug_DEMCR_VC_CORERESET_Msk;
    FLASH->ACR= (FLASH_ACR_PRFTBE | (1 << 0));
    SCB->CCR = SCB_CCR_DIV_0_TRP_Msk;
    SCB->SHCSR = ( SCB_SHCSR_USGFAULTENA_Msk | SCB_SHCSR_BUSFAULTENA_Msk | SCB_SHCSR_MEMFAULTENA_Msk);
#ifdef __ICCARM__
    SCB->VTOR = (uint32_t)__vector_table;
#elif defined __CC_ARM
    SCB->VTOR = (uint32_t)__Vectors;
#elif __GNUC__
    SCB->VTOR = (uint32_t)g_pfnVectors;
#else
#error "Vector table expected"
#endif
#endif
#if 1
#ifdef  VECT_TAB_RAM
    /* Set the Vector Table base location at 0x20000000 */ 
    NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0); 
#else  /* VECT_TAB_FLASH  */
    /* Set the Vector Table base location at 0x08000000 */ 
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);   
#endif
#endif	
#if 0
    /*** halInit() ***/
    halCommonTryToSwitchToXtal();  
    GPIO_DBG->DBGCR &= ~GPIO_DBGCR_EXTREGEN;	/* Disable the REG_EN external regulator enable signal.  Out of reset this
                                                 signal overrides PA7.  By disabling it early, PA7 is reclaimed as a GPIO.
                                                 If an external regulator is required, the following line of code should
                                                 be deleted. */
    halInternalSetRegTrim(FALSE);
    halInternalInitAdc();
    halCommonCalibratePads();
    if(!halCommonTryToSwitchToXtal())
    {
        nos_delay_us(1500);	//XTAL_STABILIZING_TIME_US = 1500us
    }
#endif
    //CLK->HSECR2 =  (uint32_t)0x00000003;
    //SystemInit();

    /*** NanoQplus Start ***/
//	SCB->SHCSR = ( SCB_SHCSR_BUSFAULTENA_Msk | SCB_SHCSR_MEMFAULTENA_Msk);
    nos_hal_initialized = TRUE;
    __set_BASEPRI(0);	// BASEPRI has been changed. Need to be restored.

    _nested_intr_cnt = 0;
}

void nos_reset()
{
    CLK->HSECR2 &=  (uint32_t)0x00000000;
    CLK->CPUCR &=  (uint32_t)0x00000000;
    NVIC_SystemReset();
    while(1);
}


// FCLK = 12MHz
void nos_delay_us(uint32_t us)
{
    us*=2;
    while( us-- != 0);
}
// FCLK = 12MHz
void nos_delay_ms(volatile uint32_t ms)
{
    ms*=1500;
    while( ms-- != 0);
}

UINT16 ntohs(UINT16 a)
{
    return (((a << 8) & 0xff00) | ((a >> 8) & 0xff));
}

UINT32 ntohl(UINT32 a)
{
    return (((a << 24) & 0xff000000)
            | ((a << 8) & 0xff0000)
            | ((a >> 8) & 0xff00)
            | ((a >> 24) & 0xff));
}
