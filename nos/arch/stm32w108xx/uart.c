// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @name uart.c
 * @breif IAR guide (low level interface, __write, __Read) implementation
 * @author Haeyong Kim (ETRI)
 * @date 2013. 1. 18
 */

#include "uart.h"
#ifdef UART_M

#include <yfuns.h>

SC_UART_TypeDef* STDUART;	// to specify stdin, stdout, stderr (default: SC_UART1)

int MyLowLevelPutchar(int x)
{
    UART_SendData(STDUART, (uint8_t)x);
    while(UART_GetFlagStatus(STDUART, UART_FLAG_TXE) == RESET); // Loop until the end of transmission
    return x;
}

/*
 * If the __write implementation uses internal buffering, uncomment
 * the following line to ensure that we are called with "buffer" as 0
 * (i.e. flush) when the application terminates.
 */

size_t __write(int handle, const unsigned char * buffer, size_t size)
{
    size_t nChars = 0;

    if (buffer == 0)
    {
        /*
         * This means that we should flush internal buffers.  Since we don't we just return.  
         (Remember, "handle" == -1 means that all  handles should be flushed.)
        */
        return 0;
    }

    /* This template only writes to "standard out" and "standard err",
     * for all other file handles it returns failure. */
    if (handle != _LLIO_STDOUT && handle != _LLIO_STDERR)
    {
        return _LLIO_ERROR;
    }

    for (; size != 0; --size)
    {
        if (MyLowLevelPutchar(*buffer++) < 0)
        {
            return _LLIO_ERROR;
        }

        ++nChars;
    }

    return nChars;
}

int MyLowLevelGetchar()
{
    while(UART_GetFlagStatus(STDUART,UART_FLAG_RXNE) == RESET);
    return UART_ReceiveData(STDUART);
}

size_t __read(int handle, unsigned char * buffer, size_t size)
{
    int nChars = 0;
    int c;

    // This template only reads from "standard in", for all other file handles it returns failure.
    if (handle != _LLIO_STDIN)
    {
        return _LLIO_ERROR;
    }

    for (; size > 0; --size)
    {
        c = MyLowLevelGetchar();
        if (c < 0) 
            break;
        *buffer++ = c;
        ++nChars;
    }
    UART_ClearITPendingBit(SC1_IT, UART_IT_RXNE); /* Clear interrupts */
    return nChars;
}


#if 0
#ifdef __GNUC__
#include <sys/stat.h>
#endif /* __GNUC__ */


#ifndef __ICCARM__
#include <stdio.h>
#define _LLIO_STDIN ((int) stdin)
#define _LLIO_STDOUT ((int) stdout)
#define _LLIO_STDERR ((int) stderr)
#define _LLIO_ERROR ((size_t)-1)
#ifndef __GNUC__
#define __write _write
#define __read _read
#endif
#undef putchar
void __io_putchar( char c );
int putchar (int c)
{
    __io_putchar((char) c);
    return c;
}
#endif /* __ICCARM__ */




#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

#ifdef __GNUC__
/* With GCC/RAISONANCE, small scanf calls __io_getchar() */
#define GETCHAR_PROTOTYPE int __io_getchar(void)
#endif /* __GNUC__ */

int PUTCHAR_PROTOTYPE(ch)
{
    USART_SendData(STDUART, (uint8_t)ch);
    while(USART_GetFlagStatus(STDUART, USART_FLAG_TXE) == RESET); // Loop until the end of transmission
    return ch;
}

#if (defined (__GNUC__))
int fflush (FILE *f)
#elif (defined (__CC_ARM))
    int fflush (FILE *f)
#elif (defined (__ICCARM__))
    size_t fflush(int handle)
#else
#error "Add fflash for the appropriate compiler"
#endif
{
    return __write(_LLIO_STDOUT, NULL, 0);
}

#endif



#endif	// UART_M
