// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

/**
 * @author Haeyong Kim (ETRI)
 * @date 2013. 1. 18
 */

#ifndef __NOS_UART_H__
#define __NOS_UART_H__

#include "kconf.h"
#ifdef UART_M

#include <stdio.h>
#include "stm32w108xx.h"
#include "nos_common.h"

extern SC_UART_TypeDef* STDUART;

// "stdio.h" library will use STDIO port default.
// Use the follwings only when UART port is not STDIO port. (STDUART_M)
// To terminate gets(), use "ctrl+enter"
#if 0
#define nos_uart_putc(port, ch)                                 \
    do{ STDUART = port; putchar(ch); STDUART = STDIO; }while(0)
#define nos_uart_puts(port, str)                              \
    do{ STDUART = port; puts(str); STDUART = STDIO; }while(0)
#define nos_uart_printf(port, ...)                                      \
    do{ STDUART = port; printf(__VA_ARGS__); STDUART = STDIO; }while(0)
#define nos_uart_sprintf(port, str, ...)                                \
    do{ STDUART = port; sprintf(str, __VA_ARGS__); STDUART = STDIO; }while(0)

#define nos_uart_getc(port, ch)                                   \
    do{ STDUART = port; ch=getchar(); STDUART = STDIO; }while(0)
#define nos_uart_gets(port, str, len)                         \
    do{ STDUART = port; gets(str); STDUART = STDIO; }while(0)
#define nos_uart_scanf(port, ...)                                       \
    do{ STDUART = port; scanf(__VA_ARGS__); STDUART = STDIO; }while(0)
#define nos_uart_sscanf(port, str, ...)                                 \
    do{ STDUART = port; sscanf(str, __VA_ARGS__); STDUART = STDIO; }while(0)
#endif

#define nos_uart_putc(port, ch)		putchar(ch)
#define nos_uart_puts(port, str)	puts(str)
#define nos_uart_printf(port, ...)	printf(__VA_ARGS__)
#define nos_uart_sprintf(port, str, ...)	sprintf(str, __VA_ARGS__)

#define nos_uart_getc(port, ch)			do{ ch=getchar(); }while(0)
#define nos_uart_gets(port, str, len) 	gets(str)
#define nos_uart_scanf(port, ...)		scanf(__VA_ARGS__)
#define nos_uart_sscanf(port, str, ...)	sscanf(str, __VA_ARGS__)

#define nos_uart_enable_rx_intr(p)	NOS_ENABLE_UART_RX_INTR(p)
#define nos_uart_disable_rx_intr(p)	NOS_DISABLE_UART_RX_INTR(p)
#define NOS_ENABLE_UART_RX_INTR(p)	UART_ITConfig(SC1_IT, UART_IT_RXNE, ENABLE)
#define NOS_DISABLE_UART_RX_INTR(p)	UART_ITConfig(SC1_IT, UART_IT_RXNE, DISABLE)
#define NOS_IS_SET_UART_RX_FLAG(p)	UART_GetFlagStatus(SC1_UART, UART_FLAG_RXNE)
#define NOS_CELAR_UART_RX_INTR(p)	UART_ClearITPendingBit(SC1_IT, UART_IT_RXNE)

extern void (*nos_stduart_rx_callback)(char);

void nos_uart_set_getc_callback(SC_UART_TypeDef* UARTx, void (*func)(char));
void (*nos_uart_get_getc_callback(SC_UART_TypeDef* UARTx))(char);


#if 0
void nos_uart_puts(UINT8 port_num, const char *str);
void nos_uart_puti(UINT8 port_num, INT32 val);	
void nos_uart_putu(UINT8 port_num, UINT32 val);
#else
// for backward compatibility.
#define nos_uart_puti(port,val)			printf("%d",val)
#define nos_uart_putu(port,val)			printf("%d",val)
#endif

#endif // __UART_M__
#endif // !__NOS_UART_H__

