// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_init.c
 * @author Haeyong Kim (ETRI)
 * @author Sangcheol Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "kconf.h"
#ifdef UART_M

#include "uart.h"
#include "platform.h"
#include "arch.h"
#include "clock.h"

#ifdef MSP430F1611_UART0_M

#if defined MSP430F1611_UART0_BR_9600
#define UART0_BR 9600L
#elif defined MSP430F1611_UART0_BR_19200
#define UART0_BR 19200L
#elif defined MSP430F1611_UART0_BR_38400
#define UART0_BR 38400L
#elif defined MSP430F1611_UART0_BR_57600
#define UART0_BR 57600L
#elif defined MSP430F1611_UART0_BR_115200
#define UART0_BR 115200L
#elif defined MSP430F1611_UART0_BR_230400
#define UART0_BR 230400L
#elif defined MSP430F1611_UART0_BR_380400
#define UART0_BR 380400L
#elif defined MSP430F1611_UART0_BR_460800
#define UART0_BR 460800L
#else
#error "MSP430F1611 UART0 unknown baud rate."
#endif

#endif //MSP430F1611_UART0_M

#ifdef MSP430F1611_UART1_M

#if defined MSP430F1611_UART1_BR_9600
#define UART1_BR 9600L
#elif defined MSP430F1611_UART1_BR_19200
#define UART1_BR 19200L
#elif defined MSP430F1611_UART1_BR_38400
#define UART1_BR 38400L
#elif defined MSP430F1611_UART1_BR_57600
#define UART1_BR 57600L
#elif defined MSP430F1611_UART1_BR_115200
#define UART1_BR 115200L
#elif defined MSP430F1611_UART1_BR_230400
#define UART1_BR 230400L
#elif defined MSP430F1611_UART1_BR_380400
#define UART1_BR 380400L
#elif defined MSP430F1611_UART1_BR_460800
#define UART1_BR 460800L
#else
#error "MSP430F1611 UART1 unknown baud rate."
#endif

#endif //MSP430F1611_UART1_M

//If you want to use 921600 baudrate, You must set SMCLK to 2048kHz or 4096kHz.

void nos_uart_init(UINT8 port_num)
{
#ifdef MSP430F1611_UART0_M
    if (port_num == MSP430F1611_USART0)
    {
        U0BR0 = (UINT16) (SMCLK * 1024L / UART0_BR) & 0xFF;
        U0BR1 = (UINT16) (SMCLK * 1024L / UART0_BR) >> 8;        

        //Use UART1 peripheral not genral I/O for P3.6, P3.7
        P3SEL |= (1 << UTXD0) | (1 << URXD0);

        //USART control register init
        //Idle-line multiprocessor protocol, UART mode, no feedback,
        // Parity disabled, 1 stop bit, 8-bit data, 1 stop bit
        U0CTL = SWRST;	//USART logic held in reset state.
        U0CTL |= CHAR;  // 8-bit data.

        // USART receive control
        U0RCTL = 0x00;

        // Clock source select
        U0TCTL = SSEL1 | SSEL0 | TXEPT;	//SMCLK, U1TXBUF is empty.

        // module enable
        ME1 &= ~USPIE0;   // USART SPI module disable
        ME1 |= (UTXE0 | URXE0);   // USART UART module enable

        // interrupt flag initialize
        IFG1 |= UTXIFG0; // TX buffer is empty.
        IFG1 &= ~URXIFG0; // RX buffer is empty

        // USART reset released for operation
        U0CTL &= ~SWRST;

        return;
    }
#endif //MSP430F1611_UART0_M

#ifdef MSP430F1611_UART1_M
    if (port_num == MSP430F1611_USART1)
    {
        U1BR0 = (UINT16) (SMCLK * 1024L / UART1_BR) & 0xFF;
        U1BR1 = (UINT16) (SMCLK * 1024L / UART1_BR) >> 8;


#if defined MSP430F1611_UART_BR_9600

#if (SMCLK == 1024)
        U0BR0 = 0x6D;
        U0BR1 = 0x00;
        U0MCTL = 0x22;
#elif (SMCLK == 2048)
        U0BR0 = 0xDA;
        U0BR1 = 0x00;
        U0MCTL = 0x45;
#elif (SMCLK == 4096)
        U0BR0 = 0xB4;
        U0BR1 = 0x01;
        U0MCTL = 0x90;
#endif //SMCLK

#elif defined MSP430F1611_UART0_BR_19200

#if (SMCLK == 1024)
        U0BR0 = 0x36;
        U0BR1 = 0x00;
        U0MCTL = 0x61;
#elif (SMCLK == 2048)
        U0BR0 = 0x6D;
        U0BR1 = 0x00;
        U0MCTL = 0x22;
#elif (SMCLK == 4096)
        U0BR0 = 0xDA;
        U0BR1 = 0x00;
        U0MCTL = 0x45;
#endif //SMCLK

#elif defined MSP430F1611_UART0_BR_38400

#if (SMCLK == 1024)
        U0BR0 = 0x1B;
        U0BR1 = 0x00;
        U0MCTL = 0x30;
#elif (SMCLK == 2048)
        U0BR0 = 0x36;
        U0BR1 = 0x00;
        U0MCTL = 0x61;
#elif (SMCLK == 4096)
        U0BR0 = 0x6D;
        U0BR1 = 0x00;
        U0MCTL = 0x22;
#endif //SMCLK

#elif defined MSP430F1611_UART0_BR_57600

#if (SMCLK == 1024)
        U0BR0 = 0x12;
        U0BR1 = 0x00;
        U0MCTL = 0x20;
#elif (SMCLK == 2048)
        U0BR0 = 0x24;
        U0BR1 = 0x00;
        U0MCTL = 0x40;
#elif (SMCLK == 4096)
        U0BR0 = 0x48;
        U0BR1 = 0x00;
        U0MCTL = 0x81;
#endif //SMCLK

#elif defined MSP430F1611_UART0_BR_115200
        
#if (SMCLK == 1024)
        U0BR0 = 0x09;
        U0BR1 = 0x00;
        U0MCTL = 0x10;
#elif (SMCLK == 2048)
        U0BR0 = 0x12;
        U0BR1 = 0x00;
        U0MCTL = 0x20;
#elif (SMCLK == 4096)
        U0BR0 = 0x24;
        U0BR1 = 0x00;
        U0MCTL = 0x40;
#endif //SMCLK

#elif defined MSP430F1611_UART0_BR_230400
        
#if (SMCLK == 1024)
        U0BR0 = 0x04;
        U0BR1 = 0x00;
        U0MCTL = 0x55;
#elif (SMCLK == 2048)
        U0BR0 = 0x09;
        U0BR1 = 0x00;
        U0MCTL = 0x10;
#elif (SMCLK == 4096)
        U0BR0 = 0x12;
        U0BR1 = 0x00;
        U0MCTL = 0x20;
#endif //SMCLK

#elif defined MSP430F1611_UART0_BR_380400
        
#if (SMCLK == 1024)
        U0BR0 = 0x02;
        U0BR1 = 0x00;
        U0MCTL = 0x75;
#elif (SMCLK == 2048)
        U0BR0 = 0x05;
        U0BR1 = 0x00;
        U0MCTL = 0x51;
#elif (SMCLK == 4096)
        U0BR0 = 0x0B;
        U0BR1 = 0x00;
        U0MCTL = 0x10;
#endif //SMCLK

#elif defined MSP430F1611_UART0_BR_460800
        
#if (SMCLK == 1024)
        U0BR0 = 0x02;
        U0BR1 = 0x00;
        U0MCTL = 0x28;
#elif (SMCLK == 2048)
        U0BR0 = 0x04;
        U0BR1 = 0x00;
        U0MCTL = 0x55;
#elif (SMCLK == 4096)
        U0BR0 = 0x09;
        U0BR1 = 0x00;
        U0MCTL = 0x10;
#endif //SMCLK

#endif //Baudrates

        //Use UART1 peripheral not genral I/O for P3.6, P3.7
        P3SEL |= (1 << UTXD1) | (1 << URXD1);

        //USART control register init
        //Idle-line multiprocessor protocol, UART mode, no feedback,
        // Parity disabled, 1 stop bit, 8-bit data, 1 stop bit
        U1CTL = SWRST;	//USART logic held in reset state.
        U1CTL |= CHAR;  // 8-bit data.

        // USART receive control
        U1RCTL = 0x00;

        // Clock source select
        U1TCTL = SSEL1 | SSEL0 | TXEPT;	//SMCLK, U1TXBUF is empty.

        // module enable
        ME2 &= ~USPIE1;   // USART SPI module disable
        ME2 |= (UTXE1 | URXE1);   // USART UART module enable

        // interrupt flag initialize
        IFG2 |= UTXIFG1; // TX buffer is empty.
        IFG2 &= ~URXIFG1; // RX buffer is empty

        // USART reset released for operation
        U1CTL &= ~SWRST;
        return;
    }
#endif //MSP430F1611_UART1_M
}

#endif // UART_M 
