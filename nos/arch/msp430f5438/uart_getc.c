// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#include "uart.h"

#ifdef UART_INPUT_M

#ifdef __MSPGCC__
#include <msp430.h>
#include <legacymsp430.h>
#elif defined __ICC430__
#include "io430.h"
#endif

#include "critical_section.h"

volatile char uart_rx_char;
void (*nos_uart0_rx_callback)(char);
void (*nos_uart1_rx_callback)(char);

void nos_uart_set_getc_callback(UINT8 port_num, void (*func)(char))
{
    if (port_num == MSP430F1611_USART0)
    {
        nos_uart0_rx_callback = func;
    }
    else if (port_num == MSP430F1611_USART1)
    {
        nos_uart1_rx_callback = func;
    }
}

void (*nos_uart_get_getc_callback(UINT8 port_num))(char)
{
    if (port_num == MSP430F1611_USART0)
    {
        return nos_uart0_rx_callback;
    }
    else if (port_num == MSP430F1611_USART1)
    {
        return nos_uart1_rx_callback;
    }
    else
    {
        return NULL;
    }
}

void nos_uart_enable_rx_intr(UINT8 port_num)
{
    if (port_num == MSP430F1611_USART0)
    {
        ENABLE_USART0_RX_vect();
    }
    else if (port_num == MSP430F1611_USART1)
    {
        ENABLE_USART1_RX_vect();
    }
}

void nos_uart_disable_rx_intr(UINT8 port_num)
{
    if (port_num == MSP430F1611_USART0)
    {
        DISABLE_USART0_RX_vect();
    }
    else if (port_num == MSP430F1611_USART1)
    {
        DISABLE_USART1_RX_vect();
    }
}

wakeup interrupt (UART0RX_VECTOR) UART0isr(void)
{
    NOS_ENTER_ISR();
    uart_rx_char = U0RXBUF;
    if (nos_uart0_rx_callback)
    {
        nos_uart0_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}

wakeup interrupt (UART1RX_VECTOR) UART1isr(void)
{
    NOS_ENTER_ISR();
    uart_rx_char = U1RXBUF;
    if (nos_uart1_rx_callback)
    {
        nos_uart1_rx_callback(uart_rx_char);
    }
    NOS_EXIT_ISR();
}

#endif /* UART_INPUT_M */
