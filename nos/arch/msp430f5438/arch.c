// -*- c-file-style:"bsd": c-basic-offset:4; indent-tabs-mode:nil;
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * MSP430F5438 Architecture
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 29.
 */

#ifdef __MSPGCC__
#include <msp430.h>
#else
#include <io.h>
#endif

#include "arch.h"
#include "critical_section.h"
#include "intr.h"
#include "pwr_mgr.h"

extern UINT16 __bss_end;
extern UINT16 _heap_end_addr;

void nos_arch_init()
{
    WDTCTL = WDTPW | WDTHOLD; // Stop WDT to conserve power.

    _nested_intr_cnt = 0;
}

void nos_delay_us(UINT16 timeout_usec)
{
    if (timeout_usec <= 4)
        return;
    timeout_usec -= 4;

    // Each loop takes 4 cycles.
    do { NOS_NOP(); } while (--timeout_usec);
}

void nos_delay_ms(UINT16 timeout_msec)
{
    if (timeout_msec)
    {
        do { nos_delay_us(1043); } while (--timeout_msec);
    }
}
