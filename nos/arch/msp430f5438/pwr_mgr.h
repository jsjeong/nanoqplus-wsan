// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Power Manager for MSP430F1611.
 * @author Jongsoo Jeong (ETRI)
 * @author Haeyong Kim (ETRI)
 */

#ifndef PWR_MGR_H
#define PWR_MGR_H

#ifdef __MSPGCC__
#include <msp430.h>
#elif defined __ICC430__
#include "io430.h"
#endif

#include "kconf.h"

enum msp430_power_mode_type
{
    MSP430_POWER_MODE_ACTIVE = 0,
    MSP430_POWER_MODE_LPM0 = LPM0_bits,
    MSP430_POWER_MODE_LPM1 = LPM1_bits,
    MSP430_POWER_MODE_LPM2 = LPM2_bits,
    MSP430_POWER_MODE_LPM3 = LPM3_bits,
    MSP430_POWER_MODE_LPM4 = LPM4_bits,
};
typedef enum msp430_power_mode_type MSP430_POWER_MODE;

MSP430_POWER_MODE nos_mcu_get_current_power_mode(void);
void nos_mcu_set_power_mode(MSP430_POWER_MODE mode);
void nos_mcu_sleep(void);

#endif	// ~PWR_MGR_H
