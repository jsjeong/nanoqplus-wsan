// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @authro Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 6. 15.
 */

#ifndef __HAL_SCHED_H__
#define __HAL_SCHED_H__
#include "kconf.h"
#ifdef KERNEL_M

#include "nos_common.h"
#include "rtc_timer.h"
#include "platform.h"
#include "intr.h"

#ifdef SCHED_PERIOD_5
#define SCHED_TIMER_MS		5UL
#elif SCHED_PERIOD_10
#define SCHED_TIMER_MS		10UL
#elif SCHED_PERIOD_32
#define SCHED_TIMER_MS		32UL
#else	// SCHED_PERIOD_100
#define SCHED_TIMER_MS		100UL
#endif
#define SCHED_TICKS         (SYS_TICKS_PER_SEC * SCHED_TIMER_MS / 1000UL)

extern volatile UINT8 next_sched_tick;

#undef KERNEL_DEFERRED_CTX_SW

/**
 * Set OCR2B to force interrupt right after now.
 */
#define NOS_SCHED_PENDING_SET()                 \
    do {                                        \
        OCR2B = TCNT2 + 1;                      \
        next_sched_tick = OCR2B;                \
        while(!IS_PENDING_TIMER2_COMPB_vect()); \
    } while (0)

void nos_sched_hal_init(void);
void nos_sched_timer_start(void);

#endif // KERNEL_M
#endif // ~__HAL_SCHED_H__
