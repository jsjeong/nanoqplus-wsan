/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @date 2012. 6. 15.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "adc.h"

#include <avr/io.h>
#include "critical_section.h"
#include "arch.h"

#include "uart.h"
#include "platform.h"

void nos_adc_on(void)
{
    _BIT_SET(ADCSRA, ADEN);
}

void nos_adc_off(void)
{
    _BIT_CLR(ADCSRA, ADEN);
}

void nos_adc_init(void)
{
    nos_adc_off();
    _BIT_CLR(ADCSRA, ADIE);
//    ADCSRA &= (~0x03);  // Clear the prescaler.
//    ADCSRA |= ADC_PRESCALER_2;
}


UINT16 nos_adc_convert(UINT8 ch, UINT8 refv, UINT8 pres, UINT8 norm_cnt)
{
    UINT32 adc_result = 0;
    UINT8 i;

    if (norm_cnt == 0) norm_cnt = 1;

    NOS_ENTER_CRITICAL_SECTION();

    _BIT_SET(ADCSRA, ADIF); // Clear interrupt flag

    ADMUX = refv | ch;
    ADCSRA &= (~0x03);
    ADCSRA |= pres;

    // First sample may be imprecise.
    nos_adc_on();
    _BIT_SET(ADCSRA, ADSC); // Start conversion.
    while(_IS_SET(ADCSRA, ADSC));

    for (i = 0; i < norm_cnt; i++)
    {
        _BIT_SET(ADCSRA, ADIF); // Clear interrupt flag
        _BIT_SET(ADCSRA, ADSC); // Start conversion.
        while(_IS_SET(ADCSRA, ADSC));

        adc_result += ADCW;
    }

    nos_adc_off();
    NOS_EXIT_CRITICAL_SECTION();

    return (adc_result / norm_cnt);
}
