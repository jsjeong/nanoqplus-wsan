// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_init.c
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 6. 15.
 */

#include "kconf.h"
#ifdef UART_M

#include <avr/io.h>
#include "uart.h"
#include "platform.h"

void nos_uart_init(UINT8 port_num)
{
#ifdef ATMEGA1284P_ASYNC_USART0
    if (port_num == ATMEGA1284P_USART0)
    {
        _BIT_SET(UCSR0A, U2X0);	// double speed on

#if defined ATMEGA1284P_ASYNC_USART0_BR_9600

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0.2%
        UBRR0L = 207;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0.2%
        UBRR0L = 103;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR0L = 95;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART0_BR_19200

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0.2%
        UBRR0L = 103;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0.2%
        UBRR0L = 51;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR0L = 47;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART0_BR_38400

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0.2%
        UBRR0L = 51;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0.2%
        UBRR0L = 25;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR0L = 23;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART0_BR_57600

#if (_SYSTEM_CLOCK == 16000000UL) //Error = -0.8%
        UBRR0L = 34;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 2.1%
        UBRR0L = 16;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR0L = 15;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART0_BR_76800

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0.2%
        UBRR0L = 25;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0.2%
        UBRR0L = 12;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR0L = 11;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART0_BR_115200

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 2.1%
        UBRR0L = 16;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = -3.5%
        UBRR0L = 8;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR0L = 7;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART0_BR_230400

#if (_SYSTEM_CLOCK == 16000000UL) //Error = -3.5%
        UBRR0L = 8;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 8.5%
        UBRR0L = 3;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR0L = 3;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART0_BR_250000

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0%
        UBRR0L = 7;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0%
        UBRR0L = 3;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = -7.8%
        UBRR0L = 3;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART0_BR_500000

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0%
        UBRR0L = 3;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0%
        UBRR0L = 1;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = -7.8%
        UBRR0L = 1;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART0_BR_1000000

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0%
        UBRR0L = 1;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0%
        UBRR0L = 0;
        UBRR0H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = -7.8%
        UBRR0L = 0;
        UBRR0H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#endif //ATMEGA1284P_ASYBNC_USART0_BR_1000000

        // UPM1, UPM0: 0 0 (no parity)
        _BIT_CLR(UCSR0C, UPM00);
        _BIT_CLR(UCSR0C, UPM01);

        // US_BS: 0 (1 stop bit)
        _BIT_CLR(UCSR0C, USBS0);

        // UCSZ2, UCSZ1, UCSZ0: 0 1 1 (8 bit)
        _BIT_CLR(UCSR0B, UCSZ02);
        _BIT_SET(UCSR0C, UCSZ01);
        _BIT_SET(UCSR0C, UCSZ00);

        // TXEN, RXEN : Enable Rx/Tx UART communication.
        _BIT_SET(UCSR0B, TXEN0);
        _BIT_SET(UCSR0B, RXEN0);

        return;
    }
#endif //ATMEGA1284P_ASYNC_USART0

#ifdef ATMEGA1284P_ASYNC_USART1
    if (port_num == ATMEGA1284P_USART1)
    {
        _BIT_SET(UCSR1A, U2X1); // double speed on

#if defined ATMEGA1284P_ASYNC_USART1_BR_9600

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0.2%
        UBRR1L = 207;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0.2%
        UBRR1L = 103;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR1L = 95;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART1_BR_19200

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0.2%
        UBRR1L = 103;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0.2%
        UBRR1L = 51;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR1L = 47;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART1_BR_38400

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0.2%
        UBRR1L = 51;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0.2%
        UBRR1L = 25;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR1L = 23;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART1_BR_57600

#if (_SYSTEM_CLOCK == 16000000UL) //Error = -0.8%
        UBRR1L = 34;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 2.1%
        UBRR1L = 16;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR1L = 15;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART1_BR_76800

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0.2%
        UBRR1L = 25;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0.2%
        UBRR1L = 12;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR1L = 11;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART1_BR_115200

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 2.1%
        UBRR1L = 16;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = -3.5%
        UBRR1L = 8;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR1L = 7;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART1_BR_230400

#if (_SYSTEM_CLOCK == 16000000UL) //Error = -3.5%
        UBRR1L = 8;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 8.5%
        UBRR1L = 3;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = 0%
        UBRR1L = 3;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART1_BR_250000

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0%
        UBRR1L = 7;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0%
        UBRR1L = 3;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = -7.8%
        UBRR1L = 3;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART1_BR_500000

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0%
        UBRR1L = 3;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0%
        UBRR1L = 1;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = -7.8%
        UBRR1L = 1;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#elif defined ATMEGA1284P_ASYNC_USART1_BR_1000000

#if (_SYSTEM_CLOCK == 16000000UL) //Error = 0%
        UBRR1L = 1;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 8000000UL) //Error = 0%
        UBRR1L = 0;
        UBRR1H = 0;
#elif (_SYSTEM_CLOCK == 7372800UL) //Error = -7.8%
        UBRR1L = 0;
        UBRR1H = 0;
#endif //_SYSTEM_CLOCK == 7372800UL

#endif //ATMEGA1284P_ASYNC_USART1_BR_1000000

        // UPM1, UPM0: 0 0 (no parity)
        _BIT_CLR(UCSR1C, UPM00);
        _BIT_CLR(UCSR1C, UPM01);

        // US_BS: 0 (1 stop bit)
        _BIT_CLR(UCSR1C, USBS0);

        // UCSZ2, UCSZ1, UCSZ0: 0 1 1 (8 bit)
        _BIT_CLR(UCSR1B, UCSZ02);
        _BIT_SET(UCSR1C, UCSZ01);
        _BIT_SET(UCSR1C, UCSZ00);

        // TXEN, RXEN : Enable Rx/Tx UART communication.
        _BIT_SET(UCSR1B, TXEN0);
        _BIT_SET(UCSR1B, RXEN0);

        return;
    }
#endif //ATMEGA1284P_ASYNC_USART1
}
#endif /* UART_M */
