/*
 * Copyright (c) 2006-2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 6. 15.
 *
 * @note This code is imported from pwr_mgr.h of ATmega128.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef PWR_MGR_H
#define PWR_MGR_H

#include <avr/io.h>
#include "nos_common.h"

#define NOS_IDLE            (0) 
#define NOS_ADC             (1 << SM0)
#define NOS_PWR_DOWN        (1 << SM1)
#define NOS_PWR_SAVE        ((1 << SM1) | (1 << SM0))
#define NOS_STANDBY         ((1 << SM2) | (1 << SM1))
#define NOS_EXT_STANDBY     ((1 << SM2) | (1 << SM1) | (1 << SM0))

#define NOS_SET_SLEEP_MODE(sleep_mode) \
    do { \
        MCUCR = ( (MCUCR & ~((1 << SM2) | (1 << SM1) | (1 << SM0))) | (sleep_mode) ); \
    } while(0)

#define NOS_SLEEP_MCU() \
    do { \
        _BIT_SET(MCUCR, SE); \
        __asm__ volatile ("sleep" "\n\t" :: ); \
        _BIT_CLR(MCUCR, SE); \
    } while(0)

#define NOS_SLEEP_ENABLE() \
    do { \
        _BIT_SET(MCUCR, SE); \
    } while(0)

#define NOS_SLEEP_DISABLE() \
    do { \
        _BIT_CLR(MCUCR, SE); \
    } while(0)

UINT16 nos_mcu_get_supply_voltage(void);

#endif	// ~PWR_MGR_H


/************ For low power consumption *************/
//----------------- Analog comparator turn off (T/C1 is realated to this)
// _BIT_SET(ACSR, ACD); // note that analog comparator is turned on initially.
// _BIT_CLR(ACSR, ACIE);
//----------------- ADC turn off
// _BIT_CLR(ADCSRA, ADEN);
// _BIT_CLR(ADCSRA, ADIE);
//----------------- Watchdog turn off
// cli();
// WDTCR |= (1 << WDCE) | (1 << WDE); // WDCE bit is cleared automatically in 4 cycles.
// _BIT_CLR(WDTCR, WDE);
// sei();

