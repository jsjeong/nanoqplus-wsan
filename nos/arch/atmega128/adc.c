/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "adc.h"

#ifdef ADC_M
#include <avr/io.h>
#include "critical_section.h"

void nos_adc_init()
{
    ////////////////////////// ADCSRA REGISTER Setting
    // ADEN(1) : ADC Enable
    // ADPS(011) : ADC Prescaler Selection Bits (Division Factor = 8)
    ADCSRA = (1 << ADPS1) | (1 << ADPS0) | (1 << ADEN);

    ////////////////////////// ADMUX REGISTER Setting
    // REFS(11) : Internal 2.56V reference with external capacitor at AREF pin
    // ADLAR(0) : Diable ADC Left Adjust Result
    ADMUX = (1 << REFS1) | (1 << REFS0) | (0 << ADLAR);
}


UINT16 nos_adc_convert(UINT8 adc_channel)
{
    UINT16 adc_result;
    NOS_ENTER_CRITICAL_SECTION();
    ADMUX = (1 << REFS1) | (1 << REFS0) | (0 << ADLAR) | (0x1f & adc_channel);
    // start of conversion
    ADCSRA |= (1 << ADSC);

    // wait until the conversion completes
    while (!(ADCSRA & (1 << ADIF)));

    // 10 bit data mode (8 bit data mode is not supported)
    // Converted values are lied on ADCH:ADCL
    // To combine these two bytes into 16bit, 2^8 = 256 need to be multiplied
    adc_result = ADC;
    NOS_EXIT_CRITICAL_SECTION();
    return(adc_result);
}

#endif //ADC_M 
