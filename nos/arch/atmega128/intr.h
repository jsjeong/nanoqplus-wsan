// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file intr.h
 * @author Haeyong Kim (ETRI)
 */

#ifndef INTR_H
#define INTR_H

#include <avr/io.h>
#include "nos_common.h"

/************************* ATmega128L 34 Interrupts ***************************/
// External Interrupt MaSK register - EIMSK
// External Interrupt Flag Register - EIFR

//External Interrupt Contrl Register A - EICRA (INT0~INT3)
//External Interrupt Contrl Register B - EICRB (INT4~INT7)
#define EINTR_REQ_LOW	0	// low level generates an interrupt request
#define EINTR_REQ_FALL	2	// falling edge
#define EINTR_REQ_RISE	3	// rising edge
// ex) INT4 : EICRB = ( EICRB&(~(0x11 << ISC40)) ) |(EINTR_REQ_FALL << ISC40)

#define ENABLE_INT0_vect()	do { _BIT_SET(EIMSK, INT0); } while (0)
#define DISABLE_INT0_vect()	do { _BIT_CLR(EIMSK, INT0); } while (0)
#define CLEAR_INT0_vect()	do { _BIT_SET(EIFR, INTF0); } while (0)
#define IS_SET_INT0_vect_FLAG()	_IS_SET(EIFR, INTF0)
#define SET_INT0_IRQ_GENERATED_ON_LOW_LEVEL() \
    do { \
        _BIT_CLR(EICRA, ISC01); \
        _BIT_CLR(EICRA, ISC00); \
    } while (0)
#define SET_INT0_IRQ_GENERATED_ON_FALLING_EDGE() \
    do { \
        _BIT_SET(EICRA, ISC01); \
        _BIT_CLR(EICRA, ISC00); \
    } while (0)
#define SET_INT0_IRQ_GENERATED_ON_RISING_EDGE() \
    do { \
        _BIT_SET(EICRA, ISC01); \
        _BIT_SET(EICRA, ISC00); \
    } while (0)

#define ENABLE_INT1_vect()	do { _BIT_SET(EIMSK, INT1); } while (0)
#define DISABLE_INT1_vect()	do { _BIT_CLR(EIMSK, INT1); } while (0)
#define CLEAR_INT1_vect()	do { _BIT_SET(EIFR, INTF1); } while (0)
#define IS_SET_INT1_vect_FLAG() _IS_SET(EIFR, INTF1)
#define SET_INT1_IRQ_GENERATED_ON_LOW_LEVEL() \
    do { \
        _BIT_CLR(EICRA, ISC11); \
        _BIT_CLR(EICRA, ISC10); \
    } while (0)
#define SET_INT1_IRQ_GENERATED_ON_FALLING_EDGE() \
    do { \
        _BIT_SET(EICRA, ISC11); \
        _BIT_CLR(EICRA, ISC10); \
    } while (0)
#define SET_INT1_IRQ_GENERATED_ON_RISING_EDGE() \
    do { \
        _BIT_SET(EICRA, ISC11); \
        _BIT_SET(EICRA, ISC10); \
    } while (0)

#define ENABLE_INT2_vect()	do { _BIT_SET(EIMSK, INT2); } while (0)
#define DISABLE_INT2_vect()	do { _BIT_CLR(EIMSK, INT2); } while (0)
#define CLEAR_INT2_vect()	do { _BIT_SET(EIFR, INTF2); } while (0)
#define IS_SET_INT2_vect_FLAG() _IS_SET(EIFR, INTF2)
#define SET_INT2_IRQ_GENERATED_ON_LOW_LEVEL() \
    do { \
        _BIT_CLR(EICRA, ISC21); \
        _BIT_CLR(EICRA, ISC20); \
    } while (0)
#define SET_INT2_IRQ_GENERATED_ON_FALLING_EDGE() \
    do { \
        _BIT_SET(EICRA, ISC21); \
        _BIT_CLR(EICRA, ISC20); \
    } while (0)
#define SET_INT2_IRQ_GENERATED_ON_RISING_EDGE() \
    do { \
        _BIT_SET(EICRA, ISC21); \
        _BIT_SET(EICRA, ISC20); \
    } while (0)

#define ENABLE_INT3_vect()	do { _BIT_SET(EIMSK, INT3); } while (0)
#define DISABLE_INT3_vect()	do { _BIT_CLR(EIMSK, INT3); } while (0)
#define CLEAR_INT3_vect()	do { _BIT_SET(EIFR, INTF3); } while (0)
#define IS_SET_INT3_vect_FLAG() _IS_SET(EIFR, INTF3)
#define SET_INT3_IRQ_GENERATED_ON_LOW_LEVEL() \
    do { \
        _BIT_CLR(EICRA, ISC31); \
        _BIT_CLR(EICRA, ISC30); \
    } while (0)
#define SET_INT3_IRQ_GENERATED_ON_FALLING_EDGE() \
    do { \
        _BIT_SET(EICRA, ISC31); \
        _BIT_CLR(EICRA, ISC30); \
    } while (0)
#define SET_INT3_IRQ_GENERATED_ON_RISING_EDGE() \
    do { \
        _BIT_SET(EICRA, ISC31); \
        _BIT_SET(EICRA, ISC30); \
    } while (0)

#define ENABLE_INT4_vect()	do { _BIT_SET(EIMSK, INT4); } while (0)
#define DISABLE_INT4_vect()	do { _BIT_CLR(EIMSK, INT4); } while (0)
#define CLEAR_INT4_vect()	do { _BIT_SET(EIFR, INTF4); } while (0)
#define IS_SET_INT4_vect_FLAG() _IS_SET(EIFR, INTF4)
#define SET_INT4_IRQ_GENERATED_ON_LOW_LEVEL() \
    do { \
        _BIT_CLR(EICRB, ISC41); \
        _BIT_CLR(EICRB, ISC40); \
    } while (0)
#define SET_INT4_IRQ_GENERATED_ON_FALLING_EDGE() \
    do { \
        _BIT_SET(EICRB, ISC41); \
        _BIT_CLR(EICRB, ISC40); \
    } while (0)
#define SET_INT4_IRQ_GENERATED_ON_RISING_EDGE() \
    do { \
        _BIT_SET(EICRB, ISC41); \
        _BIT_SET(EICRB, ISC40); \
    } while (0)

#define ENABLE_INT5_vect()	do { _BIT_SET(EIMSK, INT5); } while (0)
#define DISABLE_INT5_vect()	do { _BIT_CLR(EIMSK, INT5); } while (0)
#define CLEAR_INT5_vect()	do { _BIT_SET(EIFR, INTF5); } while (0)
#define IS_SET_INT5_vect_FLAG() _IS_SET(EIFR, INTF5)
#define SET_INT5_IRQ_GENERATED_ON_LOW_LEVEL() \
    do { \
        _BIT_CLR(EICRB, ISC51); \
        _BIT_CLR(EICRB, ISC50); \
    } while (0)
#define SET_INT5_IRQ_GENERATED_ON_FALLING_EDGE() \
    do { \
        _BIT_SET(EICRB, ISC51); \
        _BIT_CLR(EICRB, ISC50); \
    } while (0)
#define SET_INT5_IRQ_GENERATED_ON_RISING_EDGE() \
    do { \
        _BIT_SET(EICRB, ISC51); \
        _BIT_SET(EICRB, ISC50); \
    } while (0)

#define ENABLE_INT6_vect()	do { _BIT_SET(EIMSK, INT6); } while (0)
#define DISABLE_INT6_vect()	do { _BIT_CLR(EIMSK, INT6); } while (0)
#define CLEAR_INT6_vect()	do { _BIT_SET(EIFR, INTF6); } while (0)
#define IS_SET_INT6_vect_FLAG()	_IS_SET(EIFR, INTF6)
#define SET_INT6_IRQ_GENERATED_ON_LOW_LEVEL() \
    do { \
        _BIT_CLR(EICRB, ISC61); \
        _BIT_CLR(EICRB, ISC60); \
    } while (0)
#define SET_INT6_IRQ_GENERATED_ON_FALLING_EDGE() \
    do { \
        _BIT_SET(EICRB, ISC61); \
        _BIT_CLR(EICRB, ISC60); \
    } while (0)
#define SET_INT6_IRQ_GENERATED_ON_RISING_EDGE() \
    do { \
        _BIT_SET(EICRB, ISC61); \
        _BIT_SET(EICRB, ISC60); \
    } while (0)

#define ENABLE_INT7_vect()	do { _BIT_SET(EIMSK, INT7); } while (0)
#define DISABLE_INT7_vect()	do { _BIT_CLR(EIMSK, INT7); } while (0)
#define CLEAR_INT7_vect()	do { _BIT_SET(EIFR, INTF7); } while (0)
#define IS_SET_INT7_vect_FLAG()	_IS_SET(EIFR, INTF7)
#define SET_INT7_IRQ_GENERATED_ON_LOW_LEVEL() \
    do { \
        _BIT_CLR(EICRB, ISC71); \
        _BIT_CLR(EICRB, ISC70); \
    } while (0)
#define SET_INT7_IRQ_GENERATED_ON_FALLING_EDGE() \
    do { \
        _BIT_SET(EICRB, ISC71); \
        _BIT_CLR(EICRB, ISC70); \
    } while (0)
#define SET_INT7_IRQ_GENERATED_ON_RISING_EDGE() \
    do { \
        _BIT_SET(EICRB, ISC71); \
        _BIT_SET(EICRB, ISC70); \
    } while (0)


// Timer/Counter Interrupt MaSK register - TIMSK 
// Timer/Counter Interrupt Flag Register - TIFR 
// Extended Timer Interrupt MaSK register - ETIMSK
// Extended Timer Interrupt Flag Register - ETIFR
#define ENABLE_TIMER0_OVF_vect()        do { _BIT_SET(TIMSK, TOIE0); } while (0)
#define DISABLE_TIMER0_OVF_vect()       do { _BIT_CLR(TIMSK, TOIE0); } while (0)
#define CLEAR_TIMER0_OVF_vect()         do { _BIT_SET(TIFR, TOV0); } while (0)
#define SET_TIMER0_OVF_vect()           do { TCNT0=255; while(!_IS_SET(TIFR, TOV0));} while (0)

#define ENABLE_TIMER0_COMP_vect()       do { _BIT_SET(TIMSK, OCIE0); } while (0)
#define DISABLE_TIMER0_COMP_vect()      do { _BIT_CLR(TIMSK, OCIE0); } while (0)
#define CLEAR_TIMER0_COMP_vect()        do { _BIT_SET(TIFR, OCF0); } while (0)

#define ENABLE_TIMER2_OVF_vect()        do { _BIT_SET(TIMSK, TOIE2); } while (0)
#define DISABLE_TIMER2_OVF_vect()       do { _BIT_CLR(TIMSK, TOIE2); } while (0)
#define CLEAR_TIMER2_OVF_vect()         do { _BIT_SET(TIFR, TOV2); } while (0)

#define ENABLE_TIMER2_COMP_vect()       do { _BIT_SET(TIMSK, OCIE2); } while (0)
#define DISABLE_TIMER2_COMP_vect()      do { _BIT_CLR(TIMSK, OCIE2); } while (0)
#define CLEAR_TIMER2_COMP_vect()        do { _BIT_SET(TIFR, OCF2); } while (0)

#define ENABLE_TIMER1_OVF_vect()        do { _BIT_SET(TIMSK, TOIE1); } while (0)
#define DISABLE_TIMER1_OVF_vect()       do { _BIT_CLR(TIMSK, TOIE1); } while (0)
#define CLEAR_TIMER1_OVF_vect()         do { _BIT_SET(TIFR, TOV1); } while(0)
#define IS_SET_TIMER1_OVF_vect()        _IS_SET(TIFR, TOV1)

#define ENABLE_TIMER1_COMPA_vect()      do { _BIT_SET(TIMSK, OCIE1A); } while (0)
#define DISABLE_TIMER1_COMPA_vect()     do { _BIT_CLR(TIMSK, OCIE1A); } while (0)
#define CLEAR_TIMER1_COMPA_vect()       do { _BIT_SET(TIFR, OCF1A); } while(0)

#define ENABLE_TIMER1_COMPB_vect()      do { _BIT_SET(TIMSK, OCIE1B); } while (0)
#define DISABLE_TIMER1_COMPB_vect()     do { _BIT_CLR(TIMSK, OCIE1B); } while (0)
#define CLEAR_TIMER1_COMPB_vect()       do { _BIT_SET(TIFR, OCF1B); } while(0)

#define ENABLE_TIMER1_COMPC_vect()      do { _BIT_SET(ETIMSK, OCIE1C); } while (0)
#define DISABLE_TIMER1_COMPC_vect()     do { _BIT_CLR(ETIMSK, OCIE1C); } while (0)
#define CLEAR_TIMER1_COMPC_vect()       do { _BIT_SET(ETIFR, OCF1C); } while(0)

#define ENABLE_TIMER1_CAPT_vect()       do { _BIT_SET(TIMSK, TICIE1); } while (0)
#define DISABLE_TIMER1_CAPT_vect()      do { _BIT_CLR(TIMSK, TICIE1); } while (0)
#define CLEAR_TIMER1_CAPT_vect()        do { _BIT_SET(TIFR, ICF1); } while(0)

#define ENABLE_TIMER3_OVF_vect()        do { _BIT_SET(ETIMSK, TOIE3); } while (0)
#define DISABLE_TIMER3_OVF_vect()       do { _BIT_CLR(ETIMSK, TOIE3); } while (0)
#define CLEAR_TIMER3_OVF_vect()         do { _BIT_SET(ETIFR, TOV3); } while(0)

#define ENABLE_TIMER3_COMPA_vect()      do { _BIT_SET(ETIMSK, OCIE3A); } while (0)
#define DISABLE_TIMER3_COMPA_vect()     do { _BIT_CLR(ETIMSK, OCIE3A); } while (0)
#define CLEAR_TIMER3_COMPA_vect()       do { _BIT_SET(ETIFR, OCF3A); } while(0)

#define ENABLE_TIMER3_COMPB_vect()      do { _BIT_SET(ETIMSK, OCIE3B); } while (0)
#define DISABLE_TIMER3_COMPB_vect()     do { _BIT_CLR(ETIMSK, OCIE3B); } while (0)
#define CLEAR_TIMER3_COMPB_vect()       do { _BIT_SET(ETIFR, OCF3B); } while(0)

#define ENABLE_TIMER3_COMPC_vect()      do { _BIT_SET(ETIMSK, OCIE3C); } while (0)
#define DISABLE_TIMER3_COMPC_vect()     do { _BIT_CLR(ETIMSK, OCIE3C); } while (0)
#define CLEAR_TIMER3_COMPC_vect()       do { _BIT_SET(ETIFR, OCF3C); } while(0)

#define ENABLE_TIMER3_CAPT_vect()       do { _BIT_SET(ETIMSK, TICIE3); } while (0)
#define DISABLE_TIMER3_CAPT_vect()      do { _BIT_CLR(ETIMSK, TICIE3); } while (0)
#define CLEAR_TIMER3_CAPT_vect()        do { _BIT_SET(ETIFR, ICF3); } while(0)


// USART1 Control Register B - UCSR1B
// USART0 Control Register B - UCSR0B
#define ENABLE_USART0_RX_vect()         do { _BIT_SET(UCSR0B, RXCIE0); } while (0)
#define DISABLE_USART0_RX_vect()        do { _BIT_CLR(UCSR0B, RXCIE0); } while (0)
#define USART0_RX_vect_IS_SET()         _IS_SET(UCSR0B, RXCIE0)

#define ENABLE_USART0_TX_vect()         do { _BIT_SET(UCSR0B, TXCIE0); } while (0)
#define DISABLE_USART0_TX_vect()        do { _BIT_CLR(UCSR0B, TXCIE0); } while (0)

#define ENABLE_USART0_UDRE_vect()       do { _BIT_SET(UCSR0B, UDRIE0); } while (0)		//data register empty interrupt
#define DISABLE_USART0_UDRE_vect()      do { _BIT_CLR(UCSR0B, UDRIE0); } while (0)

#define ENABLE_USART1_RX_vect()         do { _BIT_SET(UCSR1B, RXCIE1); } while (0)
#define DISABLE_USART1_RX_vect()        do { _BIT_CLR(UCSR1B, RXCIE1); } while (0)
#define USART1_RX_vect_IS_SET()         _IS_SET(UCSR1B, RXCIE1)

#define ENABLE_USART1_TX_vect()         do { _BIT_SET(UCSR1B, TXCIE1); } while (0)
#define DISABLE_USART1_TX_vect()        do { _BIT_CLR(UCSR1B, TXCIE1); } while (0)

#define ENABLE_USART1_UDRE_vect()       do { _BIT_SET(UCSR1B, UDRIE1); } while (0)
#define DISABLE_USART1_UDRE_vect()      do { _BIT_CLR(UCSR1B, UDRIE1); } while (0)


// SPI Serial Transfer Complete
#define ENABLE_SPI_STC_vect()           do { _BIT_SET(SPCR, SPIE); } while (0)	//Serial Transfer Complete
#define DISABLE_SPI_STC_vect()          do { _BIT_CLR(SPCR, SPIE); } while (0)
//#define CLEAR_SPI_STC_vect()          do { _BIT_SET(SPSR, SPIF); SPDR =0; } while (0) //first reading the SPSR with SPIF set, then accessing the SPDR.

// ADC Conversion Complete 
#define ENABLE_ADC_vect()               do { _BIT_SET(ADCSRA, ADIE); } while (0)
#define DISABLE_ADC_vect()              do { _BIT_CLR(ADCSRA, ADIE); } while (0)

// EEPROM Ready, no flag
#define ENABLE_EE_READY_vect()          do { _BIT_SET(EECR, EERIE); } while (0)
#define DISABLE_EE_READY_vect()         do { _BIT_CLR(EECR, EERIE); } while (0)

// Analog Comparator
#define ENABLE_ANALOG_COMP_vect()       do { _BIT_SET(ACSR, ACIE); } while (0)
#define DISABLE_ANALOG_COMP_vect()      do { _BIT_CLR(ACSR, ACIE); } while (0)

// 2-wire Serial Interface
#define ENABLE_TWI_vect()               do { _BIT_SET(TWCR, TWIE); } while (0)
#define DISABLE_TWI_vect()              do { _BIT_CLR(TWCR, TWIE); } while (0)
#define CLEAR_TWI_vect()                do { _BIT_SET(TWCR, TWINT); } while (0)	// Note that this flag is not automatically cleared by hardware

// Store Program Memory Read, no flag
#define ENABLE_SPM_READY_vect()         do { _BIT_SET(SPMCSR, SPMIE); } while (0)
#define DISABLE_SPM_READY_vect()        do { _BIT_CLR(SPMCSR, SPMIE); } while (0)
/************************* end of ATmega128L 34 Interrupts ***************************/

#endif	// ~INTR_H
