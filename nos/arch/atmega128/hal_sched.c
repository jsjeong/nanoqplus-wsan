// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Sangcheol Kim (ETRI)
 */

#include "hal_sched.h"

#ifdef KERNEL_M
#include <avr/io.h>
#include <avr/interrupt.h>

#include "intr.h"
#include "critical_section.h"
#include "sched.h"

#define HAL_SCHED_DEBUG
#ifdef HAL_SCHED_DEBUG
#include "uart.h"
#include "platform.h"
#endif

extern UINT8 nos_ctx_sw_intr_exists;

void nos_sched_hal_init()
{
    // CTC mode. No compare output (normal port operation). 
    // Timer clock = _SYSTEM_CLOCK/8 
    // COM3A = 00, COM3B = 00, COM3C = 00, WGM3[1:0] = 00
    TCCR3A = 0x00;
    // ICNC3 = 0, ICES3 =0, WGM3[3:2]=01, CS3[2:0] = 010 (clk/8)
    TCCR3B = (1 << WGM32)|(1 << CS31);

    // Set scheduling period
    OCR3A = (UINT16)SCHED_TICKS;
}

void nos_sched_timer_start()
{
    // Timer/Counter interrupt flag register setting 
    TCNT3 = 0;
    CLEAR_TIMER3_COMPA_vect();	// clear timer interrupt flag
    ENABLE_TIMER3_COMPA_vect();	//activate timer interrupt
}


ISR(TIMER3_COMPA_vect)
{
    NOS_ENTER_ISR();
    nos_sched_handler();
    NOS_EXIT_ISR();
    nos_ctx_sw_handler();
}

#endif
