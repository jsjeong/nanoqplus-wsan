// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file uart_init.c
 * @author Sangcheol Kim (ETRI)
 * @author Haeyong Kim (ETRI)
 */

#include "kconf.h"
#ifdef UART_M

#include <avr/io.h>
#include "uart.h"
#include "platform.h"

#ifdef ATMEGA128_USART0_BR_9600
#define ATMEGA128_USART0_BR 9600
#elif defined ATMEGA128_USART0_BR_19200
#define ATMEGA128_USART0_BR 19200
#elif defined ATMEGA128_USART0_BR_38400
#define ATMEGA128_USART0_BR 38400
#elif defined ATMEGA128_USART0_BR_57600
#define ATMEGA128_USART0_BR 57600
#elif defined ATMEGA128_USART0_BR_115200
#define ATMEGA128_USART0_BR 115200
#elif defined ATMEGA128_USART0_BR_230400
#define ATMEGA128_USART0_BR 230400
#elif defined ATMEGA128_USART0_BR_500000
#define ATMEGA128_USART0_BR 500000
#elif defined ATMEGA128_USART0_BR_1000000
#define ATMEGA128_USART0_BR 1000000
#endif

#ifdef ATMEGA128_USART1_BR_9600
#define ATMEGA128_USART1_BR 9600
#elif defined ATMEGA128_USART1_BR_19200
#define ATMEGA128_USART1_BR 19200
#elif defined ATMEGA128_USART1_BR_38400
#define ATMEGA128_USART1_BR 38400
#elif defined ATMEGA128_USART1_BR_57600
#define ATMEGA128_USART1_BR 57600
#elif defined ATMEGA128_USART1_BR_115200
#define ATMEGA128_USART1_BR 115200
#elif defined ATMEGA128_USART1_BR_230400
#define ATMEGA128_USART1_BR 230400
#elif defined ATMEGA128_USART1_BR_500000
#define ATMEGA128_USART1_BR 500000
#elif defined ATMEGA128_USART1_BR_1000000
#define ATMEGA128_USART1_BR 1000000
#endif

void nos_uart_init(UINT8 port_num)
{
#ifdef ATMEGA128_USART0_UART
    if (port_num == ATMEGA128_UART0)
    {
        UCSR0A |= (1 << U2X0);
        UBRR0L = _SYSTEM_CLOCK / (8L * ATMEGA128_USART0_BR) - 1;
        UBRR0H = (_SYSTEM_CLOCK / (8L * ATMEGA128_USART0_BR) - 1) >> 8;
        UCSR0C =  (0 << UPM1) | (0 << UPM0); 
        UCSR0C |= (0 << USBS);
        UCSR0C |= (1 << UCSZ1) | (1 << UCSZ0);
        UCSR0B = (0 << UCSZ2);
        UCSR0B |=  (1 << TXEN) | (1 << RXEN);
        return;
    }
#endif

#ifdef ATMEGA128_USART1_UART
    else
    {
        UCSR1A |= (1 << U2X1);
        UBRR1L = _SYSTEM_CLOCK / (8L * ATMEGA128_USART1_BR) - 1;
        UBRR1H = (_SYSTEM_CLOCK / (8L * ATMEGA128_USART1_BR) - 1) >> 8;
        UCSR1C =  (0 << UPM1) | (0 << UPM0); 
        UCSR1C |= (0 << USBS);
        UCSR1C |= (1 << UCSZ1) | (1 << UCSZ0);
        UCSR1B = (0 << UCSZ2);
        UCSR1B |=  (1 << TXEN) | (1 << RXEN);
        return;
    }
#endif
}
#endif //UART_M
