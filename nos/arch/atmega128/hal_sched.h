// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 * @author Haeyong Kim (ETRI)
 */

#ifndef HAL_SCHED_H
#define HAL_SCHED_H
#include "kconf.h"
#ifdef KERNEL_M

#include "nos_common.h"
#include "platform.h"
#include "intr.h"

// Schedule period. 32ms is the maximum period.
// SCHED_TICKS = Clock(Hz)/prescaler * SCHED_PEROID(ms)/1000(sec)-1
// Note that changing this value does not mean channging tick interrupt interval.
#ifdef SCHED_PERIOD_5
#define SCHED_TIMER_MS		5
#define SCHED_TICKS		(_SYSTEM_CLOCK/1600-1)
#elif SCHED_PERIOD_10
#define SCHED_TIMER_MS		10
#define SCHED_TICKS		(_SYSTEM_CLOCK/800-1)
#else	// SCHED_PERIOD_32
#define SCHED_TIMER_MS		32
#define SCHED_TICKS		(_SYSTEM_CLOCK/250-1)
#endif

#undef KERNEL_DEFERRED_CTX_SW
#define NOS_SCHED_PENDING_SET()	\
    do {\
        TCNT3=SCHED_TICKS-1;\
        while(!_IS_SET(ETIFR, OCF3A));\
    } while (0)

void nos_sched_hal_init(void);
void nos_sched_timer_start(void);

#endif // KERNEL_M
#endif // ~HAL_SCHED_H
