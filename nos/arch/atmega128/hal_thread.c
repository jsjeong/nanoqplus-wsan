// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 */

#include "hal_thread.h"

#ifdef THREAD_M
#include "arch.h"
#include "sched.h"
#include "thread.h"

// initialize stack
// This is necessary because a context should be placed in the stack when context switching.
// After context switching call, the thread with this stack will be executed starting from the function pointer (func)
// kkamagui thinking move to hal or not?
STACK_PTR nos_tcb_stack_init(void (*func)(void), STACK_PTR fos, UINT16 stack_size)
{
    STACK_PTR  sptr; // stack pointer
    UINT16  temp_addr;
    sptr = (STACK_PTR)(fos + (stack_size-1)) ; // stack pointer(8bit) indicates the top of stack
    temp_addr  = (UINT16) func; // the starting address of the task

    // Initially we pushes the starting address of the task
	// As soon as context switching, this thread will go jump to the start address (function pointer) of this function.
    *sptr-- = (UINT8) temp_addr;            // PCL to be restored
    *sptr-- = (UINT8) (temp_addr >> 8);     // PCH to be restored

	// Insert fills 0x00, which is not used anywhere, into the stack.
    *sptr-- = (UINT8) 0x00;         // r1 = 0x00; push r1
    *sptr-- = (UINT8) 0x00;         // r0 = 0x00; push r0
    *sptr-- = (UINT8) 0x00;         // r2 = 0x00; push r2
    *sptr-- = (UINT8) 0x00;         // r3 = 0x00; push r3
    *sptr-- = (UINT8) 0x00;         // r4 = 0x00; push r4
    *sptr-- = (UINT8) 0x00;         // r5 = 0x00; push r5
    *sptr-- = (UINT8) 0x00;         // r6 = 0x00; push r6
    *sptr-- = (UINT8) 0x00;         // r7 = 0x00; push r7
    *sptr-- = (UINT8) 0x00;         // r8 = 0x00; push r8
    *sptr-- = (UINT8) 0x00;         // r9 = 0x00; push r9
    *sptr-- = (UINT8) 0x00;         // r10 = 0x00; push r10
    *sptr-- = (UINT8) 0x00;         // r11 = 0x00; push r11
    *sptr-- = (UINT8) 0x00;         // r12 = 0x00; push r12
    *sptr-- = (UINT8) 0x00;         // r13 = 0x00; push r13
    *sptr-- = (UINT8) 0x00;         // r14 = 0x00; push r14
    *sptr-- = (UINT8) 0x00;         // r15 = 0x00; push r15
    *sptr-- = (UINT8) 0x00;         // r16 = 0x00; push r16
    *sptr-- = (UINT8) 0x00;         // r17 = 0x00; push r17
    *sptr-- = (UINT8) 0x00;         // r18 = 0x00; push r18
    *sptr-- = (UINT8) 0x00;         // r19 = 0x00; push r19
    *sptr-- = (UINT8) 0x00;         // r20 = 0x00; push r20
    *sptr-- = (UINT8) 0x00;         // r21 = 0x00; push r21
    *sptr-- = (UINT8) 0x00;         // r22 = 0x00; push r22
    *sptr-- = (UINT8) 0x00;         // r23 = 0x00; push r23
    *sptr-- = (UINT8) 0x00;         // r24 = 0x00; push r23
    *sptr-- = (UINT8) 0x00;         // r25 = 0x00; push r23
    *sptr-- = (UINT8) 0x00;         // r26 = 0x00; push r26
    *sptr-- = (UINT8) 0x00;         // r27 = 0x00; push r27
    *sptr-- = (UINT8) 0x00;         // r28 = 0x00; push r28
    *sptr-- = (UINT8) 0x00;         // r29 = 0x00; push r29
    *sptr-- = (UINT8) 0x00;         // r30 = 0x00; push r30
    *sptr-- = (UINT8) 0x00;         // r31 = 0x00; push r31

    // SREG = 0x80; enables interrupt for context switching
    *sptr-- = (UINT8) 0x80;         // SREG ; push SREG

    return ((STACK_PTR) sptr);	// eos
}

void nos_context_switch_core(void)
{
    // Save current thread state
    NOS_THREAD_SAVE_STATE();
    __SAVE_SP(tcb[_rtid]->sptr);

    _rtid = _next_rtid;
    // the selected thread will run soon after restoring context
    tcb[_rtid]->state = RUNNING_STATE;

    //Load new thread state
    __LOAD_SP(tcb[_rtid]->sptr);
    NOS_THREAD_LOAD_STATE();
}

// just for 'EXIT_STATE' thread
void nos_context_load_core(void)
{
    tcb[_rtid]->state = RUNNING_STATE;
    // Load new thread state
    // EXC_RETURN+Global Interrupt Enable
	__LOAD_SP(tcb[_rtid]->sptr);
	NOS_THREAD_LOAD_STATE();
	//NOS_THREAD_LOAD_STATE_WITHOUT_EXEC_RETURN();
}
#endif //THREAD_M
