/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef GPIO_H
#define GPIO_H

#include "kconf.h"
#include "nos_common.h"

#define PORT_REG(n)                     PORT##n
#define DDR_REG(n)                      DDR##n
#define PIN_REG(n)                      PIN##n

#define NOS_GPIO_SET_IN(PORT, PIN)      _BIT_CLR(DDR_REG(PORT), PIN)
#define NOS_GPIO_SET_OUT(PORT, PIN)     _BIT_SET(DDR_REG(PORT), PIN)
#define NOS_GPIO_READ(PORT, PIN)        _IS_SET(PIN_REG(PORT), PIN)
#define NOS_GPIO_ON(PORT, PIN)          _BIT_SET(PORT_REG(PORT), PIN)
#define NOS_GPIO_OFF(PORT, PIN)         _BIT_CLR(PORT_REG(PORT), PIN)
#define NOS_GPIO_TOGGLE(PORT, PIN) \
    do { \
        PORT_REG(PORT) = PIN_REG(PORT) ^ (1 << PIN); \
    } while(0)

#define NOS_GPIO_INIT_IN(PORT, PIN)     NOS_GPIO_SET_IN(PORT, PIN)
#define NOS_GPIO_INIT_OUT(PORT, PIN)    NOS_GPIO_SET_OUT(PORT, PIN)
#define NOS_GPIO_INIT_PERIPHERAL(PORT, PIN)

#endif //~GPIO_H
