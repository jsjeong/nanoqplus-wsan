// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * EAP (Extensible Authentication Protocol) Message Types
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 8. 20.
 */

#ifndef EAP_TYPES_H
#define EAP_TYPES_H

#include "kconf.h"
#ifdef EAP_M
#include "nos_common.h"

enum eap_code
{
    EAP_CODE_REQUEST = 1,
    EAP_CODE_RESPONSE = 2,
    EAP_CODE_SUCCESS = 3,
    EAP_CODE_FAILURE = 4,
};

#pragma pack(1)
struct eap_request
{
    UINT8 eap_code;
    UINT8 id;
    UINT16 len;
    UINT8 type;
    // (Type-Data ...)
};
#pragma pack()

#pragma pack(1)
struct eap_response
{
    UINT8 eap_code;
    UINT8 id;
    UINT16 len;
    UINT8 type;
    // (Type-Data ...)
};
#pragma pack()

#pragma pack(1)
struct eap_success
{
    UINT8 eap_code;
    UINT8 id;
    UINT16 len;
};
#pragma pack()

#pragma pack(1)
struct eap_failure
{
    UINT8 eap_code;
    UINT8 id;
    UINT16 len;
};
#pragma pack()

enum eap_type
{
    EAP_TYPE_IDENTITY = 1,
    EAP_TYPE_NOTIFICATION = 2,
    EAP_TYPE_NAK = 3,
    EAP_TYPE_MD5_CHALLENGE = 4,
    EAP_TYPE_OTP = 5,
    EAP_TYPE_GTC = 6,
    EAP_TYPE_EXPANDED = 254,
    EAP_TYPE_EXPERIMENTAL = 255,
};

#endif //EAP_M
#endif //EAP_TYPES_H
