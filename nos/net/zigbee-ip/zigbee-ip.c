// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ZigBee IP
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 7. 29.
 */

#include "zigbee-ip.h"
#ifdef ZIGBEE_IP_M
#include "6lowpan.h"
#include "mac.h"
#include "wpan-dev.h"
#include "mle.h"
#include "zigbee-ip-mle.h"
#include "pana.h"

struct zip_control zip[CONFIG_MAX_NUM_ZIGBEE_IP_INTERFACE] = { { 0 } };
extern struct ip6_ctrl ip6;
extern WPAN_DEV wpan_dev[];

#ifdef IEEE_802_15_4_FFD
ERROR_T zip_set_beacon_allow_join(UINT8 inf, BOOL enable)
{
    if (enable)
        _BIT_SET(zip[inf].beacon.control_field, 0);
    else
        _BIT_CLR(zip[inf].beacon.control_field, 0);

    nos_mac_set_beacon_payload_ptr(zip[inf].wpan_dev,
                                   &zip[inf].beacon,
                                   sizeof(struct zip_control));
    return ERROR_SUCCESS;
}

ERROR_T zip_set_beacon_router_capacity(UINT8 inf, BOOL enable)
{
    if (enable)
        _BIT_SET(zip[inf].beacon.control_field, 1);
    else
        _BIT_CLR(zip[inf].beacon.control_field, 1);

    nos_mac_set_beacon_payload_ptr(zip[inf].wpan_dev,
                                   &zip[inf].beacon,
                                   sizeof(struct zip_control));
    return ERROR_SUCCESS;
}

ERROR_T zip_set_beacon_host_capacity(UINT8 inf, BOOL enable)
{
    if (enable)
        _BIT_SET(zip[inf].beacon.control_field, 2);
    else
        _BIT_CLR(zip[inf].beacon.control_field, 2);

    nos_mac_set_beacon_payload_ptr(zip[inf].wpan_dev,
                                   &zip[inf].beacon,
                                   sizeof(struct zip_control));
    return ERROR_SUCCESS;
}
#endif

static UINT8 get_zip_iid(UINT8 dev_id)
{
    UINT8 i;

    for (i = 0; i < CONFIG_MAX_NUM_ZIGBEE_IP_INTERFACE; i++)
    {
        if (zip[i].valid && zip[i].wpan_dev == dev_id)
            break;
    }

    return i;
}

static UINT8 get_zip_ip6_inf(UINT8 inf)
{
    UINT8 i;

    for (i = 0; i < ip6_ctrl.nnic; i++)
    {
        if (zip[i].valid && zip[i].ip6_inf == inf)
            break;
    }

    return i;
}

/**
 * Temporary frame reception handler until ZIP network discovery is completed.
 */
static void zip_rx_handler(UINT8 dev)
{
    UINT8 i;

    i = get_zip_iid(dev);
    
    if (i >= CONFIG_MAX_NUM_ZIGBEE_IP_INTERFACE)
        return;

    if (zip[i].phase == ZIP_PHASE_DISCOVERING)
    {
        NOS_MAC_RX_INFO rx_info;
        
        //Just readout from the MAC queue and discard non-beacon frames.
        while(nos_mac_rx(dev, &rx_info) == ERROR_SUCCESS);
    }
}

static void zip_mle_listener(UINT8 inf,
                             const IP6_ADDRESS *src,
                             enum mle_command cmd,
                             const void *tlvs,
                             UINT8 tlvs_len)
{
    UINT8 i;
    const struct zip_mle_tlv_src_addr *tlv_src_addr = NULL;
    const struct zip_mle_tlv_mode *tlv_mode = NULL;

    printf("%s()-received %d, TLVs len:%u\n", __FUNCTION__, cmd, tlvs_len);

    i = get_zip_ip6_inf(inf);
    if (i >= ip6_ctrl.nnic)
        return;
    
    if (zip[i].phase == ZIP_PHASE_PARENT_ESTB_REQ)
    {
        if (cmd == MLE_CMD_LINK_ACCEPT)
        {
            tlv_src_addr = mle_extract_tlv(MLE_TLV_SOURCE_ADDRESS, tlvs, tlvs_len);
            if (tlv_src_addr &&
                ntohs(tlv_src_addr->addr) == zip[i].parent_id)
            {
                struct lowpan_neighbor *neigh;

                neigh = &GET_6LOWPAN_INFO(inf)->neighbor[zip[i].parent_neigh_idx];
                
                memcpy(neigh->eui64, &src->s6_addr[8], 8);
                neigh->eui64_valid = TRUE;
                
                // Parent establishment
                zip[i].phase = ZIP_PHASE_PARENT_ESTB;

                pana_client_init(zip[i].ip6_inf, src, NULL);
            }
        }
    }   
}

static void zip_rx_beacon_handler(UINT8 dev, const struct wpan_rx_frame *beacon)
{
    UINT8 i;

    i = get_zip_iid(dev);

    if (i >= CONFIG_MAX_NUM_ZIGBEE_IP_INTERFACE)
        return;
    
    printf("%s()-beacon seq:%u,ZigBee Protocol ID:0x%02X,Ctrl:0x%02X,NetID:%s\n",
           __FUNCTION__, beacon->buf[2],
           beacon->buf[beacon->mhr_len + 2],
           beacon->buf[beacon->mhr_len + 3],
           &beacon->buf[beacon->mhr_len + 4]);

    if (beacon->buf[beacon->mhr_len + 2] == ZIGBEE_PROTOCOL_ZIP &&
        beacon->len - beacon->mhr_len == sizeof(struct zip_beacon_payload))
    {
        //TODO Collect beacons.
        struct zip_beacon_payload *beacon_payload =
            (struct zip_beacon_payload *) &beacon->buf[beacon->mhr_len];
        UINT16 pan_id;

        pan_id = beacon->buf[3] + (beacon->buf[4] << 8);

        //Invalid PAN ID
        if (pan_id == 0xffff)
            return;

        //The ZIP network does not allow join.
        if (!_IS_SET(beacon_payload->control_field, ZIP_BEACON_ALLOW_JOIN))
            return;

        //The ZIP network does not permit joining new ZIP routers.
        if (zip[i].dev_type == ZIP_ROUTER &&
            !_IS_SET(beacon_payload->control_field, ZIP_BEACON_ROUTER_CAPACITY))
            return;

        //The ZIP network does not permit joining new ZIP hosts.
        if ((zip[i].dev_type == ZIP_HOST || zip[i].dev_type == ZIP_SLEEPY_HOST) &&
            !_IS_SET(beacon_payload->control_field, ZIP_BEACON_HOST_CAPACITY))
            return;

        zip[i].pan_id = beacon->buf[3] + (beacon->buf[4] << 8);
        zip[i].parent_id = beacon->buf[5] + (beacon->buf[6] << 8);
        zip[i].beacon = *beacon_payload;

        if (zip[i].dev_type == ZIP_HOST)
            zip[i].phase = ZIP_PHASE_PARENT_ESTB_REQ;
        else if (zip[i].dev_type == ZIP_ROUTER)
            zip[i].phase = ZIP_PHASE_AUTHENTICATION;
    }
}

#ifdef IEEE_802_15_4_FFD
static void zip_rx_mac_command_handler(UINT8 dev, const struct wpan_rx_frame *command)
{
    printf("%s()-command seq:%u\n", __FUNCTION__, command->buf[2]);
}
#endif

void zip_init(UINT8 dev, const UINT8 *eui64, enum zip_device_type type)
{
    UINT8 i;
    IP6_ADDRESS parent_addr;
    struct zip_mle_tlv_mode mode_tlv;

    i = get_zip_iid(dev);
    
    //To avoid ZigBee IP initialization on a same WPAN device.
    if (i < CONFIG_MAX_NUM_ZIGBEE_IP_INTERFACE)
        return;

    for (i = 0; i < CONFIG_MAX_NUM_ZIGBEE_IP_INTERFACE; i++)
    {
        if (!zip[i].valid)
            break;
    }

    if (i >= CONFIG_MAX_NUM_ZIGBEE_IP_INTERFACE)
        return;
    
    nos_mac_init(dev, 0xffff, 0xffff, eui64, MAC_TYPE_NANO_MAC, zip_rx_handler);
    nos_mac_set_beacon_handler(dev, zip_rx_beacon_handler);

    zip[i].valid = TRUE;
    zip[i].wpan_dev = dev;
    zip[i].dev_type = type;
    zip[i].phase = ZIP_PHASE_DISCOVERING;
    zip[i].pan_id = 0xffff;
    zip[i].parent_id = 0xffff;
    
#ifdef IEEE_802_15_4_FFD
    zip[i].beacon.superframe_spec_l = 0xff;
    zip[i].beacon.superframe_spec_h = 0xff;
    zip[i].beacon.zigbee_protocol_id = 0x02;
    zip[i].beacon.control_field = 0;
    memset(zip[i].beacon.zip_network_id, 0, 16);
#endif
    
    //TODO Energy Detection scan
    
    //Active scan
    while (zip[i].phase == ZIP_PHASE_DISCOVERING)
    {
        UINT8 power;
        UINT32 wait_usec = 1;
        NOS_MAC_TX_INFO tx_info;

        tx_info.frame_type = IEEE_802_15_4_FRAME_MAC_COMMAND;
        tx_info.dest_addr = 0xffff;
        
        // Since the assigned short ID is 0xffff, the frame will not have a source address.
        tx_info.use_short_src = TRUE;
        
        tx_info.payload_length = 1;
        tx_info.payload[0] = IEEE_802_15_4_BEACON_REQUEST;

        nos_mac_tx(zip[i].wpan_dev, &tx_info);

        for (power = 0; power < ZIP_SCAN_DURATION; power++)
            wait_usec *= 2;

        wait_usec = 960 * (wait_usec + 1) * wpan_dev[zip[i].wpan_dev].symbol_duration;

        //TODO ScanDuration 13, 14 are not correctly supported.
        nos_delay_ms(wait_usec / 1000);
        nos_delay_us(wait_usec % 1000);
    }

    printf("End of discovering phase\n");

    zip[i].ip6_inf = nos_lowpan_init(zip[i].wpan_dev,
                                     zip[i].pan_id,
                                     0xffff,
                                     eui64,
                                     FALSE);
    
    if (zip[i].ip6_inf >= ip6_ctrl.nnic)
    {
        //TODO It can't be happened.
        while(1);
    }
    //nos_lowpan_set_security_mode(zip[i].wpan_dev, IEEE_802_15_4_SEC_NONE);
    
    memcpy(&parent_addr,
           "\xfe\x80\x00\x00\x00\x00\x00\x00"
           "\x00\x00\x00\xff\xfe\x00", 14);
    parent_addr.s6_addr[14] = zip[i].parent_id >> 8;
    parent_addr.s6_addr[15] = zip[i].parent_id & 0xff;
    zip[i].parent_neigh_idx = nos_ip6_add_neighbor(zip[i].ip6_inf,
                                                   &parent_addr);
    nos_lowpan_init_neighbor(GET_6LOWPAN_INFO(zip[i].ip6_inf),
                             zip[i].parent_neigh_idx,
                             &parent_addr.s6_addr[14], 2);

    mle_start(zip[i].ip6_inf, zip_mle_listener);

    // Make TLVs.
    if (zip[i].dev_type == ZIP_HOST)
    {
        mode_tlv.tlv_type = MLE_TLV_MODE;
        mode_tlv.tlv_len = 1;
        mode_tlv.mode = 0x08; //Non-sleepy host
        
        mle_send(zip[i].ip6_inf, &parent_addr, NULL, 0,
                 MLE_CMD_LINK_REQUEST, &mode_tlv, sizeof(struct zip_mle_tlv_mode));
    }
    else if (zip[i].dev_type == ZIP_SLEEPY_HOST)
    {
        mode_tlv.tlv_type = MLE_TLV_MODE;
        mode_tlv.tlv_len = 1;
        mode_tlv.mode = 0x00; //Sleepy host
        
        mle_send(zip[i].ip6_inf, &parent_addr, NULL, 0,
                 MLE_CMD_LINK_REQUEST, &mode_tlv, sizeof(struct zip_mle_tlv_mode));
    }
}

#endif //ZIGBEE_IP_M
