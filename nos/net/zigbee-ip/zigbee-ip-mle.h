// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * MLE Types for ZigBee IP
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 8. 19.
 */

#ifndef ZIGBEE_IP_MLE_H
#define ZIGBEE_IP_MLE_H

#include "kconf.h"
#ifdef ZIGBEE_IP_M
#include "nos_common.h"

#pragma pack(1)
struct zip_mle_tlv_src_addr
{
    UINT8 tlv_type;
    UINT8 tlv_len;
    UINT16 addr;
};
#pragma pack()

#pragma pack(1)
struct zip_mle_tlv_mode
{
    UINT8 tlv_type;
    UINT8 tlv_len;
    UINT8 mode;
};
#pragma pack()

#pragma pack(1)
struct zip_mle_tlv_timeout
{
    UINT8 tlv_type;
    UINT8 tlv_len;
    UINT32 timeout_sec;
};
#pragma pack()

#pragma pack(1)
struct zip_mle_tlv_challenge
{
    UINT8 tlv_type;
    UINT8 tlv_len;
    // variable length value
};
#pragma pack()

#pragma pack(1)
struct zip_mle_tlv_response
{
    UINT8 tlv_type;
    UINT8 tlv_len;
    // variable length value
};
#pragma pack()

#pragma pack(1)
struct zip_mle_tlv_frame_counter
{
    UINT8 tlv_type;
    UINT8 tlv_len;
    UINT32 frame_counter;
};
#pragma pack()

#pragma pack(1)
struct zip_mle_tlv_link_quality
{
    UINT8 tlv_type;
    UINT8 tlv_len;
    // variable length value
};
#pragma pack()

#pragma pack(1)
struct zip_mle_tlv_param
{
    UINT8 tlv_type;
    UINT8 tlv_len;
    // variable length value
};
#pragma pack()

#endif //ZIGBEE_IP_M
#endif //ZIGBEE_IP_MLE_H
