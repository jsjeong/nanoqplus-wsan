// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ZigBee IP
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 7. 29.
 */

#ifndef ZIGBEE_IP_H
#define ZIGBEE_IP_H

#include "kconf.h"
#ifdef ZIGBEE_IP_M
#include "nos_common.h"
#include "errorcodes.h"

enum zigbee_protocol_id
{
    ZIGBEE_PROTOCOL_ZIP = 0x02,
};

enum zip_device_type
{
    ZIP_ROUTER,
    ZIP_HOST,
    ZIP_SLEEPY_HOST,
};

enum zip_beacon_payload_ctrl
{
    ZIP_BEACON_ALLOW_JOIN = 0,
    ZIP_BEACON_ROUTER_CAPACITY = 1,
    ZIP_BEACON_HOST_CAPACITY = 2,
};

enum
{
    ZIP_SCAN_DURATION = 2,
};

#pragma pack(1)
struct zip_beacon_payload
{
    UINT8 superframe_spec_l;
    UINT8 superframe_spec_h;
    UINT8 zigbee_protocol_id;
    UINT8 control_field;
    UINT8 zip_network_id[16];
};
#pragma pack()

enum zip_phase
{
    ZIP_PHASE_DISCOVERING,      //all
    ZIP_PHASE_PARENT_ESTB_REQ,  //(sleepy) host
    ZIP_PHASE_PARENT_ESTB,      //(sleepy) host
    ZIP_PHASE_AUTHENTICATION,   //all 
    ZIP_PHASE_PARENT_SYNC,      //all
    ZIP_PHASE_PARENT_ND,        //all
    ZIP_PHASE_NEIGH_SYNC,       //all
    ZIP_PHASE_RANKING,          //router
    ZIP_PHASE_DATA,             //all
};

struct zip_control
{
    BOOL valid:1;
    
    UINT8 wpan_dev;
    UINT8 ip6_inf;
    UINT16 pan_id;
    UINT16 parent_id;
    UINT8 parent_neigh_idx;
    enum zip_device_type dev_type;
    enum zip_phase phase;
    
    struct zip_beacon_payload beacon;
};

#ifdef IEEE_802_15_4_FFD
ERROR_T zip_set_beacon_allow_join(BOOL enable);
ERROR_T zip_set_beacon_router_capacity(BOOL enable);
ERROR_T zip_set_beacon_host_capacity(BOOL enable);
#endif

void zip_init(UINT8 dev, const UINT8 *eui64, enum zip_device_type type);

#endif //ZIGBEE_IP_M
#endif //ZIGBEE_IP_H
