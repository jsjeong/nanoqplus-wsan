// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Point-to-Point Protocol (PPP) over serial
 * 
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 11. 12.
 */

#include "kconf.h"
#ifdef PPP_M
#include "ppp-os.h"
#include "ppp.h"
#include "packet.h"
#include "ip6-interface.h"
#include "ip6.h"
#include "heap.h"
#include "critical_section.h"
#include "user_timer.h"
#include "uart.h"
#include "platform.h"
#include "thread.h"
#include "taskq.h"

#define FLUSH_SEC  5

struct ppp_os_control
{
    BOOL valid:1;               /* TRUE when PPPOS is initialized */
    BOOL connected:1;           /* TRUE when LCP and NCP have been passed. */
    UINT8 port_id;              /* UART port number */
    UINT8 ip6_inf;              /* IPv6 network interface ID */
    UINT8 pd;                   /* PPP control block ID */
    UINT8 rx_buf_head;          /* Low-level Rx buffer (queue) head */
    UINT8 rx_buf_tail;          /* Low-level Rx buffer (queue) tail */
    UINT8 rx_buf_cnt;           /* Number of characters in low-level Rx buffer */
    UINT8 rx_buf_flush_tick;    /* Remaining time to flush low-level Rx buffer */
    UINT8 rx_buf[256];          /* Low-level Rx buffer */
    UINT8 iid[8];               /* My EUI-64 */
    UINT8 hisiid[8];            /* Opponent side's EUI-64 */
    UINT8 *input_packet;        /* Buffer for a packet to be input to the IPv6
                                   stack. */
    UINT16 input_packet_len;    /* 'input_packet' length. 0 means no packet. */

    struct ip6_neighbor ip6_neigh;
    struct ip6_prefix ip6_prefix;
};

static struct ppp_os_control ppp_os_control[CONFIG_MAX_NUM_PPP_INTERFACE];
static INT8 dequeue_thread_id = -1;
extern struct ip6_ctrl ip6_ctrl;

static UINT8 get_ppp(UINT8 port)
{
    UINT8 i;

    for (i = 0; i < CONFIG_MAX_NUM_PPP_INTERFACE; i++)
    {
        if (ppp_os_control[i].valid &&
            ppp_os_control[i].port_id == port)
            return i;
    }

    return 255;
}

static void dequeue_thread(void *args)
{
    UINT8 i;
    UINT8 c;
    UINT8 buf[256];
    UINT8 buf_idx = 0;

    while (TRUE)
    {
        for (i = 0; i < CONFIG_MAX_NUM_PPP_INTERFACE; i++)
        {
            if (!ppp_os_control[i].valid)
                continue;
        
            NOS_ENTER_CRITICAL_SECTION();
            while (ppp_os_control[i].rx_buf_cnt > 0)
            {
                c = ppp_os_control[i].rx_buf[ppp_os_control[i].rx_buf_tail];
                ppp_os_control[i].rx_buf_tail++;
                ppp_os_control[i].rx_buf_cnt--;
                NOS_EXIT_CRITICAL_SECTION();

                buf[buf_idx++] = c;
                if (buf_idx == 0) //'buf_idx' is overflowed.
                {
                    pppos_input(ppp_os_control[i].pd, buf, 256);
                }

                NOS_ENTER_CRITICAL_SECTION();
            }
            NOS_EXIT_CRITICAL_SECTION();

            if (buf_idx > 0)
            {
                pppos_input(ppp_os_control[i].pd, buf, buf_idx);
                buf_idx = 0;
            }
        }

        nos_ctx_sw(); //Yield the processor.
    }
}

static void serial_input(UINT8 port, char c)
{
    UINT8 i;

    i = get_ppp(port);
    ppp_os_control[i].rx_buf[ppp_os_control[i].rx_buf_head++] = (UINT8) c;
    ppp_os_control[i].rx_buf_cnt++;
    ppp_os_control[i].rx_buf_flush_tick = FLUSH_SEC * 10;
}

static void flushbuf(void *args)
{
    UINT8 i;

    for (i = 0; i < CONFIG_MAX_NUM_PPP_INTERFACE; i++)
    {
        if (ppp_os_control[i].valid &&
            ppp_os_control[i].rx_buf_flush_tick > 0 &&
            --ppp_os_control[i].rx_buf_flush_tick == 0)
        {
            NOS_ENTER_CRITICAL_SECTION();
            ppp_os_control[i].rx_buf_head = 0;
            ppp_os_control[i].rx_buf_tail = 0;
            ppp_os_control[i].rx_buf_cnt = 0;
            NOS_EXIT_CRITICAL_SECTION();
            ppp_os_control[i].rx_buf_flush_tick = FLUSH_SEC * 10;
        }
    }
}

static void pppos_get_iid(const void *dev, UINT8 *dst)
{
    struct ppp_os_control *pc = (struct ppp_os_control *) dev;

    if (pc->connected)
    {
        memcpy(dst, pc->iid, 8);
    }
}

static BOOL pppos_has_iid(const void *dev, const UINT8 *iid)
{
    struct ppp_os_control *pc = (struct ppp_os_control *) dev;

    if (pc->connected && memcmp(pc->iid, iid, 8) == 0)
        return TRUE;
    else
        return FALSE;
}

static ERROR_T pppos_send(UINT8 out_inf, UINT8 nexthop, PACKET *ippkt)
{
    UINT16 len, pos;
    UINT8 *buf;
    PACKET *p;
    struct ppp_os_control *poc =
        (struct ppp_os_control *) (ip6_ctrl.nic[out_inf].dev);

    if (!poc->connected)
        return ERROR_IP6_NOT_READY;
    
    len = 4 + nos_packet_length(ippkt); //4: PPP header
    buf = nos_malloc(len);

    if (!buf)
        return ERROR_NOT_ENOUGH_MEMORY;

    buf[0] = PPP_ALLSTATIONS;
    buf[1] = PPP_UI;
    buf[2] = (PPP_IPV6 >> 8) & 0xFF;
    buf[3] = PPP_IPV6 & 0xFF;
    
    for (p = ippkt, pos = 4; p != NULL; p = p->next)
    {
        memcpy(&buf[pos], p->buf, p->buf_len);
        pos += p->buf_len;
    }

    pppWrite(poc->pd, buf, len);
    nos_free(buf);

    return ERROR_SUCCESS;
}

static void pppos_get_neighbor_iid(const void *dev, UINT8 idx, UINT8 *iid)
{
    struct ppp_os_control *pc = (struct ppp_os_control *) dev;

    if (pc->connected)
    {
        memcpy(iid, pc->hisiid, 8);
    }
}

static BOOL pppos_neighbor_has_iid(const void *dev,
                                   UINT8 idx,
                                   const IP6_ADDRESS *addr)
{
    struct ppp_os_control *pc = (struct ppp_os_control *) dev;

    if (pc->connected && memcmp(pc->hisiid, &addr->s6_addr[8], 8) == 0)
        return TRUE;
    else
        return FALSE;
}

static const struct ip6_interface_link_api pppos_api =
{
    pppos_get_iid,
    pppos_has_iid,
    NULL,
    pppos_send,
    pppos_get_neighbor_iid,
    pppos_neighbor_has_iid,

    NULL,            //recv_rs
    NULL,            //recv_ra
    NULL,            //recv_ns
    NULL,            //recv_na
    NULL,            //recv_redirect
    NULL,            //confirm_reach
    NULL,            //lifetime_tick
    NULL,            //state_changed
};

static void ppp_link_status_callback(void *ctx, int err, void *arg)
{
    struct ppp_os_control *pc = (struct ppp_os_control *) ctx;
    struct eui64_pair *p;
    UINT8 i;

    i = get_ppp(pc->port_id);

    if (err == PPPERR_NONE)
    {
        // Link connected.
        if (ppp_os_control[i].connected)
            return;

        p = (struct eui64_pair *) arg;
        /* printf("%x:%x:%x:%x\n", */
        /*        (UINT16) (p->ourid[0] << 8) + p->ourid[1], */
        /*        (UINT16) (p->ourid[2] << 8) + p->ourid[3], */
        /*        (UINT16) (p->ourid[4] << 8) + p->ourid[5], */
        /*        (UINT16) (p->ourid[6] << 8) + p->ourid[7]); */
        memcpy(ppp_os_control[i].iid, p->ourid, 8);
        memcpy(ppp_os_control[i].hisiid, p->hisid, 8);
        ppp_os_control[i].connected = TRUE;

        nos_ip6_set_state(pc->ip6_inf, IP6_NODE_STATE_HOST);
    }
    else if (err == PPPERR_CONNECT)
    {
        // Link disconnected.
        if (!ppp_os_control[i].connected)
            return;
        
        ppp_os_control[i].connected = FALSE;
    }
}

UINT16 sio_write(sio_fd_t fd, void *payload, UINT16 len)
{
    UINT16 i;

    for (i = 0; i < len; i++)
    {
        nos_uart_putc(fd, ((char *) payload)[i]);
    }

    return i;
}

static void pppos_ipv6_input(void *args)
{
    PACKET ip6pkt;
    struct ppp_os_control *pc = (struct ppp_os_control *) args;

    NOS_PACKET_INIT(&ip6pkt);
    nos_packet_set_buffer(&ip6pkt,
                          pc->input_packet,
                          pc->input_packet_len,
                          TRUE,                  // to be freed later
                          PROTOCOL_IP6);
    ip6pkt.alloc = FALSE;
    nos_ip6_recv(&ip6pkt, &ip6pkt, pc->ip6_inf, 0);
    
    pc->input_packet = NULL;
    pc->input_packet_len = 0;
}

void pppos_receive(void *ctx, const UINT8 *packet, UINT16 len)
{
    struct ppp_os_control *pc = (struct ppp_os_control *) ctx;

    if (pc->input_packet)
        return; //Drop. There is a packet to be processed.

    pc->input_packet = nos_malloc(len);
    if (!pc->input_packet)
        return; //Drop. No memory.
    
    memcpy(pc->input_packet, packet, len);
    pc->input_packet_len = len;

    if (!nos_taskq_reg(pppos_ipv6_input, (void *) pc))
    {
        nos_free(pc->input_packet);
        pc->input_packet_len = 0; //Drop. Task queue is full.
    }
}

UINT8 pppos_init(UINT8 port)
{
    UINT8 i;
    INT8 pd;
    
    if (port == STDIO)
    {
        return 255;
    }

    if (dequeue_thread_id < 0)
    {
        pppInit();
        dequeue_thread_id = nos_thread_create(dequeue_thread,
                                              NULL,
                                              0,
                                              NOS_PRIORITY_NORMAL);
        nos_user_timer_create_ms(flushbuf, NULL, 100, TRUE);
    }

    i = get_ppp(port);

    // Already PPP-initialized port found.
    if (i < CONFIG_MAX_NUM_PPP_INTERFACE)
        return 255;

    for (i = 0; i < CONFIG_MAX_NUM_PPP_INTERFACE; i++)
    {
        if (!ppp_os_control[i].valid)
            break;
    }

    // No empty slot.
    if (i >= CONFIG_MAX_NUM_PPP_INTERFACE)
        return 255;

    ppp_os_control[i].port_id = port;
    ppp_os_control[i].rx_buf_head = 0;
    ppp_os_control[i].rx_buf_tail = 0;
    ppp_os_control[i].rx_buf_cnt = 0;
    ppp_os_control[i].rx_buf_flush_tick = FLUSH_SEC * 10;
    ppp_os_control[i].connected = FALSE;
    ppp_os_control[i].input_packet = NULL;
    ppp_os_control[i].input_packet_len = 0;
    
    pd = pppOpen(port, ppp_link_status_callback, (void *) &ppp_os_control[i]);

    if (pd >= 0)
    {
        ppp_os_control[i].pd = pd;
        ppp_os_control[i].valid = TRUE;
        nos_uart_set_getc_callback(port, serial_input);
        nos_uart_enable_rx_intr(port);
        ppp_os_control[i].ip6_inf = nos_ip6_interface_init(&pppos_api,
                                                           IP6_INTERFACE_PPP,
                                                           &ppp_os_control[i],
                                                           &ppp_os_control[i].ip6_neigh,
                                                           1,
                                                           &ppp_os_control[i].ip6_prefix,
                                                           1,
                                                           CONFIG_PPP_MRU);
        
        /*
         * Link-local addresses of both side ends will be confirmed as unique by
         * NCP.
         */
        ip6_ctrl.nic[ppp_os_control[i].ip6_inf].ll_dad_passed = TRUE;
    }
    else
    {
        ppp_os_control[i].valid = FALSE;
        ppp_os_control[i].ip6_inf = 255;
    }
    
    //Starting protocols is triggered when the link is connected.
    
    return ppp_os_control[i].ip6_inf;
}

UINT8 pppos_get_ip6_inf(UINT8 port)
{
    UINT8 i = get_ppp(port);
    if (i < CONFIG_MAX_NUM_PPP_INTERFACE)
    {
        return ppp_os_control[i].ip6_inf;
    }
    else
    {
        return 255;
    }
}
#endif //PPP_M
