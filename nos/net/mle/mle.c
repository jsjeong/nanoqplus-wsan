// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * MLE (Mesh Link Establishment) Server
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 8. 14.
 */

#include "mle.h"
#ifdef MLE_M
#include "udp.h"
#include "heap.h"

struct mle_control
{
    UINT8 inf;
    MLE_RECV_HANDLER notify;
};

enum
{
    MLE_PORT_NUMBER = 19788,
};

struct mle_control mle_cb[CONFIG_MAX_NUM_MLE_INTERFACE] = { { 0 } };

static void mle_listener(UINT8 in_inf,
                         const IP6_ADDRESS *src_addr,
                         UINT16 src_port,
                         UINT16 dst_port,
                         const UINT8 *msg, UINT16 len)
{
    UINT8 i;
    UINT8 *plain_msg;
    UINT16 plain_msg_len;

    if (len == 0)
    {
        // Invalid length
        printf("%s()-invalid len\n", __FUNCTION__);
        return;
    }
    
    if (msg[0] == 0x00)
    {
        //TODO Unsecure if the message is secured at MLE layer.
        return;
    }
    else if (msg[0] == 0xff)
    {
        plain_msg = (UINT8 *) &msg[1];
        plain_msg_len = len - 1;
    }
    else
    {
        // Invalid security suite indicator.
        printf("%s()-invalid sec 0x%x\n", __FUNCTION__, msg[0]);
        return;
    }

    for (i = 0; i < CONFIG_MAX_NUM_MLE_INTERFACE; i++)
    {
        if (mle_cb[i].notify && mle_cb[i].inf == in_inf)
        {
            break;
        }
    }

    if (i >= CONFIG_MAX_NUM_MLE_INTERFACE)
    {
        //No MLE interface.
        printf("%s()-no interface\n", __FUNCTION__);
        return;
    }

    mle_cb[i].notify(in_inf, src_addr, plain_msg[0], &plain_msg[1], plain_msg_len - 1);
}

void mle_start(UINT8 inf, MLE_RECV_HANDLER handler)
{
    UINT8 i;
    BOOL started = FALSE;

    for (i = 0; i < CONFIG_MAX_NUM_MLE_INTERFACE; i++)
    {
        if (mle_cb[i].notify)
        {
            started = TRUE;
            
            if (mle_cb[i].inf == inf)
            {
                mle_cb[i].notify = handler;
                return;
            }
        }
    }

    for (i = 0; i < CONFIG_MAX_NUM_MLE_INTERFACE; i++)
    {
        if (!mle_cb[i].notify)
        {
            mle_cb[i].notify = handler;
            break;
        }
    }

    if (!started)
    {
        // Start a UDP server.
        nos_udp_set_listen(MLE_PORT_NUMBER, mle_listener);
    }

    printf("%s()-START!\n", __FUNCTION__);
}

ERROR_T mle_send(UINT8 inf,
                 const IP6_ADDRESS *dst,
                 const void *aux_header,
                 UINT8 aux_header_len,
                 enum mle_command cmd,
                 const void *tlvs,
                 UINT8 tlvs_len)
{
    UINT16 total_len;
    UINT8 *message;
    
    if (aux_header)
    {
        //TODO Not supported.
        return ERROR_NOT_SUPPORTED;
    }

    total_len = (1 + 1 + tlvs_len);
    message = nos_malloc(total_len);

    if (!message)
    {
        return ERROR_NOT_ENOUGH_MEMORY;
    }

    message[0] = (aux_header) ? 0 : 255;
    message[1] = cmd;
    memcpy(&message[2], tlvs, tlvs_len);
    
    nos_udp_sendto(inf, MLE_PORT_NUMBER, dst, MLE_PORT_NUMBER, message, total_len);
    
    printf("%s()-Send command\n", __FUNCTION__);
    nos_free(message);
    return ERROR_FAIL;
}

const void *mle_extract_tlv(enum mle_tlv_type tlv_type, const UINT8 *tlvs, UINT16 tlvs_len)
{
    while (tlvs_len > 0)
    {
        if (tlvs[0] == tlv_type)
        {
            return &tlvs[0];
        }
        else
        {
            UINT16 opt_len;
            opt_len = 2 + tlvs[1];
            if (tlvs_len >= opt_len)
            {
                tlvs_len -= opt_len;
                tlvs += opt_len;
            }
            else
            {
                return NULL;
            }
        }
    }

    return NULL;
}

#endif //MLE_M
