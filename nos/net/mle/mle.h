// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * MLE (Mesh Link Establishment) Server
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 8. 14.
 */

#ifndef MLE_H
#define MLE_H

#include "kconf.h"
#ifdef MLE_M
#include "nos_common.h"
#include "errorcodes.h"
#include "ip6-types.h"

enum mle_command
{
    MLE_CMD_LINK_REQUEST                 = 0x00,
    MLE_CMD_LINK_ACCEPT                  = 0x01,
    MLE_CMD_LINK_ACCEPT_AND_REQUEST      = 0x02,
    MLE_CMD_LINK_REJECT                  = 0x03,
    MLE_CMD_ADVERTISEMENT                = 0x04,
    MLE_CMD_UPDATE                       = 0x05,
    MLE_CMD_UPDATE_REQUEST               = 0x06,
};

typedef void (*MLE_RECV_HANDLER)(UINT8 inf,
                                 const IP6_ADDRESS *src,
                                 enum mle_command cmd,
                                 const void *tlvs,
                                 UINT8 tlvs_len);

void mle_start(UINT8 inf, MLE_RECV_HANDLER handler);

enum mle_tlv_type
{
    MLE_TLV_SOURCE_ADDRESS = 0,
    MLE_TLV_MODE = 1,
    MLE_TLV_TIMEOUT = 2,
    MLE_TLV_CHALLENGE = 3,
    MLE_TLV_RESPONSE = 4,
    MLE_TLV_LINK_LAYER_FRAME_COUNTER = 5,
    MLE_TLV_LINK_QUALITY = 6,
    MLE_TLV_PARAMETER = 7,
};

ERROR_T mle_send(UINT8 inf,
                 const IP6_ADDRESS *dst,
                 const void *aux_header,
                 UINT8 aux_header_len,
                 enum mle_command cmd,
                 const void *tlvs,
                 UINT8 tlvs_len);

const void *mle_extract_tlv(enum mle_tlv_type tlv_type, const UINT8 *tlvs, UINT16 tlvs_len);

#endif //MLE_M
#endif //MLE_H
