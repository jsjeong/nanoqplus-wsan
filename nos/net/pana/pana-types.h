// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * PANA (Protocol for Carrying Authentication for Network Access) Message Types
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 8. 20.
 */

#ifndef PANA_TYPES_H
#define PANA_TYPES_H

#include "kconf.h"
#ifdef PANA_M
#include "nos_common.h"
#include "ip6-types.h"

#pragma pack(1)
struct pana_header
{
    UINT16 reserved;
    UINT16 message_len;
#ifdef BIT_ORDER_BIG_ENDIAN
    UINT8 r_bit:1;         //Requst
    UINT8 s_bit:1;         //Start
    UINT8 c_bit:1;         //Complete
    UINT8 a_bit:1;         //Reauthentication
    UINT8 p_bit:1;         //Ping
    UINT8 i_bit:1;         //IP reconfiguration
    UINT8 res1:2;
#elif BIT_ORDER_LITTLE_ENDIAN
    UINT8 res1:2;
    UINT8 i_bit:1;         //IP reconfiguration
    UINT8 p_bit:1;         //Ping
    UINT8 a_bit:1;         //Reauthentication
    UINT8 c_bit:1;         //Complete
    UINT8 s_bit:1;         //Start
    UINT8 r_bit:1;         //Request
#endif
    UINT8 res2;
    UINT16 message_type;
    UINT32 session_id;
    UINT32 seq;
};
#pragma pack()

enum pana_msg_type
{
    PANA_MSG_PCI = 1,
    PANA_MSG_PAR_PAN = 2,
    PANA_MSG_PTR_PTA = 3,
    PANA_MSG_PNR_PNA = 4,
};

#pragma pack(1)
struct pana_avp
{
    UINT16 avp_code;
#ifdef BIT_ORDER_BIG_ENDIAN
    UINT8 v_bit:1;
    UINT8 res1:7;
#elif BIT_ORDER_LITTLE_ENDIAN
    UINT8 res1:7;
    UINT8 v_bit:1;
#endif
    UINT8 res2;
    UINT16 avp_len;
    UINT16 res3;
    // (optional) vendor ID (4 octets)
};
#pragma pack()

enum pana_avp_code
{
    PANA_AUTH_AVP = 1,
    PANA_EAP_PAYLOAD_AVP = 2,
    PANA_INTEGRITY_ALGORITHM_AVP = 3,
    PANA_KEY_ID_AVP = 4,
    PANA_NONCE_AVP = 5,
    PANA_PRF_ALGORITHM_AVP = 6,
    PANA_RESULT_CODE_AVP = 7,
    PANA_SESSION_LIFETIME_AVP = 8,
    PANA_TERMINATION_CAUSE_AVP = 9,
};

#endif //PANA_M
#endif //PANA_TYPES_H
