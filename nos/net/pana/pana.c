// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * PANA (Protocol for Carrying Authentication for Network Access) Implementation
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 8. 20.
 */

#include "pana.h"
#ifdef PANA_M
#include "noslib.h"
#include "heap.h"
#include "udp.h"
#include <stdlib.h>

#ifdef EAP_M
#include "eap.h"
#endif

enum
{
    PANA_PAA_PORT = 716,
};

enum pana_message
{
    PANA_PCI, PANA_PAR, PANA_PAN, PANA_PTR, PANA_PTN, PANA_PNR, PANA_PNA,
};

enum pana_state
{
    PANA_STATE_PAC,
    PANA_STATE_PRE,
};

struct pana_control
{
    UINT8 inf;
    enum pana_state state;
    UINT16 listening_port;
    IP6_ADDRESS parent; //can be either PAA or PRE.
    UINT32 seq;
    PANA_EVENT_HANDLER notify;
};

struct pana_control pana_cb[CONFIG_MAX_NUM_PANA_INTERFACE] = { { 0 } };

static void pana_send(UINT8 pana_inf,
                      const IP6_ADDRESS *dst_addr,
                      UINT16 dst_port,
                      enum pana_message msg,
                      UINT32 seq,
                      const void *avps,
                      UINT16 avps_len)
{
    ERROR_T err;
    struct pana_header *header;

    header = nos_malloc(sizeof(struct pana_header) + avps_len);
    if (!header)
        return;

    memset(header, 0, sizeof(struct pana_header) + avps_len);
    header->message_len = htons(sizeof(struct pana_header) + avps_len);
    printf("%s()-len:%u\n", __FUNCTION__, sizeof(struct pana_header) + avps_len);
    header->res2 = 0;
    
    if (msg == PANA_PCI)
    {
        header->message_type = htons(PANA_MSG_PCI);
    }
    else if (msg == PANA_PAR)
    {
        header->message_type = htons(PANA_MSG_PAR_PAN);
        header->r_bit = 1;
        //TODO header->(s_bit or c_bit) = 1;
        //TODO header->i_bit = 1;
    }
    else if (msg == PANA_PAN)
    {
        header->message_type = htons(PANA_MSG_PAR_PAN);
        //TODO header->(s_bit or c_bit) = 1;
    }
    else if (msg == PANA_PTR)
    {
        header->message_type = htons(PANA_MSG_PTR_PTA);
        header->r_bit = 1;
    }
    else if (msg == PANA_PTN)
    {
        header->message_type = htons(PANA_MSG_PTR_PTA);
    }
    else if (msg == PANA_PNR)
    {
        header->message_type = htons(PANA_MSG_PNR_PNA);
        header->r_bit = 1;
        //TODO header->(a_bit or p_bit) = 1;
    }
    else if (msg == PANA_PNA)
    {
        header->message_type = htons(PANA_MSG_PNR_PNA);
        //TODO header->(a_bit or p_bit) = 1;
    }
    else
    {
        nos_free(header);
        return;
    }
    header->seq = 0;
    memcpy((void *) (header + 1), avps, avps_len);

    err = nos_udp_sendto(pana_cb[pana_inf].inf,
                         pana_cb[pana_inf].listening_port,
                         dst_addr,
                         dst_port,
                         header,
                         sizeof(struct pana_header) + avps_len);

    printf("%s()-result:%d\n", __FUNCTION__, err);
}

static void pana_listener(UINT8 in_inf,
                          const IP6_ADDRESS *src_addr,
                          UINT16 src_port,
                          UINT16 dst_port,
                          const UINT8 *msg, UINT16 len)
{
}

void pana_client_init(UINT8 inf, const IP6_ADDRESS *parent, PANA_EVENT_HANDLER handler)
{
    UINT8 i, empty = CONFIG_MAX_NUM_PANA_INTERFACE;

    for (i = 0; i < CONFIG_MAX_NUM_PANA_INTERFACE; i++)
    {
        if (pana_cb[i].notify && pana_cb[i].inf == inf)
            return;
        else if (!pana_cb[i].notify)
            empty = i;
    }

    if (empty >= CONFIG_MAX_NUM_PANA_INTERFACE)
        return;

    pana_cb[i].inf = inf;
    pana_cb[i].listening_port = nos_udp_get_unused_random_port();
    pana_cb[i].notify = handler;
    pana_cb[i].parent = *parent;
    pana_cb[i].seq = rand();
    pana_cb[i].state = PANA_STATE_PAC;
    
    nos_udp_set_listen(pana_cb[i].listening_port, pana_listener);
    pana_send(i, parent, PANA_PAA_PORT, PANA_PCI, 0, NULL, 0);

    printf("%s()\n", __FUNCTION__);
}

#endif //PANA_M
