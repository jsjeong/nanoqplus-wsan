// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Generic IEEE 802.15.4 MAC
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 22.
 */

#include "kconf.h"

#ifdef IEEE_802_15_4_M
#include "mac.h"
#include "platform.h"
#include "wpan-dev.h"

#ifdef NANO_MAC_M
#include "nano-mac.h"
#endif

NOS_MAC nos_mac[WPAN_DEV_CNT];
extern WPAN_DEV wpan_dev[WPAN_DEV_CNT];

ERROR_T nos_mac_init(UINT8 dev_id, UINT16 pan_id, UINT16 short_id,
                     const UINT8 *eui64, enum nos_mac_type mac_type,
                     void (*rx_event)(UINT8))
{
    ERROR_T err = ERROR_NOT_SUPPORTED;
    
    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;

    nos_mac[dev_id].notify = rx_event;

#ifdef NANO_MAC_M
    if (mac_type == MAC_TYPE_NANO_MAC)
    {
        nmac_init(dev_id, pan_id, short_id, eui64);
        err = ERROR_SUCCESS;
    }
#endif

#ifdef LPLL_MAC_M
    if (mac_type == MAC_TYPE_LPLL_MAC)
    {
        lpll_mac_init(dev_id, pan_id, short_id, eui64);
        err = ERROR_SUCCESS;
    }
#endif

#ifdef GLOSSY_M
    if (mac_type == MAC_TYPE_GLOSSY)
    {
        glossy_init(dev_id, pan_id, short_id, eui64);
        err = ERROR_SUCCESS;
    }
#endif

    return err;
}

ERROR_T nos_mac_tx(UINT8 dev_id, NOS_MAC_TX_INFO *frame)
{
    struct nos_mac_tx_args a;
    
    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;

    a.frame = frame;
    return nos_mac[dev_id].command(dev_id, NOS_MAC_CMD_TX, &a);
}

ERROR_T nos_mac_rx(UINT8 dev_id, NOS_MAC_RX_INFO *frame)
{
    struct nos_mac_rx_args a;
    
    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;

    a.frame = frame;
    return nos_mac[dev_id].command(dev_id, NOS_MAC_CMD_RX, &a);
}

UINT8 nos_mac_get_max_tx_payload_len(UINT8 dev_id, BOOL dst_eui64, BOOL src_eui64, UINT8 sec)
{
    return ieee_802_15_4_get_max_tx_payload_len(dst_eui64, src_eui64, sec);
}

ERROR_T nos_mac_set_beacon_handler(UINT8 dev_id,
                                   void (*rx_beacon_handler)(UINT8 dev_id,
                                                             const struct wpan_rx_frame *beacon))
{
    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;

    nos_mac[dev_id].notify_beacon_reception = rx_beacon_handler;
    return ERROR_SUCCESS;
}

#ifdef IEEE_802_15_4_FFD
ERROR_T nos_mac_set_mac_command_handler(UINT8 dev_id,
                                        void (*rx_command_handler)(UINT8 dev_id,
                                                                   const struct wpan_rx_frame *command))
{
    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;

    nos_mac[dev_id].notify_mac_command_reception = rx_command_handler;
    return ERROR_SUCCESS;
}

#endif

#endif //IEEE_802_15_4_M
