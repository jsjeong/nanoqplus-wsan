/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Duty-Cycling based Low-Power Low-Latency MAC
 * @author Jeehoon Lee (UST-ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 9. 5.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef LPLL_MAC_H
#define LPLL_MAC_H

#include "kconf.h"
#ifdef LPLL_MAC_M

#include "nos_common.h"
#include "ieee-802-15-4.h"

#define LPLL_CURRENT_TIME()   (TBR)

void lpll_mac_init(UINT16 panid, UINT16 sid, const UINT8 *lid);
void lpll_mac_set_rx_cb(void (*func)(void));
BOOL lpll_mac_rx(NOS_MAC_RX_INFO* rx_info);
static BOOL lpll_mac_rx_event_handler(void);
static UINT8 lpll_mac_read_frame(void);
static void lpll_mac_exe_missing_cb(void);
BOOL lpll_mac_tx(NOS_MAC_TX_INFO* tx_info, BOOL ack_required);
void lpll_mac_rf_on(void);
void lpll_mac_rf_off(void);
void lpll_mac_timer_init(void);
void lpll_mac_wakeup(void);
void lpll_mac_listen(void);
void lpll_mac_short_sleep_mode(void);



/**
 * @brief Adjust the modem's Tx power to @p p value.
 */
void lpll_mac_set_tx_power(UINT8 p);
UINT8 lpll_mac_get_opmode(void);
UINT16 lpll_mac_get_pan_id(void);
UINT16 lpll_mac_get_short_id(void);
UINT8 *lpll_mac_get_eui64(UINT8 *eui64);
BOOL lpll_mac_eui64_is_set(void);
UINT8 lpll_mac_get_max_tx_payload_len(BOOL da_eui64, UINT8 sec);

#endif /* LPLL_MAC_M */
#endif /* LPLL_MAC_H */
