// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Duty-Cycling based Low-Power Low-Latency MAC
 * @author Jeehoon Lee (UST-ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 9. 5.
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "lpll_mac.h"

#ifdef LPLL_MAC_M
#include <stdlib.h>
#include <string.h>
#include "errorcodes.h"
#include "platform.h"
#include "arch.h"
#include "critical_section.h"
#include "wpan-dev.h"
#include "pwr_mgr.h"

//#define LPLL_MAC_DEBUG
#ifdef LPLL_MAC_DEBUG
#include "uart.h"
#include "platform.h"
#endif /* LPLL_MAC_DEBUG */

#define LPLL_ID     0x12
#define DYNAMIC_SLEEP_TIME  //Modify the period of sleep

#define CCA_NUMBER     2   // The number of CCA trial for checking signal

#define LPLL_MAC_CCA_DELAY                  1186    // 1159(us)

/* tick count */
// 1 tick = 30.517578125 us
// CC2420 wait for ACK = 864us (54 symbol)
#define LPLL_MAC_SLEEP_TIME                 4096    // 125 (ms)
#define LPLL_MAC_SHORT_SLEEP_TIME           1024    // 31.25 (ms)
#define LPLL_MAC_SHORT_SLEEP_DURATION       32768   // 1 (sec)
#define LPLL_MAC_TX_INTERVAL                37      // 1141 (us) 
#define LPLL_MAC_LONGEST_PACKET_TX_TIME     138     // 4211 (us)
#define LPLL_MAC_CCA_TIME                   6       // 183.10546875 (us)
#define LPLL_MAC_CCA_INTERVAL  (LPLL_MAC_TX_INTERVAL + 1)
#define LPLL_MAC_CHECK_TIME    ((CCA_NUMBER * LPLL_MAC_CCA_TIME) + ((CCA_NUMBER - 1) * LPLL_MAC_CCA_INTERVAL))
#define LPLL_MAC_LISTEN_TIME   ((2 * LPLL_MAC_LONGEST_PACKET_TX_TIME) + LPLL_MAC_TX_INTERVAL)
#define LPLL_MAC_STROBE_TIME   ((2 * LPLL_MAC_CHECK_TIME) + LPLL_MAC_SLEEP_TIME)

// frame type.
enum
{
    DATA_FRAME = 1,
    ACK_FRAME = 2,
    ERROR_FRAME = 3,
};

// Rx queue
enum { MAC_RXQ_LEN = 4 };
enum { MAC_MAX_TX_TRIALS = 3 }; // Tx trials when Ack is not received.

struct _mac_rx_queue
{
    UINT8 front;
    UINT8 rear;
    UINT8 nitem;
    NOS_MAC_RXQ_ENTITY data[MAC_RXQ_LEN];
};

// Simple MAC environments
static struct _mac_env
{
    UINT8 tx_seq; // Tx : Packet sequence number.
    volatile UINT8 last_rx_seq; // Rx : Packet sequence number. (distinguish duplicated packet)
    volatile UINT16 last_src_address;
    volatile UINT8 last_src_address_eui64[8];
    UINT8 rx_missing_cb; // the number of missing RX callback 
    BOOL rx_on_state; // store rf recv state that user specified.
    BOOL tx_on_state; // store rf recv state that user specified.  
    UINT32 sec_frame_counter;
    BOOL short_sleep_state; // short sleep state
    UINT16 short_sleep_duration;   // the duration to maintain short sleep time
} MAC_ENV;

static UINT8 csma_retries = IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BACKOFFS;
static UINT8 csma_min_be = IEEE_802_15_4_2006_DEFAULT_CSMA_MIN_BE;
static UINT8 csma_max_be = IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BE;

volatile static struct _mac_rx_queue lpll_rxq;
#define MAC_IS_RXQ_FULL()    (lpll_rxq.nitem == MAC_RXQ_LEN)
#define MAC_IS_RXQ_EMPTY()   (lpll_rxq.nitem == 0)
#define MAC_RXQ_NITEM()      (lpll_rxq.nitem)

volatile static UINT8 lpll_mac_rx_missing_cb;
static void (*lpll_mac_rx_callback)(void);

void lpll_mac_init(UINT16 panid, UINT16 sid, const UINT8 *lid)
{
    // TODO Initialize the LPLL MAC.
    NOS_ENTER_CRITICAL_SECTION();

    // Set RX queue
    lpll_rxq.front = 0;
    lpll_rxq.rear = 0;
    lpll_rxq.nitem = 0;

    // Set environments
    srand(panid + sid);

    // Set RF interrupt callback function
    lpll_mac_set_rx_cb(NULL);

    nos_rf_modem_config(panid, sid, lid,
            lpll_mac_rx_event_handler);

    // starts from random sequece number to avoid same ack sequence number
    MAC_ENV.tx_seq = rand() % 256;
    MAC_ENV.last_rx_seq = MAC_ENV.tx_seq;
    MAC_ENV.rx_missing_cb = 0;
    MAC_ENV.rx_on_state = FALSE;
    MAC_ENV.tx_on_state = FALSE;
    MAC_ENV.last_src_address = 0xffff;
    memset((void *) MAC_ENV.last_src_address_eui64, 0xff, 8);
    MAC_ENV.sec_frame_counter = 0;
    MAC_ENV.short_sleep_state = FALSE;
    MAC_ENV.short_sleep_duration = 0;


    //CC2420_SETREG(CC2420_RSSI, 0xfa00); //Set CCA threshold to -6 (-51dBm)

    lpll_mac_timer_init();

    lpll_mac_rf_off();

    NOS_EXIT_CRITICAL_SECTION();
}

void lpll_mac_timer_init(void)
{
    // Initialize TimerB
    TBCTL = TBSSEL0 | MC1;	// ACLK(32kHz), conitnuous mode.  default : 0x0000

    /* Wake-up period for low-power MAC radio duty cycle */
    TBCCR1 = LPLL_MAC_SLEEP_TIME;
    TBCCTL1 &= ~CCIFG;  // Clear Timer B CCR1 vect
    TBCCTL1 |= CCIE;    // Enable Timer B CCR1 vect

    /* TBCCR2: TX strobe timer */
    //TBCCTL2 &= ~CCIFG;  // Clear Timer B CCR2 vect
    //TBCCTL2 |= CCIE;    // Enable Timer B CCR2 vect

    /* TBCCR3: RX listen timer */
    //TBCCTL3 &= ~CCIFG;  // Clear Timer B CCR3 vect
    //TBCCTL3 |= CCIE;    // Enable Timer B CCR3 vect

    /* TBCCR4: short sleep duration */
    TBCCTL4 &= ~CCIFG;  // Clear Timer B CCR4 vect
    TBCCTL4 |= CCIE;    // Enable Timer B CCR4 vect

    /* Short sleep wake-up period for low-power MAC radio dudy cycle */
    //TBCCR5 = LPLL_MAC_SHORT_SLEEP_TIME;
    //TBCCTL5 &= ~CCIFG;  // Clear Timer B CCR1 vect
    //TBCCTL5 |= CCIE;    // Enable Timer B CCR1 vect
}

void lpll_mac_listen(void)
{
#ifdef LPLL_MAC_DEBUG
    nos_uart_putc(STDIO, ' ');
    nos_uart_putc(STDIO, 'L');
#endif //LPLL_MAC_DEBUG
#ifdef CC2420_M
    CLEAR_FIFOP_INTR();
    ENABLE_FIFOP_INTR();

    CC2420_AUTOACK_REP_ON(); // Turn on automatic packet acknowledgment

    TBCCR3 = LPLL_CURRENT_TIME() + LPLL_MAC_LISTEN_TIME;
    TBCCTL3 &= ~CCIFG;   // Clear Interrupt flag.
    TBCCTL3 |= CCIE;     // Enable TBCCR2 interrupt.
    while((TBCCTL3 & CCIFG) == 0)
    {
        if(IS_SET_FIFOP_INTRFLAG())
        {
            TBCCTL3 &= ~CCIE;    // Disable Interrupt.    
            TBCCTL3 &= ~CCIFG;   // Clear Interrupt flag.
#ifdef DYNAMIC_SLEEP_TIME
            if (MAC_ENV.short_sleep_state == FALSE)
            {
                lpll_mac_short_sleep_mode();
            }
#endif //DYNAMIC_SLEEP_TIME
            return;
        }
    }
    TBCCTL3 &= ~CCIE;    // Disable Interrupt.    
    TBCCTL3 &= ~CCIFG;   // Clear Interrupt flag.
    
    TBCCTL1 &= ~CCIFG;  // Clear Timer B CCR1 vect

    lpll_mac_rf_off();
    lpll_mac_flush_rx_buf();

    DISABLE_FIFOP_INTR();
#endif /* CC2420_M */
}

void lpll_mac_wakeup(void)
{
#ifdef LPLL_MAC_DEBUG
    nos_uart_putc(STDIO, ' ');
    nos_uart_putc(STDIO, 'W');
#endif //LPLL_MAC_DEBUG
    for(UINT8 cca_cnt = 0; cca_cnt < CCA_NUMBER; cca_cnt++)
    {
        lpll_mac_rf_on();   // RF on
  
        if (!nos_rf_modem_cca())   // Detect signal
        {
            lpll_mac_listen();     // Ready to receive packets
            return;
        }
        
        lpll_mac_rf_off();  // RF off 
        
        if (cca_cnt < (CCA_NUMBER-1))
        {
            nos_delay_us(LPLL_MAC_CCA_DELAY);
        }
    }
}

void lpll_mac_short_sleep_mode(void)
{
    MAC_ENV.short_sleep_state = TRUE;
    
    // Initialize TimerB
    //TBCTL = TBSSEL0 | MC1;  // ACLK(32kHz), conitnuous mode.  default : 0x0000

    //NOS_ENTER_CRITICAL_SECTION();
    
    //TBCTL |= TBCLR;     //Reset TimerB

    //TBCTL = TBSSEL0 | MC1;  // Start Timer

    /* Disable normal sleep period timer */  
    //TBCCTL1 &= ~CCIE;    // Disable Timer B CCR1 vect
    //TBCCTL1 &= ~CCIFG;  // Clear Timer B CCR1 vect

    /* Enable short sleep duration timer */
    TBCCR4 = LPLL_CURRENT_TIME() + LPLL_MAC_SHORT_SLEEP_DURATION;
    TBCCTL4 &= ~CCIFG;  // Clear TImer B CCR4 vect
    TBCCTL4 |= CCIE;    // Enable Timer B CCR4 vect

    TBCCR1 = LPLL_CURRENT_TIME() + LPLL_MAC_SHORT_SLEEP_TIME;

    /* Enable short sleep period timer */
    //TBCCR5 = LPLL_MAC_SHORT_SLEEP_TIME;
    //TBCCTL5 &= ~CCIFG;  // Clear Timer B CCR5 vect
    //TBCCTL5 |= CCIE;    // Enable Timer B CCR5 vect

    //NOS_ENTER_CRITICAL_SECTION();
}

void lpll_mac_set_rx_cb(void (*func)(void))
{
    NOS_ENTER_CRITICAL_SECTION();
    lpll_rxq.front = 0;
    lpll_rxq.rear = 0;
    lpll_rxq.nitem = 0;
    lpll_mac_rx_missing_cb = 0;
    lpll_mac_rx_callback = func;
    NOS_EXIT_CRITICAL_SECTION();
}

BOOL lpll_mac_rx(NOS_MAC_RX_INFO* rx_info)
{
#ifdef LPLL_MAC_DEBUG
        nos_uart_putc(STDIO, 'R');
        nos_uart_putc(STDIO, '\n');
#endif //LPLL_MAC_DEBUG

    UINT8 idx;
    UINT16 fcf;
    BOOL ret;


#ifdef CC2420_M
//    ENABLE_FIFOP_INTR();
#endif /* CC2420_M */

    cont:
    if ( !MAC_IS_RXQ_EMPTY() )
    {
        // remove an item from RX queue.
        fcf = (UINT16) lpll_rxq.data[lpll_rxq.front].header[0] +
                ((UINT16) lpll_rxq.data[lpll_rxq.front].header[1] << 8);
        idx = FRAME_IDX_SRCADDR(fcf);

        if (_IS_SET(
                lpll_rxq.data[lpll_rxq.front].frame_status,
                FRAME_SRC_ADDR_IS_EUI64))
        {
            rx_info->use_eui64 = TRUE;
            rx_info->src_addr_eui64[0] = lpll_rxq.data[lpll_rxq.front].header[idx+7];
            rx_info->src_addr_eui64[1] = lpll_rxq.data[lpll_rxq.front].header[idx+6];
            rx_info->src_addr_eui64[2] = lpll_rxq.data[lpll_rxq.front].header[idx+5];
            rx_info->src_addr_eui64[3] = lpll_rxq.data[lpll_rxq.front].header[idx+4];
            rx_info->src_addr_eui64[4] = lpll_rxq.data[lpll_rxq.front].header[idx+3];
            rx_info->src_addr_eui64[5] = lpll_rxq.data[lpll_rxq.front].header[idx+2];
            rx_info->src_addr_eui64[6] = lpll_rxq.data[lpll_rxq.front].header[idx+1];
            rx_info->src_addr_eui64[7] = lpll_rxq.data[lpll_rxq.front].header[idx];
        }
        else
        {
            rx_info->use_eui64 = FALSE;
            rx_info->src_addr =
                    (lpll_rxq.data[lpll_rxq.front].header[idx]) +
                    (lpll_rxq.data[lpll_rxq.front].header[idx+1] << 8);
        }
        rx_info->rssi = lpll_rxq.data[lpll_rxq.front].rssi;
        rx_info->corr = lpll_rxq.data[lpll_rxq.front].corr;
        idx = lpll_rxq.data[lpll_rxq.front].header_length;

        if (lpll_rxq.data[lpll_rxq.front].header[idx-1] == LPLL_ID)    // Check LPLL MAC ID
        {
            //Subtract the size of padding from the payload length.
            rx_info->payload_length =
                    lpll_rxq.data[lpll_rxq.front].payload_length -
                    lpll_rxq.data[lpll_rxq.front].header[idx-2];
        }

        if (FRAME_CONTAINS_SECHDR(fcf))
        {
            lpll_rxq.front = (lpll_rxq.front+1)%MAC_RXQ_LEN;
            --lpll_rxq.nitem;
            goto cont;
        }

        memcpy((void *) rx_info->payload,
                (void*)(lpll_rxq.data[lpll_rxq.front].payload),
                rx_info->payload_length);

        lpll_rxq.front = (lpll_rxq.front+1)%MAC_RXQ_LEN;
        --lpll_rxq.nitem;

        ret = TRUE;

    }
    else
    {
        ret = FALSE;
    }
    lpll_mac_rf_off();
    //nos_led_toggle(2);
    return ret;
}

static BOOL lpll_mac_rx_event_handler(void)
{
#ifdef LPLL_MAC_DEBUG
    nos_uart_putc(STDIO, 'r');
    nos_uart_putc(STDIO, '\r');
    nos_uart_putc(STDIO, '\n');
#endif /* LPLL_MAC_DEBUG */

    UINT8 frame_type;
    // handle modem RX buf overflow (full)
    // turn off RX(auto-ack)

    // ISR may handle several frames or no frame (modem RX buf is empty)
    
    while (!nos_rf_modem_rxbuf_is_empty())
    {
        if (MAC_IS_RXQ_FULL())
        {
            if (MAC_ENV.rx_missing_cb)
            {
#ifdef LPLL_MAC_DEBUG
                nos_uart_putc(STDIO, '+');
#endif /* LPLL_MAC_DEBUG */
                lpll_mac_exe_missing_cb();
                continue;
            }
            else
            {
#ifdef LPLL_MAC_DEBUG
                nos_uart_putc(STDIO, '-');
#endif /* LPLL_MAC_DEBUG */                
                // 1. Modem RX turn off
                // 2. Auto-ack off
                // 3. Disable Modem RX interrupt
                // 4. Flush modem RX buf (frames in modem buf may already sent Ack frames.)
                //HAL_MacCtrlSet(gu8HalState_MacCtrlReg & (~MG2410_MacCtrl_AutoAck));		//disable auto-ack
                return TRUE; // Signal to stop RX interrupt.
            }
        }
        else
        {
            // read only one frame without checking NMAC Queue status
            frame_type = lpll_mac_read_frame();
            if (frame_type == DATA_FRAME)// read only one frame without checking NMAC Queue status
            {
                //		nos_uart_putc(STDIO, ')');
                ++MAC_ENV.rx_missing_cb; // or enquene lpll_mac_rx_callback in task queue
            }
            else if (frame_type == ERROR_FRAME)
            {
                //		nos_uart_putc(STDIO, '(');
                //HAL_RxFifoReset();	// flush modem RX buf
                return FALSE; // Signal to continue receive.
            }
        }
    }

    // Modem RX buf is empty now.
    lpll_mac_exe_missing_cb();
    return FALSE; // Signal to continue receive.
}

/**
 * @brief Read a frame out from a radio transceiver.
 *
 * @warn This function must be called in the critical section.
 */
static UINT8 lpll_mac_read_frame(void)
{
    struct _nmac_rx_frame tmp_frame;
    UINT8 srcaddr_idx;

    tmp_frame.rx_qinfo =
            (NOS_MAC_RXQ_ENTITY *) &lpll_rxq.data[lpll_rxq.rear];

    if (nos_rf_modem_read_frame((struct _nmac_rx_frame *) &tmp_frame))
    {
        // Check whether it's an ACK for me or not.
        if (tmp_frame.frame_length == IEEE_802_15_4_ACK_SIZE &&
                FRAME_IS_ACK(tmp_frame.frame_ctrl_field))
        {

            if (tmp_frame.rx_seq == MAC_ENV.tx_seq)
            {
                return ACK_FRAME; // Indicate the successful Ack reception
            }
            else
            {
                return FALSE; // Ack not for me
            }
        }

        // Invalid packet or unknown FCF
        if (tmp_frame.frame_length < IEEE_802_15_4_MIN_FRAME_OVERHEAD_SIZE ||
                (!FRAME_IS_DATA(tmp_frame.frame_ctrl_field) &&
                        !FRAME_IS_ACK(tmp_frame.frame_ctrl_field)))
        {
#ifdef LPLL_MAC_DEBUG
            nos_uart_putc(STDIO, 'd');
            nos_uart_putc(STDIO, '\n');
#endif /* LPLL_MAC_DEBUG */            
            return ERROR_FRAME;
        }

        srcaddr_idx = FRAME_IDX_SRCADDR(tmp_frame.frame_ctrl_field);

        // Check duplication.
        if(MAC_ENV.last_rx_seq == tmp_frame.rx_seq)
        {
            if (_IS_SET(tmp_frame.rx_qinfo->frame_status,
                    FRAME_SRC_ADDR_IS_EUI64))
            {
                if (memcmp((void *) &tmp_frame.rx_qinfo->header[srcaddr_idx],
                        (void *) MAC_ENV.last_src_address_eui64, 8) == 0)
                    return FALSE;
            }
            else
            {
                if (((tmp_frame.rx_qinfo->header[srcaddr_idx + 1] << 8) +
                        (tmp_frame.rx_qinfo->header[srcaddr_idx]))
                        == MAC_ENV.last_src_address)
                    return FALSE;
            }
        }

#ifdef IEEE_802_15_4_RXADDR_LIMIT_M
        {
            UINT16 src_16;

            src_16 = (tmp_frame.rx_qinfo->header[srcaddr_idx + 1] << 8)
                    | (tmp_frame.rx_qinfo->header[srcaddr_idx]);

#if CONFIG_IEEE_802_15_4_MIN_RXADDR > 0
            if (src_16 < CONFIG_IEEE_802_15_4_MIN_RXADDR) return FALSE;
#endif

#if CONFIG_IEEE_802_15_4_MAX_RXADDR < 65535
            if (src_16 > CONFIG_IEEE_802_15_4_MAX_RXADDR) return FALSE;
#endif
        }
#endif /* IEEE_802_15_4_RXADDR_LIMIT_M */

        // Remember last received data packet sequence number.
        MAC_ENV.last_rx_seq = tmp_frame.rx_seq;
        if (_IS_SET(tmp_frame.rx_qinfo->frame_status, FRAME_SRC_ADDR_IS_EUI64))
            memcpy((void *) MAC_ENV.last_src_address_eui64,
                    (void *) &tmp_frame.rx_qinfo->header[srcaddr_idx], 8);
        else
            MAC_ENV.last_src_address =
                    (tmp_frame.rx_qinfo->header[srcaddr_idx + 1] << 8) +
                    (tmp_frame.rx_qinfo->header[srcaddr_idx]);

        //=========== Insert a frame to the RX queue=================//
        // The rx_queue cannot be full. (already have been checked.)
        ++lpll_rxq.nitem;
        lpll_rxq.rear = (lpll_rxq.rear + 1) % MAC_RXQ_LEN;
        //================================================//
        return DATA_FRAME;
    }
    else
    {
        return ERROR_FRAME;
    }
}

static void lpll_mac_exe_missing_cb(void)
{
    if (lpll_mac_rx_callback)
    {
        while (MAC_ENV.rx_missing_cb)
        {
            --MAC_ENV.rx_missing_cb;
            lpll_mac_rx_callback();
        }
    }
    else
    {
        MAC_ENV.rx_missing_cb = 0;
    }
}

static void lpll_mac_flush_rx_buf(void)
{
    UINT8 frame_type;

    // may handle several frames or no frame (modem RX buf is empty)
    while (!nos_rf_modem_rxbuf_is_empty())
    {
        if (MAC_IS_RXQ_FULL())
        {
            nos_rf_modem_flush_rxbuf();
            return;
        }
        else
        {
            // read only one frame without checking NMAC Queue status
            frame_type = lpll_mac_read_frame();
            if (frame_type == DATA_FRAME)// read only one frame without checking NMAC Queue status
            {
                ++MAC_ENV.rx_missing_cb; // or enquene lpll_mac_rx_callback in task queue
            }
            else if (frame_type == ERROR_FRAME)
            {
                nos_rf_modem_flush_rxbuf();
                return;
            }
        }
    }
}

BOOL lpll_mac_unslotted_csma_ca(void)
{
    UINT8 foo, nb, be, max_delay = 1;

    be = csma_min_be;

    for (foo = be; foo > 0; foo--)
        max_delay *= 2;

    for (nb = 0; nb < csma_retries; nb++)
    {
        foo = rand() % max_delay;
        nos_delay_us(foo * SYMBOL_DURATION * IEEE_802_15_4_UNIT_BACKOFF_PERIOD);

        lpll_mac_flush_rx_buf();// handles rx_frames while backoff period.

        if (nos_rf_modem_cca() == TRUE)
            return TRUE;
        
        if (be + 1 <= csma_max_be)
        {
            be++;
            max_delay *= 2;
        }
    }
#ifdef LPLL_MAC_DEBUG
    nos_uart_puts(STDIO, "\ncsma fail");
#endif /* LPLL_MAC_DEBUG */
    return FALSE;
}

BOOL lpll_mac_tx_on_csma(NOS_MAC_TX_INFO* tx_info, UINT8 seq, UINT8 tx_retries)
{
    BOOL tx_success = FALSE;
    UINT8 retrans_trials;
    BOOL ack_required;

#ifdef CC2420_INLINE_SECURITY_M
    if (cc2420_secure_frame(tx_info) != ERROR_SUCCESS)
        return FALSE;
#endif /* CC2420_INLINE_SECURITY_M */

    ack_required = (tx_retries > 0);
    lpll_mac_flush_rx_buf();
    nos_rf_modem_write_frame(tx_info);
    if (ack_required)
    {
        for (retrans_trials = 0; retrans_trials < tx_retries; ++retrans_trials)
        {
            if (lpll_mac_unslotted_csma_ca())
            {
                // Retransmit until time is over or ACK is received.
#ifdef CC2420_M

#ifdef LPLL_MAC_DEBUG
                nos_uart_puts(STDIO, "\ncsma success");
#endif /* LPLL_MAC_DEBUG */

                TBCCR2 = LPLL_CURRENT_TIME() + LPLL_MAC_STROBE_TIME;
                TBCCTL2 &= ~CCIFG;   // Clear Interrupt flag.
                TBCCTL2 |= CCIE;     // Enable TBCCR2 interrupt.
#ifdef LPLL_MAC_DEBUG
                nos_uart_putc(STDIO, '\n');
                nos_uart_putc(STDIO, '\r');
#endif /* LPLL_MAC_DEBUG */
                while ((tx_success == FALSE)&&((TBCCTL2 & CCIFG) == 0))
                {
#ifdef LPLL_MAC_DEBUG
                    nos_uart_putc(STDIO, 'T');
#endif /* LPLL_MAC_DEBUG */
                    if (!nos_rf_modem_tx(tx_info))
                        continue;
                    if (nos_rf_modem_wait_for_ack(seq))
                    {
                        tx_success = TRUE;
#ifdef DYNAMIC_SLEEP_TIME
                        if (MAC_ENV.short_sleep_state == FALSE)
                            lpll_mac_short_sleep_mode();
#endif //DYNAMIC_SLEEP_TIME
                    }
                }
                TBCCTL2 &= ~CCIE;    // Disable Interrupt.    
                TBCCTL2 &= ~CCIFG;   // Clear Interrupt flag.
#endif /* CC2420_M */
                break;
            }
        }
    }
    else
    {
        tx_success = lpll_mac_unslotted_csma_ca();
    }

    return tx_success;
}

BOOL lpll_mac_tx(NOS_MAC_TX_INFO* tx_info, BOOL ack_required)
{
    BOOL tx_success;
    UINT8 eui64_src[16];
    UINT8 mpdu_length;
    UINT8 header_tail;
    UINT8 payload_tail;

#if IEEE_802_15_4_DEV_INDEPENDENT_SECURITY_M
    if (!ieee_802_15_4_check_sec_params(tx_info->sec, tx_info->sec_key_idx))
    {
        // Invalid security arguments.
        return FALSE;
    }
#endif /* IEEE_802_15_4_DEV_INDEPENDENT_SECURITY_M */

    // if broadcast msg
    if (tx_info->dest_addr_eui64 == NULL && tx_info->dest_addr == 0xFFFF)
    {
        ack_required = FALSE;
    }
    
    if (ieee_802_15_4_make_data_frame(tx_info,
                                      MAC_ENV.tx_seq,
                                      nos_rf_modem_get_pan_id(),
                                      nos_rf_modem_get_eui64(eui64_src),
                                      nos_rf_modem_get_short_id(),
                                      ack_required) != ERROR_SUCCESS)
        return FALSE;

    mpdu_length = tx_info->header_length + tx_info->payload_length + tx_info->sec_mic_length + 2;
    header_tail = tx_info->header_length;
    payload_tail = tx_info->payload_length;
    tx_info->header[header_tail++] = LPLL_ID;    //LPLL Identifier

    if (mpdu_length < 40)
    {
        while (payload_tail < tx_info->payload_length + 40 - mpdu_length)
        {
            tx_info->payload[payload_tail++] = 0x0;
        }
        tx_info->header[header_tail++] = 40 - mpdu_length;    //The size of padding
    }
    else
    {
        tx_info->header[header_tail++] = 0;    //There is no padding
    }
    
    tx_info->payload_length = payload_tail;
    tx_info->header_length = header_tail;

#ifdef IEEE_802_15_4_SECURITY_M
    if (tx_info->sec != IEEE_802_15_4_SEC_NONE)
    {
#ifdef IEEE_802_15_4_DEV_SECURITY_ACCELERATOR_M
        // Nothing to do.
#else
#ifdef AES_M
        /*
         * Secure the frame by using security utilities are not provided by
         * IEEE 802.15.4 radio transceivers.
         */
        if (ieee_802_15_4_secure_frame(tx_info, ack_required) < ERROR_SUCCESS)
            return FALSE;

#else
        // No way to secure the frame.
        return FALSE;
#endif /* AES_M */
#endif /* IEEE_802_15_4_DEV_SECURITY_ACCELERATOR_M */
    }
    else
    {
        tx_info->sec_mic_length = 0;
    }
#else
    // Ignore security level when 802.15.4 security mode is disabled.
    tx_info->sec = IEEE_802_15_4_SEC_NONE;
#endif /* IEEE_802_15_4_SECURITY_M */

    MAC_ENV.tx_on_state = TRUE;

    tx_success = FALSE;

    NOS_ENTER_CRITICAL_SECTION();
    // if RF module was turned off, turn it on.
    if (!MAC_ENV.rx_on_state)
    {
        lpll_mac_rf_on();
    }

    if (ack_required)
    {
        tx_success = lpll_mac_tx_on_csma(
                tx_info,
                MAC_ENV.tx_seq,
                MAC_MAX_TX_TRIALS);
    }
    else
    {
        tx_success = lpll_mac_tx_on_csma(
                tx_info,
                MAC_ENV.tx_seq,
                0);
    }

    lpll_mac_rf_off();  // Turn off RF module

    NOS_EXIT_CRITICAL_SECTION();

    ++MAC_ENV.tx_seq;
    MAC_ENV.tx_on_state = FALSE;    
    return tx_success;
}

void lpll_mac_rf_on(void)
{
    NOS_ENTER_CRITICAL_SECTION();
    UINT8 spiStatusByte;
    CC2420_SWITCH_ON();
    // wait for the crystal oscillator to become stable
    do {
        CC2420_UPD_STATUS(spiStatusByte);
    } while (!(spiStatusByte & (1 << CC2420_XOSC16M_STABLE)));
    MAC_ENV.rx_on_state = TRUE;
    NOS_EXIT_CRITICAL_SECTION();
}

void lpll_mac_rf_off(void)
{
    NOS_ENTER_CRITICAL_SECTION();
    CC2420_SWITCH_OFF();
    MAC_ENV.rx_on_state = FALSE;
    NOS_EXIT_CRITICAL_SECTION();
}

void lpll_mac_set_tx_power(UINT8 p)
{
    nos_mg2410_set_tx_power(p);
}

UINT8 lpll_mac_get_opmode(void)
{
    return nos_rf_modem_get_opmode();
}

UINT16 lpll_mac_get_pan_id(void)
{
    return nos_rf_modem_get_pan_id();
}

UINT16 lpll_mac_get_short_id(void)
{
    return nos_rf_modem_get_short_id();
}

UINT8 *lpll_mac_get_eui64(UINT8 *eui64)
{
    return nos_rf_modem_get_eui64(eui64);
}

BOOL lpll_mac_eui64_is_set(void)
{
    return nos_rf_modem_eui64_is_set();
}

UINT8 lpll_mac_get_max_tx_payload_len(BOOL da_eui64, UINT8 sec)
{
    return ieee_802_15_4_get_max_tx_payload_len(da_eui64,
                                                lpll_mac_eui64_is_set(),
                                                sec);
}

wakeup interrupt (TIMERB1_VECTOR) TBIV_ISR(void)
{
    NOS_ENTER_ISR();
#ifdef LPLL_MAC_DEBUG
    nos_uart_putc(STDIO, '\n');
    nos_uart_putc(STDIO, '\r');
    nos_uart_putc(STDIO, 'I');
#endif /* LPLL_MAC_DEBUG */

    switch (TBIV)
    {
        case 0x02:
#ifdef LPLL_MAC_DEBUG
            nos_uart_putc(STDIO, '2');
#endif /* LPLL_MAC_DEBUG */            
            if(MAC_ENV.short_sleep_state)
            {
#ifdef LPLL_MAC_DEBUG
                nos_uart_putc(STDIO, 'S');
#endif /* LPLL_MAC_DEBUG */
                TBCCR1 = LPLL_CURRENT_TIME() + LPLL_MAC_SHORT_SLEEP_TIME;
            }
            else
            {
#ifdef LPLL_MAC_DEBUG
                nos_uart_putc(STDIO, 'L');
#endif /* LPLL_MAC_DEBUG */
                TBCCR1 = LPLL_CURRENT_TIME() + LPLL_MAC_SLEEP_TIME;
            }
            if (!(MAC_ENV.tx_on_state))
                lpll_mac_wakeup();  //Wakeup and Check Radio Signal
            break;
            
        case 0x08:
#ifdef LPLL_MAC_DEBUG
            nos_uart_putc(STDIO, '8');
#endif /* LPLL_MAC_DEBUG */
            //nos_delay_ms(500);
            MAC_ENV.short_sleep_state = FALSE;
            TBCCTL4 &= ~CCIE;
            TBCCTL4 &= ~CCIFG;
            break;
            
        default:
            break;
    }
    NOS_EXIT_ISR();
}

#endif /* LPLL_MAC_M */
