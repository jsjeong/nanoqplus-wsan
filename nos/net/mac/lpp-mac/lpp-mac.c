// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

#include "lpp-mac.h"
#ifdef LPP_MAC
#include <stdlib.h>
#include <string.h>
#include "queue.h"
#include "critical_section.h"
#include "taskq.h"
#include "lpp.h"
#include "rf_modem.h"

enum
{
    TXQLEN = 5,
    RXQLEN = 5,
    PROBE_INTERVAL = 2,
    TX_LISTEN_TIMEOUT = 4,
};

struct lpp_mac_control
{
    BOOL txing:1;
    
    struct queue txq_ctrl;
    NOS_MAC_TX_INFO tx_frame[TXQLEN];
    NOS_MAC_TX_INFO tx_frame_buf;
    ERROR_T tx_result;
    
    struct queue rxq_ctrl;
    struct wpan_rx_frame rx_frame[RXQLEN];
    struct wpan_rx_frame rx_frame_buf;

    UINT8 tx_seq;
    UINT8 last_rx_seq;
    UINT16 last_rx_src_addr;
    //UINT8 last_rx_src_addr64[8];
    UINT16 last_probe_src_addr;      //To avoid duplicate broadcasts
    void (*rx_notify)(void);
    void (*tx_notify)(NOS_MAC_TX_INFO *frame, ERROR_T result);
};

static struct lpp_mac_control mac;

UINT32 radio_on_timestamp;
UINT8 radio_on_ticks;
UINT32 radio_off_timestamp;
UINT8 radio_off_ticks;

static void print_radio_on_time(void)
{
    printf("[T] radio on:%lu.%u\n", radio_on_timestamp, radio_on_ticks);
    printf("[T] radio off:%lu.%u\n", radio_off_timestamp, radio_off_ticks);
}

static BOOL rx_handler(void)
{
    UINT8 seq;
    UINT16 src;

    while (!nos_rf_modem_rxbuf_is_empty())
    {
        mac.rx_frame_buf.len = nos_rf_modem_read_rxbuf(mac.rx_frame_buf.buf,
                                                       &mac.rx_frame_buf.rssi,
                                                       &mac.rx_frame_buf.lqi,
                                                       &mac.rx_frame_buf.crc_ok);

        if (!mac.rx_frame_buf.crc_ok)
        {
            nos_rf_modem_flush_rxbuf();
            break;
        }
        
        seq = mac.rx_frame_buf.buf[2];
        src = mac.rx_frame_buf.buf[7] + (mac.rx_frame_buf.buf[8] << 8);

        if (seq == mac.last_rx_seq && src == mac.last_rx_src_addr)
            continue;

        mac.last_rx_seq = seq;
        mac.last_rx_src_addr = src;
    
        lpp_receive_control_packet(mac.rx_frame_buf.buf, mac.rx_frame_buf.len);
    }

    return (queue_is_full(&mac.rxq_ctrl));
}

static void initiate_tx(void *args)
{
    UINT8 idx;

    if (queue_pop(&mac.txq_ctrl, &idx) == ERROR_SUCCESS)
    {
        mac.tx_frame_buf = mac.tx_frame[idx];
        lpp_stop();
        lpp_set_listen_timeout(TX_LISTEN_TIMEOUT);
        lpp_listen();
        mac.txing = TRUE;
    }
}

/* static void sleep(void *args) */
/* { */
/*     UINT16 d; */

/*     printf("%s()\n", __FUNCTION__); */
/*     lpp_stop(); */
/*     d = rand(); */
/*     d %= 50000; */
/*     nos_delay_us(d); */
/*     lpp_sleep(); */

/*     printf("probe!\n"); */
/* } */

static void lpp_event_handler(enum lpp_state state)
{
    if (state == LPP_STOP)
    {
    }
    else if (state == LPP_LISTEN)
    {
    }
    else if (state == LPP_SLEEP)
    {
        printf("Sleeping...\n");
        print_radio_on_time();
    }
    else if (state == LPP_LISTEN_TIMEOUT)
    {
        if (mac.txing)
        {
            if (mac.tx_frame_buf.dest_addr == 0xFFFF)
            {
                mac.tx_result = ERROR_SUCCESS;
                mac.last_probe_src_addr = nos_rf_modem_get_short_id();
            }
            else
            {
                mac.tx_result = ERROR_802154_TX_FAIL;
            }

            mac.txing = FALSE;
            mac.tx_seq++;
        
            if (mac.tx_notify != NULL)
                mac.tx_notify(&mac.tx_frame_buf, mac.tx_result);

            if (!queue_is_empty(&mac.txq_ctrl))
            {
                nos_taskq_reg(initiate_tx, NULL);
            }
        }
    }
}

void lpp_mac_init(UINT16 pan_id, UINT8 *eui64, UINT16 short_id)
{
    queue_init(&mac.txq_ctrl, TXQLEN);
    queue_init(&mac.rxq_ctrl, RXQLEN);

    nos_rf_modem_config(pan_id, short_id, eui64, rx_handler);

    srand(pan_id + short_id);
    mac.tx_seq = rand();
    mac.last_rx_seq = mac.tx_seq;
    mac.last_rx_src_addr = short_id;
    mac.last_probe_src_addr = short_id;
    /* if (eui64) */
    /*     memcpy(mac.last_rx_src_addr64, eui64, 8); */
    mac.rx_notify = NULL;
    mac.tx_notify = NULL;
    mac.txing = FALSE;

    lpp_init();
    lpp_set_event_handler(lpp_event_handler);
    lpp_set_probe_interval(PROBE_INTERVAL);
    lpp_sleep();
}

ERROR_T lpp_mac_tx(NOS_MAC_TX_INFO *frame)
{
    UINT8 idx;
    ERROR_T result;

    if (!frame)
        return FALSE;

    NOS_ENTER_CRITICAL_SECTION();
    if (queue_add(&mac.txq_ctrl, &idx) == ERROR_SUCCESS)
    {
        mac.tx_frame[idx] = *frame;
        result = SUCCESS_OPERATION_RESERVED;
    }
    else
    {
        result = ERROR_GENERAL_FAILURE;
    }
    NOS_EXIT_CRITICAL_SECTION();

    if (!mac.txing && !queue_is_empty(&mac.txq_ctrl))
    {
        nos_taskq_reg(initiate_tx, NULL);
    }

    return result;
}

BOOL lpp_mac_rx(NOS_MAC_RX_INFO *frame)
{
    UINT8 idx;
    BOOL result;

    if (!frame)
        return FALSE;

    NOS_ENTER_CRITICAL_SECTION();
    if (queue_pop(&mac.rxq_ctrl, &idx) == ERROR_SUCCESS)
    {
        frame->src_addr = mac.rx_frame[idx].buf[7] + (mac.rx_frame[idx].buf[8] << 8);
        frame->use_eui64 = FALSE;
        frame->rssi = mac.rx_frame[idx].rssi;
        frame->corr = mac.rx_frame[idx].lqi;
        frame->payload_length = mac.rx_frame[idx].len - 9;
        memcpy(frame->payload, &mac.rx_frame[idx].buf[9], frame->payload_length);
        result = TRUE;
    }
    else
    {
        result = FALSE;
    }
    NOS_EXIT_CRITICAL_SECTION();

    return result;
}

void lpp_mac_set_rx_notifier(void (*notify_to)(void))
{
    mac.rx_notify = notify_to;
}

void lpp_mac_set_tx_notifier(void (*notify_to)(NOS_MAC_TX_INFO *frame, ERROR_T result))
{
    mac.tx_notify = notify_to;
}

/**
 * Implementation of LPP.
 */
UINT8 lpp_get_probe(UINT8 *pkt)
{
    pkt[0] = 0x41;
    pkt[1] = 0x98;
    pkt[2] = mac.tx_seq++;
    pkt[3] = (nos_rf_modem_get_pan_id() & 0xFF);
    pkt[4] = (nos_rf_modem_get_pan_id() >> 8);
    pkt[5] = 0xff;
    pkt[6] = 0xff;
    pkt[7] = (nos_rf_modem_get_short_id() & 0xFF);
    pkt[8] = (nos_rf_modem_get_short_id() >> 8);

    return 9;
}

/**
 * Implementation of LPP. Called after returning TRUE in lpp_handle_probe().
 */
UINT8 lpp_get_probe_ack(UINT8 *pkt)
{
    mac.tx_result = ieee_802_15_4_make_data_frame(&mac.tx_frame_buf,
                                                  mac.tx_seq,
                                                  nos_rf_modem_get_pan_id(),
                                                  NULL,
                                                  nos_rf_modem_get_short_id(),
                                                  FALSE);

    if (mac.tx_result != ERROR_SUCCESS)
    {
        mac.tx_notify(&mac.tx_frame_buf, mac.tx_result);
        return 0;
    }

    memcpy(pkt, mac.tx_frame_buf.header, mac.tx_frame_buf.header_length);
    memcpy(&pkt[mac.tx_frame_buf.header_length], mac.tx_frame_buf.payload,
           mac.tx_frame_buf.payload_length);

    return mac.tx_frame_buf.header_length + mac.tx_frame_buf.payload_length;
}

/**
 * Implementation of LPP.
 */
BOOL lpp_is_probe(const UINT8 *pkt, UINT8 size)
{
    UINT16 pan, dst;
    
    if (size != 9)
        return FALSE;

    pan = pkt[3] + (pkt[4] << 8);
    dst = pkt[5] + (pkt[6] << 8);
    
    if (pkt[0] == 0x41 && pkt[1] == 0x98 &&
        (pan == nos_rf_modem_get_pan_id() || pan == 0xffff) &&
        (dst == nos_rf_modem_get_short_id() || dst == 0xffff))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/**
 * Implementation of LPP.
 */
BOOL lpp_is_probe_ack(const UINT8 *pkt, UINT8 size)
{
    UINT16 pan, dst;
    
    if (size <= 9)
        return FALSE;

    pan = pkt[3] + (pkt[4] << 8);
    dst = pkt[5] + (pkt[6] << 8);

    if (pkt[0] == 0x41 && pkt[1] == 0x98 &&
        (pan == nos_rf_modem_get_pan_id() || pan == 0xffff) &&
        (dst == nos_rf_modem_get_short_id() || dst == 0xffff))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static void stop_tx(void *args)
{
    UINT8 d;

    mac.tx_seq++;
    mac.txing = FALSE;

    lpp_stop();

    d = rand();
    d %= 50000;
    nos_delay_us(d);
    lpp_sleep();

    if (mac.tx_notify)
        mac.tx_notify(&mac.tx_frame_buf, ERROR_SUCCESS);

    
    if (!queue_is_empty(&mac.txq_ctrl))
    {
        nos_taskq_reg(initiate_tx, NULL);
    }
}

/**
 * Implementation of LPP. Returning TRUE makes send the data frame as a probe ack.
 */
BOOL lpp_handle_probe(const UINT8 *probe, UINT8 len)
{
    UINT16 src;

    if (!mac.txing)
        return FALSE;
    
    src = probe[7] + (probe[8] << 8);
    
    if (mac.tx_frame_buf.dest_addr == 0xffff && src != mac.last_probe_src_addr)
    {
        mac.last_probe_src_addr = src;
        return TRUE;
    }
    
    if (src == mac.tx_frame_buf.dest_addr)
    {
        nos_taskq_reg(stop_tx, NULL);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/**
 * Implementation of LPP.
 */
BOOL lpp_handle_probe_ack(const UINT8 *probe_ack, UINT8 len)
{
    UINT8 idx;
    BOOL result;
    
    NOS_ENTER_CRITICAL_SECTION();
    if (queue_add(&mac.rxq_ctrl, &idx) == ERROR_SUCCESS)
    {
        mac.rx_frame[idx] = mac.rx_frame_buf;
        result = TRUE;
    }
    else
    {
        result = FALSE;
    }
    NOS_EXIT_CRITICAL_SECTION();

    if (mac.rx_notify)
    {
        mac.rx_notify();
    }
    
    return FALSE; //To accept more data packets.
}

#endif //LPP_MAC

