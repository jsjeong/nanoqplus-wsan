// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#ifndef MAC_H
#define MAC_H

#include "nos_common.h"
#include "errorcodes.h"
#include "ieee-802-15-4.h"

#define nos_mac_set_sec_key(idx,key)            ieee_802_15_4_set_sec_key(idx,key)

enum
{
    NOS_MAC_MAX_SAFE_RX_PAYLOAD_SIZE = IEEE_802_15_4_MAX_SAFE_RX_PAYLOAD_SIZE,
    NOS_MAC_MAX_SAFE_TX_PAYLOAD_SIZE = IEEE_802_15_4_MAX_SAFE_TX_PAYLOAD_SIZE,
};

#ifdef LPLL_MAC_M
#include "lpll_mac.h"
#define nos_mac_init(panid,sid,lid)             lpll_mac_init(panid,sid,lid)
#define nos_mac_set_tx_power(level)             lpll_mac_set_tx_power(level)
#define nos_mac_set_rx_cb(func)                 lpll_mac_set_rx_cb(func)

#define nos_mac_tx_noack(frame)                 lpll_mac_tx(frame, FALSE)
#define nos_mac_tx(frame)                       lpll_mac_tx(frame, TRUE)
#define nos_mac_rx(frame)                       lpll_mac_rx(frame)

#define nos_mac_rx_on()                         lpll_mac_rx_on()
#define nos_mac_rx_off()                        lpll_mac_rx_off()

#define nos_mac_get_pan_id()                    lpll_mac_get_pan_id()
#define nos_mac_get_short_id()                  lpll_mac_get_short_id()
#define nos_mac_get_eui64(t)                    lpll_mac_get_eui64(t)
#define nos_mac_eui64_is_set()                  lpll_mac_eui64_is_set()
#define nos_mac_get_max_tx_payload_len(d,s)     lpll_mac_get_max_tx_payload_len(d,s)
#endif //LPLL_MAC_M

#ifdef GLOSSY_M
//TODO
#endif //GLOSSY_M

#ifdef LPP_MAC
#include "lpp-mac.h"
#define nos_mac_init(pan,sid,eui64)             lpp_mac_init(pan,eui64,sid)
#define nos_mac_set_rx_cb(func)                 lpp_mac_set_rx_notifier(func)
#define nos_mac_set_tx_cb(func)                 lpp_mac_set_tx_notifier(func)
#define nos_mac_tx(f)                           lpp_mac_tx(f)
#define nos_mac_rx(f)                           lpp_mac_rx(f)
#endif

typedef enum
{
    NOS_MAC_CMD_TX,
    NOS_MAC_CMD_RX,
} NOS_MAC_CMD;

struct nos_mac_ctrl
{
    ERROR_T (*command)(UINT8 dev_id, NOS_MAC_CMD cmd, void *args);
    void (*notify)(UINT8 dev_id);

    void (*notify_beacon_reception)(UINT8 dev_id, const struct wpan_rx_frame *beacon);
#ifdef IEEE_802_15_4_FFD
    void (*notify_mac_command_reception)(UINT8 dev_id, const struct wpan_rx_frame *command);
#endif

    void *mac_ctrl;    /* MAC specific control blocks */
};
typedef struct nos_mac_ctrl NOS_MAC;

enum nos_mac_type
{
    MAC_TYPE_NANO_MAC,
    MAC_TYPE_LPLL_MAC,
    MAC_TYPE_GLOSSY,
};

ERROR_T nos_mac_init(UINT8 dev_id, UINT16 pan_id, UINT16 short_id,
                     const UINT8 *eui64,
                     enum nos_mac_type mac_type,
                     void (*rx_event)(UINT8));

ERROR_T nos_mac_tx(UINT8 dev_id, NOS_MAC_TX_INFO *frame);

struct nos_mac_tx_args
{
    NOS_MAC_TX_INFO *frame;
};

ERROR_T nos_mac_rx(UINT8 dev_id, NOS_MAC_RX_INFO *frame);

struct nos_mac_rx_args
{
    NOS_MAC_RX_INFO *frame;
};

UINT8 nos_mac_get_max_tx_payload_len(UINT8 dev_id, BOOL dst_eui64, BOOL src_eui64, UINT8 sec);

ERROR_T nos_mac_set_beacon_handler(UINT8 dev_id,
                                   void (*rx_beacon_handler)(UINT8 dev_id,
                                                             const struct wpan_rx_frame *beacon));

#ifdef IEEE_802_15_4_FFD
ERROR_T nos_mac_set_mac_command_handler(UINT8 dev_id,
                                        void (*rx_command_handler)(UINT8 dev_id,
                                                                   const struct wpan_rx_frame *command));
#endif

#endif // ~MAC_H
