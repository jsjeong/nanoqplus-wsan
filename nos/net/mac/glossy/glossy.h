// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Glossy MAC
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 12.
 */

#ifndef GLOSSY_H_
#define GLOSSY_H_

#include "nos_common.h"
#include "ieee-802-15-4.h"
#include "queue.h"

struct glossy_control
{
    UINT8 dev_id;
    void (*rx_callback)(UINT8 dev_id,
                        UINT16 src,
                        const UINT8 *payload,
                        UINT8 size);
    UINT16 delay_ticks;
    UINT16 src_recent_rcvd;
    UINT8 seq_recent_rcvd;
    UINT16 src_recent_consume;
    UINT8 seq_recent_consume;
    UINT8 relay_count;
    UINT8 seq;
    UINT8 tx_buf[128];
    struct wpan_rx_frame rx_frame_buf;
    struct wpan_rx_frame rx_frame[3];
    struct queue rxq_ctrl;
    
#ifdef GLOSSY_LPP
    BOOL tx_pending:1;
    BOOL tx_result:1;
    BOOL session_established:1;
    UINT8 tx_hops;
    UINT8 tx_seq;
    UINT16 tx_originator;
    UINT16 tx_destination;
    NOS_MAC_TX_INFO frame_to_send;
    UINT8 my_tx_seq;
    INT8 session_timeout;
    INT8 flood_timeout;
    void (*tx_notifier)(NOS_MAC_TX_INFO *, BOOL);
#endif
};

/**
 * Initialize Glossy MAC instance.
 *
 * @param[in] dev_id  Device ID
 * @param[in] panid   PAN ID
 * @param[in] sid     Short ID
 * @param[in] lid     EUI-64
 */
void glossy_init(UINT8 dev_id, UINT16 panid, UINT16 sid, const UINT8 *lid);

/**
 * Set a callback function to be called when a packet reception.
 *
 * @param[in] dev_id  Device ID
 * @param[in] func    Callback function pointer
 */
void glossy_set_rx_cb(UINT8 dev_id,
                      void (*func)(UINT8 dev_id,
                                   UINT16 src,
                                   const UINT8 *payload,
                                   UINT8 size));

BOOL glossy_rx(NOS_MAC_RX_INFO* f);

/**
 * Initiate a Glossy transmission.
 *
 * @param[in] dev_id  Device ID
 * @param[in] f       A frame to be sent
 */
BOOL glossy_tx(UINT8 dev_id, NOS_MAC_TX_INFO* f);

#ifdef GLOSSY_LPP
BOOL glossy_lpp_send(UINT16 dst, UINT8 *payload, UINT8 size, void (*notifier)(NOS_MAC_TX_INFO *, BOOL));
#endif

void glossy_rx_on(void);
void glossy_rx_off(void);
void glossy_set_tx_power(UINT8 p);
UINT8 glossy_get_opmode(void);
UINT16 glossy_get_pan_id(void);
UINT16 glossy_get_short_id(void);
UINT8 *glossy_get_eui64(UINT8 *eui64);
BOOL glossy_eui64_is_set(void);
UINT8 glossy_get_max_tx_payload_len(BOOL da_eui64, UINT8 sec);

#endif /* GLOSSY_H_ */
