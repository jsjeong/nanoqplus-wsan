// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Implementation of LPP (Low Power Probing) Interface for Glossy
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 7. 8.
 */

#include "kconf.h"

#if defined GLOSSY_LPP
#include "glossy.h"
#include "lpp-interface.h"
#include "lpp.h"
#include "rf_modem.h"
#include <string.h>
#include <stdlib.h>
#include "heap.h"
#include "taskq.h"
#include "user_timer.h"
#include "critical_section.h"
#include "queue.h"
#include "utc_clock.h"
extern struct glossy_control glossy;
extern BOOL glossy_tx(NOS_MAC_TX_INFO *f);

enum
{
    GLOSSY_LPP_PROBE = 0x00,
    GLOSSY_LPP_PROBE_ACK = 0x01,
    GLOSSY_LPP_SUPPRESS = 0x02,
    GLOSSY_LPP_DATA_ACK = 0x03,
    GLOSSY_LPP_CLOSE_CONN = 0x04,
    GLOSSY_LPP_DATA = 0x05,
};

enum
{
    GLOSSY_LPP_PROBE_SIZE = 1,
    GLOSSY_LPP_PROBE_ACK_SIZE = 7,
    GLOSSY_LPP_SUPPRESS_SIZE = 5,
    GLOSSY_LPP_DATA_ACK_SIZE = 2,
    GLOSSY_LPP_CLOSE_CONN_SIZE = 2,
};

#define SET_PROBE_TYPE(buf)        do { (buf)[0] = GLOSSY_LPP_PROBE; } while(0)
#define IS_PROBE_TYPE(buf)         ((buf)[0] == GLOSSY_LPP_PROBE)

#define SET_PROBE_ACK_TYPE(buf)    do { (buf)[0] = GLOSSY_LPP_PROBE_ACK; } while(0)
#define SET_PROBE_ACK_HOPS(buf,h)  do { (buf)[1] = (h); } while(0)
#define SET_PROBE_ACK_ORIG(buf,o)  do { (buf)[2] = (o) & 0xFF; (buf)[3] = ((o) >> 8); } while(0)
#define SET_PROBE_ACK_DST(buf,d)   do { (buf)[4] = (d) & 0xFF; (buf)[5] = ((d) >> 8); } while(0)
#define SET_PROBE_ACK_SEQ(buf,s)   do { (buf)[6] = (s); } while(0)
#define IS_PROBE_ACK_TYPE(buf)     ((buf)[0] == GLOSSY_LPP_PROBE_ACK)
#define GET_PROBE_ACK_HOPS(buf)    (buf)[1]
#define GET_PROBE_ACK_ORIG(buf)    ((buf)[2] + ((buf)[3] << 8))
#define GET_PROBE_ACK_DST(buf)     ((buf)[4] + ((buf)[5] << 8))
#define GET_PROBE_ACK_SEQ(buf)     (buf)[6]

#define SET_SUPPRESS_TYPE(buf)      do { (buf)[0] = GLOSSY_LPP_SUPPRESS; } while(0)
#define SET_SUPPRESS_HOPS(buf,h)    do { (buf)[1] = (h); } while(0)
#define SET_SUPPRESS_ORIG(buf,o)    do { (buf)[2] = (o) & 0xFF; (buf)[3] = ((o) >> 8); } while(0)
#define SET_SUPPRESS_SEQ(buf,s)     do { (buf)[4] = (s); } while(0)
#define IS_SUPPRESS_TYPE(buf)       ((buf)[0] == GLOSSY_LPP_SUPPRESS)
#define GET_SUPPRESS_HOPS(buf)      (buf)[1]
#define GET_SUPPRESS_ORIG(buf)      ((buf)[2] + ((buf)[3] << 8))
#define GET_SUPPRESS_SEQ(buf)       (buf)[4]

#define SET_DATA_ACK_TYPE(buf)     do { (buf)[0] = GLOSSY_LPP_DATA_ACK; } while(0)
#define SET_DATA_ACK_SEQ(buf,s)    do { (buf)[1] = (s); } while(0)
#define IS_DATA_ACK_TYPE(buf)      ((buf)[0] == GLOSSY_LPP_DATA_ACK)
#define GET_DATA_ACK_SEQ(buf)      (buf)[1]

#define SET_CLOSE_CONN_TYPE(buf)   do { (buf)[0] = GLOSSY_LPP_CLOSE_CONN; } while(0)
#define SET_CLOSE_CONN_SEQ(buf,s)  do { (buf)[1] = (s); } while(0)
#define IS_CLOSE_CONN_TYPE(buf)    ((buf)[0] == GLOSSY_LPP_CLOSE_CONN)
#define GET_CLOSE_CONN_SEQ(buf)    (buf)[1]

#define SET_DATA_TYPE(buf)         do { (buf)[0] = GLOSSY_LPP_DATA; } while(0)
#define SET_DATA_SEQ(buf,s)        do { (buf)[1] = (s); } while(0)
#define SET_DATA_PAYLOAD(buf,p,l)  do { memcpy(&(buf)[3], p, l); (buf)[2] = l; } while(0)
#define IS_DATA_TYPE(buf)          ((buf)[0] == GLOSSY_LPP_DATA)
#define GET_DATA_SEQ(buf)          (buf)[1]
#define GET_DATA_LEN(buf)          (buf)[2]
#define GET_DATA_PAYLOAD(p,buf)    memcpy(p, &(buf)[3], (buf)[2])

struct tx_queue_element
{
    UINT16 dst;
    UINT8 payload[100];
    UINT8 size;
    void (*notifier)(NOS_MAC_TX_INFO *, BOOL);
};

#define TXQLEN 5
struct tx_queue_element frame_to_send[TXQLEN];
struct queue txq;

#define SESSION_TIMEOUT  3    //sec
#define FLOOD_INIT_DELAY 400  //msec
#define PROBE_INTERVAL   2    //sec
#define LISTEN_TIMEOUT   30   //sec

UINT32 radio_on_timestamp;
UINT8 radio_on_ticks;
UINT32 radio_off_timestamp;
UINT8 radio_off_ticks;
UINT32 session_created_timestamp;
UINT8 session_created_ticks;

static void print_radio_on_time(void)
{
    UINT32 on_sec;
    UINT8 on_ticks;

    printf("[T] radio on:%lu.%u\n", radio_on_timestamp, radio_on_ticks);
    printf("[T] radio off:%lu.%u\n", radio_off_timestamp, radio_off_ticks);
}

static void close_session(void *args)
{
    UINT16 d;

    // Prevent receiving LPP packets during delay.
    nos_rf_modem_rx_off();
    
    printf("Close Glossy session.\n");

    if (glossy.tx_notifier)
    {
        glossy.tx_notifier(&glossy.frame_to_send, glossy.tx_result);
        glossy.tx_notifier = NULL;
    }

    print_radio_on_time();

    d = rand();
    nos_delay_ms(d % 100);
    lpp_sleep();
}

static void send_close_connection(void)
{
    NOS_MAC_TX_INFO *f;
    BOOL result;

    f = nos_malloc(sizeof(NOS_MAC_TX_INFO));
    if (f == NULL)
        return;

    f->dest_addr_eui64 = NULL;

    if (nos_rf_modem_get_short_id() == glossy.tx_destination)
    {
        f->dest_addr = glossy.tx_originator;
    }
    else if (nos_rf_modem_get_short_id() == glossy.tx_originator)
    {
        f->dest_addr = glossy.tx_destination;
    }
    else
    {
        goto out;
    }
    
    SET_CLOSE_CONN_TYPE(f->payload);
    SET_CLOSE_CONN_SEQ(f->payload, glossy.tx_seq);
    f->payload_length = GLOSSY_LPP_CLOSE_CONN_SIZE;

    result = glossy_tx(f);
    
    printf("Send a close connection message to %u. (%d)\n", f->dest_addr, result);

out:
    nos_free(f);
}

static void session_timeout(void *args)
{
    if (glossy.session_timeout < 0 ||
        !glossy.session_established)
        return;
    
    printf("Session timeout\n");

    if (glossy.tx_originator == nos_rf_modem_get_short_id() ||
        glossy.tx_destination == nos_rf_modem_get_short_id())
        send_close_connection();

    close_session(NULL);
}

void send_data_ack(void *args)
{
    NOS_MAC_TX_INFO *f;
    BOOL result;

    f = nos_malloc(sizeof(NOS_MAC_TX_INFO));
    if (f == NULL)
        return;

    f->dest_addr_eui64 = NULL;
    f->dest_addr = glossy.tx_originator;
    SET_DATA_ACK_TYPE(f->payload);
    SET_DATA_ACK_SEQ(f->payload, glossy.tx_seq);
    f->payload_length = GLOSSY_LPP_DATA_ACK_SIZE;

    result = glossy_tx(f);
    nos_user_timer_destroy(glossy.session_timeout);
    glossy.session_timeout = -1;
    nos_taskq_reg(close_session, NULL);
    
    printf("DATA ACK message to %u.(%d)\n", glossy.tx_originator, result);

    nos_free(f);
}

static void send_lpp_suppress(void *args)
{
    NOS_MAC_TX_INFO *f;
    BOOL result;

    f = nos_malloc(sizeof(NOS_MAC_TX_INFO));
    if (f == NULL)
        return;

    f->dest_addr_eui64 = NULL;
    f->dest_addr = glossy.tx_originator;
    SET_SUPPRESS_TYPE(f->payload);
    SET_SUPPRESS_HOPS(f->payload, glossy.tx_hops);
    SET_SUPPRESS_ORIG(f->payload, glossy.tx_originator);
    SET_SUPPRESS_SEQ(f->payload, glossy.tx_seq);
    f->payload_length = GLOSSY_LPP_SUPPRESS_SIZE;

    result = glossy_tx(f);
    glossy.session_established = TRUE;
    glossy.session_timeout = nos_user_timer_create_sec(session_timeout, NULL, SESSION_TIMEOUT, FALSE);
    if (glossy.session_timeout >= 0)
    {
        lpp_stop();
    }

    NOS_ENTER_CRITICAL_SECTION();
    session_created_ticks = TCNT2;
    session_created_timestamp = nos_get_utc_time();
    NOS_EXIT_CRITICAL_SECTION();
    
    printf("SUPPRESS message to %u. (%d) [T:%lu.%u]\n",
           glossy.tx_originator, result,
           session_created_timestamp, session_created_ticks);

    nos_free(f);
}

static void send_data(void *args)
{
    BOOL result;
    
    result = glossy_tx(&glossy.frame_to_send);
    printf("DATA message. (%d)\n", result);
}

static void initiate_session(void *args)
{
    UINT8 idx;
    
    NOS_ENTER_CRITICAL_SECTION();
    if (glossy.tx_pending == FALSE && queue_pop(&txq, &idx) == ERROR_SUCCESS)
    {
        glossy.tx_notifier = frame_to_send[idx].notifier;
        glossy.tx_result = FALSE;
        glossy.tx_pending = TRUE;
        glossy.tx_hops = 5;
        glossy.tx_seq = glossy.my_tx_seq++;
        glossy.tx_originator = nos_rf_modem_get_short_id();
        glossy.tx_destination = frame_to_send[idx].dst;
        lpp_set_listen_timeout(LISTEN_TIMEOUT);

        glossy.frame_to_send.dest_addr = glossy.tx_destination;
        glossy.frame_to_send.dest_addr_eui64 = NULL;
        SET_DATA_TYPE(glossy.frame_to_send.payload);
        SET_DATA_SEQ(glossy.frame_to_send.payload, glossy.tx_seq);
        SET_DATA_PAYLOAD(glossy.frame_to_send.payload, frame_to_send[idx].payload, frame_to_send[idx].size);
        glossy.frame_to_send.payload_length = frame_to_send[idx].size + 3;

        lpp_stop();
        lpp_listen();
    }
    NOS_EXIT_CRITICAL_SECTION();
}

void on_new_lpp_state(enum lpp_state state)
{
    if (state == LPP_STOP)
    {
        printf("LPP is stopped.\n");
    }
    else if (state == LPP_SLEEP)
    {
        glossy.tx_pending = FALSE;
        glossy.tx_result = FALSE;
        if (glossy.tx_notifier)
        {
            glossy.tx_notifier(&glossy.frame_to_send, FALSE);
            glossy.tx_notifier = NULL;
        }

        if (!queue_is_empty(&txq))
        {
            nos_user_timer_create_sec(initiate_session, NULL, 1+rand()%10, FALSE);
        }
        
        printf("Sleeping... (%u->%u, %u)\n",
               glossy.tx_originator, glossy.tx_destination, glossy.tx_seq);
        print_radio_on_time();
    }
    else if (state == LPP_LISTEN)
    {
        printf("Listening... (%u->%u, %u, Hop:%u)\n",
               glossy.tx_originator, glossy.tx_destination, glossy.tx_seq, glossy.tx_hops);
    }
    else if (state == LPP_LISTEN_TIMEOUT)
    {
        printf("Listen timeout!\n");
        if (glossy.tx_originator == nos_rf_modem_get_short_id() ||
            glossy.tx_destination == nos_rf_modem_get_short_id())
            send_close_connection();
    }
    else
    {
        printf("Unknown state!!\n");
    }
}

BOOL glossy_lpp_receive_control_packet(const UINT8 *glossy_pkt, UINT8 pkt_len)
{
    UINT16 src, dst;
    
    /*
     * Check the received packet is a Glossy-LPP control packet such as the
     * suppression, or the end-to-end ack.
     */
    if (glossy.tx_pending == FALSE)
    {
        return TRUE; // to prevent receiving frames without LPP phase.
    }

    dst = glossy_pkt[5] + (glossy_pkt[6] << 8);
    src = glossy_pkt[7] + (glossy_pkt[8] << 8);

    if (IS_SUPPRESS_TYPE(&glossy_pkt[9]))
    {
        if (src == glossy.tx_destination &&
            dst == glossy.tx_originator &&
            pkt_len == 9 + GLOSSY_LPP_SUPPRESS_SIZE &&
            GET_SUPPRESS_SEQ(&glossy_pkt[9]) == glossy.tx_seq) // 9: MAC header
        {
            NOS_ENTER_CRITICAL_SECTION();
            session_created_ticks = TCNT2;
            session_created_timestamp = nos_get_utc_time();
            NOS_EXIT_CRITICAL_SECTION();
            
            glossy.session_timeout = nos_user_timer_create_sec(session_timeout, NULL, SESSION_TIMEOUT, FALSE);
            
            if (glossy.session_timeout >= 0)
            {
                glossy.session_established = TRUE;
                lpp_stop();
                
                if (glossy.tx_originator == nos_rf_modem_get_short_id())
                {
                    // I'm the initiator. Start a Glossy transmission.
                    nos_user_timer_create_ms(send_data, NULL, FLOOD_INIT_DELAY, FALSE);
                }
            }
            //LPP suppress message.
            printf("SUPPRESS received. [T:%lu.%u]\n",
                   session_created_timestamp, session_created_ticks);
        }
        else
        {
            printf("SUPPRESS not my flow. (%u->%u (%u))\n",
                   src, dst, GET_SUPPRESS_SEQ(&glossy_pkt[9]));
        }
        return TRUE;
    }
    else if (IS_DATA_ACK_TYPE(&glossy_pkt[9]))
    {
        if (GET_DATA_ACK_SEQ(&glossy_pkt[9]) == glossy.tx_seq &&
            src == glossy.tx_destination &&
            dst == glossy.tx_originator &&
            pkt_len == 9 + GLOSSY_LPP_DATA_ACK_SIZE)
        {
            printf("DATA ACK received.\n");
            
            nos_user_timer_destroy(glossy.session_timeout);
            glossy.session_timeout = -1;
            glossy.tx_result = TRUE;
            nos_taskq_reg(close_session, NULL);
            lpp_stop();
        }
        else
        {
            printf("DATA ACK not my flow. (%u->%u (%u))\n",
                   src, dst, GET_DATA_ACK_SEQ(&glossy_pkt[9]));
        }
        return TRUE;
    }
    else if (IS_CLOSE_CONN_TYPE(&glossy_pkt[9]))
    {
        if (GET_CLOSE_CONN_SEQ(&glossy_pkt[9]) == glossy.tx_seq &&
            ((src == glossy.tx_originator && dst == glossy.tx_destination) ||
             (src == glossy.tx_destination && dst == glossy.tx_originator)) &&
            pkt_len == 9 + GLOSSY_LPP_CLOSE_CONN_SIZE)
        {
            printf("CLOSE CONN received.\n");

            nos_user_timer_destroy(glossy.session_timeout);
            glossy.session_timeout = -1;
            glossy.tx_result = FALSE;
            nos_taskq_reg(close_session, NULL);
            lpp_stop();
        }
        else
        {
            printf("CLOSE CONN not my flow. (%u->%u (%u))\n",
                   src, dst, GET_CLOSE_CONN_SEQ(&glossy_pkt[9]));
        }
        return TRUE;
    }
    else if (IS_DATA_TYPE(&glossy_pkt[9]))
    {
        if (src == glossy.tx_originator &&
            dst == glossy.tx_destination &&
            GET_DATA_SEQ(&glossy_pkt[9]) == glossy.tx_seq)
        {
            if (dst == nos_rf_modem_get_short_id())
            {
                printf("DATA for me\n");
                //nos_user_timer_create_ms(send_data_ack, NULL, FLOOD_INIT_DELAY, FALSE);
                nos_delay_ms(FLOOD_INIT_DELAY);
                send_data_ack(NULL);
                return FALSE;
            }
            else
            {
                printf("Glossy forward!\n");
                return TRUE;
            }
        }
        else
        {
            printf("DATA not my flow (%u->%u (%u))\n", src, dst, GET_DATA_SEQ(&glossy_pkt[9]));
            return TRUE;
        }
    }
    else
    {
        printf("Non Glossy-LPP control packet-type:%u\n", glossy_pkt[9]);
        return FALSE;
    }
}

BOOL glossy_lpp_send(UINT16 dst, UINT8 *payload, UINT8 size, void (*notifier)(NOS_MAC_TX_INFO *, BOOL))
{
    UINT8 idx;
    BOOL result;

    NOS_ENTER_CRITICAL_SECTION();
    if (queue_add(&txq, &idx) == ERROR_SUCCESS)
    {
        frame_to_send[idx].dst = dst;
        memcpy(frame_to_send[idx].payload, payload, size);
        frame_to_send[idx].size = size;
        frame_to_send[idx].notifier = notifier;
        result = TRUE;
    }
    else
    {
        result = FALSE;
    }
    NOS_EXIT_CRITICAL_SECTION();

    if (!queue_is_empty(&txq))
    {
        nos_user_timer_create_sec(initiate_session, NULL, 1+rand()%10, FALSE);
    }
    
    return result;
}

/**
 * Implementation of LPP interface.
 */
UINT8 lpp_get_probe(UINT8 *pkt)
{
    // pkt[0:8]: IEEE 802.15.4 header.
    pkt[0] = 0x41;
    pkt[1] = 0x98;
    pkt[2] = glossy.seq;
    pkt[3] = (nos_rf_modem_get_pan_id() & 0xFF);
    pkt[4] = (nos_rf_modem_get_pan_id() >> 8);
    pkt[5] = 0xff;
    pkt[6] = 0xff;
    pkt[7] = (nos_rf_modem_get_short_id() & 0xFF);
    pkt[8] = (nos_rf_modem_get_short_id() >> 8);

    // pkt[9:9]: Probe
    SET_PROBE_TYPE(&pkt[9]);

    glossy.src_recent_rcvd = nos_rf_modem_get_short_id();
    glossy.seq_recent_rcvd = glossy.seq++;
    
    return (9 + GLOSSY_LPP_PROBE_SIZE);
}

/**
 * Implementation of LPP interface.
 */
UINT8 lpp_get_probe_ack(UINT8 *pkt)
{
    // pkt[0:8]: IEEE 802.15.4 header.
    pkt[0] = 0x41;
    pkt[1] = 0x98;
    pkt[2] = glossy.seq;
    pkt[3] = (nos_rf_modem_get_pan_id() & 0xFF);
    pkt[4] = (nos_rf_modem_get_pan_id() >> 8);
    pkt[5] = 0xff;
    pkt[6] = 0xff;
    pkt[7] = (nos_rf_modem_get_short_id() & 0xFF);
    pkt[8] = (nos_rf_modem_get_short_id() >> 8);

    // pkt[9:15]: Probe Ack
    SET_PROBE_ACK_TYPE(&pkt[9]);
    SET_PROBE_ACK_HOPS(&pkt[9], glossy.tx_hops);
    SET_PROBE_ACK_ORIG(&pkt[9], glossy.tx_originator);
    SET_PROBE_ACK_DST(&pkt[9], glossy.tx_destination);
    SET_PROBE_ACK_SEQ(&pkt[9], glossy.tx_seq);

    glossy.src_recent_rcvd = nos_rf_modem_get_short_id();
    glossy.seq_recent_rcvd = glossy.seq++;
    
    return (9 + GLOSSY_LPP_PROBE_ACK_SIZE);
}

/**
 * Implementation of LPP interface.
 */
BOOL lpp_is_probe(const UINT8 *pkt, UINT8 size)
{
    if (size == 9 + GLOSSY_LPP_PROBE_SIZE &&
        IS_PROBE_TYPE(&pkt[9]))
    {
        return TRUE;
    }

    return FALSE;
}

/**
 * Implementation of LPP interface.
 */
BOOL lpp_is_probe_ack(const UINT8 *pkt, UINT8 size)
{
    if (size == 9 + GLOSSY_LPP_PROBE_ACK_SIZE &&
        IS_PROBE_ACK_TYPE(&pkt[9]))
    {
        return TRUE;
    }

    return FALSE;
}

/**
 * Implementation of LPP interface
 */
BOOL lpp_handle_probe(const UINT8 *probe, UINT8 len)
{
    return TRUE;
}

/**
 * Implementation of LPP interface
 */
BOOL lpp_handle_probe_ack(const UINT8 *probe_ack, UINT8 len)
{
    if (glossy.tx_originator == GET_PROBE_ACK_ORIG(&probe_ack[9]) &&
        glossy.tx_seq == GET_PROBE_ACK_SEQ(&probe_ack[9]))
    {
        printf("Duplicate initiation\n");
        return FALSE;
    }
    else if (nos_rf_modem_get_short_id() == GET_PROBE_ACK_ORIG(&probe_ack[9]))
    {
        printf("Address conflicts\n");
        return FALSE;
    }
    else if (GET_PROBE_ACK_HOPS(&probe_ack[9]) == 1)
    {
        printf("Hop limits\n");
        return FALSE;
    }
    else
    {
        if (GET_PROBE_ACK_DST(&probe_ack[9]) == nos_rf_modem_get_short_id())
        {
            if (nos_user_timer_create_ms(send_lpp_suppress, NULL, FLOOD_INIT_DELAY, FALSE) < 0)
            {
                printf("Timer busy\n");
                return TRUE;
            }
        }
        
        glossy.tx_result = FALSE;
        glossy.tx_notifier = NULL;
        glossy.tx_pending = TRUE;
        glossy.session_established = FALSE;
        glossy.tx_seq= GET_PROBE_ACK_SEQ(&probe_ack[9]);
        glossy.tx_originator = GET_PROBE_ACK_ORIG(&probe_ack[9]);
        glossy.tx_destination = GET_PROBE_ACK_DST(&probe_ack[9]);
        glossy.tx_hops = GET_PROBE_ACK_HOPS(&probe_ack[9]) - 1;
        lpp_set_listen_timeout(LISTEN_TIMEOUT);

        return TRUE;
    }
}

void glossy_lpp_init(void)
{
    glossy.tx_pending = FALSE;
    glossy.my_tx_seq = rand();
    glossy.tx_seq = rand();
    glossy.tx_originator = 0xffff;
    glossy.tx_destination = 0xffff;
    glossy.session_timeout = -1;

    queue_init(&txq, TXQLEN);
    
    lpp_init();
    lpp_set_event_handler(on_new_lpp_state);
    lpp_set_probe_interval(PROBE_INTERVAL);
    lpp_sleep();
}

#endif //GLOSSY_LPP
