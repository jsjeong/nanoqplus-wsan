// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Glossy MAC
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 12.
 */

#include "glossy.h"
#ifdef GLOSSY_M
#include "wpan-dev.h"
#include "critical_section.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "glossy-interface.h"
#include "led.h"
#include "taskq.h"
#include "heap.h"

#ifdef GLOSSY_RS
#include "TinyRS.h"
#endif

#ifdef GLOSSY_LPP
#include "lpp.h"
extern void on_new_lpp_state(enum lpp_state new_state);
extern BOOL glossy_lpp_receive_control_packet(const UINT8 *buf, UINT8 len);
#endif

#define RELAY_COUNT_LIMIT 1

extern WPAN_DEV wpan_dev[];
extern NOS_MAC nos_mac[];

static void handle_frame(void *args)
{
    struct glossy_control *gc = (struct glossy_control *) args;
    UINT8 q_idx;
    struct wpan_rx_frame frame;
    ERROR_T err;
    
    static UINT16 seq = 0;
    static UINT16 success = 0;

    NOS_ENTER_CRITICAL_SECTION();
    err = queue_pop(&gc->rxq_ctrl, &q_idx);
    if (err == ERROR_SUCCESS)
    {
        frame = gc->rx_frame[q_idx];
    }
    NOS_EXIT_CRITICAL_SECTION();

    if (err == ERROR_SUCCESS)
    {
        if ((gc->src_recent_consume == (frame.buf[7] + (frame.buf[8] << 8))) &&
            (gc->seq_recent_consume == (frame.buf[2])))
        {
            printf("duplicate\n");
            return;
        }

        printf("%u. Received (%u bytes), RSSI:%d, LQI:%d, CRC %s(%u)\n",
               seq++, frame.len, frame.rssi, frame.lqi,
               ((frame.crc_ok) ? "OK" : "FAIL"),
               ((frame.crc_ok) ? ++success : success));

        if (!frame.crc_ok)
        {
            return;
        }
    
        gc->src_recent_consume = (frame.buf[7] + (frame.buf[8] << 8));
        gc->seq_recent_consume = (frame.buf[2]);

#ifdef GLOSSY_LPP
        if (glossy_lpp_receive_control_packet(frame.buf, frame.len) == TRUE)
        {
            return;
        }
#endif
        
        if (gc->rx_callback != NULL)
        {
            UINT16 src;
        
#ifdef GLOSSY_RS
            UINT16 num_err = 0;

            //Decode with TinyRS.
            if (tinyrs_decode(frame.buf, frame.len, &num_err) == ERROR_SUCCESS)
            {
                printf("Decoding success (# of errors:%u, len:%u)\n",
                       num_err, frame.len);
            }
            else
            {
                printf("Decoding fail (# of errors:%u, len:%u)\n",
                       num_err, frame.len);
            }
#endif

            src = frame.buf[7] + (frame.buf[8] << 8);
#ifdef GLOSSY_LPP
            gc->rx_callback(gc->dev_id, src, &frame.buf[12], frame.len - 12);
#else
            gc->rx_callback(gc->dev_id, src, &frame.buf[9], frame.len - 9);
#endif
        }
    }
}

static void glossy_rx_event_handler(UINT8 dev_id, UINT8 event, void *args)
{
    struct glossy_ctrl *g = (struct glossy_ctrl *) nos_mac[dev_id].mac_ctrl;
    UINT16 ticks_ready = 0;
    UINT16 ticks_tx = 0;
    UINT16 ticks_tx_done = 0;
    UINT16 ticks_read = 0;
    UINT16 ticks_enque = 0;
    BOOL frame_for_me = FALSE;
    BOOL need_to_forward = FALSE;
    BOOL duplicate = FALSE;
    
    UINT8 len, seq;
    UINT16 dst, src;
    BOOL crc_ok;

    glossy_start_delay_timer(dev_id, g->delay_ticks);
    
    wpan_dev_glossy_lock(dev_id);
    nos_led_on(2);

    crc_ok = wpan_dev_glossy_peek_rx_frame(dev_id, &seq, &dst, &src);
    
    if (crc_ok)
    {
        if (src == g->src_recent_rcvd && seq == g->seq_recent_rcvd)
        {
            if (g->relay_count < RELAY_COUNT_LIMIT)
            {
                g->relay_count++;
                need_to_forward = TRUE;
            }
            else
            {
                need_to_forward = FALSE;
            }
            duplicate = TRUE;
            frame_for_me = FALSE;
            goto fwd;
        }
        else
        {
            duplicate = FALSE;
            g->relay_count = 0;
            g->src_recent_rcvd = src;
            g->seq_recent_rcvd = seq;
        }

#ifdef GLOSSY_LPP
        frame_for_me = (dst == nos_rf_modem_get_short_id() ||
                        dst == 0xFFFF ||
                        (g->tx_pending &&
                         (dst == g->tx_originator ||
                          dst == g->tx_destination)));

        if (g->tx_pending &&
            ((src == g->tx_originator && dst == g->tx_destination) ||
             (src == g->tx_destination && dst == g->tx_originator)) &&
            dst != nos_rf_modem_get_short_id())
        {
            need_to_forward = TRUE;
        }
        else
        {
            need_to_forward = FALSE;
        }
#else
        frame_for_me = (dst == wpan_dev[dev_id].short_id || dst == 0xFFFF);
        need_to_forward = (!frame_for_me || dst == 0xFFFF);
#endif     
    }
    else
    {
        /* printf("CRC fail\n"); */
        frame_for_me = FALSE;
        need_to_forward = FALSE;
        duplicate = FALSE;
    }

fwd:
    if (need_to_forward)
    {
        ERROR_T err;
        ticks_ready = glossy_get_ticks(dev_id);

        glossy_wait_until_delay_done(dev_id);
        ticks_tx = glossy_get_ticks(dev_id);

        err = wpan_dev_tx(dev_id, NULL);
        ticks_tx_done = glossy_get_ticks(dev_id);
        /* printf("Fwd done(err:%d,seq:%u,rc:%u) - (Ready:%ut, Tx:%ut)\n", */
        /*        err, seq, g->relay_count,  */
        /*        ticks_ready, ticks_tx); */
    }

    if ((frame_for_me && !duplicate) || !crc_ok)
    {
        UINT8 q_idx;
        
        wpan_dev_read_frame(dev_id, &g->rx_frame_buf);
        ticks_read = glossy_get_ticks(dev_id);

#ifdef GLOSSY_LPP
        if (crc_ok && 
            lpp_receive_control_packet(&g->rx_frame_buf.buf,
                                       &g->rx_frame_buf.len) == TRUE)
        {
            glossy.rx_frame_pending = FALSE;
            goto out;
        }
#endif
        
        if (queue_is_full(&g->rxq_ctrl))
            goto out;

        if (queue_add(&g->rxq_ctrl, &q_idx)
            == ERROR_SUCCESS)
        {
            g->rx_frame[q_idx].crc_ok = crc_ok;
            g->rx_frame[q_idx].rssi = g->rx_frame_buf.rssi;
            g->rx_frame[q_idx].lqi = g->rx_frame_buf.lqi;

            if (crc_ok && g->rx_frame_buf.len < 127)
            {
                g->rx_frame[q_idx].len = g->rx_frame_buf.len;

                memcpy(g->rx_frame[q_idx].buf,
                       g->rx_frame_buf.buf,
                       g->rx_frame_buf.len);
            }

            ticks_enque = glossy_get_ticks(dev_id);
        }

        nos_taskq_reg(handle_frame, (void *) g);
    }

out:
    glossy_stop_timer(dev_id);
    glossy_reset_timer(dev_id);

    /* printf("t:%u-%u-%u-%u-%u(%d,%u,%u,%u)\n",  */
    /*        ticks_ready, ticks_tx, ticks_tx_done, ticks_read, ticks_enque,  */
    /*        crc_ok, g->relay_count, */
    /*        g->src_recent_rcvd, */
    /*        g->seq_recent_rcvd); */
    /* printf("from:%u, to:%u, seq:%u, frame for me:%d, need to fwd:%d\n", */
    /*        src, dst, seq, frame_for_me, need_to_forward); */
    
    wpan_dev_glossy_release(dev_id);
    nos_led_off(2);

    /* printf("(%d:%d, %u:%u:%u)", */
    /*        crc_ok, g->rx_frame_buf.crc_ok, */
    /*        g->src_recent_rcvd, */
    /*        g->seq_recent_rcvd, */
    /*        g->rx_frame_buf.buf[2]); */

    /* printf("[%c%c%c]\n",  */
    /*        (frame_for_me) ? 'M' : '.', */
    /*        (need_to_forward) ? 'F' : '.', */
    /*        (duplicate) ? 'D' : '.'); */
}

void glossy_init(UINT8 dev_id, UINT16 panid, UINT16 sid, const UINT8 *lid)
{
    struct glossy_ctrl *g;
    srand(sid);
    NOS_ENTER_CRITICAL_SECTION();

    nos_mac[dev_id].mac_ctrl =
        (struct glossy_ctrl *) nos_malloc(sizeof(struct glossy_ctrl));
    if (!nos_mac[dev_id].mac_ctrl)
        while(1);
    g = (struct glossy_ctrl *) nos_mac[dev_id].mac_ctrl;
    
    wpan_dev_setup(dev_id, panid, sid, lid, glossy_rx_event_handler, MAC_TYPE_GLOSSY);
    g->dev_id = dev_id;
    g->delay_ticks = 20000;
    g->rx_callback = NULL;
    g->src_recent_rcvd = 0xffff;
    g->seq_recent_rcvd = 0;
    g->src_recent_consume = 0xffff;
    g->seq_recent_consume = 0;
    g->seq = rand();
    g->relay_count = 0;
    queue_init(&g->rxq_ctrl, 3);

#ifdef ISN_903N
    NOS_GPIO_INIT_OUT(A, 2);
    NOS_GPIO_INIT_OUT(A, 3);
#endif

#ifdef GLOSSY_RS
    tinyrs_init();
#endif

#ifdef GLOSSY_LPP
    glossy_lpp_init();
#endif

    NOS_EXIT_CRITICAL_SECTION();
}

void glossy_set_rx_cb(UINT8 dev_id,
                      void (*func)(UINT8 dev_id,
                                   UINT16 src,
                                   const UINT8 *payload,
                                   UINT8 size))
{
    struct glossy_ctrl *g = (struct glossy_ctrl *) nos_mac[dev_id].mac_ctrl;
    g->rx_callback = func;
}

BOOL glossy_tx(UINT8 dev_id, NOS_MAC_TX_INFO* f)
{
    struct glossy_ctrl *g = (struct glossy_ctrl *) nos_mac[dev_id].mac_ctrl;
    UINT8 retry;
    BOOL result;
    UINT8 max_delay;

    f->frame_type = IEEE_802_15_4_FRAME_DATA;
    if (ieee_802_15_4_make_frame(g->tx_buf,
                                 f,
                                 g->seq,
                                 wpan_dev[dev_id].pan_id,
                                 NULL,
                                 wpan_dev[dev_id].short_id,
                                 FALSE) != ERROR_SUCCESS)
    {
        return FALSE;
    }

#ifdef GLOSSY_RS    
    {
        UINT8 *tmp;
        UINT8 len;
        ERROR_T err;
        
        //Encode with TinyRS.
        len = f->header_length + f->payload_length;
        tmp = nos_malloc(DATA_LEN + PARITY_LEN);
        if (tmp == NULL)
        {
            printf("malloc fail\n");
            return FALSE;
        }

        memcpy(&tmp[0], f->header, f->header_length);
        memcpy(&tmp[f->header_length], f->payload, f->payload_length);
        err = tinyrs_encode(tmp, len);
        if (err != ERROR_SUCCESS)
        {
            nos_free(tmp);
            printf("encoding fail:%d\n", err);
            return FALSE;
        }

        memcpy(&f->payload[f->payload_length], &tmp[DATA_LEN], PARITY_LEN);
        f->payload_length += PARITY_LEN;
        nos_free(tmp);
    }
#endif

    NOS_ENTER_CRITICAL_SECTION();

    g->src_recent_rcvd = wpan_dev[dev_id].short_id;
    g->seq_recent_rcvd = g->seq++;
    g->relay_count = 0;
    g->src_recent_consume = g->src_recent_rcvd;
    g->seq_recent_consume = g->seq_recent_rcvd;

    wpan_dev_glossy_lock(dev_id);
    NOS_EXIT_CRITICAL_SECTION();

    wpan_dev_write_frame(dev_id, g->tx_buf);
    //at86rf212_set_tx_power(5);

    // Random backoff
    max_delay = 8;
    result = FALSE;
    for (retry = 0; retry < 3; retry++)
    {
        UINT8 delay = rand() % max_delay;
        nos_delay_us(8 * 20 * wpan_dev[dev_id].symbol_duration);
        if (wpan_dev_cca(dev_id) == TRUE &&
            wpan_dev_tx(dev_id, NULL) == ERROR_SUCCESS)
        {
            result = TRUE;
            break;
        }

        max_delay *= 2;
    }
    //at86rf212_set_tx_power(5);

    wpan_dev_glossy_release(dev_id);

#ifdef GLOSSY_RS
    f->payload_length -= PARITY_LEN;
#endif //GLOSSY_RS
    return result;
}

#endif //GLOSSY_M
