// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "kconf.h"
#ifdef NANO_MAC_M

#include "nano-mac.h"
#include "heap.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "errorcodes.h"
#include "platform.h"
#include "arch.h"
#include "critical_section.h"
#include "wpan-dev.h"
#include "mac.h"

#if defined(LIBNOS_AES_M)
#include "aes.h"
#endif

#ifdef NANO_MAC_LBT
#include "nano-mac-lbt.h"
#endif

#include <stdio.h>

// frame type.
enum
{
    DATA_FRAME = 1,
    ACK_FRAME = 2,
    ERROR_FRAME = 3,
};

extern NOS_MAC nos_mac[WPAN_DEV_CNT];
extern WPAN_DEV wpan_dev[WPAN_DEV_CNT];

static void nmac_rx_event_handler(UINT8 dev_id, UINT8 event, void *args);

ERROR_T nmac_command(UINT8 dev_id, NOS_MAC_CMD cmd, void *args)
{
    if (cmd == NOS_MAC_CMD_TX)
    {
        struct nos_mac_tx_args *a;
        a = (struct nos_mac_tx_args *) args;
        return nmac_tx(dev_id, a->frame);
    }
    else if (cmd == NOS_MAC_CMD_RX)
    {
        struct nos_mac_rx_args *a;
        a = (struct nos_mac_rx_args *) args;
        return nmac_rx(dev_id, a->frame);
    }
    else
    {
        return ERROR_NOT_SUPPORTED;
    }
}

void nmac_init(UINT8 dev_id, UINT16 panid, UINT16 shortid, const UINT8 *longid)
{
    struct nano_mac_ctrl *mac;
    
    NOS_ENTER_CRITICAL_SECTION();

    nos_mac[dev_id].mac_ctrl =
        (struct nano_mac_ctrl *) nos_malloc(sizeof(struct nano_mac_ctrl));
    mac = (struct nano_mac_ctrl *) nos_mac[dev_id].mac_ctrl;
    if (!mac)
        while(1);

    // Set a Rx queue.
    queue_init(&mac->rxq_ctrl, NMAC_RXQ_LEN);
    
    wpan_dev_setup(dev_id, panid, shortid, longid,
                   nmac_rx_event_handler,
                   MAC_TYPE_NANO_MAC);

    srand(panid + shortid);

    // starts from random sequece number to avoid same ack sequence number
    mac->tx_seq = rand();
    mac->last_rx_seq = mac->tx_seq;
    mac->sec_frame_counter = 0;
    mac->sleeping = FALSE;
    nmac_set_csma(dev_id,
                  IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BACKOFFS,
                  IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BE,
                  IEEE_802_15_4_2006_DEFAULT_CSMA_MIN_BE);

#ifdef NANO_MAC_LBT
    nmac_lbt_init();
#endif

    nos_mac[dev_id].command = nmac_command;
    NOS_EXIT_CRITICAL_SECTION();
}

void nmac_set_csma(UINT8 dev_id, UINT8 retries,UINT8 max_be,UINT8 min_be)
{
    struct nano_mac_ctrl *mac =
        (struct nano_mac_ctrl *) nos_mac[dev_id].mac_ctrl;
    mac->csma_retries = retries;
    mac->csma_max_be = max_be;
    mac->csma_min_be = min_be;
}

/**
 * @warn This function should be called atomically. If you need to call it from
 * multiple threads, you have to consider synchronization by declaring critical
 * section, or using semaphore.
 */
ERROR_T nmac_rx(UINT8 dev_id, NOS_MAC_RX_INFO* rx_info)
{
    struct nano_mac_ctrl *mac =
        (struct nano_mac_ctrl *) nos_mac[dev_id].mac_ctrl;
    UINT8 i;
    ERROR_T result;
    struct wpan_rx_frame frame;

    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;

cont:
    NOS_ENTER_CRITICAL_SECTION();
    result = queue_pop(&mac->rxq_ctrl, &i);
    if (result == ERROR_SUCCESS)
    {
        frame = mac->rx_frame[i];
    }
    else
    {
        result = ERROR_FAIL;
    }
    
    if (mac->sleeping)
    {
        wpan_dev_wakeup(dev_id);
        mac->sleeping = FALSE;
    }
    NOS_EXIT_CRITICAL_SECTION();
    
    if (result == ERROR_SUCCESS)
    {
        UINT16 fcf;
        UINT8 idx;
        UINT16 srcshort = 0xffff;
        UINT8 *srclong = NULL;

        frame.mhr_len = ieee_802_15_4_verify_frame(&frame,
                                                   !wpan_dev[dev_id].addrdec_support,
                                                   wpan_dev[dev_id].pan_id,
                                                   wpan_dev[dev_id].short_id,
                                                   wpan_dev[dev_id].eui64);

        if (frame.mhr_len == 0)
        {
            goto cont;
        }

        fcf = (frame.buf[0] + (frame.buf[1] << 8));
        if (FRAME_CONTAINS_LONGDST(fcf))
        {
            rx_info->short_dst = FALSE;
        }
        else if (FRAME_CONTAINS_SHORTDST(fcf))
        {
            rx_info->short_dst = TRUE;
        }
        
        idx = FRAME_IDX_SRCADDR(fcf);
        if (FRAME_CONTAINS_LONGSRC(fcf))
        {
            srclong = &frame.buf[idx];
        }
        else if (FRAME_CONTAINS_SHORTSRC(fcf))
        {
            srcshort = (frame.buf[idx] + (frame.buf[idx + 1] << 8));
        }

        // Drop duplicate frames.
        if(mac->last_rx_seq == frame.buf[2])
        {
            goto cont;
        }

#ifdef IEEE_802_15_4_RXADDR_LIMIT_M
        if (FRAME_CONTAINS_SHORTSRC(fcf))
        {
#if CONFIG_IEEE_802_15_4_MIN_RXADDR > 0
            if (srcshort < CONFIG_IEEE_802_15_4_MIN_RXADDR)
            {
                goto cont;
            }
#endif

#if CONFIG_IEEE_802_15_4_MAX_RXADDR < 65535
            if (srcshort > CONFIG_IEEE_802_15_4_MAX_RXADDR)
            {
                goto cont;
            }
#endif
        }
#endif

        // Remember last received data packet sequence number.
        mac->last_rx_seq = frame.buf[2];

        if (FRAME_CONTAINS_LONGSRC(fcf))
        {
            rx_info->short_src = FALSE;
            rx_info->src_addr_eui64[0] = srclong[7];
            rx_info->src_addr_eui64[1] = srclong[6];
            rx_info->src_addr_eui64[2] = srclong[5];
            rx_info->src_addr_eui64[3] = srclong[4];
            rx_info->src_addr_eui64[4] = srclong[3];
            rx_info->src_addr_eui64[5] = srclong[2];
            rx_info->src_addr_eui64[6] = srclong[1];
            rx_info->src_addr_eui64[7] = srclong[0];
            idx += 8;
        }
        else if (FRAME_CONTAINS_SHORTSRC(fcf))
        {
            rx_info->short_src = TRUE;
            rx_info->src_addr = srcshort;
            idx += 2;
        }
        
        rx_info->rssi = frame.rssi;
        rx_info->corr = frame.lqi;
        rx_info->payload_length = frame.len - idx;

        if (frame.secured && FRAME_CONTAINS_SECHDR(fcf))
        {
            if (wpan_dev[dev_id].sec_support)
            {
                goto cont;
            }
            else
            {
#ifdef LIBNOS_AES_M
                /*
                 * Decrypt/authenticate the frame by using security utilities
                 * are not provided by IEEE 802.15.4 radio transceivers.
                 */
                if (ieee_802_15_4_unsecure_frame(&frame) < 0)
                {
                    goto cont;
                }
#else
                goto cont;
#endif
            }
        }

        if (FRAME_IS_BEACON(fcf))
        {
            if (nos_mac[dev_id].notify_beacon_reception)
            {
                NOS_EXIT_CRITICAL_SECTION();
                nos_mac[dev_id].notify_beacon_reception(dev_id, &frame);
                NOS_ENTER_CRITICAL_SECTION();
            }

            goto cont;
        }

#ifdef IEEE_802_15_4_FFD
        if (FRAME_IS_MAC_CMD(fcf))
        {
            if (nos_mac[dev_id].notify_mac_command_reception)
            {
                NOS_EXIT_CRITICAL_SECTION();
                nos_mac[dev_id].notify_mac_command_reception(dev_id, &frame);
                NOS_ENTER_CRITICAL_SECTION();
            }

            goto cont;
        }
#endif

        memcpy((void *) rx_info->payload, (void *) &frame.buf[idx],
               rx_info->payload_length);

        result = ERROR_SUCCESS;
    }

    return result;
}

static void nmac_enqueue_rx_frames(UINT8 dev_id)
{
    struct nano_mac_ctrl *mac =
        (struct nano_mac_ctrl *) nos_mac[dev_id].mac_ctrl;
    BOOL recv = FALSE;
    UINT8 idx;

    while (!wpan_dev_rxbuf_is_empty(dev_id))
    {
        if (wpan_dev_read_frame(dev_id, &mac->rx_frame_buf) == ERROR_SUCCESS)
        {
            recv = TRUE;
            if (queue_add(&mac->rxq_ctrl, &idx) == ERROR_SUCCESS)
            {
                mac->rx_frame[idx] = mac->rx_frame_buf;
                // Enqueue success. Go ahead.
            }
            else
            {
                // Queue is full. Stop.
                break;
            }
        }
    }

    // Modem RX buf is empty now.
    if (recv)
        nos_mac[dev_id].notify(dev_id);
}

/**
 * @warn Called by interrupt handler of WPAN devices.
 */
static void nmac_rx_event_handler(UINT8 dev_id, UINT8 event, void *args)
{
    struct nano_mac_ctrl *mac =
        (struct nano_mac_ctrl *) nos_mac[dev_id].mac_ctrl;
    BOOL *stop_interrupt = (BOOL *) args;
    
    nmac_enqueue_rx_frames(dev_id);

    if (stop_interrupt)
        *stop_interrupt = queue_is_full(&mac->rxq_ctrl);
}

static BOOL nmac_unslotted_csma_ca(UINT8 dev_id)
{
    struct nano_mac_ctrl *mac =
        (struct nano_mac_ctrl *) nos_mac[dev_id].mac_ctrl;
    UINT8 foo, nb, be, max_delay = 1;
    
    be = mac->csma_min_be;

    for (foo = be; foo > 0; foo--)
        max_delay *= 2;

    for (nb = 0; nb < mac->csma_retries; nb++)
    {
        foo = rand() % max_delay;
        nos_delay_us(foo * 
                     wpan_dev[dev_id].symbol_duration * 
                     IEEE_802_15_4_UNIT_BACKOFF_PERIOD);

        // handles rx_frames while backoff period.
        nmac_enqueue_rx_frames(dev_id);
        wpan_dev_flush_rxbuf(dev_id);

        if (wpan_dev_cca(dev_id))
        {
            if (wpan_dev_tx(dev_id, mac->tx_buf) == ERROR_SUCCESS)
                return TRUE;
        }

        if (be + 1 <= mac->csma_max_be)
        {
            be++;
            max_delay *= 2;
        }
    }
    return FALSE;
}

ERROR_T nmac_tx_with_retry(UINT8 dev_id, NOS_MAC_TX_INFO* tx_info, UINT8 seq,
                           UINT8 tx_retries)
{
    struct nano_mac_ctrl *mac =
        (struct nano_mac_ctrl *) nos_mac[dev_id].mac_ctrl;
    ERROR_T err = ERROR_802154_TX_FAIL;

#ifdef NANO_MAC_LBT
    if (nmac_lbt_request_channel(TRUE) != ERROR_SUCCESS)
    {
        return ERROR_BUSY;
    }
#endif

    if (wpan_dev_lock(dev_id) != ERROR_SUCCESS)
    {
        return ERROR_BUSY;
    }

    wpan_dev_write_frame(dev_id, mac->tx_buf);

    if (wpan_dev[dev_id].hw_autotx)
    {
        tx_info->tx_count = tx_retries;
        err = wpan_dev_tx(dev_id, mac->tx_buf);
    }
    else
    {
        BOOL ack_required = (tx_retries > 0) ? TRUE : FALSE;
        
        tx_info->tx_count = 0;
        
        if (ack_required)
        {
            do {
                tx_info->tx_count++;
                if (nmac_unslotted_csma_ca(dev_id))
                {
                    if (wpan_dev_wait_ack(dev_id, seq) == ERROR_SUCCESS)
                    {
                        err = ERROR_SUCCESS;
                        break;
                    }
                }
            } while(tx_info->tx_count < 1 + tx_retries);
        }
        else
        {
            tx_info->tx_count++;
            if (nmac_unslotted_csma_ca(dev_id))
            {
                err = ERROR_SUCCESS;
            }
        }
    }

    wpan_dev_release(dev_id);

#ifdef NANO_MAC_LBT
    nmac_lbt_release_channel(err == ERROR_SUCCESS);
#endif

    return err;
}

ERROR_T nmac_tx(UINT8 dev_id, NOS_MAC_TX_INFO* tx_info)
{
    struct nano_mac_ctrl *mac =
        (struct nano_mac_ctrl *) nos_mac[dev_id].mac_ctrl;
    ERROR_T result;
    BOOL ack_required;

    if (!ieee_802_15_4_check_sec_params(tx_info->sec, tx_info->sec_key_idx))
    {
        // Invalid security arguments.
        return ERROR_INVALID_ARGS;
    }
    
    ack_required = (tx_info->dest_addr_eui64 || tx_info->dest_addr != 0xFFFF);
    result = ieee_802_15_4_make_frame(mac->tx_buf,
                                      tx_info,
                                      mac->tx_seq,
                                      wpan_dev[dev_id].pan_id,
                                      ((tx_info->use_short_src ||
                                        !wpan_dev[dev_id].eui64_assigned) ?
                                       NULL : wpan_dev[dev_id].eui64),
                                      ((tx_info->use_short_src) ?
                                       wpan_dev[dev_id].short_id : 0xffff),
                                      ack_required);
    if (result != ERROR_SUCCESS)
    {
        return result;
    }

#ifdef IEEE_802_15_4_SECURITY_M
    if (tx_info->sec != IEEE_802_15_4_SEC_NONE)
    {
        if (!wpan_dev[dev_id].sec_support)
        {
#ifdef LIBNOS_AES_M
            /*
             * Secure the frame by using security utilities are not provided by
             * IEEE 802.15.4 radio transceivers.
             */
            result = ieee_802_15_4_secure_frame(tx_info, ack_required);
            if (result != ERROR_SUCCESS)
            {
                return result;
            }
#else
            // No way to secure the frame.
            return ERROR_802154_SECURING_FAIL;
#endif /* LIBNOS_AES_M */
        }
    }
    else
    {
        tx_info->sec_mic_length = 0;
    }
#else
    // Ignore security level when 802.15.4 security mode is disabled.
    tx_info->sec = IEEE_802_15_4_SEC_NONE;
    tx_info->sec_mic_length = 0;
#endif /* IEEE_802_15_4_SECURITY_M */
    
    if (wpan_dev[dev_id].rx_on == FALSE || mac->sleeping)
    {
        return ERROR_FAIL;
    }

    result = nmac_tx_with_retry(dev_id,
                                tx_info,
                                mac->tx_seq,
                                ((ack_required) ? CONFIG_NANO_MAC_TX_RETRIAL : 0));
    
    ++mac->tx_seq;
    return result;
}

#endif //NANO_MAC_M
