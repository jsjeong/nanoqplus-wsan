// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LBT Protocol NanoMAC Part
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 4. 26.
 */

#include "nano-mac-lbt.h"

#ifdef NANO_MAC_LBT
#include "user_timer.h"
#include "critical_section.h"

enum
{
    LBT_IDLE = 0,
    LBT_TX_SINGLE = 1,
    LBT_TX_DIALOGUE = 2,
    LBT_TX_OFF = 3,
};

enum
{
    LBT_MAX_TX_SINGLE_MS = 1000,
    LBT_MAX_TX_DIALOGUE_MS = 4000,
#ifdef NANO_MAC_LBT_KCC
    LBT_MIN_TX_OFF_MS = 50,
#else
    LBT_MIN_TX_OFF_MS = 100,
#endif
};

extern volatile UINT16 nos_user_timer_expired_bit;

static UINT8 state;
static INT8 timeout = NOS_USER_TIMER_CREATE_ERROR;

void on_timeout(void *args)
{
    nos_user_timer_deactivate(timeout);

    if (state == LBT_TX_SINGLE ||
        state == LBT_TX_DIALOGUE)
    {
        if (nos_user_timer_reschedule_ms(timeout, LBT_MIN_TX_OFF_MS) == TRUE)
        {
            state = LBT_TX_OFF;
            nos_user_timer_activate(timeout);
        }
        else
        {
            state = LBT_IDLE;
        }
    }
    else
    {
        state = LBT_IDLE;
    }
}

void nmac_lbt_init(void)
{
    state = LBT_IDLE;

    NOS_ENTER_CRITICAL_SECTION();
    timeout = nos_user_timer_create_sec(on_timeout, NULL,
                                        nos_user_timer_get_max_sec(),
                                        NOS_USER_TIMER_PERIODIC);
    nos_user_timer_deactivate(timeout);
    NOS_EXIT_CRITICAL_SECTION();
}

ERROR_T nmac_lbt_request_channel(BOOL block)
{
    BOOL rescheduled;
    ERROR_T result;
    
    if (state == LBT_IDLE)
    {
#ifdef NANO_MAC_LBT_KCC
        rescheduled = nos_user_timer_reschedule_ms(timeout, LBT_MAX_TX_DIALOGUE_MS);
#else
        rescheduled = nos_user_timer_reschedule_ms(timeout, LBT_MAX_TX_SINGLE_MS);
#endif
        if (rescheduled == TRUE)
        {
            nos_user_timer_activate(timeout);
#ifdef NANO_MAC_LBT_KCC
            state = LBT_TX_DIALOGUE;
#else
            state = LBT_TX_SINGLE;
#endif
            result = ERROR_SUCCESS;
        }
        else
        {
            // Already expired, so the next state is LBT_TX_OFF.
            result = ERROR_GENERAL_FAILURE;
        }
    }
#ifndef NANO_MAC_LBT_KCC
    else if (state == LBT_TX_SINGLE)
    {
        rescheduled = nos_user_timer_reschedule_ms(timeout, LBT_MAX_TX_DIALOGUE_MS);
        if (rescheduled == TRUE)
        {
            nos_user_timer_activate(timeout);
            state = LBT_TX_DIALOGUE;
            result = ERROR_SUCCESS;
        }
        else
        {
            // Already expired, so the next state is LBT_TX_OFF.
            result = ERROR_GENERAL_FAILURE;
        }
    }
#endif
    else if (state == LBT_TX_DIALOGUE)
    {
        result = ERROR_SUCCESS;
    }
    else //if (state == LBT_TX_OFF)
    {
        if (block)
        {
            // busy wait.
            while(!(_IS_SET(nos_user_timer_expired_bit, timeout) ||
                    state != LBT_TX_OFF));

            /* Case when the timeout timer is expired, but it cannot enter
               on_timeout() function due to currently running in user timer or
               task. The nos_user_timer_expired_bit should be cleared manually.
            */
            if (state == LBT_TX_OFF)
            {
                _BIT_CLR(nos_user_timer_expired_bit, timeout);
                nos_user_timer_deactivate(timeout);
                state = LBT_IDLE;
            }
            result = ERROR_SUCCESS;
        }
        else
        {
            result = ERROR_BUSY;
        }
    }
    
    return result;
}

ERROR_T nmac_lbt_release_channel(BOOL accessed)
{
    if (state == LBT_TX_SINGLE ||
        state == LBT_TX_DIALOGUE)
    {
        if (accessed == FALSE)
        {
            // End of polling sequence
            if (nos_user_timer_reschedule_ms(timeout, LBT_MIN_TX_OFF_MS) == TRUE)
            {
                nos_user_timer_activate(timeout);
            }
        }
        
        return ERROR_SUCCESS;
    }
    else
    {
        return ERROR_GENERAL_FAILURE;
    }
}

#endif //NANO_MAC_LBT
