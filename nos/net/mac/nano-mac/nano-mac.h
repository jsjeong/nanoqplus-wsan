// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#ifndef NANO_MAC_H
#define NANO_MAC_H

#include "nos_common.h"
#include "ieee-802-15-4.h"
#include "queue.h"

enum
{
    NMAC_RXQ_LEN = 4,
};

struct nano_mac_ctrl
{
    BOOL sleeping:1;
    UINT8 tx_seq;                     /* Transmitted frame sequence number */
    UINT8 last_rx_seq;                /* Recently received frame sequence number
                                         (to distinguish duplicate frame) */
    UINT32 sec_frame_counter;

    UINT8 csma_retries;
    UINT8 csma_min_be;
    UINT8 csma_max_be;

    UINT8 tx_buf[128];
    struct queue rxq_ctrl;
    struct wpan_rx_frame rx_frame[NMAC_RXQ_LEN];
    struct wpan_rx_frame rx_frame_buf;
};
typedef struct nano_mac_ctrl NANO_MAC;

void nmac_init(UINT8 dev_id, UINT16 panid, UINT16 shortid, const UINT8 *longid);

void nmac_set_csma(UINT8 dev_id, UINT8 retries,UINT8 max_be,UINT8 min_be);

ERROR_T nmac_rx(UINT8 dev_id, NOS_MAC_RX_INFO* rx_info);

ERROR_T nmac_tx(UINT8 dev_id, NOS_MAC_TX_INFO* tx_info);

ERROR_T nmac_tx_with_retry(UINT8 dev_id, NOS_MAC_TX_INFO* tx_info, UINT8 seq, UINT8 tx_retries);

UINT8 nmac_get_max_tx_payload_len(UINT8 dev_id, BOOL da_eui64, UINT8 sec);

#endif // ~NANO_MAC_H
