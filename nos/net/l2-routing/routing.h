/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef ROUTING_H
#define ROUTING_H
#include "kconf.h"
#ifdef MESH_ROUTING_M

//#include "nos_common.h"
#include "mac.h"
#define nos_nwk_get_opmode()    nos_mac_get_opmode()
#define nos_nwk_get_pan_id()    nos_mac_get_pan_id()
#define nos_nwk_get_short_id()  nos_mac_get_short_id()

#ifdef RENO_M
#include "reno.h"

enum { ROUTING_HEADER_SIZE = RENO_HEADER_SIZE };
enum { NOS_NWK_MAX_PAYLOAD_SIZE = RENO_MAX_PACKET_PAYLOAD_SIZE };

#define nos_nwk_init(panid,id,cb_func_ptr)              reno_init(panid,id,cb_func_ptr)

#define nos_nwk_rx(src_id, port, data_len, data_ptr)    reno_recv_from_nwk(src_id, port, data_len, data_ptr)
#define nos_nwk_tx(dest_id, port, data_len, data_ptr)   reno_send_to_nwk(dest_id, port, data_len, data_ptr)
#define nos_nwk_set_rx_cb(func)                         reno_set_rx_cb(func)
#endif

#ifdef TENO_M
#include "teno.h"
enum { NOS_NWK_MAX_PAYLOAD_SIZE = TENO_MAX_PACKET_PAYLOAD_SIZE };
#define nos_nwk_init(panid,id,cb_func_ptr)                                  teno_init(panid,id,cb_func_ptr) // void teno_init(UINT8 channel, UINT16 pan_id, UINT16 node_id, void (*func)(void));
#define nos_nwk_set_to_sink()                                               teno_role_as_sink()
#define nos_nwk_set_to_node()                                               teno_role_as_node()
#define nos_nwk_rx(src_id, port, data_len, data_ptr, parent_id, dest_id)    teno_recv_from_nwk(src_id, port, data_len, data_ptr, parent_id, dest_id) //void teno_recv_from_nwk(UINT16* src_id, UINT8* port, UINT8* data_length, void* data, UINT16* parent_id, UINT16* dest_id);
#define nos_nwk_tx_to_sink(port, data_len, data_ptr)                        teno_send_to_sink(port, data_len, data_ptr) //BOOL teno_send_to_sink(UINT8 port, UINT8 data_length, void* data);
#define nos_nwk_tx(dest_id, port, data_len, data_ptr)                       teno_send_to_node(dest_id, port, data_len, data_ptr) //BOOL teno_send_to_node(UINT16 dest_id, UINT8 port, UINT8 data_length, void* data);
#define nos_nwk_set_rx_cb(func)                                             teno_set_rx_cb(func)
#endif

#endif //--- MESH_ROUTING_M ---//
#endif //--- ~ROUTING_H ---//
//=========================== End of (routing.h) ============================

