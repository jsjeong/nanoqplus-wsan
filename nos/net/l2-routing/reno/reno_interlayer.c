/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "reno.h"
#ifdef RENO_M
#include <string.h>
#include "mac.h"
#include "critical_section.h"
#include "taskq.h"

NOS_MAC_TX_INFO _reno_mac_tx_info;
NOS_MAC_RX_INFO _reno_mac_rx_info;
RENO_PACKET _reno_rx_packet;


extern void reno_rx_packet_handler(RENO_PACKET* rx_packet_ptr, UINT16 prev_hop_addr);


//=======================================================================================
//  inter-layer operation functions (Network <-> MAC)
//=======================================================================================  

void reno_recv_from_mac(void *args)
{
    UINT8 i;
    while ( nos_mac_rx( &_reno_mac_rx_info) )
    {
        i = 0;
        if (_reno_mac_rx_info.payload_length < RENO_HEADER_SIZE)
            continue;
        _reno_rx_packet.payload_size = _reno_mac_rx_info.payload_length - RENO_HEADER_SIZE;
        _reno_rx_packet.nwk_header.msg_type = ((UINT8*)_reno_mac_rx_info.payload)[i++];
        _reno_rx_packet.nwk_header.msg_hop_limit = ((UINT8*)_reno_mac_rx_info.payload)[i++];
        _reno_rx_packet.nwk_header.msg_hop_count = ((UINT8*)_reno_mac_rx_info.payload)[i++];
        if (_reno_rx_packet.nwk_header.msg_hop_count > _reno_rx_packet.nwk_header.msg_hop_limit)
        {
            continue;
        }
        _reno_rx_packet.nwk_header.dest_addr_l = ((UINT8*)_reno_mac_rx_info.payload)[i++];
        _reno_rx_packet.nwk_header.dest_addr_h = ((UINT8*)_reno_mac_rx_info.payload)[i++];
        _reno_rx_packet.nwk_header.src_addr_l = ((UINT8*)_reno_mac_rx_info.payload)[i++];
        _reno_rx_packet.nwk_header.src_addr_h = ((UINT8*)_reno_mac_rx_info.payload)[i++];
        _reno_rx_packet.nwk_header.var = ((UINT8*)_reno_mac_rx_info.payload)[i++];
#ifdef NANOMON
        _reno_rx_packet.nwk_header.parent_addr_l = ((UINT8*)_reno_mac_rx_info.payload)[i++];
        _reno_rx_packet.nwk_header.parent_addr_h = ((UINT8*)_reno_mac_rx_info.payload)[i++];
#endif
        _reno_rx_packet.payload = &(((UINT8*)_reno_mac_rx_info.payload)[i++]);

        reno_rx_packet_handler(&_reno_rx_packet, _reno_mac_rx_info.src_addr);
    }
}

BOOL reno_send_to_mac(RENO_PACKET *packet_info_ptr, UINT16 dest_addr)
{
    NOS_ENTER_CRITICAL_SECTION();
    ++packet_info_ptr->nwk_header.msg_hop_count;
    _reno_mac_tx_info.frame_type = IEEE_802_15_4_FRAME_DATA;
    _reno_mac_tx_info.dest_addr = dest_addr;
    _reno_mac_tx_info.use_short_src = TRUE;

    memcpy(_reno_mac_tx_info.payload, &packet_info_ptr->nwk_header, RENO_HEADER_SIZE);
    memcpy(&_reno_mac_tx_info.payload[RENO_HEADER_SIZE], packet_info_ptr->payload, packet_info_ptr->payload_size);
    _reno_mac_tx_info.payload_length = RENO_HEADER_SIZE + packet_info_ptr->payload_size;

    if (nos_mac_tx(&_reno_mac_tx_info) == ERROR_SUCCESS)
    {
        NOS_EXIT_CRITICAL_SECTION();
        return TRUE;
    }
    else
    {
        NOS_EXIT_CRITICAL_SECTION();
        return FALSE;
    }
}

// this will be called when there are unreaded frames in RXFIFO of MAC layer
void reno_cb_from_mac(void)
{
    nos_taskq_reg(reno_recv_from_mac, NULL);
}

void reno_mac_init(UINT16 pan_id, UINT16 short_id, UINT8 *ext_id)
{
    nos_mac_init(pan_id, short_id, ext_id);
    nos_mac_set_rx_cb( reno_cb_from_mac );
}
#endif //--- RENO_M ---//

