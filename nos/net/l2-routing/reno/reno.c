/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @date 2006. 9. 1.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "reno.h"

#ifdef RENO_M

//#define RENO_DEBUG
#ifdef RENO_DEBUG
#include "uart.h"
#include "platform.h"
#endif

#include <stdlib.h>
#include <string.h>
#include "critical_section.h"
#include "sched.h"
#include "thread.h"
#include "nanomon_app.h"


#ifdef STATIC_MAC_INFO_M
#include <avr/eeprom.h>
#endif

#ifdef FOTA_M
#include "fota.h"
#ifndef RENO_DEBUG
#include "uart.h"
#include "platform.h"
#endif // ~RENO_DEBUG
#endif // ~FOTA_M

#define RENO_INVALID_PATH (0)
#define RENO_FREP_PATH  (1)
#define RENO_RREQ_PATH  (2)
#define RENO_RREP_PATH  (3)
#define RENO_DATA_PATH  (4)
#define RENO_TABLE_LOOKUP_FAIL  (0xFF)
#define RENO_TXQ_LENGTH    (256) // must not be changed


// For TX messages
struct _reno_tx_queue_
{
    UINT8 front, rear;
    UINT8 fifo[RENO_TXQ_LENGTH];
} _reno_txq;

// For a RX message
struct _reno_rx_info_
{
    UINT16 address;
#ifdef NANOMON
    UINT16 parent_address;
#endif
    UINT8 port;
    UINT8 payload_len;
    void* payload;
} _reno_rx_info;


RENO_NIB  _reno_nib;

void (*reno_rx_callback)(void);
RENO_ROUTE_ENTRY new_entry;                             // for reno_rx_packet_handler(). stores new route information
RENO_PACKET _reno_ctrl_msg_tx_packet;                   // for RREQ, RREP, RERR, FREP
RENO_PACKET _reno_tx_info;                              //used for sending DATA packet in TXQ
UINT8 _reno_tx_payload[RENO_MAX_PACKET_PAYLOAD_SIZE];   //used for sending DATA packet in TXQ
UINT8 route_trace_thread_id;

#define RENO_TXQ_IS_EMPTY()   ( (_reno_txq.front == _reno_txq.rear) ? (TRUE) : (FALSE))
#define RENO_TXQ_VACANT_SIZE()  ( (UINT8)(_reno_txq.rear - _reno_txq.front - 1) )
#define RENO_TXQ_DATA_SIZE()   ( (UINT8)(_reno_txq.front - _reno_txq.rear) )
#define RENO_PENDING_TX_DEST_ADDR() ( (UINT16)_reno_tx_info.nwk_header.dest_addr_h << 8 | _reno_tx_info.nwk_header.dest_addr_l )


void reno_route_trace_task(void* args);
BOOL reno_tx_pkt_enqueue(RENO_PACKET *tx_pkt_ptr);
BOOL reno_tx_pkt_dequeue(RENO_PACKET *pkt_ptr);
void reno_rx_packet_handler(RENO_PACKET* rx_packet_ptr, UINT16 prev_hop_addr);
UINT8 reno_route_table_lookup(UINT16 address);
BOOL reno_is_new_route(UINT8 table_index, UINT8 route_seq, UINT8 hops, UINT8 prior);
void reno_route_table_insert_entry(RENO_ROUTE_ENTRY* new_entry_ptr, UINT8 index);
void reno_route_table_delete_destination(UINT16 dest_address);
void reno_send_rreq(UINT16 dest_addr);
void reno_send_rrep(UINT16 dest_addr, UINT16 next_addr);
void reno_send_frep(UINT16 dest_addr, UINT16 src_addr, UINT8 hops, UINT8 seq);
void reno_send_rerr(UINT16 dest_addr, UINT16 src_addr, UINT16 next_addr);
#ifdef RENO_DEBUG
void reno_print_route_table(void);
void reno_print_pkt_info(RENO_PACKET* pkt_ptr);
#endif

//interlayer.c
extern void reno_mac_init(UINT16 pan_id, UINT16 short_id, UINT8 *ext_id);
extern BOOL reno_send_to_mac(RENO_PACKET *packet_info_ptr, UINT16 dest_addr);

// Initializing  [ Channel:0x0B~0x1A (11~26), PAN ID : 0x0000~0xfffE(0~65534), Node ID(MAC short address) : 0x0000~0xfffE(0~65534)]
void reno_init(UINT16 pan_id, UINT16 short_id, void (*rx_callback)(void))
{
    UINT8 i;

    _reno_tx_info.payload = (void*)_reno_tx_payload;

    // Sets routing table
    for (i = 0; i < RENO_TABLE_SIZE; ++i)
    {
        _reno_nib.route_table[i].dest_addr = 0xFFFF;
        _reno_nib.route_table[i].state = RENO_INVALID_PATH;
    }

    // Initializes RENO TX queue
    _reno_txq.front = 0;
    _reno_txq.rear = 0;

    // Create a thread for looking for route
    route_trace_thread_id = nos_thread_create(reno_route_trace_task, NULL, 0, NOS_PRIORITY_HIGHEST);
    nos_thread_suspend(route_trace_thread_id);

    // callback function will be called whenever a packet is received.
    reno_set_rx_cb(rx_callback);

    // MAC layer init
    reno_mac_init(pan_id, short_id, NULL);

    // Sets network information base
    _reno_nib.my_addr = nos_mac_get_short_id();

    _reno_nib.next_index = 0;
    _reno_nib.route_seq_num = (rand() % 255) * (_reno_nib.my_addr % 255);

#ifdef FOTA_M
    mac_rf_channel = nos_mac_get_opmode();
    mac_pan_id = nos_mac_get_pan_id();
#endif /* FOTA_M */

}


void reno_set_rx_cb(void (*func)(void))
{
    reno_rx_callback = func;
}

//=======================================================================================
//  inter-layer operation functions (Application <-> Network)
//=======================================================================================  
// Function for TX
UINT8 reno_send_to_nwk(UINT16 dest_id, UINT8 port, UINT8 data_length, void* tx_string)
{
    UINT8 table_index;
    RENO_PACKET tx_info;

#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend to nwk");
#endif
    tx_info.nwk_header.msg_type = RENO_DATA_MSG;
    tx_info.nwk_header.msg_hop_limit = RENO_MAX_HOP_LIMIT;
    tx_info.nwk_header.msg_hop_count = 0;
    tx_info.nwk_header.dest_addr_l = (UINT8)dest_id;
    tx_info.nwk_header.dest_addr_h = (UINT8)(dest_id >> 8);
    tx_info.nwk_header.src_addr_l = (UINT8)_reno_nib.my_addr;
    tx_info.nwk_header.src_addr_h = (UINT8)(_reno_nib.my_addr >> 8);
    tx_info.nwk_header.var = port;
    tx_info.payload_size = data_length;
    tx_info.payload = tx_string;

    NOS_ENTER_CRITICAL_SECTION();
    table_index = reno_route_table_lookup(dest_id);
    if (table_index != RENO_TABLE_LOOKUP_FAIL)
    {
        if ( reno_send_to_mac( &tx_info, _reno_nib.route_table[table_index].next_hop_addr ) )
        {
#ifdef RENO_DEBUG
            nos_uart_puts(STDIO, "\n\rsend to nwk - success");
#endif
            NOS_EXIT_CRITICAL_SECTION();
            return RENO_TX_SUCCEEDED;
        }
        else
        {
#ifdef RENO_DEBUG
            nos_uart_puts(STDIO, "\n\rsend to nwk - fail. delete table.");
#endif
            // TX fail! delete entry.
            reno_route_table_delete_destination(dest_id);
        }
    }
    // store in TX queue
    if (reno_tx_pkt_enqueue(&tx_info))
    {
        NOS_EXIT_CRITICAL_SECTION();
        return RENO_TX_DELAYED;
    }
    else
    {
        NOS_EXIT_CRITICAL_SECTION();
        return RENO_TX_FAILED;
    }  
}



// For RX
void reno_recv_from_nwk(UINT16* src_id, UINT8* port, UINT8* data_length, void* rx_string)
{
#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rrecv_from_nwk");
#endif
    NOS_ENTER_CRITICAL_SECTION();
    *src_id = _reno_rx_info.address;
    *port = _reno_rx_info.port;
    *data_length = _reno_rx_info.payload_len;
    memcpy(rx_string, _reno_rx_info.payload, _reno_rx_info.payload_len);
    NOS_EXIT_CRITICAL_SECTION();
}



// handle TXQ. find a new route for a pending msg. And then send it or drop it.
void reno_route_trace_task(void* args)
{
    UINT8 index;
    UINT8 i;

    while(TRUE)
    {
        // if TXQ is empty.
        if (!reno_tx_pkt_dequeue(&_reno_tx_info))
        {
#ifdef RENO_DEBUG
            nos_uart_puts(STDIO, "\n\rroute_trace task: suspended");
#endif
            // route trace task stop (self-stop)
            nos_thread_suspend(route_trace_thread_id);
        }
        else
        {
            for (i=1; i<= RENO_ROUTE_TRACE_MAX_TRIAL; ++i)
            {
                NOS_ENTER_CRITICAL_SECTION();
                index = reno_route_table_lookup(RENO_PENDING_TX_DEST_ADDR());
                if (index != RENO_TABLE_LOOKUP_FAIL)
                {
                    NOS_EXIT_CRITICAL_SECTION();
                    break;
                }
                else
                {
                    NOS_EXIT_CRITICAL_SECTION();
                    reno_send_rreq(RENO_PENDING_TX_DEST_ADDR());
#ifdef RENO_DEBUG
                    nos_uart_puts(STDIO, "\n\rtrying to find a new route");
#endif
                    if (i == 1 || i == 2)
                        nos_thread_sleep_ms(RENO_ROUTE_TRACE_DELAY_MS);
                    else
                        nos_thread_sleep_ms(RENO_ROUTE_TRACE_DELAY_MS*i*i);
                    NOS_ENTER_CRITICAL_SECTION();
                    index = reno_route_table_lookup(RENO_PENDING_TX_DEST_ADDR());
                    if (index != RENO_TABLE_LOOKUP_FAIL)
                    { 
#ifdef RENO_DEBUG
                        nos_uart_puts(STDIO, "\n\rroute has been found");
#endif
                        reno_send_to_mac(&_reno_tx_info, _reno_nib.route_table[index].next_hop_addr);
                        NOS_EXIT_CRITICAL_SECTION();
                        break;
                    }
                    else
                    {
                        NOS_EXIT_CRITICAL_SECTION();
                    }
                }
            }
        }
    }
}



// return a packet insertion is succeeded or not
// manage route trace thread also.
BOOL reno_tx_pkt_enqueue(RENO_PACKET *tx_pkt_ptr)
{
    UINT8 front_forward_vacant_part_size;
    BOOL was_empty;

    NOS_ENTER_CRITICAL_SECTION();
    if ( RENO_TXQ_VACANT_SIZE() < RENO_HEADER_SIZE + 1 + tx_pkt_ptr->payload_size )
    {
        NOS_EXIT_CRITICAL_SECTION();
        return FALSE;
    }

#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rtx packet enqueue");
#endif

    if ( RENO_TXQ_IS_EMPTY() )
        was_empty = TRUE;
    else
        was_empty = FALSE;

    front_forward_vacant_part_size = RENO_TXQ_LENGTH - _reno_txq.front;
    if (front_forward_vacant_part_size < RENO_HEADER_SIZE + 1)
    {
        //cut header
        memcpy( &(_reno_txq.fifo[_reno_txq.front]), tx_pkt_ptr, front_forward_vacant_part_size);
        memcpy( _reno_txq.fifo, (UINT8*)tx_pkt_ptr + front_forward_vacant_part_size, RENO_HEADER_SIZE + 1-front_forward_vacant_part_size);
        _reno_txq.front += RENO_HEADER_SIZE + 1;
        memcpy( &(_reno_txq.fifo[_reno_txq.front]), tx_pkt_ptr->payload, tx_pkt_ptr->payload_size);
        _reno_txq.front += tx_pkt_ptr->payload_size;
    }
    else if (front_forward_vacant_part_size == RENO_HEADER_SIZE + 1)
    {
        memcpy( &(_reno_txq.fifo[_reno_txq.front]), tx_pkt_ptr, RENO_HEADER_SIZE + 1);
        memcpy( _reno_txq.fifo, tx_pkt_ptr->payload, tx_pkt_ptr->payload_size);
        _reno_txq.front = tx_pkt_ptr->payload_size;
    }
    else if (front_forward_vacant_part_size < RENO_HEADER_SIZE + 1 + tx_pkt_ptr->payload_size)
    {
        // cut payload
        memcpy( &(_reno_txq.fifo[_reno_txq.front]), tx_pkt_ptr, RENO_HEADER_SIZE + 1);
        _reno_txq.front += RENO_HEADER_SIZE + 1;
        memcpy( &(_reno_txq.fifo[_reno_txq.front]), tx_pkt_ptr->payload, front_forward_vacant_part_size - (RENO_HEADER_SIZE + 1));
        memcpy( _reno_txq.fifo, (UINT8*)tx_pkt_ptr->payload + front_forward_vacant_part_size - (RENO_HEADER_SIZE + 1), tx_pkt_ptr->payload_size - (front_forward_vacant_part_size - (RENO_HEADER_SIZE + 1)));
        _reno_txq.front += tx_pkt_ptr->payload_size; 
    }
    else
    {
        memcpy( &(_reno_txq.fifo[_reno_txq.front]), tx_pkt_ptr, RENO_HEADER_SIZE + 1);
        _reno_txq.front += RENO_HEADER_SIZE + 1;
        memcpy( &(_reno_txq.fifo[_reno_txq.front]), tx_pkt_ptr->payload, tx_pkt_ptr->payload_size);
        _reno_txq.front += tx_pkt_ptr->payload_size;
    }

    NOS_EXIT_CRITICAL_SECTION();
    // route trace task run if TXQ was empty.
    if (was_empty)
    {
        nos_thread_resume(route_trace_thread_id);
    }

#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rtx packet enqueue:out");
#endif

    return TRUE;
}



// called by route trace task only
BOOL reno_tx_pkt_dequeue(RENO_PACKET *pkt_ptr)
{
    UINT8 first_data_part_size;

    if (RENO_TXQ_IS_EMPTY())
    {
        //dummy dest_address
        pkt_ptr->nwk_header.dest_addr_h = 0xFF;
        pkt_ptr->nwk_header.dest_addr_l = 0xFF;  
        return FALSE;
    }

    NOS_ENTER_CRITICAL_SECTION();
    pkt_ptr->payload_size = _reno_txq.fifo[_reno_txq.rear++];
    if ( pkt_ptr->payload_size > RENO_MAX_PACKET_PAYLOAD_SIZE || RENO_TXQ_DATA_SIZE() < RENO_HEADER_SIZE )
    {
        _reno_txq.front = 0;
        _reno_txq.rear = 0;
#ifdef RENO_DEBUG
        nos_uart_puts(STDIO, "\n\rTX queue reset!! ");
        while(1);
#endif    
        NOS_EXIT_CRITICAL_SECTION();
        return FALSE;
    }

#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rtx packet dequeue");
#endif

    first_data_part_size = RENO_TXQ_LENGTH - _reno_txq.rear;
    if (RENO_HEADER_SIZE > first_data_part_size)
    {
        //cut header
        memcpy( &(pkt_ptr->nwk_header), &(_reno_txq.fifo[_reno_txq.rear]), first_data_part_size);
        memcpy( ((UINT8*)&(pkt_ptr->nwk_header))+first_data_part_size, _reno_txq.fifo, RENO_HEADER_SIZE - first_data_part_size);
        _reno_txq.rear += RENO_HEADER_SIZE;
        memcpy( pkt_ptr->payload, &(_reno_txq.fifo[_reno_txq.rear]), pkt_ptr->payload_size);
        _reno_txq.rear += pkt_ptr->payload_size;

    }
    else if (RENO_HEADER_SIZE == first_data_part_size)
    {
        memcpy( &(pkt_ptr->nwk_header), &(_reno_txq.fifo[_reno_txq.rear]), RENO_HEADER_SIZE);
        memcpy( pkt_ptr->payload, _reno_txq.fifo, pkt_ptr->payload_size);
        _reno_txq.rear = pkt_ptr->payload_size;
    }
    else if (RENO_HEADER_SIZE + pkt_ptr->payload_size > first_data_part_size)
    {
        //cut payload
        memcpy( &(pkt_ptr->nwk_header), &(_reno_txq.fifo[_reno_txq.rear]), RENO_HEADER_SIZE);
        _reno_txq.rear += RENO_HEADER_SIZE;
        memcpy( pkt_ptr->payload, &(_reno_txq.fifo[_reno_txq.rear]), first_data_part_size - RENO_HEADER_SIZE );
        memcpy( (UINT8*)pkt_ptr->payload + first_data_part_size - RENO_HEADER_SIZE, _reno_txq.fifo, pkt_ptr->payload_size - (first_data_part_size - RENO_HEADER_SIZE) );
        _reno_txq.rear += pkt_ptr->payload_size;
    }
    else
    {
        memcpy( &(pkt_ptr->nwk_header), &(_reno_txq.fifo[_reno_txq.rear]), RENO_HEADER_SIZE);
        _reno_txq.rear += RENO_HEADER_SIZE;
        memcpy( pkt_ptr->payload, &(_reno_txq.fifo[_reno_txq.rear]), pkt_ptr->payload_size);
        _reno_txq.rear += pkt_ptr->payload_size;
    }
    NOS_EXIT_CRITICAL_SECTION();
    return TRUE;
}



// NO reentrance. called by kernel thread.
void reno_rx_packet_handler(RENO_PACKET* rx_packet_ptr, UINT16 prev_hop_addr)
{
    UINT8 table_index;
    UINT16 dest_addr, src_addr;

#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rrx packet handler");
#endif

    dest_addr = (UINT16)(rx_packet_ptr->nwk_header.dest_addr_h << 8) + rx_packet_ptr->nwk_header.dest_addr_l;
    src_addr = (UINT16)(rx_packet_ptr->nwk_header.src_addr_h << 8) + rx_packet_ptr->nwk_header.src_addr_l;

    if (rx_packet_ptr->nwk_header.msg_type == RENO_DATA_MSG)
    {
#ifdef RENO_DEBUG
        nos_uart_puts(STDIO, "\n\rDATA msg");
#endif
        NOS_ENTER_CRITICAL_SECTION();
        table_index = reno_route_table_lookup(src_addr);
        if (table_index == RENO_TABLE_LOOKUP_FAIL)
        {
            // insert route
            new_entry.dest_addr = src_addr;
            new_entry.next_hop_addr = prev_hop_addr;
            new_entry.state = RENO_DATA_PATH;
            new_entry.seq = 0;
            new_entry.hops_to_dest = rx_packet_ptr->nwk_header.msg_hop_count;
            reno_route_table_insert_entry(&new_entry, table_index);
        }
        NOS_EXIT_CRITICAL_SECTION();

        if (dest_addr == _reno_nib.my_addr)
        {
#ifdef FOTA_M
            //nos_uart_puts(STDIO, "\n\rbefore fota_check()");
            // Check whether it is FOTA packet or not.
            fota_check(mac_rf_channel, mac_pan_id, _reno_nib.my_addr, 
                    src_addr, prev_hop_addr, rx_packet_ptr->payload);
#endif //~FOTA_M
            _reno_rx_info.address = src_addr;
            _reno_rx_info.port = rx_packet_ptr->nwk_header.var;
            _reno_rx_info.payload_len = rx_packet_ptr->payload_size;
            _reno_rx_info.payload = rx_packet_ptr->payload;
            if (reno_rx_callback != NULL)
                reno_rx_callback();
#ifdef RENO_DEBUG
            else
                nos_uart_puts(STDIO, "\n\rrx callback fucntion was not registered.");
#endif
        }
        else
        {
            NOS_ENTER_CRITICAL_SECTION();
            table_index = reno_route_table_lookup(dest_addr);
            if (table_index != RENO_TABLE_LOOKUP_FAIL)
            {  
                if ( reno_send_to_mac( rx_packet_ptr, _reno_nib.route_table[table_index].next_hop_addr ) )
                {
                    NOS_EXIT_CRITICAL_SECTION();
                    return;
                }
                else
                {
                    reno_route_table_delete_destination(dest_addr);
                    // delete entries from route table
                    //reno_route_table_delete_neighbors(_reno_nib.route_table[table_index].next_hop_addr);
                }
            }
            NOS_EXIT_CRITICAL_SECTION();

            // send RERR to source
            reno_send_rerr(src_addr, dest_addr, prev_hop_addr);
            // this is an option
            reno_tx_pkt_enqueue(rx_packet_ptr);
        }
    }


    else if (rx_packet_ptr->nwk_header.msg_type == RENO_RREQ_MSG)
    {
#ifdef RENO_DEBUG
        nos_uart_puts(STDIO, "\n\rRREQ msg");
#endif
        // check source address
        if (src_addr != _reno_nib.my_addr)
        {
            NOS_ENTER_CRITICAL_SECTION();
            // check if it is a new route or not
            table_index = reno_route_table_lookup(src_addr);
            if (reno_is_new_route(table_index, rx_packet_ptr->nwk_header.var, rx_packet_ptr->nwk_header.msg_hop_count, RENO_RREQ_PATH))
            {
                // insert a new route for souce node
                new_entry.dest_addr = src_addr;
                new_entry.next_hop_addr = prev_hop_addr;
                new_entry.state = RENO_RREQ_PATH;
                new_entry.seq = rx_packet_ptr->nwk_header.var;
                new_entry.hops_to_dest = rx_packet_ptr->nwk_header.msg_hop_count;
                reno_route_table_insert_entry(&new_entry, table_index);

                if (dest_addr == _reno_nib.my_addr)
                {
                    reno_send_rrep(src_addr, prev_hop_addr);
                }
                else
                {
                    // send FREP if ...
                    table_index = reno_route_table_lookup(dest_addr);
                    if (table_index != RENO_TABLE_LOOKUP_FAIL 
                            && prev_hop_addr == src_addr 
                            && prev_hop_addr != _reno_nib.route_table[table_index].next_hop_addr 
                            && _reno_nib.route_table[table_index].state != RENO_INVALID_PATH)
                    {
                        reno_send_frep(prev_hop_addr, dest_addr, _reno_nib.route_table[table_index].hops_to_dest, _reno_nib.route_table[table_index].seq);
                    }
                    //forwarding
                    reno_send_to_mac(rx_packet_ptr, 0xFFFF);
                }
            }
            NOS_EXIT_CRITICAL_SECTION();
        }
    }


    else if (rx_packet_ptr->nwk_header.msg_type == RENO_RREP_MSG)
    {
#ifdef RENO_DEBUG
        nos_uart_puts(STDIO, "\n\rRREP msg");
#endif
        NOS_ENTER_CRITICAL_SECTION();
        // if the route to the RREP's source is not valid, insert a new route.
        table_index = reno_route_table_lookup(src_addr);
        if (reno_is_new_route(table_index, rx_packet_ptr->nwk_header.var, rx_packet_ptr->nwk_header.msg_hop_count, RENO_RREP_PATH))
        {
            // insert route
            new_entry.dest_addr = src_addr;
            new_entry.next_hop_addr = prev_hop_addr;
            new_entry.state = RENO_RREP_PATH;
            new_entry.seq = rx_packet_ptr->nwk_header.var;
            new_entry.hops_to_dest = rx_packet_ptr->nwk_header.msg_hop_count;
            reno_route_table_insert_entry(&new_entry, table_index);
        }
        // forwards next node if i am not the destination of the RREP
        if (dest_addr != _reno_nib.my_addr)
        {
            table_index = reno_route_table_lookup(dest_addr);
            reno_send_to_mac(rx_packet_ptr, _reno_nib.route_table[table_index].next_hop_addr);   
        }
        NOS_EXIT_CRITICAL_SECTION();
    }


    else if (rx_packet_ptr->nwk_header.msg_type == RENO_FREP_MSG)
    {
#ifdef RENO_DEBUG
        nos_uart_puts(STDIO, "\n\rFREP msg");
#endif
        NOS_ENTER_CRITICAL_SECTION();
        // if the route to the RREP's source is not valid, insert a new route.
        table_index = reno_route_table_lookup(src_addr);
        if (reno_is_new_route(table_index, rx_packet_ptr->nwk_header.var, rx_packet_ptr->nwk_header.msg_hop_count, RENO_FREP_PATH))
        {
            // insert route
            new_entry.dest_addr = src_addr;
            new_entry.next_hop_addr = prev_hop_addr;
            new_entry.state = RENO_FREP_PATH;
            new_entry.seq = rx_packet_ptr->nwk_header.var;
            new_entry.hops_to_dest = rx_packet_ptr->nwk_header.msg_hop_count;
            reno_route_table_insert_entry(&new_entry, table_index);
        }
        NOS_EXIT_CRITICAL_SECTION();
    }


    else if (rx_packet_ptr->nwk_header.msg_type == RENO_RERR_MSG)
    {
#ifdef RENO_DEBUG
        nos_uart_puts(STDIO, "\n\rRERR msg");
#endif
        NOS_ENTER_CRITICAL_SECTION();
        //delete route entry (destination match)
        reno_route_table_delete_destination(src_addr);
        if (dest_addr != _reno_nib.my_addr)
        {
            // forward
            table_index = reno_route_table_lookup(dest_addr);
            reno_send_to_mac( rx_packet_ptr, _reno_nib.route_table[table_index].next_hop_addr );
        }
        NOS_EXIT_CRITICAL_SECTION();  
    }
#ifdef RENO_DEBUG
    else
        nos_uart_puts(STDIO, "\n\rrx packet type error.");
#endif
}



// Searches a destination node in the routing table and returns the position in the routing table for it.
// If not found, returns RENO_TABLE_LOOKUP_FAIL
UINT8 reno_route_table_lookup(UINT16 address)
{
    UINT8 i;

    for ( i=0; i< RENO_TABLE_SIZE; ++i)
    {
        if ( _reno_nib.route_table[i].dest_addr == address &&
                _reno_nib.route_table[i].state != RENO_INVALID_PATH )
        {
#ifdef RENO_DEBUG
            nos_uart_puts(STDIO, "\n\rlook up success");
#endif
            return i;
        }
    }
#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rlook up fail");
#endif
    return RENO_TABLE_LOOKUP_FAIL;
}



// MUST be called in CRITICAL_SECTION
BOOL reno_is_new_route(UINT8 table_index, UINT8 route_seq, UINT8 hops, UINT8 prior)
{
    if (table_index == RENO_TABLE_LOOKUP_FAIL || _reno_nib.route_table[table_index].seq != route_seq)
        return TRUE;
    else
    {
        if (_reno_nib.route_table[table_index].state < prior)
            return TRUE;
        else if (_reno_nib.route_table[table_index].state == prior)
        {
            if (_reno_nib.route_table[table_index].hops_to_dest <= hops)
                return FALSE;
            else
                return TRUE;
        }
        else //if (_reno_nib.route_table[table_index].state > prior)
            return FALSE;
    }
}



// MUST be called in CRITICAL_SECTION 
void reno_route_table_insert_entry(RENO_ROUTE_ENTRY* new_entry_ptr, UINT8 index)
{
    UINT8 i;
#ifdef MON_INSTANT_ROUTE_INFO
    // send a new route information to sink
    if (new_entry_ptr->dest_addr == NANOMON_SINK_ADDR && new_entry_ptr->next_hop_addr != _reno_prev_parent_addr)
    {
        _reno_prev_parent_addr = new_entry_ptr->next_hop_addr;
        _reno_ctrl_msg_tx_packet.nwk_header.msg_type = RENO_DATA_MSG;
        _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_limit = RENO_MAX_HOP_LIMIT;
        _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_count = 0;
        _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_l = (UINT8)NANOMON_SINK_ADDR;
        _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_h = (UINT8)(NANOMON_SINK_ADDR >> 8);
        _reno_ctrl_msg_tx_packet.nwk_header.src_addr_l = (UINT8)_reno_nib.my_addr;
        _reno_ctrl_msg_tx_packet.nwk_header.src_addr_h = (UINT8)(_reno_nib.my_addr >> 8); 
        _reno_ctrl_msg_tx_packet.nwk_header.var = NANOMON_CTRL_PORT;
        _reno_ctrl_msg_tx_packet.nwk_header.parent_addr_l = (UINT8)(new_entry_ptr->next_hop_addr);
        _reno_ctrl_msg_tx_packet.nwk_header.parent_addr_h = (UINT8)((new_entry_ptr->next_hop_addr) >> 8);
        _reno_ctrl_msg_tx_packet.payload_size = 0;
        _reno_ctrl_msg_tx_packet.payload = NULL;
        reno_send_to_mac(&_reno_ctrl_msg_tx_packet, new_entry_ptr->next_hop_addr);
    }
#endif

    // if there is no route information, find next vacant entry
    if (index == RENO_TABLE_LOOKUP_FAIL)
    {
#ifdef RENO_DEBUG
        nos_uart_puts(STDIO, "\n\rinsert entry. new entry");
#endif
        index = _reno_nib.next_index;
        for (i=0; i<RENO_TABLE_SIZE; ++i)
        {
            if (_reno_nib.route_table[index].state == RENO_INVALID_PATH)
                break;
            index = (index+1)%RENO_TABLE_SIZE;
        }
    }
    else if (_reno_nib.route_table[index].dest_addr != new_entry_ptr->dest_addr)
    {
#ifdef RENO_DEBUG
        nos_uart_puts(STDIO, "\n\rinsert pkt error. this case MUST NOT be happened.");
#endif
        return; 

    }
    memcpy ( &(_reno_nib.route_table[index]), new_entry_ptr, sizeof(RENO_ROUTE_ENTRY));
    _reno_nib.next_index = (index+1)%RENO_TABLE_SIZE;


#ifdef RENO_DEBUG
    reno_print_route_table();
#endif
}



void reno_route_table_delete_destination(UINT16 dest_address)
{
    UINT8 i;
    NOS_ENTER_CRITICAL_SECTION();
    for ( i=0; i< RENO_TABLE_SIZE; ++i)
    {
        if (_reno_nib.route_table[i].dest_addr == dest_address && _reno_nib.route_table[i].state != RENO_INVALID_PATH)
        {
            _reno_nib.route_table[i].state = RENO_INVALID_PATH;
            break;
        }
    }
#ifdef RENO_DEBUG
    reno_print_route_table();
#endif
    NOS_EXIT_CRITICAL_SECTION();
}



void reno_send_rreq(UINT16 dest_addr)
{
    NOS_ENTER_CRITICAL_SECTION();
#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend rreq.");
#endif
    _reno_ctrl_msg_tx_packet.nwk_header.msg_type = RENO_RREQ_MSG;
    _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_limit = RENO_MAX_HOP_LIMIT;
    _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_count = 0;
    _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_l = (UINT8)dest_addr;
    _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_h = (UINT8)(dest_addr >> 8);
    _reno_ctrl_msg_tx_packet.nwk_header.src_addr_l = (UINT8)_reno_nib.my_addr;
    _reno_ctrl_msg_tx_packet.nwk_header.src_addr_h = (UINT8)(_reno_nib.my_addr >> 8); 
    _reno_ctrl_msg_tx_packet.nwk_header.var = _reno_nib.route_seq_num++; // used to prohibit pingpong broadcasting

    _reno_ctrl_msg_tx_packet.payload_size = 0;
    _reno_ctrl_msg_tx_packet.payload = NULL;

    reno_send_to_mac(&_reno_ctrl_msg_tx_packet, 0xFFFF);
    NOS_EXIT_CRITICAL_SECTION();
}



void reno_send_rrep(UINT16 dest_addr, UINT16 next_addr)
{
    NOS_ENTER_CRITICAL_SECTION();
#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend rrep.");
#endif
    _reno_ctrl_msg_tx_packet.nwk_header.msg_type = RENO_RREP_MSG;
    _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_limit = RENO_MAX_HOP_LIMIT;
    _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_count = 0;
    _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_l = (UINT8)dest_addr;
    _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_h = (UINT8)(dest_addr >> 8);
    _reno_ctrl_msg_tx_packet.nwk_header.src_addr_l = (UINT8)_reno_nib.my_addr;
    _reno_ctrl_msg_tx_packet.nwk_header.src_addr_h = (UINT8)(_reno_nib.my_addr >> 8); 
    _reno_ctrl_msg_tx_packet.nwk_header.var = _reno_nib.route_seq_num++;

    _reno_ctrl_msg_tx_packet.payload_size = 0;
    _reno_ctrl_msg_tx_packet.payload = NULL;

    reno_send_to_mac(&_reno_ctrl_msg_tx_packet, next_addr);
    NOS_EXIT_CRITICAL_SECTION();
}



void reno_send_frep(UINT16 dest_addr, UINT16 src_addr, UINT8 hops, UINT8 seq)
{
    NOS_ENTER_CRITICAL_SECTION();
#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend freq.");
#endif
    _reno_ctrl_msg_tx_packet.nwk_header.msg_type = RENO_FREP_MSG;
    _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_limit = hops+1;
    _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_count = hops;
    _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_l = (UINT8)dest_addr;
    _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_h = (UINT8)(dest_addr >> 8);
    _reno_ctrl_msg_tx_packet.nwk_header.src_addr_l = (UINT8)src_addr;
    _reno_ctrl_msg_tx_packet.nwk_header.src_addr_h = (UINT8)(src_addr >> 8); 
    _reno_ctrl_msg_tx_packet.nwk_header.var = seq;

    _reno_ctrl_msg_tx_packet.payload_size = 0;
    _reno_ctrl_msg_tx_packet.payload = NULL;

    reno_send_to_mac(&_reno_ctrl_msg_tx_packet, dest_addr);
    NOS_EXIT_CRITICAL_SECTION();
}



void reno_send_rerr(UINT16 dest_addr, UINT16 src_addr, UINT16 next_addr)
{
    NOS_ENTER_CRITICAL_SECTION();
#ifdef RENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend rerr.");
#endif
    _reno_ctrl_msg_tx_packet.nwk_header.msg_type = RENO_RERR_MSG;
    _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_limit = RENO_MAX_HOP_LIMIT;
    _reno_ctrl_msg_tx_packet.nwk_header.msg_hop_count = 0;
    _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_l = (UINT8)dest_addr;
    _reno_ctrl_msg_tx_packet.nwk_header.dest_addr_h = (UINT8)(dest_addr >> 8);
    _reno_ctrl_msg_tx_packet.nwk_header.src_addr_l = (UINT8)src_addr;
    _reno_ctrl_msg_tx_packet.nwk_header.src_addr_h = (UINT8)(src_addr >> 8); 

    _reno_ctrl_msg_tx_packet.nwk_header.var = 0;
    _reno_ctrl_msg_tx_packet.payload_size = 0;
    _reno_ctrl_msg_tx_packet.payload = NULL;

    reno_send_to_mac(&_reno_ctrl_msg_tx_packet, next_addr);
    NOS_EXIT_CRITICAL_SECTION();
}

#ifdef RENO_DEBUG
void reno_print_route_table(void)
{
    UINT8 i;
    nos_uart_puts(STDIO, "\n\r============ROUTE TABLE===========\n\r");
    nos_uart_puts(STDIO, "index\tdest\tnext\thops\tstate\tseq\n\r");
    for (i=0; i<RENO_TABLE_SIZE; ++i)
    {
        if (_reno_nib.route_table[i].state != RENO_INVALID_PATH)
        {
            nos_uart_putu(STDIO, i);
            nos_uart_puts(STDIO, "\t");
            nos_uart_putu(STDIO, _reno_nib.route_table[i].dest_addr);
            nos_uart_puts(STDIO, "\t");
            nos_uart_putu(STDIO, _reno_nib.route_table[i].next_hop_addr);
            nos_uart_puts(STDIO, "\t");
            nos_uart_putu(STDIO, _reno_nib.route_table[i].hops_to_dest);
            nos_uart_puts(STDIO, "\t");
            if ( _reno_nib.route_table[i].state == RENO_RREQ_PATH)
                nos_uart_puts(STDIO, "RREQ\t");
            else if ( _reno_nib.route_table[i].state == RENO_RREP_PATH)
                nos_uart_puts(STDIO, "RREP\t");
            else if ( _reno_nib.route_table[i].state == RENO_RREP_PATH)
                nos_uart_puts(STDIO, "FREP\t");
            else
                nos_uart_puts(STDIO, "DATA\t");
            nos_uart_putu(STDIO, _reno_nib.route_table[i].seq);
            nos_uart_puts(STDIO, "\r\n");
        }
    }


}

void reno_print_pkt_info(RENO_PACKET* pkt_ptr)
{
    //UINT8 i;
    nos_uart_puts(STDIO, "\n\r");
    nos_uart_puts(STDIO, "\n\rmsg_type : ");
    nos_uart_putu(STDIO, pkt_ptr->nwk_header.msg_type);
    nos_uart_puts(STDIO, "\n\rmsg_hop_count : ");
    nos_uart_putu(STDIO, pkt_ptr->nwk_header.msg_hop_count);
    nos_uart_puts(STDIO, "\n\rdestination : ");
    nos_uart_putu(STDIO, pkt_ptr->nwk_header.dest_addr_l + (UINT16)((pkt_ptr->nwk_header.dest_addr_h)<<8) );
    nos_uart_puts(STDIO, "\n\rsource : ");
    nos_uart_putu(STDIO, pkt_ptr->nwk_header.src_addr_l + (UINT16)((pkt_ptr->nwk_header.src_addr_h)<<8) );
    nos_uart_puts(STDIO, "\n\rvar : ");
    nos_uart_putu(STDIO, pkt_ptr->nwk_header.var);
    nos_uart_puts(STDIO, "\n\rpayload size : ");
    nos_uart_putu(STDIO, pkt_ptr->payload_size);
    /*nos_uart_puts(STDIO, "\n\rpayload : ");
      for (i=0; i<pkt_ptr->payload_size; ++i)
      {
      nos_uart_putu(STDIO, ((UINT8*)pkt_ptr->payload)[i]);
      nos_uart_puts(STDIO, " ");
      }*/
}
#endif


#endif // RENO_M

