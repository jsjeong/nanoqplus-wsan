// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @date 2006. 9. 1.
 */

#ifndef RENO_H
#define RENO_H
#include "kconf.h"
#ifdef RENO_M

#include "nos_common.h"
#include "mac.h"

// User changeable variables
enum { RENO_MAX_HOP_LIMIT = 20 }; // The maximum hop limit of RENO_PACKET messages. MUST be smaller than 255
enum { RENO_TABLE_SIZE = 100 }; // MUST be smaller than 255. 6byte per entry.

enum { RENO_ROUTE_TRACE_DELAY_MS = 300 }; // maximum delay is trial*trial*delay_ms
enum { RENO_ROUTE_TRACE_MAX_TRIAL = 4 };

// RENO packet header format
#pragma pack(1)
struct _reno_header_
{
    UINT8 msg_type;
    UINT8 msg_hop_limit;
    UINT8 msg_hop_count;
    UINT8 dest_addr_l;
    UINT8 dest_addr_h;
    UINT8 src_addr_l;
    UINT8 src_addr_h;
    UINT8 var;   // port for DATA, RERR, hops for RREQ, RREP, FREP
} ;
#pragma pack()
typedef struct _reno_header_ RENO_HEADER;

#pragma pack(1)
struct _reno_packet_
{
    UINT8 payload_size;
    RENO_HEADER nwk_header;
    void *payload; // Packet payload
} ;
#pragma pack()
typedef struct _reno_packet_ RENO_PACKET;


// Routing table entry
typedef struct _reno_route_entry_
{
    UINT16 dest_addr;
    UINT16 next_hop_addr;   // next node address to send packets to the destination node.
    UINT8 state;    // 0:invalid, 1:FREP path, 2:RREQ path, 3:RREP path.
    UINT8 seq;
    UINT8 hops_to_dest;
} RENO_ROUTE_ENTRY;

// Each node has one NIB (network information base)
typedef struct _reno_network_informaion_base_
{
    UINT16 my_addr;  // my address
    UINT8 next_index;  // The next index of inserted index in last
    UINT8 route_seq_num;
    RENO_ROUTE_ENTRY route_table[RENO_TABLE_SIZE];
} RENO_NIB;


// message type
enum
{
    RENO_DATA_MSG = 0x0B,
    RENO_RREQ_MSG = 0x0C,
    RENO_RREP_MSG = 0x0D,
    RENO_RERR_MSG = 0x0E,
    RENO_FREP_MSG = 0x0F, // 1-hop neighbor's rrep
};

// RENO packet
enum
{
    RENO_HEADER_SIZE = 8,
};

enum {
    RENO_MAX_PACKET_PAYLOAD_SIZE = (NOS_MAC_MAX_SAFE_TX_PAYLOAD_SIZE - RENO_HEADER_SIZE) };

// "reno_send_to_nwk()" return values
enum
{
    RENO_TX_SUCCEEDED = 2,
    RENO_TX_DELAYED = 1,
    RENO_TX_FAILED = 0,
};

void reno_init(UINT16 pan_id, UINT16 short_id, void (*rx_callback)(void));
void reno_set_rx_cb(void (*func)(void));
UINT8 reno_send_to_nwk(UINT16 dest_id, UINT8 port, UINT8 data_length, void* tx_string);
void reno_recv_from_nwk(UINT16* src_id, UINT8* port, UINT8* data_length, void* rx_string);

#endif // RENO_M
#endif // ~RENO_H
//=========================== End of (reno.h) =========================

