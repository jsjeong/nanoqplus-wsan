// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @date 2008. 1.
 */

#ifndef TENO_H
#define TENO_H
#include "kconf.h"
#ifdef TENO_M

#include "nos_common.h"
#include "mac.h"

#define TENO_MAX_TREE_DEPTH  (50) // maximum depth for source routing
#define TENO_GENERAL_SINK_ID (0xFFFF)
#define TENO_BROADCAST_ID  (0xFFFF)


// Packet size
enum
{
    TENO_JOIN_PACKET_SIZE = 2,
    TENO_REPLY_PACKET_SIZE = 2,
    TENO_RELEASE_PACKET_SIZE = 1,
    TENO_DATA_PACKET_HEADER_SIZE = 13,
};

enum
{
    TENO_MAX_PACKET_PAYLOAD_SIZE =
            (NOS_MAC_MAX_SAFE_TX_PAYLOAD_SIZE - TENO_DATA_PACKET_HEADER_SIZE),
};

// Message type (01~0F : RENO, 11~1F : TENO)
enum
{
    TENO_JOIN = 0x11,
    TENO_REPLY = 0x12,
    TENO_RELEASE = 0x13,
    TENO_UPDATA = 0x14,
    TENO_DOWNDATA = 0x15,
    TENO_SRC_ROUTING_DATA = 0x16,
};

// State
enum
{
    TENO_ALONE_STATE = 20,
    TENO_JOIN_STATE = 21,
    TENO_SINK_STATE = 22,
};

// Network layer packet format.  [UINT16] should be aligned at even address
#pragma pack(1)
struct _teno_join_packet
{
    UINT8 msg_type;
    UINT8 depth;
};
#pragma pack()
typedef struct _teno_join_packet TENO_JOIN_PACKET;

#pragma pack(1)
struct _teno_reply_packet
{
    UINT8 msg_type;
    UINT8 depth;
};
#pragma pack()
typedef struct _teno_reply_packet TENO_REPLY_PACKET;

#pragma pack(1)
struct _teno_release_packet
{
    UINT8 msg_type;
};
#pragma pack()
typedef struct _teno_release_packet TENO_RELEASE_PACKET;

typedef struct _teno_data_packet
{
    UINT8 msg_type;
    UINT8 msg_hop_limit;
    UINT8 msg_hop_count;
    UINT8 depth; // depth of MAC layer source node (current sensor node's depth)
    UINT8 dest_id_l;
    UINT8 dest_id_h;
    UINT8 src_id_l;
    UINT8 src_id_h;
    UINT8 parent_id_l;
    UINT8 parent_id_h;
    UINT8 port;
    UINT8 seq;
    UINT8 payload_size;
    void  *payload; // Packet payload
} TENO_DATA_PACKET;

// Each node has one NIB (network information base)
typedef struct _teno_network_informaion_base
{
    volatile UINT16 my_id;   // my ID.
    volatile UINT8 my_depth;
    volatile UINT8 state;
    volatile UINT16 parent_id; // parent
    volatile UINT16 parent_candidate;
    volatile BOOL candidate;
    volatile UINT8 tx_seq;
} TENO_NIB;
extern TENO_NIB teno_nib;

#define TENO_GET_PARENT_ID (teno_nib.parent_id)
#define TENO_GET_MY_DEPTH  (teno_nib.my_depth)

void teno_init(UINT16 panid, UINT16 id, void (*rx_callback)(void));
void teno_set_rx_cb(void (*func)(void));
void teno_role_as_sink(void);
void teno_role_as_node(void);
BOOL teno_send_to_sink(UINT8 port, UINT8 data_length, void* tx_string);
BOOL teno_send_to_node(UINT16 dest_id, UINT8 port, UINT8 data_length, void* tx_string);
void teno_recv_from_nwk(UINT16* src_id, UINT8* port, UINT8* data_length, void* rx_string, UINT16* parent_id, UINT16* dest_id);

#endif // TENO_M
#endif // ~TENO_H
//=========================== End of (teno.h) =========================

