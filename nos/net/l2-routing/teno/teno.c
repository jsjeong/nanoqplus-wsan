/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @date 2008. 01.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "teno.h"

//#define TENO_DEBUG
#ifdef TENO_DEBUG
#include "uart.h"
#include "platform.h"
#endif

#ifdef TENO_M
#include <string.h>
#include <stdlib.h>
#include "arch.h"
#include "mac.h"
#include "critical_section.h"
#include "user_timer.h"
#include "taskq.h"
#include "nanomon_app.h"

TENO_NIB teno_nib;
INT8 join_timer_id; // user timer ID created to send JOIN msg

// We don't need to support re-entrance for RX. So we use global variables to reduce overhead for each thread stack memory
void (*teno_rx_callback)(void);
NOS_MAC_RX_INFO teno_mac_rx_info;
UINT8 teno_mac_payload[NOS_MAC_MAX_SAFE_RX_PAYLOAD_SIZE];
TENO_JOIN_PACKET teno_join_packet;
TENO_REPLY_PACKET teno_reply_packet;
TENO_RELEASE_PACKET teno_release_packet;
TENO_DATA_PACKET teno_rx_data_packet;

// TENO routing has 1-item long data queue.
struct _nos_teno_val_data
{
    UINT16 src_id;
    UINT16 dest_id;
    UINT16 parent_id;
    UINT8 port;
    UINT8 rx_seq; // for checking duplicated packet
    UINT8 data_length;
    void* data_ptr;
} teno_val_data;

void teno_cb_from_mac(void);
void teno_start_alone_state(void);
void teno_join_timer(void *args);
void teno_send_join_to_mac(void);
BOOL teno_send_reply_to_mac(UINT16 dest_id);
BOOL teno_send_release_to_mac(UINT16 dest_id);
BOOL teno_send_data_to_mac(TENO_DATA_PACKET* tx_packet_ptr, UINT16 dest_id);
BOOL teno_send_to_node_by_flooding(TENO_DATA_PACKET* tx_packet_ptr);
BOOL teno_send_data_to_parent(TENO_DATA_PACKET* tx_packet_ptr);
BOOL teno_del_curr_parent(void);
void teno_recv_from_mac(void *args);
void teno_recv_updata_from_mac(void);
void teno_recv_downdata_from_mac(void);
void teno_store_data(TENO_DATA_PACKET* rx_packet_ptr);



#ifdef NANOMON
BOOL (*teno_src_routing_callback)(TENO_DATA_PACKET*);

void teno_set_src_routing_cb(BOOL (*func)(TENO_DATA_PACKET*))
{
    teno_src_routing_callback = func;
}
#endif

void teno_init(UINT16 panid, UINT16 id, void (*rx_callback)(void))
{
    NOS_ENTER_CRITICAL_SECTION();
    // NWK layer initialization
    teno_set_rx_cb(rx_callback);
    teno_nib.my_id = id;
    srand(id);
    teno_nib.tx_seq = (UINT16)rand();
    teno_nib.parent_id = TENO_GENERAL_SINK_ID;
    teno_nib.parent_candidate = TENO_GENERAL_SINK_ID;
    join_timer_id = NOS_USER_TIMER_CREATE_ERROR; //no join timer created
    teno_val_data.port = 0xFF; //dummy port
    teno_val_data.rx_seq = 0xFF; //dummy sequence

    // MAC layer initialization
    nos_mac_init(panid, id, NULL);
    nos_mac_set_rx_cb(teno_cb_from_mac);

    // to join a network
    teno_start_alone_state();
    NOS_EXIT_CRITICAL_SECTION();
}


//Sets callback function for NWK RX.
void teno_set_rx_cb(void (*func)(void))
{
    teno_rx_callback = func;
}


// Make a node to sink
void teno_role_as_sink(void)
{
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rrole_as_sink");
#endif
    NOS_ENTER_CRITICAL_SECTION();
    teno_nib.state = TENO_SINK_STATE;
    teno_nib.my_depth = 0;
    teno_nib.parent_id = TENO_GENERAL_SINK_ID;
    teno_nib.parent_candidate = TENO_GENERAL_SINK_ID;
    teno_nib.candidate = FALSE;
    NOS_EXIT_CRITICAL_SECTION();
    teno_send_reply_to_mac(TENO_BROADCAST_ID);
}


// Make a sink to a normal node
void teno_role_as_node(void)
{
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rrole_as_node");
#endif
    NOS_ENTER_CRITICAL_SECTION();
    if(teno_nib.state == TENO_SINK_STATE)
    {
        teno_start_alone_state();
    }
    NOS_EXIT_CRITICAL_SECTION();
}


BOOL teno_send_to_sink(UINT8 port, UINT8 data_length, void* tx_string)
{
    return teno_send_to_node(TENO_GENERAL_SINK_ID, port, data_length, tx_string);
}


BOOL teno_send_to_node(UINT16 dest_id, UINT8 port, UINT8 data_length, void* tx_string)
{
    TENO_DATA_PACKET teno_tx_data_packet;
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend to node");
#endif
    teno_tx_data_packet.msg_type = TENO_UPDATA; // every packet must be sent to sink first.
    teno_tx_data_packet.msg_hop_limit = TENO_MAX_TREE_DEPTH;
    teno_tx_data_packet.msg_hop_count = 0;
    teno_tx_data_packet.dest_id_l = (UINT8)dest_id;
    teno_tx_data_packet.dest_id_h = (UINT8)(dest_id >> 8);
    teno_tx_data_packet.src_id_l = (UINT8)teno_nib.my_id;
    teno_tx_data_packet.src_id_h = (UINT8)(teno_nib.my_id >> 8);
    teno_tx_data_packet.parent_id_l = (UINT8)teno_nib.parent_id;
    teno_tx_data_packet.parent_id_h = (UINT8)(teno_nib.parent_id >> 8);
    teno_tx_data_packet.port = port;
    teno_tx_data_packet.seq = ++teno_nib.tx_seq; // increasing sequence number
    teno_tx_data_packet.payload_size = data_length;
    teno_tx_data_packet.payload = tx_string;

    if(dest_id == teno_nib.my_id)
    {
        teno_store_data(&teno_tx_data_packet);
        return TRUE;
    }
    else
    {
        if (teno_nib.state == TENO_ALONE_STATE)
        {
            return FALSE;
        }
        else if (teno_nib.state == TENO_JOIN_STATE)
        {
            return teno_send_data_to_parent(&teno_tx_data_packet);
        }
        else if (teno_nib.state == TENO_SINK_STATE)
        {
#ifdef NANOMON
            if (dest_id == TENO_BROADCAST_ID || teno_src_routing_callback == NULL)
                return teno_send_to_node_by_flooding(&teno_tx_data_packet);
            else
                return teno_src_routing_callback(&teno_tx_data_packet);
#else
            return teno_send_to_node_by_flooding(&teno_tx_data_packet);
#endif
        }
        return FALSE;
    }
}


// This must be called in teno_rx_callback()
void teno_recv_from_nwk(UINT16* src_id, UINT8* port, UINT8* data_length,
        void* rx_string, UINT16* parent_id, UINT16* dest_id)
{
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rrecv_from_nwk");
#endif
    NOS_ENTER_CRITICAL_SECTION();
    *src_id = teno_val_data.src_id;
    if (dest_id)
        *dest_id = teno_val_data.dest_id;
    if (parent_id)
        *parent_id = teno_val_data.parent_id;
    *port = teno_val_data.port;
    *data_length = teno_val_data.data_length;
    memcpy(rx_string, teno_val_data.data_ptr, teno_val_data.data_length);
    NOS_EXIT_CRITICAL_SECTION();
}

















//Callback function for MAC RX interrupt.
void teno_cb_from_mac(void)
{
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rMAC_RX");
#endif
    nos_taskq_reg(teno_recv_from_mac, NULL);
}


void teno_start_alone_state(void)
{
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rstart_alone_state");
#endif
    NOS_ENTER_CRITICAL_SECTION();
    if (teno_nib.state != TENO_ALONE_STATE)
    {
        teno_nib.my_depth = 0xFF;
        teno_nib.state = TENO_ALONE_STATE;
        teno_nib.candidate = FALSE;

        // to join a network
        teno_send_join_to_mac();
        if(join_timer_id < 0) // join_timer has not been created yet.
        {
            join_timer_id = nos_user_timer_create_sec(teno_join_timer, NULL, 5, NOS_USER_TIMER_PERIODIC);
            if (join_timer_id < 0) // user timer was not created.
            {
                // we can't control this case.
                /*NOS_DISABLE_GLOBAL_INTERRUPT();
#ifdef TENO_DEBUG
nos_uart_puts(STDIO, "\n\rError! : TENO routing failed to create join timer. System has halted.");
#endif
while (1);*/
            }
        }
    }
    NOS_EXIT_CRITICAL_SECTION();
}


// Broadcast JOIN msg. 
void teno_join_timer(void *args)
{
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rjoin_timer");
#endif
    if (teno_nib.state == TENO_ALONE_STATE)
    {
        teno_send_join_to_mac();
    }
    else
    {
        NOS_ENTER_CRITICAL_SECTION();
        nos_user_timer_destroy(join_timer_id);
        join_timer_id = NOS_USER_TIMER_CREATE_ERROR; // means no join timer created
        NOS_EXIT_CRITICAL_SECTION();
    }
}


// Broadcast JOIN msg.
void teno_send_join_to_mac(void)
{
    NOS_MAC_TX_INFO mac_tx_info;
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend join");
#endif

    if (teno_nib.state != TENO_SINK_STATE)
    {
        teno_join_packet.msg_type = TENO_JOIN;
        teno_join_packet.depth = teno_nib.my_depth;
        mac_tx_info.dest_addr = 0xffff;
        mac_tx_info.routing_header_length  = TENO_JOIN_PACKET_SIZE;
        mac_tx_info.routing_header_ptr  = (void *) &teno_join_packet;
        mac_tx_info.payload_length = 0;
        nos_mac_tx_noack(&mac_tx_info);
    }
}


// Send REPLY msg.
BOOL teno_send_reply_to_mac(UINT16 dest_id)
{
    NOS_MAC_TX_INFO mac_tx_info;
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend reply");
#endif

    if(teno_nib.state != TENO_ALONE_STATE)
    {
        teno_reply_packet.msg_type = TENO_REPLY;
        teno_reply_packet.depth = teno_nib.my_depth;
        mac_tx_info.frame_type = IEEE_802_15_4_FRAME_DATA;
        mac_tx_info.dest_addr = dest_id;
        mac_tx_info.use_short_src = TRUE;
        memcpy(mac_tx_info.payload, &teno_reply_packet, TENO_REPLY_PACKET_SIZE);
        mac_tx_info.payload_length = TENO_REPLY_PACKET_SIZE;
        return (nos_mac_tx(&mac_tx_info) == ERROR_SUCCESS ? TRUE : FALSE);
    }
    return FALSE;
}


// Send RELEASE msg.
BOOL teno_send_release_to_mac(UINT16 dest_id)
{
    NOS_MAC_TX_INFO mac_tx_info;
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend release");
#endif

    if (teno_nib.state != TENO_SINK_STATE)
    {
        teno_release_packet.msg_type = TENO_RELEASE;
        mac_tx_info.frame_type = IEEE_802_15_4_FRAME_DATA;
        mac_tx_info.dest_addr = dest_id;
        mac_tx_info.use_short_src = TRUE;
        memcpy(mac_tx_info.payload, &teno_release_packet, TENO_RELEASE_PACKET_SIZE);
        mac_tx_info.payload_length = TENO_RELEASE_PACKET_SIZE;
        return (nos_mac_tx(&mac_tx_info) == ERROR_SUCCESS ? TRUE : FALSE);
    }
    return FALSE;
}


// Send DATA msg.
BOOL teno_send_data_to_mac(TENO_DATA_PACKET* tx_packet_ptr, UINT16 dest_id)
{
    NOS_MAC_TX_INFO mac_tx_info;
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend data");
#endif

    ++tx_packet_ptr->msg_hop_count;
    tx_packet_ptr->depth = teno_nib.my_depth;
    mac_tx_info.frame_type = IEEE_802_15_4_FRAME_DATA;
    mac_tx_info.dest_addr = dest_id;
    mac_tx_info.use_short_src = TRUE;
    memcpy(mac_tx_info.payload, tx_packet_ptr, TENO_DATA_PACKET_HEADER_SIZE);
    memcpy(&mac_tx_info.payload[TENO_DATA_PACKET_HEADER_SIZE], tx_packet_ptr->payload, tx_packet_ptr->payload_size);
    mac_tx_info.payload_length = TENO_DATA_PACKET_HEADER_SIZE + tx_packet_ptr->payload_size;
    if(dest_id == TENO_BROADCAST_ID)
    {
        nos_mac_tx_noack(&mac_tx_info);
        return TRUE;
    }
    else
    {
      return (nos_mac_tx(&mac_tx_info) == ERROR_SUCCESS ? TRUE : FALSE);
    }
}



BOOL teno_send_data_to_parent(TENO_DATA_PACKET* tx_packet_ptr)
{
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend_data_upward");
#endif

    while (teno_nib.state == TENO_JOIN_STATE)
    {
        if (teno_send_data_to_mac(tx_packet_ptr, teno_nib.parent_id))
        {
            return TRUE;
        }
        else
        {
            if (teno_del_curr_parent())
            {
                if ( teno_nib.my_id ==  (UINT16)((tx_packet_ptr->src_id_h << 8) + tx_packet_ptr->src_id_l) )
                {
                    tx_packet_ptr->parent_id_l = (UINT8)teno_nib.parent_id;
                    tx_packet_ptr->parent_id_h = (UINT8)(teno_nib.parent_id >> 8);
                }
            }
        }
    }
    return FALSE;
}





// return node's state. JOIN(True), ALONE or SINK(False).
BOOL teno_del_curr_parent(void)
{
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rdel_curr_parent");
#endif
    if (teno_nib.state == TENO_JOIN_STATE)
    {
        if (teno_nib.candidate == TRUE)
        {
            NOS_ENTER_CRITICAL_SECTION();
            teno_nib.candidate = FALSE;
            teno_nib.parent_id = teno_nib.parent_candidate;
            NOS_EXIT_CRITICAL_SECTION();
#ifdef NANOMON
            teno_send_to_sink(NANOMON_CTRL_PORT, 0, NULL); // send dummy data to maintain tree topology
#endif
            // to fresh the link, broadcast JOIN message 1 times.
            teno_send_join_to_mac();
            return TRUE;
        }
        else
        {
            // this will broadcast JOIN message until the node is joined to a network.
            teno_start_alone_state();
            return FALSE;
        }
    }
    else
    {
        return FALSE;
    }
}


// Do not support re-entrance. Should be called by taskq_reg only.
// 1. check teno_nib.state
// 2. check depth
// 3. check rx_seq (for downward packet)
// 4. check previous node
// 5. check destination
void teno_recv_from_mac(void *args)
{
    UINT8 msg_type, depth;
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rrecv_from_mac");
#endif

    while ( nmac_rx(&teno_mac_rx_info) )
    {
        msg_type = ((UINT8*)teno_mac_rx_info.payload)[0];

        //================== JOIN ===================//
        if(msg_type == TENO_JOIN)
        {
#ifdef TENO_DEBUG
            nos_uart_puts(STDIO, "-join");
#endif  
            if (teno_nib.state == TENO_JOIN_STATE)
            {
                if (teno_nib.my_depth < TENO_MAX_TREE_DEPTH)
                {
                    depth = ((UINT8*)teno_mac_rx_info.payload)[1];
                    if(depth > teno_nib.my_depth)
                    {
                        if(teno_mac_rx_info.src_addr != teno_nib.parent_id)
                        {
                            nos_delay_ms(teno_nib.my_depth);
                            teno_send_reply_to_mac(teno_mac_rx_info.src_addr);
                        }
                    }
                }

            }
            else if (teno_nib.state == TENO_SINK_STATE)
            {
                teno_send_reply_to_mac(teno_mac_rx_info.src_addr);
            }
        }

        //================== REPLY ===================//
        else if (msg_type == TENO_REPLY)
        {
#ifdef TENO_DEBUG
            nos_uart_puts(STDIO, "-reply");
#endif  
            depth = ((UINT8*)teno_mac_rx_info.payload)[1];
            if (teno_nib.state == TENO_ALONE_STATE)
            {
                teno_nib.my_depth = depth+1;
                teno_nib.state = TENO_JOIN_STATE;
                teno_nib.parent_id = teno_mac_rx_info.src_addr;
#ifdef NANOMON
                teno_send_to_sink(NANOMON_CTRL_PORT, 0, NULL); // send dummy data to maintain tree topology
#endif
            }
            else if (teno_nib.state == TENO_JOIN_STATE)
            {
                // shorter depth to sink
                if (depth+1 < teno_nib.my_depth)
                {
                    teno_nib.candidate = FALSE;
                    teno_nib.my_depth = depth+1;
                    teno_nib.parent_id = teno_mac_rx_info.src_addr;
#ifdef NANOMON
                    teno_send_to_sink(NANOMON_CTRL_PORT, 0, NULL); // send dummy data to maintain tree topology
#endif
                }
                // the same depth to sink
                else if (depth+1 == teno_nib.my_depth)
                {
                    if(teno_mac_rx_info.src_addr != teno_nib.parent_id)
                    {
                        teno_nib.candidate = TRUE;
                        teno_nib.parent_candidate = teno_mac_rx_info.src_addr;
                    }
                }
            }
        }

        //================== RELEASE  ===================//
        else if (msg_type == TENO_RELEASE)
        {
#ifdef TENO_DEBUG
            nos_uart_puts(STDIO, "-release");
#endif  
            if(teno_nib.state == TENO_JOIN_STATE)
            {
                if (teno_mac_rx_info.src_addr == teno_nib.parent_id)
                {
                    teno_del_curr_parent();
                }
                else if (teno_mac_rx_info.src_addr == teno_nib.parent_candidate)
                {
                    teno_nib.candidate = FALSE;
                }
            }
        }

        //================== UPDATA ===================//
        else if (msg_type == TENO_UPDATA)
        {
#ifdef TENO_DEBUG
            nos_uart_puts(STDIO, "-updata");
#endif  
            teno_recv_updata_from_mac();
        }

        //================== DOWNDATA ===================//
        else if (msg_type == TENO_DOWNDATA)
        {
#ifdef TENO_DEBUG
            nos_uart_puts(STDIO, "-downdata");
#endif  
            if (teno_nib.state != TENO_SINK_STATE)
            {
                teno_recv_downdata_from_mac();
            }
        }
    }
}



void teno_recv_updata_from_mac(void)
{
    UINT8 i=0;
    UINT16 dest_id;
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rrecv_updata_from_mac");
#endif
    teno_rx_data_packet.msg_type  = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.msg_hop_limit  = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.msg_hop_count  = ((UINT8*)teno_mac_rx_info.payload)[i++];
    if (teno_rx_data_packet.msg_hop_count > teno_rx_data_packet.msg_hop_limit)
    {
        return;
    }
    teno_rx_data_packet.depth = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.dest_id_l = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.dest_id_h = ((UINT8*)teno_mac_rx_info.payload)[i++];
    dest_id = (UINT16)((teno_rx_data_packet.dest_id_h << 8) + teno_rx_data_packet.dest_id_l);
    teno_rx_data_packet.src_id_l = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.src_id_h = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.parent_id_l = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.parent_id_h = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.port  = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.seq = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.payload_size = ((UINT8*)teno_mac_rx_info.payload)[i++];
    if (teno_rx_data_packet.payload_size > TENO_MAX_PACKET_PAYLOAD_SIZE)
    {
        return;
    }
    teno_rx_data_packet.payload = (void*)( &(((UINT8*)teno_mac_rx_info.payload)[i++]) );

    if (teno_nib.state == TENO_ALONE_STATE)
    {
        // previous node is in ALONE or SINK state
        if(teno_rx_data_packet.depth == 0xFF || teno_rx_data_packet.depth == 0)
        {
            // I am the packet's destination.
            if (dest_id == teno_nib.my_id)
            {
                teno_store_data(&teno_rx_data_packet);
            }
        }
        else // previous node is in JOIN state
        {
            // I am the packet's destination.
            if (dest_id == teno_nib.my_id)
            {
                teno_send_release_to_mac(teno_mac_rx_info.src_addr);
                teno_store_data(&teno_rx_data_packet);
            }
            else
            {
                // send upwarddata back to child from parent will make child delete curruent link. So don't need to send RELEASE msg.
                teno_send_data_to_mac(&teno_rx_data_packet, teno_mac_rx_info.src_addr);
            }
        }
    }
    else if (teno_nib.state == TENO_SINK_STATE)
    {
        if (teno_rx_data_packet.depth != 1)
        {
            teno_send_reply_to_mac(teno_mac_rx_info.src_addr);
        }
        // I am the packet's destination.
        if (dest_id == teno_nib.my_id || dest_id == TENO_GENERAL_SINK_ID)
        {
            teno_store_data(&teno_rx_data_packet);
        }
        else
        {
#ifdef NANOMON
            if (teno_src_routing_callback)
                teno_src_routing_callback(&teno_rx_data_packet);
            else
                teno_send_to_node_by_flooding(&teno_rx_data_packet);
#else
            teno_send_to_node_by_flooding(&teno_rx_data_packet);
#endif
        }
    }
    else if (teno_nib.state == TENO_JOIN_STATE)
    {
        // past parent is in ALONE state now. delete current link.
        if (teno_mac_rx_info.src_addr == teno_nib.parent_id)
        {
            if(teno_del_curr_parent())
            {
                // to reduce packet loss, send previous data packet to other parent
                teno_send_data_to_parent(&teno_rx_data_packet);
            }
        }
        else
        {
            if (teno_rx_data_packet.depth <= teno_nib.my_depth)
            {
                teno_send_release_to_mac(teno_mac_rx_info.src_addr);
            }
            // haekim@10.01.14, block to send reply if the child is a leaf node (SFD)
            else if (teno_rx_data_packet.depth > teno_nib.my_depth+1 
                    && teno_rx_data_packet.depth != 0xFF)
            {
                teno_send_reply_to_mac(teno_mac_rx_info.src_addr);
            }
            // I am the packet's destination.
            if (dest_id == teno_nib.my_id)
            {
                teno_store_data(&teno_rx_data_packet);
            }
            else
            {
                teno_send_data_to_parent(&teno_rx_data_packet);
            }
        }
    }
}




void teno_recv_downdata_from_mac(void)
{
    UINT8 i=0;
    UINT16 dest_id;
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rrecv_downdata_from_mac");
#endif
    teno_rx_data_packet.msg_type  = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.msg_hop_limit  = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.msg_hop_count  = ((UINT8*)teno_mac_rx_info.payload)[i++];
    if (teno_rx_data_packet.msg_hop_count > teno_rx_data_packet.msg_hop_limit)
    {
        return;
    }
    teno_rx_data_packet.depth = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.dest_id_l = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.dest_id_h = ((UINT8*)teno_mac_rx_info.payload)[i++];
    dest_id = (UINT16)((teno_rx_data_packet.dest_id_h << 8) + teno_rx_data_packet.dest_id_l);
    teno_rx_data_packet.src_id_l = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.src_id_h = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.parent_id_l = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.parent_id_h = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.port  = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.seq = ((UINT8*)teno_mac_rx_info.payload)[i++];
    teno_rx_data_packet.payload_size = ((UINT8*)teno_mac_rx_info.payload)[i++];
    if (teno_rx_data_packet.payload_size > TENO_MAX_PACKET_PAYLOAD_SIZE)
    {
        return;
    }
    teno_rx_data_packet.payload = (void*)( &(((UINT8*)teno_mac_rx_info.payload)[i++]) );

    if (teno_nib.state == TENO_ALONE_STATE)
    {
        // I am the packet's destination.
        if (dest_id == teno_nib.my_id || dest_id == TENO_BROADCAST_ID)
        {
            teno_store_data(&teno_rx_data_packet);
        }
    }
    else if (teno_nib.state == TENO_JOIN_STATE)
    {
        if (teno_rx_data_packet.depth < teno_nib.my_depth)
        {
            // there is a shorter link
            if (teno_rx_data_packet.depth+1 < teno_nib.my_depth)
            {
                // this can be changed to unicast to the source. 
                teno_send_join_to_mac();
            }
            // I am the packet's destination.
            if (dest_id == teno_nib.my_id)
            {
                teno_store_data(&teno_rx_data_packet);
            }
            else if (dest_id == TENO_BROADCAST_ID)
            {
                teno_send_data_to_mac(&teno_rx_data_packet, TENO_BROADCAST_ID);
                teno_store_data(&teno_rx_data_packet);
            }
            else
            {
                teno_send_data_to_mac(&teno_rx_data_packet, TENO_BROADCAST_ID);
            }
        }
    }
}


BOOL teno_send_to_node_by_flooding(TENO_DATA_PACKET* tx_packet_ptr)
{
#ifdef TENO_DEBUG
    nos_uart_puts(STDIO, "\n\rsend_data_by_flooding");
#endif
    if (teno_nib.state == TENO_SINK_STATE)
    {
        tx_packet_ptr->msg_type = TENO_DOWNDATA;
        tx_packet_ptr->msg_hop_count = 0;
        teno_send_data_to_mac(tx_packet_ptr, TENO_BROADCAST_ID);
        return TRUE;
    }
    return FALSE;
}



void teno_store_data(TENO_DATA_PACKET* rx_packet_ptr)
{
    // if it is not a duplicated packet.
    if ( (teno_val_data.rx_seq != rx_packet_ptr->seq)
            || (teno_val_data.src_id != (UINT16)((rx_packet_ptr->src_id_h << 8) + rx_packet_ptr->src_id_l)) )
    {
        NOS_ENTER_CRITICAL_SECTION();
        teno_val_data.src_id = (UINT16)((rx_packet_ptr->src_id_h << 8) + rx_packet_ptr->src_id_l);
        teno_val_data.dest_id = (UINT16)((rx_packet_ptr->dest_id_h << 8) + rx_packet_ptr->dest_id_l);
        teno_val_data.parent_id = (UINT16)((rx_packet_ptr->parent_id_h << 8) + rx_packet_ptr->parent_id_l);
        teno_val_data.port = rx_packet_ptr->port;
        teno_val_data.data_length = rx_packet_ptr->payload_size;
        teno_val_data.data_ptr = rx_packet_ptr->payload;
        NOS_EXIT_CRITICAL_SECTION();
        if (teno_rx_callback)
        {
            // [teno_recv_from_nwk(...)] must be called from callback.
            teno_rx_callback();
        }
    }
}



#endif // TENO_M
