// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief IEEE 802.15.4 MAC sublayer
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 1. 9.
 */

#ifndef IEEE_802_15_4_H_
#define IEEE_802_15_4_H_

#include "nos_common.h"
#include "errorcodes.h"

enum { IEEE_802_15_4_UNIT_BACKOFF_PERIOD = 20 };
enum { IEEE_802_15_4_TURNAROUND_TIME = 12 };
enum { IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BACKOFFS = 4 };
enum
{
    IEEE_802_15_4_2006_DEFAULT_CSMA_MIN_BE = 3,
    IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BE = 5,
};

#define IEEE_802_15_4_BACKOFF_DURATION \
    (IEEE_802_15_4_SYMBOL_DURATION * IEEE_802_15_4_UNIT_BACKOFF_PERIOD)

enum
{
    IEEE_802_15_4_FRAME_SIZE =                  127,
    IEEE_802_15_4_MIN_MAC_HEADER_SIZE =         9,  //(2+1+2+2+2)
    IEEE_802_15_4_MAX_MAC_HEADER_SIZE =         37, //((2+1+2+8+2+8)+(1+4+9))
    IEEE_802_15_4_FOOTER_LEN =                  2,
    IEEE_802_15_4_MIN_FRAME_OVERHEAD_SIZE = (IEEE_802_15_4_MIN_MAC_HEADER_SIZE +
                                             IEEE_802_15_4_FOOTER_LEN),       //11
    IEEE_802_15_4_MAX_FRAME_OVERHEAD_SIZE = (IEEE_802_15_4_MAX_MAC_HEADER_SIZE +
                                             IEEE_802_15_4_FOOTER_LEN),       //39
    IEEE_802_15_4_MAX_SAFE_RX_PAYLOAD_SIZE = (IEEE_802_15_4_FRAME_SIZE -
                                              IEEE_802_15_4_MIN_FRAME_OVERHEAD_SIZE),   //116
    IEEE_802_15_4_MAX_SAFE_TX_PAYLOAD_SIZE = (IEEE_802_15_4_FRAME_SIZE -
                                              IEEE_802_15_4_MAX_FRAME_OVERHEAD_SIZE),   //88
    IEEE_802_15_4_MAX_TX_PAYLOAD_SIZE =      IEEE_802_15_4_MAX_SAFE_RX_PAYLOAD_SIZE,
    IEEE_802_15_4_ACK_SIZE = (2 + 1 + IEEE_802_15_4_FOOTER_LEN), //5 (FCF + seq. + footer)
};

enum ieee_802_15_4_security_mode
{
    IEEE_802_15_4_SEC_NONE         = 0,
    IEEE_802_15_4_SEC_CBC_MAC_4    = 1,
    IEEE_802_15_4_SEC_CBC_MAC_8    = 2,
    IEEE_802_15_4_SEC_CBC_MAC_16   = 3,
    IEEE_802_15_4_SEC_CTR          = 4,
    IEEE_802_15_4_SEC_CCM_4        = 5,
    IEEE_802_15_4_SEC_CCM_8        = 6,
    IEEE_802_15_4_SEC_CCM_16       = 7,
    IEEE_802_15_4_SEC_LAST         = 7,
};
typedef UINT8 IEEE_802_15_4_SEC_MODE;

enum ieee_802_15_4_fcf_mask
{
    IEEE_802_15_4_FCF_MASK_FRAME_TYPE           = 0x0007,
    IEEE_802_15_4_FCF_MASK_SECURITY_ENABLED     = 0x0008,
    IEEE_802_15_4_FCF_MASK_FRAME_PENDING        = 0x0010,
    IEEE_802_15_4_FCF_MASK_ACK_REQUEST          = 0x0020,
    IEEE_802_15_4_FCF_MASK_PAN_ID_COMPRESSION   = 0x0040,
    IEEE_802_15_4_FCF_MASK_DST_ADDRESSING_MODE  = 0x0c00,
    IEEE_802_15_4_FCF_MASK_FRAME_VERSION        = 0x3000,
    IEEE_802_15_4_FCF_MASK_SRC_ADDRESSING_MODE  = 0xc000,
};

enum ieee_802_15_4_fcf
{
    IEEE_802_15_4_FRAME_BEACON      = (0 << 0),
    IEEE_802_15_4_FRAME_DATA        = (1 << 0),
    IEEE_802_15_4_FRAME_ACK         = (2 << 0),
    IEEE_802_15_4_FRAME_MAC_COMMAND = (3 << 0),
    IEEE_802_15_4_SECURITY_ENABLED  = (1 << 3),
    IEEE_802_15_4_ACK_REQUESTED     = (1 << 5),
    IEEE_802_15_4_PAN_ID_COMPRESSED = (1 << 6),
    IEEE_802_15_4_DST_NOT_PRESENT   = (0 << 10),
    IEEE_802_15_4_DST_SHORT_ADDR    = (2 << 10),
    IEEE_802_15_4_DST_LONG_ADDR     = (3 << 10),
    IEEE_802_15_4_VER_2003          = (0 << 12),
    IEEE_802_15_4_VER_2006          = (1 << 12),
    IEEE_802_15_4_SRC_NOT_PRESENT   = (0 << 14),
    IEEE_802_15_4_SRC_SHORT_ADDR    = (2 << 14),
    IEEE_802_15_4_SRC_LONG_ADDR     = (3 << 14),
};

enum ieee_802_15_4_command_frame_type
{
    IEEE_802_15_4_ASSOCIATION_REQUEST           = 0x01,
    IEEE_802_15_4_ASSOCIATION_RESPONSE          = 0x02,
    IEEE_802_15_4_DISASSOCIATION_NOTIFICATION   = 0x03,
    IEEE_802_15_4_DATA_REQUEST                  = 0x04,
    IEEE_802_15_4_PAN_ID_CONFLICT_NOTIFICATION  = 0x05,
    IEEE_802_15_4_ORPHAN_NOTIFICATION           = 0x06,
    IEEE_802_15_4_BEACON_REQUEST                = 0x07,
    IEEE_802_15_4_COORDINATOR_REALIGNMENT       = 0x08,
    IEEE_802_15_4_GTS_REQUEST                   = 0x09,
};

#define FRAME_IS_BEACON(fcf)                                            \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_FRAME_TYPE)               \
     == IEEE_802_15_4_FRAME_BEACON)
#define FRAME_IS_DATA(fcf)                                              \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_FRAME_TYPE)               \
     == IEEE_802_15_4_FRAME_DATA)
#define FRAME_IS_ACK(fcf)                                               \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_FRAME_TYPE)               \
     == IEEE_802_15_4_FRAME_ACK)
#define FRAME_IS_MAC_CMD(fcf)                                           \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_FRAME_TYPE)               \
     == IEEE_802_15_4_FRAME_MAC_COMMAND)

#define FRAME_REQUESTS_ACK(fcf)                                         \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_ACK_REQUEST)              \
     == (UINT16) IEEE_802_15_4_ACK_REQUESTED)
#define FRAME_PANID_COMPRESSED(fcf)                                     \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_PAN_ID_COMPRESSION)       \
     == (UINT16) IEEE_802_15_4_PAN_ID_COMPRESSED)
#define FRAME_CONTAINS_NODST(fcf)                                       \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_DST_ADDRESSING_MODE)      \
     == (UINT16) IEEE_802_15_4_DST_NOT_PRESENT)
#define FRAME_CONTAINS_LONGDST(fcf)                                     \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_DST_ADDRESSING_MODE)      \
     == (UINT16) IEEE_802_15_4_DST_LONG_ADDR)
#define FRAME_CONTAINS_SHORTDST(fcf)                                    \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_DST_ADDRESSING_MODE)      \
     == (UINT16) IEEE_802_15_4_DST_SHORT_ADDR)
#define FRAME_CONTAINS_NOSRC(fcf)                                       \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_SRC_ADDRESSING_MODE)      \
     == (UINT16) IEEE_802_15_4_SRC_NOT_PRESENT)
#define FRAME_CONTAINS_LONGSRC(fcf)                                     \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_SRC_ADDRESSING_MODE)      \
     == (UINT16) IEEE_802_15_4_SRC_LONG_ADDR)
#define FRAME_CONTAINS_SHORTSRC(fcf)                                    \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_SRC_ADDRESSING_MODE)      \
     == (UINT16) IEEE_802_15_4_SRC_SHORT_ADDR)
#define FRAME_CONTAINS_SECHDR(fcf)                                      \
    (((fcf) & (UINT16) IEEE_802_15_4_FCF_MASK_SECURITY_ENABLED)         \
     == (UINT16) IEEE_802_15_4_SECURITY_ENABLED)

enum ieee_802_15_4_mac_header_fields
{
    FRAME_IDX_FCF = 0,
    FRAME_IDX_SEQ = 2,
    FRAME_IDX_DSTPAN = 3,
    FRAME_IDX_DSTADDR = 5,
};
#define FRAME_IDX_SRCADDR(fcf)\
    (2 + 1 + 2 + ((FRAME_CONTAINS_SHORTDST(fcf)) ? 2 : 8)+  \
     ((FRAME_PANID_COMPRESSED(fcf)) ? 0 : 2))

#define FRAME_IDX_SECHDR(fcf)\
    (FRAME_IDX_SRCADDR(fcf) + ((FRAME_CONTAINS_SHORTSRC(fcf) ? 2 : 8)))

#if 0
// Frame control field (16bit)
// L byte (LSB first): Type(3) + Security Enabled(1) + Frmae Pending(1) + Ack request(1) + Intra PAN(1) + Reserved(1)
// H byte (LSB first): Reserved(2) + Destination addressing mode(2) + Reserved(2) + Source addressing mode (2)
// Typical FCF: 1000 1000 01x0 0001 - x: Auto ack on(1)/off(0)
// Data with Ack request
#define NMAC_FCF_ACK_ON 0x8861
#define NMAC_FCF_ACK_ON_L 0x61
#define NMAC_FCF_ACK_ON_H 0x88
// Data without Ack request
#define NMAC_FCF_ACK_OFF 0x8841
#define NMAC_FCF_ACK_OFF_L 0x41
#define NMAC_FCF_ACK_OFF_H 0x88
// Ack
#define NMAC_FCF_ACK_L 0x02
#define NMAC_FCF_ACK_H 0x00
#endif

enum
{
    IEEE_802_15_4_SECCTRL_MASK_SECLEVEL = 0x07,
    IEEE_802_15_4_SECCTRL_MASK_KEYID_MODE = 0x18,
};

enum ieee_802_15_4_sec_keyid_mode
{
    IEEE_802_15_4_SEC_KEYID_MODE0 = (0 << 3),
    IEEE_802_15_4_SEC_KEYID_MODE1 = (1 << 3),
    IEEE_802_15_4_SEC_KEYID_MODE2 = (2 << 3),
    IEEE_802_15_4_SEC_KEYID_MODE3 = (3 << 3),
};

#define AUXSECHDR_KEYID_MODE_IS_0(sc)\
    (((sc) & IEEE_802_15_4_SECCTRL_MASK_KEYID_MODE) ==\
            (UINT8) IEEE_802_15_4_SEC_KEYID_MODE0)
#define AUXSECHDR_KEYID_MODE_IS_1(sc)\
    (((sc) & IEEE_802_15_4_SECCTRL_MASK_KEYID_MODE) ==\
            (UINT8) IEEE_802_15_4_SEC_KEYID_MODE1)
#define AUXSECHDR_KEYID_MODE_IS_2(sc)\
    (((sc) & IEEE_802_15_4_SECCTRL_MASK_KEYID_MODE) ==\
            (UINT8) IEEE_802_15_4_SEC_KEYID_MODE2)
#define AUXSECHDR_KEYID_MODE_IS_3(sc)\
    (((sc) & IEEE_802_15_4_SECCTRL_MASK_KEYID_MODE) ==\
            (UINT8) IEEE_802_15_4_SEC_KEYID_MODE3)

enum
{
    IEEE_802_15_4_SEC_KEY_ENTRIES = 1,
};

// Tx information
typedef struct _nos_mac_tx_info
{
    BOOL use_short_src:1;
    UINT8 frame_type;
    UINT16 dest_addr;
    UINT8 *dest_addr_eui64;
    IEEE_802_15_4_SEC_MODE sec;
    UINT8 sec_key_idx;
    UINT8 header[IEEE_802_15_4_MAX_MAC_HEADER_SIZE];
    UINT8 header_length;
    UINT8 payload[IEEE_802_15_4_MAX_TX_PAYLOAD_SIZE];
    UINT8 payload_length;
    UINT8 sec_mic[16];
    UINT8 sec_mic_length;
    UINT8 tx_count;
} NOS_MAC_TX_INFO;

struct wpan_rx_frame
{
    UINT8 len;
    UINT8 buf[127];
    UINT8 mhr_len;
    UINT8 lqi;
    INT8 rssi;
    BOOL crc_ok:1;
    BOOL secured:1;
};

// Rx information
typedef struct _nos_mac_rx_info
{
    UINT16 src_addr;
    UINT8 src_addr_eui64[8];
    BOOL short_src:1;
    BOOL short_dst:1;
    INT8 rssi;
    UINT8 corr;
    UINT8 payload_length;
    UINT8 payload[IEEE_802_15_4_MAX_SAFE_RX_PAYLOAD_SIZE];
} NOS_MAC_RX_INFO;

UINT8 ieee_802_15_4_get_max_tx_payload_len(BOOL da_eui64, BOOL sa_eui64, UINT8 secmode);

ERROR_T ieee_802_15_4_make_frame(UINT8 *buf,
                                 NOS_MAC_TX_INFO *tx,
                                 UINT8 seq,
                                 UINT16 pan,
                                 const UINT8 *eui64_src,
                                 UINT16 short_src,
                                 BOOL ack);

/**
 * @param ack_sender: ACK sending function pointer.
 */
UINT8 ieee_802_15_4_verify_frame(struct wpan_rx_frame *frame,
                                 BOOL addr_check_required,
                                 UINT16 pan_id,
                                 UINT16 node_id,
                                 const UINT8 *node_eui64);

UINT8 ieee_802_15_4_get_mhr_length(const struct wpan_rx_frame *frame);

#ifdef LIBNOS_AES_M
ERROR_T ieee_802_15_4_secure_frame(NOS_MAC_TX_INFO *tx_info, BOOL ack_required);
ERROR_T ieee_802_15_4_unsecure_frame(NOS_MAC_RXQ_ENTITY *e);
#endif

UINT8 ieee_802_15_4_get_sec_overhead_len(IEEE_802_15_4_SEC_MODE mode);
BOOL ieee_802_15_4_set_sec_key(UINT8 idx, const UINT8 *key);
UINT8 *ieee_802_15_4_get_sec_key(UINT8 idx, UINT8 *key);
BOOL ieee_802_15_4_check_sec_params(IEEE_802_15_4_SEC_MODE mode, UINT8 key_idx);

#endif // IEEE_802_15_4_H_
