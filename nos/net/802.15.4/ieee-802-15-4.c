// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief IEEE 802.15.4 MAC sublayer
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 6. 11.
 */

#include "kconf.h"

#ifdef IEEE_802_15_4_M
#include "ieee-802-15-4.h"
#include <string.h>
#include <stdio.h>
#include "heap.h"

UINT8 ieee_802_15_4_get_max_tx_payload_len(BOOL da_eui64, BOOL sa_eui64, UINT8 secmode)
{
    UINT8 len = IEEE_802_15_4_FRAME_SIZE;

    len -= (2 + 1 + 2) + 2; // (FCF + Seq + PAN ID) + (FCS)
    len -= (da_eui64) ? 8 : 2; // destination address
    len -= (sa_eui64) ? 8 : 2; // source address
    len -= ieee_802_15_4_get_sec_overhead_len(secmode);
    return len;
}

ERROR_T ieee_802_15_4_make_frame(UINT8 *buf,
                                 NOS_MAC_TX_INFO *tx,
                                 UINT8 seq,
                                 UINT16 pan,
                                 const UINT8 *eui64_src,
                                 UINT16 short_src,
                                 BOOL ack)
{
    UINT16 fcf;
    UINT8 i;

    if (!tx || !buf)
        return ERROR_INVALID_ARGS;

    fcf = tx->frame_type;
    
    i = 6;

    if (tx->frame_type == IEEE_802_15_4_FRAME_BEACON)
    {
        //No destination address 
        fcf |= (UINT16) IEEE_802_15_4_DST_NOT_PRESENT;
    }
    else if (tx->frame_type == IEEE_802_15_4_FRAME_DATA ||
             tx->frame_type == IEEE_802_15_4_FRAME_MAC_COMMAND)
    {
        if (tx->dest_addr_eui64)
        {
            fcf |= (UINT16) IEEE_802_15_4_DST_LONG_ADDR;
            buf[i++] = tx->dest_addr_eui64[7];
            buf[i++] = tx->dest_addr_eui64[6];
            buf[i++] = tx->dest_addr_eui64[5];
            buf[i++] = tx->dest_addr_eui64[4];
            buf[i++] = tx->dest_addr_eui64[3];
            buf[i++] = tx->dest_addr_eui64[2];
            buf[i++] = tx->dest_addr_eui64[1];
            buf[i++] = tx->dest_addr_eui64[0];
        }
        else
        {
            fcf |= (UINT16) IEEE_802_15_4_DST_SHORT_ADDR;
            buf[i++] = tx->dest_addr & 0xff;
            buf[i++] = tx->dest_addr >> 8;
        }
    }
    else
    {
        return ERROR_INVALID_ARGS;
    }

    if (eui64_src)
    {
        fcf |= (UINT16) IEEE_802_15_4_SRC_LONG_ADDR;
        buf[i++] = eui64_src[7];
        buf[i++] = eui64_src[6];
        buf[i++] = eui64_src[5];
        buf[i++] = eui64_src[4];
        buf[i++] = eui64_src[3];
        buf[i++] = eui64_src[2];
        buf[i++] = eui64_src[1];
        buf[i++] = eui64_src[0];
    }
    else if (short_src != 0xffff)
    {
        fcf |= (UINT16) IEEE_802_15_4_SRC_SHORT_ADDR;
        buf[i++] = short_src & 0xff;
        buf[i++] = short_src >> 8;
    }
    else
    {
        fcf |= IEEE_802_15_4_SRC_NOT_PRESENT;
    }

    if (!FRAME_CONTAINS_NODST(fcf) && !FRAME_CONTAINS_NOSRC(fcf))
    {
        //TODO Intra PAN communication except beacon is only supported currently.
        fcf |= IEEE_802_15_4_PAN_ID_COMPRESSED;
    }

    if (ack)
    {
        fcf |= IEEE_802_15_4_ACK_REQUESTED;
    }

#ifdef IEEE_802_15_4_SECURITY_M
    if (tx->sec != IEEE_802_15_4_SEC_NONE)
        fcf |= IEEE_802_15_4_SECURITY_ENABLED;
#endif

    buf[1] = fcf & 0xff;
    buf[2] = fcf >> 8;
    buf[3] = seq;
    buf[4] = pan & 0xff;
    buf[5] = pan >> 8;

#ifdef IEEE_802_15_4_SECURITY_M
    if (tx->sec != IEEE_802_15_4_SEC_NONE)
    {
        static UINT32 sec_frame_counter = 0;

        buf[i++] = ((tx->sec << 0) +
                    IEEE_802_15_4_SEC_KEYID_MODE1);   // default in NanoQplus
        buf[i++] = sec_frame_counter & 0xff;
        buf[i++] = (sec_frame_counter >> 8) & 0xff;
        buf[i++] = (sec_frame_counter >> 16) & 0xff;
        buf[i++] = (sec_frame_counter >> 24) & 0xff;
        buf[i++] = tx->sec_key_idx;

        sec_frame_counter++;
        buf[0] = i - 1;

        // For authentication tag length.
        if (tx->sec == IEEE_802_15_4_SEC_CBC_MAC_4 ||
            tx->sec == IEEE_802_15_4_SEC_CCM_4)
        {
            buf[0] += 4;
        }
        else if (tx->sec == IEEE_802_15_4_SEC_CBC_MAC_8 ||
                 tx->sec == IEEE_802_15_4_SEC_CCM_8)
        {
            buf[0] += 8;
        }
        else if (tx->sec == IEEE_802_15_4_SEC_CBC_MAC_16 ||
                 tx->sec == IEEE_802_15_4_SEC_CCM_16)
        {
            buf[0] += 16;
        }
    }
    else
    {
        buf[0] = i - 1;
    }
#else
    buf[0] = i - 1;
#endif

    buf[0] += 2; //FCS
    
    if (buf[0] + tx->payload_length > IEEE_802_15_4_FRAME_SIZE)
        return ERROR_802154_FRAME_TOO_BIG;
    
    memcpy(&buf[i], tx->payload, tx->payload_length);
    buf[0] += tx->payload_length;

    return ERROR_SUCCESS;
}

//#define CONVERT_DEBUG

UINT8 ieee_802_15_4_verify_frame(struct wpan_rx_frame *frame,
                                 BOOL addr_check_required,
                                 UINT16 pan_id,
                                 UINT16 node_id,
                                 const UINT8 *node_eui64)
{
    UINT8 len;
    UINT16 fcf, tmp;

#ifdef CONVERT_DEBUG
    printf("Total len:%d\n", frame->len);
    for (len = 0; len < frame->len; len++)
    {
        printf("%02X ", frame->buf[len]);
    }
    printf("\n");
#endif
    
    if (frame->len < (2 + 1) || frame->len > 127)
    {
#ifdef CONVERT_DEBUG
        printf("invalid len\n");
#endif
        return 0;
    }
    
    fcf = frame->buf[FRAME_IDX_FCF] + (frame->buf[FRAME_IDX_FCF + 1] << 8);
#ifdef CONVERT_DEBUG
    printf("FCF:%04X\n", fcf);
#endif
    len = frame->len - (2 + 1);

    if (FRAME_IS_ACK(fcf) &&
        frame->len == IEEE_802_15_4_ACK_SIZE - IEEE_802_15_4_FOOTER_LEN)
    {
        //Ack frame
        return 0;
    }

    // Destination PAN ID
    if (FRAME_CONTAINS_LONGDST(fcf) || FRAME_CONTAINS_SHORTDST(fcf))
    {
        if (len >= 2)
        {
            len -= 2;
        }
        else
        {
#ifdef CONVERT_DEBUG
            printf("invalid len3 (len:%d)\n", len);
#endif
            return 0;
        }
        
        if (addr_check_required)
        {
            tmp = (frame->buf[FRAME_IDX_DSTPAN] +
                   (frame->buf[FRAME_IDX_DSTPAN + 1] << 8));

            if (tmp != 0xFFFF && tmp != pan_id)
            {
#ifdef CONVERT_DEBUG
                printf("wrong PAN\n");
#endif
                return 0;
            }            
        }
    }

    // Destination node ID
    if (node_eui64 != NULL &&
        FRAME_CONTAINS_LONGDST(fcf) &&
        len >= 8)
    {
        if (addr_check_required)
        {
            if (frame->buf[FRAME_IDX_DSTADDR + 0] != node_eui64[7] ||
                frame->buf[FRAME_IDX_DSTADDR + 1] != node_eui64[6] ||
                frame->buf[FRAME_IDX_DSTADDR + 2] != node_eui64[5] ||
                frame->buf[FRAME_IDX_DSTADDR + 3] != node_eui64[4] ||
                frame->buf[FRAME_IDX_DSTADDR + 4] != node_eui64[3] ||
                frame->buf[FRAME_IDX_DSTADDR + 5] != node_eui64[2] ||
                frame->buf[FRAME_IDX_DSTADDR + 6] != node_eui64[1] ||
                frame->buf[FRAME_IDX_DSTADDR + 7] != node_eui64[0])
            {
#ifdef CONVERT_DEBUG
                printf("wrong dst long\n");
#endif
                return 0;
            }

            len -= 8;
        }
    }
    else if (FRAME_CONTAINS_SHORTDST(fcf) &&
             len >= 2)
    {     
        if (addr_check_required)
        {
            tmp = (frame->buf[FRAME_IDX_DSTADDR] +
                   (frame->buf[FRAME_IDX_DSTADDR + 1] << 8));
            if (tmp != 0xFFFF &&
                tmp != node_id)
            {
#ifdef CONVERT_DEBUG
                printf("wrong dst short\n");
#endif
                return 0;
            }
        }
        len -= 2;
    }
    else if (!FRAME_CONTAINS_NODST(fcf))
    {
#ifdef CONVERT_DEBUG
        printf("invalid dst addressing\n");
#endif
        return 0;
    }

    // Source PAN ID
    if ((FRAME_CONTAINS_LONGSRC(fcf) || FRAME_CONTAINS_SHORTSRC(fcf)) &&
        !FRAME_PANID_COMPRESSED(fcf))
    {
        if (len >= 2)
        {
            len -= 2;
        }
        else
        {
#ifdef CONVERT_DEBUG
            printf("invalid len3 (len:%d)\n", len);
#endif
            return 0;
        }
    }

    // source address
    if (FRAME_CONTAINS_LONGSRC(fcf) && len >= 8)
    {
        len -= 8;
    }
    else if (FRAME_CONTAINS_SHORTSRC(fcf) && len >= 2)
    {
        len -= 2;
    }
    else if (!FRAME_CONTAINS_NOSRC(fcf))
    {
#ifdef CONVERT_DEBUG
        printf("invalid src addressing\n");
#endif
        return 0;
    }

    // Security header
    if (FRAME_CONTAINS_SECHDR(fcf) /*&& len >= SECHDR_LEN + MIC_LEN*/)
    {
        //TODO
        /*len -= SECHDR_LEN + MIC_LEN;*/

#ifdef CONVERT_DEBUG
        printf("not supported\n");
#endif
        return 0; // not supported.
    }

    return frame->len - len;
}

#endif // IEEE_802_15_4_M
