// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief IEEE 802.15.4 common functions
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 10. 22.
 */

#include "kconf.h"

#ifdef IEEE_802_15_4_M
#include "ieee-802-15-4.h"
#include <string.h>
#include "heap.h"


#ifdef IEEE_802_15_4_SECURITY_M

#ifdef LIBNOS_AES_M
#include "aes.h"
#endif

struct ieee_802_15_4_sec_key
{
    UINT8 data[16];
};

static struct ieee_802_15_4_sec_key sec_key[IEEE_802_15_4_SEC_KEY_ENTRIES];
#endif /* IEEE_802_15_4_SECURITY_M */


UINT8 ieee_802_15_4_get_sec_overhead_len(IEEE_802_15_4_SEC_MODE mode)
{
    // Exclude auxiliary security header and MIC.
#ifdef IEEE_802_15_4_SECURITY_M
    if (mode == IEEE_802_15_4_SEC_CBC_MAC_4 ||
            mode == IEEE_802_15_4_SEC_CCM_4)
    {
        return (1 + 4 + 1) + 4;
    }
    else if (mode == IEEE_802_15_4_SEC_CBC_MAC_8 ||
            mode == IEEE_802_15_4_SEC_CCM_8)
    {
        return (1 + 4 + 1) + 8;
    }
    else if (mode == IEEE_802_15_4_SEC_CBC_MAC_16 ||
            mode == IEEE_802_15_4_SEC_CCM_16)
    {
        return (1 + 4 + 1) + 16;
    }
    else if (mode == IEEE_802_15_4_SEC_CTR)
    {
        return (1 + 4 + 1);
    }
    else
    {
        return 0;
    }
#else
    return 0;
#endif /* IEEE_802_15_4_SECURITY_M */
}

#ifdef IEEE_802_15_4_SECURITY_M
#ifdef LIBNOS_AES_M
ERROR_T ieee_802_15_4_secure_frame(NOS_MAC_TX_INFO *tx_info, BOOL ack_required)
{
    UINT8 *auth;
    UINT8 auth_len;
    UINT8 auth_len_aligned;

    UINT8 *nonce;

    UINT8 *plain_text;
    UINT8 plain_text_len;
    UINT8 plain_text_len_aligned;

    UINT16 fcf;
    UINT8 idx_src;
    UINT8 idx_frame_cnt;
    ERROR_T ret;

    if (tx_info->sec == IEEE_802_15_4_SEC_NONE)
        return SUCCESS_NOTHING_HAPPENED;

    if (tx_info->sec_key_idx >= IEEE_802_15_4_SEC_KEY_ENTRIES)
        return ERROR_INVALID_ARGS;

    auth_len = 2 + tx_info->header_length;
    auth_len_aligned = (auth_len % 16 == 0) ?
            auth_len : ((auth_len / 16) + 1) * 16;

    plain_text_len = tx_info->payload_length;
    plain_text_len_aligned = (plain_text_len % 16 == 0) ?
            plain_text_len : ((plain_text_len / 16) + 1) * 16;

    nonce = (UINT8 *) nos_malloc(16 + auth_len_aligned + plain_text_len_aligned);
    if (nonce == NULL)
        return ERROR_NOT_ENOUGH_MEMORY;

    auth = nonce + 16;
    auth[0] = (auth_len - 2) >> 8;
    auth[1] = (auth_len - 2) & 0xff;

    memcpy(&auth[2], tx_info->header, tx_info->header_length);
    if (auth_len < auth_len_aligned)
        memset(&auth[auth_len], 0, auth_len_aligned - auth_len);

    plain_text = auth + auth_len_aligned;
    memcpy(plain_text, tx_info->payload, tx_info->payload_length);
    if (plain_text_len < plain_text_len_aligned)
        memset(&auth[auth_len_aligned + plain_text_len], 0,
                plain_text_len_aligned - plain_text_len);

    fcf = tx_info->header[0] + ((UINT16) tx_info->header[1] << 8);
    idx_src = FRAME_IDX_SRCADDR(fcf);
    if (FRAME_CONTAINS_LONGSRC(fcf))
    {
        memcpy(&nonce[1], &tx_info->header[idx_src], 8);
        idx_frame_cnt = idx_src + 8 + 1;
    }
    else
    {
        memset(&nonce[1], 0, 8);
        idx_frame_cnt = idx_src + 2 + 1;
    }
    nonce[9] = tx_info->header[idx_frame_cnt];
    nonce[10] = tx_info->header[idx_frame_cnt + 1];
    nonce[11] = tx_info->header[idx_frame_cnt + 2];
    nonce[12] = tx_info->header[idx_frame_cnt + 3];
    nonce[13] = tx_info->sec;

    switch (tx_info->sec)
    {
    case IEEE_802_15_4_SEC_CBC_MAC_4:
        nonce[0] = 0x49;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_cbcmac128(tx_info->sec_mic, auth,
                auth_len_aligned + plain_text_len_aligned, nonce,
                sec_key[tx_info->sec_key_idx].data))
        {
            ret = ERROR_802154_SECURING_FAIL;
            break;
        }
        tx_info->sec_mic_length = 4;
        ret = ERROR_SUCCESS;
        break;

    case IEEE_802_15_4_SEC_CBC_MAC_8:
        nonce[0] = 0x59;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_cbcmac128(tx_info->sec_mic, auth,
                auth_len_aligned + plain_text_len_aligned, nonce,
                sec_key[tx_info->sec_key_idx].data))
        {
            ret = ERROR_802154_SECURING_FAIL;
        }
        else
        {
            tx_info->sec_mic_length = 8;
            ret = ERROR_SUCCESS;
        }
        break;

    case IEEE_802_15_4_SEC_CBC_MAC_16:
        nonce[0] = 0x79;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_cbcmac128(tx_info->sec_mic, auth,
                auth_len_aligned + plain_text_len_aligned, nonce,
                sec_key[tx_info->sec_key_idx].data))
        {
            ret = ERROR_802154_SECURING_FAIL;
        }
        else
        {
            tx_info->sec_mic_length = 16;
            ret = ERROR_SUCCESS;
        }
        break;

    case IEEE_802_15_4_SEC_CTR:
        nonce[0] = 0x01;
        nonce[14] = 0x00;
        nonce[15] = 0x00;

        if (!nos_aes_ctr128(tx_info->payload, plain_text, plain_text_len_aligned,
                nonce, sec_key[tx_info->sec_key_idx].data))
        {
            ret = ERROR_802154_SECURING_FAIL;
        }
        else
        {
            tx_info->sec_mic_length = 0;
            ret = ERROR_SUCCESS;
        }
        break;

    case IEEE_802_15_4_SEC_CCM_4:
        nonce[0] = 0x49;
        nonce[14] = 0x00;
        nonce[15] = plain_text_len;

        if (!nos_aes_cbcmac128(tx_info->sec_mic, auth,
                auth_len_aligned + plain_text_len_aligned, nonce,
                sec_key[tx_info->sec_key_idx].data))
        {
            ret = ERROR_802154_SECURING_FAIL;
            break;
        }
        tx_info->sec_mic_length = 4;

        nonce[0] = 0x01;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_ctr128(tx_info->payload, plain_text, plain_text_len_aligned,
                nonce, sec_key[tx_info->sec_key_idx].data))
            ret = ERROR_802154_SECURING_FAIL;
        else
            ret = ERROR_SUCCESS;
        break;

    case IEEE_802_15_4_SEC_CCM_8:
        nonce[0] = 0x59;
        nonce[14] = 0x00;
        nonce[15] = plain_text_len;
        if (!nos_aes_cbcmac128(tx_info->sec_mic, auth,
                auth_len_aligned + plain_text_len_aligned, nonce,
                sec_key[tx_info->sec_key_idx].data))
        {
            ret = ERROR_802154_SECURING_FAIL;
            break;
        }
        tx_info->sec_mic_length = 8;

        nonce[0] = 0x01;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_ctr128(tx_info->payload, plain_text, plain_text_len_aligned,
                nonce, sec_key[tx_info->sec_key_idx].data))
            ret = ERROR_802154_SECURING_FAIL;
        else
            ret = ERROR_SUCCESS;
        break;

    case IEEE_802_15_4_SEC_CCM_16:
        nonce[0] = 0x79;
        nonce[14] = 0x00;
        nonce[15] = plain_text_len;
        if (!nos_aes_cbcmac128(tx_info->sec_mic, auth,
                auth_len_aligned + plain_text_len_aligned, nonce,
                sec_key[tx_info->sec_key_idx].data))
        {
            ret = ERROR_802154_SECURING_FAIL;
            break;
        }
        tx_info->sec_mic_length = 16;

        nonce[0] = 0x01;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_ctr128(tx_info->payload, plain_text, plain_text_len_aligned,
                nonce, sec_key[tx_info->sec_key_idx].data))
            ret = ERROR_802154_SECURING_FAIL;
        else
            ret = ERROR_SUCCESS;
        break;
    }

    nos_free(nonce);

    return ret;
}

ERROR_T ieee_802_15_4_unsecure_frame(NOS_MAC_RXQ_ENTITY *e)
{
    UINT8 *nonce;
    UINT8 *auth;
    UINT8 authlen;
    UINT8 authlen_aligned;

    UINT8 *cipher;
    UINT8 cipherlen;
    UINT8 cipherlen_aligned;

    UINT16 fcf;
    UINT8 idx_src;
    UINT8 idx_sechdr;
    UINT8 keyidx;

    UINT8 mic[16];
    ERROR_T ret;

    authlen = 2 + e->header_length;
    authlen_aligned = (authlen % 16 == 0) ?
            authlen : ((authlen / 16) + 1) * 16;

    cipherlen = e->payload_length;
    cipherlen_aligned = (cipherlen % 16 == 0) ?
            cipherlen : ((cipherlen / 16) + 1) * 16;

    nonce = nos_malloc(16 + authlen_aligned + cipherlen_aligned);
    if (nonce == NULL)
        return ERROR_NOT_ENOUGH_MEMORY;

    auth = nonce + 16;
    auth[0] = (authlen - 2) >> 8;
    auth[1] = (authlen - 2) & 0xff;

    memcpy(&auth[2], e->header, e->header_length);
    if (authlen < authlen_aligned)
        memset(&auth[authlen], 0, authlen_aligned - authlen);

    cipher = auth + authlen_aligned;
    memcpy(cipher, e->payload, e->payload_length);
    if (cipherlen < cipherlen_aligned)
        memset(&cipher[cipherlen], 0, cipherlen_aligned - cipherlen);

    fcf = e->header[0] + ((UINT16) e->header[1] << 8);
    idx_src = FRAME_IDX_SRCADDR(fcf);
    if (FRAME_CONTAINS_LONGSRC(fcf))
    {
        memcpy(&nonce[1], &e->header[idx_src], 8);
        idx_sechdr = idx_src + 8;
    }
    else
    {
        memset(&nonce[1], 0, 8);
        idx_sechdr = idx_src + 2;
    }

    nonce[9] = e->header[idx_sechdr + 1];
    nonce[10] = e->header[idx_sechdr + 2];
    nonce[11] = e->header[idx_sechdr + 3];
    nonce[12] = e->header[idx_sechdr + 4];
    nonce[13] = e->header[idx_sechdr] & IEEE_802_15_4_SECCTRL_MASK_SECLEVEL;

    keyidx = 0;
    switch (e->header[idx_sechdr] & IEEE_802_15_4_SECCTRL_MASK_KEYID_MODE)
    {
    case IEEE_802_15_4_SEC_KEYID_MODE0:
        ret = ERROR_NOT_SUPPORTED;
        goto drop;

    case IEEE_802_15_4_SEC_KEYID_MODE1:
        keyidx = e->header[idx_sechdr + 5];
        break;

    case IEEE_802_15_4_SEC_KEYID_MODE2:
        ret = ERROR_NOT_SUPPORTED;
        goto drop;

    case IEEE_802_15_4_SEC_KEYID_MODE3:
        ret = ERROR_NOT_SUPPORTED;
        goto drop;
    }

    switch (e->header[idx_sechdr] & IEEE_802_15_4_SECCTRL_MASK_SECLEVEL)
    {
    case IEEE_802_15_4_SEC_CBC_MAC_4:
        nonce[0] = 0x49;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_cbcmac128(mic, auth,
                authlen_aligned + cipherlen_aligned, nonce,
                sec_key[keyidx].data))
        {
            ret = ERROR_802154_UNSECURING_FAIL;
            break;
        }

        if (memcmp(mic, e->sec_mic, 4) != 0)
            ret = ERROR_802154_INVALID_SEC_FRAME;
        else
            ret = ERROR_SUCCESS;
        break;

    case IEEE_802_15_4_SEC_CBC_MAC_8:
        nonce[0] = 0x59;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_cbcmac128(mic, auth,
                authlen_aligned + cipherlen_aligned, nonce,
                sec_key[keyidx].data))
        {
            ret = ERROR_802154_UNSECURING_FAIL;
            break;
        }

        if (memcmp(mic, e->sec_mic, 8) != 0)
            ret = ERROR_802154_INVALID_SEC_FRAME;
        else
            ret = ERROR_SUCCESS;
        break;

    case IEEE_802_15_4_SEC_CBC_MAC_16:
        nonce[0] = 0x79;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_cbcmac128(mic, auth,
                authlen_aligned + cipherlen_aligned, nonce,
                sec_key[keyidx].data))
        {
            ret = ERROR_802154_UNSECURING_FAIL;
            break;
        }

        if (memcmp(mic, e->sec_mic, 16) != 0)
            ret = ERROR_802154_INVALID_SEC_FRAME;
        else
            ret = ERROR_SUCCESS;
        break;

    case IEEE_802_15_4_SEC_CTR:
        nonce[0] = 0x01;
        nonce[14] = 0x00;
        nonce[15] = 0x00;

        if (!nos_aes_ctr128(e->payload, cipher, cipherlen_aligned,
                nonce, sec_key[keyidx].data))
            ret = ERROR_802154_UNSECURING_FAIL;
        else
            ret = ERROR_SUCCESS;
        break;

    case IEEE_802_15_4_SEC_CCM_4:
        nonce[0] = 0x01;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_ctr128(cipher, cipher, cipherlen_aligned,
                nonce, sec_key[keyidx].data))
        {
            ret = ERROR_802154_UNSECURING_FAIL;
            break;
        }

        memset(&cipher[cipherlen], 0, cipherlen_aligned - cipherlen);
        nonce[0] = 0x49;
        nonce[14] = 0x00;
        nonce[15] = cipherlen;
        if (!nos_aes_cbcmac128(mic, auth,
                authlen_aligned + cipherlen_aligned, nonce,
                sec_key[keyidx].data))
        {
            ret = ERROR_802154_UNSECURING_FAIL;
            break;
        }

        if (memcmp(mic, e->sec_mic, 4) != 0)
            ret = ERROR_802154_INVALID_SEC_FRAME;
        else
        {
            memcpy(e->payload, cipher, cipherlen);
            ret = ERROR_SUCCESS;
        }
        break;

    case IEEE_802_15_4_SEC_CCM_8:
        nonce[0] = 0x01;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_ctr128(cipher, cipher, cipherlen_aligned,
                nonce, sec_key[keyidx].data))
        {
            ret = ERROR_802154_UNSECURING_FAIL;
            break;
        }

        memset(&cipher[cipherlen], 0, cipherlen_aligned - cipherlen);
        nonce[0] = 0x59;
        nonce[14] = 0x00;
        nonce[15] = cipherlen;
        if (!nos_aes_cbcmac128(mic, auth,
                authlen_aligned + cipherlen_aligned, nonce,
                sec_key[keyidx].data))
        {
            ret = ERROR_802154_UNSECURING_FAIL;
            break;
        }

        if (memcmp(mic, e->sec_mic, 8) != 0)
            ret = ERROR_802154_INVALID_SEC_FRAME;
        else
        {
            memcpy(e->payload, cipher, cipherlen);
            ret = ERROR_SUCCESS;
        }
        break;

    case IEEE_802_15_4_SEC_CCM_16:
        nonce[0] = 0x01;
        nonce[14] = 0x00;
        nonce[15] = 0x00;
        if (!nos_aes_ctr128(cipher, cipher, cipherlen_aligned,
                nonce, sec_key[keyidx].data))
        {
            ret = ERROR_802154_UNSECURING_FAIL;
            break;
        }

        memset(&cipher[cipherlen], 0, cipherlen_aligned - cipherlen);
        nonce[0] = 0x79;
        nonce[14] = 0x00;
        nonce[15] = cipherlen;
        if (!nos_aes_cbcmac128(mic, auth,
                authlen_aligned + cipherlen_aligned, nonce,
                sec_key[keyidx].data))
        {
            ret = ERROR_802154_UNSECURING_FAIL;
            break;
        }

        if (memcmp(mic, e->sec_mic, 16) != 0)
            ret = ERROR_802154_INVALID_SEC_FRAME;
        else
        {
            memcpy(e->payload, cipher, cipherlen);
            ret = ERROR_SUCCESS;
        }
        break;
    }

    drop:
    nos_free(nonce);
    return ret;
}
#endif /* LIBNOS_AES_M */
#endif /* IEEE_802_15_4_SECURITY_M */

BOOL ieee_802_15_4_set_sec_key(UINT8 idx, const UINT8 *key)
{
#ifdef IEEE_802_15_4_SECURITY_M
    if (idx < IEEE_802_15_4_SEC_KEY_ENTRIES)
    {
        memcpy(sec_key[idx].data, key, 16);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
#else
    return FALSE;
#endif /* IEEE_802_15_4_SECURITY_M */
}

UINT8 *ieee_802_15_4_get_sec_key(UINT8 idx, UINT8 *key)
{
#ifdef IEEE_802_15_4_SECURITY_M
    if (idx < IEEE_802_15_4_SEC_KEY_ENTRIES && key != NULL)
    {
        memcpy(key, sec_key[idx].data, 16);
        return key;
    }
    else
    {
        return NULL;
    }
#else
    return NULL;
#endif /* IEEE_802_15_4_SECURITY_M */
}

BOOL ieee_802_15_4_check_sec_params(IEEE_802_15_4_SEC_MODE mode, UINT8 key_idx)
{
    if (mode == IEEE_802_15_4_SEC_NONE)
    {
        return TRUE;
    }
#ifdef IEEE_802_15_4_SECURITY_M
    else if (mode == IEEE_802_15_4_SEC_CBC_MAC_4 ||
             mode == IEEE_802_15_4_SEC_CBC_MAC_8 ||
             mode == IEEE_802_15_4_SEC_CBC_MAC_16 ||
             mode == IEEE_802_15_4_SEC_CTR ||
             mode == IEEE_802_15_4_SEC_CCM_4 ||
             mode == IEEE_802_15_4_SEC_CCM_8 ||
             mode == IEEE_802_15_4_SEC_CCM_16)
    {
        return (key_idx < IEEE_802_15_4_SEC_KEY_ENTRIES);
    }
#endif /* IEEE_802_15_4_SECURITY_M */
    else
    {
        return FALSE;
    }
}
#endif // IEEE_802_15_4_M
