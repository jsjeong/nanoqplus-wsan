// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2014
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file nos_init.c
 *
 * NanoQplus Initializer
 *
 * @date 2014. 2. 12.
 * @author Sangcheol Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "kconf.h"
#include "nos.h"

#include "platform.h"
#include "arch.h"

#ifdef KERNEL_M
#include "kernel.h"
#endif

void nos_init(void)
{
    NOS_DISABLE_GLOBAL_INTERRUPT();
    nos_arch_init();
    nos_platform_init();
    
#ifdef KERNEL_M
    nos_kernel_init();
#endif

#ifdef INET_M
    ip6_core_init(CONFIG_MAX_NUM_IP6_NET_INTERFACE,
                  CONFIG_MAX_NUM_IP6_ROUTING);
#endif
    
#ifdef RPL_M
    rpl_core_init(CONFIG_MAX_NUM_RPL_INTERFACE);
#endif
#ifdef RPL_OF0_M
    rpl_enable_of0();
#endif

#ifdef UDP_M
    udp_core_init(CONFIG_MAX_NUM_UDP_PORT);
#endif

#ifdef TCP_M
    tcp_core_init(CONFIG_MAX_NUM_TCP_CONN);
#endif
    
#ifdef LOWPAN_M
    lowpan_core_init(CONFIG_MAX_NUM_6LOWPAN_INTERFACE);
#endif
#ifdef ETHERNET6_M
    ether6_core_init(CONFIG_MAX_NUM_ETHER6_INTERFACE);
#endif
    NOS_ENABLE_GLOBAL_INTERRUPT();
}
