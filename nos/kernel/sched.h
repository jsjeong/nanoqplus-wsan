// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef SCHED_H
#define SCHED_H
#include "kconf.h"
#ifdef KERNEL_M

//#define STACK_DEBUG
#ifdef STACK_DEBUG
#include "platform.h"
#include "uart.h"
#endif

#include "nos_common.h"

#include "hal_thread.h"

#ifndef THREAD_EXT_M
#include "threadq.h"
#else
#include "threadq_ext.h"
#endif

// the maximum number of threads supported
#ifndef THREAD_EXT_M
enum { MAX_NUM_USER_THREAD = (5) }; // the number of user threads
#else
enum { MAX_NUM_USER_THREAD = (15) }; // the number of user threads
#endif

enum { MAX_NUM_TOTAL_THREAD = (MAX_NUM_USER_THREAD+1) }; // the number of user threads + idle thread

// Variables and Functions
// thread control block
typedef struct _tcb
{
    void (*func)(void *);       // function pointer
    void *args_data;            // function arguments
    UINT8 id;                   // thread id (tid)
    UINT8 state;                // thread state
    UINT8 priority;             // thread priority
#ifdef THREAD_M
    UINT16 sleep_tick;          // the number of ticks to sleep; sleepdetected whenever timer event occurs
    THREADQ waitq;              // a set of threads for this thread to exit
    STACK_PTR sptr;             // thread stack pointer
    UINT16 stack_size;          // thread stack size
    STACK_PTR stack_start_addr; // to detect stack overflow, but (main) idle thread has no limit
#endif
} _TCB;

// priority setting
// LOWEST <<----------------->> HIGHEST
//   0     1     2     3     4     5 
#define PRIORITY_LEVEL_COUNT 6  // 6 prorities available - assignment : SYSTEM(0, 5), USER(1-4)
enum
{
    PRIORITY_ULTRA = 5,     // reserved for the system thread
    NOS_PRIORITY_HIGHEST = 4,
    NOS_PRIORITY_HIGH = 3,
    NOS_PRIORITY_NORMAL = 2,
    NOS_PRIORITY_LOW = 1,
    PRIORITY_LOWEST = 0,    // reserved for the idle thread
};

extern volatile BOOL _sched_enable_bit;
#define NOS_ENABLE_SCHED() do {_sched_enable_bit = 1;} while (0)
#define NOS_DISABLE_SCHED() do {_sched_enable_bit = 0;} while (0)

//extern UINT8 nos_ctx_sw_intr_exists;
extern volatile UINT8  _rtid, _next_rtid;  // running thread id (to indicate which thread is currently running)
extern volatile _TCB *tcb[MAX_NUM_TOTAL_THREAD]; // tcb pointer array

void nos_ctx_sw_handler(void);
void nos_sched_handler(void);
void nos_ctx_sw(void);
void nos_sched_init(void);
void nos_sched_start(void);

#endif // KERNEL_M
#endif // ~SCHED_H
