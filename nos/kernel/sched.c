// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 * @author Haeyong Kim (ETRI)
 */

#include "sched.h"

#ifdef KERNEL_M

#include <string.h>
#include "hal_sched.h" 
#include "hal_thread.h"
#include "heap.h"
#include "arch.h"
#include "critical_section.h"
#include "pwr_mgr.h"

#include "hal_thread.h"
#include "user_timer.h"
#include "thread.h"
#include "taskq.h"

#include <stdio.h>

/****sched variables***/

/* Running thread IDs (to indicate which thread is currently running) */
volatile UINT8 _rtid, _next_rtid;

/* Priority bit setting for threads, which is set for the priority of the thread
   when the thread becomes in ready state. */
volatile UINT8 _ready_priority_bit;

#ifndef THREAD_EXT_M
/* Created threads' ID bitmap used for avoiding duplicate thread ID */
UINT8 _created_tid_bit;
UINT8 _suspended_tid_bit;
/* Sleeping threads' ID bitmap indicates whether a thread is in sleeping state or not. */
UINT8 _sleep_tid_bit;
#else
UINT16 _created_tid_bit;
UINT16 _suspended_tid_bit;
UINT16 _sleep_tid_bit;
#endif
volatile BOOL _sched_enable_bit;

#ifdef USER_TIMER_M
volatile UINT16 nos_sched_tick;
extern volatile UINT16 nos_next_user_timer_expire_tick;
extern UINT16 nos_user_timer_activated_bit;
extern volatile UINT16 nos_user_timer_expired_bit;
extern NOS_USER_TIMER nos_user_timer[NOS_MAX_NUM_USER_TIMER];
extern void nos_user_timer_exe(void);
#endif

extern volatile UINT8 taskq_head;
extern volatile UINT8 taskq_tail;

UINT16 _init_ticks_left; // ticks left until first wake-up (initially setting)
UINT16 _ticks_left;      // ticks left currently

volatile THREADQ _rdy_q[PRIORITY_LEVEL_COUNT]; 
// array of ready queues for each thread priority
// this queue enables to keep the order among threads with same priorities
// The concept looks like this :
//
//      prio[7] prio[6] prio[5] prio[4] prio[3] prio[2] prio[1] prio[0]
//        |       |       |       |       |       |        |      |
//       ---     ---     ---     ---     ---     ---      ---    ---
//       |-|     |-|     |-|     |-|     |-|     |-|      |-|    |-|
//       |-|     |-|     |-|     |-|     |-|     |-|      |-|    |-|
//       |-|     |-|     |-|     |-|     |-|     |-|      |-|    |-|
//       |-|     |-|     |-|     |-|     |-|     |-|      |-|    |-|
//       ---     ---     ---     ---     ---     ---      ---    ---
//     _rdy_q[7] ...     ...  _rdy_q[4]  ...  _rdy_q[2]  ...   _rdy_q[0]
//

volatile _TCB *tcb[MAX_NUM_TOTAL_THREAD]; // tcb pointer array
struct _tcb kernel_tcb;

/***************
If context switch interrupt exists and nested interrupt is supported,

1. Sched interrupt (the highest priority)

  nos_sched_handler();	
  NOS_CTX_SW_PENDING_SET(); // pending 'ctx_sw_interrupt

2. Context switch interrupt
   (the lowest priority. Do not use functions realted to '_nested_intr_cnt')

  nos_ctx_sw_handler();	// may return here from interrupt. And it will turn on global interrupt

If context switch interrupt exists and nested interrupt is not supported,

1. Sched interrupt (the highest priority)

  NOS_ENTER_ISR();
	nos_sched_handler();
  NOS_EXIT_ISR();
	NOS_CTX_SW_PENDING_SET(); // pending 'ctx_sw_interrupt
	
2. Context switch interrupt
   (the lowest priority. Do not use functions realted to '_nested_intr_cnt')

	nos_ctx_sw_handler();	// may return here from interrupt. And it will turn on global interrupt

or if ctx_sw interrupt does not exist,

1. Sched interrupt

  NOS_ENTER_ISR();
	nos_sched_handler();
  NOS_EXIT_ISR();
	nos_ctx_sw_handler();	// maybe return here from interrupt with turning on global interrupt

***************/

/**
 * @warn It must be called with disabled global interrupt.
 * @warn It must not use ENTER_CRITICAL() or ENTER_ISR().
 * @warn It must enable global interrupt after returning to caller.
 *       (Or nos_context_switch_core() will enable global interrupt when it returns.)
 */
void nos_ctx_sw_handler(void)
{
#ifdef THREAD_M
    UINT8 i,j;
	
    /* There are no threads in the ready queue (This happens when the program
       starts). In this case, no context switching is required because no
       threads currently exists to be switched. */
    if (_ready_priority_bit == 0x00)
        return;

    /* Find wheter there is a thread with highest priority or not. */
    for (i = PRIORITY_LEVEL_COUNT - 1; i > 0; i--)
    {
        if (_ready_priority_bit & (1 << i))
        {
            break; // A therad with 'i' priority is found.
        }
    }

    /* Thread is running (normal state). */
    if (tcb[_rtid]->state == RUNNING_STATE)
    {
        // In case equal or higher priority tasks, enforce context switching.
        if (i >= tcb[_rtid]->priority)
        {
            // Get the thread id of the thread with highest priority.
            THREADQ_FETCH(_rdy_q[i], &_next_rtid);
            if (THREADQ_IS_EMPTY(_rdy_q[i]))
            {
                /* Since the thread queue for this priority is empty, the
                   corresponding bit in _ready_priority_bit is cleared. */
                _BIT_CLR(_ready_priority_bit, i);
            }
            
            // Change current thread state from 'RUNNING' to 'READY'.
            j = tcb[_rtid]->priority;
            _BIT_SET(_ready_priority_bit, j);
            THREADQ_ENQ(_rdy_q[j], _rtid);
            tcb[_rtid]->state = READY_STATE;

            // Architecutre dependent context switch part (STACK PUSH and POP)
            nos_context_switch_core();
        }
    }
    
    /* Thread will exit. The current context will not be stored. */
    else if (tcb[_rtid]->state == EXIT_STATE)
    {
        // Release allocated memory.
        NOS_ENTER_ISR(); // To prevent enabling global interrupt during nos_free().
        nos_free((void*)(tcb[_rtid]->stack_start_addr));
        nos_free((void*)(tcb[_rtid]));
        NOS_EXIT_ISR();

        // Clear created thread list.
        _BIT_CLR(_created_tid_bit, _rtid);

        // Get the thread id of the thread with highest priority.
        THREADQ_FETCH(_rdy_q[i], &_rtid);
        if (THREADQ_IS_EMPTY(_rdy_q[i]))
        {
            /* Since the thread queue for this priority is empty, the
               corresponding bit in _ready_priority_bit is cleared. */
            _BIT_CLR(_ready_priority_bit, i);
        }
        
        // Architecutre dependent context load part (STACK POP only)
        nos_context_load_core();
    }

    /* WAITING?, SLEEPING?, READY? (exceptional state. will not change current '_rtid' state) */
    else
    {
        // Get the thread id of the thread with highest priority.
        THREADQ_FETCH(_rdy_q[i], &_next_rtid);
        if (THREADQ_IS_EMPTY(_rdy_q[i]))
        {
            /* Since the thread queue for this priority is empty, the
               corresponding bit in _ready_priority_bit is cleared. */
            _BIT_CLR(_ready_priority_bit, i);
        }

        // Architecutre dependent context switch part (STACK PUSH and POP)
        nos_context_switch_core();
    }
#endif /* THREAD_M */
}

/**
 * @warn This function must be invoked Scheduler timer interrupt which has the
 *       highest priority at every SCHED_TIMER_MS.
 * @warn It wakes up sleeping threads whose sleeping time is expired.
 * @warn Make no other interrupts preempt this one by disable global interrupt.
 */
void nos_sched_handler(void)
{
#ifdef THREAD_M
    /* Handle sleeping threads waiting for time-expiration. */
    if (_sleep_tid_bit)
    {
        UINT8 k;
        if (--_ticks_left == 0) // expired
        {
            UINT8 i;
            
            _ticks_left = 65535; // need to find a minimum for efficiency

            for (i = 1; i < MAX_NUM_TOTAL_THREAD; i++)
            {
                if (NOS_THREAD_IS_SLEEP(i))
                {
                    tcb[i]->sleep_tick -= _init_ticks_left;

                    if (tcb[i]->sleep_tick == 0)
                    {
                         // Expired. Wake up the thread.
                        _BIT_CLR(_sleep_tid_bit, i);
                        k = tcb[i]->priority;
                        tcb[i]->state = READY_STATE;
                        THREADQ_ENQ(_rdy_q[k], i);
                        _BIT_SET(_ready_priority_bit, k);
                    }

                    /* Find a minumum. */
                    if (tcb[i]->sleep_tick != 0 && _ticks_left > tcb[i]->sleep_tick)
                        _ticks_left = tcb[i]->sleep_tick;
                }
            }
            _init_ticks_left = _ticks_left;
        }
    }
#endif /* THREAD_M */

#ifdef USER_TIMER_M
    ++nos_sched_tick;

    if (nos_user_timer_activated_bit &&
        nos_next_user_timer_expire_tick == nos_sched_tick)
    {
        UINT8 i;
        for (i = 0; i < NOS_MAX_NUM_USER_TIMER; ++i)
        {
            if (_IS_SET(nos_user_timer_activated_bit, i) &&
                nos_user_timer[i].next_sched_tick == nos_sched_tick)
            {
                _BIT_SET(nos_user_timer_expired_bit, i);
                if (nos_user_timer[i].periodic_ticks == 0) // one_shot timer
                {
                    _BIT_CLR(nos_user_timer_activated_bit, i);
                }
                else
                {
                    nos_user_timer[i].next_sched_tick = nos_sched_tick + nos_user_timer[i].periodic_ticks;
                }
            }
        }

        if (nos_user_timer_expired_bit)
        {
            /* Make a thread ready for execution. Below is the same as
               nos_thread_priority_change(0, PRIORITY_ULTRA). But if
               nos_thread_priority_change() called, then global interrupt will be
               set by CRITICAL_SECTION macro. (ERROR!!) */
            
            if (tcb[0]->state == READY_STATE)
            {
                i = tcb[0]->priority;
                THREADQ_DEQ(_rdy_q[i], 0);
                if (THREADQ_IS_EMPTY(_rdy_q[i]))
                    _BIT_CLR(_ready_priority_bit, i);
                THREADQ_ENQ(_rdy_q[PRIORITY_ULTRA], 0);
                tcb[0]->priority = PRIORITY_ULTRA;
                _BIT_SET(_ready_priority_bit, PRIORITY_ULTRA);
            }
            else
            {
                // Just change its priority.
                tcb[0]->priority = PRIORITY_ULTRA;
            }
        }
        nos_user_timer_check_next_expire_tick();
    }
#endif //USER_TIMER_M
}

void nos_ctx_sw(void)
{
#ifdef KERNEL_DEFERRED_CTX_SW
    // Trigger interrupt for context_switch (not for scheduler).
    NOS_CTX_SW_PENDING_SET();
#else
    // in critical section or in ISR
    if (!NOS_IS_CTX_SW_ALLOWABLE())
    {
        NOS_SCHED_PENDING_SET();
    }
    else
    {
        NOS_DISABLE_GLOBAL_INTERRUPT();
        nos_ctx_sw_handler();   // it will turn on global interrupt
        NOS_ENABLE_GLOBAL_INTERRUPT();  // just in case
    }
#endif
}

void nos_sched_init(void)
{
    UINT8 i;

    /*
     * When the program starts, only main thread is running. The main thread
     * plays a role as an idle thread in a multi-threaded programming context.
     * The ID and priority of the main thread are set to 0. This implies that
     * the main thread is executed only when the other threads are not running.
     */
    
    /*
     * A corresponding bit (for the id of each thread) is set or unset (bit-wise
     * operation) for a variable related to a thread. (e.g. if a thread of tid=2
     * is in asleep, _sleep_tid_bit = 0b00000100 = 0x02.
     */

    _rtid               = 0;  // main thread (tid=0) is initially running
    _ready_priority_bit = 0;  // initially empty (no threads are in rady state)
    _created_tid_bit    = 1;  /* the main thread (tid=0) is created
                               * ((0-th bit is set) 0b00000001 = 0x01)
                               */
    _sleep_tid_bit      = 0;  // no threads in sleep state
    _suspended_tid_bit  = 0;  // no threads in suspended state

    // The next two variables are used only when there are any sleeping threads.
    _init_ticks_left = 65535;
    _ticks_left = 0;

#ifdef USER_TIMER_M
    nos_sched_tick = 0;
    nos_user_timer_init();
#endif

    /* Initialize ready queue. There is a corresponding ready queue for each priority. */
    for (i=0; i<PRIORITY_LEVEL_COUNT; i++)
    {
        THREADQ_INIT(_rdy_q[i]);
    }

    /* TCB setting for the main (idle) thread. The main thread is initially
       running. The priority of the main thread is 0 (lowest priority). */
    memset((void *) &kernel_tcb, 0, sizeof(struct _tcb));
    tcb[0] = &kernel_tcb;

#ifdef THREAD_M
    tcb[0]->stack_start_addr = (STACK_PTR) SYSTEM_STACK_START_ADDR;
    tcb[0]->stack_size = SYSTEM_STACK_SIZE;
#ifdef STACK_DEBUG
    *(tcb[_rtid]->stack_start_addr) = 0xFF;
#endif
#endif

    tcb[0]->id = 0;
    tcb[0]->state = RUNNING_STATE;
    tcb[0]->priority = PRIORITY_LOWEST;

    /* Setup periodic scheduler interrupt. */
    nos_sched_hal_init();

    /* haekim temp*/
    // initial settings for power management
    //NOS_SET_SLEEP_MODE(NOS_IDLE);

#ifdef ENABLE_SCHEDULING
    NOS_ENABLE_SCHED();
#else
    NOS_DISABLE_SCHED();
#endif
}

/**
 * @note The nos_sched_start() is called by the main thread (= idle thread).
 * @note This function is exeucted by the idle thread infinitely.
 * @note That this funtion (by the idle thread) is about to run implies that no
 *       threads are not in ready state. Since ihe idle thread (executing this
 *       function) does no work but uselessly occupies the CPU, it should
 *       frequently go into power saving mode to save CPU power.
 */
void nos_sched_start(void)
{
    nos_sched_timer_start();

    nos_ctx_sw(); // context switch for the other threads to occupy the CPU directly

    while(1)
    {

        if (tcb[0]->priority == PRIORITY_ULTRA)
        {
            // Process Task Queue here
            if (taskq_head != taskq_tail)
            {
                nos_taskq_exe();
            }

#ifdef USER_TIMER_M
            // Process Timer Functions here
            if (nos_user_timer_expired_bit)
            {
                nos_user_timer_exe();
                continue;
            }
#endif
            if (taskq_head != taskq_tail) continue;

            nos_thread_priority_change(0, PRIORITY_LOWEST);
        }
        else if (tcb[0]->priority == PRIORITY_LOWEST)
        {
            /*
             * The idle thread is running. Go into power saving mode.
             * This thread will be automatically awaken by hardware.
             */
#ifdef NOS_MCU_SUPPORTS_SLEEP
            nos_mcu_sleep();
#endif
        }
        nos_ctx_sw();
    }
}
#endif
