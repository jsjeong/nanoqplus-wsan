// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Executes registered works by the system thread at the nearest time.
 * @author Sangcheol Kim (ETRI)
 * @author Haeyong Kim (ETRI)
 */

#include "taskq.h"

#include "kconf.h"
#ifdef KERNEL_M
#include "critical_section.h"
#include "sched.h"
#include "thread.h"

//#define TASKQ_DEBUG
#ifdef TASKQ_DEBUG
#include "platform.h"
#include "uart.h"
#endif /* TASKQ_DEBUG */

#ifdef TASKQ_LEN_8
#define TASKQ_LEN       8
#define TASKQ_LEN_MASK  0x07
#elif defined TASKQ_LEN_16
#define TASKQ_LEN       16
#define TASKQ_LEN_MASK  0x0F
#elif defined TASKQ_LEN_32
#define TASKQ_LEN       32
#define TASKQ_LEN_MASK  0x1F
#elif defined TASKQ_LEN_64
#define TASKQ_LEN       64
#define TASKQ_LEN_MASK  0x3F
#elif defined TASKQ_LEN_128
#define TASKQ_LEN       128
#define TASKQ_LEN_MASK  0x7F
#endif

struct _nos_task_element
{
    void (*func)(void *);
    void *args;
};
typedef struct _nos_task_element TASK;

volatile UINT8 taskq_head;
volatile UINT8 taskq_tail;
volatile TASK _nos_taskq[TASKQ_LEN];

// Work Queue : a set of functions to be run by the system thread as soon as possible
void nos_taskq_init(void)
{
    taskq_head = 0;
    taskq_tail = 0;
}

// You must not use thread function in this function.
BOOL nos_taskq_reg(void (*func)(void *), void *args)
{
    UINT8 foo;
    
    NOS_ENTER_CRITICAL_SECTION();
    foo = (taskq_tail + 1) & TASKQ_LEN_MASK;
    
    // insert *func into work queue.
    if (taskq_head == foo)		// if queue is full
    {
        NOS_EXIT_CRITICAL_SECTION();
        return FALSE; // cannot register work because the work queue is full
    }
    else
    {
        _nos_taskq[taskq_tail].func = func;
        _nos_taskq[taskq_tail].args = args;
        taskq_tail = foo;
        NOS_EXIT_CRITICAL_SECTION();

        if ( tcb[0]->priority != PRIORITY_ULTRA )
        {
            // the idle thread becomes "super-thread" with ultra-highest-priority
            nos_thread_priority_change(0, PRIORITY_ULTRA);
            nos_ctx_sw();
        }        
        return TRUE;
    }
}

/**
 * Execute a task in the task queue.
 */
void nos_taskq_exe(void)
{
    UINT8 temp_pos;

    NOS_ENTER_CRITICAL_SECTION();
    temp_pos = taskq_head;
    taskq_head = (taskq_head + 1) & TASKQ_LEN_MASK;
    NOS_EXIT_CRITICAL_SECTION();

    (_nos_taskq[temp_pos].func)(_nos_taskq[temp_pos].args);
}
#endif
