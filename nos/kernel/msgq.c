/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "msgq.h"

#ifdef KERNEL_M
#ifdef MSGQ_M
#include "heap.h"
#include "critical_section.h"
#include "sched.h"

#include "led.h"
#include "uart.h"


MSGQ nos_msgq_create(UINT8 type, UINT8 len)
{
    MSGQ mq;
    if (!(type == NOS_MSGQ_UINT8 || type == NOS_MSGQ_UINT16 || type == NOS_MSGQ_UINT32))
    {
        return NULL;
    }

    mq = (MSGQ) nos_malloc(sizeof(struct _msgq));
    if (mq == NULL)
    {
        return NULL;
    }
    if (type == NOS_MSGQ_UINT8)
        mq->msg = nos_malloc(sizeof(UINT8)*len);
    else if (type == NOS_MSGQ_UINT16) 
        mq->msg = nos_malloc(sizeof(UINT16)*len);
    else if (type == NOS_MSGQ_UINT32) 
        mq->msg = nos_malloc(sizeof(UINT32)*len);
    if (mq->msg == NULL)
    {
        nos_free(mq);
        return NULL;
    }
    mq->front  = 0;
    mq->rear  = 0;
    mq->nitem  = 0;
    mq->len  = len;
    mq->type = type;
    return mq;
}

void nos_msgq_destroy(MSGQ mq)
{
    nos_free(mq->msg);
    nos_free(mq);
}

BOOL nos_msgq_send_nb(MSGQ mq, void *msg)
{
    NOS_ENTER_CRITICAL_SECTION();
    // message queue is full
    if (mq->nitem == mq->len)
    {
        NOS_EXIT_CRITICAL_SECTION();
        return FALSE;
    }
    // message queue is not full
    else
    {
        if (mq->type == NOS_MSGQ_UINT8)
            *((UINT8*)mq->msg + mq->rear) = *((UINT8*)msg);
        else if (mq->type == NOS_MSGQ_UINT16)
            *((UINT16*)mq->msg + mq->rear) = *((UINT16*)msg);
        else if (mq->type == NOS_MSGQ_UINT32)
            *((UINT32*)mq->msg + mq->rear) = *((UINT32*)msg);
        ++mq->nitem;
        mq->rear = (mq->rear+1)%mq->len;
        NOS_EXIT_CRITICAL_SECTION();
        return TRUE;
    }
}

BOOL nos_msgq_recv_nb(MSGQ mq, void *msg)
{
    NOS_ENTER_CRITICAL_SECTION();
    if (mq->nitem != 0)
    {
        if (mq->type == NOS_MSGQ_UINT8)
            *((UINT8*)msg) = *((UINT8*) mq->msg + mq->front);
        else if (mq->type == NOS_MSGQ_UINT16)
            *((UINT16*)msg) = *((UINT16*) mq->msg + mq->front);
        else if (mq->type == NOS_MSGQ_UINT32)
            *((UINT32*)msg) = *((UINT32*) mq->msg + mq->front);
        mq->front = (mq->front+1)%mq->len;
        --mq->nitem;
        NOS_EXIT_CRITICAL_SECTION();
        return TRUE;
    }
    // message queue is empty
    else
    {
        NOS_EXIT_CRITICAL_SECTION();
        return FALSE;
    }
}

void nos_msgq_send(MSGQ mq, void *msg)
{
    // while queue is full
    while (nos_msgq_send_nb(mq, msg) == FALSE)
        nos_ctx_sw();
}

void nos_msgq_recv(MSGQ mq, void *msg)
{
    // while queue is empty
    while (nos_msgq_recv_nb(mq, msg) == FALSE)
        nos_ctx_sw();
}


#endif // MSGQ_M
#endif // KERNEL_M
