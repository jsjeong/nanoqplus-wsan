/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef MSGQ_H
#define MSGQ_H
#include "kconf.h"
#ifdef KERNEL_M
#ifdef MSGQ_M
#include "nos_common.h"

enum {
    NOS_MSGQ_UINT8 = (1),
    NOS_MSGQ_UINT16 = (2),
    NOS_MSGQ_UINT32 = (3),
};

struct _msgq
{
    void *msg;    // array of messages
    UINT8 len;    // length of queue
    UINT8 type;   // type of a message in the queue
    UINT8 front, rear, nitem; // front, rear of the queue, the number of items in the queue
};
typedef struct _msgq* MSGQ;

#define MSGQ_IS_FULL(mq) (mq->nitem == mq->len)
#define MSGQ_IS_EMPTY(mq) (mq->nitem == 0)

MSGQ  nos_msgq_create(UINT8 type, UINT8 len);
void  nos_msgq_destroy(MSGQ mq);

// non-blocking
BOOL nos_msgq_send_nb(MSGQ mq, void *data);
BOOL nos_msgq_recv_nb(MSGQ mq, void *data);
// blocking
void nos_msgq_send(MSGQ mq, void *msg);
void nos_msgq_recv(MSGQ mq, void *msg);

#endif // MSGQ_M
#endif // KERNEL_M
#endif // ~MSGQ_H
