// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "user_timer.h"
#include "kconf.h"

#ifdef USER_TIMER_M
#include "sched.h"
#include "heap.h"
#include "critical_section.h"
#include "hal_sched.h"

UINT16	nos_user_timer_created_bit;
UINT16	nos_user_timer_activated_bit;
volatile UINT16	nos_user_timer_expired_bit;
NOS_USER_TIMER nos_user_timer[NOS_MAX_NUM_USER_TIMER];
volatile UINT16 nos_next_user_timer_expire_tick;
extern volatile UINT16 nos_sched_tick;

void nos_user_timer_init(void)
{
    nos_user_timer_created_bit = 0;
    nos_user_timer_activated_bit = 0;
    nos_user_timer_expired_bit = 0;
    nos_next_user_timer_expire_tick = 0;
}

void nos_user_timer_check_next_expire_tick(void)
{
    UINT8 i;
    UINT16 tmp_next_tick_diff, next_tick_diff;
    
    NOS_ENTER_CRITICAL_SECTION();

    next_tick_diff = 0xFFFF;
    for (i = 0; i < NOS_MAX_NUM_USER_TIMER; ++i)
    {
        if (_IS_SET(nos_user_timer_activated_bit, i))
        {
            tmp_next_tick_diff = nos_user_timer[i].next_sched_tick - nos_sched_tick;
            if (tmp_next_tick_diff < next_tick_diff)
            {
                next_tick_diff = tmp_next_tick_diff;
                nos_next_user_timer_expire_tick = nos_user_timer[i].next_sched_tick;
            }
        }
    }
    
    NOS_EXIT_CRITICAL_SECTION();
}

UINT16 nos_user_timer_get_max_sec(void)
{
    return ((UINT16) (655 * SCHED_TIMER_MS) / 10);
}

UINT32 nos_user_timer_get_max_ms(void)
{
    return ((UINT32) (65535 * SCHED_TIMER_MS));
}

INT8 nos_user_timer_create(void (*func)(void *),
                           void *arg,
                           UINT16 ticks,
                           BOOL is_periodic)
{
    UINT8 id;
    
    if (ticks == 0 || func == NULL)
    {
        return NOS_USER_TIMER_CREATE_ERROR;
    }

    NOS_ENTER_CRITICAL_SECTION();

    for (id = 0; id < NOS_MAX_NUM_USER_TIMER; ++id)
    {
        if ( !_IS_SET(nos_user_timer_created_bit,id) )
        {
            nos_user_timer[id].func = func;
            nos_user_timer[id].arg = arg;
            nos_user_timer[id].next_sched_tick = nos_sched_tick + ticks;
            if (is_periodic == NOS_USER_TIMER_ONE_SHOT)
            {
                nos_user_timer[id].periodic_ticks = 0;
            }
            else //if (is_periodic == NOS_USER_TIMER_PERIODIC)
            {
                nos_user_timer[id].periodic_ticks = ticks;
            }
            _BIT_SET(nos_user_timer_created_bit,id);
            _BIT_SET(nos_user_timer_activated_bit,id);
            nos_user_timer_check_next_expire_tick();
            
            NOS_EXIT_CRITICAL_SECTION();
            return id;
        }
    }

    NOS_EXIT_CRITICAL_SECTION();
    return NOS_USER_TIMER_NO_EMPTY_SLOT_ERROR;
}

INT8 nos_user_timer_create_ms(void (*func)(void *),
                              void *arg,
                              UINT16 msec,
                              BOOL periodic)
{
    if (msec == 0)
    {
        return NOS_USER_TIMER_CREATE_ERROR;
    }
    else
    {
        return nos_user_timer_create(func,
                                     arg,
                                     (msec - 1) / SCHED_TIMER_MS + 1,
                                     periodic);
    }
}

INT8 nos_user_timer_create_sec(void (*func)(void *),
                               void *arg,
                               UINT16 sec,
                               BOOL periodic)
{
    if (sec == 0 || sec > nos_user_timer_get_max_sec())
    {
        return NOS_USER_TIMER_CREATE_ERROR;
    }
    else
    {
        return nos_user_timer_create(func,
                                     arg,
                                     sec * (1000UL / SCHED_TIMER_MS),
                                     periodic);
    }
}

BOOL nos_user_timer_activate(UINT8 id)
{
    BOOL ret;
    
    if (id > NOS_MAX_NUM_USER_TIMER)
        return FALSE;

    NOS_ENTER_CRITICAL_SECTION();
    
    if (!_IS_SET(nos_user_timer_created_bit, id) ||
        nos_user_timer[id].periodic_ticks == 0)
    {
        ret = FALSE;
    }
    else
    {
        _BIT_SET(nos_user_timer_activated_bit,id);
        nos_user_timer[id].next_sched_tick = nos_sched_tick + nos_user_timer[id].periodic_ticks;
        nos_user_timer_check_next_expire_tick();
        ret = TRUE;
    }

    NOS_EXIT_CRITICAL_SECTION();
    return ret;
}

BOOL nos_user_timer_deactivate(UINT8 id)
{
    BOOL ret;
    
    if (id > NOS_MAX_NUM_USER_TIMER)
        return FALSE;

    NOS_ENTER_CRITICAL_SECTION();

    if (!_IS_SET(nos_user_timer_created_bit, id) ||
        nos_user_timer[id].periodic_ticks == 0)
    {
        ret = FALSE;
    }
    else
    {
        _BIT_CLR(nos_user_timer_activated_bit,id);
        ret = TRUE;
    }

    NOS_EXIT_CRITICAL_SECTION();
    return ret;
}

BOOL nos_user_timer_destroy(UINT8 id)
{
    BOOL ret;
    
    if (id > NOS_MAX_NUM_USER_TIMER)
        return FALSE;

    NOS_ENTER_CRITICAL_SECTION();
    if ( !_IS_SET(nos_user_timer_created_bit, id))
    {
        // Not created timer.
        NOS_EXIT_CRITICAL_SECTION();
        return FALSE;
    }
    else if (_IS_SET(nos_user_timer_expired_bit, id))
    {
        // Expired, but not executed timer.
        nos_user_timer[id].periodic_ticks = 0;
        NOS_EXIT_CRITICAL_SECTION();
        return TRUE;
    }
    else
    {
        // Created and not-expired timer.
        _BIT_CLR(nos_user_timer_created_bit,id);
        _BIT_CLR(nos_user_timer_activated_bit,id);
        _BIT_CLR(nos_user_timer_expired_bit,id);
        ret = TRUE;
    }

    NOS_EXIT_CRITICAL_SECTION();
    return ret;
}

BOOL nos_user_timer_reschedule(UINT8 id, UINT16 ticks)
{
    BOOL ret;

    if (id > NOS_MAX_NUM_USER_TIMER)
        return FALSE;

    NOS_ENTER_CRITICAL_SECTION();

    if (!_IS_SET(nos_user_timer_created_bit, id) ||
        _IS_SET(nos_user_timer_expired_bit, id))
    {
        ret = FALSE;
    }

    nos_user_timer[id].next_sched_tick = nos_sched_tick + ticks;
    if (nos_user_timer[id].periodic_ticks > 0)
    {
        nos_user_timer[id].periodic_ticks = ticks;
    }
    ret = TRUE;

    NOS_EXIT_CRITICAL_SECTION();
    return ret;
}

BOOL nos_user_timer_reschedule_ms(UINT8 id, UINT16 msec)
{
    if (msec == 0)
    {
        return FALSE;
    }
    else
    {
        return nos_user_timer_reschedule(id, (msec - 1) / SCHED_TIMER_MS + 1);
    }
}

BOOL nos_user_timer_reschedule_sec(UINT8 id, UINT16 sec)
{
    if (sec == 0 || sec > nos_user_timer_get_max_sec())
    {
        return FALSE;
    }
    else
    {
        return nos_user_timer_reschedule(id, sec * (1000UL / SCHED_TIMER_MS));
    }
}

void nos_user_timer_exe(void)
{
    UINT8 id;

    NOS_ENTER_CRITICAL_SECTION();

    for (id = 0; id < NOS_MAX_NUM_USER_TIMER; ++id)
    {
        if (_IS_SET(nos_user_timer_expired_bit, id))
        {
            // execute the timer function
            NOS_EXIT_CRITICAL_SECTION();
            (nos_user_timer[id].func)(nos_user_timer[id].arg);
            NOS_ENTER_CRITICAL_SECTION();

            _BIT_CLR(nos_user_timer_expired_bit, id);
            if (nos_user_timer[id].periodic_ticks == 0) // destroy one shot-timer
            {
                nos_user_timer_destroy(id);
            }
        }
    }
    
    NOS_EXIT_CRITICAL_SECTION();
}

UINT32 nos_get_sched_tick_ms(void)
{
    return (nos_sched_tick * SCHED_TIMER_MS);
}

#endif
