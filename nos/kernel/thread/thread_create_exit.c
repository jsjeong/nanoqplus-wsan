// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 * @author Junkeun Song (ETRI)
 */

#include "thread.h"

#ifdef THREAD_M
#include "arch.h"
#include "platform.h"
#include "critical_section.h"
#include "heap.h"
#include "sched.h"
#include "hal_sched.h"
#include <string.h>
#include <stdio.h>

extern volatile THREADQ _rdy_q[PRIORITY_LEVEL_COUNT];
extern volatile UINT8  _ready_priority_bit; // priority bit setting for threads, which is set for the priority of the thread when the thread becomes in ready state.

static void nos_thread_start(void)
{
    // execute the corresponding function by the thread
    (tcb[(UINT8) _rtid]->func)(tcb[(UINT8) _rtid]->args_data);

    /* post-process */
    nos_thread_exit();
}

INT8 nos_thread_create(void (*func)(void *args), void *args_data, UINT16 stack_size, UINT8 priority)
{
    UINT8 tid;
    UINT8 align;

    NOS_ENTER_CRITICAL_SECTION();
#ifndef THREAD_EXT_M
    if (_created_tid_bit == 0x3f) // full in slots
#else
    if (_created_tid_bit == 0xffff) // full in slots
#endif
    {
        NOS_EXIT_CRITICAL_SECTION();
        return THREAD_CREATE_ID_EXHAUST_ERROR;
    }

    for (tid=1; tid<MAX_NUM_TOTAL_THREAD; ++tid)
    {
        if (~_created_tid_bit & (1 << tid))
        {
            break; // found an empty slot
        }
    }

    // Error handling when priority is given 0 or higher than defined (0 is reserved for the idle thread)
    if (priority == 0 || priority >= PRIORITY_LEVEL_COUNT)
    {
        NOS_EXIT_CRITICAL_SECTION();
        return THREAD_CREATE_PRIORITY_OUT_OF_RANGE_ERROR;
    }

    // tcb (thread control block) settings when creating thread
    // A thread has id (*ptid), initialized stack memory, stack pointer and the number of sleeping ticks (sleep_tick) if sleeping.
    stack_size += DEFAULT_STACK_SIZE;
    align = stack_size % (sizeof(STACK_ENTRY));
    if ( align )
    {
        stack_size = stack_size + sizeof(STACK_ENTRY) - align;
    }

    tcb[tid] = (struct _tcb*)nos_malloc(sizeof(struct _tcb)); 
    if (tcb[tid] == NULL)
    {
        NOS_EXIT_CRITICAL_SECTION();
        return THREAD_CREATE_INSUFFICIENT_MEMORY_ERROR;
    }

    tcb[tid]->stack_start_addr =(STACK_PTR)nos_malloc(stack_size);
    if (tcb[tid]->stack_start_addr == NULL)
    {
        nos_free((void *) tcb[tid]);
        NOS_EXIT_CRITICAL_SECTION();
        return THREAD_CREATE_INSUFFICIENT_MEMORY_ERROR;
    }
    
	// Record the tid of this thread in the _created_tid_bit if there was no error.
    _BIT_SET(_created_tid_bit, tid);
    
#ifdef STACK_DEBUG
    *(tcb[tid]->stack_start_addr) = 0xFF;
#endif
    tcb[tid]->sptr = nos_tcb_stack_init(nos_thread_start, tcb[tid]->stack_start_addr, stack_size);
    tcb[tid]->stack_size = stack_size;
    tcb[tid]->sleep_tick    = 0;
    tcb[tid]->func    = func;
    tcb[tid]->args_data   = args_data;
    tcb[tid]->id = tid;  //dummy data.
    THREADQ_INIT(tcb[tid]->waitq); // for nos_thread_join(..)

    // This thread becomes in ready state (inserted into ready_queue).
    // Three steps (in terms of the thread priority) :
    //   1) find a ready queue for the priority of this thread
    //   2) insert tid of this thread into the ready queue
    //   3) Set the ready priority bit for the priority of this thread

    THREADQ_ENQ(_rdy_q[priority], tid);
    tcb[tid]->state = READY_STATE;
    tcb[tid]->priority = priority;

    _BIT_SET(_ready_priority_bit, priority);

    NOS_EXIT_CRITICAL_SECTION();
    return tid;
}

void nos_thread_exit(void)
{
    UINT8 wait_tid, t_priority;

    NOS_ENTER_CRITICAL_SECTION();
    // the waiting threads to be joined that need to be awaken at this point
    while (!THREADQ_IS_EMPTY(tcb[_rtid]->waitq))
    {
        THREADQ_FETCH(tcb[_rtid]->waitq, &wait_tid);
        //j is a fetched thread id
        tcb[wait_tid]->state = READY_STATE;
        t_priority = tcb[wait_tid]->priority;
        THREADQ_ENQ(_rdy_q[t_priority], wait_tid);
        _BIT_SET(_ready_priority_bit, t_priority);
    }
    // let this thread be in EXIT_STATE and then context switching.
    tcb[_rtid]->state = EXIT_STATE;
    NOS_EXIT_CRITICAL_SECTION();

    // wait for 'context-switch'
    nos_ctx_sw();
    while (1);
}
#endif
