// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 */

#include "thread.h"

#ifdef KERNEL_M
#include "arch.h"
#include "critical_section.h"
#include "sched.h"

extern volatile THREADQ _rdy_q[PRIORITY_LEVEL_COUNT];
extern volatile UINT8  _ready_priority_bit; // priority bit setting for threads, which is set for the priority of the thread when the thread becomes in ready state.

UINT8 nos_thread_priority_change(UINT8 tid, UINT8 new_priority)
{
    UINT8 i;
    if (tcb[tid]->priority == new_priority || 
            tcb[tid]->priority >= PRIORITY_LEVEL_COUNT)
        return THREAD_PRIORITY_CHANGE_ERROR;

    NOS_ENTER_CRITICAL_SECTION();
    // if a thread with tid is in ready state
    if (tcb[tid]->state == READY_STATE)
    {
        i = tcb[tid]->priority;
        THREADQ_DEQ(_rdy_q[i], tid);
        if (THREADQ_IS_EMPTY(_rdy_q[i]))
            _BIT_CLR(_ready_priority_bit, i);

        THREADQ_ENQ(_rdy_q[new_priority], tid);
        tcb[tid]->priority = new_priority;
        _BIT_SET(_ready_priority_bit, new_priority);
    }
    else // the thread in other states
    {
        tcb[tid]->priority = new_priority; // just change its priority
    }
    NOS_EXIT_CRITICAL_SECTION();

    return THREAD_NO_ERROR;
}
#endif
