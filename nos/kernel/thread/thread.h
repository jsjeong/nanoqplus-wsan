// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 */

#ifndef THREAD_H
#define THREAD_H

#include "kconf.h"
#ifdef KERNEL_M
#include "nos_common.h"
#include "sched.h"
#include "hal_thread.h"
#include "hal_sched.h"

// the maximum thread_sleep seconds
#define NOS_THREAD_SLEEP_MAX_SEC  ((UINT16)(655*SCHED_TIMER_MS)/10)

// thread states
enum
{
    READY_STATE = (1),
    RUNNING_STATE = (2),
    WAITING_STATE = (3),
    SLEEPING_STATE = (4),
    EXIT_STATE = (5),
};

enum
{
    THREAD_EXIT_ERROR = (8),
    THREAD_KILL_ERROR = (7),
    THREAD_SLEEP_ERROR = (6),
    THREAD_WAKEUP_ERROR = (5),
    THREAD_RESUME_ERROR = (4),
    THREAD_PRIORITY_CHANGE_ERROR = (3),
    THREAD_NO_ERROR = (0),
    THREAD_CREATE_ID_EXHAUST_ERROR = (-1),
    THREAD_CREATE_PRIORITY_OUT_OF_RANGE_ERROR = (-2),
    THREAD_CREATE_INSUFFICIENT_MEMORY_ERROR = (-3),
};

//extern variables
extern volatile UINT8  _rtid;    // running thread id (to indicate which thread is currently running)

#ifndef THREAD_EXT_M
extern UINT8   _created_tid_bit;       // used for avoiding duplicated thread id, which is set when a thread is created
extern UINT8   _suspended_tid_bit;
extern UINT8   _sleep_tid_bit;         // indicates whether a thread is in sleeping state or not
#else
extern UINT16  _created_tid_bit;       // used for avoiding duplicated thread id, which is set when a thread is created
extern UINT16   _suspended_tid_bit;
extern UINT16  _sleep_tid_bit;         // indicates whether a thread is in sleeping state or not
#endif

extern UINT16  _init_ticks_left; // ticks left until first wake-up (initially setting)
extern UINT16  _ticks_left; // ticks left currently

extern volatile _TCB *tcb[MAX_NUM_TOTAL_THREAD]; // tcb pointer array
extern volatile UINT8 _ready_priority_bit;


// BIT OPERATION VARIABLES; for a thread with tid k, k-th bit of the variable is set or clear.
// The next defitinitions were not currently be used (will be considered later).
#define NOS_THREAD_IS_READY(tid)  (_ready_priority_bit & (1 << tid))
#define NOS_THREAD_IS_CREATED(tid)  (_created_tid_bit & (1 << tid))
#define NOS_THREAD_IS_SLEEP(tid)  (_sleep_tid_bit & (1 << tid))
#define NOS_THREAD_IS_SUSPENDED(tid)  (_suspended_tid_bit & (1 << tid))
#define NOS_THREAD_IS_NOT_READY(tid)  (~_ready_priority_bit & (1 << tid))
#define NOS_THREAD_IS_NOT_CREATED(tid)  (~_created_tid_bit & (1 << tid))
#define NOS_THREAD_IS_NOT_SLEEP(tid)  (~_sleep_tid_bit & (1 << tid))
#define NOS_THREAD_IS_NOT_SUSPENDED(tid) (~_suspended_tid_bit & (1 << tid))

#ifdef THREAD_M
INT8 nos_thread_create(void (*func)(void *args), void *args_data, UINT16 stack_size, UINT8 priority);
void nos_thread_exit(void);
UINT8 nos_thread_join(UINT8 tid);
UINT8 nos_thread_suspend(UINT8 tid);
UINT8 nos_thread_resume(UINT8 tid);
UINT8 nos_thread_kill(UINT8 tid);
UINT8 nos_thread_wakeup(UINT8 tid);
#endif
UINT8 nos_thread_priority_change(UINT8 tid, UINT8 new_priority);

#if defined (THREAD_M) || defined (UART_M)
UINT8 nos_thread_sleep(UINT16 ticks);
UINT8 nos_thread_sleep_ms(UINT16 msec);
UINT8 nos_thread_sleep_sec(UINT16 seconds);
#endif

// returns thread information
#define NOS_GET_THREAD_ID()   (tcb[_rtid]->id)
#define NOS_GET_THREAD_STATE(tid)  (tcb[tid]->state)
#define NOS_GET_THREAD_PRIORITY(tid)  (tcb[tid]->priority)
#define NOS_GET_THREAD_STACK_POINTER(tid) (tcb[tid]->sptr)
#define NOS_GET_THREAD_STACK_UNUSED_SPACE(tid) \
    ((UINT16)(tcb[tid]->sptr - tcb[tid]->stack_start_addr))

UINT8 nos_get_num_user_threads(void);
UINT16 nos_get_current_thread_unused_stack_space(void);

#endif
#endif // ~THREAD_H
