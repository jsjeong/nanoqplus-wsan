/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

///////////////////////////////
//   BIT TABLE
//
// 0000 0000 0000 000f 
// 0000 0000 0000 00f0
// 0000 0000 0000 0f00
// 0000 0000 0000 f000
// 0000 0000 000f 0000
// 0000 0000 0f00 0000
// 0000 0000 f000 0000
// 0000 000f 0000 0000
// 0000 00f0 0000 0000
// 0000 0f00 0000 0000
// 0000 f000 0000 0000
// 000f 0000 0000 0000
// 00f0 0000 0000 0000
// 0f00 0000 0000 0000
// f000 0000 0000 0000

#ifndef THREADQ_H
#define THREADQ_H
#include "kconf.h"
#ifdef KERNEL_M

#ifdef THREAD_EXT_M
#define THREADQ_ENQ(queue, tid)       \
    do {         \
        queue.q[queue.idx/16] += ((UINT16) tid << (queue.idx%16));  \
        queue.idx += 4;       \
    } while (0)

#define THREADQ_DEQ(queue, tid)                                                  \
    do {                                                                               \
        if ((UINT8) (queue.q[0] & 0x000f) == tid)                                      \
        {                                                                           \
            queue.q[0] = ((queue.q[0] & 0xfff0) >> 4) + ((queue.q[1] & 0x000f) << 12);   \
            queue.q[1] = ((queue.q[1] & 0xfff0) >> 4) + ((queue.q[2] & 0x000f) << 12);   \
            queue.q[2] = ((queue.q[2] & 0xfff0) >> 4) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[0] & 0x00f0) >> 4) == tid)                           \
        {                                                                        \
            queue.q[0] = ((queue.q[0] & 0xff00) >> 4) + (queue.q[0] & 0x000f) + ((queue.q[1] & 0x000f) << 12);   \
            queue.q[1] = ((queue.q[1] & 0xfff0) >> 4) + ((queue.q[2] & 0x000f) << 12);   \
            queue.q[2] = ((queue.q[2] & 0xfff0) >> 4) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[0] & 0x0f00) >> 8) == tid)                           \
        {                                                                        \
            queue.q[0] = ((queue.q[0] & 0xf000) >> 4) + (queue.q[0] & 0x00ff) + ((queue.q[1] & 0x000f) << 12);   \
            queue.q[1] = ((queue.q[1] & 0xfff0) >> 4) + ((queue.q[2] & 0x000f) << 12);   \
            queue.q[2] = ((queue.q[2] & 0xfff0) >> 4) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[0] & 0xf000) >> 12) == tid)                           \
        {                                                                        \
            queue.q[0] = (queue.q[0] & 0x0fff) + ((queue.q[1] & 0x000f) << 12);   \
            queue.q[1] = ((queue.q[1] & 0xfff0) >> 4) + ((queue.q[2] & 0x000f) << 12);   \
            queue.q[2] = ((queue.q[2] & 0xfff0) >> 4) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) (queue.q[1] & 0x000f) == tid)                            \
        {                                                                        \
            queue.q[1] = ((queue.q[1] & 0xfff0) >> 4) + ((queue.q[2] & 0x000f) << 12);   \
            queue.q[2] = ((queue.q[2] & 0xfff0) >> 4) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[1] & 0x00f0) >> 4) == tid)                           \
        {                                                                        \
            queue.q[1] = ((queue.q[1] & 0xff00) >> 4) + (queue.q[1] & 0x000f) + ((queue.q[2] & 0x000f) << 12);   \
            queue.q[2] = ((queue.q[2] & 0xfff0) >> 4) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[1] & 0x0f00) >> 8) == tid)                           \
        {                                                                        \
            queue.q[1] = ((queue.q[1] & 0xf000) >> 4) + (queue.q[1] & 0x00ff) + ((queue.q[2] & 0x000f) << 12);   \
            queue.q[2] = ((queue.q[2] & 0xfff0) >> 4) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[1] & 0xf000) >> 12) == tid)                           \
        {                                                                        \
            queue.q[1] = (queue.q[1] & 0x0fff) + ((queue.q[2] & 0x000f) << 12);   \
            queue.q[2] = ((queue.q[2] & 0xfff0) >> 4) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) (queue.q[2] & 0x000f) == tid)                           \
        {                                                                        \
            queue.q[2] = ((queue.q[2] & 0xfff0) >> 4) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[2] & 0x00f0) >> 4) == tid)                           \
        {                                                                        \
            queue.q[2] = ((queue.q[2] & 0xff00) >> 4) + (queue.q[2] & 0x000f) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[2] & 0x0f00) >> 8) == tid)                           \
        {                                                                        \
            queue.q[2] = ((queue.q[2] & 0xf000) >> 4) + (queue.q[2] & 0x00ff) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[2] & 0xf000) >> 12) == tid)                           \
        {                                                                        \
            queue.q[2] = (queue.q[2] & 0x0fff) + ((queue.q[3] & 0x000f) << 12);   \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) (queue.q[3] & 0x000f) == tid)                           \
        {                                                                        \
            queue.q[3] = ((queue.q[3] & 0xfff0) >> 4);        \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[3] & 0x00f0) >> 4) == tid)                           \
        {                                                                        \
            queue.q[3] = ((queue.q[3] & 0xff00) >> 4) + (queue.q[3] & 0x000f);     \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else if ((UINT8) ((queue.q[3] & 0x0f00) >> 8) == tid)                           \
        {                                                                        \
            queue.q[3] = ((queue.q[3] & 0xf000) >> 4) + (queue.q[3] & 0x00ff);     \
            queue.idx -= 4;                                                  \
        }                                                                        \
        else ;                                                                   \
    } while (0)

#define THREADQ_FETCH(queue, tid_ptr)   \
    do {       \
        *tid_ptr= (UINT8) (queue.q[0] & 0x000f);  \
        queue.q[0] = (queue.q[0] >> 4) + ((queue.q[1] & 0x000f) << 12); \
        queue.q[1] = (queue.q[1] >> 4) + ((queue.q[2] & 0x000f) << 12); \
        queue.q[2] = (queue.q[2] >> 4) + ((queue.q[3] & 0x000f) << 12); \
        queue.q[3] = (queue.q[3] >> 4);   \
        queue.idx -= 4;     \
    } while (0)

#define THREADQ_INIT(queue)    \
    do {       \
        queue.idx = 0;     \
        queue.q[0] = 0;     \
        queue.q[1] = 0;     \
        queue.q[2] = 0;     \
        queue.q[3] = 0;     \
    } while (0)

#define THREADQ_IS_EMPTY(queue) (queue.idx == 0)  
#define THREADQ_IS_FULL(queue)  (queue.idx == 60)
#define THREADQ_NITEM(queue) (queue.idx/4)

typedef struct _threadq
{
    UINT8 idx; // position to insert
    UINT16 q[4]; // q[0][1][2][3]
} THREADQ;

#endif // ~THREAD_EXT_M

#endif // KERNEL_M
#endif // ~THREADQ_H
