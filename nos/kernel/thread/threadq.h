// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 */

#ifndef THREADQ_H
#define THREADQ_H
#include "kconf.h"
#ifdef KERNEL_M

#ifndef THREAD_EXT_M
#define THREADQ_ENQ(queue, tid)                 \
    do {                                        \
        queue.q += (tid << queue.idx);          \
        queue.idx += 3;                         \
    } while (0)

#define THREADQ_ENQ_FIRST(queue, tid)           \
    do {                                        \
        queue.q <<= 3;                          \
        queue.q += tid;                         \
        queue.idx += 3;                         \
    } while (0)

///////////////////////////////
//   BIT TABLE
//
// 0000 0000 0000 0111 = 0x0007
// 0000 0000 0011 1000 = 0x0038
// 0000 0001 1100 0000 = 0x01c0
// 0000 1110 0000 0000 = 0x0e00
// 0111 0000 0000 0000 = 0x7000

// 1111 1111 1111 1000 = 0xfff8
// 1111 1111 1100 0000 = 0xffc0
// 1111 1110 0000 0000 = 0xfe00
// 1111 0000 0000 0000 = 0xf000
// 1000 0000 0000 0000 = 0x8000

#define THREADQ_DEQ(queue, tid)                                       \
    do {                                                              \
        if ((queue.q & 0x0007) == tid)                                \
        {                                                             \
            queue.q = (queue.q & 0xfff8) >> 3;                        \
            queue.idx -= 3;                                           \
        }                                                             \
        else if (((queue.q & 0x0038) >> 3) == tid)                    \
        {                                                             \
            queue.q = ((queue.q & 0xffc0) >> 3) + (queue.q & 0x0007); \
            queue.idx -= 3;                                           \
        }                                                             \
        else if (((queue.q & 0x01c0) >> 6) == tid)                    \
        {                                                             \
            queue.q = ((queue.q & 0xfe00) >> 3) + (queue.q & 0x003f); \
            queue.idx -= 3;                                           \
        }                                                             \
        else if (((queue.q & 0x0e00) >> 9) == tid)                    \
        {                                                             \
            queue.q = ((queue.q & 0xf000) >> 3) + (queue.q & 0x01ff); \
            queue.idx -= 3;                                           \
        }                                                             \
        else if (((queue.q & 0x7000) >> 12) == tid)                   \
        {                                                             \
            queue.q = (queue.q & 0x0fff);                             \
            queue.idx -= 3;                                           \
        }                                                             \
        else ;                                                        \
    } while (0)

#define THREADQ_FETCH(queue, tid_ptr)           \
        do {                                    \
            *tid_ptr= queue.q & 0x0007;         \
            queue.q = queue.q >> 3;             \
            queue.idx -= 3;                     \
        } while (0)

#define THREADQ_INIT(queue)                     \
        do {                                    \
            queue.idx = 0;                      \
            queue.q = 0;                        \
        } while (0)

#define THREADQ_IS_EMPTY(queue) (queue.idx == 0)  
#define THREADQ_IS_FULL(queue)  (queue.idx == 15)
#define THREADQ_NITEM(queue) (queue.idx/3)

typedef struct _threadq 
{
    UINT8 idx; // position to insert
    UINT16 q; // 5/444/333/222/111/000
} THREADQ;
#endif // ~THREAD_EXT_M

#endif // KERNEL_M
#endif // ~THREADQ_H
