// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 */

#include "thread.h"

#ifdef THREAD_M
#include "arch.h"
#include "critical_section.h"
#include "sched.h"

UINT8 nos_thread_sleep(UINT16 ticks)
{
    UINT8 i;
    UINT16 ticks_diff;

    if (ticks == 0) 
        return THREAD_SLEEP_ERROR;

    NOS_ENTER_CRITICAL_SECTION();

    ticks_diff = _init_ticks_left - _ticks_left; // time elapsed in ticks_diff
    _ticks_left = ticks;

    if (_sleep_tid_bit) // if there is a sleeping thread, then, ticks_init_left and _ticks_left will be greater than 0
    {
        // for all sleeping threads, find a minimum _init_ticks_left after substracting ticks_diff
        for (i=1; i<MAX_NUM_TOTAL_THREAD; i++)
        {
            if (NOS_THREAD_IS_SLEEP(i)) 
            {
                tcb[i]->sleep_tick -= ticks_diff;
                if (_ticks_left > tcb[i]->sleep_tick)
                    _ticks_left = tcb[i]->sleep_tick;
            }
        }
    }

    _init_ticks_left = _ticks_left;

    tcb[_rtid]->sleep_tick = ticks;  // sleeping time of this thread in ticks
    _BIT_SET(_sleep_tid_bit, _rtid);   // indicates that this thread goes into sleep
    tcb[_rtid]->state = SLEEPING_STATE; // this thread is being in sleep state

    NOS_EXIT_CRITICAL_SECTION();
    // context switching so that other thread can occupy the CPU while this thread is sleeping.
    nos_ctx_sw();
    return THREAD_NO_ERROR;
}

/**
 * The nos_thread_sleep_ms() function suspends the thread for @p msec
 * milliseconds. Note that accurate sleeping time is not guaranteed for two
 * reasons.
 *
 * First, because it is calculated as unit of scheduling time interval, the
 * sleeping time values may be rounded. For example, if SCHED_TIMER_MS is 250ms,
 * this function sleeps the thread only in the unit of 250ms (250ms, 500ms,
 * 750ms, etc).
 *
 * Second, after this thread is awaken, it is not executed immediately. It is
 * inserted into the ready queue and waits for being selected by the scheduler.
 *
 * Thus, the sleeping time may be more longer than expected. Don't use this
 * function when the exact time requirement is needed.
 */
UINT8 nos_thread_sleep_ms(UINT16 msec)
{
    return nos_thread_sleep(msec / SCHED_TIMER_MS + 1);
}

UINT8 nos_thread_sleep_sec(UINT16 seconds)
{
    // SECONDS * (1000msec/sec) /SCHED_TIMER_MS = TICKS <=65535
    // SECONDS  <= 65535ticks / (1000msec/sec) * SCHED_TIMER_MS
    // SECONDS <= 65.535(ticks/sec) * SCHED_TIMER_MS (5, 10, 32)
    // 65.5 * 5ms <= NOS_THREAD_SLEEP_MAX_SEC <= 65.5 * 32ms
    // NOS_THREAD_SLEEP_MAX_SEC = 327sec(5ms scheduling), 655(10ms scheduling), 2096sec(32ms scheduling)
    if (seconds > NOS_THREAD_SLEEP_MAX_SEC)
    {
        return nos_thread_sleep(65535);
    }
    return nos_thread_sleep( (UINT16)((UINT32)seconds*1000/SCHED_TIMER_MS) );
}


#endif
