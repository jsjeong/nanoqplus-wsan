// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Junkeun Song (ETRI)
 * @author Sangcheol Kim (ETRI)
 */

#include "kconf.h"

#ifdef SEM_M
#include "sem.h"
#include "critical_section.h"
#include "heap.h"

#include "sched.h"
#include "thread.h"
#ifndef THREAD_EXT_M
#include "threadq.h"
#else
#include "threadq_ext.h"
#endif
#include "uart.h"

extern volatile UINT8 _rtid;
#ifndef THREAD_EXT_M
extern UINT8 _suspended_tid_bit;
#else
extern UINT16 _suspended_tid_bit;
#endif
extern THREADQ _rdy_q[PRIORITY_LEVEL_COUNT];
extern volatile UINT8    _ready_priority_bit;    // priority bit setting for threads, which is set for the priority of the thread when the thread becomes in ready state

struct _semaphore
{
    volatile UINT8 val; 	// value of semaphore
    THREADQ	 waitq;		// waiting queue
};

SEMAPHORE nos_semaphore_create(UINT8 value)
{
    struct _semaphore *s;

    s = (struct _semaphore *) nos_malloc(sizeof(struct _semaphore));

    THREADQ_INIT(s->waitq);
    s->val = value; 

    return (SEMAPHORE) s;
}

void nos_semaphore_destroy(SEMAPHORE sem)
{
    nos_free(sem);
}

void nos_signal(SEMAPHORE s)
{
    struct _semaphore *sem;
    UINT8 i, k;

    sem = (struct _semaphore *) s;
    
    NOS_ENTER_CRITICAL_SECTION();
    sem->val++;

    // for every waiting threads except suspended threads
    // because waiting threads can be suspended by other thread
    // but suspended threads must not be ready here

    while (!THREADQ_IS_EMPTY(sem->waitq))
    {
        THREADQ_FETCH(sem->waitq, &i); // i is a fetched thread id
        if (NOS_THREAD_IS_NOT_SUSPENDED(i))
        {
            k = tcb[i]->priority;
            tcb[i]->state = READY_STATE;
            THREADQ_ENQ(_rdy_q[k], i);
            _BIT_SET(_ready_priority_bit, k);
        }
    }

    NOS_EXIT_CRITICAL_SECTION();
    //	printf("tid = %d released semaphore\n", _rtid);

    // To give the semaphore as soon as possible to the next waiting thread for the semaphore
    nos_ctx_sw();
}

void nos_wait(SEMAPHORE s)
{
    struct _semaphore *sem = (struct _semaphore *) s;
    
WAIT_START:
    NOS_ENTER_CRITICAL_SECTION();
    if (sem->val > 0)
    {
        // acquire the semaphore
        sem->val--;
        NOS_EXIT_CRITICAL_SECTION();
        //	printf("tid = %d acquired semaphore\n", _rtid);
    }
    else
    {
        // no resoure
        THREADQ_ENQ(sem->waitq, _rtid);
        tcb[_rtid]->state = WAITING_STATE;

        NOS_EXIT_CRITICAL_SECTION();
        nos_ctx_sw();

        // If the resource is released, this thread resumes from here
        // because we are not sure whether the thread gets the resource or not.
        // It should try again.

        goto WAIT_START;	// need to recheck when resuming
    }

}

BOOL nos_wait_nb(SEMAPHORE s)
{
    struct _semaphore *sem = (struct _semaphore *) s;
    
    NOS_ENTER_CRITICAL_SECTION();
    if (sem->val > 0)
    {
        // acquire the semaphore
        sem->val--;
        NOS_EXIT_CRITICAL_SECTION();

        return TRUE;
    }
    else
    {
        NOS_EXIT_CRITICAL_SECTION();

        return FALSE;
    }
}
#endif // SEM_M
