// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file nos.h
 * @brief Application Programming Interfaces for user applications
 * @author Sangcheol Kim (ETRI)
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#ifndef NOS_H
#define NOS_H

#include "kconf.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "platform.h"

/**
 * @brief NanoQplus OS initialization
 *
 * It should be called by main() when startup.
 */
void nos_init(void);

#include "critical_section.h"

/**
 * Enter a critical section for atomic operations.
 * @warning It disables global interrupts.
 */
#define ENTER_CRITICAL()                        NOS_ENTER_CRITICAL_SECTION()

/**
 * Exit from a critical section.
 * @warning It MUST be called after ENTER_CRITICAL() called.
 */
#define EXIT_CRITICAL()                         NOS_EXIT_CRITICAL_SECTION()

#include "arch.h"
#define mcu_reboot()                            NOS_RESET()
#define delay_us(time)                          nos_delay_us(time) /**< @link nos_delay_us() @endlink */
#define delay_ms(time)                          nos_delay_ms(time) /**< @link nos_delay_ms() @endlink */

#ifdef BATTERY_MONITOR_M
#include "bat-monitor.h"
#define read_battery_level()                    nos_battery_read_level() /**< @link nos_battery_read_level() @endlink */
#else //~BATTERY_MONITOR_M
#define read_battery_level()                    0
#endif

#ifdef KERNEL_M
#include "sched.h"
#define enable_sched()                          NOS_ENABLE_SCHED()
#define disable_sched()                         NOS_DISABLE_SCHED()
#define context_sw()                            nos_ctx_sw() /**< @link nos_ctx_sw() @endlink */
#define sched_start()                           nos_sched_start() /**< @link nos_sched_start() @endlink */

#ifdef THREAD_M
#include "thread.h"
#define PRIORITY_HIGHEST                        NOS_PRIORITY_HIGHEST
#define PRIORITY_HIGH                           NOS_PRIORITY_HIGH
#define PRIORITY_NORMAL                         NOS_PRIORITY_NORMAL
#define PRIORITY_LOW                            NOS_PRIORITY_LOW
#define THREAD_SLEEP_MAX_SEC                    NOS_THREAD_SLEEP_MAX_SEC
#define thread_create(func, args, stack, pri)   nos_thread_create(func, args, stack, pri) /**< @link nos_thread_create() @endlink */
#define thread_sleep(ticks)                     nos_thread_sleep(ticks) /**< @link nos_thread_sleep() @endlink */
#define thread_sleep_ms(ms)                     nos_thread_sleep_ms(ms) /**< @link nos_thread_sleep_ms() @endlink */
#define thread_sleep_sec(sec)                   nos_thread_sleep_sec(sec) /**< @link nos_thread_sleep_sec() @endlink */
#define thread_wakeup(tid)                      nos_thread_wakeup(tid) /**< @link nos_thread_wakeup() @endlink */
#define thread_suspend(tid)                     nos_thread_suspend(tid) /**< @link void nos_thread_suspend() @endlink */
#define thread_resume(tid)                      nos_thread_resume(tid) /**< @link nos_thread_resume() @endlink */
#define thread_join(tid)                        nos_thread_join(tid) /**< @link nos_thread_join() @endlink */
#define thread_kill(tid)                        nos_thread_kill(tid)
#define thread_priority_change(tid, pri)        nos_thread_priority_change(tid, pri) /**< @link nos_thread_priority_change() @endlink */
#define get_thread_id()                         NOS_GET_THREAD_ID()
#define get_thread_prior(tid)                   NOS_GET_THREAD_PRIORITY(tid)
#define get_num_user_threads()                  nos_get_num_user_threads() /**< @link nos_get_num_user_threads() @endlink */
#endif /* THREAD_M */

#include "taskq.h"
#define taskq_reg(func,args)                    nos_taskq_reg(func,args) /**< @link nos_taskq_reg() @endlink */


#ifdef USER_TIMER_M
#include "user_timer.h"
#define USER_TIMER_ONE_SHOT                     NOS_USER_TIMER_ONE_SHOT
#define USER_TIMER_PERIODIC                     NOS_USER_TIMER_PERIODIC
#define USER_TIMER_MAX_SEC                      nos_user_timer_get_max_sec()
#define user_timer_create(func,arg,ticks,opt)   nos_user_timer_create(func,arg,ticks,opt) /**< @link nos_user_timer_create() @endlink */
#define user_timer_create_ms(func,arg,ms,opt)   nos_user_timer_create_ms(func,arg,ms,opt) /**< @link nos_user_timer_create_ms() @endlink */
#define user_timer_create_sec(func,arg,sec,opt)	nos_user_timer_create_sec(func,arg,sec,opt) /**< @link nos_user_timer_create_sec() @endlink */
#define user_timer_destroy(tmid)                nos_user_timer_destroy(tmid) /**< @link nos_user_timer_destroy() @endlink */
#define user_timer_activate(tmid)               nos_user_timer_activate(tmid) /**< @link nos_user_timer_activate() @endlink */
#define user_timer_deactivate(tmid)             nos_user_timer_deactivate(tmid) /**< @link nos_user_timer_deactivate() @endlink */
#endif /* USER_TIMER_M */

#ifdef SEM_M
#include "sem.h"
#define sem_create(val)                         nos_semaphore_create(val) /**< @link nos_semaphore_create() @endlink */
#define sem_destroy(sem)                        nos_semaphore_destroy(sem) /**< @link nos_semaphore_destroy() @endlink */
#define sem_signal(sem)                         nos_signal(sem) /**< @link nos_signal() @endlink */
#define sem_wait(sem)                           nos_wait(sem) /**< @link nos_wait() @endlink */
#define sem_wait_nb(sem)                        nos_wait_nb(sem) /**< @link nos_wait() @endlink */
#endif /* SEM_M */

#ifdef MSGQ_M
#include "msgq.h"
#define MSGQ_UINT8                              NOS_MSGQ_UINT8
#define MSGQ_UINT16                             NOS_MSGQ_UINT16
#define MSGQ_UINT32                             NOS_MSGQ_UINT32
#define msgq_create(type, len)                  nos_msgq_create(type, len) /**< @link nos_msgq_create() @endlink */
#define msgq_destroy(msgq)                      nos_msgq_destroy(msgq) /**< @link nos_msgq_destroy() @endlink */
#define msgq_send(q, msg)                       nos_msgq_send(q, msg) /**< @link nos_msgq_send() @endlink */
#define msgq_recv(q, msg)                       nos_msgq_recv(q, msg) /**< @link nos_msgq_recv() @endlink */
#define msgq_send_nb(q, msg)                    nos_msgq_send_nb(q, msg) /**< @link nos_msgq_send_nb() @endlink */
#define msgq_recv_nb(q, msg)                    nos_msgq_recv_nb(q, msg) /**< @link nos_msgq_recv_nb() @endlink */
#endif /* MSGQ_M */

#endif /* KERNEL_M */

// Basic Functions
#ifdef LED_M
#include "led.h"
#define led_on(n)                               nos_led_on(n) /**< @link nos_led_on() @endlink */
#define led_off(n)                              nos_led_off(n) /**< @link nos_led_off() @endlink */
#define led_toggle(n)                           nos_led_toggle(n) /**< @link nos_led_toggle() @endlink */
#else //~LED_M
// Dummy macros to prevent compile errors.
#define led_on(n)
#define led_off(n)
#define led_toggle(n)
#endif /* LED_M */

#ifdef BUTTON_M
#include "button.h"
#define button_set_callback(func)               nos_button_set_callback(func) /**< @link nos_button_set_callback() @endlink */
#endif

#ifdef UART_M
#include "uart.h"
#define uart_putc(port,byte)                    nos_uart_putc(port, byte)
#define uart_puts(port,string)                  nos_uart_puts(port, string)
#define enable_uart_rx_intr(p)                  nos_uart_enable_rx_intr(p)
#define disable_uart_rx_intr(p)                 nos_uart_disable_rx_intr(p)
#define uart_set_getc_callback(p,func)          nos_uart_set_getc_callback(p, func)
#define uart_gets(port,str,sz)                  nos_uart_gets(port, str, sz)
#endif

#ifdef TIMECHK_M
#include "time_check.h"
//MAXIMUM measurable period : 2sec,  Resolution :  30.517578125us unit
#define timechk_start()                         NOS_TIMECHK_START()
#define timechk_end()                           NOS_TIMECHK_END()
#define timechk_us_duration()                   nos_timechk_us_duration() /**< @link nos_timechk_us_duration() @endlink */
#endif

#ifdef LIB_UTC_CLOCK_M
#include "utc-clock.h"
#define set_utc_time(sec)                 nos_set_utc_time(sec) /**< @link nos_set_utc_time() @endlink */
#define get_utc_time()                    nos_get_utc_time() /**< @link nos_get_utc_time() @endlink */
#endif

#ifdef ADC_M
#define adc_convert(channel)                    nos_adc_convert(channel) /**< @link nos_adc_convert() @endlink */
#endif /* ADC_M */

#ifdef LIBNOS_AES_M
#include "aes.h"
#define aes_cbc128(out,in,len,iv,key,enc)       nos_aes_cbc128(out,in,len,iv,key,enc) /**< @link nos_aes_cbc128() @endlink */
#define aes_cbcmac128(mic,in,len,iv,key)        nos_aes_cbcmac128(mic,in,len,iv,key) /**< @link nos_aes_cbcmac128() @endlink */
#define aes_ctr128(out,in,len,ic,key)           nos_aes_ctr128(out,in,len,ic,key) /**< @link nos_aes_ctr128() @endlink */
#endif /* LIBNOS_AES_M */

// Network
#ifdef IEEE_802_15_4_M
#include "mac.h"

enum
{
    MAC_MAX_SAFE_RX_PAYLOAD_SIZE = NOS_MAC_MAX_SAFE_RX_PAYLOAD_SIZE,
    MAC_MAX_SAFE_TX_PAYLOAD_SIZE = NOS_MAC_MAX_SAFE_TX_PAYLOAD_SIZE,
};
typedef NOS_MAC_RX_INFO MAC_RX_INFO;
typedef NOS_MAC_TX_INFO MAC_TX_INFO;

#define mac_init(dev,panid,sid,eui64,mactype,rxevent)                   \
    nos_mac_init(dev,panid,sid,eui64,mactype,rxevent) /**< @link nos_mac_init() @endlink */
#define mac_tx(dev,frame)                                     \
    nos_mac_tx(dev,frame) /**< @link nos_mac_tx() @endlink */
#define mac_rx(dev,frame)                                     \
    nos_mac_rx(dev,frame) /**< @link nos_mac_rx() @endlink */
#define mac_get_tx_payload_len(dev,dst_eui64,src_eui64,sec)             \
    nos_mac_get_max_tx_payload_len(dev,dst_eui64,src_eui64,sec) /**< @link nos_mac_get_max_tx_payload_len() @endlink */

#endif /* IEEE_802_15_4_M */

#if defined MESH_ROUTING_M
#include "routing.h"
#define NWK_MAX_PAYLOAD_SIZE                    NOS_NWK_MAX_PAYLOAD_SIZE
#define nwk_init(panid,id,cb_func_ptr)          nos_nwk_init(panid,id,cb_func_ptr) /**< @link nos_nwk_init() @endlink */
#define nwk_set_rx_cb(func)                     nos_nwk_set_rx_cb(func) /**< @link nos_nwk_set_rx_cb() @endlink */
#define nwk_get_my_channel()                    nos_nwk_get_opmode() /**< @link nos_nwk_get_my_channel() @endlink */
#define nwk_get_my_pan_id()                     nos_nwk_get_pan_id() /**< @link nos_nwk_get_my_pan_id() @endlink */
#define nwk_get_my_short_id()                   nos_nwk_get_short_id() /**< @link now_nwk_get_my_short_id() @endlink */

//FIXME More abstractions are needed.
#ifdef RENO_M

#define nwk_rx(src_id, port, len, data)         nos_nwk_rx(src_id, port, len, data) /**< @link nos_nwk_rx() @endlink */
#define nwk_tx(dest_id, port, len, data)        nos_nwk_tx(dest_id, port, len, data) /**< @link nos_nwk_tx() @endlink */
#endif /* RENO_M */

#ifdef TENO_M
#define nwk_rx(src_id, port, len, data)         nos_nwk_rx(src_id, port, len, data, NULL, NULL) //void teno_recv_from_nwk(UINT16* src_id, UINT8* port, UINT8* data_length, void* data, UINT16* parent_id, UINT16* dest_id);
#define nwk_tx(dest_id, port, len, data)        nos_nwk_tx(dest_id, port, len, data) //BOOL teno_send_to_node(UINT16 dest_id, UINT8 port, UINT8 data_length, void* data);
#define nwk_tx_to_sink(port, len, data)         nos_nwk_tx_to_sink(port, len, data) //BOOL teno_send_to_sink(UINT8 port, UINT8 data_length, void* data);
#define nwk_set_to_sink()                       nos_nwk_set_to_sink() // void teno_role_as_sink(void);
#define nwk_set_to_node()                       nos_nwk_set_to_node() //void teno_role_as_node(void);
#endif /* TENO_M */

#endif /* MESH_ROUTING_M */

#ifdef PPP_M
#include "ppp-os.h"
#endif

#ifdef COAP_M
#include "coap.h"
#define coap_init(port)                         nos_coap_init(port) /**< @link nos_coap_init() @endlink */
#define coap_request(src_port,dst_addr,dst_port,uri_path,method,payload,payload_len,content_type,f) \
    nos_coap_request(src_port,dst_addr,dst_port,uri_path,method,payload,payload_len,content_type,f) /**< @link nos_coap_request() @endlink */
#define coap_cancel_request(req)                nos_coap_cancel_request(req) /**< @link nos_coap_cancel_request() @endlink */
#define coap_make_resource(parent,name,tag)     nos_coap_make_resource(parent,name,tag) /**< @link nos_coap_register_resource() @endlink */
#define coap_remove_resource(r)                 nos_coap_remove_resource(r) /**< @link nos_coap_remove_resource() @endlink */
#define coap_get_resource(parent,loc,loc_len)   nos_coap_get_resource(parent,loc,loc_len) /**< @link nos_coap_?get_resource() @endlink */
#define coap_get_resource_by_string(parent,loc) \
    nos_coap_get_resource(parent,loc,strlen(loc)) /**< @link nos_coap_?get_resource() @endlink */
#define coap_set_action_on_request(r,flags,f) \
    nos_coap_set_action_on_request(r,flags,f) /**< @link nos_coap_set_func_on_request() @endlink */
#define coap_put_answer(a,m,l)                 nos_coap_put_answer(a,m,l) /**< @link nos_coap_put_answer() @endlink */
#endif /* COAP_M */

#ifdef ZIGBEE_IP_M
#include "zigbee-ip.h"
#endif

// Storage System
#ifdef STORAGE_M
#include "storage.h"
#define storage_get_size(id,size)               nos_storage_get_size(id,size)
#define storage_read(id,addr,data,len)          nos_storage_read(id,addr,data,len) /**< @link nos_storage_read() @endlink */
#define storage_write(id,addr,data,len)         nos_storage_write(id,addr,data,len) /**< @link nos_storage_write() @endlink */
#define storage_erase(id,addr,len)              nos_storage_erase(id,addr,len) /**< @link nos_storage_erase() @endlink */
#define storage_erase_all(id)                   nos_storage_erase_all(id) /**< @link nos_storage_erase_all() @endlink */
#endif /* STORAGE_M */

// Architecture specific definitions
#ifdef ATMEGA128
#define SLEEP_IDLE                              NOS_IDLE
#define SLEEP_ADC                               NOS_ADC
#define SLEEP_PWR_DOWN                          NOS_PWR_DOWN
#define SLEEP_PWR_SAVE                          NOS_PWR_SAVE
#define SLEEP_STANDBY                           NOS_STANDBY
#define SLEEP_EXT_STANDBY                       NOS_EXT_STANDBY
#ifdef EEPROM_M
#include <avr/eeprom.h>
#endif /* EEPROM_M */
#endif /* ATMEGA128 */

#ifdef MSP430F1611
#include "temp_diode.h"
#define mcu_temp_get_data()                     nos_intergrated_temp_get_data()  /**< @link nos_integrated_temp_get_data() @endlink */
#endif /* MSP430F1611 */

// Device specific definitions
#ifdef SENSOR_INTERNAL_VOLTAGE_M
#include "generic-internal-voltage.h"
#define internal_voltage_sensor_get_data()      nos_internal_voltage_sensor_get_data()  /**< @link nos_internal_voltage_sensor_get_data() @endlink */
#endif /* SENSOR_INTERNAL_VOLTAGE_M */

// Default device ID to select default devices of a platform.
#define DEFAULT 0

#ifdef AT86RF212_M
#include "rf212-api.h"
#endif

#ifdef CC2520_M
#include "cc2520-api.h"
#endif

#ifdef SENSOR_GAS_M
#include "generic-gas-sensor.h"
#define gas_sensor_on()                         nos_gas_sensor_power_on() /**< @link nos_gas_sensor_power_on() @endlink */
#define gas_sensor_off()                        nos_gas_sensor_power_off() /**< @link nos_gas_sensor_power_off() @endlink */
#define gas_sensor_get_data()                   nos_gas_sensor_get_data() /**< @link nos_gas_sensor_get_data() @endlink */
#endif /* SENSOR_GAS_M */

#ifdef SENSOR_M
#include "sensor.h"
#define sensor_on(id)                           nos_sensor_on(id)
#define sensor_off(id)                          nos_sensor_off(id)
#define sensor_read(id)                         nos_sensor_read(id)
#endif

#ifdef CLCD_M
#include "clcd.h"
#endif

#ifdef SENSOR_MIC_M
#include "generic-mic-sensor.h"
#define mic_sensor_on()                         nos_mic_sensor_power_on() /**< @link nos_mic_sensor_power_on() @endlink */
#define mic_sensor_off()                        nos_mic_sensor_power_off() /**< @link nos_mic_sensor_power_off() @endlink */
#define mic_sensor_get_data()                   nos_mic_sensor_get_data() /**< @link nos_mic_sensor_get_data() @endlink */
#endif /* SENSOR_MIC_M */

#ifdef SENSOR_TEMPERATURE_M
#include "generic-temperature-sensor.h"
#ifdef HAVE_DEVICE_IDS
#define temperature_sensor_on(id)               nos_temperature_sensor_on(id)
#define temperature_sensor_off(id)              nos_temperature_sensor_off(id)
#define temperature_sensor_read(id)             nos_temperature_sensor_read(id)

#else //~HAVE_DEVICE_IDS: for backward compatibility
#define temperature_sensor_on()                 nos_temperature_sensor_power_on() /**< @link nos_temperature_sensor_power_on() @endlink */
#define temperature_sensor_off()                nos_temperature_sensor_power_off() /**< @link nos_temperature_sensor_power_off() @endlink */
#define temperature_sensor_get_data()           nos_temperature_sensor_get_data() /**< @link nos_temperature_sensor_get_data() @endlink */
#endif //~HAVE_DEVICE_IDS
#endif /* SENSOR_TEMPERATURE_M */

#ifdef SENSOR_HUMIDITY_M
#include "generic-humidity-sensor.h"
#ifdef HAVE_DEVICE_IDS
#define humidity_sensor_on(id)                  nos_humidity_sensor_on(id)
#define humidity_sensor_off(id)                 nos_humidity_sensor_off(id)
#define humidity_sensor_read(id)                nos_humidity_sensor_read(id)

#else //~HAVE_DEVICE_IDS: for backward compatibility
#define humidity_sensor_on()                    nos_humidity_sensor_power_on() /**< @link nos_humidity_sensor_power_on() @endlink */
#define humidity_sensor_off()                   nos_humidity_sensor_power_off() /**< @link nos_humidity_sensor_power_off() @endlink */
#define humidity_sensor_get_data()              nos_humidity_sensor_get_data() /**< @link nos_humidity_sensor_get_data() @endlink */
#endif //~HAVE_DEVICE_IDS
#endif /* SENSOR_HUMIDITY_M */

#ifdef SENSOR_PIR_M
#include "generic-pir-sensor.h"
#define pir_sensor_on()                         nos_pir_sensor_power_on() /**< @link nos_pir_sensor_power_on() @endlink */
#define pir_sensor_off()                        nos_pir_sensor_power_off() /**< @link nos_pir_sensor_power_off() @endlink */
#define pir_sensor_get_data()                   nos_pir_sensor_get_data() /**< @link nos_pir_sensor_get_data() @endlink */
#define pir_sensor_set_callback(func)           nos_pir_sensor_set_callback(func) /**< @link nos_pir_sensor_set_callback() @endlink */
#endif /* SENSOR_PIR_M */

#ifdef SENSOR_ULTRASONIC_M
#include "generic-ultrasonic-sensor.h"
#define ultrasonic_sensor_on()                  nos_ultrasonic_sensor_power_on() /**< @link nos_ultrasonic_sensor_power_on() @endlink */
#define ultrasonic_sensor_off()                 nos_ultrasonic_sensor_power_off() /**< @link nos_ultrasonic_sensor_power_off() @endlink */
#define ultrasonic_sensor_set_callback(f)       nos_ultrasonic_sensor_set_callback(f) /**< @link nos_ultrasonic_sensor_set_callback() @endlink */
#define ultrasonic_sensor_trigger()             nos_ultrasonic_sensor_trigger() /**< @link nos_ultrasonic_sensor_trigger() @endlink */
#define ultrasonic_sensor_get_data()            nos_ultrasonic_sensor_get_data() /**< @link nos_ultrasonic_sensor_get_data() @endlink */
#endif /* SENSOR_ULTRASONIC_M */

#ifdef RELAY_M
#include "generic-relay.h"
#define relay_on(id)                            nos_relay_on(id) /**< @link nos_relay_on() @endlink */
#define relay_off(id)                           nos_relay_off(id) /**< @link nos_relay_off() @endlink */
#define relay_toggle(id)                        nos_relay_toggle(id) /**< @link nos_relay_toggle() @endlink */
#define relay_get_state(id)                     nos_relay_get_state(id) /**< @link nos_relay_get_state() @endlink */
#endif /* RELAY_M */

#ifdef POWERMETER_M
#include "generic-powermeter.h"
#define powermeter_get_state()                  nos_powermeter_get_state() /**< @link nos_powermeter_get_state() @endlink */
#endif /* POWERMETER_M */

#ifdef EMLCD_M
#include "emlcd.h"
#define emlcd_print_string(str)                 nos_emlcd_print_string(str) /**< @link nos_emlcd_print_string() @endlink */
#endif /* EMLCD_M */

/* NanoQplus based libraries */
#include <6lowpan-api.h>
#include <ether6-api.h>
#include <ip6-api.h>
#include <rpl-api.h>
#include <udp-api.h>
#include <tcp-api.h>
#include <trickle.h>

/* External libraries */
// CyaSSL-2.8.0
#include <cyassl/ctaocrypt/md5.h>
#include <cyassl/ctaocrypt/md4.h>
#include <cyassl/ctaocrypt/sha.h>
#include <cyassl/ctaocrypt/sha256.h>
#include <cyassl/ctaocrypt/sha512.h>
#include <cyassl/ctaocrypt/arc4.h>
#include <cyassl/ctaocrypt/random.h>
#include <cyassl/ctaocrypt/coding.h>
#include <cyassl/ctaocrypt/rsa.h>
#include <cyassl/ctaocrypt/des3.h>
#include <cyassl/ctaocrypt/aes.h>
#include <cyassl/ctaocrypt/camellia.h>
#include <cyassl/ctaocrypt/hmac.h>
#include <cyassl/ctaocrypt/dh.h>
#include <cyassl/ctaocrypt/dsa.h>
#include <cyassl/ctaocrypt/hc128.h>
#include <cyassl/ctaocrypt/rabbit.h>
#include <cyassl/ctaocrypt/pwdbased.h>
#include <cyassl/ctaocrypt/ripemd.h>
#include <cyassl/ctaocrypt/ecc.h>

//
#include <xively/xively.h>
#include <xively/xi_helpers.h>
#endif /* NOS_H */
