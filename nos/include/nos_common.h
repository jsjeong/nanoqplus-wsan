// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file nos_common.h
 * @brief Common constants, keywords, macros, and functions.
 * @author Sangcheol Kim (ETRI)
 */

#ifndef __NOS_COMMON_H__
#define __NOS_COMMON_H__

#include "typedef.h"

// ASCII Characters
#define _BS    (0x08)    // Back Space (ASCII)
#define _CR    (0x0D)    // Carriage Return (ASCII)
#define _LF    (0x0A)    // LineFeed
#define _SPACE (0x20)    //Space

#ifdef __IAR_SYSTEMS_ICC__
#undef _MIN
#undef _MAX
#endif

#ifndef _MIN
#define _MIN(n,m) (((n) < (m)) ? (n) : (m)) // get a minimum number among n and m
#endif

#ifndef _MAX
#define _MAX(n,m) (((n) < (m)) ? (m) : (n)) // get a max number among n and m
#endif /* ~_MAX */

#define _ABS(n)  (((n) < 0) ? (-(n)) : (n)) // get absolute number 
#define _BYTES2WORD(low, high) ( (UINT16)(low) | (UINT16)((high)<<8) )

#ifndef FALSE
enum { FALSE = 0 };
#endif

#ifndef TRUE
enum { TRUE = 1 };
#endif

#ifndef NULL
enum { NULL = 0 };
#endif

#define _BIT_SET(reg,n) ((reg) |= (1 << (n)))
#define _BIT_CLR(reg,n) ((reg) &= ~(1 << (n)))
#define _IS_SET(reg,n)  ((((reg) & (1 << (n))) != 0) ? TRUE : FALSE)

/**
 * Delay @p usec microseconds.
 */
void nos_delay_us(UINT16 usec);

/**
 * Delay @p msec milliseconds.
 */
void nos_delay_ms(UINT16 msec);

/**
 * Convert 2-byte unsigned integer @p a from network to byte byte order.
 */
UINT16 ntohs(UINT16 a);

/**
 * Convert 2-byte unsigned integer @p a from host to byte byte order.
 */
#define htons(a) ntohs(a)

/**
 * Convert 4-byte unsigned integer @p a from network to host byte order.
 */
UINT32 ntohl(UINT32 a);

/**
 * Convert 4-byte unsigned integer @p a from host to byte order.
 */
#define htonl(a) ntohl(a)

#endif // !__NOS_COMMON_H__
