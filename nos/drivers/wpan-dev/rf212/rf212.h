// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jeehoon Lee (UST-ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#ifndef AT86RF212_H
#define AT86RF212_H
#include "kconf.h"
#ifdef AT86RF212_M

#include "ieee-802-15-4.h"
#include "rf212-api.h"

// O-QPSK modulation. 250Kbps. us. related to modulation of at86rf212_init().
#define SYMBOL_DURATION 16

enum rf212_clkm
{
    RF212_CLKM_16MHZ,
    RF212_CLKM_8MHZ,
    RF212_CLKM_4MHZ,
    RF212_CLKM_2MHZ,
    RF212_CLKM_1MHZ,
    RF212_CLKM_250KHZ,
    RF212_CLKM_SYMRATE,
    RF212_CLKM_NOCLK,
};

void rf212_init(UINT8 dev_id, enum rf212_clkm clkm);
void rf212_interrupt_handler(UINT8 dev_id);

void at86rf212_set_rx_handler(BOOL (*mac_event_handler)(void));

void at86rf212_set_csma(UINT8 retries, UINT8 max_be, UINT8 min_be);
void at86rf212_set_tx_power(INT8 dBm);
void at86rf212_set_channel(UINT8 channel);
UINT8 at86rf212_get_opmode(void);

void at86rf212_osc_off(UINT8 dev_id);
void at86rf212_osc_on(UINT8 dev_id);

void at86rf212_flush_rxbuf(void);

UINT8 at86rf212_read_frame_buf(UINT8 *buf, INT8 *rssi, UINT8 *lqi, BOOL *crc_ok);

BOOL at86rf212_auto_tx(NOS_MAC_TX_INFO *tx_info);

#define TAT_STATUS_START_VALUE                  ( 0x40 )
typedef enum
{
    TAT_SUCCESS = TAT_STATUS_START_VALUE,
    //!< The requested service was performed successfully.
    TAT_UNSUPPORTED_DEVICE,
    //!< The connected device is not an Atmel AT86RF230.
    TAT_INVALID_ARGUMENT,
    //!< One or more of the supplied function arguments are invalid.
    TAT_TIMED_OUT,
    //!< The requested service timed out.
    TAT_WRONG_STATE,
    //!< The end-user tried to do an invalid state transition.
    TAT_BUSY_STATE,
    //!< The radio transceiver is busy receiving or transmitting.
    TAT_STATE_TRANSITION_FAILED,
    //!< The requested state transition could not be completed.
    TAT_CCA_IDLE,
    //!< Channel in idle. Ready to transmit a new frame.
    TAT_CCA_BUSY,
    //!< Channel busy.
    TAT_TRX_BUSY,
    //!< Transceiver is busy receiving or transmitting data.
    TAT_BAT_LOW,
    //!< Measured battery voltage is lower than voltage threshold.
    TAT_BAT_OK,
    //!< Measured battery voltage is above the voltage threshold.
    TAT_CRC_FAILED,
    //!< The CRC failed for the actual frame.
    TAT_CHANNEL_ACCESS_FAILURE,
    //!< The channel access failed during the auto mode.
    TAT_NO_ACK,
    //!< No acknowledge frame was received.
} tat_status_t;

struct rf212
{
    BOOL use_eui64:1;
    BOOL frame_received:1;
    BOOL frame_ready_to_send:1;
    UINT8 channel;
    enum rf212_mode mode;
};

#endif // AT86RF212_M
#endif // ~AT86RF212_H
