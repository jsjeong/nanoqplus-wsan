// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jeehoon Lee (UST-ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "rf212.h"

#ifdef AT86RF212_M
#include <string.h>
#include "rf212-interface.h"
#include "rf212-mem-access.h"
#include "rf212-const.h"
#include "critical_section.h"
#include "arch.h"
#include "wpan-dev.h"
#include "platform.h"

#ifdef GLOSSY_M
#include "rf212-glossy.h"
#endif

//#define AT86RF212_DEBUG
#ifdef AT86RF212_DEBUG
#include <stdio.h>
#endif

#ifdef AT86RF212_ON_CHIP_AUTO_TRX
#ifdef IEEE_802_15_4_M
static UINT8 at86rf212_csma_retries =
        IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BACKOFFS;
static UINT8 at86rf212_csma_min_be = IEEE_802_15_4_2006_DEFAULT_CSMA_MIN_BE;
static UINT8 at86rf212_csma_max_be = IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BE;
#endif /* IEEE_802_15_4_M */
#endif /* AT86RF212_ON_CHIP_AUTO_TRX */

extern WPAN_DEV wpan_dev[];

ERROR_T rf212_set_channel(UINT8 dev_id, UINT8 channel)
{
    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;

    if (channel > MAX_CHANNEL)
    {
        channel = 5; //914MHz
    }
    rf212_subreg_write(dev_id, SR_CHANNEL, channel);
    wpan_dev[dev_id].c.rf212.channel = channel;
    return ERROR_SUCCESS;
}

ERROR_T rf212_set_mode(UINT8 dev_id, enum rf212_mode mode)
{
    UINT8 phy_mode;
    UINT8 tx_offset;

    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;

    wpan_dev[dev_id].c.rf212.mode = mode;

    if (mode == RF212_BPSK_20KBPS)
    {
        phy_mode = 0x20;
        tx_offset = TX_POWER_OFFSET_PLUS_2;
        wpan_dev[dev_id].symbol_duration = 50;
    }
    else if (mode == RF212_BPSK_40KBPS)
    {
        phy_mode = 0x24;
        tx_offset = TX_POWER_OFFSET_PLUS_2;
        wpan_dev[dev_id].symbol_duration = 25;
    }
    else if (mode == RF212_OQPSK_100KBPS)
    {
        phy_mode = 0x08;
        tx_offset = TX_POWER_OFFSET_PLUS_1;
        wpan_dev[dev_id].symbol_duration = 40;
    }
    else if (mode == RF212_OQPSK_250KBPS_SIN)
    {
        phy_mode = 0x2c;
        tx_offset = TX_POWER_OFFSET_PLUS_1;
        wpan_dev[dev_id].symbol_duration = 16;
    }
    else if (mode == RF212_OQPSK_250KBPS_RC_0_8)
    {
        phy_mode = 0x1c;
        tx_offset = TX_POWER_OFFSET_PLUS_1;
        wpan_dev[dev_id].symbol_duration = 16;
    }
    else
    {
        return ERROR_NOT_SUPPORTED;
    }

    rf212_reg_write(dev_id, RG_TRX_CTRL_2, 
                    phy_mode | 
                    FRAME_BUFFER_PROTECTION);
    rf212_reg_write(dev_id, RG_RF_CTRL_0, 
                    PA_LT_2us | 
                    tx_offset);
    return ERROR_SUCCESS;
}

void rf212_set_tx_power(UINT8 dev_id, INT8 dBm)
{
    // ref (p. 107)
    const UINT8 pwr_reg_table[22] =
    { 0x0d, 0x0c, 0x0b, 0x0a, 0x09, 0x08, 0x07, 0x06, 0x05, 0x04, 0x25, 0x24,
            0x23, 0x22, 0x42, 0x85, 0x84, 0x83, 0x82, 0x81, 0xa1, 0xe1 };

    if (dBm > 10 || dBm < -11)
        dBm = 4;
    dBm = pwr_reg_table[dBm + 11]; // set range into 0~21 (-11dBm ~ 10dBm)

    rf212_reg_write(dev_id, RG_PHY_TX_PWR, dBm);
}

static void rf212_pll_on(UINT8 dev_id)
{
    UINT8 st;

#ifdef AT86RF212_DEBUG
    UINT8 intr, i = 0;
#endif

#ifdef AT86RF212_DEBUG
    printf("%s()\n", __FUNCTION__);
#endif

    rf212_reg_write(dev_id, RG_IRQ_MASK, 0);
    
    st = rf212_subreg_read(dev_id, SR_TRX_STATUS);
    if (st == TRX_STATUS_TRX_OFF)
    {
        rf212_subreg_write(dev_id, SR_TRX_CMD, CMD_PLL_ON);
        nos_delay_us(110); //t_TR4
    }
    else if (st == TRX_STATUS_RX_ON ||
             st == TRX_STATUS_PLL_ON ||
             st == TRX_STATUS_TX_ARET_ON ||
             st == TRX_STATUS_RX_AACK_ON)
    {
        rf212_subreg_write(dev_id, SR_TRX_CMD, CMD_PLL_ON);
        nos_delay_us(1); //t_TR9
    }
    else if (st == TRX_STATUS_BUSY_TX ||
             st == TRX_STATUS_BUSY_TX_ARET ||
             st == TRX_STATUS_BUSY_RX ||
             st == TRX_STATUS_BUSY_RX_AACK)
    {
        rf212_subreg_write(dev_id, SR_TRX_CMD, CMD_PLL_ON);
    }

    while (rf212_subreg_read(dev_id, SR_TRX_STATUS) != TRX_STATUS_PLL_ON);

    rf212_clear_mcu_irq(dev_id);
    rf212_reg_read(dev_id, RG_IRQ_STATUS);
    rf212_reg_write(dev_id, RG_IRQ_MASK, IRQ3_TRX_END);
}

static void rf212_listen(UINT8 dev_id)
{
    UINT8 st;
#ifdef AT86RF212_DEBUG
    UINT8 i = 0;
#endif

#ifdef AT86RF212_DEBUG
    printf("%s()\n", __FUNCTION__);
#endif

#ifdef AT86RF212_ON_CHIP_AUTO_TRX
    
    /* Change state from TRX_OFF to RX_AACK_ON.
     * Set the radio to RX_AACK_ON state.
     */
    rf212_subreg_write(dev_id, SR_TRX_CMD, CMD_RX_AACK_ON);
    do {
        st = rf212_subreg_read(dev_id, SR_TRX_STATUS);
    } while(st != TRX_STATUS_RX_AACK_ON);
    
#else // ~AT86RF212_ON_CHIP_AUTO_TRX

    do {
        st = rf212_subreg_read(dev_id, SR_TRX_STATUS);
        if (st == TRX_STATUS_TRX_OFF)
        {
            rf212_subreg_write(dev_id, SR_TRX_CMD, CMD_RX_ON);
            nos_delay_us(110); //t_TR6
        }
        else if (st == TRX_STATUS_PLL_ON)
        { 
            rf212_subreg_write(dev_id, SR_TRX_CMD, CMD_RX_ON);
            nos_delay_us(1); //t_TR8
        }

#ifdef AT86RF212_DEBUG
        if (i < 20)
        {
            printf("l:0x%x\n", st);
            i++;
        }
#endif
    } while(st != TRX_STATUS_RX_ON);

#endif // ~AT86RF212_ON_CHIP_AUTO_TRX

    //chip_status.frame_ready_to_send = FALSE;
}

// prepare to RX (config. and turn on)
static void rf212_config(UINT8 dev_id)
{
#ifdef AT86RF212_ON_CHIP_AUTO_TRX
    at86rf212_set_csma(IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BACKOFFS,
                       IEEE_802_15_4_2006_DEFAULT_CSMA_MAX_BE,
                       IEEE_802_15_4_2006_DEFAULT_CSMA_MIN_BE);
#endif

    rf212_reg_read(dev_id, RG_IRQ_STATUS);
    rf212_reg_write(dev_id, RG_IRQ_MASK, IRQ3_TRX_END);
    rf212_listen(dev_id);
}

#ifdef AT86RF212_ON_CHIP_AUTO_TRX
void at86rf212_set_csma(UINT8 retries, UINT8 max_be, UINT8 min_be)
{
    UINT8 max_min_be;

    /* random seed for csma. Works in basic mode only(?) */

    // The short_address is used as a random number seed.
    rf212_reg_write(dev_id, RG_CSMA_SEED_0,
            chip_short_addr << rf212_subreg_read(SR_RND_VALUE)); // RND_VALUE:2bits
    rf212_subreg_write(SR_CSMA_SEED_1, chip_short_addr >> 13); // high 3bits

    /* max_csma_retries */
    rf212_subreg_write(SR_MAX_CSMA_RETRIES, retries); // default:4. 1~5
    at86rf212_csma_retries = retries;

    max_min_be = (max_be << 4) | min_be;
    rf212_reg_write(dev_id, RG_CSMA_BE, max_min_be);
    at86rf212_csma_min_be = min_be;
    at86rf212_csma_max_be = max_be;
}
#endif /* AT86RF212_ON_CHIP_AUTO_TRX */

void rf212_set_pan_id(UINT8 dev_id)
{
#ifdef AT86RF212_ON_CHIP_AUTO_TRX
    UINT8 tmp_byte = wpan_dev[dev_id].pan_id & 0xFF;
    rf212_reg_write(dev_id, RG_PAN_ID_0, tmp_byte);

    tmp_byte = (wpan_dev[dev_id].pan_id >> 8) & 0xFF;
    rf212_reg_write(dev_id, RG_PAN_ID_1, tmp_byte); // higher 8bit
#endif
}

void rf212_set_short_address(UINT8 dev_id)
{
#ifdef AT86RF212_ON_CHIP_AUTO_TRX
    UINT8 short_address_byte = wpan_dev[dev_id].short_id & 0xFF;
    rf212_reg_write(dev_id, RG_SHORT_ADDR_0, short_address_byte);

    short_address_byte = (wpan_dev[dev_id].short_id >> 8) & 0xFF;
    rf212_reg_write(dev_id, RG_SHORT_ADDR_1, short_address_byte); // higher 8bit
#endif
}

void at86rf212_set_ieee_addr(const UINT8* eui64)
{
#ifdef AT86RF212_ON_CHIP_AUTO_TRX
    rf212_reg_write(dev_id, RG_IEEE_ADDR_0, eui64[7]); // the lowest byte
    rf212_reg_write(dev_id, RG_IEEE_ADDR_1, eui64[6]);
    rf212_reg_write(dev_id, RG_IEEE_ADDR_2, eui64[5]);
    rf212_reg_write(dev_id, RG_IEEE_ADDR_3, eui64[4]);
    rf212_reg_write(dev_id, RG_IEEE_ADDR_4, eui64[3]);
    rf212_reg_write(dev_id, RG_IEEE_ADDR_5, eui64[2]);
    rf212_reg_write(dev_id, RG_IEEE_ADDR_6, eui64[1]);
    rf212_reg_write(dev_id, RG_IEEE_ADDR_7, eui64[0]); // the highst byte
#endif /* AT86RF212_ON_CHIP_AUTO_TRX */
}

#ifdef UTC_CLOCK_M
#include "utc_clock.h"

extern UINT32 radio_on_timestamp;
extern UINT8 radio_on_ticks;
extern UINT32 radio_off_timestamp;
extern UINT8 radio_off_ticks;
#endif

void rf212_osc_off(UINT8 dev_id)
{
    BOOL sleeping;
    UINT8 st;

    rf212_get_pin(dev_id, RF212_PIN_SLP_TR, &sleeping);
    if (sleeping)
    {
        // Already sleeping.
        return;
    }

    rf212_reg_write(dev_id, RG_IRQ_MASK, 0);

    do
    {
        st = rf212_subreg_read(dev_id, SR_TRX_STATUS);
        
        if (st == TRX_STATUS_RX_ON || 
            st == TRX_STATUS_PLL_ON)
        {
            rf212_subreg_write(dev_id, SR_TRX_CMD, CMD_FORCE_TRX_OFF);
            nos_delay_us(TIME_CMD_FORCE_TRX_OFF);
        }
    } while(st != TRX_STATUS_TRX_OFF);

    rf212_reg_read(dev_id, RG_IRQ_STATUS);
    rf212_reg_write(dev_id, RG_IRQ_MASK, IRQ3_TRX_END);

    rf212_set_pin(dev_id, RF212_PIN_SLP_TR, TRUE);

#ifdef UTC_CLOCK_M
    NOS_ENTER_CRITICAL_SECTION();
    radio_off_ticks = TCNT2;
    radio_off_timestamp = nos_get_utc_time();
    NOS_EXIT_CRITICAL_SECTION();
    nos_led_off(1);
#endif

#ifdef AT86RF212_DEBUG
    printf("\\\n");
#endif
}

void rf212_osc_on(UINT8 dev_id)
{
    UINT8 st;
    BOOL sleeping;

#ifdef AT86RF212_DEBUG
    printf("/\n");
#endif

    rf212_get_pin(dev_id, RF212_PIN_SLP_TR, &sleeping);
    if (!sleeping)
    {
        // Already waked up state.
        return;
    }
    
    rf212_set_pin(dev_id, RF212_PIN_SLP_TR, FALSE); // wakeup (TRX_OFF)
    nos_delay_us(TIME_SLEEP_TO_TRX_OFF); // 880us

    do
    {
        st = rf212_subreg_read(dev_id, SR_TRX_STATUS);
    } while(st != TRX_STATUS_TRX_OFF);

    rf212_listen(dev_id);

#ifdef UTC_CLOCK_M    
    NOS_ENTER_CRITICAL_SECTION();
    radio_on_ticks = TCNT2;
    radio_on_timestamp = nos_get_utc_time();
    nos_led_on(1);
    NOS_EXIT_CRITICAL_SECTION();
#endif
}

static void rf212_write_frame(UINT8 dev_id, const UINT8 *buf)
{
#ifdef AT86RF212_DEBUG
    printf("%s()-begins\n", __FUNCTION__);
#endif

    rf212_pll_on(dev_id);
    rf212_write_frame_buf(dev_id, buf);
    wpan_dev[dev_id].c.rf212.frame_ready_to_send = TRUE;
}

#ifdef AT86RF212_ON_CHIP_AUTO_TRX
BOOL at86rf212_auto_tx(NOS_MAC_TX_INFO *tx_info)
{
    // Extended Operating Mode
    UINT8 tx_status;

    // trials
    rf212_subreg_write(SR_MAX_FRAME_RETRIES, tx_info->tx_count);

    // Set the radio to PLL_ON state as soon as RX_AACK is completed.
    rf212_pll_on(dev_id);

    // turn off TX interrupt while TXing
    rf212_reg_write(dev_id, RG_IRQ_MASK, 0);

    if (chip_status.frame_ready_to_send == FALSE)
    {
        //write a frame to TRX buffer
        rf212_write_frame(tx_info);
    }

    //Set the radio to TX_ARET_ON state.
    rf212_subreg_write(SR_TRX_CMD, CMD_TX_ARET_ON);
    while (rf212_subreg_read(SR_TRX_STATUS) != TRX_STATUS_TX_ARET_ON); // from/to PLL state: 1us

    // Toggle the SLP_TR pin to initiate the frame transmission.
    rf212_set_pin(RF212_PIN_SLP_TR, TRUE);
    nos_delay_us(0);
    rf212_set_pin(RF212_PIN_SLP_TR, FALSE);

    // Wait for TX done.
    while (rf212_subreg_read(SR_TRX_STATUS) != TRX_STATUS_BUSY_TX_ARET);
    while (rf212_subreg_read(SR_TRX_STATUS) != TRX_STATUS_TX_ARET_ON);

    // Read TX result. (ref p.61)
    tx_status = rf212_subreg_read(SR_TRAC_STATUS);

#ifdef AT86RF212_DEBUG
    printf("%s()-result:0x%x\n", __FUNCTION__, tx_status);
#endif

    if (tx_status == TRAC_SUCCESS || tx_status == TRAC_SUCCESS_DATA_PENDING)
    {
        /*
         * There may be no way to get the number of transmission attempts in
         * the TX_ARET mode.
         */
        tx_info->tx_count = 1;
        tx_status = TRUE;
    }
    else
    {
        // CHANNEL_ACCESS_FAILURE, NO_ACK, INVAILD
        tx_status = FALSE;
    }

    at86rf212_reg_read(RG_IRQ_STATUS);
    rf212_reg_write(dev_id, RG_IRQ_MASK, IRQ3_TRX_END);

    // Set the radio to RX_AACK_ON state.
    rf212_listen(dev_id);

    return tx_status;
}
#endif

ERROR_T rf212_tx(UINT8 dev_id, const UINT8 *buf)
{
    UINT8 intr;

#ifdef AT86RF212_DEBUG
    printf("%s()\n", __FUNCTION__);
#endif

    if (wpan_dev[dev_id].mac_type == MAC_TYPE_NANO_MAC)
    {
        rf212_pll_on(dev_id);
        rf212_write_frame_buf(dev_id, buf);
    }
    
    rf212_reg_write(dev_id, RG_IRQ_MASK, 0);

    rf212_clear_mcu_irq(dev_id);
    rf212_reg_read(dev_id, RG_IRQ_STATUS);

    // Just send a frame stored in the Frame Buffer.
    rf212_set_pin(dev_id, RF212_PIN_SLP_TR, TRUE);
    nos_delay_us(0);
    rf212_set_pin(dev_id, RF212_PIN_SLP_TR, FALSE);
    
    // Wait for TX done.
    while (rf212_subreg_read(dev_id, SR_TRX_STATUS) != TRX_STATUS_PLL_ON);
    
    intr = rf212_reg_read(dev_id, RG_IRQ_STATUS);
    rf212_reg_write(dev_id, RG_IRQ_MASK, IRQ3_TRX_END);

    if (wpan_dev[dev_id].mac_type == MAC_TYPE_NANO_MAC)
    {
        rf212_listen(dev_id);
    }
    
#ifdef AT86RF212_DEBUG
    printf("%s()-intr:0x%x, TRX:0x%x\n", __FUNCTION__, 
           intr, rf212_subreg_read(dev_id, SR_TRX_STATUS));
#endif

    return (intr & IRQ3_TRX_END) ? ERROR_SUCCESS : ERROR_FAIL;
}

BOOL rf212_rxbuf_is_empty(UINT8 dev_id)
{
    return (wpan_dev[dev_id].c.rf212.frame_received == FALSE);
}

void rf212_flush_rxbuf(UINT8 dev_id)
{
    struct wpan_rx_frame tmp;

    // To release the Frame Buffer from the Dynamic Frame Buffer Protection.
    rf212_read_frame_buf(dev_id, &tmp);
    wpan_dev[dev_id].c.rf212.frame_received = FALSE;
}

/**
 * @description called by modem HW IRQ.
 */
void rf212_interrupt_handler(UINT8 dev_id)
{
    UINT8 interrupts;
#ifdef AT86RF212_ON_CHIP_AUTO_TRX
    UINT8 trac_status;
#endif

#ifdef AT86RF212_ON_CHIP_AUTO_TRX
    // ignore TRAC_STATUS for RX. it's not working for this revision of HW
    do {
        trac_status = rf212_subreg_read(dev_id, SR_TRAC_STATUS);
    } while (trac_status == TRAC_INVALID); //Wait until AACK is "SUCCESS_WAIT_FOR_ACK"
#endif

    /*
     * Clearing the interrupt flag of the MCU side should be done before
     * clearing the flag of the RF212 side.
     */
    rf212_clear_mcu_irq(dev_id);
    interrupts = rf212_reg_read(dev_id, RG_IRQ_STATUS); //Read and Clear register flag

    while (interrupts)
    {
#ifdef AT86RF212_DEBUG
    printf("%s()-IRQ:0x%X\n", __FUNCTION__, interrupts);
#endif
    if (interrupts & IRQ3_TRX_END)
    {
        wpan_dev[dev_id].c.rf212.frame_received = TRUE;
        wpan_dev[dev_id].c.rf212.frame_ready_to_send = FALSE;

        if (interrupts & IRQ2_RX_START)
        {
            wpan_dev[dev_id].notify(dev_id, 0, NULL);
        }
    }

    if (!rf212_rxbuf_is_empty(dev_id))
    {
#ifdef AT86RF212_DEBUG
        printf("%s()-Still not empty (IRQ:0x%X)\n", __FUNCTION__, interrupts);
#endif

        rf212_flush_rxbuf(dev_id);
    }

    rf212_clear_mcu_irq(dev_id);
    interrupts = rf212_reg_read(dev_id, RG_IRQ_STATUS);
    }

#ifdef AT86RF212_DEBUG
    printf("%s()-out (IRQ:0x%X)\n", __FUNCTION__, interrupts);
#endif
}

static void rf212_send_ack(UINT8 dev_id, UINT8 seq)
{
    UINT8 ack_frame[4] = { 0x05, 0x02, 0x00, 0x00 };
    ack_frame[3] = seq;

    rf212_tx(dev_id, ack_frame);

#ifdef AT86RF212_DEBUG
    printf("%s()-seq:%u\n", __FUNCTION__, seq);
#endif
}

static ERROR_T rf212_read_frame(UINT8 dev_id, struct wpan_rx_frame *f)
{
    ERROR_T err;
    UINT16 fcf, pan, id;

#ifdef AT86RF212_DEBUG
    printf("%s()-begins\n", __FUNCTION__);
#endif

    err = rf212_read_frame_buf(dev_id, f);
    wpan_dev[dev_id].c.rf212.frame_received = FALSE;

    if (err != ERROR_SUCCESS)
    {
        return err;
    }

    fcf = f->buf[0] + (f->buf[1] << 8);
    pan = f->buf[3] + (f->buf[4] << 8);
    id = f->buf[5] + (f->buf[6] << 8);

    if (FRAME_REQUESTS_ACK(fcf) &&
        (pan == 0xFFFF || pan == wpan_dev[dev_id].pan_id) &&
        ((FRAME_CONTAINS_SHORTDST(fcf) && id == wpan_dev[dev_id].short_id) ||
         (f->len >= 14 &&
          FRAME_CONTAINS_LONGDST(fcf) &&
          f->buf[5] == wpan_dev[dev_id].eui64[7] &&
          f->buf[6] == wpan_dev[dev_id].eui64[6] &&
          f->buf[7] == wpan_dev[dev_id].eui64[5] &&
          f->buf[8] == wpan_dev[dev_id].eui64[4] &&
          f->buf[9] == wpan_dev[dev_id].eui64[3] &&
          f->buf[10] == wpan_dev[dev_id].eui64[2] &&
          f->buf[11] == wpan_dev[dev_id].eui64[1] &&
          f->buf[12] == wpan_dev[dev_id].eui64[0])))
    {
        rf212_send_ack(dev_id, f->buf[2]);
    }

    if (!f->crc_ok)
        return ERROR_CRC_ERROR;

    f->secured = FALSE;
    return ERROR_SUCCESS;
}

/**
 * @warn Before entering this function, the AT86RF212's state should be either
 * RX_ON (in the basic operating mode) or RX_AACK_ON (in the extended operating
 * mode).
 */
BOOL rf212_cca(UINT8 dev_id)
{
    BOOL channel_is_clear;

    // Turn off TX interrupt while CCA.
    rf212_reg_write(dev_id, RG_IRQ_MASK, 0);

#ifdef AT86RF212_DEBUG
    printf("%s()\n", __FUNCTION__);
#endif

#ifdef AT86RF212_ON_CHIP_AUTO_TRX
    rf212_pll_on(dev_id);
    rf212_subreg_write(dev_id, SR_TRX_CMD, CMD_RX_ON);
    while (rf212_subreg_read(dev_id, SR_TRX_STATUS) != TRX_STATUS_RX_ON);
#endif

    rf212_subreg_write(dev_id, SR_CCA_REQUEST, 1);

    while (rf212_subreg_read(dev_id, SR_CCA_DONE) != 1);
    channel_is_clear = rf212_subreg_read(dev_id, SR_CCA_STATUS);

#ifdef AT86RF212_ON_CHIP_AUTO_TRX
    rf212_pll_on(dev_id);
    rf212_listen(dev_id);
#endif

    // Clear interrupt flags.
    rf212_reg_read(dev_id, RG_IRQ_STATUS);
    rf212_reg_write(dev_id, RG_IRQ_MASK, IRQ3_TRX_END);

    return channel_is_clear;
}

ERROR_T rf212_wait_for_ack(UINT8 dev_id, UINT8 seq)
{
    UINT8 intr;
    ERROR_T err = ERROR_FAIL;
    struct wpan_rx_frame ack;

#ifdef AT86RF212_DEBUG
    printf("%s()\n", __FUNCTION__);
#endif

    rf212_reg_write(dev_id, RG_IRQ_MASK, 0);

    // Wait during turn around time.
    // BPSK: macAckWaitDuration = 120 symbol periods
    // O-QPSK: macAckWaitDuration = 54 symbol periods

    // An enough duration value is used for the software acknowledge.
    //nos_delay_us(54 * SYMBOL_DURATION);
    nos_delay_us(4000);

    intr = rf212_reg_read(dev_id, RG_IRQ_STATUS);
#ifdef AT86RF212_DEBUG
    printf("%s()-intr:0x%x\n", __FUNCTION__, intr);
#endif
    rf212_reg_write(dev_id, RG_IRQ_MASK, IRQ3_TRX_END);

    if (intr & IRQ3_TRX_END)
    {
        if (rf212_read_frame_buf(dev_id, &ack) == ERROR_SUCCESS)
        {
            if (FRAME_IS_ACK(ack.buf[0] + (ack.buf[1] << 8)) &&
                ack.buf[2] == seq)
            {
#ifdef AT86RF212_DEBUG
                printf("%s()-acked\n", __FUNCTION__);
#endif
                err = ERROR_SUCCESS;
            }
            else
            {
                err = ERROR_INVALID_FRAME;
            }
        }
    }

    return err;
}

static const struct wpan_dev_cmdset rf212_api =
{
    rf212_config,
    rf212_read_frame,
    rf212_write_frame,
    rf212_rxbuf_is_empty,
    rf212_flush_rxbuf,
    rf212_osc_off,
    rf212_osc_on,
    rf212_tx,
    rf212_wait_for_ack,
    rf212_cca,
    rf212_set_pan_id,
    rf212_set_short_address,
    NULL,
    
#ifdef GLOSSY_M
    rf212_peek_received_frame,
    rf212_pll_on,
    rf212_listen,
#endif
};

void rf212_init(UINT8 dev_id, enum rf212_clkm clkm)
{
    UINT8 st;

#ifdef AT86RF212_DEBUG
    printf("%s()\n", __FUNCTION__);
#endif
    
    rf212_clear_mcu_irq(dev_id);
    rf212_enable_mcu_irq(dev_id, TRUE);
    
    wpan_dev[dev_id].hw_autotx = FALSE;
    wpan_dev[dev_id].sec_support = FALSE;
    wpan_dev[dev_id].addrdec_support = FALSE;
    wpan_dev[dev_id].cmd = &rf212_api;
    wpan_dev[dev_id].c.rf212.frame_received = FALSE;

    /* HW reset of radio transeiver */
    rf212_set_pin(dev_id, RF212_PIN_RESETN, FALSE); // RESET state
    rf212_set_pin(dev_id, RF212_PIN_SLP_TR, FALSE);
    nos_delay_us(TIME_RESET);
    rf212_set_pin(dev_id, RF212_PIN_RESETN, TRUE); // TRX_OFF state
    
    /* Force transition to TRX_OFF */
    rf212_subreg_write(dev_id, SR_TRX_CMD, CMD_FORCE_TRX_OFF);
    nos_delay_us(TIME_P_ON_TO_TRX_OFF);
    do { 
        st = rf212_subreg_read(dev_id, SR_TRX_STATUS); 
    } while (st != TRX_STATUS_TRX_OFF);
    
    if (clkm == RF212_CLKM_16MHZ)
    {
        rf212_reg_write(dev_id, RG_TRX_CTRL_0,
                        PAD_IO_2mA |
                        PAD_IO_CLKM_4mA |
                        CLKM_16MHz);
    }
    else if (clkm == RF212_CLKM_8MHZ)
    {
        rf212_reg_write(dev_id, RG_TRX_CTRL_0,
                        PAD_IO_2mA |
                        PAD_IO_CLKM_4mA |
                        CLKM_8MHz);
    }
    else if (clkm == RF212_CLKM_4MHZ)
    {
        rf212_reg_write(dev_id, RG_TRX_CTRL_0,
                        PAD_IO_2mA |
                        PAD_IO_CLKM_4mA |
                        CLKM_4MHz);
    }
    else if (clkm == RF212_CLKM_2MHZ)
    {
        rf212_reg_write(dev_id, RG_TRX_CTRL_0,
                        PAD_IO_2mA |
                        PAD_IO_CLKM_4mA |
                        CLKM_2MHz);
    }
    else if (clkm == RF212_CLKM_1MHZ)
    {
        rf212_reg_write(dev_id, RG_TRX_CTRL_0,
                        PAD_IO_2mA |
                        PAD_IO_CLKM_4mA |
                        CLKM_1MHz);
    }
    else if (clkm == RF212_CLKM_NOCLK)
    {
        rf212_reg_write(dev_id, RG_TRX_CTRL_0,
                        PAD_IO_2mA |
                        PAD_IO_CLKM_4mA |
                        CLKM_NO_CLK);
    }

    rf212_reg_write(dev_id, RG_TRX_CTRL_1,
                    PA_EXT_DISABLED |
                    IRQ_2_EXT_DISABLED |
                    TX_AUTO_CRC_ON |
                    RX_BL_CTRL_DISABLED |
                    SPI_CMD_MODE_PHY_RSSI |
                    IRQ_MASK_MODE_STATUS_FIRST |
                    IRQ_POL_ACTIVE_HIGH);
    
    // Carrier sense "AND" energy above threshold
    rf212_subreg_write(dev_id, SR_CCA_MODE, 3);

    // Set default mode and channel. (250kbps, OQPSK-SIN and 5)
    rf212_set_mode(dev_id, RF212_OQPSK_250KBPS_SIN);
    rf212_set_channel(dev_id, 5);

    /* Set TX power. */
    rf212_set_tx_power(dev_id, 5);

    rf212_reg_write(dev_id, RG_CCA_THRES, 0x70 + 0x02);
    rf212_subreg_write(dev_id, SR_JCM_EN, 1);
}

#endif //AT86RF212_M
