// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * RF212 driver add-on to support Glossy
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 13.
 */

#include "rf212-glossy.h"

#if defined AT86RF212_M && defined GLOSSY_M

#include "rf212.h"
#include "rf212-mem-access.h"
#include "rf212-const.h"

BOOL rf212_peek_received_frame(UINT8 dev_id, 
                               UINT8 *seq, 
                               UINT16 *dst, 
                               UINT16 *src)
{
    BOOL rcvd_crc;
    UINT8 id_buf[2];

    rcvd_crc = (rf212_subreg_read(dev_id, SR_RX_CRC_VALID) == 0) ? FALSE : TRUE;
    if (rcvd_crc == FALSE)
    {
        return FALSE;
    }

    if (seq) rf212_sram_read(dev_id, 3, seq, 1);

    if (dst)
    {
        rf212_sram_read(dev_id, 6, id_buf, 2);
        *dst = id_buf[0] + (id_buf[1] << 8);
    }

    if (src)
    {
        rf212_sram_read(dev_id, 8, id_buf, 2);
        *src = id_buf[0] + (id_buf[1] << 8);
    }
    
    return TRUE;
}

#endif //AT86RF212_M && GLOSSY_M
