// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * AT86RF212 Memory Access
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 9.
 */

#include "rf212-mem-access.h"
#ifdef AT86RF212_M
#include "rf212.h"
#include "rf212-interface.h"
#include "wpan-dev.h"

static const INT8 rssi_base_val[] = { -100, -100, -98, -98, -97 };
extern WPAN_DEV wpan_dev[];

UINT8 rf212_reg_read(UINT8 dev_id, UINT8 reg)
{
    UINT8 ret_value = 0;

    reg = (0x80) | ((0x3f) & reg); // read access: "10"+reg_addr[5:0]]

    rf212_request_cs(dev_id);
    rf212_spi(dev_id, reg, NULL);
    rf212_spi(dev_id, 0, &ret_value);
    rf212_release_cs(dev_id);

    return ret_value;
}

//return aligned sub-value
UINT8 rf212_subreg_read(UINT8 dev_id, UINT8 reg, UINT8 mask, UINT8 position)
{
    UINT8 register_value;

    //Read current register value and mask out subregister.
    register_value = rf212_reg_read(dev_id, reg);
    register_value &= mask;
    register_value >>= position; //Align subregister value.

    return register_value;
}

UINT8 rf212_reg_write(UINT8 dev_id, UINT8 reg, UINT8 value)
{
    UINT8 phy_status;
    reg = (0xc0) | ((0x3f) & reg); // write access: "11"+reg_addr[5:0]

    rf212_request_cs(dev_id);
    rf212_spi(dev_id, reg, &phy_status);
    rf212_spi(dev_id, value, NULL);
    rf212_release_cs(dev_id);

    return phy_status;
}

// value is not aligned.
UINT8 rf212_subreg_write(UINT8 dev_id, UINT8 reg, UINT8 mask, UINT8 position, UINT8 value)
{
    //Read current register value and mask area outside the subregister.
    UINT8 register_value = rf212_reg_read(dev_id, reg);
    register_value &= ~mask;

    //Start preparing the new subregister value. shift in place and mask.
    value <<= position;
    value &= mask;

    value |= register_value; //Set the new subregister value.

    //Write the modified register value.
    return rf212_reg_write(dev_id, reg, value); //return PHY_STATUS
}

void rf212_sram_write(UINT8 dev_id, UINT8 addr, const UINT8 *buf, UINT8 size)
{
    UINT8 i;
    
    rf212_request_cs(dev_id);
    rf212_spi(dev_id, 0x40, NULL); //SRAM Write
    rf212_spi(dev_id, addr, NULL);
    for (i = 0; i < size; i++)
    {
        rf212_spi(dev_id, buf[i], NULL);
    }
    rf212_release_cs(dev_id);
}

void rf212_sram_read(UINT8 dev_id, UINT8 addr, UINT8 *buf, UINT8 size)
{
    UINT8 i;
    
    rf212_request_cs(dev_id);
    rf212_spi(dev_id, 0x00, NULL); //SRAM Read
    rf212_spi(dev_id, addr, NULL);
    for (i = 0; i < size; i++)
    {
        rf212_spi(dev_id, 0, &buf[i]);
    }
    rf212_release_cs(dev_id);
}

ERROR_T rf212_write_frame_buf(UINT8 dev_id, const UINT8 *buf)
{
    UINT8 i;

#ifdef AT86RF212_DEBUG
    printf("%s()-begins\n", __FUNCTION__);
#endif

    rf212_request_cs(dev_id);
    rf212_spi(dev_id, 0x60, NULL);
    rf212_spi(dev_id, buf[0], NULL);

    for (i = 0; i < buf[0] - 2; i++)
    {
        rf212_spi(dev_id, buf[i + 1], NULL);
    }

    // dummy data to prevent TRX_UR interrupt.
    rf212_spi(dev_id, 0, NULL);
    rf212_spi(dev_id, 0, NULL);
    rf212_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T rf212_read_frame_buf(UINT8 dev_id, struct wpan_rx_frame *f)
{
    UINT8 len, i, c;
    ERROR_T err = ERROR_SUCCESS;

    rf212_request_cs(dev_id);
    rf212_spi(dev_id, 0x20, NULL);
    rf212_spi(dev_id, 0, &len);

    if (len < IEEE_802_15_4_ACK_SIZE ||
        len > IEEE_802_15_4_FRAME_SIZE)
    {
        err = ERROR_FAIL;
        goto out;
    }

    for (i = 0; i < len; i++)
    {
        rf212_spi(dev_id, 0, &f->buf[i]);
    }

    f->len = len - 2;

    rf212_spi(dev_id, 0, &f->lqi);
    rf212_spi(dev_id, 0, (UINT8 *) &f->rssi);

    rf212_spi(dev_id, 0, &c);
    f->crc_ok = (_IS_SET(c, 7)) ? TRUE : FALSE;

out:
    rf212_release_cs(dev_id);

    if (wpan_dev[dev_id].c.rf212.mode == RF212_BPSK_20KBPS ||
        wpan_dev[dev_id].c.rf212.mode == RF212_BPSK_40KBPS)
    {
        f->rssi -= 100;
    }
    else if (wpan_dev[dev_id].c.rf212.mode == RF212_OQPSK_100KBPS ||
             wpan_dev[dev_id].c.rf212.mode == RF212_OQPSK_250KBPS_SIN)
    {
        f->rssi -= 98;
    }
    else if (wpan_dev[dev_id].c.rf212.mode == RF212_OQPSK_250KBPS_RC_0_8)
    {
        f->rssi -= 97;
    }

    return err;
}

#endif //AT86RF212_M
