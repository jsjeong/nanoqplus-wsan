// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Generic IEEE 802.15.4 device
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 12. 28.
 */

#include "wpan-dev.h"
#ifdef IEEE_802_15_4_M
#include "platform.h"
#include "critical_section.h"
#include <string.h>

WPAN_DEV wpan_dev[WPAN_DEV_CNT];

void wpan_dev_setup(UINT8 dev_id,
                    UINT16 pan,
                    UINT16 short_id,
                    const UINT8 *eui64,
                    void (*mac_event_notifier)(UINT8, UINT8, void *),
                    enum nos_mac_type mac_type)
{
    wpan_dev_set_pan_id(dev_id, pan);
    wpan_dev_set_short_id(dev_id, short_id);
    
    if (eui64)
    {
        wpan_dev[dev_id].eui64_assigned = TRUE;
        memcpy(wpan_dev[dev_id].eui64, eui64, 8);
        wpan_dev_set_eui64(dev_id, eui64);
    }
    else
    {
        wpan_dev[dev_id].eui64_assigned = FALSE;
    }

    wpan_dev[dev_id].locked = FALSE;
    wpan_dev[dev_id].rx_on = TRUE;
    wpan_dev[dev_id].notify = mac_event_notifier;
    wpan_dev[dev_id].mac_type = mac_type;

    if (wpan_dev[dev_id].cmd->setup)
        wpan_dev[dev_id].cmd->setup(dev_id);
}

ERROR_T wpan_dev_lock(UINT8 id)
{
    ERROR_T err;

    NOS_ENTER_CRITICAL_SECTION();
    if (wpan_dev[id].locked == TRUE)
    {
        err = ERROR_BUSY;
        goto error;
    }

    wpan_dev[id].locked = TRUE;
    err = ERROR_SUCCESS;
    
error:
    NOS_EXIT_CRITICAL_SECTION();
    return err;
}

ERROR_T wpan_dev_release(UINT8 id)
{
    ERROR_T err;

    NOS_ENTER_CRITICAL_SECTION();
    if (wpan_dev[id].locked == FALSE)
    {
        err = SUCCESS_NOTHING_HAPPENED;
        goto error;
    }

    wpan_dev[id].locked = FALSE;
    err = ERROR_SUCCESS;
    
error:
    NOS_EXIT_CRITICAL_SECTION();
    return err;
}

BOOL wpan_dev_cca(UINT8 dev_id)
{
    if (wpan_dev[dev_id].cmd->cca)
    {
        return wpan_dev[dev_id].cmd->cca(dev_id);
    }
    else
    {
        return TRUE;
    }
}

void wpan_dev_set_pan_id(UINT8 dev_id, UINT16 new_pan_id)
{
    wpan_dev[dev_id].pan_id = new_pan_id;
    if (wpan_dev[dev_id].cmd->set_pan_id)
        wpan_dev[dev_id].cmd->set_pan_id(dev_id);
}

void wpan_dev_set_short_id(UINT8 dev_id, UINT16 new_short_id)
{
    wpan_dev[dev_id].short_id = new_short_id;
    if (wpan_dev[dev_id].cmd->set_short_id)
        wpan_dev[dev_id].cmd->set_short_id(dev_id);
}

void wpan_dev_set_eui64(UINT8 dev_id, const UINT8 *new_eui64)
{
    memcpy(wpan_dev[dev_id].eui64, new_eui64, 8);
    if (wpan_dev[dev_id].cmd->set_eui64)
        wpan_dev[dev_id].cmd->set_eui64(dev_id);
}

#endif //IEEE_802_15_4_M
