// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2520 instructions
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 15.
 */

#ifndef CC2520_INS_H
#define CC2520_INS_H

#include "kconf.h"
#ifdef CC2520_M
#include "nos_common.h"

// Instruction prototypes
UINT8  CC2520_SNOP(UINT8 dev_id);
UINT8  CC2520_SIBUFEX(UINT8 dev_id);
UINT8  CC2520_SSAMPLECCA(UINT8 dev_id);
UINT8  CC2520_SXOSCON(UINT8 dev_id);
UINT8  CC2520_STXCAL(UINT8 dev_id);
UINT8  CC2520_SRXON(UINT8 dev_id);
UINT8  CC2520_STXON(UINT8 dev_id);
UINT8  CC2520_STXONCCA(UINT8 dev_id);
UINT8  CC2520_SRFOFF(UINT8 dev_id);
UINT8  CC2520_SXOSCOFF(UINT8 dev_id);
UINT8  CC2520_SFLUSHRX(UINT8 dev_id);
UINT8  CC2520_SFLUSHTX(UINT8 dev_id);
UINT8  CC2520_SACK(UINT8 dev_id);
UINT8  CC2520_SACKPEND(UINT8 dev_id);
UINT8  CC2520_SNACK(UINT8 dev_id);
UINT8  CC2520_SRXMASKBITSET(UINT8 dev_id);
UINT8  CC2520_SRXMASKBITCLR(UINT8 dev_id);
UINT8  CC2520_IBUFLD(UINT8 dev_id, UINT8 i);
UINT8  CC2520_SRES(UINT8 dev_id);
UINT8  CC2520_MEMRD(UINT8 dev_id, UINT16 addr, UINT16 count, UINT8  *pData);
UINT8  CC2520_MEMRD8(UINT8 dev_id, UINT16 addr);
UINT16 CC2520_MEMRD16(UINT8 dev_id, UINT16 addr);
UINT32 CC2520_MEMRD24(UINT8 dev_id, UINT16 addr);
UINT8  CC2520_MEMWR(UINT8 dev_id, UINT16 addr, UINT16 count, UINT8  *pData);
UINT8  CC2520_MEMWR8(UINT8 dev_id, UINT16 addr, UINT8 value);
UINT8  CC2520_MEMWR16(UINT8 dev_id, UINT16 addr, UINT16 value);
UINT8  CC2520_MEMWR24(UINT8 dev_id, UINT16 addr, UINT32 value);
UINT8  CC2520_RXBUFCP_BEGIN(UINT8 dev_id, UINT16 addr, UINT8 *pCurrCount);
UINT8  CC2520_RXBUFCP_END(UINT8 dev_id, UINT16 addr, UINT8 count, UINT8  *pData);
UINT8  CC2520_RXBUF(UINT8 dev_id, UINT8 count, UINT8  *pData);
UINT8  CC2520_RXBUF8(UINT8 dev_id);
UINT16 CC2520_RXBUF16(UINT8 dev_id);
UINT8  CC2520_RXBUFMOV(UINT8 dev_id, UINT8 pri, UINT16 addr, UINT8 count, UINT8 *pCurrCount);
UINT8  CC2520_TXBUF(UINT8 dev_id, UINT8 count, UINT8  *pData);
void   CC2520_TXBUF8(UINT8 dev_id, UINT8 data);
void   CC2520_TXBUF16(UINT8 dev_id, UINT16 data);
UINT8  CC2520_TXBUFCP(UINT8 dev_id, UINT8 pri, UINT16 addr, UINT8 count, UINT8 *pCurrCount);
UINT8  CC2520_RANDOM(UINT8 dev_id, UINT8 count, UINT8  *pData);
UINT8  CC2520_RANDOM8(UINT8 dev_id);
UINT16 CC2520_RANDOM16(UINT8 dev_id);
UINT8  CC2520_RXMASKOR(UINT8 dev_id, UINT16 orMask);
UINT8  CC2520_RXMASKAND(UINT8 dev_id, UINT16 andMask);
UINT8  CC2520_MEMCP(UINT8 dev_id, UINT8 pri, UINT16 count, UINT16 src, UINT16 dest);
UINT8  CC2520_MEMCPR(UINT8 dev_id, UINT8 pri, UINT16 count, UINT16 src, UINT16 dest);
UINT8  CC2520_MEMXCP(UINT8 dev_id, UINT8 pri, UINT16 count, UINT16 src, UINT16 dest);
UINT8  CC2520_MEMXWR(UINT8 dev_id, UINT16 addr, UINT16 count, UINT8  *pData);
UINT8  CC2520_BSET(UINT8 dev_id, UINT8 bitAddr);
UINT8  CC2520_BCLR(UINT8 dev_id, UINT8 bitAddr);
UINT8  CC2520_CTR(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT8 n, UINT16 src, UINT16 dest);
UINT8  CC2520_UCTR(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT8 n, UINT16 src, UINT16 dest);
UINT8  CC2520_CBCMAC(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 src, UINT16 dest, UINT8 m);
UINT8  CC2520_UCBCMAC(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 src, UINT8 m);
UINT8  CC2520_CCM(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT8 n, UINT16 src, \
    UINT16 dest, UINT8 f, UINT8 m);
UINT8  CC2520_UCCM(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT8 n, UINT16 src, \
    UINT16 dest, UINT8 f, UINT8 m);
UINT8  CC2520_ECB(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 src, UINT16 dest);
UINT8  CC2520_ECBO(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 addr);
UINT8  CC2520_ECBX(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 src, UINT16 dest);
UINT8  CC2520_ECBXO(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 addr);
UINT8  CC2520_INC(UINT8 dev_id, UINT8 pri, UINT8 c, UINT16 addr);
UINT8  CC2520_ABORT(UINT8 dev_id, UINT8 c);
UINT8  CC2520_REGRD(UINT8 dev_id, UINT8 addr, UINT8 count, UINT8  *pValues);
UINT8  CC2520_REGRD8(UINT8 dev_id, UINT8 addr);
UINT16 CC2520_REGRD16(UINT8 dev_id, UINT8 addr);
UINT32 CC2520_REGRD24(UINT8 dev_id, UINT8 addr);
UINT8  CC2520_REGWR(UINT8 dev_id, UINT8 addr, UINT8 count, UINT8  *pValues);
void   CC2520_REGWR8(UINT8 dev_id, UINT8 addr, UINT8 value);
void   CC2520_REGWR16(UINT8 dev_id, UINT8 addr, UINT16 value);
void   CC2520_REGWR24(UINT8 dev_id, UINT8 addr, UINT32 value);
UINT8  CC2520_INS_STROBE(UINT8 dev_id, UINT8 strobe);

#endif //CC2520_M
#endif //CC2520_INS_H
