// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2520 instructions
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 15.
 */

#include "cc2520-ins.h"

#ifdef CC2520_M
#include "cc2520-ins.h"
#include "cc2520-interface.h"
#include "cc2520-const.h"

#define UPPER_WORD(a) ((UINT16) (((UINT32)(a)) >> 16))
#define HIWORD(a)     UPPER_WORD(a)

#define LOWER_WORD(a) ((UINT16) ((UINT32)(a)))
#define LOWORD(a)     LOWER_WORD(a)

#define UPPER_BYTE(a) ((UINT8) (((UINT16)(a)) >> 8))
#define HIBYTE(a)     UPPER_BYTE(a)

#define LOWER_BYTE(a) ((UINT8) ( (UINT16)(a))      )
#define LOBYTE(a)     LOWER_BYTE(a)

#define BLOCK_SIZE  255

/***********************************************************************************
 * @fn      CC2520_INS_MEMCP_COMMON
 *
 * @brief   memory copy
 *
 * @param   UINT8 instr -
 *          UINT8 pri -
 *          UINT16 count -
 *          UINT16 src -
 *          UINT16 dest -
 *
 * @return  UINT8 - status byte
 */
static UINT8 CC2520_INS_MEMCP_COMMON(UINT8 dev_id, UINT8 instr, UINT8 pri, UINT16 count,
                                     UINT16 src, UINT16 dest)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, instr | pri);
    cc2520_spi(dev_id, count);
    cc2520_spi(dev_id, (HIBYTE(src) << 4) | HIBYTE(dest));
    cc2520_spi(dev_id, LOBYTE(src));
    cc2520_spi(dev_id, LOBYTE(dest));
    cc2520_release_cs(dev_id);
    return s;
}

/***********************************************************************************
 * @fn      CC2520_INS_STROBE
 *
 * @brief   send strobe commane
 *
 * @param   UINT8 strobe - strobe command
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_INS_STROBE(UINT8 dev_id, UINT8 strobe)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, strobe);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_SNOP
 *
 * @brief   SNOP instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SNOP(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SNOP);
}


/***********************************************************************************
 * @fn      CC2520_SIBUFEX
 *
 * @brief   SIBUFEX instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SIBUFEX(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SIBUFEX);
}


/***********************************************************************************
 * @fn      CC2520_SSAMPLECCA
 *
 * @brief   Sample CCA instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SSAMPLECCA(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SSAMPLECCA);
}


/***********************************************************************************
 * @fn      CC2520_SXOSCON
 *
 * @brief   SXOSCON instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SXOSCON(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SXOSCON);
}


/***********************************************************************************
 * @fn      CC2520_STXCAL
 *
 * @brief   STXCAL instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_STXCAL(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_STXCAL);
}


/***********************************************************************************
 * @fn      CC2520_SRXON
 *
 * @brief   SRXON instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SRXON(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SRXON);
}


/***********************************************************************************
 * @fn      CC2520_STXON
 *
 * @brief   STXON instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_STXON(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_STXON);
}


/***********************************************************************************
 * @fn      CC2520_STXONCCA
 *
 * @brief   STXONCCA instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_STXONCCA(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_STXONCCA);
}


/***********************************************************************************
 * @fn      CC2520_SRFOFF
 *
 * @brief   SRFOFF instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SRFOFF(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SRFOFF);
}


/***********************************************************************************
 * @fn      CC2520_SXOSCOFF
 *
 * @brief   SXOSCOFF instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SXOSCOFF(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SXOSCOFF);
}


/***********************************************************************************
 * @fn      CC2520_SFLUSHRX
 *
 * @brief   SFLUSHRX instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SFLUSHRX(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SFLUSHRX);
}


/***********************************************************************************
 * @fn      CC2520_SFLUSHTX
 *
 * @brief   SFLUSHTX instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SFLUSHTX(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SFLUSHTX);
}


/***********************************************************************************
 * @fn      CC2520_SACK
 *
 * @brief   SACK instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SACK(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SACK);
}


/***********************************************************************************
 * @fn      CC2520_SACKPEND
 *
 * @brief   SACKPEND instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SACKPEND(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SACKPEND);
}


/***********************************************************************************
 * @fn      CC2520_SNACK
 *
 * @brief   SNACK instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SNACK(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SNACK);
}


/***********************************************************************************
 * @fn      CC2520_SRXMASKBITSET
 *
 * @brief   SRXMASKBITSET instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SRXMASKBITSET(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SRXMASKBITSET);
}


/***********************************************************************************
 * @fn      CC2520_SRXMASKBITCLR
 *
 * @brief   SRXMASKBITCLR instruction strobe
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SRXMASKBITCLR(UINT8 dev_id)
{
    return CC2520_INS_STROBE(dev_id, CC2520_INS_SRXMASKBITCLR);
}


/***********************************************************************************
 * @fn      CC2520_IBUFLD
 *
 * @brief   IBUFLD - load instruction to instruction buffer
 *
 * @param   UINT8 i -
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_IBUFLD(UINT8 dev_id, UINT8 i)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_IBUFLD);
    cc2520_spi(dev_id, i);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_SRES
 *
 * @brief   SRES - Reset device except SPI interface
 *
 * @param   none
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_SRES(UINT8 dev_id)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_SRES);
    cc2520_spi(dev_id, 0x00);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_MEMRD
 *
 * @brief   Read memory
 *
 * @param   UINT16 addr
 *          UINT16 count
 *          UINT8  *pData
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_MEMRD(UINT8 dev_id, UINT16 addr, UINT16 count, UINT8  *pData)
{
    UINT8 s, i;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_MEMRD | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));

    for (i = 0; i < count; i++)
    {
        pData[i] = cc2520_spi(dev_id, 0);
    }
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_MEMRD8
 *
 * @brief   Read memory 8 bits
 *
 * @param   UINT16 addr
 *
 * @return  UINT8 - result
 */
UINT8 CC2520_MEMRD8(UINT8 dev_id, UINT16 addr)
{
    UINT8 value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_MEMRD | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    value = cc2520_spi(dev_id, 0x00);
    cc2520_release_cs(dev_id);
    return value;
}


/***********************************************************************************
 * @fn      CC2520_MEMRD16
 *
 * @brief   Read memory 16 bits
 *
 * @param   UINT16 addr
 *
 * @return  UINT16 - result
 */
UINT16 CC2520_MEMRD16(UINT8 dev_id, UINT16 addr)
{
    EWORD value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_MEMRD | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    value.b.b0 = cc2520_spi(dev_id, 0x00);
    value.b.b1 = cc2520_spi(dev_id, 0x00);
    cc2520_release_cs(dev_id);
    return value.w;
}


/***********************************************************************************
 * @fn      CC2520_MEMRD24
 *
 * @brief   Read memory 24 bits
 *
 * @param   UINT16 addr
 *
 * @return  UINT32 - result
 */
UINT32 CC2520_MEMRD24(UINT8 dev_id, UINT16 addr)
{
    EDWORD value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_MEMRD | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    value.b.b0 = cc2520_spi(dev_id, 0x00);
    value.b.b1 = cc2520_spi(dev_id, 0x00);
    value.b.b2 = cc2520_spi(dev_id, 0x00);
    value.b.b3 = 0x00;
    cc2520_release_cs(dev_id);
    return value.dw;
}


/***********************************************************************************
 * @fn      CC2520_MEMWR
 *
 * @brief   Write memory
 *
 * @param   UINT16 addr
 *          UINT16 count
 *          UINT8  *pData
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_MEMWR(UINT8 dev_id, UINT16 addr, UINT16 count, UINT8  *pData)
{
    UINT8 s, i;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_MEMWR | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    for (i = 0; i < count; i++)
    {
        cc2520_spi(dev_id, pData[i]);
    }
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_MEMWR8
 *
 * @brief   Write memory 8 bits
 *
 * @param   UINT16 addr
 *          UINT8 value
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_MEMWR8(UINT8 dev_id, UINT16 addr, UINT8 value)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_MEMWR | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    cc2520_spi(dev_id, value);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_MEMWR16
 *
 * @brief   Write memory 16 bits
 *
 * @param   UINT16 addr
 *          UINT16 value
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_MEMWR16(UINT8 dev_id, UINT16 addr, UINT16 value)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_MEMWR | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(value));
    cc2520_spi(dev_id, HIBYTE(value));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_MEMWR24
 *
 * @brief   Write memory 24 bits
 *
 * @param   UINT16 addr
 *          UINT32 value
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_MEMWR24(UINT8 dev_id, UINT16 addr, UINT32 value)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_MEMWR | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(LOWORD(value)));
    cc2520_spi(dev_id, HIBYTE(LOWORD(value)));
    cc2520_spi(dev_id, LOBYTE(HIWORD(value)));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_RXBUF
 *
 * @brief   Read bytes from RX buffer
 *
 * @param   UINT8 count
 *          UINT8  *pData
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_RXBUF(UINT8 dev_id, UINT8 count, UINT8  *pData)
{
    UINT8 s, i;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_RXBUF);
    
    for (i = 0; i < count; i++)
    {
        pData[i] = cc2520_spi(dev_id, 0);
    }

    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_RXBUF8
 *
 * @brief   Read 8 bits from RX buffer
 *
 * @param   none
 *
 * @return  UINT8 - result
 */
UINT8 CC2520_RXBUF8(UINT8 dev_id)
{
    UINT8 value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_RXBUF);
    value = cc2520_spi(dev_id, 0x00);
    cc2520_release_cs(dev_id);
    return value;
}


/***********************************************************************************
 * @fn      CC2520_RXBUF16
 *
 * @brief   Read 16 bits from RX buffer
 *
 * @param   none
 *
 * @return  UINT16 - result
 */
UINT16 CC2520_RXBUF16(UINT8 dev_id)
{
    EWORD value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_RXBUF);
    value.b.b0 = cc2520_spi(dev_id, 0x00);
    value.b.b1 = cc2520_spi(dev_id, 0x00);
    cc2520_release_cs(dev_id);
    return value.w;
}


/***********************************************************************************
 * @fn      CC2520_RXBUFCP_BEGIN
 *
 * @brief   Copy RX buf to memory. Call this routine before CC2520_RXBUFCP_END
 *
 * @param   UINT16 addr - copy destination
 *          UINT8 *pCurrCount - number of bytes in RX buf
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_RXBUFCP_BEGIN(UINT8 dev_id, UINT16 addr, UINT8 *pCurrCount)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_RXBUFCP);
    if (pCurrCount) {
        *pCurrCount = cc2520_spi(dev_id, HIBYTE(addr));
    } else {
        cc2520_spi(dev_id, HIBYTE(addr));
    }
    return s;
}


/***********************************************************************************
 * @fn      CC2520_RXBUFCP_END
 *
 * @brief   Copy RX buf to memory and read into buffer.
 *          Call this routine after CC2520_RXBUFCP_BEGIN.
 *
 * @param   UINT16 addr - copy destination
 *          UINT8 count - number of bytes
 *          UINT8  *pData - data buffer
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_RXBUFCP_END(UINT8 dev_id, UINT16 addr, UINT8 count, UINT8  *pData)
{
    UINT8 s, i;
    s = cc2520_spi(dev_id, LOBYTE(addr));
    for (i = 0; i < count; i++)
    {
        pData[i] = cc2520_spi(dev_id, 0);
    }
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_RXBUFMOV
 *
 * @brief   RXBUFMOV instruction - moves oldest bytes from RX buffer to the memory
 *          location addr.
 *
 * @param   UINT8 pri - instruction priority
 *          UINT16 addr - memory location
 *          UINT8 count - number of bytes
 *          UINT8 *pCurrCount
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_RXBUFMOV(UINT8 dev_id, UINT8 pri, UINT16 addr, UINT8 count, UINT8 *pCurrCount)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_RXBUFMOV | pri);
    if (pCurrCount) {
        *pCurrCount = cc2520_spi(dev_id, count);
    } else {
        cc2520_spi(dev_id, count);
    }
    cc2520_spi(dev_id, HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_TXBUF
 *
 * @brief   Write data to TX buffer
 *
 * @param   UINT8 count - number of bytes
 *          UINT8  *pData - pointer to data buffer
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_TXBUF(UINT8 dev_id, UINT8 count, UINT8  *pData)
{
    UINT8 s, i;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_TXBUF);
    for (i = 0; i < count; i++)
    {
        cc2520_spi(dev_id, pData[i]);
    }
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_TXBUF8
 *
 * @brief   Write 8 bits to TX buffer
 *
 * @param   UINT8 data
 *
 * @return  none
 */
void CC2520_TXBUF8(UINT8 dev_id, UINT8 data)
{
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_TXBUF);
    cc2520_spi(dev_id, data);
    cc2520_release_cs(dev_id);
}


/***********************************************************************************
 * @fn      CC2520_TXBUF16
 *
 * @brief   Write 16 bits to TX buffer
 *
 * @param   UINT16 data
 *
 * @return  none
 */
void CC2520_TXBUF16(UINT8 dev_id, UINT16 data)
{
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_TXBUF);
    cc2520_spi(dev_id, LOBYTE(data));
    cc2520_spi(dev_id, HIBYTE(data));
    cc2520_release_cs(dev_id);
}


/***********************************************************************************
 * @fn      CC2520_TXBUFCP
 *
 * @brief   Copy data from memory location addr to TX buf
 *
 * @param   UINT8 pri - priority
 *          UINT16 addr - memory location
 *          UINT8 count - number of bytes
 *          UINT8 *pCurrCount
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_TXBUFCP(UINT8 dev_id, UINT8 pri, UINT16 addr, UINT8 count, UINT8 *pCurrCount)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_TXBUFCP | pri);
    if (pCurrCount) {
        *pCurrCount = cc2520_spi(dev_id, count);
    } else {
        cc2520_spi(dev_id, count);
    }
    cc2520_spi(dev_id, HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_RANDOM
 *
 * @brief   Random generated bytes
 *
 * @param   UINT8 count
 *          UINT8  *pData
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_RANDOM(UINT8 dev_id, UINT8 count, UINT8  *pData)
{
    UINT8 s, i;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_RANDOM);
    cc2520_spi(dev_id, 0x00);
    for (i = 0; i < count; i++)
    {
        pData[i] = cc2520_spi(dev_id, 0);
    }
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_RANDOM8
 *
 * @brief   Random generated byte
 *
 * @param  none
 *
 * @return  UINT8
 */
UINT8 CC2520_RANDOM8(UINT8 dev_id)
{
    UINT8 value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_RANDOM);
    cc2520_spi(dev_id, 0x00);
    value = cc2520_spi(dev_id, 0x00);
    cc2520_release_cs(dev_id);
    return value;
}


/***********************************************************************************
 * @fn      CC2520_RANDOM16
 *
 * @brief   Random generated bytes
 *
 * @param  none
 *
 * @return  UINT16
 */
UINT16 CC2520_RANDOM16(UINT8 dev_id)
{
    EWORD value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_RANDOM);
    cc2520_spi(dev_id, 0x00);
    value.b.b0 = cc2520_spi(dev_id, 0x00);
    value.b.b1 = cc2520_spi(dev_id, 0x00);
    cc2520_release_cs(dev_id);
    return value.w;
}


/***********************************************************************************
 * @fn      CC2520_RXMASKOR
 *
 * @brief   RXMASKOR instruction - bitwise OR between RX enable mask and orMask
 *
 * @param  UINT16 orMask -
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_RXMASKOR(UINT8 dev_id, UINT16 orMask)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_RXMASKOR);
    cc2520_spi(dev_id, HIBYTE(orMask));
    cc2520_spi(dev_id, LOBYTE(orMask));
    cc2520_release_cs(dev_id);
    return s;
}

/***********************************************************************************
 * @fn      CC2520_RXMASKAND
 *
 * @brief   RXMASKAND - bitwise AND between RX enable mask and andMask
 *
 * @param  UINT16 andMask
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_RXMASKAND(UINT8 dev_id, UINT16 andMask)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_RXMASKAND);
    cc2520_spi(dev_id, HIBYTE(andMask));
    cc2520_spi(dev_id, LOBYTE(andMask));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_MEMCP
 *
 * @brief   MEMCP - Copy data between memory blocks
 *
 * @param  UINT8 pri - priority
 *         UINT16 count - number of bytes
 *         UINT16 src - source address
 *         UINT16 dest - destination address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_MEMCP(UINT8 dev_id, UINT8 pri, UINT16 count, UINT16 src, UINT16 dest)
{
    return CC2520_INS_MEMCP_COMMON(dev_id, CC2520_INS_MEMCP, pri, count, src, dest);
}


/***********************************************************************************
 * @fn      CC2520_MEMCPR
 *
 * @brief   MEMCPR - Copy data between memory blocks and revert endianess
 *
 * @param  UINT8 pri - priority
 *         UINT16 count - number of bytes
 *         UINT16 src - source address
 *         UINT16 dest - destination address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_MEMCPR(UINT8 dev_id, UINT8 pri, UINT16 count, UINT16 src, UINT16 dest)
{
    return CC2520_INS_MEMCP_COMMON(dev_id, CC2520_INS_MEMCPR, pri, count, src, dest);
}


/***********************************************************************************
 * @fn      CC2520_MEMXCP
 *
 * @brief   MEMXCP - XOR one memory block with another memory block
 *
 * @param  UINT8 pri - priority
 *         UINT16 count - number of bytes
 *         UINT16 src - source address
 *         UINT16 dest - destination address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_MEMXCP(UINT8 dev_id, UINT8 pri, UINT16 count, UINT16 src, UINT16 dest)
{
    return CC2520_INS_MEMCP_COMMON(dev_id, CC2520_INS_MEMXCP, pri, count, src, dest);
}


/***********************************************************************************
 * @fn      CC2520_MEMXWR
 *
 * @brief   MEMXWR - XOR memory
 *
 * @param  UINT16 addr - memory address
 *         UINT16 count - number of bytes
 *         UINT8  *pData - unaltered data
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_MEMXWR(UINT8 dev_id, UINT16 addr, UINT16 count, UINT8  *pData)
{
    UINT8 s, i;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_MEMXWR);
    cc2520_spi(dev_id, HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    for (i = 0; i < count; i++)
    {
        cc2520_spi(dev_id, pData[i]);
    }
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_BSET
 *
 * @brief   BSET - set a single bit
 *
 * @param  UINT8 bitAddr - address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_BSET(UINT8 dev_id, UINT8 bitAddr)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_BSET);
    cc2520_spi(dev_id, bitAddr);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_BCLR
 *
 * @brief   BCLR - Clear a single bit
 *
 * @param  UINT8 bitAddr - address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_BCLR(UINT8 dev_id, UINT8 bitAddr)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_BCLR);
    cc2520_spi(dev_id, bitAddr);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_CTR
 *
 * @brief   CTR - Counter mode encryption
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - number of plaintext bytes
 *         UINT8 n - address of nonce
 *         UINT16 src - starting address of plaintext
 *         UINT16 dest - destination address, if dest=0 destination address is set
 *                       equal to src address.
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_CTR(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT8 n, UINT16 src, UINT16 dest)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_CTR | pri);
    cc2520_spi(dev_id, k);
    cc2520_spi(dev_id, c);
    cc2520_spi(dev_id, n);
    cc2520_spi(dev_id, (HIBYTE(src) << 4) | HIBYTE(dest));
    cc2520_spi(dev_id, LOBYTE(src));
    cc2520_spi(dev_id, LOBYTE(dest));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_UCTR
 *
 * @brief   UCTR - Counter mode decryption.
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - number of plaintext bytes
 *         UINT8 n - address of nonce
 *         UINT16 src - starting address of ciphertext
 *         UINT16 dest - destination address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_UCTR(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT8 n, UINT16 src, UINT16 dest)
{
    return CC2520_CTR(dev_id, pri, k, c, n, src, dest);
}


/***********************************************************************************
 * @fn     CC2520_CBCMAC
 *
 * @brief  CBCMAC - authentication using CBC-MAC
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - number of plaintext bytes
 *         UINT16 src - starting address of plaintext
 *         UINT16 dest - destination address
 *         UINT8 m - sets length of integrity code (m=1,2,3 gives lenght of integrity
 *                   field 4,8,16)
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_CBCMAC(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 src, UINT16 dest, UINT8 m)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_CBCMAC | pri);
    cc2520_spi(dev_id, k);
    cc2520_spi(dev_id, c);
    cc2520_spi(dev_id, (HIBYTE(src) << 4) | HIBYTE(dest));
    cc2520_spi(dev_id, LOBYTE(src));
    cc2520_spi(dev_id, LOBYTE(dest));
    cc2520_spi(dev_id, m);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn     CC2520_UCBCMAC
 *
 * @brief  UCBCMAC - reverse authentication using CBC-MAC
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - number of plaintext bytes
 *         UINT16 src - starting address of plaintext
 *         UINT8 m - sets length of integrity code (m=1,2,3 gives lenght of integrity
 *                   field 4,8,16)
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_UCBCMAC(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 src, UINT8 m)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_UCBCMAC | pri);
    cc2520_spi(dev_id, k);
    cc2520_spi(dev_id, c);
    cc2520_spi(dev_id, HIBYTE(src));
    cc2520_spi(dev_id, LOBYTE(src));
    cc2520_spi(dev_id, m);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_CCM
 *
 * @brief   CCM - encryption and authentication using CCM
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - number of plaintext bytes to authenticate and encrypt
 *         UINT8 n - address of nonce
 *         UINT16 src - starting address of plaintext
 *         UINT16 dest - destination address
 *         UINT8 f - number of plaintext bytes to authenticate
 *         UINT8 m - sets length of integrity code (m=1,2,3 gives lenght of integrity
 *                   field 4,8,16)
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_CCM(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT8 n, UINT16 src, \
                 UINT16 dest, UINT8 f, UINT8 m)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_CCM | pri);
    cc2520_spi(dev_id, k);
    cc2520_spi(dev_id, c);
    cc2520_spi(dev_id, n);
    cc2520_spi(dev_id, (HIBYTE(src) << 4) | HIBYTE(dest));
    cc2520_spi(dev_id, LOBYTE(src));
    cc2520_spi(dev_id, LOBYTE(dest));
    cc2520_spi(dev_id, f);
    cc2520_spi(dev_id, m);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_UCCM
 *
 * @brief   UCCM - decryption and reverse authentication using CCM
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - number of bytes to decrypt and authenticate
 *         UINT8 n - address of nonce
 *         UINT16 src - starting address
 *         UINT16 dest - destination address
 *         UINT8 f - number of bytes to authenticate
 *         UINT8 m - ets length of integrity code (m=1,2,3 gives lenght of integrity
 *                   field 4,8,16)
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_UCCM(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT8 n, UINT16 src, \
                  UINT16 dest, UINT8 f, UINT8 m)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_UCCM | pri);
    cc2520_spi(dev_id, k);
    cc2520_spi(dev_id, c);
    cc2520_spi(dev_id, n);
    cc2520_spi(dev_id, (HIBYTE(src) << 4) | HIBYTE(dest));
    cc2520_spi(dev_id, LOBYTE(src));
    cc2520_spi(dev_id, LOBYTE(dest));
    cc2520_spi(dev_id, f);
    cc2520_spi(dev_id, m);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_ECB
 *
 * @brief   ECB encryption
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - encrypts 16-C bytes of data in a 16 byte block
 *         UINT16 src - source address
 *         UINT16 dest - destination address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_ECB(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 src, UINT16 dest)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_ECB | pri);
    cc2520_spi(dev_id, k);
    cc2520_spi(dev_id, (c << 4) | HIBYTE(src));
    cc2520_spi(dev_id, LOBYTE(src));
    cc2520_spi(dev_id, HIBYTE(dest));
    cc2520_spi(dev_id, LOBYTE(dest));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_ECBO
 *
 * @brief   ECB encryption overwriting plaintext
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - encrypts 16-C bytes of data in a 16 byte block
 *         UINT16 addr - source and destination address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_ECBO(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 addr)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_ECBO | pri);
    cc2520_spi(dev_id, k);
    cc2520_spi(dev_id, (c << 4) | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_ECBX
 *
 * @brief   ECB encryption and XOR
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - encrypts 16-C bytes of data in a 16 byte block
 *         UINT16 src - source address
 *         UINT16 dest - destination address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_ECBX(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 src, UINT16 dest)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_ECBX | pri);
    cc2520_spi(dev_id, k);
    cc2520_spi(dev_id, (c << 4) | HIBYTE(src));
    cc2520_spi(dev_id, LOBYTE(src));
    cc2520_spi(dev_id, HIBYTE(dest));
    cc2520_spi(dev_id, LOBYTE(dest));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_ECBXO
 *
 * @brief   ECBO and XOR
 *
 * @param  UINT8 pri - priority
 *         UINT8 k - address of key
 *         UINT8 c - encrypts 16-C bytes of data in a 16 byte block
 *         UINT16 addr - source and destination address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_ECBXO(UINT8 dev_id, UINT8 pri, UINT8 k, UINT8 c, UINT16 addr)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_ECBXO | pri);
    cc2520_spi(dev_id, k);
    cc2520_spi(dev_id, (c << 4) | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_INC
 *
 * @brief   INC - increment instruction. Increments 2^c byte word with least
 *                significant byte at address.
 *
 * @param  UINT8 pri - priority
 *         UINT8 c - increments 2^c byte word
 *         UINT16 addr - address
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_INC(UINT8 dev_id, UINT8 pri, UINT8 c, UINT16 addr)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_INC | pri);
    cc2520_spi(dev_id, (c << 4) | HIBYTE(addr));
    cc2520_spi(dev_id, LOBYTE(addr));
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_ABORT
 *
 * @brief   Abort ongoing data management or security instruction
 *
 * @param  UINT8 c - abort mode (see datasheet)
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_ABORT(UINT8 dev_id, UINT8 c)
{
    UINT8 s;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_ABORT);
    cc2520_spi(dev_id, c);
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_REGRD
 *
 * @brief   Register read. Can only be started from addresses below 0x40
 *
 * @param  UINT8 addr - address
 *         UINT8 count - number of bytes
 *         UINT8  *pValues - buffer to store result
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_REGRD(UINT8 dev_id, UINT8 addr, UINT8 count, UINT8  *pValues)
{
    UINT8 s, i;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_REGRD | addr);
    for (i = 0; i < count; i++)
    {
        pValues[i] = cc2520_spi(dev_id, 0);
    }
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_REGRD8
 *
 * @brief   Read one register byte
 *
 * @param  UINT8 addr - address
 *
 * @return  UINT8 - result
 */
UINT8 CC2520_REGRD8(UINT8 dev_id, UINT8 addr)
{
    UINT8 value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_REGRD | addr);
    value = cc2520_spi(dev_id, 0x00);
    cc2520_release_cs(dev_id);
    return value;
}


/***********************************************************************************
 * @fn      CC2520_REGRD16
 *
 * @brief   Read two register bytes
 *
 * @param  UINT8 addr - address
 *
 * @return  UINT16 - result
 */
UINT16 CC2520_REGRD16(UINT8 dev_id, UINT8 addr)
{
    EWORD value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_REGRD | addr);
    value.b.b0 = cc2520_spi(dev_id, 0x00);
    value.b.b1 = cc2520_spi(dev_id, 0x00);
    cc2520_release_cs(dev_id);
    return value.w;
}


/***********************************************************************************
 * @fn      CC2520_REGRD24
 *
 * @brief   Read three register bytes
 *
 * @param  UINT8 addr - address
 *
 * @return  UINT32 - result
 */
UINT32 CC2520_REGRD24(UINT8 dev_id, UINT8 addr)
{
    EDWORD value;
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_REGRD | addr);
    value.b.b0 = cc2520_spi(dev_id, 0x00);
    value.b.b1 = cc2520_spi(dev_id, 0x00);
    value.b.b2 = cc2520_spi(dev_id, 0x00);
    value.b.b3 = 0x00;
    cc2520_release_cs(dev_id);
    return value.dw;
}


/***********************************************************************************
 * @fn      CC2520_REGWR
 *
 * @brief   Register write. Can only be started from addresses below 0x40
 *
 * @param  UINT8 addr - address
 *         UINT8 count - number of bytes
 *         UINT8  *pValues - data buffer
 *
 * @return  UINT8 - status byte
 */
UINT8 CC2520_REGWR(UINT8 dev_id, UINT8 addr, UINT8 count, UINT8  *pValues)
{
    UINT8 s, i;
    cc2520_request_cs(dev_id);
    s = cc2520_spi(dev_id, CC2520_INS_REGWR | addr);
    for (i = 0; i < count; i++)
    {
        cc2520_spi(dev_id, pValues[i]);
    }
    cc2520_release_cs(dev_id);
    return s;
}


/***********************************************************************************
 * @fn      CC2520_REGWR8
 *
 * @brief   Write one register byte
 *
 * @param  UINT8 addr - address
 *         UINT8 value
 *
 * @return  none
 */
void CC2520_REGWR8(UINT8 dev_id, UINT8 addr, UINT8 value)
{
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_REGWR | addr);
    cc2520_spi(dev_id, value);
    cc2520_release_cs(dev_id);
}


/***********************************************************************************
 * @fn      CC2520_REGWR16
 *
 * @brief   Write two register bytes
 *
 * @param  UINT8 addr - address
 *         UINT16 value
 *
 * @return  none
 */
void CC2520_REGWR16(UINT8 dev_id, UINT8 addr, UINT16 value)
{
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_REGWR | addr);
    cc2520_spi(dev_id, LOBYTE(value));
    cc2520_spi(dev_id, HIBYTE(value));
    cc2520_release_cs(dev_id);
}


/***********************************************************************************
 * @fn      CC2520_REGWR24
 *
 * @brief   Write three register bytes
 *
 * @param  UINT8 addr
 *         UINT32 value
 *
 * @return  none
 */
void CC2520_REGWR24(UINT8 dev_id, UINT8 addr, UINT32 value)
{
    cc2520_request_cs(dev_id);
    cc2520_spi(dev_id, CC2520_INS_REGWR | addr);
    cc2520_spi(dev_id, LOBYTE(LOWORD(value)));
    cc2520_spi(dev_id, HIBYTE(LOWORD(value)));
    cc2520_spi(dev_id, LOBYTE(HIWORD(value)));
    cc2520_release_cs(dev_id);
}
#endif //CC2520_M
