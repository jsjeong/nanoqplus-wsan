// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2520 Driver
 *
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2013.1.24
 */

#include "cc2520.h"

#ifdef CC2520_M

#include "cc2520-interface.h"
#include "cc2520-ins.h"
#include "arch.h"
#include "cc2520-const.h"
#include "string.h"
#include "platform.h"
#include "wpan-dev.h"

// IEEE 802.15.4 defined constants (2.4 GHz logical channels)
#define MIN_CHANNEL           11    // 2405 MHz
#define MAX_CHANNEL           26    // 2480 MHz
#define CHANNEL_SPACING       5     // MHz

typedef struct
{
    uint8_t reg_addr;
    uint8_t val;
} cc2520_reg_t;

/**
 * Recommended register settings which differ from the data sheet.
 * (p103. 28.1 Register Settings Update)
 */
static const cc2520_reg_t cc2520_reg_init[] =
{
    { CC2520_CCACTRL0,    0xF8 }, /* CCA treshold -84dBm */
    { CC2520_MDMCTRL0,    0x85 },
    { CC2520_MDMCTRL1,    0x14 },
    { CC2520_RXCTRL,      0x3F },
    { CC2520_FSCTRL,      0x5A },
    { CC2520_FSCAL1,      0x2B },
    { CC2520_AGCCTRL1,    0x11 },
    { CC2520_ADCTEST0,    0x10 },
    { CC2520_ADCTEST1,    0x0E },
    { CC2520_ADCTEST2,    0x03 },

    // Initialization for nano-mac
    { CC2520_TXPOWER,     0x32 },
    { CC2520_FRMCTRL0,    0x60 }, /* Auto-ack */
    { CC2520_FRMCTRL1,    0x00 }, /* STXON will not set bit 14 in the RXENABLE
                                   * register. Remain idle.
                                   *
                                   * STXONCCA: the receiver would be on before
                                   * the transmission and will be turned back on
                                   * afterwards (unless the RXENABLE registers
                                   * have been cleared in the mean time).
                                   */
    { CC2520_GPIOCTRL0,   1 + CC2520_EXC_RX_FRM_DONE },	// Exception def. + 1 = GPIO def.
    { CC2520_GPIOCTRL1,   1 + CC2520_EXC_TX_FRM_DONE },		// Exception def. + 1 = GPIO def.
    { CC2520_GPIOCTRL2,   CC2520_GPIO_SAMPLED_CCA },		// GPIO def
    { CC2520_GPIOCTRL3,   CC2520_GPIO_FIFO },
    { 0,                  0x00 }
};

extern WPAN_DEV wpan_dev[];

static BOOL cc2520_wait_xosc_ready(UINT8 dev_id)
{
    UINT16 i = 1000;
    cc2520_request_cs(dev_id);
    while (i-- > 0 && !cc2520_pin_is_set(dev_id, CC2520_PIN_SPI_MISO))
    {
        nos_delay_us(1);
    }
    cc2520_release_cs(dev_id);
    return (i > 0);
}

/**
 * Reset the chip and enter into the LPM2.
 */
static void cc2520_off(UINT8 dev_id)
{
    cc2520_set_pin(dev_id, CC2520_PIN_RESET, FALSE);
    cc2520_release_cs(dev_id);
    cc2520_set_pin(dev_id, CC2520_PIN_VREG_EN, FALSE);
}

/**
 * Wake up from the LPM2 and enter the Active mode.
 */
static void cc2520_on(UINT8 dev_id)
{
    cc2520_set_pin(dev_id, CC2520_PIN_VREG_EN, TRUE);
    nos_delay_us(CC2520_VREG_MAX_STARTUP_TIME);
    cc2520_set_pin(dev_id, CC2520_PIN_RESET, TRUE);
    if (!cc2520_wait_xosc_ready(dev_id))
    {
    }
}

void cc2520_set_tx_power(UINT8 dev_id, INT8 dbm)
{
    if (dbm >= 5)
        CC2520_REGWR8(dev_id, CC2520_TXPOWER, 0xF7);
    else if (dbm >= 3)
        CC2520_REGWR8(dev_id, CC2520_TXPOWER, 0xF2);
    else if (dbm >= 2)
        CC2520_REGWR8(dev_id, CC2520_TXPOWER, 0xAB);
    else if (dbm >= 1)
        CC2520_REGWR8(dev_id, CC2520_TXPOWER, 0x13);
    else if (dbm >= 0)
        CC2520_REGWR8(dev_id, CC2520_TXPOWER, 0x32);
    else if (dbm >= -2)
        CC2520_REGWR8(dev_id, CC2520_TXPOWER, 0x81);
    else if (dbm >= -4)
        CC2520_REGWR8(dev_id, CC2520_TXPOWER, 0x88);
    else if (dbm >= -7)
        CC2520_REGWR8(dev_id, CC2520_TXPOWER, 0x2C);
    else
        CC2520_REGWR8(dev_id, CC2520_TXPOWER, 0x03);
}

ERROR_T cc2520_set_channel(UINT8 dev_id, UINT8 channel)
{
    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;
    
    if (channel < 11 || channel > 26)
        return ERROR_INVALID_ARGS;
    
    wpan_dev[dev_id].c.cc2520.channel = channel;
    CC2520_REGWR8(dev_id, CC2520_FREQCTRL,
                  MIN_CHANNEL + ((channel - MIN_CHANNEL) * CHANNEL_SPACING));
    return ERROR_SUCCESS;
}

void cc2520_write_frame(UINT8 dev_id, const UINT8 *buf)
{
    CC2520_SFLUSHTX(dev_id);
    CC2520_TXBUF8(dev_id, buf[0]);
    CC2520_TXBUF(dev_id, buf[0] - 2, (UINT8 *) &buf[1]);
}

BOOL cc2520_rxbuf_is_empty(UINT8 dev_id)
{
    if (wpan_dev[dev_id].c.cc2520.rx_frame.len == 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/**
 * @brief Flush Rx FIFO.
 *
 * It strobes SFLUSHRX command twice and waits until RSSI_VALID bit is set.
 *
 * @param[in] dev_id  Device ID
 */
void cc2520_flush_rxbuf(UINT8 dev_id)
{
    /* Flushing twice for CC2520 Errata bug ID 1. */
    CC2520_SFLUSHRX(dev_id);
    CC2520_SFLUSHRX(dev_id);

    /* Wait until RSSI_VALID bit is set for CC2520 Errata bug ID 3. */
    while (!(CC2520_SNOP(dev_id) & CC2520_STB_RSSI_VALID_BV));
}

void cc2520_setup(UINT8 dev_id)
{
    wpan_dev[dev_id].c.cc2520.last_ack_seq = -1;
    wpan_dev[dev_id].c.cc2520.rx_frame.len = 0;

    //TODO MAC specific register settings.

    CC2520_SRXON(dev_id);
    cc2520_flush_rxbuf(dev_id);
    cc2520_rx_done_intr_flag_enable(dev_id, TRUE);
}

void cc2520_sleep(UINT8 dev_id)
{
    CC2520_SRFOFF(dev_id);
}

void cc2520_wakeup(UINT8 dev_id)
{
    CC2520_SRXON(dev_id);
}

/**
 * @brief Transmit a frame.
 *
 * @param[in] dev_id  Device ID
 * @param[in] buf     Frame buffer pointer
 * @return
 *  * ERROR_SUCCESS         Success
 *  * ERROR_TXFAIL_CH_BUSY  Tx fail due to channel busy (CCA fail)
 *
 * @warning Before entering this function, the CC2520 state would be Rx and its
 *          RSSI_VALID bit would be set (Rx calibrated).
 */
ERROR_T cc2520_tx(UINT8 dev_id, const UINT8 *buf)
{
    CC2520_STXONCCA(dev_id);
    nos_delay_us(32);
    
    //Check whether TX started or not.
    if (!cc2520_sampled_cca_pin_is_set(dev_id))
        return ERROR_TXFAIL_CH_BUSY;

    while (!cc2520_tx_done_pin_is_set(dev_id));
    CC2520_CLEAR_EXC(dev_id, CC2520_EXC_TX_FRM_DONE);

    return ERROR_SUCCESS;
}

ERROR_T cc2520_wait_for_ack(UINT8 dev_id, UINT8 seq)
{
    UINT8 i;
    uint8_t tmp;
    uint16_t fcf;
    ERROR_T err;
	
    wpan_dev[dev_id].c.cc2520.last_ack_seq = -1;
    
    // ACK should be arrived in 34symbol (544us)
    nos_delay_us((12 + 3) * wpan_dev[dev_id].symbol_duration); // 3 is margin.

    for (i = 22; i > 0; i--)
    {
        // Check Ack in case that ISR cannot be called 
        if(cc2520_rx_done_intr_flag_is_set(dev_id))
        {
            err = ERROR_INVALID_FRAME;
            
            tmp = CC2520_RXBUF8(dev_id);
            if (tmp == IEEE_802_15_4_ACK_SIZE)
            {
                fcf = CC2520_RXBUF16(dev_id);
                if (FRAME_IS_ACK(fcf))
                {
                    tmp = CC2520_RXBUF8(dev_id);	// sequence number
                    if (tmp == seq)
                    {
                        err = ERROR_SUCCESS;
                    }
                }
            }
            
            CC2520_CLEAR_EXC(dev_id, CC2520_EXC_RX_FRM_DONE);
            cc2520_rx_done_intr_flag_clear(dev_id);
            cc2520_flush_rxbuf(dev_id);

            return err;
        }

        // in case that ISR can be executed while wating
        if (wpan_dev[dev_id].c.cc2520.last_ack_seq == seq)
        {
            return ERROR_SUCCESS;
        }
        else
        {
            nos_delay_us(wpan_dev[dev_id].symbol_duration);
        }
    }
    return ERROR_FAIL;
}

void cc2520_set_short_addr(UINT8 dev_id)
{
    CC2520_MEMWR16(dev_id, CC2520_RAM_SHORTADDR, wpan_dev[dev_id].short_id);
}

void cc2520_set_pan_id(UINT8 dev_id)
{
    CC2520_MEMWR16(dev_id, CC2520_RAM_PANID, wpan_dev[dev_id].pan_id);
}

void cc2520_set_eui64(UINT8 dev_id)
{
    CC2520_MEMWR8(dev_id, CC2520_RAM_EXTADDR + 0, wpan_dev[dev_id].eui64[7]);
    CC2520_MEMWR8(dev_id, CC2520_RAM_EXTADDR + 1, wpan_dev[dev_id].eui64[6]);
    CC2520_MEMWR8(dev_id, CC2520_RAM_EXTADDR + 2, wpan_dev[dev_id].eui64[5]);
    CC2520_MEMWR8(dev_id, CC2520_RAM_EXTADDR + 3, wpan_dev[dev_id].eui64[4]);
    CC2520_MEMWR8(dev_id, CC2520_RAM_EXTADDR + 4, wpan_dev[dev_id].eui64[3]);
    CC2520_MEMWR8(dev_id, CC2520_RAM_EXTADDR + 5, wpan_dev[dev_id].eui64[2]);
    CC2520_MEMWR8(dev_id, CC2520_RAM_EXTADDR + 6, wpan_dev[dev_id].eui64[1]);
    CC2520_MEMWR8(dev_id, CC2520_RAM_EXTADDR + 7, wpan_dev[dev_id].eui64[0]);
}

ERROR_T cc2520_read_frame(UINT8 dev_id, struct wpan_rx_frame *f)
{
    // non-intended ack blocking.
    // FRMFILT1.ACCEPT_FT2_ACK set after TX. and clear after ack has been received or timeout 

    if (wpan_dev[dev_id].c.cc2520.rx_frame.len > 0)
    {
        *f = wpan_dev[dev_id].c.cc2520.rx_frame;
        f->secured = FALSE; //TODO
        
        wpan_dev[dev_id].c.cc2520.rx_frame.len = 0;
        
        return (f->crc_ok) ? ERROR_SUCCESS : ERROR_CRC_ERROR;
    }
    else
    {
        return ERROR_INVALID_FRAME;
    }
}

static const struct wpan_dev_cmdset cc2520_api =
{
    cc2520_setup,
    cc2520_read_frame,
    cc2520_write_frame,
    cc2520_rxbuf_is_empty,
    cc2520_flush_rxbuf,
    cc2520_sleep,
    cc2520_wakeup,
    cc2520_tx,
    cc2520_wait_for_ack,
    NULL,
    cc2520_set_pan_id,
    cc2520_set_short_addr,
    cc2520_set_eui64,
};

void cc2520_init(UINT8 dev_id, UINT8 init_ch)
{
    const cc2520_reg_t *reg_init_ptr;
    
    // CC2520 reset
    cc2520_off(dev_id);
    nos_delay_us(1100);
    cc2520_on(dev_id);

    wpan_dev[dev_id].hw_autotx = FALSE;
    wpan_dev[dev_id].sec_support = FALSE; //TODO
    wpan_dev[dev_id].addrdec_support = TRUE;
    wpan_dev[dev_id].symbol_duration = 16;
    wpan_dev[dev_id].cmd = &cc2520_api;
    
    // Write non-default register values
    reg_init_ptr = cc2520_reg_init;
    while (reg_init_ptr->reg_addr != 0)
    {
        CC2520_MEMWR8(dev_id, reg_init_ptr->reg_addr,reg_init_ptr->val);
        reg_init_ptr++;
    }
    cc2520_set_tx_power(dev_id, 5);
    cc2520_set_channel(dev_id, init_ch);
}

void cc2520_isr(UINT8 dev_id)
{
    wpan_dev[dev_id].c.cc2520.rx_frame.len = CC2520_RXBUF8(dev_id);
    if (wpan_dev[dev_id].c.cc2520.rx_frame.len < 127 &&
        wpan_dev[dev_id].c.cc2520.rx_frame.len > 2)
    {
        wpan_dev[dev_id].c.cc2520.rx_frame.len -= 2;
        CC2520_RXBUF(dev_id,
                     wpan_dev[dev_id].c.cc2520.rx_frame.len,
                     wpan_dev[dev_id].c.cc2520.rx_frame.buf);
        wpan_dev[dev_id].c.cc2520.rx_frame.rssi = (INT8) CC2520_RXBUF8(dev_id);
        wpan_dev[dev_id].c.cc2520.rx_frame.lqi = CC2520_RXBUF8(dev_id);

        wpan_dev[dev_id].c.cc2520.rx_frame.crc_ok =
            ((wpan_dev[dev_id].c.cc2520.rx_frame.lqi & CC2520_FCS_CRC_BM) ?
             TRUE : FALSE);
        wpan_dev[dev_id].c.cc2520.rx_frame.lqi &= CC2520_FCS_CORR_BM;
    }
    else
    {
        wpan_dev[dev_id].c.cc2520.rx_frame.len = 0;
    }
    
    CC2520_CLEAR_EXC(dev_id, CC2520_EXC_RX_FRM_DONE);
    cc2520_rx_done_intr_flag_clear(dev_id);
    cc2520_flush_rxbuf(dev_id);

    if (wpan_dev[dev_id].c.cc2520.rx_frame.crc_ok &&
             (wpan_dev[dev_id].c.cc2520.rx_frame.len + 2 ==
              IEEE_802_15_4_ACK_SIZE))
    {
        // Handles Ack frame
        UINT16 fcf = (wpan_dev[dev_id].c.cc2520.rx_frame.buf[0] +
                      (wpan_dev[dev_id].c.cc2520.rx_frame.buf[1] << 8));
        
        if (FRAME_IS_ACK(fcf))
        {
            wpan_dev[dev_id].c.cc2520.last_ack_seq =
                wpan_dev[dev_id].c.cc2520.rx_frame.buf[2];
        }
        wpan_dev[dev_id].c.cc2520.rx_frame.len = 0;	// empty
    }
    else if (wpan_dev[dev_id].c.cc2520.rx_frame.len > 0)
    {
        // Call MAC layer handler for data frame. 'bottom isr' will be better. (need to be fixed)
        if (wpan_dev[dev_id].notify)
            wpan_dev[dev_id].notify(dev_id, 0, NULL);
        else
            wpan_dev[dev_id].c.cc2520.rx_frame.len = 0;
    }
}

#endif //CC2520_M
