// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * AT86RF230 Driver
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 11.
 */

#include "rf230.h"
#ifdef AT86RF230_M
#include "rf230-interface.h"
#include "rf230-const.h"
#include "rf230-mem-access.h"
#include "wpan-dev.h"
#include "platform.h"
#include "arch.h"

#ifdef GLOSSY_M
#include "rf230-glossy.h"
#endif

extern WPAN_DEV wpan_dev[];
extern NOS_MAC nos_mac[];

ERROR_T rf230_set_channel(UINT8 dev_id, UINT8 channel)
{
    if (dev_id >= WPAN_DEV_CNT)
        return ERROR_NOT_FOUND;

    if (channel > 26 || channel < 11)
        return ERROR_INVALID_ARGS;

    rf230_reg_write(dev_id, RF230_REG_PHY_CC_CCA,
                    RF230_CCA_MODE3_CS_WITH_ED |
                    channel);
    wpan_dev[dev_id].c.rf230.channel = channel;
    
    return ERROR_SUCCESS;
}

static const UINT8 rf230_tx_power_table[8] =
{
    0xA,  //-4.2dBm ~= -4
    0x9,  //-3.2dBm ~= -3
    0x8,  //-2.2dBm ~= -2
    0x7,  //-1.2dBm ~= -1
    0x6,  //-0.2dBm ~= 0
    0x4,  //+1.1dBm ~= 1
    0x2,  //+2.1dBm ~= 2
    0x0,  //+3.0dBm ~= 3
};
void rf230_set_tx_power(UINT8 dev_id, INT8 dbm)
{
    if (dbm < -4) dbm = 0;
    else if (dbm > 3) dbm = 7;
    else dbm += 4;

    rf230_reg_write(dev_id, RF230_REG_PHY_TX_PWR,
                    RF230_TX_AUTO_CRC_ENABLE |
                    rf230_tx_power_table[(UINT8) dbm]);
}

static void rf230_pll_on(UINT8 dev_id)
{
    UINT8 st;

    rf230_enable_mcu_irq(dev_id, FALSE);
    
    do {
        st = rf230_subreg_read(dev_id, RF230_SREG_TRX_STATUS);

        if (st == RF230_TRX_STATUS_RX_ON || st == RF230_TRX_STATUS_TRX_OFF)
        {
            rf230_reg_write(dev_id, RF230_REG_TRX_STATE,
                            RF230_TRX_CMD_PLL_ON);
        }
    } while(st != RF230_TRX_STATUS_PLL_ON);

    rf230_clear_mcu_irq(dev_id);
    rf230_enable_mcu_irq(dev_id, TRUE);
    rf230_reg_read(dev_id, RF230_REG_IRQ_STATUS);
}

static void rf230_listen(UINT8 dev_id)
{
    UINT8 st;

    do {
        st = rf230_subreg_read(dev_id, RF230_SREG_TRX_STATUS);
        if (st == RF230_TRX_STATUS_TRX_OFF || st == RF230_TRX_STATUS_PLL_ON)
        {
            rf230_reg_write(dev_id, RF230_REG_TRX_STATE,
                            RF230_TRX_CMD_RX_ON);
        }
    } while(st != RF230_TRX_STATUS_RX_ON);

#ifdef GLOSSY_M
    if (wpan_dev[dev_id].mac_type == MAC_TYPE_GLOSSY)
        wpan_dev[dev_id].c.rf230.glossy_forward_ready = FALSE;
#endif
}

static void rf230_config(UINT8 dev_id)
{
#ifdef GLOSSY_M
    if (wpan_dev[dev_id].mac_type == MAC_TYPE_GLOSSY)
        wpan_dev[dev_id].c.rf230.glossy_forward_ready = FALSE;
#endif

    rf230_reg_read(dev_id, RF230_REG_IRQ_STATUS);
    rf230_reg_write(dev_id, RF230_REG_IRQ_MASK, RF230_IRQ_TRX_END);
    rf230_listen(dev_id);
}

void rf230_osc_off(UINT8 dev_id)
{
    BOOL sleeping;
    UINT8 st;

    rf230_get_pin(dev_id, RF230_PIN_SLP_TR, &sleeping);
    if (sleeping)
    {
        // Already sleeping.
        return;
    }

    rf230_enable_mcu_irq(dev_id, FALSE);

    do
    {
        st = rf230_subreg_read(dev_id, RF230_SREG_TRX_STATUS);
        
        if (st == RF230_TRX_STATUS_RX_ON || st == RF230_TRX_STATUS_PLL_ON)
        {
            rf230_reg_write(dev_id, RF230_REG_TRX_STATE,
                            RF230_TRX_CMD_FORCE_TRX_OFF);
            nos_delay_us(1);
        }
    } while(st != RF230_TRX_STATUS_TRX_OFF);

    rf230_clear_mcu_irq(dev_id);
    rf230_enable_mcu_irq(dev_id, TRUE);
    rf230_reg_read(dev_id, RF230_REG_IRQ_STATUS);

    rf230_set_pin(dev_id, RF230_PIN_SLP_TR, TRUE);
}

void rf230_osc_on(UINT8 dev_id)
{
    UINT8 st;
    BOOL sleeping;

    rf230_get_pin(dev_id, RF230_PIN_SLP_TR, &sleeping);
    if (!sleeping)
    {
        // Already waked up state.
        return;
    }
    
    rf230_set_pin(dev_id, RF230_PIN_SLP_TR, FALSE);
    nos_delay_us(880);

    do
    {
        st = rf230_subreg_read(dev_id, RF230_SREG_TRX_STATUS);
    } while(st != RF230_TRX_STATUS_TRX_OFF);

    rf230_listen(dev_id);
}

static void rf230_write_frame(UINT8 dev_id, const UINT8 *buf)
{
    rf230_pll_on(dev_id);
    rf230_write_frame_buf(dev_id, buf);
    wpan_dev[dev_id].c.rf230.frame_ready_to_send = TRUE;
}

ERROR_T rf230_tx(UINT8 dev_id, const UINT8 *buf)
{
    UINT8 intr;

    if (wpan_dev[dev_id].mac_type == MAC_TYPE_NANO_MAC)
    {
        rf230_pll_on(dev_id);
        rf230_write_frame_buf(dev_id, buf);
    }

    rf230_enable_mcu_irq(dev_id, FALSE);

    // Just send a frame stored in the Frame Buffer.
    rf230_set_pin(dev_id, RF230_PIN_SLP_TR, TRUE);
    nos_delay_us(0);
    rf230_set_pin(dev_id, RF230_PIN_SLP_TR, FALSE);

    // Wait for TX done.
    while (rf230_subreg_read(dev_id, RF230_SREG_TRX_STATUS) !=
           RF230_TRX_STATUS_PLL_ON);

    rf230_clear_mcu_irq(dev_id);
    rf230_enable_mcu_irq(dev_id, TRUE);
    intr = rf230_reg_read(dev_id, RF230_REG_IRQ_STATUS);

    if (wpan_dev[dev_id].mac_type == MAC_TYPE_NANO_MAC)
    {
        rf230_listen(dev_id);
    }
    
    return (intr & RF230_IRQ_TRX_END) ? ERROR_SUCCESS : ERROR_FAIL;
}

BOOL rf230_rxbuf_is_empty(UINT8 dev_id)
{
    return (wpan_dev[dev_id].c.rf230.frame_received == FALSE);
}

void rf230_flush_rxbuf(UINT8 dev_id)
{
    struct wpan_rx_frame tmp;

    // To release the Frame Buffer from the Dynamic Frame Buffer Protection.
    rf230_read_frame_buf(dev_id, &tmp);
    wpan_dev[dev_id].c.rf230.frame_received = FALSE;
}

static void rf230_send_ack(UINT8 dev_id, UINT8 seq)
{
    UINT8 ack_frame[4] = { 0x05, 0x02, 0x00, 0x00 };
    ack_frame[3] = seq;

    rf230_tx(dev_id, ack_frame);
}

static ERROR_T rf230_read_frame(UINT8 dev_id, struct wpan_rx_frame *f)
{
    struct glossy_ctrl *g = (struct glossy_ctrl *) nos_mac[dev_id].mac_ctrl;
    ERROR_T err;
    UINT16 fcf, pan, id;

#ifdef GLOSSY_M
    if (wpan_dev[dev_id].mac_type == MAC_TYPE_GLOSSY &&
        wpan_dev[dev_id].c.rf230.glossy_forward_ready)
    {
        *f = nos_mac[dev_id].c.glossy.rx_frame_buf;
        err = ERROR_SUCCESS;
    }
    else
    {
        err = rf230_read_frame_buf(dev_id, f);
    }
#else
    err = rf230_read_frame_buf(dev_id, f);
#endif
    
    wpan_dev[dev_id].c.rf230.frame_received = FALSE;

    if (err != ERROR_SUCCESS)
    {
        return err;
    }

    fcf = f->buf[0] + (f->buf[1] << 8);
    pan = f->buf[3] + (f->buf[4] << 8);
    id = f->buf[5] + (f->buf[6] << 8);

    if (FRAME_REQUESTS_ACK(fcf) &&
        (pan == 0xFFFF || pan == wpan_dev[dev_id].pan_id) &&
        ((FRAME_CONTAINS_SHORTDST(fcf) && id == wpan_dev[dev_id].short_id) ||
         (f->len >= 14 &&
          FRAME_CONTAINS_LONGDST(fcf) &&
          f->buf[5] == wpan_dev[dev_id].eui64[7] &&
          f->buf[6] == wpan_dev[dev_id].eui64[6] &&
          f->buf[7] == wpan_dev[dev_id].eui64[5] &&
          f->buf[8] == wpan_dev[dev_id].eui64[4] &&
          f->buf[9] == wpan_dev[dev_id].eui64[3] &&
          f->buf[10] == wpan_dev[dev_id].eui64[2] &&
          f->buf[11] == wpan_dev[dev_id].eui64[1] &&
          f->buf[12] == wpan_dev[dev_id].eui64[0])))
    {
        rf230_send_ack(dev_id, f->buf[2]);
    }

    if (!f->crc_ok)
        return ERROR_CRC_ERROR;

    f->secured = FALSE;
    return ERROR_SUCCESS;
}

BOOL rf230_cca(UINT8 dev_id)
{
    UINT8 prev_st;
    BOOL channel_is_clear;

    // Turn off TX interrupt while CCA.
    rf230_enable_mcu_irq(dev_id, FALSE);

    prev_st = rf230_subreg_read(dev_id, RF230_SREG_TRX_STATUS);
    if (prev_st != RF230_TRX_STATUS_RX_ON)
    {
        rf230_listen(dev_id);
    }
    
    rf230_reg_write(dev_id, RF230_REG_PHY_CC_CCA,
                    RF230_CCA_REQUEST |
                    RF230_CCA_MODE3_CS_WITH_ED |
                    wpan_dev[dev_id].c.rf230.channel);

    while (TRUE)
    {
        UINT8 st = rf230_reg_read(dev_id, RF230_REG_TRX_STATUS);
        if (st & RF230_CCA_DONE)
        {
            channel_is_clear = ((st & RF230_CCA_STATUS_MASK) == RF230_CCA_STATUS_IDLE);
            break;
        }
    }

    // Clear interrupt flags.
    rf230_clear_mcu_irq(dev_id);
    rf230_enable_mcu_irq(dev_id, TRUE);
    rf230_reg_read(dev_id, RF230_REG_IRQ_STATUS);

    if (prev_st != RF230_TRX_STATUS_RX_ON)
    {
        rf230_pll_on(dev_id);
    }
    return channel_is_clear;
}

ERROR_T rf230_wait_for_ack(UINT8 dev_id, UINT8 seq)
{
    UINT8 intr;
    ERROR_T err = ERROR_FAIL;
    struct wpan_rx_frame ack;

    rf230_enable_mcu_irq(dev_id, FALSE);

    // An enough duration value is used for the software acknowledge.
    //nos_delay_us(54 * SYMBOL_DURATION);
    nos_delay_us(4000);

    rf230_clear_mcu_irq(dev_id);
    rf230_enable_mcu_irq(dev_id, TRUE);
    intr = rf230_reg_read(dev_id, RF230_REG_IRQ_STATUS);

    if (intr & RF230_IRQ_TRX_END)
    {
        if (rf230_read_frame_buf(dev_id, &ack) == ERROR_SUCCESS)
        {
            if (FRAME_IS_ACK(ack.buf[0] + (ack.buf[1] << 8)) &&
                ack.buf[2] == seq)
            {
                err = ERROR_SUCCESS;
            }
            else
            {
                err = ERROR_INVALID_FRAME;
            }
        }
    }

    return err;
}

static const struct wpan_dev_cmdset rf230_api =
{
    rf230_config,
    rf230_read_frame,
    rf230_write_frame,
    rf230_rxbuf_is_empty,
    rf230_flush_rxbuf,
    rf230_osc_off,
    rf230_osc_on,
    rf230_tx,
    rf230_wait_for_ack,
    rf230_cca,
    NULL,
    NULL,
    
#ifdef GLOSSY_M
    rf230_peek_received_frame,
    rf230_pll_on,
    rf230_listen,
#endif
};

void rf230_init(UINT8 dev_id, enum rf230_clkm clkm)
{
    UINT8 status;
    
    wpan_dev[dev_id].hw_autotx = FALSE;
    wpan_dev[dev_id].sec_support = FALSE;
    wpan_dev[dev_id].addrdec_support = FALSE;
    wpan_dev[dev_id].symbol_duration = 12;
    wpan_dev[dev_id].cmd = &rf230_api;
    wpan_dev[dev_id].c.rf230.frame_received = FALSE;

    /* HW reset of radio transeiver */
    rf230_set_pin(dev_id, RF230_PIN_RESETN, FALSE); // RESET state
    rf230_set_pin(dev_id, RF230_PIN_SLP_TR, FALSE);
    nos_delay_us(6);
    rf230_set_pin(dev_id, RF230_PIN_RESETN, TRUE); // TRX_OFF state

    /* Force transition to TRX_OFF */
    rf230_reg_write(dev_id, RF230_REG_TRX_STATE, RF230_TRX_CMD_FORCE_TRX_OFF);
    nos_delay_us(510);
    
    do {
        status = rf230_subreg_read(dev_id, RF230_SREG_TRX_STATUS);
    } while (status != RF230_TRX_STATUS_TRX_OFF);

    if (clkm == RF230_CLKM_16MHZ)
    {
        rf230_reg_write(dev_id, RF230_REG_TRX_CTRL_0,
                        RF230_PAD_IO_2mA |
                        RF230_PAD_IO_CLKM_4mA |
                        RF230_CLKM_16MHz);
    }
    else if (clkm == RF230_CLKM_8MHZ)
    {
        rf230_reg_write(dev_id, RF230_REG_TRX_CTRL_0,
                        RF230_PAD_IO_2mA |
                        RF230_PAD_IO_CLKM_4mA |
                        RF230_CLKM_8MHz);
    }
    else if (clkm == RF230_CLKM_4MHZ)
    {
        rf230_reg_write(dev_id, RF230_REG_TRX_CTRL_0,
                        RF230_PAD_IO_2mA |
                        RF230_PAD_IO_CLKM_4mA |
                        RF230_CLKM_4MHz);
    }
    else if (clkm == RF230_CLKM_2MHZ)
    {
        rf230_reg_write(dev_id, RF230_REG_TRX_CTRL_0,
                        RF230_PAD_IO_2mA |
                        RF230_PAD_IO_CLKM_4mA |
                        RF230_CLKM_2MHz);
    }
    else if (clkm == RF230_CLKM_1MHZ)
    {
        rf230_reg_write(dev_id, RF230_REG_TRX_CTRL_0,
                        RF230_PAD_IO_2mA |
                        RF230_PAD_IO_CLKM_4mA |
                        RF230_CLKM_1MHz);
    }
    else if (clkm == RF230_CLKM_NOCLK)
    {
        rf230_reg_write(dev_id, RF230_REG_TRX_CTRL_0,
                        RF230_PAD_IO_2mA |
                        RF230_PAD_IO_CLKM_4mA |
                        RF230_CLKM_NO_CLK);
    }

    rf230_set_tx_power(dev_id, 3); //3: default Tx power.
    rf230_set_channel(dev_id, 11); //11: default channel.
}

void rf230_interrupt_handler(UINT8 dev_id)
{
    UINT8 interrupts;

    /*
     * Clearing the interrupt flag of the MCU side should be done before
     * clearing the flag of the RF230 side.
     */
    rf230_clear_mcu_irq(dev_id);
    interrupts = rf230_reg_read(dev_id, RF230_REG_IRQ_STATUS);

    if (interrupts)
    {
        wpan_dev[dev_id].c.rf230.frame_received = TRUE;
        wpan_dev[dev_id].c.rf230.frame_ready_to_send = FALSE;
    }
    else
    {
        return;
    }

    wpan_dev[dev_id].notify(dev_id, 0, NULL);

    if (!rf230_rxbuf_is_empty(dev_id))
    {
        rf230_flush_rxbuf(dev_id);
    }
}

#endif //AT86RF230_M
