// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * AT86RF230 memory access
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 11.
 */

#ifndef AT86RF230_MEM_ACCESS_H
#define AT86RF230_MEM_ACCESS_H

#include "kconf.h"
#ifdef AT86RF230_M
#include "nos_common.h"
#include "errorcodes.h"
#include "ieee-802-15-4.h"

UINT8 rf230_reg_read(UINT8 dev_id, UINT8 reg);
UINT8 rf230_subreg_read(UINT8 dev_id, UINT8 reg, UINT8 mask, UINT8 position);
UINT8 rf230_reg_write(UINT8 dev_id, UINT8 reg, UINT8 value);
UINT8 rf230_subreg_write(UINT8 dev_id, UINT8 reg, UINT8 mask, UINT8 position, UINT8 value);
void rf230_sram_write(UINT8 dev_id, UINT8 addr, const UINT8 *buf, UINT8 size);
void rf230_sram_read(UINT8 dev_id, UINT8 addr, UINT8 *buf, UINT8 size);

ERROR_T rf230_write_frame_buf(UINT8 dev_id, const UINT8 *buf);
ERROR_T rf230_read_frame_buf(UINT8 dev_id, struct wpan_rx_frame *f);

#endif //AT86RF230_M
#endif //AT86RF230_MEM_ACCESS_H
