// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * AT86RF230 constants
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 11.
 */

#ifndef RF230_CONST_H
#define RF230_CONST_H

enum AT86RF212_REGISTER
{
    RF230_REG_TRX_STATUS =     0x01,
    RF230_REG_TRX_STATE =      0x02,
    RF230_REG_TRX_CTRL_0 =     0x03,
    RF230_REG_TRX_CTRL_1 =     0x04,
    RF230_REG_PHY_TX_PWR =     0x05,
    RF230_REG_PHY_RSSI =       0x06,
    RF230_REG_PHY_ED_LEVEL =   0x07,
    RF230_REG_PHY_CC_CCA =     0x08,
    RF230_REG_CCA_THRES =      0x09,
    RF230_REG_IRQ_MASK =       0x0E,
    RF230_REG_IRQ_STATUS =     0x0F,
    RF230_REG_VREG_CTRL =      0x10,
    RF230_REG_BATMON =         0x11,
    RF230_REG_XOSC_CTRL =      0x12,
    RF230_REG_PLL_CF =         0x1A,
    RF230_REG_PLL_DCU =        0x1B,
    RF230_REG_PART_NUM =       0x1C,
    RF230_REG_VERSION_NUM =    0x1D,
    RF230_REG_MAN_ID_0 =       0x1E,
    RF230_REG_MAN_ID_1 =       0x1F,
    RF230_REG_SHORT_ADDR_0 =   0x20,
    RF230_REG_SHORT_ADDR_1 =   0x21,
    RF230_REG_PAN_ID_0 =       0x22,
    RF230_REG_PAN_ID_1 =       0x23,
    RF230_REG_IEEE_ADDR_0 =    0x24,
    RF230_REG_IEEE_ADDR_1 =    0x25,
    RF230_REG_IEEE_ADDR_2 =    0x26,
    RF230_REG_IEEE_ADDR_3 =    0x27,
    RF230_REG_IEEE_ADDR_4 =    0x28,
    RF230_REG_IEEE_ADDR_5 =    0x29,
    RF230_REG_IEEE_ADDR_6 =    0x2A,
    RF230_REG_IEEE_ADDR_7 =    0x2B,
    RF230_REG_XAH_CTRL =       0x2C,
    RF230_REG_CSMA_SEED_0 =    0x2D,
    RF230_REG_CSMA_SEED_1 =    0x2E,
};

/* TRX_STATUS */
#define RF230_SREG_CCA_DONE         RF230_REG_TRX_STATUS, 0x80, 7

enum RF230_CCA_DONE_VALUE
{
    RF230_CCA_DONE = (1 << 7),
};

#define RF230_SREG_CCA_STATUS       RF230_REG_TRX_STATUS, 0x40, 6

enum RF230_CCA_STATUS_VALUE
{
    RF230_CCA_STATUS_MASK = 0x40,
    RF230_CCA_STATUS_BUSY = (0 << 6),
    RF230_CCA_STATUS_IDLE = (1 << 6),
};

#define RF230_SREG_TRX_STATUS       RF230_REG_TRX_STATUS, 0x1f, 0

enum RF230_TRX_STATUS_VALUE
{
    RF230_TRX_STATUS_P_ON                    = 0x00,
    RF230_TRX_STATUS_BUSY_RX                 = 0x01,
    RF230_TRX_STATUS_BUSY_TX                 = 0x02,
    RF230_TRX_STATUS_RX_ON                   = 0x06,
    RF230_TRX_STATUS_TRX_OFF                 = 0x08,
    RF230_TRX_STATUS_PLL_ON                  = 0x09,
    RF230_TRX_STATUS_SLEEP_REG               = 0x0f,
    RF230_TRX_STATUS_BUSY_RX_AACK            = 0x11,
    RF230_TRX_STATUS_BUSY_TX_ARET            = 0x12,
    RF230_TRX_STATUS_RX_AACK_ON              = 0x16,
    RF230_TRX_STATUS_TX_ARET_ON              = 0x19,
    RF230_TRX_STATUS_RX_ON_NOCLK             = 0x1c,
    RF230_TRX_STATUS_RX_AACK_ON_NOCLK        = 0x1d,
    RF230_TRX_STATUS_BUSY_RX_AACK_NOCLK      = 0x1e,
    RF230_TRX_STATUS_TRANSITION_IN_PROGRESS  = 0x1f,
};

/* TRX_STATE */
#define RF230_SREG_TRAC_STATUS      RF230_REG_TRX_STATE, 0xe0, 5

enum RF230_TRAC_STATUS
{
    RF230_TRAC_STATUS_SUCCESS                 = 0,
    RF230_TRAC_STATUS_SUCCESS_DATA_PENDING    = 1,
    RF230_TRAC_STATUS_SUCCESS_WAIT_FOR_ACK    = 2,
    RF230_TRAC_STATUS_CHANNEL_ACCESS_FAILURE  = 3,
    RF230_TRAC_STATUS_NO_ACK                  = 5,
    RF230_TRAC_STATUS_INVALID                 = 7,
};

enum RF230_TRX_CMD
{
    RF230_TRX_CMD_NOP            = (0x00 << 0),
    RF230_TRX_CMD_TX_START       = (0x02 << 0),
    RF230_TRX_CMD_FORCE_TRX_OFF  = (0x03 << 0),
    RF230_TRX_CMD_RX_ON          = (0x06 << 0),
    RF230_TRX_CMD_TRX_OFF        = (0x08 << 0),
    RF230_TRX_CMD_PLL_ON         = (0x09 << 0),
    RF230_TRX_CMD_RX_AACK_ON     = (0x16 << 0),
    RF230_TRX_CMD_TX_ARET_ON     = (0x19 << 0),
};

/* TRX_CTRL_0 */
enum RF230_PAD_IO
{
    RF230_PAD_IO_2mA = (0 << 6),
    RF230_PAD_IO_4mA = (1 << 6),
    RF230_PAD_IO_6mA = (2 << 6),
    RF230_PAD_IO_8mA = (3 << 6),
};

enum RF230_PAD_IO_CLKM
{
    RF230_PAD_IO_CLKM_2mA = (0 << 4),
    RF230_PAD_IO_CLKM_4mA = (1 << 4),
    RF230_PAD_IO_CLKM_6mA = (2 << 4),
    RF230_PAD_IO_CLKM_8mA = (3 << 4),
};

enum RF230_CLKM_FREQUENCY
{
    RF230_CLKM_NO_CLK = (0 << 0),
    RF230_CLKM_1MHz = (1 << 0),
    RF230_CLKM_2MHz = (2 << 0),
    RF230_CLKM_4MHz = (3 << 0),
    RF230_CLKM_8MHz = (4 << 0),
    RF230_CLKM_16MHz = (5 << 0),
};

#define RF230_SREG_TX_AUTO_CRC_ON     RF230_REG_PHY_TX_PWR, 0x80, 7
#define RF230_SREG_TX_PWR             RF230_REG_PHY_TX_PWR, 0x1f, 0

enum RF230_TX_AUTO_CRC
{
    RF230_TX_AUTO_CRC_ENABLE = (1 << 7),
    RF230_TX_AUTO_CRC_DISABLE = (0 << 7),
};

enum RF230_TX_PWR
{
    RF230_TX_PWR_3p0dBm  = (0x00 << 0), //+3.0 dBm
    RF230_TX_PWR_2p6dBm  = (0x01 << 0), //+2.6 dBm
    RF230_TX_PWR_2p1dBm  = (0x02 << 0), //+2.1 dBm
    RF230_TX_PWR_1p6dBm  = (0x03 << 0), //+1.6 dBm
    RF230_TX_PWR_1p1dBm  = (0x04 << 0), //+1.1 dBm
    RF230_TX_PWR_0p5dBm  = (0x05 << 0), //+0.5 dBm
    RF230_TX_PWR_n0p2dBm = (0x06 << 0), //-0.2 dBm
    RF230_TX_PWR_n1p2dBm = (0x07 << 0), //-1.2 dBm
    RF230_TX_PWR_n2p2dBm = (0x08 << 0), //-2.2 dBm
    RF230_TX_PWR_n3p2dBm = (0x09 << 0), //-3.2 dBm
    RF230_TX_PWR_n4p2dBm = (0x0A << 0), //-4.2 dBm
};

#define RF230_SREG_RX_CRC_VALID       RF230_REG_PHY_RSSI, 0x80, 7
#define RF230_SREG_RSSI               RF230_REG_PHY_RSSI, 0x1f, 0

enum RF230_RX_CRC_VALID_VALUES
{
    RF230_RX_CRC_VALID_MASK = 0x80,
    RF230_RX_CRC_VALID_SHIFT = 7,
};

enum RF230_RSSI_VALUES
{
    RF230_RSSI_MASK = 0x1F,
    RF230_RSSI_SHIFT = 0,
};

enum RF230_CCA_REQUEST
{
    RF230_CCA_REQUEST = (1 << 7),
};

enum RF230_CCA_MODE
{
    RF230_CCA_MODE1_ED_ONLY = (1 << 5),
    RF230_CCA_MODE2_CS_ONLY = (2 << 5),
    RF230_CCA_MODE3_CS_WITH_ED = (3 << 5),
};

#define RF230_SREG_CHANNEL            RF230_REG_PHY_CC_CCA, 0x1f, 0

#define RF230_SREG_CCA_ED_THRES       RF230_REG_CCA_THRES, 0x0f, 0

enum RF230_IRQ_VALUE
{
    RF230_IRQ_BAT_LOW     = (1 << 7),
    RF230_IRQ_TRX_UR      = (1 << 6),
    RF230_IRQ_TRX_END     = (1 << 3),
    RF230_IRQ_RX_START    = (1 << 2),
    RF230_IRQ_PLL_UNLOCK  = (1 << 1),
    RF230_IRQ_PLL_LOCK    = (1 << 0),
};

#define RF230_SREG_IRQ_BAT_LOW        RF230_REG_IRQ_STATUS, 0x80, 7
#define RF230_SREG_IRQ_TRX_UR         RF230_REG_IRQ_STATUS, 0x40, 6
#define RF230_SREG_IRQ_TRX_END        RF230_REG_IRQ_STATUS, 0x08, 3
#define RF230_SREG_IRQ_RX_START       RF230_REG_IRQ_STATUS, 0x04, 2
#define RF230_SREG_IRQ_PLL_UNLOCK     RF230_REG_IRQ_STATUS, 0x02, 1
#define RF230_SREG_IRQ_PLL_LOCK       RF230_REG_IRQ_STATUS, 0x01, 0

#define RF230_SREG_AVREG_EXT          RF230_REG_VREG_CTRL, 0x80, 7
#define RF230_SREG_AVDD_OK            RF230_REG_VREG_CTRL, 0x40, 6
#define RF230_SREG_DVREG_EXT          RF230_REG_VREG_CTRL, 0x08, 3
#define RF230_SREG_DVDD_OK            RF230_REG_VREG_CTRL, 0x04, 2

#define RF230_SREG_BATMON_OK          RF230_REG_BATMON, 0x20, 5
#define RF230_SREG_BATMON_HR          RF230_REG_BATMON, 0x10, 4
#define RF230_SREG_BATMON_VTH         RF230_REG_BATMON, 0x0f, 0

#define RF230_SREG_XTAL_MODE          RF230_REG_XOSC_CTRL, 0xf0, 4
#define RF230_SREG_XTAL_TRIM          RF230_REG_XOSC_CTRL, 0x0f, 0

#define RF230_SREG_PLL_CF_START       RF230_REG_PLL_CF, 0x80, 7
#define RF230_SREG_PLL_DCU_START      RF230_REG_PLL_DCU, 0x80, 7

#define RF230_SREG_MAX_FRAME_RETRIES  RF230_REG_XAH_CTRL, 0xf0, 4
#define RF230_SREG_MAX_CSMA_RETRIES   RF230_REG_XAH_CTRL, 0x0e, 1

#define RF230_SREG_MIN_BE             RF230_REG_CSMA_SEED_1, 0xc0, 6
#define RF230_SREG_AACK_SET_PD        RF230_REG_CSMA_SEED_1, 0x20, 5
#define RF230_SREG_I_AM_COORD         RF230_REG_CSMA_SEED_1, 0x08, 3
#define RF230_SREG_CSMA_SEED_1        RF230_REG_CSMA_SEED_1, 0x07, 0

#define MIN_CHANNEL  (1)
#define MAX_CHANNEL  (10)

enum
{
    TIME_TO_ENTER_P_ON = 510, //!< Transition time from VCC is applied to P_ON.
    TIME_P_ON_TO_TRX_OFF = 510, //!< Transition time from P_ON to TRX_OFF.
    TIME_SLEEP_TO_TRX_OFF = 880, //!< Transition time from SLEEP to TRX_OFF.
    TIME_RESET = 6, //!< Time to hold the RST pin low during reset
    TIME_ED_MEASUREMENT = 140, //!< Time it takes to do a ED measurement.
    TIME_CCA = 140, //!< Time it takes to do a CCA.
    TIME_PLL_LOCK = 150, //!< Maximum time it should take for the PLL to lock.
    TIME_FTN_TUNING = 25, //!< Maximum time it should take to do the filter tuning.
    TIME_NOCLK_TO_WAKE = 6, //!< Transition time from *_NOCLK to being awake.
    TIME_CMD_FORCE_TRX_OFF = 1, //!< Time it takes to execute the FORCE_TRX_OFF command.
    TIME_TRX_OFF_TO_PLL_ACTIVE = 180, //!< Transition time from TRX_OFF to: RX_ON, PLL_ON, TX_ARET_ON and RX_AACK_ON.
    TIME_STATE_TRANSITION_PLL_ACTIVE = 1, //!< Transition time from PLL active state to another.
};

#endif // RF230_CONST_H
