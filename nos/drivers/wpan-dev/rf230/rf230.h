// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * AT86RF230 Driver
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 11.
 */

#ifndef AT86RF230_H
#define AT86RF230_H
#include "kconf.h"
#ifdef AT86RF230_M
#include "nos_common.h"
#include "errorcodes.h"

enum rf230_clkm
{
    RF230_CLKM_16MHZ,
    RF230_CLKM_8MHZ,
    RF230_CLKM_4MHZ,
    RF230_CLKM_2MHZ,
    RF230_CLKM_1MHZ,
    RF230_CLKM_250KHZ,
    RF230_CLKM_SYMRATE,
    RF230_CLKM_NOCLK,
};

void rf230_init(UINT8 dev_id, enum rf230_clkm clkm);

void rf230_interrupt_handler(UINT8 dev_id);

struct rf230
{
    BOOL frame_received:1;
    BOOL frame_ready_to_send:1;
#ifdef GLOSSY_M
    BOOL glossy_forward_ready:1;
#endif
    UINT8 channel;
};

#endif //AT86RF230_M
#endif //AT86RF230_H

