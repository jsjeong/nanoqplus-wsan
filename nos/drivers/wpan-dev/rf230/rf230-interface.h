// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * AT86RF230 Interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 10.
 */

#ifndef RF230_INTERFACE_H
#define RF230_INTERFACE_H

#include "kconf.h"
#ifdef AT86RF230_M

#include "nos_common.h"
#include "errorcodes.h"

void rf230_interface_init(UINT8 dev_id);
ERROR_T rf230_request_cs(UINT8 dev_id);
ERROR_T rf230_release_cs(UINT8 dev_id);
ERROR_T rf230_spi(UINT8 dev_id, UINT8 txdata, UINT8 *rxdata);

enum rf230_pin
{
    RF230_PIN_RESETN,
    RF230_PIN_SLP_TR,
    RF230_PIN_IRQ,
    RF230_PIN_SPI_CSN,
};

ERROR_T rf230_set_pin(UINT8 dev_id, enum rf230_pin pin, BOOL state);
ERROR_T rf230_get_pin(UINT8 dev_id, enum rf230_pin pin, BOOL *state);

ERROR_T rf230_enable_mcu_irq(UINT8 dev_id, BOOL enable);
ERROR_T rf230_clear_mcu_irq(UINT8 dev_id);

#endif //AT86RF230_M
#endif //RF230_INTERFACE_H
