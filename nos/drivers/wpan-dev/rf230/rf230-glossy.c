// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * RF230 driver add-on to support Glossy
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 11.
 */

#include "rf230-glossy.h"

#if defined AT86RF230_M && defined GLOSSY_M

#include "rf230.h"
#include "rf230-mem-access.h"
#include "rf230-const.h"
#include "rf230-interface.h"
#include "mac.h"
#include "wpan-dev.h"

extern NOS_MAC nos_mac[];
extern WPAN_DEV wpan_dev[];

/**
 * AT86RF230 has a single frame buffer. However, frame's octet positions of the
 * frame buffer are different with Tx and Rx cases. PHR has to be written to the
 * frame buffer before transmit. But PHR is not written to the frame buffer after
 * reception. So rf230_peek_received_frame() should read out the received frame,
 * and write it again including appropriate its PHR.
 */
BOOL rf230_peek_received_frame(UINT8 dev_id, 
                               UINT8 *seq, 
                               UINT16 *dst, 
                               UINT16 *src)
{
    struct glossy_ctrl *g;
    BOOL rcvd_crc;
    UINT8 id_buf[2];
    UINT8 i;

    g = (struct glossy_ctrl *) nos_mac[dev_id].mac_ctrl;
    rf230_read_frame_buf(dev_id, &g->rx_frame_buf);
    if (!g->rx_frame_buf.crc_ok)
    {
        return FALSE;
    }
    
    if (seq)
        *seq = g->rx_frame_buf.buf[2];

    if (dst)
        *dst = g->rx_frame_buf.buf[5] + (g->rx_frame_buf.buf[6] << 8);

    if (src)
        *src = g->rx_frame_buf.buf[7] + (g->rx_frame_buf.buf[8] << 8);

    // Write the read frame to the frame buffer with PHR.
    rf230_request_cs(dev_id);
    rf230_spi(dev_id, 0x60, NULL);
    rf230_spi(dev_id, g->rx_frame_buf.len + 2, NULL);
    for (i = 0; i < g->rx_frame_buf.len; i++)
    {
        rf230_spi(dev_id, g->rx_frame_buf.buf[i], NULL);
    }
    rf230_release_cs(dev_id);
    
    wpan_dev[dev_id].c.rf230.glossy_forward_ready = TRUE;
    
    return TRUE;
}

#endif //AT86RF230_M && GLOSSY_M
