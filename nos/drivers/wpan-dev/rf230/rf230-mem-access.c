// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * AT86RF230 memory access
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 11.
 */

#include "rf230-mem-access.h"
#ifdef AT86RF230_M
#include "rf230.h"
#include "rf230-interface.h"
#include "rf230-const.h"
#include "wpan-dev.h"

static const INT8 rssi_base_val[] = { -100, -100, -98, -98, -97 };
extern WPAN_DEV wpan_dev[];

UINT8 rf230_reg_read(UINT8 dev_id, UINT8 reg)
{
    UINT8 ret_value = 0;

    reg = (0x80) | ((0x3f) & reg); // read access: "10"+reg_addr[5:0]]

    rf230_request_cs(dev_id);
    rf230_spi(dev_id, reg, NULL);
    rf230_spi(dev_id, 0, &ret_value);
    rf230_release_cs(dev_id);

    return ret_value;
}

UINT8 rf230_subreg_read(UINT8 dev_id, UINT8 reg, UINT8 mask, UINT8 position)
{
    UINT8 register_value;

    //Read current register value and mask out subregister.
    register_value = rf230_reg_read(dev_id, reg);
    register_value &= mask;
    register_value >>= position; //Align subregister value.

    return register_value;
}

UINT8 rf230_reg_write(UINT8 dev_id, UINT8 reg, UINT8 value)
{
    UINT8 phy_status;
    reg = (0xc0) | ((0x3f) & reg); // write access: "11"+reg_addr[5:0]

    rf230_request_cs(dev_id);
    rf230_spi(dev_id, reg, &phy_status);
    rf230_spi(dev_id, value, NULL);
    rf230_release_cs(dev_id);

    return phy_status;
}

// value is not aligned.
UINT8 rf230_subreg_write(UINT8 dev_id, UINT8 reg, UINT8 mask, UINT8 position, UINT8 value)
{
    //Read current register value and mask area outside the subregister.
    UINT8 register_value = rf230_reg_read(dev_id, reg);
    register_value &= ~mask;

    //Start preparing the new subregister value. shift in place and mask.
    value <<= position;
    value &= mask;

    value |= register_value; //Set the new subregister value.

    //Write the modified register value.
    return rf230_reg_write(dev_id, reg, value); //return PHY_STATUS
}

void rf230_sram_write(UINT8 dev_id, UINT8 addr, const UINT8 *buf, UINT8 size)
{
    UINT8 i;
    
    rf230_request_cs(dev_id);
    rf230_spi(dev_id, 0x40, NULL); //SRAM Write
    rf230_spi(dev_id, addr, NULL);
    for (i = 0; i < size; i++)
    {
        rf230_spi(dev_id, buf[i], NULL);
    }
    rf230_release_cs(dev_id);
}

void rf230_sram_read(UINT8 dev_id, UINT8 addr, UINT8 *buf, UINT8 size)
{
    UINT8 i;
    
    rf230_request_cs(dev_id);
    rf230_spi(dev_id, 0x00, NULL); //SRAM Read
    rf230_spi(dev_id, addr, NULL);
    for (i = 0; i < size; i++)
    {
        rf230_spi(dev_id, 0, &buf[i]);
    }
    rf230_release_cs(dev_id);
}

ERROR_T rf230_write_frame_buf(UINT8 dev_id, const UINT8 *buf)
{
    UINT8 i;

#ifdef AT86RF230_DEBUG
    printf("%s()-begins\n", __FUNCTION__);
#endif

    rf230_request_cs(dev_id);
    rf230_spi(dev_id, 0x60, NULL);
    rf230_spi(dev_id, buf[0], NULL);

    // '- 2' means FCS. FCS will be added in CC2420 automatically.
    for (i = 0; i < buf[0] - 2; i++)
    {
        rf230_spi(dev_id, buf[i + 1], NULL);
    }
    rf230_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T rf230_read_frame_buf(UINT8 dev_id, struct wpan_rx_frame *f)
{
    UINT8 len, i;

    rf230_request_cs(dev_id);
    rf230_spi(dev_id, 0x20, NULL);
    rf230_spi(dev_id, 0, &len);

    if (len < IEEE_802_15_4_ACK_SIZE ||
        len > IEEE_802_15_4_FRAME_SIZE)
    {
        rf230_release_cs(dev_id);
        return ERROR_FAIL;
    }

    for (i = 0; i < len; i++)
    {
        rf230_spi(dev_id, 0, &f->buf[i]);
    }

    f->len = len - 2;

    rf230_spi(dev_id, 0, &f->lqi);
    rf230_release_cs(dev_id);

    f->crc_ok = rf230_subreg_read(dev_id, RF230_SREG_RX_CRC_VALID);
    f->rssi = rf230_reg_read(dev_id, RF230_REG_PHY_ED_LEVEL) - 91;
    
    return ERROR_SUCCESS;
}

#endif //AT86RF230_M
