// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2420-Kmote interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 17.
 */

#ifndef CC2420_INTERFACE_H
#define CC2420_INTERFACE_H

#include "kconf.h"
#ifdef CC2420_M
#include "nos_common.h"
#include "errorcodes.h"

void cc2420_interface_init(UINT8 id);
ERROR_T cc2420_request_cs(UINT8 id);
ERROR_T cc2420_release_cs(UINT8 id);
ERROR_T cc2420_spi(UINT8 id, UINT8 txdata, UINT8 *rxdata);

/**
 * Enable the voltage regulator and reset the chip.
 *
 * @param[in] id Chip ID
 * @return ERROR_SUCCESS      Operation success.
 *         ERROR_INVALID_ARGS Invalid @p id.
 */
ERROR_T cc2420_on(UINT8 id);

/**
 * Disable the voltage regulator.
 *
 * @param[in] id Chip ID
 * @return ERROR_SUCCESS      Operation success.
 *         ERROR_INVALID_ARGS Invalid @p id.
 */
ERROR_T cc2420_off(UINT8 id);

ERROR_T cc2420_reset(UINT8 id);
ERROR_T cc2420_enable_fifop_intr(UINT8 id, BOOL enable);
ERROR_T cc2420_clear_fifop_intr(UINT8 id);
ERROR_T cc2420_check_fifop_intr(UINT8 id, BOOL *flag_is_set);

enum
{
    CC2420_FIFOP,
    CC2420_FIFO,
    CC2420_SFD,
    CC2420_CCA,
};
ERROR_T cc2420_get_pin_state(UINT8 id, UINT8 pin, BOOL *pin_is_set);

#endif //CC2420_M
#endif //CC2420_INTERFACE_H
