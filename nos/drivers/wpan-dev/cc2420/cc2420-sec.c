// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2420 inline security function
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 17.
 */

#include "cc2420-sec.h"

#ifdef CC2420_INLINE_SECURITY_M

extern CC2420 cc2420[CC2420_DEV_CNT];

enum nonce_index
{
    CC2420_NONCE_IDX_BLOCK_COUNTER  = 0,
    CC2420_NONCE_IDX_SEC_LEVEL      = 2,
    CC2420_NONCE_IDX_FRAME_COUNTER  = 3,
    CC2420_NONCE_IDX_SRC_ADDR       = 7,
    CC2420_NONCE_IDX_FLAGS          = 15,
};

void cc2420_sec_init(UINT8 id)
{
    memset(cc2420[id].sec_txnonce, 0, 16);
    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_BLOCK_COUNTER] = 0x01;
    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_FLAGS] = 0x01;

    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_SRC_ADDR+0] = cc2420[id].eui64[7];
    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_SRC_ADDR+1] = cc2420[id].eui64[6];
    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_SRC_ADDR+2] = cc2420[id].eui64[5];
    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_SRC_ADDR+3] = cc2420[id].eui64[4];
    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_SRC_ADDR+4] = cc2420[id].eui64[3];
    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_SRC_ADDR+5] = cc2420[id].eui64[2];
    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_SRC_ADDR+6] = cc2420[id].eui64[1];
    cc2420[id].sec_txnonce[CC2420_NONCE_IDX_SRC_ADDR+7] = cc2420[id].eui64[0];
}

void cc2420_set_sec_key(UINT8 idx, const UINT8 *key)
{
    if (idx == 0)
    {
        chip_status.key0_in_use = TRUE;
        cc2420_write_ram(id, key, CC2420RAM_KEY0, 16, FALSE);
    }
    else if (idx == 1)
    {
        chip_status.key1_in_use = TRUE;
        cc2420_write_ram(id, key, CC2420RAM_KEY1, 16, FALSE);
    }
}

ERROR_T cc2420_secure_frame(NOS_MAC_TX_INFO *tx_info)
{
    UINT16 secctrl;
    UINT16 sec_level;
    UINT8 mic_len;

    if (tx_info->sec == IEEE_802_15_4_SEC_NONE)
    {
        sec_level = CC2420_SECCTRL0_NO_SECURITY;
    }
    else if (tx_info->sec == IEEE_802_15_4_SEC_CBC_MAC_4)
    {
        secctrl = 0;
        cc2420_set_register(id, CC2420_REG_SECCTRL1, secctrl);

        sec_level = CC2420_SECCTRL0_CBC_MAC;
    }
    else if (tx_info->sec == IEEE_802_15_4_SEC_CBC_MAC_8)
    {
        secctrl = 0;
        cc2420_set_register(id, CC2420_REG_SECCTRL1, secctrl);

        sec_level = CC2420_SECCTRL0_CBC_MAC;
    }
    else if (tx_info->sec == IEEE_802_15_4_SEC_CBC_MAC_16)
    {
        secctrl = 0;
        cc2420_set_register(id, CC2420_REG_SECCTRL1, secctrl);

        sec_level = CC2420_SECCTRL0_CBC_MAC;
    }
    else if (tx_info->sec == IEEE_802_15_4_SEC_CTR)
    {
        // To skip header being encrypted.
        secctrl = tx_info->header_length + (tx_info->header_length << 8);
        cc2420_set_register(id, CC2420_REG_SECCTRL1, secctrl);

        sec_level = CC2420_SECCTRL0_CTR;
    }
    else if (tx_info->sec == IEEE_802_15_4_SEC_CCM_4)
    {
        // To skip header being encrypted.
        secctrl = tx_info->header_length + (tx_info->header_length << 8);
        cc2420_set_register(id, CC2420_REG_SECCTRL1, secctrl);

        sec_level = CC2420_SECCTRL0_CCM;
    }
    else if (tx_info->sec == IEEE_802_15_4_SEC_CCM_8)
    {
        // To skip header being encrypted.
        secctrl = tx_info->header_length + (tx_info->header_length << 8);
        cc2420_set_register(id, CC2420_REG_SECCTRL1, secctrl);

        sec_level = CC2420_SECCTRL0_CCM;
    }
    else if (tx_info->sec == IEEE_802_15_4_SEC_CCM_16)
    {
        // To skip header being encrypted.
        secctrl = tx_info->header_length + (tx_info->header_length << 8);
        cc2420_set_register(id, CC2420_REG_SECCTRL1, secctrl);

        sec_level = CC2420_SECCTRL0_CCM;
    }
    else
    {
        return ERROR_INVALID_ARGS;
    }

    mic_len = (tx_info->sec_mic_length == 0) ?
        1 : (tx_info->sec_mic_length - 2) / 2;
    secctrl = (sec_level << CC2420_SECCTRL0_SEC_MODE_IDX) |
        (mic_len << CC2420_SECCTRL0_SEC_M_IDX) |
        (0 << CC2420_SECCTRL0_TXKEYSEL_IDX) |
        CC2420_SECCTRL0_SEC_CBC_HEAD;
    cc2420_set_register(id, CC2420_REG_SECCTRL0, secctrl);
#ifdef CC2420_DEBUG
    printf("\nSECCTRL:%x\n", secctrl);
#endif /* CC2420_DEBUG */

    if (tx_info->sec != IEEE_802_15_4_SEC_NONE)
    {
        UINT16 fcf;
        UINT8 tmp_key[16];

        fcf = tx_info->header[0] + ((UINT16) tx_info->header[1] << 8);
#ifdef CC2420_DEBUG
        printf("\nfcf:%x, sechdr idx:%u", fcf, FRAME_IDX_SECHDR(fcf));
#endif /* CC2420_DEBUG */
        cc2420_sec_txnonce[CC2420_NONCE_IDX_SEC_LEVEL] = tx_info->sec;
        cc2420_sec_txnonce[CC2420_NONCE_IDX_FRAME_COUNTER] =
            tx_info->header[FRAME_IDX_SECHDR(fcf) + 1];
        cc2420_sec_txnonce[CC2420_NONCE_IDX_FRAME_COUNTER + 1] =
            tx_info->header[FRAME_IDX_SECHDR(fcf) + 2];
        cc2420_sec_txnonce[CC2420_NONCE_IDX_FRAME_COUNTER + 2] =
            tx_info->header[FRAME_IDX_SECHDR(fcf) + 3];
        cc2420_sec_txnonce[CC2420_NONCE_IDX_FRAME_COUNTER + 3] =
            tx_info->header[FRAME_IDX_SECHDR(fcf) + 4];
        cc2420_write_ram(id, cc2420_sec_txnonce, CC2420RAM_TXNONCE, 16, TRUE);

#ifdef CC2420_DEBUG
        printf("\n");
        for (fcf = 0; fcf < 16; fcf++)
            printf("%x ", cc2420_sec_txnonce[fcf]);
#endif /* CC2420_DEBUG */

        if (ieee_802_15_4_get_sec_key(tx_info->sec_key_idx, tmp_key) == NULL)
            return ERROR_802154_INVALID_KEY_IDX;

        cc2420_write_ram(id, tmp_key, CC2420RAM_KEY0, 16, FALSE);
    }

    return ERROR_SUCCESS;
}

ERROR_T cc2420_unsecure_frame(NOS_MAC_RXQ_ENTITY *e, UINT8 sechdr_idx,
                              UINT8 key_idx)
{
    UINT8 rxnonce[16] = {
        0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 };
    UINT16 reg;
    UINT16 sec_level;

    switch (e->header[sechdr_idx] & IEEE_802_15_4_SECCTRL_MASK_SECLEVEL)
    {
    case IEEE_802_15_4_SEC_CBC_MAC_4:
        reg = 0;
        cc2420_set_register(id, CC2420_REG_SECCTRL1, reg);
        sec_level = CC2420_SECCTRL0_CBC_MAC;
        e->sec_mic_length = 4;
        break;

    case IEEE_802_15_4_SEC_CBC_MAC_8:
        reg = 0;
        cc2420_set_register(id, CC2420_REG_SECCTRL1, reg);
        sec_level = CC2420_SECCTRL0_CBC_MAC;
        e->sec_mic_length = 8;
        break;

    case IEEE_802_15_4_SEC_CBC_MAC_16:
        reg = 0;
        cc2420_set_register(id, CC2420_REG_SECCTRL1, reg);
        sec_level = CC2420_SECCTRL0_CBC_MAC;
        e->sec_mic_length = 16;
        break;

    case IEEE_802_15_4_SEC_CTR:
        // To skip header being decrypted.
        reg = e->header_length + (e->header_length << 8);
        cc2420_set_register(id, CC2420_REG_SECCTRL1, reg);
        sec_level = CC2420_SECCTRL0_CTR;
        e->sec_mic_length = 0;
        break;

    case IEEE_802_15_4_SEC_CCM_4:
        // To skip header being decrypted.
        reg = e->header_length + (e->header_length << 8);
        cc2420_set_register(id, CC2420_REG_SECCTRL1, reg);
        sec_level = CC2420_SECCTRL0_CCM;
        e->sec_mic_length = 4;
        break;

    case IEEE_802_15_4_SEC_CCM_8:
        // To skip header being decrypted.
        reg = e->header_length + (e->header_length << 8);
        cc2420_set_register(id, CC2420_REG_SECCTRL1, reg);
        sec_level = CC2420_SECCTRL0_CCM;
        e->sec_mic_length = 8;
        break;

    case IEEE_802_15_4_SEC_CCM_16:
        // To skip header being decrypted.
        reg = e->header_length + (e->header_length << 8);
        cc2420_set_register(id, CC2420_REG_SECCTRL1, reg);
        sec_level = CC2420_SECCTRL0_CCM;
        e->sec_mic_length = 16;
        break;

    default: //IEEE_802_15_4_SEC_NONE
        sec_level = CC2420_SECCTRL0_NO_SECURITY;
        e->sec_mic_length = 0;
        break;
    }

    reg = (sec_level << CC2420_SECCTRL0_SEC_MODE_IDX) |
        (((e->sec_mic_length == 0) ? 1 : ((e->sec_mic_length - 2) / 2)) << CC2420_SECCTRL0_SEC_M_IDX) |
        (0 << CC2420_SECCTRL0_RXKEYSEL_IDX) |
        CC2420_SECCTRL0_SEC_CBC_HEAD |
        CC2420_SECCTRL0_RXFIFO_PROTECTION;
    cc2420_set_register(id, CC2420_REG_SECCTRL0, reg);

#ifdef CC2420_DEBUG
    printf("\nSECCTRL:%x\n", reg);
#endif /* CC2420_DEBUG */

    if ((e->header[sechdr_idx] & IEEE_802_15_4_SECCTRL_MASK_SECLEVEL) !=
        IEEE_802_15_4_SEC_NONE)
    {
        UINT16 fcf;
        UINT8 status;
        UINT8 loop;
        UINT8 tmp_key[16];

        rxnonce[CC2420_NONCE_IDX_FRAME_COUNTER] = e->header[sechdr_idx + 1];
        rxnonce[CC2420_NONCE_IDX_FRAME_COUNTER + 1] = e->header[sechdr_idx + 2];
        rxnonce[CC2420_NONCE_IDX_FRAME_COUNTER + 2] = e->header[sechdr_idx + 3];
        rxnonce[CC2420_NONCE_IDX_FRAME_COUNTER + 3] = e->header[sechdr_idx + 4];

        fcf = e->header[0] + ((UINT16) e->header[1] << 8);
        if (FRAME_CONTAINS_LONGSRC(fcf))
        {
            memcpy(&rxnonce[CC2420_NONCE_IDX_SRC_ADDR],
                   &e->header[FRAME_IDX_SRCADDR(fcf)], 8);
        }
        else
        {
            memset(&rxnonce[CC2420_NONCE_IDX_SRC_ADDR], 0, 8);
        }
        rxnonce[CC2420_NONCE_IDX_SEC_LEVEL] =
            e->header[sechdr_idx] & IEEE_802_15_4_SECCTRL_MASK_SECLEVEL;
        cc2420_write_ram(id, rxnonce, CC2420RAM_RXNONCE, 16, TRUE);

        if (ieee_802_15_4_get_sec_key(key_idx, tmp_key) == NULL)
            return ERROR_802154_INVALID_KEY_IDX;

        cc2420_write_ram(id, tmp_key, CC2420RAM_KEY0, 16, FALSE);

        cc2420_command(id, CC2420_CMD_SRXDEC);
        loop = 0;
        do {
#ifdef CC2420_DEBUG
            nos_uart_putc(STDIO, '*');
#endif /* CC2420_DEBUG */
            nos_delay_us(1);
            cc2420_command(id, CC2420_CMD_SNOP, &status);
        } while((status & (1 << CC2420_ENC_BUSY)) && (loop++ < 250));

        if (loop > 250)
        {
            // Fail.
#ifdef CC2420_DEBUG
            nos_uart_putc(STDIO, 'X');
#endif /* CC2420_DEBUG */
            cc2420_flush_rxbuf();
            return ERROR_802154_UNSECURING_FAIL;
        }
        // Flag this frame is already decrypted/authenticated.
        _BIT_CLR(e->frame_status, FRAME_IS_SECURED);
    }
    return ERROR_SUCCESS;
}
#endif /* CC2420_INLINE_SECURITY_M */
