// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2420 Driver
 *
 * @author Junkeun Song (ETRI)
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @author Jeehoon Lee (UST-ETRI)
 */

#include "cc2420.h"

#ifdef CC2420_M
#include "mac.h"
#include "wpan-dev.h"
#include "arch.h"
#include "platform.h"
#include "critical_section.h"
#include "cc2420-interface.h"
#include "cc2420-sec.h"
#include "cc2420-access.h"
#include "cc2420-const.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#if defined(COOJA_SIM_M) && defined(BATTERY_MONITOR_M)
#include "bat-monitor.h"
#endif //COOJA_SIM_M && BATTERY_MONITOR_M

#define SYMBOL_DURATION 16 // us. (32chips * 0.5us)

#if defined CC2420_INIT_CHANNEL_11
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_11
#elif defined CC2420_INIT_CHANNEL_12
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_12
#elif defined CC2420_INIT_CHANNEL_13
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_13
#elif defined CC2420_INIT_CHANNEL_14
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_14
#elif defined CC2420_INIT_CHANNEL_15
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_15
#elif defined CC2420_INIT_CHANNEL_16
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_16
#elif defined CC2420_INIT_CHANNEL_17
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_17
#elif defined CC2420_INIT_CHANNEL_18
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_18
#elif defined CC2420_INIT_CHANNEL_19
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_19
#elif defined CC2420_INIT_CHANNEL_20
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_20
#elif defined CC2420_INIT_CHANNEL_21
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_21
#elif defined CC2420_INIT_CHANNEL_22
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_22
#elif defined CC2420_INIT_CHANNEL_23
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_23
#elif defined CC2420_INIT_CHANNEL_24
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_24
#elif defined CC2420_INIT_CHANNEL_25
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_25
#elif defined CC2420_INIT_CHANNEL_26
#define CC2420_INIT_CHANNEL CC2420_CHANNEL_26
#else
#error "Unknown channel."
#endif

extern WPAN_DEV wpan_dev[WPAN_DEV_CNT];

static void cc2420_sleep(UINT8 dev_id)
{
    cc2420_request_cs(dev_id);
    cc2420_spi(dev_id, CC2420_CMD_SRFOFF, NULL);
    cc2420_release_cs(dev_id);
}

static void cc2420_wakeup(UINT8 dev_id)
{
    cc2420_request_cs(dev_id);
    cc2420_spi(dev_id, CC2420_CMD_SRXON, NULL);
    cc2420_spi(dev_id, CC2420_CMD_SFLUSHTX, NULL);
    cc2420_spi(dev_id, CC2420_CMD_SFLUSHRX, NULL);
    cc2420_spi(dev_id, CC2420_CMD_SFLUSHRX, NULL);
    cc2420_release_cs(dev_id);
}

static void cc2420_set_pan_id(UINT8 dev_id)
{
#ifdef BYTE_ORDER_LITTLE_ENDIAN
    cc2420_write_ram(dev_id, (UINT8 *) &wpan_dev[dev_id].pan_id,
                     CC2420_RAM_PANID, 2, TRUE);
#else
    cc2420_write_ram(dev_id, (UINT8 *) &wpan_dev[dev_id].pan_id,
                     CC2420_RAM_PANID, 2, FALSE);
#endif
}

static void cc2420_set_short_id(UINT8 dev_id)
{
#ifdef BYTE_ORDER_LITTLE_ENDIAN
    cc2420_write_ram(dev_id, (UINT8 *) &wpan_dev[dev_id].short_id,
                     CC2420_RAM_SHORTADDR, 2, TRUE);
#else
    cc2420_write_ram(dev_id, (UINT8 *) &wpan_dev[dev_id].short_id,
                     CC2420_RAM_SHORTADDR, 2, FALSE);
#endif
}

static void cc2420_set_eui64(UINT8 dev_id)
{
    cc2420_write_ram(dev_id, wpan_dev[dev_id].eui64,
                     CC2420_RAM_IEEEADDR, 8, FALSE);
}

/**
 * Setup CC2420.
 *
 * @param[in] id  CC2420 ID to be setup.
 *
 * @note This function should be called by wpan_dev_config() and cc2420_command()
 *       with valid @p id.
 */
void cc2420_setup(UINT8 dev_id)
{
    wpan_dev[dev_id].c.cc2420.in_ovf_handle = FALSE;
    wpan_dev[dev_id].c.cc2420.ovf_rx_len = 0;

    /* Set the correlation threshold 20. Do not change. */
    cc2420_set_register(dev_id, CC2420_REG_MDMCTRL1, 0x0500);

    /* Turn off RXFIFO_PROTECTION. */
    cc2420_set_register(dev_id, CC2420_REG_SECCTRL0, 0x01C4);

    cc2420_set_register(dev_id, CC2420_REG_IOCFG0,
                        BCN_ACCEPT_REGARDLESS_PAN |
                        FIFO_ACTIVE_HIGH |
                        FIFOP_ACTIVE_HIGH |
                        SFD_ACTIVE_HIGH |
                        CCA_ACTIVE_HIGH |
                        0x7F); // Max FIFOP_THR

    
    cc2420_set_register(dev_id, CC2420_REG_MDMCTRL0,
                        REJECT_RESERVED_FRAME_TYPES |
                        PAN_COORDINATOR |
                        ENABLE_ADR_DECODE |
                        (2 << CCA_HYST_SHIFT) |
                        CCA_MODE_3 |
                        ENABLE_AUTOCRC |
                        ENABLE_AUTOACK |
                        PREAMBLE_3BYTE);

    /* Set radio channel. */
    cc2420_channel_init(dev_id, CC2420_INIT_CHANNEL);

#ifdef CC2420_INLINE_SECURITY_M
    cc2420_sec_init(dev_id);
#endif

    cc2420_wakeup(dev_id);

    // Enable FIFOP interrupt.
    cc2420_clear_fifop_intr(dev_id);
    cc2420_enable_fifop_intr(dev_id, TRUE);
}

ERROR_T cc2420_channel_init(UINT8 id, CC2420_CHANNEL ch)
{
    ERROR_T err;
    
    err = cc2420_set_register(id, CC2420_REG_FSCTRL, 0x4000 + (UINT16)ch);

    return err;
}

void cc2420_flush_rxbuf(UINT8 dev_id)
{
    cc2420_command(dev_id, CC2420_CMD_SFLUSHRX, NULL);
    cc2420_command(dev_id, CC2420_CMD_SFLUSHRX, NULL);
}

void cc2420_intr_handler(UINT8 id)
{
    BOOL stop_intr = FALSE;
    BOOL fifop_is_set, fifo_is_set;

#ifdef CC2420_DEBUG
    printf("%s() from %d\n", __FUNCTION__, id);
#endif

    cc2420_clear_fifop_intr(id);

    /* FIFOP interrupt handler. Callback function will be executed here until RX
     * queue is empty.
     */
    cc2420_get_pin_state(id, CC2420_FIFOP, &fifop_is_set);
    while(fifop_is_set)
    {
        cc2420_get_pin_state(id, CC2420_FIFO, &fifo_is_set);
        if(!fifo_is_set)
        {
            // Overflow handling swamp
        
#ifdef CC2420_DEBUG
            printf("%s()-ovf\n", __FUNCTION__);
#endif

            // to block infinite loop
            wpan_dev[id].c.cc2420.ovf_rx_len = 0;
            wpan_dev[id].c.cc2420.in_ovf_handle = TRUE;
            wpan_dev[id].notify(id, 0, &stop_intr);
            wpan_dev[id].c.cc2420.in_ovf_handle = FALSE;
            wpan_dev[id].c.cc2420.ovf_rx_len = 0;

            cc2420_flush_rxbuf(id);

            // if rx_queue is full
            if (stop_intr)
            {
                //cc2420_enable_fifop_intr(id, FALSE);
            }
#ifdef CC2420_DEBUG
            printf("%s()-?\n", __FUNCTION__);
#endif
            return;
        }
        else
        {
#ifdef CC2420_DEBUG
            printf("%s()-rx\n", __FUNCTION__);
#endif

            wpan_dev[id].notify(id, 0, &stop_intr);

            if (stop_intr)
            {
                cc2420_flush_rxbuf(id);
                //cc2420_enable_fifop_intr(id, FALSE);
                return;
            }
        }

        cc2420_get_pin_state(id, CC2420_FIFOP, &fifop_is_set);
    }
}

ERROR_T cc2420_read_frame(UINT8 id, struct wpan_rx_frame *f)
{
    cc2420_rxfifo_pop_single(id, &f->len);
    f->len -= 2; // 2 is length of FCS.

    if (f->len > 127)
    {
        if (wpan_dev[id].c.cc2420.in_ovf_handle)
        {
            wpan_dev[id].c.cc2420.ovf_rx_len =
                IEEE_802_15_4_MAX_SAFE_RX_PAYLOAD_SIZE;
        }
        return ERROR_INVALID_FRAME;
    }
    
    if (wpan_dev[id].c.cc2420.in_ovf_handle)
        wpan_dev[id].c.cc2420.ovf_rx_len += f->len;
    
    cc2420_rxfifo_pop_burst(id, f->buf, f->len);

    cc2420_rxfifo_pop_single(id, (UINT8 *) &f->rssi);
    cc2420_rxfifo_pop_single(id, (UINT8 *) &f->lqi);

    if (f->lqi & CC2420_FCS_CRC_BM)
    {
        // Get the Correlation value (7bit)
        f->lqi &= CC2420_FCS_CORR_BM;
        f->crc_ok = TRUE;
    }
    else
    {
        f->crc_ok = FALSE;
        return ERROR_CRC_ERROR;
    }

    f->secured = FALSE;

    return ERROR_SUCCESS;
}

static void cc2420_write_frame(UINT8 id, const UINT8 *buf)
{
#ifdef CC2420_INLINE_SECURITY_M
    //TODO error handling
    cc2420_secure_frame(tx);
#endif

    cc2420_txfifo_push(id, buf);
}

BOOL cc2420_rxbuf_is_empty(UINT8 id)
{
    BOOL fifop_is_set, fifo_is_set;

    cc2420_get_pin_state(id, CC2420_FIFOP, &fifop_is_set);
    if (fifop_is_set)
    {
        if (wpan_dev[id].c.cc2420.in_ovf_handle == TRUE)
        {
            /*
             * To read out IEEE_802_15_4_MAX_SAFE_RX_PAYLOAD_SIZE byte data from
             * RX queue.
             */
#ifdef CC2420_DEBUG
            printf("%s()-ovf_rx_len:%u\n", __FUNCTION__, wpan_dev[id].c.cc2420.ovf_rx_len);
#endif
            return (wpan_dev[id].c.cc2420.ovf_rx_len >=
                    IEEE_802_15_4_MAX_SAFE_RX_PAYLOAD_SIZE);
        }
        else
        {
            cc2420_get_pin_state(id, CC2420_FIFO, &fifo_is_set);
            return (!fifo_is_set);
        }
    }
    else
    {
        return TRUE;
    }
}

BOOL cc2420_cca(UINT8 id)
{
    UINT8 status; 
    BOOL cca_is_set;

    cc2420_command(id, CC2420_CMD_SNOP, &status);
    if (!_IS_SET(status, CC2420_RSSI_VALID))
        nos_delay_us(8 * SYMBOL_DURATION);

    cc2420_command(id, CC2420_CMD_SNOP, &status);

    if (_IS_SET(status, CC2420_RSSI_VALID))
    {
        cc2420_get_pin_state(id, CC2420_CCA, &cca_is_set);
        return cca_is_set;
    }
    else
    {
        return FALSE;
    }
}

ERROR_T cc2420_tx(UINT8 id, const UINT8 *buf)
{
    UINT8 foo;
    BOOL sfd_is_set;

#ifdef LPLL_MAC_M
    cc2420_command(id, CC2420_CMD_STXON);
#else

    // 8+32 symbol = LIFS
    nos_delay_us(32 * SYMBOL_DURATION);

    // TX if channel was clear for LIFS (40symbol)
    cc2420_command(id, CC2420_CMD_STXONCCA, NULL);
#endif // LPLL_MAC_M

    cc2420_command(id, CC2420_CMD_SNOP, &foo);
    cc2420_command(id, CC2420_CMD_SNOP, &foo); // for msp. We should call this twice. dunno why.

    if (foo & (1 << CC2420_TX_ACTIVE)) //is CC2420 in TX state?
    {
        // Wait for initiating Tx.
        do { cc2420_get_pin_state(id, CC2420_SFD, &sfd_is_set); } while (!sfd_is_set);

        // ...> Transmitting ...>

        // Wait for Tx completion.
        do { cc2420_get_pin_state(id, CC2420_SFD, &sfd_is_set); } while (sfd_is_set);

#if defined(COOJA_SIM_M) && defined(BATTERY_MONITOR_M)
        nos_battery_discharge(buf[0]);
#endif
        return ERROR_SUCCESS;
    }
    return ERROR_TXFAIL_CH_BUSY;
}

ERROR_T cc2420_wait_for_ack(UINT8 id, UINT8 seq)
{
    struct wpan_rx_frame rcvd;
    BOOL fifop_flag_is_set;
    ERROR_T err;

    cc2420_flush_rxbuf(id);

    cc2420_enable_fifop_intr(id, FALSE);
    cc2420_clear_fifop_intr(id);
    err = ERROR_FAIL;
    
    nos_delay_us(54 * SYMBOL_DURATION);

    cc2420_check_fifop_intr(id, &fifop_flag_is_set);
    if (fifop_flag_is_set)
    {
        if (cc2420_read_frame(id, &rcvd) == ERROR_SUCCESS)
        {
            // Check whether it's an ACK for me or not.
            if (FRAME_IS_ACK(rcvd.buf[0] + (rcvd.buf[1] << 8)) &&
                rcvd.buf[2] == seq)
            {
                err = ERROR_SUCCESS; // valid Ack reception
            }
            else
            {
                err = ERROR_INVALID_FRAME;
            }
        }
    }

    cc2420_clear_fifop_intr(id);
    cc2420_enable_fifop_intr(id, TRUE);

    cc2420_flush_rxbuf(id);
    return err;
}

static const struct wpan_dev_cmdset cc2420_api =
{
    cc2420_setup,
    cc2420_read_frame,
    cc2420_write_frame,
    cc2420_rxbuf_is_empty,
    cc2420_flush_rxbuf,
    cc2420_sleep,
    cc2420_wakeup,
    cc2420_tx,
    cc2420_wait_for_ack,
    cc2420_cca,
    cc2420_set_pan_id,
    cc2420_set_short_id,
    cc2420_set_eui64,
};

void cc2420_init(UINT8 id)
{
    UINT8 st;
    
    wpan_dev[id].hw_autotx = FALSE;
    wpan_dev[id].sec_support = TRUE;
    wpan_dev[id].addrdec_support = TRUE;
    wpan_dev[id].symbol_duration = SYMBOL_DURATION;
    wpan_dev[id].cmd = &cc2420_api;
    cc2420_on(id);
    
    cc2420_reset(id);
    cc2420_set_register(id, CC2420_REG_MAIN, 0x0000);
    cc2420_set_register(id, CC2420_REG_MAIN, 0xF800);

    cc2420_command(id, CC2420_CMD_SXOSCON, NULL);

    // Wait until the crystal oscillator to be stable.
    do
    {
        cc2420_command(id, CC2420_CMD_SNOP, &st);
    } while (!(st & (1 << CC2420_XOSC16M_STABLE)));
}

#endif // CC2420_M
