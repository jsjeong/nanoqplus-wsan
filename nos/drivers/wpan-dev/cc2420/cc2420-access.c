// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2420 Registers, FIFOs, RAM access
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 17.
 */

#include "cc2420-access.h"
#ifdef CC2420_M
#include "cc2420-interface.h"

enum cc2420_fifo
{
    CC2420_TXFIFO = 0x3E,
    CC2420_RXFIFO = 0x3F,
};

ERROR_T cc2420_set_register(UINT8 id, UINT8 reg, UINT16 value)
{
    cc2420_request_cs(id);
    cc2420_spi(id, reg, NULL);
    cc2420_spi(id, (value >> 8) & 0xff, NULL);
    cc2420_spi(id, (value >> 0) & 0xff, NULL);
    cc2420_release_cs(id);

    return ERROR_SUCCESS;
}

ERROR_T cc2420_get_register(UINT8 id, UINT8 reg, UINT16 *value)
{
    UINT8 rxd[2];

    if (value == NULL)
        return ERROR_INVALID_ARGS;
    
    cc2420_request_cs(id);
    cc2420_spi(id, reg | 0x40, NULL);
    cc2420_spi(id, 0, &rxd[0]);
    cc2420_spi(id, 0, &rxd[1]);
    cc2420_release_cs(id);

    *value = ((UINT16) rxd[0] << 8) + rxd[1];
    return ERROR_SUCCESS;
}

ERROR_T cc2420_command(UINT8 id, UINT8 cmd, UINT8 *status)
{
    UINT8 s;
    
    cc2420_request_cs(id);
    cc2420_spi(id, cmd, &s);
    cc2420_release_cs(id);

    if (status != NULL)
        *status = s;
    
    return ERROR_SUCCESS;
}

ERROR_T cc2420_write_ram(UINT8 id, const UINT8 *buf, UINT16 ram, UINT8 n, BOOL asc)
{
    INT16 i;

    cc2420_request_cs(id);
    cc2420_spi(id, (0x80 | (ram & 0x7F)), NULL);
    cc2420_spi(id, (ram >> 1) & 0xC0, NULL);
    
    if (asc)
    {
        for (i = 0; i < n; ++i)
            cc2420_spi(id, buf[i], NULL);
    }
    else
    {
        for (i= (n - 1); i >= 0; --i)
            cc2420_spi(id, buf[i], NULL);
    }
    cc2420_release_cs(id);
    return ERROR_SUCCESS;
}

ERROR_T cc2420_rxfifo_pop_single(UINT8 id, UINT8 *rxdata)
{
    if (rxdata == NULL)
        return ERROR_INVALID_ARGS;
    
    cc2420_request_cs(id);
    cc2420_spi(id, CC2420_RXFIFO | 0x40, NULL);
    cc2420_spi(id, 0, rxdata);
    cc2420_release_cs(id);

    return ERROR_SUCCESS;
}

ERROR_T cc2420_rxfifo_pop_burst(UINT8 id, UINT8 *buf, UINT8 n)
{
    INT16 i;

    if (buf == NULL || n == 0)
    {
        return ERROR_INVALID_ARGS;
    }

    cc2420_request_cs(id);
    cc2420_spi(id, CC2420_RXFIFO | 0x40, NULL);
    for (i = 0; i < n; i++)
        cc2420_spi(id, 0, &buf[i]);
    cc2420_release_cs(id);

    return ERROR_SUCCESS;
}

ERROR_T cc2420_txfifo_push(UINT8 id, const UINT8 *buf)
{
    UINT8 i;
    
    cc2420_request_cs(id);
    cc2420_spi(id, CC2420_CMD_SFLUSHTX, NULL);
    cc2420_spi(id, CC2420_TXFIFO, NULL);

    cc2420_spi(id, buf[0], NULL);

    // '- 2' means FCS. FCS will be added in CC2420 automatically.
    for (i = 0; i < buf[0] - 2; i++)
        cc2420_spi(id, buf[i + 1], NULL);
    
    cc2420_release_cs(id);

    return ERROR_SUCCESS;
}

#endif //CC2420_M
