// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2420 Constants
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 8. 7.
 */

#ifndef CC2420_CONST_H
#define CC2420_CONST_H

enum cc2420_iocfg0
{
    BCN_ACCEPT_MATCHED_PAN =    (0 << 11),
    BCN_ACCEPT_REGARDLESS_PAN = (1 << 11),
    FIFO_ACTIVE_HIGH =          (0 << 10),
    FIFO_ACTIVE_LOW =           (1 << 10),
    FIFOP_ACTIVE_HIGH =         (0 << 9),
    FIFOP_ACTIVE_LOW =          (1 << 9),
    SFD_ACTIVE_HIGH =           (0 << 8),
    SFD_ACTIVE_LOW =            (1 << 8),
    CCA_ACTIVE_HIGH =           (0 << 7),
    CCA_ACTIVE_LOW =            (1 << 7),
};

enum cc2420_mdmctrl0
{
    REJECT_RESERVED_FRAME_TYPES = (0 << 13),
    ACCEPT_RESERVED_FRAME_TYPES = (1 << 13),
    PAN_COORDINATOR = (1 << 12),
    DISABLE_ADR_DECODE = (0 << 11),
    ENABLE_ADR_DECODE = (1 << 11),
    CCA_HYST_SHIFT = 8,
    CCA_MODE_1 = (1 << 6),
    CCA_MODE_2 = (2 << 6),
    CCA_MODE_3 = (3 << 6),
    DISABLE_AUTOCRC = (0 << 5),
    ENABLE_AUTOCRC = (1 << 5),
    DISABLE_AUTOACK = (0 << 4),
    ENABLE_AUTOACK = (1 << 4),
    PREAMBLE_1BYTE = (0 << 0),
    PREAMBLE_2BYTE = (1 << 0),
    PREAMBLE_3BYTE = (2 << 0),
    PREAMBLE_4BYTE = (3 << 0),
    PREAMBLE_5BYTE = (4 << 0),
    PREAMBLE_6BYTE = (5 << 0),
    PREAMBLE_7BYTE = (6 << 0),
    PREAMBLE_8BYTE = (7 << 0),
    PREAMBLE_9BYTE = (8 << 0),
    PREAMBLE_10BYTE = (9 << 0),
    PREAMBLE_11BYTE = (10 << 0),
    PREAMBLE_12BYTE = (11 << 0),
    PREAMBLE_13BYTE = (12 << 0),
    PREAMBLE_14BYTE = (13 << 0),
    PREAMBLE_15BYTE = (14 << 0),
    PREAMBLE_16BYTE = (15 << 0),
};

#endif //CC2420_CONST_H
