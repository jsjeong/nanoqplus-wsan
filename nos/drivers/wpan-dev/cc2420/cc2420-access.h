// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC2420 Registers, FIFOs, RAM access
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 17.
 */

#ifndef CC2420_ACCESS_H
#define CC2420_ACCESS_H

#include "kconf.h"
#ifdef CC2420_M
#include "nos_common.h"
#include "errorcodes.h"
#include "ieee-802-15-4.h"

// Registers
enum cc2420_register
{
    CC2420_REG_MAIN             = 0x10,
    CC2420_REG_MDMCTRL0         = 0x11,
    CC2420_REG_MDMCTRL1         = 0x12,
    CC2420_REG_RSSI             = 0x13,
    CC2420_REG_SYNCWORD         = 0x14,
    CC2420_REG_TXCTRL           = 0x15,
    CC2420_REG_RXCTRL0          = 0x16,
    CC2420_REG_RXCTRL1          = 0x17,
    CC2420_REG_FSCTRL           = 0x18,
    CC2420_REG_SECCTRL0         = 0x19,
    CC2420_REG_SECCTRL1         = 0x1A,
    CC2420_REG_BATTMON          = 0x1B,
    CC2420_REG_IOCFG0           = 0x1C,
    CC2420_REG_IOCFG1           = 0x1D,
    CC2420_REG_MANFIDL          = 0x1E,
    CC2420_REG_MANFIDH          = 0x1F,
    CC2420_REG_FSMTC            = 0x20,
    CC2420_REG_MANAND           = 0x21,
    CC2420_REG_MANOR            = 0x22,
    CC2420_REG_AGCCTRL          = 0x23,
    CC2420_REG_AGCTST0          = 0x24,
    CC2420_REG_AGCTST1          = 0x25,
    CC2420_REG_AGCTST2          = 0x26,
    CC2420_REG_FSTST0           = 0x27,
    CC2420_REG_FSTST1           = 0x28,
    CC2420_REG_FSTST2           = 0x29,
    CC2420_REG_FSTST3           = 0x2A,
    CC2420_REG_RXBPFTST         = 0x2B,
    CC2420_REG_FSMSTATE         = 0x2C,
    CC2420_REG_ADCTST           = 0x2D,
    CC2420_REG_DACTST           = 0x2E,
    CC2420_REG_TOPTST           = 0x2F,
    CC2420_REG_RESERVED         = 0x30,
};

ERROR_T cc2420_set_register(UINT8 id, UINT8 reg, UINT16 value);
ERROR_T cc2420_get_register(UINT8 id, UINT8 reg, UINT16 *value);

enum cc2420_commands
{
    CC2420_CMD_SNOP             = 0x00,
    CC2420_CMD_SXOSCON          = 0x01,
    CC2420_CMD_STXCAL           = 0x02,
    CC2420_CMD_SRXON            = 0x03,
    CC2420_CMD_STXON            = 0x04,
    CC2420_CMD_STXONCCA         = 0x05,
    CC2420_CMD_SRFOFF           = 0x06,
    CC2420_CMD_SXOSCOFF         = 0x07,
    CC2420_CMD_SFLUSHRX         = 0x08,
    CC2420_CMD_SFLUSHTX         = 0x09,
    CC2420_CMD_SACK             = 0x0A,
    CC2420_CMD_SACKPEND         = 0x0B,
    CC2420_CMD_SRXDEC           = 0x0C,
    CC2420_CMD_STXENC           = 0x0D,
    CC2420_CMD_SAES             = 0x0E,
};

ERROR_T cc2420_command(UINT8 id, UINT8 cmd, UINT8 *status);

enum cc2420_ram_addr
{
    CC2420_RAM_TXFIFO        = 0x000,
    CC2420_RAM_RXFIFO        = 0x080,
    CC2420_RAM_KEY0          = 0x100,
    CC2420_RAM_RXNONCE       = 0x110,
    CC2420_RAM_SABUF         = 0x120,
    CC2420_RAM_KEY1          = 0x130,
    CC2420_RAM_TXNONCE       = 0x140,
    CC2420_RAM_CBCSTATE      = 0x150,
    CC2420_RAM_IEEEADDR      = 0x160,
    CC2420_RAM_PANID         = 0x168,
    CC2420_RAM_SHORTADDR     = 0x16A,
};

ERROR_T cc2420_write_ram(UINT8 id, const UINT8 *buf, UINT16 ram, UINT8 n, BOOL asc);

ERROR_T cc2420_rxfifo_pop_single(UINT8 id, UINT8 *rxdata);
ERROR_T cc2420_rxfifo_pop_burst(UINT8 id, UINT8 *buf, UINT8 n);
ERROR_T cc2420_txfifo_push(UINT8 id, const UINT8 *buf);

#endif //CC2420_M
#endif //CC2420_ACCESS_H
