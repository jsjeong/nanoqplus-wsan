// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#ifndef CC2420_H
#define CC2420_H
#include "kconf.h"
#ifdef CC2420_M

#include "nos_common.h"
#include "platform.h"
#include "ieee-802-15-4.h"
#include "errorcodes.h"

#define RF_VREG_SET()       NOS_GPIO_ON(CC2420_VREG_EN_PORT, CC2420_VREG_EN_PIN)
#define CC2420_PIN_INIT() \
    do { \
        NOS_GPIO_INIT_OUT(CC2420_VREG_EN_PORT, CC2420_VREG_EN_PIN); \
        NOS_GPIO_INIT_OUT(CC2420_CSN_PORT, CC2420_CSN_PIN); \
        NOS_GPIO_INIT_IN(CC2420_FIFOP_PORT, CC2420_FIFOP_PIN); \
        NOS_GPIO_INIT_IN(CC2420_FIFO_PORT, CC2420_FIFO_PIN); \
        NOS_GPIO_INIT_IN(CC2420_CCA_PORT, CC2420_CCA_PIN); \
        NOS_GPIO_INIT_IN(CC2420_SFD_PORT, CC2420_SFD_PIN); \
        NOS_GPIO_INIT_OUT(CC2420_RESET_N_PORT, CC2420_RESET_N_PIN); \
    } while (0)

// Pin status
#define RF_VREG_IS_SET()        NOS_GPIO_READ(CC2420_VREG_EN_PORT, CC2420_VREG_EN_PIN)
#define RF_FIFOP_IS_SET()       NOS_GPIO_READ(CC2420_FIFOP_PORT, CC2420_FIFOP_PIN)
#define RF_FIFO_IS_SET()        NOS_GPIO_READ(CC2420_FIFO_PORT, CC2420_FIFO_PIN)
#define RF_SFD_IS_SET()         NOS_GPIO_READ(CC2420_SFD_PORT, CC2420_SFD_PIN)
#define RF_CCA_IS_SET()         NOS_GPIO_READ(CC2420_CCA_PORT, CC2420_CCA_PIN)
#define RF_RXFIFO_OVERFLOW()    ((RF_FIFOP_IS_SET()) && (!(RF_FIFO_IS_SET())))

// If RXFIFO is full (128byte), overflow is indicated.
enum { CC2420_RXFIFO_SIZE = 128 };

// MAC frame Length byte in PHY : The MSB has no meaning.
// Note that the length byte itself is not included included in the packet length
#define CC2420_GET_FRAME_LENGTH(x) \
    do { \
        x &= (0x7F);\
    } while (0)

// Reset CC2420
#define CC2420_RESET() \
    do { \
        CC2420_SETREG(CC2420_MAIN, 0x0000); \
        CC2420_SETREG(CC2420_MAIN, 0xF800); \
    } while (0)

// To save more energy, use POWER DOWN mode for long RF off state.
//	1. CC2420_STROBE(CC2420_SXOSCOFF);	// crystal oscillator off
//	2. CC2420_STROBE(CC2420_SXOSCON); 	// crystal oscillator on
//	    wait_until_xosc_stable(); // wait for the crystal oscillator to become stable
//	    CC2420_STROBE(CC2420_SFLUSHRX);

// Turn on RF
#define CC2420_SWITCH_ON()	\
    do {		\
        CC2420_SPI_ENABLE();	\
        CC2420_TX_ADDR(CC2420_SRXON);	\
        CC2420_TX_ADDR(CC2420_SFLUSHTX);	\
        CC2420_TX_ADDR(CC2420_SFLUSHRX);	\
        CC2420_TX_ADDR(CC2420_SFLUSHRX);	\
        CC2420_SPI_DISABLE();	\
    } while(0)

// Turn off RF

// Transmission Power Level (1~31)
#define CC2420_SET_TX_LEVEL(level) \
    do{ \
        if (level >= 31) \
        CC2420_SETREG (CC2420_TXCTRL, 0xA0FF); \
        else if (level <= 1) \
        CC2420_SETREG (CC2420_TXCTRL, 0xA0E0); \
        else \
        CC2420_SETREG (CC2420_TXCTRL, 0xA0E0+level); \
    }while(0)

#define CC2420_FLUSH_RXBUF() \
    do { \
        CC2420_STROBE(CC2420_SFLUSHRX); \
        CC2420_STROBE(CC2420_SFLUSHRX); \
    } while(0)

// Frame control field (16bit)  
// L byte (LSB first): Type(3) + Security Enabled(1) + Frmae Pending(1) + Ack request(1) + Intra PAN(1) + Reserved(1)
// H byte (LSB first): Reserved(2) + Destination addressing mode(2) + Reserved(2) + Source addressing mode (2)
#define CC2420_FCF_ACK_BM 0x0020
// Auto-Ack FCF
#define CC2420_ACK_FCF	0x0002

// Frame check sequence (RSSI + Corr : 16 bit)
// L byte : RSSI (signed)
// H byte (LSB first) : Correlation value (unsigned, 7bit) + CRC OK(1)
#define CC2420_FCS_CRC_BM 0x80
#define CC2420_FCS_CORR_BM 0x78


// CC2420 register constants
// Command strobes


// FIFOs

// Memory sizes
#define CC2420_RAM_SIZE         368
#define CC2420_FIFO_SIZE        128

// Status byte
#define CC2420_XOSC16M_STABLE    6
#define CC2420_TX_UNDERFLOW      5
#define CC2420_ENC_BUSY          4
#define CC2420_TX_ACTIVE         3
#define CC2420_LOCK              2
#define CC2420_RSSI_VALID        1

// Security
// SECCTRL0.SEC_MODE (security mode)
#define CC2420_SECCTRL0_SEC_MODE_IDX        0
#define CC2420_SECCTRL0_NO_SECURITY         0
#define CC2420_SECCTRL0_CBC_MAC             1
#define CC2420_SECCTRL0_CTR                 2
#define CC2420_SECCTRL0_CCM                 3

// SECCTRL0.SEC_M (number of bytes in the authentication field)
#define CC2420_SECCTRL0_SEC_M_IDX           2

// SECCTRL0.RXKEYSEL/TXKEYSEL (key selection)
#define CC2420_SECCTRL0_RXKEYSEL_IDX        5
#define CC2420_SECCTRL0_TXKEYSEL_IDX        6
#define CC2420_SECCTRL0_SAKEYSEL_IDX        7

#define CC2420_NUMBER_OF_KEYS               2

// Others
#define CC2420_SECCTRL0_SEC_CBC_HEAD        0x0100
#define CC2420_SECCTRL0_RXFIFO_PROTECTION   0x0200

/*
 * MAC Security flags definitions. Note that the bits are shifted compared to
 * the actual security flags defined by IEEE 802.15.4, please see the CC2420
 * datasheet for details.
 */
#define CC2420_CTR_FLAGS                0x42
#define CC2420_CCM_FLAGS                0x09


// Functions and Variables

enum cc2420_channel_type
{
    CC2420_CHANNEL_11 = (2405 - 2048 + ((11 - 11) * 5)),
    CC2420_CHANNEL_12 = (2405 - 2048 + ((12 - 11) * 5)),
    CC2420_CHANNEL_13 = (2405 - 2048 + ((13 - 11) * 5)),
    CC2420_CHANNEL_14 = (2405 - 2048 + ((14 - 11) * 5)),
    CC2420_CHANNEL_15 = (2405 - 2048 + ((15 - 11) * 5)),
    CC2420_CHANNEL_16 = (2405 - 2048 + ((16 - 11) * 5)),
    CC2420_CHANNEL_17 = (2405 - 2048 + ((17 - 11) * 5)),
    CC2420_CHANNEL_18 = (2405 - 2048 + ((18 - 11) * 5)),
    CC2420_CHANNEL_19 = (2405 - 2048 + ((19 - 11) * 5)),
    CC2420_CHANNEL_20 = (2405 - 2048 + ((20 - 11) * 5)),
    CC2420_CHANNEL_21 = (2405 - 2048 + ((21 - 11) * 5)),
    CC2420_CHANNEL_22 = (2405 - 2048 + ((22 - 11) * 5)),
    CC2420_CHANNEL_23 = (2405 - 2048 + ((23 - 11) * 5)),
    CC2420_CHANNEL_24 = (2405 - 2048 + ((24 - 11) * 5)),
    CC2420_CHANNEL_25 = (2405 - 2048 + ((25 - 11) * 5)),
    CC2420_CHANNEL_26 = (2405 - 2048 + ((26 - 11) * 5)),
};
typedef enum cc2420_channel_type CC2420_CHANNEL;

struct cc2420_device
{
    BOOL in_ovf_handle:1;
#ifdef CC2420_INLINE_SECURITY_M
    BOOL key0_in_use:1;
    BOOL key1_in_use:1;
#endif

    UINT8 ovf_rx_len; /* Total received packet length when overflow state. */

#ifdef CC2420_INLINE_SECURITY_M
    UINT8 sec_txnonce[16];
#endif

};
typedef struct cc2420_device CC2420;

void cc2420_init(UINT8 id);
ERROR_T cc2420_channel_init(UINT8 id, CC2420_CHANNEL ch);
void cc2420_set_sec_key(UINT8 idx, const UINT8 *key);
void cc2420_intr_handler(UINT8 id);

ERROR_T cc2420_set_autoack(UINT8 id, BOOL on);

#endif // CC2420_M
#endif // ~CC2420_H
