// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ADF7023-J radio chip constants.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 1.
 */

#ifndef ADF7023_J_CONST_H
#define ADF7023_J_CONST_H

enum adf7023_commands
{
    /* Synchronizes the communication processor to the host microprocessor after
       reset. */
    CMD_SYNC = 0xA2,

    /* Invoke transition of device into state PHY_OFF. */
    CMD_PHY_OFF = 0xB0,
    
    /* Invoke transition of device into state PHY_ON. */
    CMD_PHY_ON = 0xB1,

    /* Invoke transition of device into state PHY_RX. */
    CMD_PHY_RX = 0xB2,

    /* Invoke transition of device into state PHY_TX. */
    CMD_PHY_TX = 0xB5,

    /* Invoke transition of device into state PHY_SLEEP. */
    CMD_PHY_SLEEP = 0xBA,

    /* Configures the radio parameters based on the BBRAM values. */
    CMD_CONFIG_DEV = 0xBB,

    /* Performs an RSSI measurement. */
    CMD_GET_RSSI = 0xBC,

    /* Performs an calibration of the IF filter. */
    CMD_BB_CAL = 0xBE,

    /* Performs a full hardware reset. The device enters PHY_SLEEP. */
    CMD_HW_RESET = 0xC8,

    /* Prepares the program RAM for a download. */
    CMD_RAM_LOAD_INIT = 0xBF,

    /* Performs a reset of the communications processor after loading RAM. */
    CMD_RAM_LOAD_DONE = 0xC7,

    /* Initiates an image rejection calibration using the IR cal code stored on
       program RAM. */
    CMD_IR_CAL = 0xBD,

    /* Requires the AES software module. */
    CMD_AES_ENCRYPT = 0xD0,
    CMD_AES_DECRYPT_INIT = 0xD1,
    CMD_AES_DECRYPT = 0xD2,

    /* Sequential Write */
    SPI_MEM_WR = 0x18,

    /* Sequential Read */
    SPI_MEM_RD = 0x38,

    /* Random Write */
    SPI_MEMR_WR = 0x08,

    /* Random Read */
    SPI_MEMR_RD = 0x28,

    /* No operation */
    SPI_NOP = 0xFF,
};

enum status_word_mask
{
    SPI_READY = 0x80,
    IRQ_STATUS = 0x40,
    CMD_READY = 0x20,
    FW_STATE = 0x1F,
};

enum fw_state
{
    INITIALIZING = 0x0F,
    BUSY = 0x00,
    PHY_OFF = 0x11,
    PHY_ON = 0x12,
    PHY_RX = 0x13,
    PHY_TX = 0x14,
    PHY_SLEEP = 0x06,
    PERFORMING_RSSI = 0x05,
    PERFORMING_DEC_INIT = 0x08,
    PERFORMING_DEC = 0x09,
    PERFORMING_ENC = 0x0A,
};

enum packet_ram_address
{
    PACKET_RAM_BASE = 0x10,
    PACKET_RAM_END = 0x100,
    PACKET_RAM_802_15_4_RX = PACKET_RAM_BASE,
    PACKET_RAM_802_15_4_TX = PACKET_RAM_BASE,
};

enum memory
{
    PACKET_RAM = 0,
    BBRAM = 1,
    MCR = 3,
};

enum BBRAM_register
{
    BB_INTERRUPT_MASK_0 = 0x100, 
    BB_INTERRUPT_MASK_1 = 0x101,
    BB_NUMBER_OF_WAKEUPS_0 = 0x102,
    BB_NUMBER_OF_WAKEUPS_1 = 0x103,
    BB_NUMBER_OF_WAKEUPS_IRQ_THRESHOLD_0 = 0x104,
    BB_NUMBER_OF_WAKEUPS_IRQ_THRESHOLD_1 = 0x105,
    BB_RX_DWELL_TIME = 0x106,
    BB_PARMTIME_DIVIDER = 0x107,
    BB_SWM_RSSI_THRESH = 0x108,
    BB_CHANNEL_FREQ_0 = 0x109,
    BB_CHANNEL_FREQ_1 = 0x10A,
    BB_CHANNEL_FREQ_2 = 0x10B,
    BB_RADIO_CFG_0 = 0x10C,
    BB_RADIO_CFG_1 = 0x10D,
    BB_RADIO_CFG_2 = 0x10E,
    BB_RADIO_CFG_3 = 0x10F,
    BB_RADIO_CFG_4 = 0x110,
    BB_RADIO_CFG_5 = 0x111,
    BB_RADIO_CFG_6 = 0x112,
    BB_RADIO_CFG_7 = 0x113,
    BB_RADIO_CFG_8 = 0x114,
    BB_RADIO_CFG_9 = 0x115,
    BB_RADIO_CFG_10 = 0x116,
    BB_RADIO_CFG_11 = 0x117,
    BB_IMAGE_REJECT_CAL_PHASE = 0x118,
    BB_IMAGE_REJECT_CAL_AMPLITUDE = 0x119,
    BB_MODE_CONTROL = 0x11A,
    BB_PREAMBLE_MATCH = 0x11B,
    BB_SYMBOL_MODE = 0x11C,
    BB_PREAMBLE_LEN = 0x11D,
    BB_CRC_POLY_0 = 0x11E,
    BB_CRC_POLY_1 = 0x11F,
    BB_SYNC_CONTROL = 0x120,
    BB_SYNC_BYTE_0 = 0x121,
    BB_SYNC_BYTE_1 = 0x122,
    BB_SYNC_BYTE_2 = 0x123,
    BB_TX_BASE_ADR = 0x124,
    BB_RX_BASE_ADR = 0x125,
    BB_PACKET_LENGTH_CONTROL = 0x126,
    BB_PACKET_LENGTH_MAX = 0x127,
    BB_STATIC_REG_FIX = 0x128,
    BB_ADDRESS_MATCH_OFFSET = 0x129,
    BB_ADDRESS_LENGTH = 0x12A,
    BB_ADDRESS_MATCH_BYTE_0 = 0x12B,
    BB_ADDRESS_MASK_BYTE_0 = 0x12C,
    BB_ADDRESS_MATCH_BYTE_1 = 0x12D,
    BB_ADDRESS_MASK_BYTE_1 = 0x12E,
    BB_ADDRESS_MATCH_BYTE_2 = 0x12F,
    BB_ADDRESS_MASK_BYTE_2 = 0x130,
    BB_ADDRESS_MATCH_BYTE_3 = 0x131,
    BB_ADDRESS_MASK_BYTE_3 = 0x132,
    BB_ADDRESS_MATCH_BYTE_4 = 0x133,
    BB_ADDRESS_MASK_BYTE_4 = 0x134,
    BB_ADDRESS_MATCH_BYTE_5 = 0x135,
    BB_ADDRESS_MASK_BYTE_5 = 0x136,
    BB_ADDRESS_MATCH_END = 0x137,
    BB_RSSI_WAIT_TIME = 0x138,
    BB_TESTMODES = 0x139,
    BB_TRANSITION_CLOCK_DIV = 0x13A,
    BB_RESERVED_0 = 0x13B,
    BB_RESERVED_1 = 0x13C,
    BB_RESERVED_2 = 0x13D,
    BB_RX_SYNTH_LOCK_TIME = 0x13E,
    BB_TX_SYNTH_LOCK_TIME = 0x13F,
};

enum BBRAM_INTERRUPT_MASK_0_VALUE
{
    INTERRUPT_MASK_NUM_WAKEUPS = (1 << 7), 
    INTERRUPT_MASK_SWM_RSSI_DET = (1 << 6), 
    INTERRUPT_MASK_AES_DONE = (1 << 5), 
    INTERRUPT_MASK_TX_EOF = (1 << 4), 
    INTERRUPT_MASK_ADDRESS_MATCH = (1 << 3), 
    INTERRUPT_MASK_CRC_CORRECT = (1 << 2), 
    INTERRUPT_MASK_SYNC_DETECT = (1 << 1), 
    INTERRUPT_MASK_PREAMBLE_DETECT = (1 << 0), 
};

enum BBRAM_INTERRUPT_MASK_1_VALUE
{
    INTERRUPT_MASK_BATTERY_ALARM = (1 << 7), 
    INTERRUPT_MASK_CMD_READY = (1 << 6), 
    INTERRUPT_MASK_WUC_TIMEOUT = (1 << 4), 
    INTERRUPT_MASK_SPI_READY = (1 << 1), 
    INTERRUPT_MASK_CMD_FINISHED = (1 << 0), 
};

enum BBRAM_PACKET_LENGTH_CONTROL_VALUE
{    
    DATA_MODE_PACKET = (0 << 3),
    DATA_MODE_SPORT_PD = (1 << 3),
    DATA_MODE_SPORT_SD = (2 << 3),
    
    CRC_EN_NO = (0 << 5),
    CRC_EN = (1 << 5),

    PACKET_LEN_VARIABLE = (0 << 6),
    PACKET_LEN_FIXED = (1 << 6),

    DATA_BYTE_LSB_FIRST = (0 << 7),
    DATA_BYTE_MSB_FIRST = (1 << 7),
};

enum BBRAM_SYNC_CONTROL_OFFSET
{
    SYNC_WORD_LENGTH_OFFSET = 0,
    SYNC_ERROR_TOL_LENGTH_OFFSET = 6,
};

enum BBRAM_SYMBOL_MODE_VALUE
{
    SYMBOL_LENGTH_8_BIT = (0 << 0),
    SYMBOL_LENGTH_10_BIT = (1 << 0),

    DATA_WHITENING_DISABLED = (0 << 3),
    DATA_WHITENING_ENABLED = (1 << 3),

    EIGHT_TEN_ENC_DISABLED = (0 << 4),
    EIGHT_TEN_ENC_ENABLED = (1 << 4),

    PROG_CRC_EN_DISABLED = (0 << 5),
    PROG_CRC_EN_ENABLED = (1 << 5),

    MANCHESTER_ENC_DISABLED = (0 << 6),
    MANCHESTER_ENC_ENABLED = (1 << 6),
};

enum BBRAM_PREAMBLE_MATCH_VALUE
{
    PREAMBLE_DETECTION_DISABLED                     = (0 << 0),
    
    ALLOW_4_ERR_BIT_PAIRS_IN_12BIT_PAIRS = (8 << 0),
    ALLOW_3_ERR_BIT_PAIRS_IN_12BIT_PAIRS = (9 << 0),
    ALLOW_2_ERR_BIT_PAIRS_IN_12BIT_PAIRS = (10 << 0),
    ALLOW_1_ERR_BIT_PAIRS_IN_12BIT_PAIRS = (11 << 0),
    ALLOW_0_ERR_BIT = (12 << 0),
};

enum BBRAM_MODE_CONTROL_VALUE
{
    SWM_EN_DISABLED = (0 << 7),   
    SWM_EN_ENABLED = (1 << 7),   

    BB_CAL_DISABLED = (0 << 6),   
    BB_CAL_ENABLED = (1 << 6),   

    SWM_RSSI_QUAL_DISABLED = (0 << 5),   
    SWM_RSSI_QUAL_ENABLED = (1 << 5),   

    TX_TO_RX_AUTO_TURNAROUND_DISABLED = (0 << 4),   
    TX_TO_RX_AUTO_TURNAROUND_ENABLED = (1 << 4),   

    RX_TO_TX_AUTO_TURNAROUND_DISABLED = (0 << 3),   
    RX_TO_TX_AUTO_TURNAROUND_ENABLED = (1 << 3),   

    CUSTOM_TRX_SYNTH_LOCK_TIME_EN_USE_DEFAULT = (0 << 2),   
    CUSTOM_TRX_SYNTH_LOCK_TIME_EN_USE_CUSTOM = (1 << 2),   

    EXT_LNA_EN_DISABLED = (0 << 1),   
    EXT_LNA_EN_ENABLED = (1 << 1),   

    EXT_PA_EN_DISABLED = (0 << 0),   
    EXT_PA_EN_ENABLED = (1 << 0),
};
 
enum BBRAM_RADIO_CFG_11_OFFSET
{
    AFC_KP_OFFSET = 4,
    AFC_KI_OFFSET = 0,
};

enum BBRAM_RADIO_CFG_10_VALUE
{
    AFC_LOCK_MODE_FREE_RUNNING = (0 << 0),
    AFC_LOCK_MODE_DISABLED = (1 << 0),
    AFC_LOCK_MODE_HOLD = (2 << 0),
    AFC_LOCK_MODE_LOCK = (3 << 0),

    AFC_SCHEME = (2 << 2),
    AFC_POLARITY = (0 << 4),
};

enum BBRAM_RADIO_CFG_9_VALUE
{
    DEMOD_SCHEME_FSK = (0 << 0),
    DEMOD_SCHEME_GFSK = (1 << 0),
    DEMOD_SCHEME_OOK = (2 << 0),

    MOD_SCHEME_2LEVEL_FSK_MSK = (0 << 3),
    MOD_SCHEME_2LEVEL_GFSK_GMSK = (1 << 3),
    MOD_SCHEME_OOK = (2 << 3),
    MOD_SCHEME_CARRIER_ONLY = (3 << 3),

    IFBW_100KHZ = (0 << 6),
    IFBW_150KHZ = (1 << 6),
    IFBW_200KHZ = (2 << 6),
    IFBW_300KHZ = (3 << 6),
};

enum BBRAM_RADIO_CFG_8_VALUE
{
    PA_SINGLE_DIFF_SEL_SINGLE_ENDED = (0 << 7),
    PA_SINGLE_DIFF_SEL_DIFFERENTIAL = (1 << 7),

    PA_POWER_SETTING_3 = (0 << 3),
    PA_POWER_SETTING_7 = (1 << 3),
    PA_POWER_SETTING_11 = (2 << 3),
    PA_POWER_SETTING_15 = (3 << 3),
    PA_POWER_SETTING_19 = (4 << 3),
    PA_POWER_SETTING_23 = (5 << 3),
    PA_POWER_SETTING_27 = (6 << 3),
    PA_POWER_SETTING_31 = (7 << 3),
    PA_POWER_SETTING_35 = (8 << 3),
    PA_POWER_SETTING_39 = (9 << 3),
    PA_POWER_SETTING_43 = (10 << 3),
    PA_POWER_SETTING_47 = (11 << 3),
    PA_POWER_SETTING_51 = (12 << 3),
    PA_POWER_SETTING_55 = (13 << 3),
    PA_POWER_SETTING_59 = (14 << 3),
    PA_POWER_SETTING_63 = (15 << 3),

    PA_RAMP_256 = (1 << 0), 
    PA_RAMP_128 = (2 << 0), 
    PA_RAMP_64 = (3 << 0), 
    PA_RAMP_32 = (4 << 0), 
    PA_RAMP_16 = (5 << 0), 
    PA_RAMP_8 = (6 << 0), 
    PA_RAMP_4 = (7 << 0),
};

enum BBRAM_RADIO_CFG_7_VALUE_OFFSET
{
    AGC_LOCK_MODE_FREE_RUNNING = (0 << 6),
    AGC_LOCK_MODE_MANUAL = (1 << 6),
    AGC_LOCK_MODE_HOLD = (2 << 6),
    AGC_LOCK_MODE_LOCK_AFTER_PREAMBLE = (3 << 6),

    SYNTH_LUT_CONTROL_PREDEF_RX_PREDEF_TX = (0 << 4),
    SYNTH_LUT_CONTROL_CUSTOM_RX_PREDEF_TX = (1 << 4),
    SYNTH_LUT_CONTROL_PREDEF_RX_CUSTOM_TX = (2 << 4),
    SYNTH_LUT_CONTROL_CUSTOM_RX_CUSTOM_TX = (3 << 4),

    SYNTH_LUT_CONFIG_1_OFFSET = 0,
};

enum BBRAM_RADIO_CFG_6_OFFSET
{
    SYNTH_LUT_CONFIG_0_OFFSET = 2,
    DISCRIM_PHASE_OFFSET = 0,
};

enum BBRAM_RADIO_CFG_4_OFFSET
{
    POST_DEMOD_BW_OFFSET = 0,
};

enum BBRAM_RADIO_CFG_3_OFFSET
{
    DISCRIM_BW_OFFSET = 0,
};

enum BBRAM_RADIO_CFG_2_OFFSET
{
    FREQ_DEVIATION_LOW_OFFSET = 0,
};

enum BBRAM_RADIO_CFG_1_OFFSET
{
    FREQ_DEVIATION_HIGH_OFFSET = 4,
    DATA_RATE_HIGH_OFFSET = 0,
};

enum BBRAM_RADIO_CFG_0_OFFSET
{
    DATA_RATE_LOW_OFFSET = 0,
};

enum MCR_REGISTER
{
    MCR_PA_LEVEL_MCR = 0x307,
    MCR_WUC_CONFIG_HIGH = 0x30C,
    MCR_WUC_CONFIG_LOW = 0x30D,
    MCR_WUC_VALUE_HIGH = 0x30E,
    MCR_WUC_VALUE_LOW = 0x30F,
 
    MCR_WUC_FLAG_RESET = 0x310,
    MCR_WUC_STATUS = 0x311, //R
    MCR_RSSI_READBACK = 0x312,
    MCR_MAX_AFC_RANGE = 0x315,
    MCR_IMAGE_REJECT_CAL_CONFIG = 0x319,

    MCR_CHIP_SHUTDOWN = 0x322,
    MCR_POWERDOWN_RX = 0x325,
    MCR_ADC_READBACK_HIGH = 0x327, //R
    MCR_ADC_READBACK_LOW = 0x328, //R
    
    MCR_BATTERY_MONITOR_THRESHOLD_VOLTAGE = 0x32d,
    MCR_EXT_UC_CLK_DIVIDE = 0x32e,
    MCR_AGC_CLK_DIVIDE = 0x32f,

    MCR_INTERRUPT_SOURCE_0 = 0x336,
    MCR_INTERRUPT_SOURCE_1 = 0x337,
    MCR_CALIBRATION_CONTROL = 0x338,
    MCR_CALIBRATION_STATUS = 0x339, //R

    MCR_RXBB_CAL_CALWRD_READBACK = 0x345, //R
    MCR_RXBB_CAL_CALWRD_OVERWRITE = 0x346,

    MCR_ADC_CONFIG_LOW = 0x359,
    MCR_ADC_CONFIG_HIGH = 0x35a,
    
    MCR_AGC_CONFIG = 0x35c,
    MCR_AGC_MODE = 0x35d,
    MCR_AGC_LOW_THRESHOLD = 0x35e,
    MCR_AGC_HIGH_THRESHOLD = 0x35f,
    
    MCR_AGC_GAIN_STATUS = 0x360, //R

    MCR_FREQUENCY_ERROR_READBACK = 0x372, //R

    MCR_VCO_BAND_OVRW_VAL = 0x3cb,
    MCR_VCO_AMPL_OVRW_VAL = 0x3cc,
    MCR_VCO_OVRW_EN = 0x3cd,

    MCR_VCO_CAL_CFG = 0x3d0,
    MCR_OSC_CONFIG = 0x3d2,
    MCR_VCO_BAND_READBACK = 0x3da, //R
    MCR_VCO_AMPL_READBACK = 0x3db, //R

    MCR_ANALOG_TEST_BUS = 0x3f8,
    MCR_RSSI_TXTMUX_SEL = 0x3f9,
    MCR_GPIO_CONFIGURE = 0x3fa,
    MCR_TEST_DAC_GAIN = 0x3fd,
};

enum MCR_INTERRUPT_SOURCE_0_OFFSET
{
    INTERRUPT_SOURCE_NUM_WAKEUPS = (1 << 7),
    INTERRUPT_SOURCE_SWM_RSSI_DET = (1 << 6),
    INTERRUPT_SOURCE_AES_DONE = (1 << 5),
    INTERRUPT_SOURCE_TX_EOF = (1 << 4),
    INTERRUPT_SOURCE_ADDRESS_MATCH = (1 << 3),
    INTERRUPT_SOURCE_CRC_CORRECT = (1 << 2),
    INTERRUPT_SOURCE_SYNC_DETECT = (1 << 1),
    INTERRUPT_SOURCE_PREAMBLE_DETECT = (1 << 0),
};

enum MCR_INTERRUPT_SOURCE_1_OFFSET
{
    INTERRUPT_SOURCE_BATTERY_ALARM = (1 << 7),
    INTERRUPT_SOURCE_CMD_READY = (1 << 6),
    INTERRUPT_SOURCE_WUC_TIMEOUT = (1 << 4),
    INTERRUPT_SOURCE_SPI_READY = (1 << 1),
    INTERRUPT_SOURCE_CMD_FINISHED = (1 << 0),
};

struct adf7023j_chip_status
{
    BOOL ready_to_recv:1;
    BOOL ready_to_send:1;
    BOOL eui64_is_set:1;
    BOOL locked:1;          /// TRUE when the chip is used for Tx.
    BOOL ack_received:1;    /// TRUE when an valid Ack frame is received.
    BOOL wait_for_ack:1;    /// TRUE when the node is waiting for an Ack frame.
    BOOL setup_channel:1;   /// TRUE when the user setup the channel frequency.
    BOOL setup_mode:1;      /// TRUE when the user setup the operation mode.
    UINT8 ack_seq;          /// Received Ack frame's sequence number.
};

#endif //ADF7023_J_CONST_H
