// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ADF7023-J radio chip 802.15.4 support part.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 1.
 */

#include "adf7023-j.h"

#ifdef ADF7023J_M

#include "adf7023-j-const.h"
#include "adf7023-j-mem-access.h"
#include "arch.h"
#include "critical_section.h"
#include <stdlib.h>
#include "wpan-dev.h"
#include "platform.h"

#ifdef BENCHMARK_M
#include "benchmark.h"
#endif

extern WPAN_DEV wpan_dev[WPAN_DEV_CNT];

void adf7023j_write_frame(UINT8 dev_id, const UINT8 *buf)
{
    UINT8 size;

    wpan_dev[dev_id].c.adf7023j.ready_to_send = TRUE;
    wpan_dev[dev_id].c.adf7023j.ready_to_recv = FALSE;

    size = 1 + buf[0] - 2; // + length of PHR

    /* Write the Tx frame to the packet RAM. */
    if (adf7023j_write_pktram(dev_id, PACKET_RAM_802_15_4_TX, &size, 1) != ERROR_SUCCESS ||
        adf7023j_write_pktram(dev_id, PACKET_RAM_802_15_4_TX + 1, &buf[1], size) != ERROR_SUCCESS)
    {
        wpan_dev[dev_id].c.adf7023j.ready_to_send = FALSE;
        wpan_dev[dev_id].c.adf7023j.ready_to_recv = TRUE;
    }
}

BOOL adf7023j_send_ack(UINT8 dev_id, UINT8 seq)
{
    UINT8 ack_frame[4] = {4, 0x02, 0x00, 0};
    UINT8 status;
    
    ack_frame[3] = seq;
    if (adf7023j_write_pktram(dev_id,
                              PACKET_RAM_802_15_4_TX,
                              ack_frame,
                              sizeof(ack_frame)) != ERROR_SUCCESS)
    {
        return FALSE;
    }
    
    wpan_dev[dev_id].c.adf7023j.ready_to_send = TRUE;

    adf7023j_send_command(dev_id, CMD_PHY_TX);
    do { adf7023j_get_status(dev_id, &status); } while((status & FW_STATE) != PHY_ON);

    /* printf("|%s()-state:0x%x|", __FUNCTION__, status); */
    return TRUE;
}

ERROR_T adf7023j_read_frame(UINT8 dev_id, struct wpan_rx_frame *frame)
{
    UINT16 fcf, pan, id;
    ERROR_T result;

/* #ifdef BENCHMARK_M */
/*     UINT16 ack_ready_time = 0, ack_send_done_time = 0; */
/* #endif */
    
/* #ifdef BENCHMARK_M */
/*     benchmark_reset(); */
/*     benchmark_start(); */
/* #endif */

    // Send Ack as soon as possible if required.

    /* Read 14 byte, the minimum length to decide whether Ack is required or
       not, from the Packet RAM.
       14 = length(1) + FCF(2) + seq(1) + PAN(2) + ID(8)
    */
    if (adf7023j_read_pktram(dev_id, PACKET_RAM_802_15_4_RX, &frame->len, 1) != ERROR_SUCCESS ||
        adf7023j_read_pktram(dev_id, PACKET_RAM_802_15_4_RX + 1, frame->buf, 13) != ERROR_SUCCESS)
    {
        result = ERROR_FAIL;
        goto error;
    }

    fcf = (frame->buf[0] + (frame->buf[1] << 8));
    pan = (frame->buf[3] + (frame->buf[4] << 8));
    id = (frame->buf[5] + (frame->buf[6] << 8));

    if (!wpan_dev[dev_id].c.adf7023j.locked &&
        FRAME_REQUESTS_ACK(fcf) &&
        (pan == 0xFFFF || pan == wpan_dev[dev_id].pan_id) &&
        ((FRAME_CONTAINS_SHORTDST(fcf) && id == wpan_dev[dev_id].short_id) ||
         (FRAME_CONTAINS_LONGDST(fcf) &&
          frame->buf[5] == wpan_dev[dev_id].eui64[7] &&
          frame->buf[6] == wpan_dev[dev_id].eui64[6] &&
          frame->buf[7] == wpan_dev[dev_id].eui64[5] &&
          frame->buf[8] == wpan_dev[dev_id].eui64[4] &&
          frame->buf[9] == wpan_dev[dev_id].eui64[3] &&
          frame->buf[10] == wpan_dev[dev_id].eui64[2] &&
          frame->buf[11] == wpan_dev[dev_id].eui64[1] &&
          frame->buf[12] == wpan_dev[dev_id].eui64[0])))
    {
/* #ifdef BENCHMARK_M */
/*         ack_ready_time = benchmark_get_usec(); */
/* #endif */
        
        adf7023j_send_ack(dev_id, frame->buf[2]);
        
/* #ifdef BENCHMARK_M */
/*         ack_send_done_time = benchmark_get_usec(); */
/* #endif */
    }
    else if (wpan_dev[dev_id].c.adf7023j.wait_for_ack &&
             frame->len == 4 &&
             frame->buf[0] == 0x02 &&
             frame->buf[1] == 0x00)
    {
        wpan_dev[dev_id].c.adf7023j.ack_received = TRUE;
        wpan_dev[dev_id].c.adf7023j.ack_seq = frame->buf[2];
        
        // To indicate the MAC layer to discard the Ack frame.
        result = ERROR_INVALID_FRAME;
        goto error;
    }

/* #ifdef BENCHMARK_M */
/*     benchmark_stop(); */
/* #endif */

    if (frame->len > 128)
    {
        result = ERROR_INVALID_FRAME;
        goto error;
    }

    if (frame->len > 14)
    {
        //More octets are required to be read.
        if (adf7023j_read_pktram(dev_id,
                                 PACKET_RAM_802_15_4_RX + 14,
                                 &frame->buf[13], frame->len - 14) !=
            ERROR_SUCCESS)
        {
            result = ERROR_FAIL;
            goto error;
        }
    }

    if (adf7023j_read_mcr(dev_id,
                          MCR_RSSI_READBACK,
                          (UINT8 *) &frame->rssi) !=
        ERROR_SUCCESS)
    {
        result = ERROR_FAIL;
        goto error;
    }

    frame->len -= 1; // - length of PHR
    frame->rssi -= 107;
    frame->crc_ok = TRUE;
    frame->secured = FALSE;

    result = ERROR_SUCCESS;
error:
    wpan_dev[dev_id].c.adf7023j.ready_to_recv = TRUE;

/* #ifdef BENCHMARK_M */
/*     printf("%s()-benchmark (ready for ack:%u us, ack send done:%u us)\n", */
/*            __FUNCTION__, ack_ready_time, ack_send_done_time); */
/* #endif */

    return result;
}

#define MIN_CCA_CNT 5

BOOL adf7023j_cca(UINT8 dev_id)
{
    INT8 r;
    UINT8 i;
    BOOL clear = TRUE;
    UINT8 cnt;
    UINT8 status;

/* #ifdef BENCHMARK_M */
/*     UINT16 t_state_rx = 0, t_cca = 0, t_state_on = 0; */
/*     benchmark_reset(); */
/*     benchmark_start(); */
/* #endif */
    
/* #ifdef BENCHMARK_M */
/*     t_state_rx = benchmark_get_usec(); */
/* #endif */
    
#ifdef IEEE_802_15_4_DEV_LBT
    cnt = MIN_CCA_CNT + (rand() % MIN_CCA_CNT);
#else
    cnt = MIN_CCA_CNT;
#endif

    NOS_ENTER_CRITICAL_SECTION();
    adf7023j_send_command(dev_id, CMD_PHY_ON);
    do { adf7023j_get_status(dev_id, &status); } while ((status & FW_STATE) != PHY_ON);
    NOS_EXIT_CRITICAL_SECTION();

    /* printf("%s()-", __FUNCTION__); */
    for (i = 0; i < cnt; i++)
    {
        /* printf("[%u/%u:", i, cnt); */

        adf7023j_send_command(dev_id, CMD_GET_RSSI);
        do { adf7023j_get_status(dev_id, &status); } while ((status & FW_STATE) != PHY_ON);

        adf7023j_read_mcr(dev_id, MCR_RSSI_READBACK, (UINT8 *) &r);
        r -= 107;
        
        /* printf("%d]", r); */
    
        if (clear)
        {
            clear = (r < -95) ? TRUE : FALSE;
        }

#ifdef IEEE_802_15_4_DEV_LBT
        // Exit when the channel is clear for at least 5 ms.
        if (i == (MIN_CCA_CNT - 1) && clear)
        {
            /* printf("!"); */
            break;
        }
#endif
    }
    /* printf("\n"); */

/* #ifdef BENCHMARK_M */
/*     t_cca = benchmark_get_usec(); */
/* #endif */

    NOS_ENTER_CRITICAL_SECTION();
    adf7023j_send_command(dev_id, CMD_PHY_RX);
    do { adf7023j_get_status(dev_id, &status); } while ((status & FW_STATE) != PHY_RX);
    NOS_EXIT_CRITICAL_SECTION();

    adf7023j_get_status(dev_id, &status);
    /* printf("%s()-CCA:%d, status:0x%x\n", __FUNCTION__, clear, status); */

/* #ifdef BENCHMARK_M */
/*     t_state_on = benchmark_get_usec(); */

/*     printf("%s()-state_rx:%u, cca:%u, state_on:%u\n", __FUNCTION__, t_state_rx, t_cca, t_state_on); */
/* #endif */
    
    return clear;
}

ERROR_T adf7023j_transmit(UINT8 dev_id, const UINT8 *buf)
{
    UINT8 status;

    if (buf == NULL)
        return ERROR_INVALID_ARGS;

    /* printf("%s()-begins\n", __FUNCTION__); */
    
    // Write a frame to the Packet RAM.
    NOS_ENTER_CRITICAL_SECTION();
        
    if (adf7023j_send_command(dev_id, CMD_PHY_ON) != ERROR_SUCCESS)
    {
        NOS_EXIT_CRITICAL_SECTION();
        return ERROR_FAIL;
    }
    do { adf7023j_get_status(dev_id, &status); } while((status & FW_STATE) != PHY_ON);
    adf7023j_clear_mcu_irq(dev_id);
    adf7023j_write_mcr(dev_id, MCR_INTERRUPT_SOURCE_0, 0xFF);
    
    NOS_EXIT_CRITICAL_SECTION();

    adf7023j_write_frame(dev_id, buf);
    adf7023j_send_command(dev_id, CMD_PHY_TX);
    do { adf7023j_get_status(dev_id, &status); } while((status & FW_STATE) != PHY_ON);

    NOS_ENTER_CRITICAL_SECTION();

    // Back to PHY_RX
    adf7023j_send_command(dev_id, CMD_PHY_RX);
    do { adf7023j_get_status(dev_id, &status); } while((status & FW_STATE) != PHY_RX);
    
    NOS_EXIT_CRITICAL_SECTION();

    /* printf("%s()-done\n", __FUNCTION__); */
    return ERROR_SUCCESS;
}

ERROR_T adf7023j_wait_for_ack(UINT8 dev_id, UINT8 seq)
{
    UINT8 acked = FALSE;
    UINT16 i;
    /* static UINT16 min_i = 0, max_i = 0; */
    
    //Software Ack requires longer wait time than IEEE 802.15.4.
    //nos_delay_us(2500 * SYMBOL_DURATION);
    
    /* nos_delay_us(6000); */

    wpan_dev[dev_id].c.adf7023j.wait_for_ack = TRUE;
    for (i = 0; i < 2000; i++)
    {
        nos_delay_us(10);
        
        if (wpan_dev[dev_id].c.adf7023j.ack_received &&
            wpan_dev[dev_id].c.adf7023j.ack_seq == seq)
        {
            // Acknowledged.
            acked = TRUE;

            /* if (min_i == 0 || i < min_i) */
            /*     min_i = i; */

            /* if (max_i == 0 || i > max_i) */
            /*     max_i = i; */

            /* printf("ACK(%u / min:%u / max:%u)\n", i, min_i, max_i); */
            wpan_dev[dev_id].c.adf7023j.ack_received = FALSE;
            
            break;
        }
    }

    if (i == 2000)
    {
        //No interrupt.
        acked = FALSE;
        /* printf("NO INTR\n"); */
    }

    wpan_dev[dev_id].c.adf7023j.ack_received = FALSE;
    wpan_dev[dev_id].c.adf7023j.wait_for_ack = FALSE;
    
    return (acked) ? ERROR_SUCCESS : ERROR_FAIL;
}

#endif //ADF7023J_M
