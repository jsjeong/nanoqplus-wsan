// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ADF7023-J radio chip driver.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 1.
 */

#include "adf7023-j.h"

#ifdef ADF7023J_M

#include "adf7023-j-const.h"
#include "adf7023-j-mem-access.h"
#include "arch.h"
#include "critical_section.h"
#include "platform.h"
#include "wpan-dev.h"

#ifdef BENCHMARK_M
#include "benchmark.h"
#endif

extern WPAN_DEV wpan_dev[WPAN_DEV_CNT];

BOOL adf7023j_rxbuf_is_empty(UINT8 dev_id)
{
    if (wpan_dev[dev_id].c.adf7023j.ready_to_send == TRUE)
        return TRUE;
    else
        return wpan_dev[dev_id].c.adf7023j.ready_to_recv;
}

void adf7023j_clear_pktram(UINT8 dev_id)
{
    wpan_dev[dev_id].c.adf7023j.ready_to_recv = TRUE;
}

void adf7023j_wakeup(UINT8 dev_id)
{
}

void adf7023j_sleep(UINT8 dev_id)
{
}

void adf7023j_set_tx_power(UINT8 dev_id, UINT8 level)
{
    if (level < 1) level = 1;
    else if (level > 63) level = 63;
    
    adf7023j_write_mcr(dev_id, MCR_PA_LEVEL_MCR, level);
}

/**
 * Lock the ADF7023-J chip not to be used for Tx.
 */
ERROR_T adf7023j_lock(UINT8 dev_id)
{
    UINT8 status;
    ERROR_T err;

    NOS_ENTER_CRITICAL_SECTION();
    if (wpan_dev[dev_id].c.adf7023j.locked == TRUE)
    {
        // Already locked.
        err = ERROR_BUSY;
        goto error;
    }

    adf7023j_get_status(dev_id, &status);
    if ((status & FW_STATE) != PHY_RX || (status & IRQ_STATUS) != 0)
    {
        /* printf("%s()-not rx state or irq is pending:0x%X (EXTI:0x%x, pin:%d, NVIC:%d)\n", */
        /*        __FUNCTION__, status, */
        /*        EXTI_GetITStatus(EXTI_Line8), */
        /*        GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8), */
        /*        NVIC_GetPendingIRQ(EXTI9_5_IRQn)); */
        err = ERROR_BUSY;
        goto error;
    }
    
    wpan_dev[dev_id].c.adf7023j.locked = TRUE;
    err = ERROR_SUCCESS;
    
error:
    NOS_EXIT_CRITICAL_SECTION();
    return err;
}

/**
 * Release the ADF7023-J chip to be used for Tx.
 */
ERROR_T adf7023j_release(UINT8 dev_id)
{
    UINT8 status;
    ERROR_T err;

    NOS_ENTER_CRITICAL_SECTION();
    if (wpan_dev[dev_id].c.adf7023j.locked == FALSE)
    {
        // Already released.
        err = SUCCESS_NOTHING_HAPPENED;
        goto error;
    }

    wpan_dev[dev_id].c.adf7023j.locked = FALSE;

    adf7023j_get_status(dev_id, &status);
    /* printf("%s() exit state:0x%02X (EXTI:%d, pin:%d, NVIC:%d)\n", */
    /*        __FUNCTION__, status, */
    /*        EXTI_GetITStatus(EXTI_Line0), */
    /*        GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_0), */
    /*        NVIC_GetPendingIRQ(EXTI0_IRQn)); */
    err = ERROR_SUCCESS;
    
error:
    NOS_EXIT_CRITICAL_SECTION();
    return err;
}

/**
 *
 */
void adf7023j_intr_handler(UINT8 dev_id)
{
    UINT8 intr, status;
    BOOL stop_intr = FALSE;

    /* printf("<"); */

    // Wait until Rx done.
    do { adf7023j_get_status(dev_id, &status); } while((status & FW_STATE) != PHY_ON);

    /* printf("%x|", status); */

    wpan_dev[dev_id].c.adf7023j.ready_to_send = FALSE;
    wpan_dev[dev_id].c.adf7023j.ready_to_recv = FALSE;

    if (adf7023j_read_mcr(dev_id, MCR_INTERRUPT_SOURCE_0, &intr) == ERROR_SUCCESS &&
        intr != 0)
    {
        if (wpan_dev[dev_id].notify != NULL)
            wpan_dev[dev_id].notify(dev_id, 0, &stop_intr);
    }

    /* printf("%x|", intr); */
    
    // Clear the interrupt.
    adf7023j_write_mcr(dev_id, MCR_INTERRUPT_SOURCE_0, 0xFF);

    // Listen.
    adf7023j_send_command(dev_id, CMD_PHY_RX);
    do { adf7023j_get_status(dev_id, &status); } while((status & FW_STATE) != PHY_RX);

    wpan_dev[dev_id].c.adf7023j.ready_to_recv = TRUE;
    /* printf("%x>", status); */
}

void adf7023j_setup_channel_freq(UINT8 dev_id, UINT32 freq_khz)
{
    UINT32 channel_freq;
    
    /*
     * According to Table 59 in ADF7023-J data sheet,
     * CHANNEL_FREQ = Frequency (kHz) * 1000 * 2^16 / 26000000
     *              = Frequency (kHz) * 1000 * 2^9 / 203125
     *              = Frequency (kHz) * 1000 * 512 / 203125
     *              = Frequency (kHz) * 8 * 512 / 1625
     */
    channel_freq = freq_khz * 8 * 512 / 1625;

    adf7023j_write_bbram(dev_id, BB_CHANNEL_FREQ_0, (channel_freq >> 0) & 0xFF);
    adf7023j_write_bbram(dev_id, BB_CHANNEL_FREQ_1, (channel_freq >> 8) & 0xFF);
    adf7023j_write_bbram(dev_id, BB_CHANNEL_FREQ_2, (channel_freq >> 16) & 0xFF);

    wpan_dev[dev_id].c.adf7023j.setup_channel = TRUE;
}

void adf7023j_setup_mode(UINT8 dev_id, ADF7023J_MODE mode)
{
    if (mode == ADF7023J_MODE_50KBPS)
    {
        //Data rate: 0x1F4 -> 50kbps
        //Frequency deviation: 0x0FA -> 25kHz

        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_0,
                             (0xF4 << DATA_RATE_LOW_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_1,
                             (0 << FREQ_DEVIATION_HIGH_OFFSET) |
                             (1 << DATA_RATE_HIGH_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_2,
                             (0xFA << FREQ_DEVIATION_LOW_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_3,
                             (0x82 << DISCRIM_BW_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_4,
                             (0x13 << POST_DEMOD_BW_OFFSET));

        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_6,
                             (0 << SYNTH_LUT_CONFIG_0_OFFSET) |
                             (2 << DISCRIM_PHASE_OFFSET));

        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_8,
                             PA_SINGLE_DIFF_SEL_SINGLE_ENDED |
                             PA_POWER_SETTING_63 |
                             PA_RAMP_8);
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_9,
                             IFBW_200KHZ |
                             MOD_SCHEME_2LEVEL_GFSK_GMSK |
                             DEMOD_SCHEME_FSK);

        wpan_dev[dev_id].c.adf7023j.setup_channel = TRUE;
    }
    else if (mode == ADF7023J_MODE_150KBPS)
    {
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_0,
                             (0xDC << DATA_RATE_LOW_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_1,
                             (1 << FREQ_DEVIATION_HIGH_OFFSET) |
                             (5 << DATA_RATE_HIGH_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_2,
                             (0x77 << FREQ_DEVIATION_LOW_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_3,
                             (0x2B << DISCRIM_BW_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_4,
                             (0x38 << POST_DEMOD_BW_OFFSET));

        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_6,
                             (0 << SYNTH_LUT_CONFIG_0_OFFSET) |
                             (2 << DISCRIM_PHASE_OFFSET));

        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_8,
                             PA_SINGLE_DIFF_SEL_SINGLE_ENDED |
                             PA_POWER_SETTING_63 |
                             PA_RAMP_4);
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_9,
                             IFBW_200KHZ |
                             MOD_SCHEME_2LEVEL_GFSK_GMSK |
                             DEMOD_SCHEME_FSK);

        wpan_dev[dev_id].c.adf7023j.setup_channel = TRUE;
    }
    else if (mode == ADF7023J_MODE_200KBPS)
    {
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_0,
                             (0xD0 << DATA_RATE_LOW_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_1,
                             (1 << FREQ_DEVIATION_HIGH_OFFSET) |
                             (7 << DATA_RATE_HIGH_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_2,
                             (0xF4 << FREQ_DEVIATION_LOW_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_3,
                             (0x20 << DISCRIM_BW_OFFSET));
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_4,
                             (0x4B << POST_DEMOD_BW_OFFSET));

        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_6,
                             (0 << SYNTH_LUT_CONFIG_0_OFFSET) |
                             (1 << DISCRIM_PHASE_OFFSET));

        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_8,
                             PA_SINGLE_DIFF_SEL_SINGLE_ENDED |
                             PA_POWER_SETTING_63 |
                             PA_RAMP_4);
    
        adf7023j_write_bbram(dev_id, BB_RADIO_CFG_9,
                             IFBW_300KHZ |
                             MOD_SCHEME_2LEVEL_GFSK_GMSK |
                             DEMOD_SCHEME_FSK);

        wpan_dev[dev_id].c.adf7023j.setup_channel = TRUE;
    }
}

void adf7023j_init_mcr(UINT8 dev_id)
{
    adf7023j_write_mcr(dev_id, MCR_PA_LEVEL_MCR, 63);
    adf7023j_write_mcr(dev_id, MCR_WUC_CONFIG_HIGH, 0);
    adf7023j_write_mcr(dev_id, MCR_WUC_CONFIG_LOW, 0);
    adf7023j_write_mcr(dev_id, MCR_WUC_VALUE_HIGH, 0);
    adf7023j_write_mcr(dev_id, MCR_WUC_VALUE_LOW, 0);
 
    adf7023j_write_mcr(dev_id, MCR_WUC_FLAG_RESET, 0x02);
    adf7023j_write_mcr(dev_id, MCR_MAX_AFC_RANGE, 0x32);
    adf7023j_write_mcr(dev_id, MCR_IMAGE_REJECT_CAL_CONFIG, 0);

    adf7023j_write_mcr(dev_id, MCR_CHIP_SHUTDOWN, 0);
    adf7023j_write_mcr(dev_id, MCR_POWERDOWN_RX, 0);
    
    adf7023j_write_mcr(dev_id, MCR_BATTERY_MONITOR_THRESHOLD_VOLTAGE, 0);
    adf7023j_write_mcr(dev_id, MCR_EXT_UC_CLK_DIVIDE, 0);
    adf7023j_write_mcr(dev_id, MCR_AGC_CLK_DIVIDE, 0x28);

    adf7023j_write_mcr(dev_id, MCR_CALIBRATION_CONTROL, 0);

    adf7023j_write_mcr(dev_id, MCR_RXBB_CAL_CALWRD_OVERWRITE, 0);

    adf7023j_write_mcr(dev_id, MCR_ADC_CONFIG_LOW, 0);
    adf7023j_write_mcr(dev_id, MCR_ADC_CONFIG_HIGH, 0x11);
    
    adf7023j_write_mcr(dev_id, MCR_AGC_CONFIG, 0x9C);
    adf7023j_write_mcr(dev_id, MCR_AGC_MODE, 0x1C);
    adf7023j_write_mcr(dev_id, MCR_AGC_LOW_THRESHOLD, 0x37);
    adf7023j_write_mcr(dev_id, MCR_AGC_HIGH_THRESHOLD, 0x69);
    
    adf7023j_write_mcr(dev_id, MCR_VCO_BAND_OVRW_VAL, 0);
    adf7023j_write_mcr(dev_id, MCR_VCO_AMPL_OVRW_VAL, 0);
    adf7023j_write_mcr(dev_id, MCR_VCO_OVRW_EN, 0);

    adf7023j_write_mcr(dev_id, MCR_VCO_CAL_CFG, 0x01);
    adf7023j_write_mcr(dev_id, MCR_OSC_CONFIG, 0x20);

    adf7023j_write_mcr(dev_id, MCR_ANALOG_TEST_BUS, 0);
    adf7023j_write_mcr(dev_id, MCR_RSSI_TXTMUX_SEL, 0);
    adf7023j_write_mcr(dev_id, MCR_GPIO_CONFIGURE, 0);
    adf7023j_write_mcr(dev_id, MCR_TEST_DAC_GAIN, 4);
}

void adf7023j_init_bbram(UINT8 dev_id)
{
    adf7023j_write_bbram(dev_id, BB_INTERRUPT_MASK_0,
                         INTERRUPT_MASK_CRC_CORRECT);
    adf7023j_write_bbram(dev_id, BB_INTERRUPT_MASK_1, 0);

    adf7023j_write_bbram(dev_id, BB_NUMBER_OF_WAKEUPS_0, 0);
    adf7023j_write_bbram(dev_id, BB_NUMBER_OF_WAKEUPS_1, 0);
    adf7023j_write_bbram(dev_id, BB_NUMBER_OF_WAKEUPS_IRQ_THRESHOLD_0, 0);
    adf7023j_write_bbram(dev_id, BB_NUMBER_OF_WAKEUPS_IRQ_THRESHOLD_1, 0);

    adf7023j_write_bbram(dev_id, BB_RX_DWELL_TIME, 0);
    adf7023j_write_bbram(dev_id, BB_PARMTIME_DIVIDER, 0x33);
    adf7023j_write_bbram(dev_id, BB_SWM_RSSI_THRESH, 0);    

    adf7023j_write_bbram(dev_id, BB_RADIO_CFG_5, 0);

    adf7023j_write_bbram(dev_id, BB_RADIO_CFG_7,
                         AGC_LOCK_MODE_FREE_RUNNING |
                         SYNTH_LUT_CONTROL_PREDEF_RX_PREDEF_TX |
                         (0 << SYNTH_LUT_CONFIG_1_OFFSET));
    
    adf7023j_write_bbram(dev_id, BB_RADIO_CFG_10,
                         AFC_LOCK_MODE_DISABLED |
                         AFC_SCHEME |
                         AFC_POLARITY); //0x09
    
    adf7023j_write_bbram(dev_id, BB_RADIO_CFG_11,
                         (3 << AFC_KP_OFFSET) |
                         (7 << AFC_KI_OFFSET)); //0x37
    
    adf7023j_write_bbram(dev_id, BB_IMAGE_REJECT_CAL_PHASE, 0);
    adf7023j_write_bbram(dev_id, BB_IMAGE_REJECT_CAL_AMPLITUDE, 0);

    adf7023j_write_bbram(dev_id, BB_MODE_CONTROL,
                         SWM_EN_DISABLED |
                         BB_CAL_ENABLED |
                         SWM_RSSI_QUAL_DISABLED |
                         TX_TO_RX_AUTO_TURNAROUND_DISABLED |
                         RX_TO_TX_AUTO_TURNAROUND_DISABLED |
                         CUSTOM_TRX_SYNTH_LOCK_TIME_EN_USE_DEFAULT |
                         EXT_LNA_EN_DISABLED |
                         EXT_PA_EN_DISABLED);
    
    adf7023j_write_bbram(dev_id, BB_PREAMBLE_MATCH,
                         ALLOW_0_ERR_BIT);
    
    adf7023j_write_bbram(dev_id, BB_SYMBOL_MODE,
                         SYMBOL_LENGTH_8_BIT |
                         DATA_WHITENING_DISABLED |
                         EIGHT_TEN_ENC_DISABLED |
                         PROG_CRC_EN_DISABLED |
                         MANCHESTER_ENC_DISABLED);
    
    adf7023j_write_bbram(dev_id, BB_PREAMBLE_LEN, 0x08);

    adf7023j_write_bbram(dev_id, BB_CRC_POLY_0, 0x00);
    adf7023j_write_bbram(dev_id, BB_CRC_POLY_1, 0x00);

    // Sync word
    adf7023j_write_bbram(dev_id, BB_SYNC_CONTROL,
                         (0 << SYNC_ERROR_TOL_LENGTH_OFFSET) |
                         (24 << SYNC_WORD_LENGTH_OFFSET));
    adf7023j_write_bbram(dev_id, BB_SYNC_BYTE_0, 0x12);
    adf7023j_write_bbram(dev_id, BB_SYNC_BYTE_1, 0x34);
    adf7023j_write_bbram(dev_id, BB_SYNC_BYTE_2, 0x56);

    adf7023j_write_bbram(dev_id, BB_TX_BASE_ADR, PACKET_RAM_802_15_4_TX);
    adf7023j_write_bbram(dev_id, BB_RX_BASE_ADR, PACKET_RAM_802_15_4_RX);
    adf7023j_write_bbram(dev_id, BB_PACKET_LENGTH_CONTROL,
                         DATA_BYTE_LSB_FIRST |
                         PACKET_LEN_VARIABLE |
                         CRC_EN |
                         DATA_MODE_PACKET |
                         4);

    // IEEE 802.15.4 compatibility
    adf7023j_write_bbram(dev_id, BB_PACKET_LENGTH_MAX, 128);
    
    adf7023j_write_bbram(dev_id, BB_STATIC_REG_FIX, 0x00);

    // Disable address matching
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MATCH_OFFSET, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_LENGTH, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MATCH_BYTE_0, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MASK_BYTE_0, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MATCH_BYTE_1, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MASK_BYTE_1, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MATCH_BYTE_2, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MASK_BYTE_2, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MATCH_BYTE_3, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MASK_BYTE_3, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MATCH_BYTE_4, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MASK_BYTE_4, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MATCH_BYTE_5, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MASK_BYTE_5, 0x00);
    adf7023j_write_bbram(dev_id, BB_ADDRESS_MATCH_END, 0x00);
    
    adf7023j_write_bbram(dev_id, BB_RSSI_WAIT_TIME, 0xA7);
    adf7023j_write_bbram(dev_id, BB_TESTMODES, 0x00);
    
    adf7023j_write_bbram(dev_id, BB_TRANSITION_CLOCK_DIV, 0x04);
    
    adf7023j_write_bbram(dev_id, BB_RESERVED_0, 0);
    adf7023j_write_bbram(dev_id, BB_RESERVED_1, 0);
    adf7023j_write_bbram(dev_id, BB_RESERVED_2, 0);
    
    adf7023j_write_bbram(dev_id, BB_RX_SYNTH_LOCK_TIME, 0x00);
    adf7023j_write_bbram(dev_id, BB_TX_SYNTH_LOCK_TIME, 0x00);
}

static void adf7023j_config(UINT8 dev_id)
{
    UINT8 status;
    
    /*
     * Setup to the default configurations if they have not been initialized by
     * user. The default channel frequency is 921.1MHz, and the default mode is
     * 50kbps data rate.
     */
    
    if (!wpan_dev[dev_id].c.adf7023j.setup_channel)
    {
        adf7023j_setup_channel_freq(dev_id, 921100);
    }

    if (!wpan_dev[dev_id].c.adf7023j.setup_mode)
    {
        adf7023j_setup_mode(dev_id, ADF7023J_MODE_50KBPS);
    }
    
    adf7023j_init_bbram(dev_id);
    adf7023j_init_mcr(dev_id);
    adf7023j_send_command(dev_id, CMD_CONFIG_DEV);
    //The ADF7023-J is now configured in the PHY_OFF state.

    adf7023j_send_command(dev_id, CMD_PHY_ON);
    adf7023j_send_command(dev_id, CMD_PHY_RX);
    do { adf7023j_get_status(dev_id, &status); } while((status & FW_STATE) != PHY_RX);
    
    wpan_dev[dev_id].c.adf7023j.ready_to_recv = TRUE;
    wpan_dev[dev_id].c.adf7023j.ready_to_send = FALSE;
    wpan_dev[dev_id].c.adf7023j.locked = FALSE;
    wpan_dev[dev_id].c.adf7023j.ack_received = FALSE;
    wpan_dev[dev_id].c.adf7023j.wait_for_ack = FALSE;
}

static const struct wpan_dev_cmdset adf7023j_cmd =
{
    adf7023j_config,
    adf7023j_read_frame,
    adf7023j_write_frame,
    adf7023j_rxbuf_is_empty,
    adf7023j_clear_pktram,
    adf7023j_sleep,
    adf7023j_wakeup,
    adf7023j_transmit,
    adf7023j_wait_for_ack,
    adf7023j_cca,
    NULL,
    NULL,
    NULL,
};

void adf7023j_init(UINT8 dev_id)
{
    wpan_dev[dev_id].hw_autotx = FALSE;
    wpan_dev[dev_id].sec_support = FALSE;
    wpan_dev[dev_id].addrdec_support = FALSE;
    wpan_dev[dev_id].symbol_duration = SYMBOL_DURATION;
    wpan_dev[dev_id].cmd = &adf7023j_cmd;
    
    /* Refer to 'Initialization After Issuing the CMD_HW_RESET Command' in the
       datasheet (Rev.B) p.29. */
    adf7023j_send_command_nowait(dev_id, CMD_HW_RESET);
    nos_delay_ms(1);
    adf7023j_send_command_nowait(dev_id, CMD_SYNC);

    wpan_dev[dev_id].c.adf7023j.setup_channel = FALSE;
    wpan_dev[dev_id].c.adf7023j.setup_mode = FALSE;
}

#endif //ADF7023J_M
