// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ADF7023-J radio chip driver.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 1.
 */

#ifndef ADF7023_J_H
#define ADF7023_J_H

#include "kconf.h"

#ifdef ADF7023J_M
#include "nos_common.h"
#include <string.h>
#include "ieee-802-15-4.h"

// For platform
void adf7023j_init(UINT8 dev_id);
void adf7023j_intr_handler(UINT8 dev_id);

// For MAC protocols
#define SYMBOL_DURATION 20 //usec (50kbps)

void adf7023j_wakeup(UINT8 dev_id);
void adf7023j_sleep(UINT8 dev_id);

/**
 * Set ADF7023-J's power amplifier level by using MCR.
 *
 * @param level: PA level (1 ~ 63)
 *
 * @warn Out of @p level value is modified to the nearest valid level value (1
 *       or 63).
 *
 * @note Initial PA level is 63.
 */
void adf7023j_set_tx_power(UINT8 dev_id, UINT8 level);

BOOL adf7023j_rxbuf_is_empty(UINT8 dev_id);
void adf7023j_clear_pktram(UINT8 dev_id);

ERROR_T adf7023j_transmit(UINT8 dev_id, const UINT8 *buf);
void adf7023j_write_frame(UINT8 dev_id, const UINT8 *buf);
ERROR_T adf7023j_read_frame(UINT8 dev_id, struct wpan_rx_frame *frame);
ERROR_T adf7023j_wait_for_ack(UINT8 dev_id, UINT8 seq);
BOOL adf7023j_cca(UINT8 dev_id);

ERROR_T adf7023j_lock(UINT8 dev_id);
ERROR_T adf7023j_release(UINT8 dev_id);

// For configurations
/**
 * Setup ADF7023-J's channel frequency in unit of kHz.
 *
 * @param freq_khz: Channel frequency (kHz)
 *
 * @warn This function should be called before upper layer protocols (MAC,
 *       6LoWPAN, ...) initialization. Calling in main() between nos_init() and
 *       upper layer protocols initializations is recommended.
 */
void adf7023j_setup_channel_freq(UINT8 dev_id, UINT32 freq_khz);

typedef enum
{
    ADF7023J_MODE_50KBPS,   //Data rate: 50kbps, Frequency deviation: 25kHz
    ADF7023J_MODE_150KBPS,  //Data rate: 150kbps, Frequency deviation: 37.5kHz
    ADF7023J_MODE_200KBPS,  //Data rate: 200kbps, Frequency deviation: 50kHz
} ADF7023J_MODE;

/**
 * Setup ADF7023-J's operation mode.
 *
 * @param mode: Mode to be set.
 *
 * @warn This function should be called before upper layer protocols (MAC,
 *       6LoWPAN, ...) initialization. Calling in main() between nos_init() and
 *       upper layer protocols initializations is recommended.
 */
void adf7023j_setup_mode(UINT8 dev_id, ADF7023J_MODE mode);

#endif //ADF7023J_M
#endif //ADF7023_J_H
