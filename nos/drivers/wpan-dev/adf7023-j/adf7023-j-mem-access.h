// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ADF7023-J radio chip memory access.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 1.
 */

#ifndef ADF7023_J_MEM_ACCESS_H
#define ADF7023_J_MEM_ACCESS_H

#include "adf7023-j.h"

#ifdef ADF7023J_M
#include "nos_common.h"
#include "arch.h"

// Platform provided interface
#include "adf7023-j-interface.h"

ERROR_T adf7023j_wait_until_cmd_ready(void);
ERROR_T adf7023j_write_bbram(UINT8 dev_id, UINT16 addr, UINT8 val);
ERROR_T adf7023j_read_bbram(UINT8 dev_id, UINT16 addr, UINT8 *val);
ERROR_T adf7023j_write_mcr(UINT8 dev_id, UINT16 addr, UINT8 val);
ERROR_T adf7023j_read_mcr(UINT8 dev_id, UINT16 addr, UINT8 *val);
ERROR_T adf7023j_write_pktram(UINT8 dev_id, UINT16 addr, const UINT8 *buf, UINT8 len);
ERROR_T adf7023j_read_pktram(UINT8 dev_id, UINT16 addr, UINT8 *buf, UINT8 len);
ERROR_T adf7023j_send_command_nowait(UINT8 dev_id, UINT8 cmd);
ERROR_T adf7023j_send_command(UINT8 dev_id, UINT8 cmd);
ERROR_T adf7023j_get_status(UINT8 dev_id, UINT8 *status);

#endif //ADF7023J_M
#endif //~ADF7023_J_MEM_ACCESS_H
