// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ADF7023-J radio chip memory access.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 1.
 */

#include "adf7023-j.h"

#ifdef ADF7023J_M
#include "nos_common.h"
#include "arch.h"

// Platform provided interface
#include "adf7023-j-interface.h"
#include "adf7023-j-const.h"

ERROR_T adf7023j_get_status(UINT8 dev_id, UINT8 *status)
{
    ERROR_T err;

    err = adf7023j_request_cs(dev_id);
    if (err != ERROR_SUCCESS)
        return err;

    adf7023j_spi_write(dev_id, SPI_NOP, NULL);
    if (status != NULL)
        adf7023j_spi_write(dev_id, SPI_NOP, status);

    nos_delay_us(0);
    adf7023j_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T adf7023j_wait_until_cmd_ready(UINT8 dev_id)
{
    UINT8 status;

    do {
        adf7023j_get_status(dev_id, &status);
        //without delay, it will not escape loop.
        nos_delay_us(1000);  //minimum about 130us??? --> not stable
    }
    while(!(status & CMD_READY));
    return ERROR_SUCCESS;
}

ERROR_T adf7023j_write_bbram(UINT8 dev_id, UINT16 addr, UINT8 val)
{
    ERROR_T err;
    
    err = adf7023j_wait_until_cmd_ready(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    if (adf7023j_request_cs(dev_id) != ERROR_SUCCESS)
        return ERROR_BUSY;

    adf7023j_spi_write(dev_id, SPI_MEM_WR | BBRAM, NULL);
    adf7023j_spi_write(dev_id, addr & 0xff, NULL);
    adf7023j_spi_write(dev_id, val, NULL);

    nos_delay_us(0);
    adf7023j_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T adf7023j_read_bbram(UINT8 dev_id, UINT16 addr, UINT8 *val)
{
    ERROR_T err;
    
    err = adf7023j_wait_until_cmd_ready(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    if (adf7023j_request_cs(dev_id) != ERROR_SUCCESS)
        return ERROR_BUSY;

    adf7023j_spi_write(dev_id, SPI_MEM_RD | BBRAM, NULL);
    adf7023j_spi_write(dev_id, addr & 0xff, NULL);
    adf7023j_spi_write(dev_id, SPI_NOP, NULL);
    adf7023j_spi_write(dev_id, SPI_NOP, val);

    nos_delay_us(0);
    adf7023j_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T adf7023j_write_mcr(UINT8 dev_id, UINT16 addr, UINT8 val)
{
    ERROR_T err;
    
    err = adf7023j_wait_until_cmd_ready(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    if (adf7023j_request_cs(dev_id) != ERROR_SUCCESS)
        return ERROR_BUSY;

    adf7023j_spi_write(dev_id, SPI_MEM_WR | MCR, NULL);
    adf7023j_spi_write(dev_id, addr & 0xff, NULL);
    adf7023j_spi_write(dev_id, val, NULL);

    nos_delay_us(0);
    adf7023j_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T adf7023j_read_mcr(UINT8 dev_id, UINT16 addr, UINT8 *val)
{
    ERROR_T err;
    
    err = adf7023j_wait_until_cmd_ready(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    if (adf7023j_request_cs(dev_id) != ERROR_SUCCESS)
        return ERROR_BUSY;

    adf7023j_spi_write(dev_id, SPI_MEM_RD | MCR, NULL);
    adf7023j_spi_write(dev_id, addr & 0xff, NULL);
    adf7023j_spi_write(dev_id, SPI_NOP, NULL);
    adf7023j_spi_write(dev_id, SPI_NOP, val);

    nos_delay_us(0);
    adf7023j_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T adf7023j_write_pktram(UINT8 dev_id, UINT16 addr, const UINT8 *buf, UINT8 len)
{
    UINT8 i;
    ERROR_T err;
    
    err = adf7023j_wait_until_cmd_ready(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    if (adf7023j_request_cs(dev_id) != ERROR_SUCCESS)
        return ERROR_BUSY;

    adf7023j_spi_write(dev_id, SPI_MEM_WR | PACKET_RAM, NULL);
    adf7023j_spi_write(dev_id, addr & 0xFF, NULL);

    for (i = 0; i < len; i++)
    {
        adf7023j_spi_write(dev_id, buf[i], NULL);
    }

    nos_delay_us(0);
    adf7023j_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T adf7023j_read_pktram(UINT8 dev_id, UINT16 addr, UINT8 *buf, UINT8 len)
{
    UINT8 i;
    ERROR_T err;
    
    err = adf7023j_wait_until_cmd_ready(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    if (adf7023j_request_cs(dev_id) != ERROR_SUCCESS)
        return ERROR_BUSY;

    adf7023j_spi_write(dev_id, SPI_MEM_RD | PACKET_RAM, NULL);
    adf7023j_spi_write(dev_id, addr & 0xFF, NULL);
    adf7023j_spi_write(dev_id, SPI_NOP, NULL);

    for (i = 0; i < len; i++)
    {
        adf7023j_spi_write(dev_id, SPI_NOP, &buf[i]);
    }

    nos_delay_us(0);
    adf7023j_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T adf7023j_send_command_nowait(UINT8 dev_id, UINT8 cmd)
{
    if (adf7023j_request_cs(dev_id) != ERROR_SUCCESS)
        return ERROR_BUSY;

    adf7023j_spi_write(dev_id, cmd, NULL);

    nos_delay_us(0);
    adf7023j_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T adf7023j_send_command(UINT8 dev_id, UINT8 cmd)
{
    ERROR_T err;

    err = adf7023j_wait_until_cmd_ready(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    if (adf7023j_request_cs(dev_id) != ERROR_SUCCESS)
        return ERROR_BUSY;

    adf7023j_spi_write(dev_id, cmd, NULL);

    nos_delay_us(0);
    adf7023j_release_cs(dev_id);

    return ERROR_SUCCESS;
}

#endif //ADF7023J_M
