// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * MG2410 interface header
 *
 * @author Haeyong Kim (ETRI)
 * @date 2013. 8. 29.
 */

#ifndef MG2410_INTERFACE_H
#define MG2410_INTERFACE_H

#include "kconf.h"
#ifdef MG2410_M
#include "nos_common.h"
#include "errorcodes.h"

ERROR_T nos_mg2410_interface_init(UINT8 dev_id);

ERROR_T nos_mg2410_clear_mcu_intr(UINT8 dev_id);

ERROR_T nos_mg2410_get_mcu_intr_status(UINT8 dev_id, BOOL *is_set);

/*** Radiopulse library dependent ***/
void CB_Control_RESETB(UINT8 dev_id, UINT8 u8High);
void CB_Control_DVREG_EN(UINT8 dev_id, UINT8 u8High);
void CB_Control_CSn(UINT8 dev_id, UINT8 u8High);	// low_active
UINT8 CB_SpiWriteByte(UINT8 dev_id, UINT8 u8WrData);
UINT8 CB_SpiWriteBurstByte(UINT8 dev_id, UINT8* pu8WrDataBuff, UINT16 u16Len);
UINT8 CB_ModemOnCheck(UINT8 dev_id);
void CB_IRQInterruptOff(UINT8 dev_id);
void CB_IRQInterruptOn(UINT8 dev_id);
void CB_HALTimerSet(UINT8 dev_id, UINT16 u16Duration_100us);
UINT16 CB_HALTimerGet(UINT8 dev_id);
/*** end of Radiopulse library dependent ***/

#endif //MG2410_M
#endif //MG2410_INTERFACE_H
