// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * NOS library header for MG2410
 *
 * @author Haeyong Kim (ETRI)
 * @date 2013. 9. 5.
 */

#ifndef __MG2410_NOS_H__
#define __MG2410_NOS_H__

#include "kconf.h"
#ifdef MG2410_M

#include <stm32f10x.h>
#include "nos_common.h"
#include "errorcodes.h"

#include "platform.h"
#include "ieee-802-15-4.h"
#include "mg2410-lib.h"


#define SYMBOL_DURATION 16

// IMR:Interrupt, EMR:Event(make a pulse without pending flag)
// PR: write 1 to clear (also clears SWIER)
#define BLOCK_RF_INTR()             do { NVIC_DisableIRQ(RF_NVIC_IRQN); } while(0)
#define UNBLOCK_RF_INTR()           do { NVIC_EnableIRQ(RF_NVIC_IRQN); } while(0)
#define ENABLE_RF_INTR()        	do { EXTI->IMR |= RF_EXTI_PIN; } while(0)
#define DISABLE_RF_INTR()       	do { EXTI->IMR &= ~RF_EXTI_PIN; } while(0)
#define SET_RF_INTR_FLAG()			do { EXTI_GenerateSWInterrupt(RF_EXTI_PIN); } while(0)
#define CLEAR_RF_INTR_FLAG()   		do { EXTI_ClearFlag(RF_EXTI_PIN); /*NVIC_ClearPendingIRQ(EXTI9_5_IRQn);*/ } while(0)
#define GET_RF_INTR_FLAG_STATUS()	(EXTI_GetFlagStatus(RF_EXTI_PIN))
#define GET_RF_INTR_STATUS()        (EXTI_GetITStatus(RF_EXTI_PIN))
#define GET_RF_INTR_PIN_STATUS()	(GPIO_ReadInputDataBit(RF_GPIO_PORT, RF_GPIO2_PIN))	//active low for very short time.

// memeber of WPAN_DEV (struct wpan_device);
struct mg2410_device
{
    volatile bool ack_rcvd;
    volatile bool tx_done;
    uint8_t rf_rx_frame[127];
    volatile uint8_t rf_rx_frame_length;
    volatile int8_t rf_rx_rssi;
//uint8_t rf_tx_frame[127];
//uint8_t rf_tx_frame_length;
//volatile int16_t rf_last_ack_seq;	// if ack is received in ISR (usually calls "read_frame" directly to check ack frame)
};
typedef struct mg2410_device MG2410;

void nos_mg2410_init(UINT8 dev_id);
void nos_mg2410_set_tx_power(UINT8 dev_id, int level);	// level: 0~18
void nos_mg2410_set_channel(UINT8 dev_id, int channel);	//channel:11~26
void nos_mg2410_isr(UINT8 dev_id);

void nos_mg2410_tx_test();









#endif //MG2410_M
#endif //__MG2410_NOS_H__
