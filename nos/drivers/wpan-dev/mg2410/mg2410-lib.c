
/*******************************************************************************
	- Chip		: MG2410
	- Vendor		: RadioPulse Inc, 2010.
	- Date		: 2011-03-15
	- Version		: VER 1.1b
	
	[2011-03-15] VER 1.1b
	- Enhanced Functions
		+ HAL_ChannelSet()

	[2010-09-14] VER 1.1
	- Enhanced Functions
		+ HAL_DmScaleGet()
		+ HAL_DataRateSet()
		+ HAL_FrequencySet()
		+ HAL_ChannelSet()
		+ HAL_RegisterConfig()
		+ HAL_RFTestMode()
	- Added Functions
		+ HAL_AGCSet()
	- Name Changed 
		+ HAL_ExtAddrSet/Get() --> HAL_IEEEAddrSet/Get()

	[2010-07-31] VER 1.0a
	- Enhanced Functions
		+ HAL_DataRateSet()
		+ HAL_FrequencySet()
		+ HAL_RegisterConfig()
		+ HAL_RFTestMode()
	- Added Functions	
		+ HAL_GpioDedicationSet()
		+ HAL_GpioModeSet()
		+ HAL_GpioGetBit()
		+ HAL_GpioGetAll()
		+ HAL_GpioSetBit()
		+ HAL_GpioSetMulti()
		+ HAL_IRQGpioSelect()
		+ HAL_TempSensorSet()
	
	[2010-07-10] VER 1.00
	- Initial Version
*******************************************************************************/


#include "MG2410.h"
#include "mg2410-lib.h"
#include "mg2410-nos.h"
#include "mg2410-interface.h"
#include "arch.h"   //delay function
#include "critical_section.h"

/***********************************************************************************
 *
 *	NAME		: HAL_Reset()
 *
 *	DESCRIPTION	: RESETB is active low signal. For reset, RESETB should be low.
 *
 * 	PARAMETER	: u8Ena - 1=Reset, 0=Normal
 *
 * 	RETURN		: void
 *
 * 	NOTES		: Callback function, CB_Control_RESETB(), which controls MG2410's RESETB pin 
 *					should be implemented by users.
 *	
 ***********************************************************************************/
void HAL_Reset(UINT8 dev_id, UINT8 u8Ena)
{
	if(u8Ena)
	{
		CB_Control_RESETB(dev_id, 0);		// Active Low
	}
	else
	{
		CB_Control_RESETB(dev_id, 1);
	}
}

/***********************************************************************************
 *
 *	NAME		: HAL_DeepSleep()
 *
 *	DESCRIPTION	: DVREG_EN is active low signal. For deep sleep, DVREG_EN should be low.
 *
 * 	PARAMETER	: u8Ena - 1=Deep sleep, 0=Normal
 *
 * 	RETURN		: void
 *
 * 	NOTES		: Callback function, CB_Control_DVREG_EN(), which controls MG2410's DVREG_EN pin
 *					should be implemented by users.
 *	
 ***********************************************************************************/
void HAL_DeepSleep(UINT8 dev_id, UINT8 u8Ena)
{
	if(u8Ena)
	{
		CB_Control_DVREG_EN(dev_id, 0);	// 0 --> DVREG OFF
	}
	else
	{
		CB_Control_DVREG_EN(dev_id, 1);	// 1 --> DVREG ON	
	}
}

/***********************************************************************************
 *
 *	NAME		: HAL_RegisterWrite()
 *
 *	DESCRIPTION	: Writes to MG2410's register.
 *
 * 	PARAMETER	: u16RegAddr - Register to be written.
 *				  u8RegValue - Value to be written. 
 *
 * 	RETURN		: void
 *
 * 	NOTES		: Following callback functions should be implemented by users.
 *					CB_IRQInterruptOff() - prevents the interrupt by MG2410's IRQ pin.
 *					CB_IRQInterruptOn() - allows the interrupt by MG2410's IRQ pin. 
 *					CB_Control_CSn() - controls MG2410's CSn pin.
 *				  	CB_SpiWriteByte() - generates a SPI clock cycle and send 1 byte.
 *	
 ***********************************************************************************/
void HAL_RegisterWrite(UINT8 dev_id, UINT16 u16RegAddr, UINT8 u8RegValue)
{
	UINT8	au8WriteByte[4];

	au8WriteByte[0] = MG2410_SpiCmd_RegWr_Byte1;
	au8WriteByte[1] = (u16RegAddr & 0x0F00) >> 5;		// b[6:3] = high_address[3:0]
	au8WriteByte[1] |= (u16RegAddr & 0x00E0) >> 5;		// b[2:0] = low_address[7:5]
	au8WriteByte[2] = MG2410_SpiCmd_RegWr_Byte3;		// b[7:6] = SPI_CMD2
	au8WriteByte[2] |= (u16RegAddr & 0x001F);			// b[4:0] = low_address[4:0]
	au8WriteByte[3] = u8RegValue;

	CB_IRQInterruptOff(dev_id);
	CB_Control_CSn(dev_id, 0);
	CB_SpiWriteByte(dev_id, au8WriteByte[0]);
	CB_SpiWriteByte(dev_id, au8WriteByte[1]);
	CB_SpiWriteByte(dev_id, au8WriteByte[2]);	
	CB_SpiWriteByte(dev_id, au8WriteByte[3]);
	CB_Control_CSn(dev_id, 1);
	CB_IRQInterruptOn(dev_id);
}

/***********************************************************************************
 *
 *	NAME		: HAL_RegisterRead()
 *
 *	DESCRIPTION	: Reads MG2410's register.
 *
 * 	PARAMETER	: u16RegAddr - Register to be read.
 *
 * 	RETURN		: UINT8. Value of the register.
 *
 * 	NOTES		: Following callback functions should be implemented by users.
 *					CB_IRQInterruptOff() - prevents the interrupt by MG2410's IRQ pin.
 *					CB_IRQInterruptOn() - allows the interrupt by MG2410's IRQ pin. 
 *					CB_Control_CSn() - controls MG2410's CSn pin.
 *				  	CB_SpiWriteByte() - generates a SPI clock cycle and send 1 byte.
 *	
 ***********************************************************************************/
UINT8 HAL_RegisterRead(UINT8 dev_id, UINT16 u16RegAddr)
{
	UINT8	au8WriteByte[4];
	UINT8	u8ReadByte;

	au8WriteByte[0] = MG2410_SpiCmd_RegRd_Byte1;
	au8WriteByte[1] = (u16RegAddr & 0x0F00) >> 5;		// b[6:3] = high_address[3:0]
	au8WriteByte[1] |= (u16RegAddr & 0x00E0) >> 5;	// b[2:0] = low_address[7:5]
	au8WriteByte[2] = MG2410_SpiCmd_RegRd_Byte3;		// b[7:6] = SPI_CMD2
	au8WriteByte[2] |= (u16RegAddr & 0x001F);			// b[4:0] = low_address[4:0]
	au8WriteByte[3] = 0x00;

	CB_IRQInterruptOff(dev_id);
	CB_Control_CSn(dev_id, 0);
	CB_SpiWriteByte(dev_id, au8WriteByte[0]);
	CB_SpiWriteByte(dev_id, au8WriteByte[1]);
	CB_SpiWriteByte(dev_id, au8WriteByte[2]);	
	u8ReadByte = CB_SpiWriteByte(dev_id, au8WriteByte[3]);
	CB_Control_CSn(dev_id, 1);
	CB_IRQInterruptOn(dev_id);

	return	u8ReadByte;
}

/***********************************************************************************
 *
 *	NAME		: HAL_TxFifoPushSingle()
 *
 *	DESCRIPTION	: Write 1 byte to MG2410's TXFIFO. The write pointer is indicated by MTFCWP 
 *					register. After writing, MTFCWP is increased by 1.
 *
 * 	PARAMETER	: u8PushData - Data to be written.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: Following callback functions should be implemented by users.
 *					CB_IRQInterruptOff() - prevents the interrupt by MG2410's IRQ pin.
 *					CB_IRQInterruptOn() - allows the interrupt by MG2410's IRQ pin. 
 *					CB_Control_CSn() - controls MG2410's CSn pin.
 *				  	CB_SpiWriteByte() - generates a SPI clock cycle and send 1 byte.
 *	
 ***********************************************************************************/
void HAL_TxFifoPushSingle(UINT8 dev_id, UINT8 u8PushData)
{
	CB_IRQInterruptOff(dev_id);
	CB_Control_CSn(dev_id, 0);
	CB_SpiWriteByte(dev_id, MG2410_SpiCmd_SinglePush);
	CB_SpiWriteByte(dev_id, u8PushData);
	CB_Control_CSn(dev_id, 1);
	CB_IRQInterruptOn(dev_id);
}

/***********************************************************************************
 *
 *	NAME		: HAL_TxFifoPushBurst()
 *
 *	DESCRIPTION	: Write multiple bytes to MG2410's TXFIFO. The write pointer is indicated by
 *					MTFCWP register. After writing, MTFCWP is increased by u8Len.
 *
 * 	PARAMETER	: pu8PushBuff - Pointer to buffer of data to be written.
 *				  u8Len - Length of writing. (0 ~ 127)
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8Len is out of range).
 *
 * 	NOTES		: Following callback functions should be implemented by users.
 *					CB_IRQInterruptOff() - prevents the interrupt by MG2410's IRQ pin.
 *					CB_IRQInterruptOn() - allows the interrupt by MG2410's IRQ pin. 
 *					CB_Control_CSn() - controls MG2410's CSn pin.
 *				  	CB_SpiWriteByte() - generates a SPI clock cycle and send 1 byte.
 *					CB_SpiWriteBurstByte() - generate lots of SPI clock cycle and send multiple bytes.
 *	
 ***********************************************************************************/
UINT8 HAL_TxFifoPushBurst(UINT8 dev_id, UINT8* pu8PushBuff, UINT8 u8Len)
{
	if(u8Len > 128)
	{
		return 1;
	}

	CB_IRQInterruptOff(dev_id);
	CB_Control_CSn(dev_id, 0);
	CB_SpiWriteByte(dev_id, MG2410_SpiCmd_BurstPush);
	CB_SpiWriteBurstByte(dev_id, pu8PushBuff, u8Len);
	CB_Control_CSn(dev_id, 1);
	CB_IRQInterruptOn(dev_id);

	return	0;
}

/***********************************************************************************
 *
 *	NAME		: HAL_RxFifoPopSingle()
 *
 *	DESCRIPTION	: Read 1 byte from MG2410's RXFIFO. The read pointer is indicated by MRFCRP 
 *					register. After reading, MRFCRP is increased by 1.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT8. Value of read RXFIFO.
 *
 * 	NOTES		: Following callback functions should be implemented by users.
 *					CB_IRQInterruptOff() - prevents the interrupt by MG2410's IRQ pin.
 *					CB_IRQInterruptOn() - allows the interrupt by MG2410's IRQ pin. 
 *					CB_Control_CSn() - controls MG2410's CSn pin.
 *				  	CB_SpiWriteByte() - generates a SPI clock cycle and send 1 byte.
 *	
 ***********************************************************************************/
UINT8 HAL_RxFifoPopSingle(UINT8 dev_id)
{
	UINT8	u8PopData;

	CB_IRQInterruptOff(dev_id);
	CB_Control_CSn(dev_id, 0);
	CB_SpiWriteByte(dev_id, MG2410_SpiCmd_SinglePop);
	u8PopData = CB_SpiWriteByte(dev_id, 0x00);
	CB_Control_CSn(dev_id, 1);
	CB_IRQInterruptOn(dev_id);

	return	u8PopData;
}

/***********************************************************************************
 *
 *	NAME		: HAL_RxFifoPopBurst()
 *
 *	DESCRIPTION	: Read multiple bytes from MG2410's RXFIFO. The read pointer is indicated by 
 *					MRFCRP register. After reading, MRFCRP is increased by u8Len.
 *
 * 	PARAMETER	: pu8PopBuff - Pointer to buffer in which read data is stored.
 *				  u8Len - Length of reading. (1 ~ 255)
 *
 * 	RETURN		: void
 *
 * 	NOTES		: Following callback functions should be implemented by users.
 *					CB_IRQInterruptOff() - prevents the interrupt by MG2410's IRQ pin.
 *					CB_IRQInterruptOn() - allows the interrupt by MG2410's IRQ pin. 
 *					CB_Control_CSn() - controls MG2410's CSn pin.
 *				  	CB_SpiWriteByte() - generates a SPI clock cycle and send 1 byte.
 *	
 ***********************************************************************************/
#if 0
void HAL_RxFifoPopBurst(UINT8 dev_id, UINT8* pu8PopBuff, UINT8 u8Len)
{
	UINT16	iw;
	UINT8	u8TunedLen;

	if(u8Len == 0)
	{
		return;
	}

	u8TunedLen = u8Len - 1;
//	u8TunedLen = u8Len;

	CB_IRQInterruptOff(dev_id);
	CB_Control_CSn(dev_id, 0);
	CB_SpiWriteByte(dev_id, MG2410_SpiCmd_BurstPop);
	//for(iw=0 ; iw<u8Len ; iw++)
	for(iw=0 ; iw<u8TunedLen ; iw++)		// FOR BUG
	{
		pu8PopBuff[iw] = CB_SpiWriteByte(dev_id, 0x00);
	}
	CB_Control_CSn(dev_id, 1);	
	CB_IRQInterruptOn(dev_id);
}
#else
void HAL_RxFifoPopBurst(UINT8 dev_id, UINT8* pu8PopBuff, UINT8 u8Len)
{
    UINT16 iw;
    UINT8 new_rd_ptr; // for bug handling

    if (u8Len == 0)
    {
        return;
    }

    new_rd_ptr = HAL_RegisterRead(dev_id, xMRFCRP);

	CB_IRQInterruptOff(dev_id);
	CB_Control_CSn(dev_id, 0);
	CB_SpiWriteByte(dev_id, MG2410_SpiCmd_BurstPop);
    for (iw = 0; iw < u8Len; iw++)
    {
        pu8PopBuff[iw] = CB_SpiWriteByte(dev_id, 0x00);
    }
	CB_Control_CSn(dev_id, 1);	
	CB_IRQInterruptOn(dev_id);
	
    //bug handling.
    new_rd_ptr += iw;
    HAL_RegisterWrite(dev_id, xMRFCRP, new_rd_ptr);
}
#endif




/***********************************************************************************
 *
 *	NAME		: HAL_TxFifoWrPtrSet()
 *
 *	DESCRIPTION	: Set TXFIFO's write pointer (Write MTFCWP register).
 *
 * 	PARAMETER	: u8WrPtr - Value to be written.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_TxFifoWrPtrSet(UINT8 dev_id, UINT8 u8WrPtr)
{
	HAL_RegisterWrite(dev_id, xMTFCWP, u8WrPtr);
}

/***********************************************************************************
 *
 *	NAME		: HAL_TxFifoRdPtrSet()
 *
 *	DESCRIPTION	: Set TXFIFO's read pointer (Write MTFCRP register).
 *
 * 	PARAMETER	: u8RdPtr - Value to be written.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_TxFifoRdPtrSet(UINT8 dev_id, UINT8 u8RdPtr)
{
	HAL_RegisterWrite(dev_id, xMTFCRP, u8RdPtr);
}

/***********************************************************************************
 *
 *	NAME		: HAL_RxFifoWrPtrSet()
 *
 *	DESCRIPTION	: Set RXFIFO's write pointer (Write MRFCWP register).
 *
 * 	PARAMETER	: u8WrPtr - Value to be written.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_RxFifoWrPtrSet(UINT8 dev_id, UINT8 u8WrPtr)
{
	HAL_RegisterWrite(dev_id, xMRFCWP, u8WrPtr);
}

/***********************************************************************************
 *
 *	NAME		: HAL_RxFifoRdPtrSet()
 *
 *	DESCRIPTION	: Set RXFIFO's read pointer (Write MRFCRP register).
 *
 * 	PARAMETER	: u8RdPtr - Value to be written.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_RxFifoRdPtrSet(UINT8 dev_id, UINT8 u8RdPtr)
{
	HAL_RegisterWrite(dev_id, xMRFCRP, u8RdPtr);
}

/***********************************************************************************
 *
 *	NAME		: HAL_RxFifoRdPtrGet()
 *
 *	DESCRIPTION	: Get RXFIFO's read pointer (Read MRFCRP register).
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT8. Value of MRFCRP register. 
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_RxFifoRdPtrGet(UINT8 dev_id)
{
	UINT8	u8RdPtr;

	u8RdPtr = HAL_RegisterRead(dev_id, xMRFCRP);
	return	u8RdPtr;
}

/***********************************************************************************
 *
 *	NAME		: HAL_RxFifoRdPtrAdd()
 *
 *	DESCRIPTION	: Increase RXFIFO's read pointer (Add MRFCRP register by u8Add).
 *
 * 	PARAMETER	: u8Add - Value to add
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_RxFifoRdPtrAdd(UINT8 dev_id, UINT8 u8Add)
{
	UINT8	u8NewMRFCRP;

	u8NewMRFCRP = HAL_RegisterRead(dev_id, xMRFCRP);
	u8NewMRFCRP += u8Add;
	HAL_RegisterWrite(dev_id, xMRFCRP, u8NewMRFCRP);
}

/***********************************************************************************
 *
 *	NAME		: HAL_DmScaleGet()
 *
 *	DESCRIPTION	: Increase RXFIFO's read pointer (Add MRFCRP register by u8Add).
 *
 * 	PARAMETER	: u8CpsMode2M - 1=2Mcps mode. 0=4Mcps mode.
 *				  u16Freq_MHz - Center frequency.
 *
 * 	RETURN		: UINT8. The vaule of DMSCALE register.
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_DmScaleGet(UINT8 dev_id, UINT8 u8CpsMode2M, UINT16 u16Freq_MHz)
{
	UINT8	u8ChanDMScale;

	if(u8CpsMode2M == 0)
	{
		u8ChanDMScale = 236;
		if(u16Freq_MHz > 2400) u8ChanDMScale--;
		if(u16Freq_MHz > 2404) u8ChanDMScale--;
		if(u16Freq_MHz > 2409) u8ChanDMScale--;
		if(u16Freq_MHz > 2414) u8ChanDMScale--;
		if(u16Freq_MHz > 2419) u8ChanDMScale--;
		if(u16Freq_MHz > 2423) u8ChanDMScale--;
		if(u16Freq_MHz > 2428) u8ChanDMScale--;
		if(u16Freq_MHz > 2433) u8ChanDMScale--;
		if(u16Freq_MHz > 2438) u8ChanDMScale--;
		if(u16Freq_MHz > 2442) u8ChanDMScale--;
		if(u16Freq_MHz > 2447) u8ChanDMScale--;
		if(u16Freq_MHz > 2452) u8ChanDMScale--;
		if(u16Freq_MHz > 2457) u8ChanDMScale--;
		if(u16Freq_MHz > 2461) u8ChanDMScale--;
	}
	else		// 2Mcps Mode. Below 2.0Mbps Mode
	{
		u8ChanDMScale = 182;
		if(u16Freq_MHz > 2402) u8ChanDMScale--;
		if(u16Freq_MHz > 2417) u8ChanDMScale--;
		if(u16Freq_MHz > 2433) u8ChanDMScale--;
		if(u16Freq_MHz > 2448) u8ChanDMScale--;
		if(u16Freq_MHz > 2463) u8ChanDMScale--;
		if(u16Freq_MHz > 2479) u8ChanDMScale--;
		if(u16Freq_MHz > 2494) u8ChanDMScale--;
	}

	return u8ChanDMScale;
}

/***********************************************************************************
 *
 *	NAME		: HAL_DataRateSet()
 *
 *	DESCRIPTION	: Set data rate
 *
 * 	PARAMETER	: u8DataRate - Data rate mode. (0 ~ 10)
 *					0	= 31.25 Kbps
 *					1	= 62.50 Kbps
 *					2	= 125 Kbps
 *					3	= 250 Kbps
 *					4	= 500 Kbps
 *					5	= 1.0 Mbps
 *					6	= 1.3 Mbps
 *					7	= 1.5 Mbps
 *					8	= 2.0 Mbps
 *					9	= 2.6 Mbps
 *					10	= 3.0 Mbps
 *					11	= 4.0 Mbps
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8DataRate is out of range).
 *
 * 	NOTES		: 500 Kbps or above mode does not work yet.
 *	
 ***********************************************************************************/
UINT8	gu8HalState_DataRate = MG2410_DataRate_250K;
UINT16	gu16HalState_MaxWaitTimeForTx_100us = 80;		// 80 * 100us = 8ms
	
UINT8 HAL_DataRateSet(UINT8 dev_id, UINT8 u8DataRate)
{
	UINT8	u8MDMCNF3;
	UINT16	u16ReadFreq_MHz;
	UINT8	u8DMSCALE;

	if(u8DataRate > MG2410_DataRate_4000K)
	{
		return 1;
	}

	gu8HalState_DataRate = u8DataRate;

	if(u8DataRate >= MG2410_DataRate_2600K)	// 4Mcps mode
	{
		HAL_RegisterWrite(dev_id, xCLKCFG, 0x01);	// bit[1]=0
		HAL_RegisterWrite(dev_id, xDMKDIV1, 0x08);
		HAL_RegisterWrite(dev_id, xRXBW1, 0x59);

		u16ReadFreq_MHz = HAL_FrequencyGet(dev_id);
		u8DMSCALE = HAL_DmScaleGet(dev_id, 0, u16ReadFreq_MHz);
		HAL_RegisterWrite(dev_id, xDMSCALE, u8DMSCALE);		

		HAL_AGCSet(dev_id, 0, 0);
	}
	else
	{
		HAL_RegisterWrite(dev_id, xCLKCFG, 0x03);	// bit[1]=1
		HAL_RegisterWrite(dev_id, xDMKDIV1, 0x04);
		HAL_RegisterWrite(dev_id, xRXBW1, 0x7D);

		u16ReadFreq_MHz = HAL_FrequencyGet(dev_id);
		u8DMSCALE = HAL_DmScaleGet(dev_id, 1, u16ReadFreq_MHz);
		HAL_RegisterWrite(dev_id, xDMSCALE, u8DMSCALE);

		HAL_AGCSet(dev_id, 1, 0);
	}
	
	u8MDMCNF3 = HAL_RegisterRead(dev_id, xMDMCNF3);
	switch(u8DataRate)
	{
		case MG2410_DataRate_31K	:
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x0D);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((340+2) * 1.2);
			break;
		case MG2410_DataRate_62K	:		
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x09);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((171+2) * 1.2);
			break;
		case MG2410_DataRate_125K	:		
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x07);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((86+2) * 1.2);
			break;
		case MG2410_DataRate_250K	:
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x06);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((43+2) * 1.2);
			break;
		case MG2410_DataRate_500K	:			
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x05);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((23+2) * 1.2);
			break;
		case MG2410_DataRate_1000K	:
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x01);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((13+2) * 1.2);
			break;
		case MG2410_DataRate_1300K	:
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x02);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((11+2) * 1.2);
			break;
		case MG2410_DataRate_1500K	:
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x03);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((10+2) * 1.2);
			break;
		case MG2410_DataRate_2000K	:
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x00);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((8+2) * 1.2);
			break;		
		case MG2410_DataRate_2600K	:
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x02);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((6+2) * 1.2);
			break;		
		case MG2410_DataRate_3000K	:
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x03);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((5+2) * 1.2);
			break;
		case MG2410_DataRate_4000K	:
			HAL_RegisterWrite(dev_id, xMDMCNF3,(u8MDMCNF3 & 0xF0) | 0x00);
			gu16HalState_MaxWaitTimeForTx_100us = (UINT16) ((4+2) * 1.2);
			break;		
	}

	if(u8DataRate > MG2410_DataRate_62K)
	{
		HAL_RegisterWrite(dev_id, xCORCNF1, 0x00);		
	}
	else
	{
		HAL_RegisterWrite(dev_id, xCORCNF1, 0x01);
	}

	if(u8DataRate >= MG2410_DataRate_2600K)	// 4Mcps mode
	{	
		HAL_RegisterWrite(dev_id, xCORCNF2, 0x9F);
	}
	else
	{
		HAL_RegisterWrite(dev_id, xCORCNF2, 0x85);
	}

	return	0;
}

/***********************************************************************************
 *
 *	NAME		: HAL_FrequencySet()
 *
 *	DESCRIPTION	: Set the RF frequency in MHz.
 *
 * 	PARAMETER	: u16Freq_MHz - Center frequency. (MHz, 2394 ~ 2507)
 *
 * 	RETURN		: UINT8. Result. 
 *					0 = Successful
 *					1 = Failed (u16Freq_MHz is out of range).
 *					2 = Failed (MG2410 is not locked).
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
 UINT8 HAL_FrequencySet(UINT8 dev_id, UINT16 u16Freq_MHz)
{
	UINT8	u8ChanOffset;
	UINT8	u8RegRead;
	UINT8	u8Locked;
	UINT8	u8ChanDMScale;

	if( (u16Freq_MHz < 2394) || (u16Freq_MHz > 2507) )
	{
		return 1;
	}
	
	u8ChanOffset = u16Freq_MHz - 2394;

	HAL_RegisterWrite(dev_id, xPLLCTRL, 0x2A);
	HAL_RegisterWrite(dev_id, xPLLFREQ, u8ChanOffset);
	HAL_RegisterWrite(dev_id, xPLLCTRL, 0x29);

	u8RegRead = HAL_RegisterRead(dev_id, xCLKCFG);
	u8RegRead &= 0x02;		// cps mode
	if(u8RegRead == 0)	// 4Mcps Mode. Above 2.6Mbps Mode
	{
		u8ChanDMScale = HAL_DmScaleGet(dev_id, 0, u16Freq_MHz);
	}
	else		// 2Mcps Mode. Below 2.0Mbps Mode
	{
		u8ChanDMScale = HAL_DmScaleGet(dev_id, 1, u16Freq_MHz);		
	}
	HAL_RegisterWrite(dev_id, xDMSCALE, u8ChanDMScale);

	u8Locked = 0;
#if 0
	CB_HALTimerSet(MAX_WAIT_100us_TICK_ForLockCheck);
	while(CB_HALTimerGet())
	{
		u8RegRead = HAL_RegisterRead(dev_id, xPLLMON);
		if(u8RegRead & 0x01)	// PLL LOCK
		{
			u8Locked = 1;
			break;
		}		
	}
	CB_HALTimerSet(0);
#else
	// max: 1000us
	for (int i=0; i < 1000; i++)
	{
		u8RegRead = HAL_RegisterRead(dev_id, xPLLMON);
		if(u8RegRead & 0x01)	// PLL LOCK
		{
			u8Locked = 1;
			break;
		}
		nos_delay_us(1);
	}
#endif
	if(u8Locked == 0)
	{
		return 2;
	}
	
	return	0;	
}

/***********************************************************************************
 *
 *	NAME		: HAL_FrequencyGet()
 *
 *	DESCRIPTION	: Get the RF frequency in MHz.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT16. Frequency to be read
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT16 HAL_FrequencyGet(UINT8 dev_id)
{
	UINT8	u8ChanOffset;
	UINT16	u8ChanRead;

	u8ChanOffset = HAL_RegisterRead(dev_id, xPLLFREQ) & 0x7F;
	u8ChanRead = 2394 + u8ChanOffset;

	return	u8ChanRead;
}

/***********************************************************************************
 *
 *	NAME		: HAL_ChannelSet()
 *
 *	DESCRIPTION	: Set the RF channel by number defined by IEEE 802.15.4
 *
 * 	PARAMETER	: u8ChanNum - Number of RF channel.
 *					11	= 2405 MHz
 *					12	= 2410 MHz
 *					13	= 2415 MHz
 *					14	= 2420 MHz
 *					15	= 2425 MHz
 *					16	= 2430 MHz
 *					17	= 2435 MHz
 *					18	= 2440 MHz
 *					19	= 2445 MHz
 *					20	= 2450 MHz
 *					21	= 2455 MHz
 *					22	= 2460 MHz
 *					23	= 2465 MHz
 *					24	= 2470 MHz
 *					25	= 2475 MHz
 *					26	= 2480 MHz
 *
 * 	RETURN		: UINT8. Result. 
 *					0=Successful
 *					1=Failed (u8ChanNum is out of range)
 *					2=Failed (Locking is failed)
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 	gu8HalState_ChanNum = 19;
UINT8 HAL_ChannelSet(UINT8 dev_id, UINT8 u8ChanNum)
{
	UINT16	u16Freq_MHz;
	UINT8	u8Status;

	if( (u8ChanNum > 26) || (u8ChanNum < 11) )
	{
		return 1;
	}

	HAL_RegisterWrite(dev_id, xPCMD0, 0x34);
	HAL_RxEnable(dev_id, 0);

	gu8HalState_ChanNum = u8ChanNum;
	u16Freq_MHz = 2405 + 5*(u8ChanNum - 11);
	u8Status = HAL_FrequencySet(dev_id, u16Freq_MHz);
	if(u8Status)	// If failed, retry
	{
		u8Status = HAL_FrequencySet(dev_id, u16Freq_MHz);
	}

	HAL_RegisterWrite(dev_id, xPCMD0, 0x3C);
	HAL_RxFifoReset(dev_id);
	HAL_RxEnable(dev_id, 1);

	return u8Status;
}

/***********************************************************************************
 *
 *	NAME		: HAL_ChannelGet()
 *
 *	DESCRIPTION	: Get the number of RF channel defined by IEEE 802.15.4
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT8. The number of RF channel.
 *					11	= 2405 MHz
 *					12	= 2410 MHz
 *					13	= 2415 MHz
 *					14	= 2420 MHz
 *					15	= 2425 MHz
 *					16	= 2430 MHz
 *					17	= 2435 MHz
 *					18	= 2440 MHz
 *					19	= 2445 MHz
 *					20	= 2450 MHz
 *					21	= 2455 MHz
 *					22	= 2460 MHz
 *					23	= 2465 MHz
 *					24	= 2470 MHz
 *					25	= 2475 MHz
 *					26	= 2480 MHz 
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_ChannelGet(UINT8 dev_id)
{
	UINT16	u16Freq_MHz;
	UINT8	u8ChanNum;

	u16Freq_MHz = HAL_FrequencyGet(dev_id);

	u8ChanNum = (u16Freq_MHz - 2405)/5 + 11;

	return	u8ChanNum;	
}

void HAL_AGCSet(UINT8 dev_id, UINT8 u8CpsMode2M, UINT8 u8Initialize)
{
	if(u8Initialize)
	{
		HAL_RegisterWrite(dev_id, xAGCCNF0, 0xD0);
		HAL_RegisterWrite(dev_id, xAGCCNF1, 0x00);
//		HAL_RegisterWrite(dev_id, xAGCCNF2, 0xC0);		// BA
		HAL_RegisterWrite(dev_id, xAGCCNF3, 0x23);
		HAL_RegisterWrite(dev_id, xAGCCNF4, 0x3B);
		HAL_RegisterWrite(dev_id, xAGCCNF5, 0x0F);
		HAL_RegisterWrite(dev_id, xAGCCNF6, 0x4E);
		HAL_RegisterWrite(dev_id, xAGCCNF7, 0x5B);
		HAL_RegisterWrite(dev_id, xAGCCNF8, 0x4E);
		HAL_RegisterWrite(dev_id, xAGCCNF9, 0x37);
//		HAL_RegisterWrite(dev_id, xAGCCNF11, 0x7C);		// AB
//		HAL_RegisterWrite(dev_id, xAGCCNF12, 0x90);		// 37
//		HAL_RegisterWrite(dev_id, xAGCCNF15, 0x1D);		// 05
//		HAL_RegisterWrite(dev_id, xAGCCNF16, 0x1E);		// 05
//		HAL_RegisterWrite(dev_id, xAGCCNF17, 0x11);		// 05
		HAL_RegisterWrite(dev_id, xAGCCNF18, 0x0F);
		HAL_RegisterWrite(dev_id, xAGCCNF19, 0x0E);
		HAL_RegisterWrite(dev_id, xAGCCNF20, 0x1B);
		HAL_RegisterWrite(dev_id, xAGCCNF21, 0x2C);
		HAL_RegisterWrite(dev_id, xAGCCNF22, 0x2C);
		HAL_RegisterWrite(dev_id, xAGCCNF23, 0x00);
		HAL_RegisterWrite(dev_id, xAGCCNF24, 0x72);
		HAL_RegisterWrite(dev_id, xAGCCNF25, 0x77);
		HAL_RegisterWrite(dev_id, xAGCCNF26, 0xF2);
//		HAL_RegisterWrite(dev_id, xAGCCNF28, 0x37);		// 47
		HAL_RegisterWrite(dev_id, xAGCCNF30, 0x00);		
	}

	if(u8CpsMode2M)
	{
		HAL_RegisterWrite(dev_id, xAGCCNF2, 0xC0);
		HAL_RegisterWrite(dev_id, xAGCCNF11, 0x7C);
		HAL_RegisterWrite(dev_id, xAGCCNF12, 0x90);
		HAL_RegisterWrite(dev_id, xAGCCNF15, 0x1D);
		HAL_RegisterWrite(dev_id, xAGCCNF16, 0x1E);
		HAL_RegisterWrite(dev_id, xAGCCNF17, 0x11);
		HAL_RegisterWrite(dev_id, xAGCCNF28, 0x37);
	}
	else
	{
		HAL_RegisterWrite(dev_id, xAGCCNF2, 0xBA);
		HAL_RegisterWrite(dev_id, xAGCCNF11, 0xAB);
		HAL_RegisterWrite(dev_id, xAGCCNF12, 0x37);
		HAL_RegisterWrite(dev_id, xAGCCNF15, 0x05);
		HAL_RegisterWrite(dev_id, xAGCCNF16, 0x05);
		HAL_RegisterWrite(dev_id, xAGCCNF17, 0x05);
		HAL_RegisterWrite(dev_id, xAGCCNF28, 0x47);
	}
	
}

/***********************************************************************************
 *
 *	NAME		: HAL_RegisterConfig()
 *
 *	DESCRIPTION	: Settling registers of MG2410 basically.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT8. Result.
 *					0 = Successful
 *					1 = Failed (DCC is not calibrated for specified period).
 *					2 = Failed (Modem is not ON for specified period).
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_RegisterConfig(UINT8 dev_id)
{
	UINT8	u8DCCCON;
	UINT8	u8Status;

	HAL_RegisterWrite(dev_id, xEXTCLK, 0x0F);

	// PLL Register	
 	HAL_RegisterWrite(dev_id, xPLLVCO1, 0xA2);
 	HAL_RegisterWrite(dev_id, xPLLVCO2, 0x53);
 	HAL_RegisterWrite(dev_id, xPLLAFC, 0x8A);
 
	// RX Register	
 	HAL_RegisterWrite(dev_id, xRXCTRL, 0x37);
	HAL_RegisterWrite(dev_id, xRXBW1, 0x7D);

	// Etc.
	HAL_RegisterWrite(dev_id, xRSVD1, 0x80);	
 	HAL_RegisterWrite(dev_id, xRXBW2, 0x00);
 	
	// Voltage Regulator
	HAL_RegisterWrite(dev_id, xAVREG, 0x13);
 
	// DCC
	//HAL_RegisterWrite(dev_id, xCLKCFG, 0x01);
 	HAL_RegisterWrite(dev_id, xDCCDAC, 0x00);
	HAL_RegisterWrite(dev_id, xPLLPD, 0xC0);
 	HAL_RegisterWrite(dev_id, xRXRFPD, 0x23);
 	HAL_RegisterWrite(dev_id, xDCCCNF, 0x0C);
 	HAL_RegisterWrite(dev_id, xDCCCON, 0x02);

	u8Status = 1;
#if 0
	CB_HALTimerSet(MAX_WAIT_100us_TICK_ForDccCheck);
	while(CB_HALTimerGet())
	{
		u8DCCCON = HAL_RegisterRead(dev_id, xDCCCON);
		if( (u8DCCCON & 0x02) == 0)
		{
			u8Status = 0;
			break;
		}	
	}
	CB_HALTimerSet(0);
#else
	// max: 1000us
	for (int i=0; i < 1000; i++)
	{
		u8DCCCON = HAL_RegisterRead(dev_id, xDCCCON);
		if( (u8DCCCON & 0x02) == 0)
		{
			u8Status = 0;
			break;
		}
		nos_delay_us(1);
	}
#endif
	if(u8Status)
	{
		return 1;
	}

 	HAL_RegisterWrite(dev_id, xDCCCNF, 0x1C);
 	HAL_RegisterWrite(dev_id, xDCCCON, 0x04);
	//
	HAL_RegisterWrite(dev_id, xPCMD1, 0x00);
	HAL_RegisterWrite(dev_id, xPLLPD, 0xff);
	HAL_RegisterWrite(dev_id, xPLLPU, 0x00);
	HAL_RegisterWrite(dev_id, xRXRFPD, 0xff);
	// Calculate Auto-Channel Allocation Value	

 	// Modem
	HAL_RegisterWrite(dev_id, xCORCNF0, 0x80);
	HAL_RegisterWrite(dev_id, xCORCNF2, 0x85);
	HAL_RegisterWrite(dev_id, xCORCNF4, 0x29);
	HAL_RegisterWrite(dev_id, xCORCNF7, 0x69);
	HAL_RegisterWrite(dev_id, xCORCNF8, 0x00);

	// AGC
	HAL_AGCSet(dev_id, 1, 1);

	//
	HAL_RegisterWrite(dev_id, xCLKCFG, 0x03);

	// Modem Init
	u8Status = HAL_FrequencySet(dev_id, 2445);

	HAL_RegisterWrite(dev_id, xPLLPU, 0xFF);
	HAL_RegisterWrite(dev_id, xPCMD0,0x1C);

	//
	// Wait For Modem-On Interrupt
	//
	HAL_RegisterWrite(dev_id, xMONCON3, 0x1F);
	HAL_RegisterWrite(dev_id, xMONCON0, 0x24);	// 0x04(GPIO2/5 dedicaton) or 0x24(GPIO2 dedication)
	HAL_RegisterWrite(dev_id, xINTCON, MG2410_INT_ModemOn);		// Enable Modem-On Interrupt
	HAL_RegisterWrite(dev_id, xPCMD0, 0x2C);		// Modem-On Request

	u8Status = 1;
#if 0	
	CB_HALTimerSet(MAX_WAIT_100us_TICK_ForModemOn);
	while(CB_HALTimerGet())
	{
		if(CB_ModemOnCheck())
		{
			u8Status = 0;
			break;
		}
	}
	CB_HALTimerSet(0);
#else
	NOS_ENTER_CRITICAL_SECTION();	// no meaning. just in case.
	for (int i=0; i < 1000; i++)
	{
		if (GET_RF_INTR_PIN_STATUS()==0)	//NOS does not allow to use interrupt while initialization. 
		{
			u8Status = 0;
			break;
		}
		nos_delay_us(1);
	}
	// clear interrupt
	HAL_InterruptGetAndClear(dev_id);
	CLEAR_RF_INTR_FLAG();
	NOS_EXIT_CRITICAL_SECTION();
#endif
	if(u8Status)
	{
		return 2;
	}

	HAL_RegisterWrite(dev_id, xINTCON, 0x00);	// Disable Modem-On Interrupt
	HAL_RegisterWrite(dev_id, xCCA0, 0xEC);	

	//
	HAL_RegisterWrite(dev_id, xMDMCNF6, 0x08);	// AutoAck Delay
	HAL_RegisterWrite(dev_id, xDMSCALE, 0xB3);
	//

	HAL_ClockModeSet(dev_id, 0);

	return	0; 	
}

/***********************************************************************************
 *
 *	NAME		: HAL_TxPowerSet()
 *
 *	DESCRIPTION	: Set transmitting power.
 *
 * 	PARAMETER	: u8TxPowerLevel - level of transmitting power
 *					0	= 9.7 dBm
 *					1	= 9.0 dBm
 *					2	= 8.0 dBm
 *					3	= 7.0 dBm
 *					4	= 6.0 dBm
 *					5	= 5.0 dBm
 *					6	= 4.0 dBm
 *					7	= 3.0 dBm
 *					8	= 2.0 dBm
 *					9	= 1.0 dBm
 *					10	= 0.0 dBm
 *					11	= -1.0 dBm
 *					12	= -2.0 dBm
 *					13	= -3.0 dBm
 *					14	= -4.0 dBm
 *					15	= -5.0 dBm
 *					16	= -6.0 dBm
 *					17	= -7.0 dBm
 *					18	= -8.0 dBm
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8TxPowerLevel is out of range).
 *
 * 	NOTES		: After canceling, MG2410 generates a TX-End interrupt.
 *	
 ***********************************************************************************/
UINT8 HAL_TxPowerSet(UINT8 dev_id, UINT8 u8TxPowerLevel)
{
	if(u8TxPowerLevel > 18)
	{
		return 1;
	}

	switch(u8TxPowerLevel)
	{
		case 0	:	HAL_RegisterWrite(dev_id, xTXPA, 0x1F);	HAL_RegisterWrite(dev_id, xTXDA, 0x1F);	break;
		case 1	:	HAL_RegisterWrite(dev_id, xTXPA, 0x1E);	HAL_RegisterWrite(dev_id, xTXDA, 0x1C);	break;
		case 2	:	HAL_RegisterWrite(dev_id, xTXPA, 0x1C);	HAL_RegisterWrite(dev_id, xTXDA, 0x1E);	break;
		case 3	:	HAL_RegisterWrite(dev_id, xTXPA, 0x1B);	HAL_RegisterWrite(dev_id, xTXDA, 0x1B);	break;
		case 4	:	HAL_RegisterWrite(dev_id, xTXPA, 0x19);	HAL_RegisterWrite(dev_id, xTXDA, 0x1D);	break;
		case 5	:	HAL_RegisterWrite(dev_id, xTXPA, 0x19);	HAL_RegisterWrite(dev_id, xTXDA, 0x1A);	break;
		case 6	:	HAL_RegisterWrite(dev_id, xTXPA, 0x15);	HAL_RegisterWrite(dev_id, xTXDA, 0x1D);	break;
		case 7	:	HAL_RegisterWrite(dev_id, xTXPA, 0x14);	HAL_RegisterWrite(dev_id, xTXDA, 0x1C);	break;
		case 8	:	HAL_RegisterWrite(dev_id, xTXPA, 0x12);	HAL_RegisterWrite(dev_id, xTXDA, 0x1F);	break;
		case 9	:	HAL_RegisterWrite(dev_id, xTXPA, 0x11);	HAL_RegisterWrite(dev_id, xTXDA, 0x1D);	break;
		case 10	:	HAL_RegisterWrite(dev_id, xTXPA, 0x0D);	HAL_RegisterWrite(dev_id, xTXDA, 0x1D);	break;
		case 11	:	HAL_RegisterWrite(dev_id, xTXPA, 0x11);	HAL_RegisterWrite(dev_id, xTXDA, 0x19);	break;
		case 12	:	HAL_RegisterWrite(dev_id, xTXPA, 0x0B);	HAL_RegisterWrite(dev_id, xTXDA, 0x1C);	break;
		case 13	:	HAL_RegisterWrite(dev_id, xTXPA, 0x0A);	HAL_RegisterWrite(dev_id, xTXDA, 0x1B);	break;
		case 14	:	HAL_RegisterWrite(dev_id, xTXPA, 0x0C);	HAL_RegisterWrite(dev_id, xTXDA, 0x12);	break;
		case 15	:	HAL_RegisterWrite(dev_id, xTXPA, 0x0B);	HAL_RegisterWrite(dev_id, xTXDA, 0x12);	break;
		case 16	:	HAL_RegisterWrite(dev_id, xTXPA, 0x0A);	HAL_RegisterWrite(dev_id, xTXDA, 0x12);	break;
		case 17	:	HAL_RegisterWrite(dev_id, xTXPA, 0x09);	HAL_RegisterWrite(dev_id, xTXDA, 0x12);	break;
		case 18	:	HAL_RegisterWrite(dev_id, xTXPA, 0x0A);	HAL_RegisterWrite(dev_id, xTXDA, 0x09);	break;
		default	:	break;
	}

	return	0;
}

/***********************************************************************************
 *
 *	NAME		: HAL_EnergyLevelGet()
 *
 *	DESCRIPTION	: Get the energy detection level
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: INT8. Value of energy detection level. This is 2's complementary value.
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
INT8 HAL_EnergyLevelGet(UINT8 dev_id)
{
	INT8		i8EnergyLevel;

	i8EnergyLevel = HAL_RegisterRead(dev_id, xAGCSTS2);

	return	i8EnergyLevel;	
}

/***********************************************************************************
 *
 *	NAME		: HAL_CCAEnable()
 *
 *	DESCRIPTION	: Enable or disable CCA function. And, Set CCA's mode.
 *
 * 	PARAMETER	: u8Ena - 0=Disable, 1=Enable.
 *				  u8CcaMode - 0=ED, 1=CD, 2=FD, 3=Reserved
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8CcaMode is out of range).
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_CCAEnable(UINT8 dev_id, UINT8 u8Ena, UINT8 u8CcaMode)
{
	UINT8	u8CCA0;
	
	if(u8CcaMode > 2)	return 1;

	if(u8Ena)
	{
		u8CCA0 = 0x10 | u8CcaMode;
	}
	else
	{
		u8CCA0 = 0x30;
	}
	HAL_RegisterWrite(dev_id, xCCA0, u8CCA0);

	return	0;	
}

/***********************************************************************************
 *
 *	NAME		: HAL_CCAThresholdSet()
 *
 *	DESCRIPTION	: Set threshold of CCA. The packet is transmitted when energy detection level is 
 *					lower than CCA threshold.
 *
 * 	PARAMETER	: i8Threshold_dBm - The threshold value. This is 2's complementary value.
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8CcaMode is out of range).
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_CCAThresholdSet(UINT8 dev_id, INT8 i8Threshold_dBm)
{
	HAL_RegisterWrite(dev_id, xCCA1, i8Threshold_dBm);
}

/***********************************************************************************
 *
 *	NAME		: HAL_LQIEnable()
 *
 *	DESCRIPTION	: Enable or disable LQI function.
 *
 * 	PARAMETER	: u8Ena - 0=Disable, 1=Enable.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_LQIEnable(UINT8 dev_id, UINT8 u8Ena)
{
	if(u8Ena)
	{
		HAL_RegisterWrite(dev_id, xLQICNF0, 0x08);
	}
	else
	{
		HAL_RegisterWrite(dev_id, xLQICNF0, 0x00);
	}
}

/***********************************************************************************
 *
 *	NAME		: HAL_LQIGet()
 *
 *	DESCRIPTION	: Get LQI of the last received packet.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT8. Value of LQI. Higher vaule means better quality.
 *
 * 	NOTES		: This function should be called when a packet is received. 
 *	
 ***********************************************************************************/
UINT8 HAL_LQIGet(UINT8 dev_id)
{
	UINT8	u8LQICNF1;
	
	u8LQICNF1 = HAL_RegisterRead(dev_id, xLQICNF1);
	
	return	u8LQICNF1;
}

/***********************************************************************************
 *
 *	NAME		: HAL_RssiGet()
 *
 *	DESCRIPTION	: Get RSSI of the last received packet.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: INT8. Value of RSSI. This is 2's complementary value.
 *
 * 	NOTES		: This function should be called when a packet is received. 
 *	
 ***********************************************************************************/
INT8 HAL_RssiGet(UINT8 dev_id)
{
	INT8		i8Rssi;

	i8Rssi = HAL_RegisterRead(dev_id, xAGCSTS3);

	return	i8Rssi;
}

/***********************************************************************************
 *
 *	NAME		: HAL_RFTestMode()
 *
 *	DESCRIPTION	: Set test mode for RF testing.
 *
 * 	PARAMETER	: u8TestMode - Test Mode
 *					0	: Disable test mode.
 *					1	: CW(Continuous Wave). Carrier Signal Generatioin.
 *					2	: CM(Continuous Modulated Spectrum). Modulated Signal Generation without IFS(Inter-Frame-Space)
 *
 * 	RETURN		: void
 *
 * 	NOTES		: This function is used for checking RF characteristics.
 *	
 ***********************************************************************************/
void HAL_RFTestMode(UINT8 dev_id, UINT8 u8TestMode)
{
	UINT8	u8ReadMDMCNF3; 

	if(u8TestMode == 1)		// CW
	{
		HAL_RegisterWrite(dev_id, xPCMD0, 0x1C);		// Modem-Off Request
		HAL_RegisterWrite(dev_id, xTST0, 0x20);		// Test Mode & CW Output
	}
	else if(u8TestMode == 2)	// CM Without IFS	
	{
		if(gu8HalState_DataRate >= MG2410_DataRate_2600K)
		{
			HAL_RegisterWrite(dev_id, xPCMD0, 0x1C);		// Modem-Off Request
			u8ReadMDMCNF3 = HAL_RegisterRead(dev_id, xMDMCNF3);
			HAL_RegisterWrite(dev_id, xMDMCNF3, (u8ReadMDMCNF3 & 0xE0) |0x01);	
			HAL_RegisterWrite(dev_id, xTST11, 0x01);
			HAL_RegisterWrite(dev_id, xTST2, 0xE0);
			HAL_RegisterWrite(dev_id, xTST8, 0x00);			
			HAL_RegisterWrite(dev_id, xMDMCNF9, 0x80);
			HAL_RegisterWrite(dev_id, xRXRFPU, 0x84);
			HAL_RegisterWrite(dev_id, xCLKFE0, 0x01);		// RXADC Clock
			HAL_RegisterWrite(dev_id, xCLKFE1, 0x10);		// TSTCLK Clock
			HAL_RegisterWrite(dev_id, xTST0, 0x22);
		}
		else if(gu8HalState_DataRate >= MG2410_DataRate_1000K)
		{
			HAL_RegisterWrite(dev_id, xPCMD0, 0x1C);		// Modem-Off Request
			u8ReadMDMCNF3 = HAL_RegisterRead(dev_id, xMDMCNF3);
			HAL_RegisterWrite(dev_id, xMDMCNF3, (u8ReadMDMCNF3 & 0xE0) |0x01);	
			HAL_RegisterWrite(dev_id, xTST11, 0x01);
			HAL_RegisterWrite(dev_id, xTST2, 0xE0);
			HAL_RegisterWrite(dev_id, xTST8, 0x00);
			HAL_RegisterWrite(dev_id, xMDMCNF9, 0x00);
			HAL_RegisterWrite(dev_id, xRXRFPU, 0x84);
			HAL_RegisterWrite(dev_id, xCLKFE0, 0x01);		// RXADC Clock
			HAL_RegisterWrite(dev_id, xCLKFE1, 0x10);		// TSTCLK Clock
			HAL_RegisterWrite(dev_id, xTST0, 0x22);
		}
		else
		{
			HAL_RegisterWrite(dev_id, xPCMD0, 0x1C);		// Modem-Off Request
			HAL_RegisterWrite(dev_id, xTST11, 0x00);
			HAL_RegisterWrite(dev_id, xTST2, 0x20);
			HAL_RegisterWrite(dev_id, xTST8, 0x00);
			HAL_RegisterWrite(dev_id, xMDMCNF9, 0x00);
			HAL_RegisterWrite(dev_id, xRXRFPU, 0x84);
			HAL_RegisterWrite(dev_id, xCLKFE0, 0x01);		// RXADC Clock
			HAL_RegisterWrite(dev_id, xCLKFE1, 0x10);		// TSTCLK Clock
			HAL_RegisterWrite(dev_id, xTST0, 0x22);
		}
	}
	else						// Disable
	{
		HAL_RegisterWrite(dev_id, xPLLPU, 0xFF);
		HAL_RegisterWrite(dev_id, xRXRFPD, 0xFF);
		HAL_RegisterWrite(dev_id, xRXRFPU, 0xFF);
		HAL_RegisterWrite(dev_id, xCLKFE1, 0x00);
		HAL_RegisterWrite(dev_id, xTST0, 0x80);
		HAL_RegisterWrite(dev_id, xTST8, 0x01);
		HAL_RegisterWrite(dev_id, xPCMD0, 0x2C);		// Modem-On

		HAL_RegisterWrite(dev_id, xCLKFE0, 0x00);		// RXADC Clock Off
		HAL_RegisterWrite(dev_id, xCLKFE1, 0x00);		// TSTCLK Clock Off
		HAL_RegisterWrite(dev_id, xTST11, 0x00);
		HAL_RegisterWrite(dev_id, xTST2, 0x25);
		HAL_RegisterWrite(dev_id, xMDMCNF9, 0x00);

		HAL_DataRateSet(dev_id, gu8HalState_DataRate);
	}

	HAL_ChannelSet(dev_id, gu8HalState_ChanNum);
}

/***********************************************************************************
 *
 *	NAME		: HAL_ChipIDGet()
 *
 *	DESCRIPTION	: Get the chip ID.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT8. Value of read chip ID.
 *
 * 	NOTES		: This function can be used for testing SPI interface. 0xCX is a valid chip ID.
 *	
 ***********************************************************************************/
UINT8 HAL_ChipIDGet(UINT8 dev_id)
{
	UINT8	u8ChipID;
	
	u8ChipID = HAL_RegisterRead(dev_id, xCHIPID);

	return	u8ChipID;
}

/***********************************************************************************
 *
 *	NAME		: HAL_TxRequest()
 *
 *	DESCRIPTION	: Request to transmit TXFIFO. The read poiner of TXFIFO is indicated by MTFCRP
 *					register and the 1st byte indicates the length of packet. After transmitting,
 *					MRFCRP is increased by (the length of packet - 2). The FCS field whose length
 *					is 2 is added by hardware MAC. So it is not increase by the length of packet.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: void
 *
 * 	NOTES		: After transmitting, MG2410 generates a TX-End interrupt.
 *	
 ***********************************************************************************/
void HAL_TxRequest(UINT8 dev_id)
{
	HAL_RegisterWrite(dev_id, xPCMD0, 0x38);		// Tx Request=0. Active Low. Auto Clear
}

/***********************************************************************************
 *
 *	NAME		: HAL_TxCancel()
 *
 *	DESCRIPTION	: Request to cancel during transmitting. 
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: void
 *
 * 	NOTES		: After canceling, MG2410 generates a TX-End interrupt.
 *	
 ***********************************************************************************/
void HAL_TxCancel(UINT8 dev_id)
{
	HAL_RegisterWrite(dev_id, xPCMD0, 0x34);		// TxStop=0. Active Low. Auto Clear.
}

/***********************************************************************************
 *
 *	NAME		: HAL_RxEnable()
 *
 *	DESCRIPTION	: Enable the receiver and clear all pending interrups. Or, disable the receiver.
 *
 * 	PARAMETER	: u8Ena. 0=Disable, 1=Enable.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_RxEnable(UINT8 dev_id, UINT8 u8Ena)
{
	if(u8Ena)
	{
		HAL_RegisterRead(dev_id, xINTIDX);		// Clear Interrupt
		HAL_RegisterRead(dev_id, xINTIDX);		// Clear Interrupt
		HAL_RegisterRead(dev_id, xINTIDX);		// Clear Interrupt
		HAL_RegisterRead(dev_id, xINTIDX);		// Clear Interrupt
		HAL_RegisterWrite(dev_id, xPCMD1, 0x00);
	}
	else
	{
		HAL_RegisterWrite(dev_id, xPCMD1, 0x01);
	}
}

/***********************************************************************************
 *
 *	NAME		: HAL_InterruptEnable()
 *
 *	DESCRIPTION	: Enable interrup's source.
 *
 * 	PARAMETER	: u8IntMask - Interrupt soruce to be enabled.
 *					bit[3] : 1=Eanble RX-End interrupt.
 *					bit[2] : 1=Eanble RX-Start interrupt.
 *					bit[1] : 1=Eanble TX-End interrupt.
 *					bit[0] : 1=Eanble Modem-On interrupt.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_InterruptEnable(UINT8 dev_id, UINT8 u8IntMask)
{
	HAL_RegisterWrite(dev_id, xINTCON, u8IntMask & 0x0F);
}

/***********************************************************************************
 *
 *	NAME		: HAL_InterruptGetAndClear()
 *
 *	DESCRIPTION	: Classify the interrupt. And, the interrupt is cleared by reading INTIDX register.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT8. The number of classified interrupt.
 *					0	: Modem-On interrupt
 *					1	: TX-End interrupt
 *					2	: RX-Start interrupt
 *					3	: RX-End interrupt
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_InterruptGetAndClear(UINT8 dev_id)
{
	UINT8	u8IntIdx;

	u8IntIdx = HAL_RegisterRead(dev_id, xINTIDX);	// Clear Interrupt

	return	(u8IntIdx & 0x03);
}

/***********************************************************************************
 *
 *	NAME		: HAL_PanIDSet()
 *
 *	DESCRIPTION	: Set PAN identifier.
 *
 * 	PARAMETER	: u16PanID - PAN identifier to set
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_PanIDSet(UINT8 dev_id, UINT16 u16PanID)
{
	HAL_RegisterWrite(dev_id, xPANID0, u16PanID);
	HAL_RegisterWrite(dev_id, xPANID1, u16PanID >> 8);
}

/***********************************************************************************
 *
 *	NAME		: HAL_PanIDGet()
 *
 *	DESCRIPTION	: Get PAN identifier.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT16. PAN identifier.
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT16 HAL_PanIDGet(UINT8 dev_id)
{
	UINT16	u16PanID;

	u16PanID = HAL_RegisterRead(dev_id, xPANID1) << 8;
	u16PanID |= HAL_RegisterRead(dev_id, xPANID0);

	return	u16PanID;
}

/***********************************************************************************
 *
 *	NAME		: HAL_ShortAddrSet()
 *
 *	DESCRIPTION	: Set 16-bit short address
 *
 * 	PARAMETER	: u16ShortAddr - short address to set
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_ShortAddrSet(UINT8 dev_id, UINT16 u16ShortAddr)
{
	HAL_RegisterWrite(dev_id, xSHORTADDR0, u16ShortAddr);
	HAL_RegisterWrite(dev_id, xSHORTADDR1, u16ShortAddr >> 8);	
}

/***********************************************************************************
 *
 *	NAME		: HAL_ShortAddrGet()
 *
 *	DESCRIPTION	: Get 16-bit short address
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT16. Short address.
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT16 HAL_ShortAddrGet(UINT8 dev_id)
{
	UINT16	u16ShortAddr;

	u16ShortAddr = HAL_RegisterRead(dev_id, xSHORTADDR1) << 8;
	u16ShortAddr |= HAL_RegisterRead(dev_id, xSHORTADDR0);

	return	u16ShortAddr;
}

/***********************************************************************************
 *
 *	NAME		: HAL_IEEEAddrSet()
 *
 *	DESCRIPTION	: Set 64-bit extended address(IEEE address).
 *
 * 	PARAMETER	: pu8ExtAddrBuff - Pointer to buffer of extended address to set.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_IEEEAddrSet(UINT8 dev_id, UINT8* pu8ExtAddrBuff)
{
	HAL_RegisterWrite(dev_id, xIEEEADDR0, pu8ExtAddrBuff[0]);
	HAL_RegisterWrite(dev_id, xIEEEADDR1, pu8ExtAddrBuff[1]);
	HAL_RegisterWrite(dev_id, xIEEEADDR2, pu8ExtAddrBuff[2]);
	HAL_RegisterWrite(dev_id, xIEEEADDR3, pu8ExtAddrBuff[3]);
	HAL_RegisterWrite(dev_id, xIEEEADDR4, pu8ExtAddrBuff[4]);
	HAL_RegisterWrite(dev_id, xIEEEADDR5, pu8ExtAddrBuff[5]);
	HAL_RegisterWrite(dev_id, xIEEEADDR6, pu8ExtAddrBuff[6]);
	HAL_RegisterWrite(dev_id, xIEEEADDR7, pu8ExtAddrBuff[7]);
}

/***********************************************************************************
 *
 *	NAME		: HAL_IEEEAddrGet()
 *
 *	DESCRIPTION	: Get 64-bit extended address(IEEE address).
 *
 * 	PARAMETER	: pu8ExtAddrBuff - Pointer to buffer in which read extended address is stored.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_IEEEAddrGet(UINT8 dev_id, UINT8* pu8ExtAddrBuff)
{
	pu8ExtAddrBuff[0] = HAL_RegisterRead(dev_id, xIEEEADDR0);
	pu8ExtAddrBuff[1] = HAL_RegisterRead(dev_id, xIEEEADDR1);
	pu8ExtAddrBuff[2] = HAL_RegisterRead(dev_id, xIEEEADDR2);
	pu8ExtAddrBuff[3] = HAL_RegisterRead(dev_id, xIEEEADDR3);
	pu8ExtAddrBuff[4] = HAL_RegisterRead(dev_id, xIEEEADDR4);
	pu8ExtAddrBuff[5] = HAL_RegisterRead(dev_id, xIEEEADDR5);
	pu8ExtAddrBuff[6] = HAL_RegisterRead(dev_id, xIEEEADDR6);
	pu8ExtAddrBuff[7] = HAL_RegisterRead(dev_id, xIEEEADDR7);
}

/***********************************************************************************
 *
 *	NAME		: HAL_MacDsnSet()
 *
 *	DESCRIPTION	: Write MACDSN register.
 *
 * 	PARAMETER	: u8Dsn - Value to be written.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_MacDsnSet(UINT8 dev_id, UINT8 u8Dsn)
{
	HAL_RegisterWrite(dev_id, xMACDSN, u8Dsn);
}

/***********************************************************************************
 *
 *	NAME		: HAL_MacCtrlSet()
 *
 *	DESCRIPTION	: Set MACCTRL register. The each bit of MACCTRL has following meaning.
 *					bit[7:5] : reserved
 *					bit[4] : prevent_ack_packet.	1=Enabled. Default value is 0.
 *					bit[3] : pan_coordinator.	1=Enabled. Default value is 0.
 *					bit[2] : addr_decode.		1=Enabled. Default value is 1.
 *					bit[1] : auto_crc.			1=Enabled. Default value is 1.
 *					bit[0] : auto_ack.			1=Enabled. Default vaule is 0.
 *
 * 	PARAMETER	: pu8ExtAddrBuff - Pointer to buffer in which read extended address is stored.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8	gu8HalState_MacCtrlReg = 0x16;

void HAL_MacCtrlSet(UINT8 dev_id, UINT8 u8MacCtrlReg)
{
	gu8HalState_MacCtrlReg = u8MacCtrlReg;
	HAL_RegisterWrite(dev_id, xMACCTRL, u8MacCtrlReg);
}

/***********************************************************************************
 *
 *	NAME		: HAL_MacStsGet()
 *
 *	DESCRIPTION	: Get value of MACSTS register. It indicates the status 
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: UINT8. The value of MACSTS register. The each bit of MACSTS has following meaning.
 *					bit[7]	: 1=Enc/Dec is under operation.
 *					bit[6]	: 1=TX is under operation.
 *					bit[5]	: 1=RX is under operation.
 *					bit[4]	: 1=SAES operation is done.
 *					bit[3]	: 1=Addressing information of last received packet is correct.
 *					bit[2]	: 1=Encryption is done.
 *					bit[1]	: 1=Decryption is done.
 *					bit[0]	: 1=FCS field  of last received packet is correct.
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_MacStsGet(UINT8 dev_id)
{
	UINT8	u8MacSts;

	u8MacSts = HAL_RegisterRead(dev_id, xMACSTS);

	return	u8MacSts;
}

/***********************************************************************************
 *
 *	NAME		: HAL_AutoAckFramePendSet()
 *
 *	DESCRIPTION	: Set value of frame-pending subfield of transmitted Auto-ACK packet.
 *
 * 	PARAMETER	: u8FramePend - Value of frame-pending subfield.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_AutoAckFramePendSet(UINT8 dev_id, UINT8 u8FramePend)
{
	if(u8FramePend)
	{
		HAL_RegisterWrite(dev_id, xAACKSTA, 0x01);
	}
	else
	{
		HAL_RegisterWrite(dev_id, xAACKSTA, 0x00);
	}
}

/***********************************************************************************
 *
 *	NAME		: HAL_TxFifoReset()
 *
 *	DESCRIPTION	: Reset TXFIFO. The read/write pointer is cleared to 0.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_TxFifoReset(UINT8 dev_id)
{
	//	Reset Pointer
	HAL_RegisterWrite(dev_id, xMTFCCTL, 0x07);
	HAL_RegisterWrite(dev_id, xMTFCCTL, 0x06);
	//	Reset State Machine	
	HAL_RegisterWrite(dev_id, xMACRST, 0x40);
	HAL_RegisterWrite(dev_id, xMACRST, 0x00);
}

/***********************************************************************************
 *
 *	NAME		: HAL_RxFifoReset()
 *
 *	DESCRIPTION	: Reset RXFIFO. The read/write pointer is cleared to 0.
 *
 * 	PARAMETER	: void
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_RxFifoReset(UINT8 dev_id)
{
	//	Reset Pointer
	HAL_RegisterWrite(dev_id, xMRFCCTL, 0x07);
	HAL_RegisterWrite(dev_id, xMRFCCTL, 0x06);
	//	Reset State Machine
	HAL_RegisterWrite(dev_id, xMACRST, 0x20);
	HAL_RegisterWrite(dev_id, xMACRST, 0x00);
}

/***********************************************************************************
 * 
 *	NAME		: HAL_GpioDedicationSet()
 *
 *	DESCRIPTION	: Set which signal is mapped to GPIO.
 *
 * 	PARAMETER	: u8SignalMode - Signal mode of GPIO. This is written to MONCON0 register.
 *					0	: P0=IRQ		P1=TRSW		P2=nTRSW	P3=CRCOK	P4=PLL_LOCK			P5=EXTCLK
 *					1	: P0=TRSW	P1=nTRSW		P2=IRQ		P3=CRCOK	P4=MAC_SEC_DONE	P5=EXTCLK
 *					2	: P0=TRSW	P1=nTRSW		P2=IRQ		P3=CRCOK	P4=PLL_LOCK			P5=EXTCLK
 *					3	: P0=TRSW	P1=IRMOD_OUT	P2=IRQ		P3=CRCOK	P4=IRMOD_DONE		P5=EXTCLK
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8SignalMode is out of range).
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_GpioDedicationSet(UINT8 dev_id, UINT8 u8SignalMode)
{
	UINT8	u8xMONCON3;

	if(u8SignalMode > 3)
	{
		return 1;
	}

	u8xMONCON3 = (u8SignalMode & 0x03) << 4;
	u8xMONCON3 |= 0x0F;
	HAL_RegisterWrite(dev_id, xMONCON3, u8xMONCON3);
	
	return 0;
}

/***********************************************************************************
 * 
 *	NAME		: HAL_GpioModeSet()
 *
 *	DESCRIPTION	: Set the mode(input, output or dedication) of each GPIO
 *
 * 	PARAMETER	: u8Port - GPIO to be set.
 *					0	: GPIO0 
 *					1	: GPIO1 
 *					2	: GPIO2 
 *					3	: GPIO3 
 *					4	: GPIO4 
 *					5	: GPIO5  
 *				  u8Mode - GPIO's mode. 0=GPIO Input, 1=GPIO Output, Otherwise=Dedication Output
 *				  u8Option - Options for GPIO mode. It only has meaning when u8Mode=0 or 1.
 *					if u8Mode=0 : 0=Pull-up, 1=Pull-down, Otherwise=High-impedance
 *					if u8Mode=1 : 
 *						bit[7:1]	: reserved
 * 						bit[0] 	: Drive Strength. 0=4mA. 1=8mA. *					
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8Port is out of range).
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_GpioModeSet(UINT8 dev_id, UINT8 u8Port, UINT8 u8Mode, UINT8 u8Option)
{
	UINT8	u8MaskOR;
	UINT8	u8MaskAND;
	UINT8	u8MONCON0;
	UINT8	u8GP_OEN;
	UINT8	u8GP_IE;
	UINT8	u8GP_PE;
	UINT8	u8GP_PS;
	UINT8	u8GP_DS;

	if(u8Port > 5)
	{
		return 1;
	}	

	u8MaskOR = 1 << u8Port;
	u8MaskAND = ~u8MaskOR;

	if(u8Mode == 1)			// GPIO Output
	{
		// GPIO Setting
		u8GP_OEN = HAL_RegisterRead(dev_id, xGP_OEN);
		u8GP_IE = HAL_RegisterRead(dev_id, xGP_IE);
		u8GP_DS = HAL_RegisterRead(dev_id, xGP_DS);
		u8GP_PE = HAL_RegisterRead(dev_id, xGP_PE);		

		u8GP_OEN &= u8MaskAND;		// OEN[x]=0
		u8GP_IE &= u8MaskAND;		// IE[x]=0
		u8GP_PE &= u8MaskAND;		// PE[x]=0
		if(u8Option & 0x01)
		{
			u8GP_DS |= u8MaskOR;	// DS[x]=1
		}
		else
		{
			u8GP_DS &= u8MaskAND;	// DS[x]=0
		}
		HAL_RegisterWrite(dev_id, xGP_DS, u8GP_DS);
		HAL_RegisterWrite(dev_id, xGP_PE, u8GP_PE);
		HAL_RegisterWrite(dev_id, xGP_IE, u8GP_IE);
		HAL_RegisterWrite(dev_id, xGP_OEN, u8GP_OEN);
		
		// Dedication or GPIO ==> GPIO
		u8MONCON0 = HAL_RegisterRead(dev_id, xMONCON0);
		if(u8Port == 5)
		{
			u8MONCON0 |= u8MaskOR; 		// bit[x]=1
		}
		else
		{
			u8MONCON0 &= u8MaskAND; 	// bit[5]=0
		}
		HAL_RegisterWrite(dev_id, xMONCON0, u8MONCON0);
	}
	else if(u8Mode == 0)		// GPIO Input
	{
		// GPIO Setting
		u8GP_OEN = HAL_RegisterRead(dev_id, xGP_OEN);
		u8GP_IE = HAL_RegisterRead(dev_id, xGP_IE);
		u8GP_PE = HAL_RegisterRead(dev_id, xGP_PE);		
		u8GP_PS = HAL_RegisterRead(dev_id, xGP_PS);

		u8GP_OEN |= u8MaskOR;		// OEN[x]=1
		u8GP_IE |= u8MaskOR;			// IE[x]=1
		if(u8Option == 0)		// Pull-up
		{
			u8GP_PE |= u8MaskOR;	// PE[x]=1
			u8GP_PS |= u8MaskOR;	// PS[x]=1
		}
		else if(u8Option == 1)
		{
			u8GP_PE |= u8MaskOR;	// PE[x]=1
			u8GP_PS &= u8MaskAND;	// PS[x]=1
		}
		else
		{
			u8GP_PE &= u8MaskAND;	// PE[x]=0
		}

		HAL_RegisterWrite(dev_id, xGP_PS, u8GP_PS);
		HAL_RegisterWrite(dev_id, xGP_PE, u8GP_PE);
		HAL_RegisterWrite(dev_id, xGP_OEN, u8GP_OEN);
		HAL_RegisterWrite(dev_id, xGP_IE, u8GP_IE);

		// Dedication or GPIO ==> GPIO
		u8MONCON0 = HAL_RegisterRead(dev_id, xMONCON0);
		if(u8Port == 5)
		{
			u8MONCON0 |= u8MaskOR; 		// bit[x]=1		
		}
		else
		{
			u8MONCON0 &= u8MaskAND; 	// bit[5]=0
		}
		HAL_RegisterWrite(dev_id, xMONCON0, u8MONCON0);
	}
	else		// Dedication Output
	{
		// Dedication or GPIO ==> Dedication
		u8MONCON0 = HAL_RegisterRead(dev_id, xMONCON0);
		if(u8Port == 5)
		{
			u8MONCON0 &= u8MaskAND; 	// bit[x]=0
		}
		else
		{
			u8MONCON0 |= u8MaskOR; 		// bit[5]=1
		}
		HAL_RegisterWrite(dev_id, xMONCON0, u8MONCON0);
	}	

	return 0;
}

/***********************************************************************************
 *
 *	NAME		: HAL_GpioGetBit()
 *
 *	DESCRIPTION	: Get the value of a GPIO which is specified by u8Port.
 *
 * 	PARAMETER	: u8GpioNum - Number of the GPIO to be got.
 *					0	: GPIO0
 *					1	: GPIO1
 *					2	: GPIO2
 *					3	: GPIO3
 *					4	: GPIO4
 *					5	: GPIO5
 *
 * 	RETURN		: UINT8. The value of GPIO. 0=Low, 1=High.
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_GpioGetBit(UINT8 dev_id, UINT8 u8GpioNum)
{
	UINT8	u8MaskAND;
	UINT8	u8GP_DIN;

	u8MaskAND = 1 << u8GpioNum;

	u8GP_DIN = HAL_RegisterRead(dev_id, xGP_DIN);
	u8GP_DIN &= u8MaskAND;

	if(u8GP_DIN)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/***********************************************************************************
 *
 *	NAME		: HAL_GpioGetAll()
 *
 *	DESCRIPTION	: Get the value of all GPIOs.
 *
 * 	PARAMETER	: None
 *
 * 	RETURN		: UINT8. Value of GPIOs.
 *					bit[7:6]	: reserved
 *					bit[5]	: value of GPIO5.
 *					bit[4]	: value of GPIO4.
 *					bit[3]	: value of GPIO3.
 *					bit[2]	: value of GPIO2.
 *					bit[1]	: value of GPIO1.
 *					bit[0]	: value of GPIO0.
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_GpioGetAll(UINT8 dev_id)
{
	UINT8	u8GP_DIN;

	u8GP_DIN = HAL_RegisterRead(dev_id, xGP_DIN);
	u8GP_DIN &= 0x3F;

	return u8GP_DIN;
}

/***********************************************************************************
 * 
 *	NAME		: HAL_GpioSetBit()
 *
 *	DESCRIPTION	: Set the value of each output GPIO.
 *
 * 	PARAMETER	: u8GpioNum - Number of the GPIO to be set.
 *					0	: GPIO0
 *					1	: GPIO1
 *					2	: GPIO2
 *					3	: GPIO3
 *					4	: GPIO4
 *					5	: GPIO5
 *				  u8OutValue : The value of the GPIO to be set. 0=Low, 1=High.
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8Port is out of range).
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_GpioSetBit(UINT8 dev_id, UINT8 u8GpioNum, UINT8 u8OutValue)
{
	UINT8	u8MaskOR;
	UINT8	u8MaskAND;
	UINT8	u8GP_DOUT;

	if(u8GpioNum > 5)
	{
		return 1;
	}

	u8MaskOR = 1 << u8GpioNum;
	u8MaskAND = ~u8MaskOR;

	u8GP_DOUT = HAL_RegisterRead(dev_id, xGP_DOUT);
	if(u8OutValue)
	{
		u8GP_DOUT |= u8MaskOR;
	}
	else
	{
		u8GP_DOUT &= u8MaskAND;
	}
	HAL_RegisterWrite(dev_id, xGP_DOUT, u8GP_DOUT);

	return 0;
}

/***********************************************************************************
 * 
 *	NAME		: HAL_GpioSetMulti()
 *
 *	DESCRIPTION	: Set the value of output GPIOs.
 *
 * 	PARAMETER	: u8GpioMask - Masking of the GPIO to be set.
 *					bit[7:6]	: reserved
 *					bit[5]	: 1=Set GPIO5. 0=Not Set.
 *					bit[4]	: 1=Set GPIO4. 0=Not Set.
 *					bit[3]	: 1=Set GPIO3. 0=Not Set.
 *					bit[2]	: 1=Set GPIO2. 0=Not Set.
 *					bit[1]	: 1=Set GPIO1. 0=Not Set.
 *					bit[0]	: 1=Set GPIO0. 0=Not Set.
 *				  u8OutValueMask : The value of each GPIO. 0=Low, 1=High.
 *					bit[7:6]	: reserved
 *					bit[5]	: GPIO5's value to be set.
 *					bit[4]	: GPIO4's value to be set.
 *					bit[3]	: GPIO3's value to be set.
 *					bit[2]	: GPIO2's value to be set.
 *					bit[1]	: GPIO1's value to be set.
 *					bit[0]	: GPIO0's value to be set.
 *
 * 	RETURN		: None
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_GpioSetMulti(UINT8 dev_id, UINT8 u8GpioMask, UINT8 u8OutValueMask)
{
	UINT8	u8SetBitMask;
	UINT8	u8ClearBitMask;
	UINT8	u8GP_DOUT;

	u8GP_DOUT = HAL_RegisterRead(dev_id, xGP_DOUT);
	u8SetBitMask = u8GpioMask & u8OutValueMask;
	u8ClearBitMask = u8GpioMask & (~u8OutValueMask);
	u8GP_DOUT |= u8SetBitMask;
	u8GP_DOUT &= ~u8ClearBitMask;
	HAL_RegisterWrite(dev_id, xGP_DOUT, u8GP_DOUT);
}

/***********************************************************************************
 * 
 *	NAME		: HAL_IRQGpioSelect()
 *
 *	DESCRIPTION	: Set the MG2410's GPIO which is used for IRQ.
 *
 * 	PARAMETER	: u8IRQGpio - GPIO for IRQ. 0=GPIO0, 2=GPIO2.
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8IRQGpio is out of range).
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_IRQGpioSelect(UINT8 dev_id, UINT8 u8IRQGpio)
{
	UINT8	u8Status;

	u8Status = 1;
	if(u8IRQGpio == 0)
	{		
		HAL_RegisterWrite(dev_id, xMONCON3, 0x0F);
		HAL_RegisterWrite(dev_id, xMONCON0, 0x01);
		u8Status = 0;		
	}
	else if(u8IRQGpio == 2)
	{
		HAL_RegisterWrite(dev_id, xMONCON3, 0x1F);
		HAL_RegisterWrite(dev_id, xMONCON0, 0x04);
		u8Status = 0;
	}

	return u8Status;
}

/***********************************************************************************
 *
 *	NAME		: HAL_TempSensorSet()
 *
 *	DESCRIPTION	: Temperature Sensor Mode. When enabled, 0.06mA current is consumed.
 *					- Temperature sensor voltage for ATEST0(pin 8)
 * 					- Chip's ground voltage for ATEST1(pin 9)
 *
 * 	PARAMETER	: u8Ena - Enable/Disable
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_TempSensorSet(UINT8 dev_id, UINT8 u8Ena)
{
	if(u8Ena)
	{
		HAL_RegisterWrite(dev_id, xTSEN, 0x80);
	}
	else
	{
		HAL_RegisterWrite(dev_id, xTSEN, 0x00);
	}
}

/***********************************************************************************
 *
 *	NAME		: HAL_BlockClock0Set()
 *
 *	DESCRIPTION	: Clock control for each block of MG2410.
 *
 * 	PARAMETER	: u8BlockClock0On. If bit[n]=1, the clock of corresponding block is ON.
 *					- bit[7]	: Clock for AES Block
 *					- bit[6]	: Clock for Modem-Tx Block
 *					- bit[5]	: Clock for Modem-RX Block
 *					- bit[4]	: Clock for Register. This clock should be ON always.
 *					- bit[3]	: Clock for RX3 Block.
 *					- bit[2]	: Clock for RX2 Block.
 *					- bit[1]	: Clock for RX1 Block.
 *					- bit[0]	: Clock for ADC Block.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_BlockClock0Set(UINT8 dev_id, UINT8 u8BlockClock0On)
{
	UINT8	u8MaskValue;

	u8MaskValue = u8BlockClock0On ^ 0xEE;
	HAL_RegisterWrite(dev_id, xCLKON0, u8MaskValue);
}

/***********************************************************************************
 *
 *	NAME		: HAL_BlockClock1Set()
 *
 *	DESCRIPTION	: Clock control for each block of MG2410.
 *
 * 	PARAMETER	: u8BlockClock1On. If bit[n]=1, the clock of corresponding block is ON.
 *					- bit[7:5]	: reserved. These should be 0.
 *					- bit[4]	: Clock for Test Mode Block.
 *					- bit[3]	: Clock for TX Block.
 *					- bit[2]	: Clock for MAC/PHY Interface Block.
 *					- bit[1]	: Clock for Correlator0 Block. This clock should be OFF always.
 *					- bit[0]	: Clock for Correlator1 Block.
 *
 * 	RETURN		: void
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
void HAL_BlockClock1Set(UINT8 dev_id, UINT8 u8BlockClock1On)
{
	UINT8	u8MaskValue;

	u8MaskValue = u8BlockClock1On ^ 0x0B;
	HAL_RegisterWrite(dev_id, xCLKON1, u8MaskValue);
}

/***********************************************************************************
 *
 *	NAME		: HAL_ClockModeSet()
 *
 *	DESCRIPTION	: Clock control for blocks according the operating mode.
 *
 * 	PARAMETER	: u8Mode - 0=TX/RX mode. 1=TX only mode. 2=RX only mode. 3=RF Test mode.
 *
 * 	RETURN		: UINT8. Result. 0=Successful, 1=Failed (u8Mode is out of range). 
 *
 * 	NOTES		: None
 *	
 ***********************************************************************************/
UINT8 HAL_ClockModeSet(UINT8 dev_id, UINT8 u8Mode)
{
	if(u8Mode > 3)
	{
		return 1;
	}

	if(u8Mode == 1)			// TX Mode
	{
		HAL_BlockClock0Set(dev_id, 0x50);
		HAL_BlockClock1Set(dev_id, 0x0C);
	}
	else if(u8Mode == 2)		// RX Mode
	{
		HAL_BlockClock0Set(dev_id, 0x3F);
		HAL_BlockClock1Set(dev_id, 0x05);
	}
	else if(u8Mode == 3)		// RF Test Mode
	{
		HAL_BlockClock0Set(dev_id, 0x10);
		HAL_BlockClock1Set(dev_id, 0x1C);
	}
	else						// TX/RX mode
	{
		HAL_BlockClock0Set(dev_id, 0x7F);
		HAL_BlockClock1Set(dev_id, 0x1D);
	}

	return 0;
}


