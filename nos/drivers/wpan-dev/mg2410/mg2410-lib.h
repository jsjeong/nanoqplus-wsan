
/*******************************************************************************
	- Chip		: MG2410
	- Vendor		: RadioPulse Inc, 2010.
	- Date		: 2010-09-14
	- Version		: VER 1.1

	[2010-07-31] VER 1.1
	- Added
		+ void HAL_AGCSet()
	- Renamed
		+ void HAL_IEEEAddrSet() <-- void HAL_ExtAddrSet()
		+ void HAL_IEEEAddrGet() <-- void HAL_ExtAddrGet()
	
	[2010-07-31] VER 1.0a
	- Added
		+ UINT8 HAL_GpioDedicationSet();
		+ UINT8 HAL_GpioModeSet();
		+ UINT8 HAL_GpioGetBit();
		+ UINT8 HAL_GpioGetAll();
		+ UINT8 HAL_GpioSetBit();
		+ void HAL_GpioSetMulti();
		+ UINT8 HAL_IRQGpioSelect();
		+ void HAL_TempSensorSet();

	[2010-07-10] VER 1.00
	- Initial Version
*******************************************************************************/


#ifndef __MG2410_LIB_H__
#define	__MG2410_LIB_H__

#if 0
#ifndef __RADIOPULSE_TYPE__
#define __RADIOPULSE_TYPE__

	typedef signed long  INT32;
	typedef signed short INT16;
	typedef signed char  INT8;

	typedef signed long  const INTC32;  	/* Read Only */
	typedef signed short const INTC16;  	/* Read Only */
	typedef signed char  const INTC8;   		/* Read Only */

	typedef volatile signed long  VINT32;
	typedef volatile signed short VINT16;
	typedef volatile signed char  VINT8;

	typedef volatile signed long  const VINTC32;  	/* Read Only */
	typedef volatile signed short const VINTC16; 		/* Read Only */
	typedef volatile signed char  const VINTC8;   		/* Read Only */

	typedef unsigned long  UINT32;
	typedef unsigned short UINT16;
	typedef unsigned char  UINT8;

	typedef unsigned long  const UINTC32;  		/* Read Only */
	typedef unsigned short const UINTC16;  		/* Read Only */
	typedef unsigned char  const UINTC8;   		/* Read Only */

	typedef volatile unsigned long  VUINT32;
	typedef volatile unsigned short VUINT16;
	typedef volatile unsigned char  VUINT8;

	typedef volatile unsigned long  const VUINTC32;  		/* Read Only */
	typedef volatile unsigned short const VUINTC16;  		/* Read Only */
	typedef volatile unsigned char  const VUINTC8;   		/* Read Only */

#endif	// #ifndef __RADIOPULSE_TYPE__
#else
#include "typedef.h"
#endif


#define	MAX_WAIT_100us_TICK_ForDccCheck		10		// 100us * 10 = 1ms
#define	MAX_WAIT_100us_TICK_ForLockCheck	10		// 100us * 10 = 1ms
#define	MAX_WAIT_100us_TICK_ForModemOn		10		// 100us * 10 = 1ms

extern 	UINT8	gu8HalState_DataRate;
extern 	UINT16	gu16HalState_MaxWaitTimeForTx_100us;
extern	UINT8	gu8HalState_MacCtrlReg;

void HAL_Reset(UINT8 dev_id, UINT8 u8Ena);
void HAL_DeepSleep(UINT8 dev_id, UINT8 u8Ena);
void HAL_RegisterWrite(UINT8 dev_id, UINT16 u16RegAddr, UINT8 u8RegValue);
UINT8 HAL_RegisterRead(UINT8 dev_id, UINT16 u16RegAddr);
void HAL_TxFifoPushSingle(UINT8 dev_id, UINT8 u8PushData);
UINT8 HAL_TxFifoPushBurst(UINT8 dev_id, UINT8* pu8PushBuff, UINT8 u8Len);
UINT8 HAL_RxFifoPopSingle(UINT8 dev_id);
void HAL_RxFifoPopBurst(UINT8 dev_id, UINT8* pu8PopBuff, UINT8 u8Len);
void HAL_TxFifoWrPtrSet(UINT8 dev_id, UINT8 u8WrPtr);
void HAL_TxFifoRdPtrSet(UINT8 dev_id, UINT8 u8RdPtr);
void HAL_RxFifoWrPtrSet(UINT8 dev_id, UINT8 u8WrPtr);
void HAL_RxFifoRdPtrSet(UINT8 dev_id, UINT8 u8RdPtr);
UINT8 HAL_RxFifoRdPtrGet(UINT8 dev_id);
void HAL_RxFifoRdPtrAdd(UINT8 dev_id, UINT8 u8Add);

UINT8 HAL_DmScaleGet(UINT8 dev_id, UINT8 u8CpsMode2M, UINT16 u16Freq_MHz);
UINT8 HAL_DataRateSet(UINT8 dev_id, UINT8 u8DataRate);
 UINT8 HAL_FrequencySet(UINT8 dev_id, UINT16 u16Freq_MHz);
UINT16 HAL_FrequencyGet(UINT8 dev_id);
UINT8 HAL_ChannelSet(UINT8 dev_id, UINT8 u8ChanNum);
UINT8 HAL_ChannelGet(UINT8 dev_id);
void HAL_AGCSet(UINT8 dev_id, UINT8 u8CpsMode2M, UINT8 u8Initialize);
UINT8 HAL_RegisterConfig(UINT8 dev_id);
UINT8 HAL_TxPowerSet(UINT8 dev_id, UINT8 u8TxPowerLevel);
INT8 HAL_EnergyLevelGet(UINT8 dev_id);
UINT8 HAL_CCAEnable(UINT8 dev_id, UINT8 u8Ena, UINT8 u8CcaMode);
void HAL_CCAThresholdSet(UINT8 dev_id, INT8 i8Threshold_dBm);
void HAL_LQIEnable(UINT8 dev_id, UINT8 u8Ena);
UINT8 HAL_LQIGet(UINT8 dev_id);
INT8 HAL_RssiGet(UINT8 dev_id);
void HAL_RFTestMode(UINT8 dev_id, UINT8 u8TestMode);
UINT8 HAL_ChipIDGet(UINT8 dev_id);	
void HAL_TxRequest(UINT8 dev_id);
void HAL_TxCancel(UINT8 dev_id);
void HAL_RxEnable(UINT8 dev_id, UINT8 u8Ena);
void HAL_InterruptEnable(UINT8 dev_id, UINT8 u8IntMask);
UINT8 HAL_InterruptGetAndClear(UINT8 dev_id);
void HAL_PanIDSet(UINT8 dev_id, UINT16 u16PanID);
UINT16 HAL_PanIDGet(UINT8 dev_id);
void HAL_ShortAddrSet(UINT8 dev_id, UINT16 u16ShortAddr);
UINT16 HAL_ShortAddrGet(UINT8 dev_id);
void HAL_IEEEAddrSet(UINT8 dev_id, UINT8* pu8ExtAddrBuff);
void HAL_IEEEAddrGet(UINT8 dev_id, UINT8* pu8ExtAddrBuff);
void HAL_MacDsnSet(UINT8 dev_id, UINT8 u8Dsn);
void HAL_MacCtrlSet(UINT8 dev_id, UINT8 u8MacCtrlReg);
UINT8 HAL_MacStsGet(UINT8 dev_id);
void HAL_AutoAckFramePendSet(UINT8 dev_id, UINT8 u8FramePend);
void HAL_TxFifoReset(UINT8 dev_id);
void HAL_RxFifoReset(UINT8 dev_id);
UINT8 HAL_GpioDedicationSet(UINT8 dev_id, UINT8 u8SignalMode);
UINT8 HAL_GpioModeSet(UINT8 dev_id, UINT8 u8Port, UINT8 u8Mode, UINT8 u8Option);
UINT8 HAL_GpioGetBit(UINT8 dev_id, UINT8 u8Port);
UINT8 HAL_GpioGetAll(UINT8 dev_id);
UINT8 HAL_GpioSetBit(UINT8 dev_id, UINT8 u8GpioNum, UINT8 u8OutValue);
void HAL_GpioSetMulti(UINT8 dev_id, UINT8 u8GpioMask, UINT8 u8OutValueMask);
UINT8 HAL_IRQGpioSelect(UINT8 dev_id, UINT8 u8IRQGpio);
void HAL_TempSensorSet(UINT8 dev_id, UINT8 u8Ena);
void HAL_BlockClock0Set(UINT8 dev_id, UINT8 u8BlockClock0On);
void HAL_BlockClock1Set(UINT8 dev_id, UINT8 u8BlockClock1On);
UINT8 HAL_ClockModeSet(UINT8 dev_id, UINT8 u8Mode);

#endif		// #ifndef __MG2410_LIB_H__
