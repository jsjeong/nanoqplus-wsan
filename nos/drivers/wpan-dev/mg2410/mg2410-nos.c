// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file mg2410-nos.c
 *
 * MG2410 library for NOS
 *
 * @author Haeyong Kim (ETRI)
 * @date 2013. 9. 5.
 */

#include "mg2410-nos.h"

#ifdef MG2410_M
#include "mg2410.h"
#include "mg2410-lib.h"
#include "mg2410-interface.h"
#include "wpan-dev.h"
#include "arch.h"
#include <string.h>
#include <stdio.h>

extern WPAN_DEV wpan_dev[WPAN_DEV_CNT];

void nos_mg2410_flush_rxbuf(UINT8 dev_id)
{
    HAL_RxFifoReset(dev_id);
    wpan_dev[dev_id].c.mg2410.rf_rx_frame_length = 0;
}

void nos_mg2410_sleep(UINT8 dev_id)
{
    HAL_RxEnable(dev_id, FALSE);
}

void nos_mg2410_wakeup(UINT8 dev_id)
{
    HAL_RxEnable(dev_id, TRUE);
}

void nos_mg2410_setup(UINT8 dev_id)
{
    //wpan_dev[dev_id].mg2410.waiting_ack = FALSE;
    
    nos_mg2410_set_tx_power(dev_id, 0);

    // Initial channel: 11.
    nos_mg2410_set_channel(dev_id, 11);
    wpan_dev[dev_id].channel = 11;

    // Enable RF interrupt.
    HAL_TxFifoReset(dev_id);
    nos_mg2410_flush_rxbuf(dev_id); // == HAL_RxFifoReset()
    nos_mg2410_clear_mcu_intr(dev_id);
    CB_IRQInterruptOn(dev_id);
    nos_mg2410_wakeup(dev_id);
}

// level: 0~18
void nos_mg2410_set_tx_power(UINT8 dev_id, int level)
{
    if (level < 0)
        level = 0;
    else if (level > 18)
        level = 18;
    
    HAL_TxPowerSet(dev_id, level);
}

/***********************************************************************************
 * @fn      nos_mg2410_set_channel
 * @brief   Set RF channel in the 2.4GHz band. The Channel must be in the range 11-26,
 *          11= 2005 MHz, channel spacing 5 MHz.
 * @param   channel - logical channel number
 * @return  none
 */
void nos_mg2410_set_channel(UINT8 dev_id, int channel)
{
    if (channel < 11)
    {
        channel = 11;
    }
    else if (channel > 26)
    {
        channel = 26;
    }
    
    HAL_ChannelSet(dev_id, channel);
}

int nos_mg2410_get_channel(UINT8 dev_id)
{
    return (int)HAL_ChannelGet(dev_id);
}

void nos_mg2410_set_pan_id(uint8_t dev_id)
{
    HAL_PanIDSet(dev_id, wpan_dev[dev_id].pan_id);
}

void nos_mg2410_set_short_addr(uint8_t dev_id)
{
    HAL_ShortAddrSet(dev_id, wpan_dev[dev_id].short_id);
}

void nos_mg2410_set_eui64(UINT8 dev_id)
{
    UINT8 tmp_eui64[8];
    
    for(int i = 0; i < 8; i++)
        tmp_eui64[i] = wpan_dev[dev_id].eui64[7-i];
    
    HAL_IEEEAddrSet(dev_id, (unsigned char*)tmp_eui64);
}

void nos_mg2410_write_frame(UINT8 dev_id, const UINT8 *buf)
{
    if (buf[0] < 5 || buf[0] > 127 || buf == NULL)
    {
        return;
    }
    
    HAL_TxFifoWrPtrSet(dev_id, 0x00);   // Write TXFIFO pointer as 0x00.
#if 1
    HAL_TxFifoPushSingle(dev_id, buf[0]);	// Write PSDU's length to TXFIFO
    HAL_TxFifoPushBurst(dev_id, (UINT8 *) buf+1, buf[0]-1);	// Write PSDU to TXFIFO, FCS(2) will be added automatically
    HAL_MacDsnSet(dev_id, buf[3]);  // Write Sequnce number into MACDSN reg. Refer "MG2410_MacCtrl_PreventAck"
#else // for test
    HAL_TxFifoPushSingle(dev_id, 0x0c);	
    HAL_TxFifoPushSingle(dev_id, 0x61);	
    HAL_TxFifoPushSingle(dev_id, 0x88);	
    HAL_TxFifoPushSingle(dev_id, tmp_seq++);	
    HAL_TxFifoPushSingle(dev_id, 0x12);	
    HAL_TxFifoPushSingle(dev_id, 0x34);	
    HAL_TxFifoPushSingle(dev_id, 0x56);	
    HAL_TxFifoPushSingle(dev_id, 0x78);	
    HAL_TxFifoPushSingle(dev_id, 0xab);	
    HAL_TxFifoPushSingle(dev_id, 0xcd);	
    HAL_TxFifoPushSingle(dev_id, 0xef);	
    // fcs(2)
#endif
}

ERROR_T nos_mg2410_tx(UINT8 dev_id, const UINT8 *buf)
{
    uint8_t status;
    ERROR_T ret_val = ERROR_FAIL;

    // Writing TXfifo and random backoff for csma-ca has been done.
    // Implement CSMA-CA including LIFS and then check whether status has been changed into TX-active state/TX-done or not.
    // ex) cca -> delay -> cca -> TX -> TX-done-check

    // We always write data on 0x00 of TXFIFO. Set read pointer to 0x00.
    HAL_TxFifoRdPtrSet(dev_id, 0x00);

    // Check Modem Status before transmitting.
    status = HAL_RegisterRead(dev_id, xPHYSTS) & 0x70;
    if(status != 0x00 && status != 0x10)	// RX idle or RX wait
    {
        return ret_val;
    }

    HAL_InterruptEnable(dev_id, MG2410_INT_TxEnd | MG2410_INT_RxEnd);
    nos_mg2410_flush_rxbuf(dev_id);	// clear RXFIFO

    wpan_dev[dev_id].c.mg2410.tx_done = FALSE;
    wpan_dev[dev_id].c.mg2410.ack_rcvd = FALSE;
    HAL_TxRequest(dev_id);    // Request transmitting. NOTE: any HAL_xxx command will call ENABLE_RF_INTR()
    
    if (nos_timer_config(10001, 8000))	//MAX TX duration is 8ms
    {
        nos_timer_start(10001);
        
        do {
            if (wpan_dev[dev_id].c.mg2410.tx_done)
            {
                ret_val = ERROR_SUCCESS;
                break;
            }
        } while (!nos_timer_expired(10001) && ret_val == ERROR_FAIL);
        
        nos_timer_release(10001);
        
#ifdef MG2410_DEBUG
        printf("\n\rTX done duration:%d\n\r", nos_timer_get_time(10001));
#endif
    }
    else
    {
        BOOL intr_flag_status;
        
        nos_delay_ms(10);
        nos_mg2410_get_mcu_intr_status(dev_id, &intr_flag_status);
        
        if (intr_flag_status)
        {
            nos_mg2410_clear_mcu_intr(dev_id);
            if (HAL_InterruptGetAndClear(dev_id) == 1) //TX end intr
            {
                ret_val = ERROR_SUCCESS;
            }
        }
    }
    
    if (ret_val == ERROR_FAIL)
    {
        HAL_TxCancel(dev_id);
        HAL_InterruptGetAndClear(dev_id);
        nos_mg2410_clear_mcu_intr(dev_id);
    }

    HAL_InterruptEnable(dev_id, MG2410_INT_RxEnd);
    return ret_val;
}


ERROR_T nos_mg2410_wait_for_ack(uint8_t dev_id, uint8_t seq)
{
    ERROR_T ret = ERROR_FAIL;
    
    // ACK should be arrived in 54symbol (864us)
    if (nos_timer_config(10000, 54*SYMBOL_DURATION))
    {
        nos_timer_start(10000);
        do{
            if (wpan_dev[dev_id].c.mg2410.ack_rcvd)
            {
                ret = ERROR_SUCCESS;
                break;
            }
        }while(!nos_timer_expired(10000));
        
#ifdef MG2410_DEBUG
        printf("\n\rWaiting Time for ACK:%d", nos_timer_get_time(10000));
#endif 
        nos_timer_release(10000);
    }
    else
    {
        nos_delay_us(54*SYMBOL_DURATION);
        if (wpan_dev[dev_id].c.mg2410.ack_rcvd)
        {
            ret = ERROR_SUCCESS;
        }
    }
    return ret;
}

BOOL nos_mg2410_rxbuf_is_empty(UINT8 dev_id)
{
    if (wpan_dev[dev_id].c.mg2410.rf_rx_frame_length == 0)
        //if (HAL_RegisterRead(dev_id, xMRFCRP) == HAL_RegisterRead(dev_id, xMRFCWP))
        //if (HAL_RegisterRead(dev_id, xMRFCSIZE) == 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

ERROR_T nos_mg2410_read_frame(UINT8 dev_id, struct wpan_rx_frame *f)
{
    // Read MAC frame length
    if (wpan_dev[dev_id].c.mg2410.rf_rx_frame_length == 0)
    {
        return ERROR_INVALID_FRAME;
    }
    
    f->len = wpan_dev[dev_id].c.mg2410.rf_rx_frame_length - 2; // 2 is length of FCS.

    memcpy(f->buf, wpan_dev[dev_id].c.mg2410.rf_rx_frame, f->len);
    f->rssi = f->lqi = wpan_dev[dev_id].c.mg2410.rf_rx_rssi;
    
    wpan_dev[dev_id].c.mg2410.rf_rx_frame_length = 0;
    return ERROR_SUCCESS;
}

//return TRUE if data packet has been read.
bool nos_mg2410_read_rxfifo(UINT8 dev_id)
{
    bool stop_interrupt = FALSE;

#ifdef MG2410_DEBUG
    nos_rf_modem_print_rxfifo_info();
#endif

    // Get RSSI of the received packet.
    wpan_dev[dev_id].c.mg2410.rf_rx_rssi = HAL_RssiGet(dev_id);

    // The first byte indicates the length of the packet.
    wpan_dev[dev_id].c.mg2410.rf_rx_frame_length = HAL_RxFifoPopSingle(dev_id);

    // check if ACK frame
    if (wpan_dev[dev_id].c.mg2410.rf_rx_frame_length == 5)
    {
        HAL_RxFifoPopBurst(dev_id,
                           (UINT8 *) wpan_dev[dev_id].c.mg2410.rf_rx_frame,
                           5);
        wpan_dev[dev_id].c.mg2410.rf_rx_frame_length = 0;
        wpan_dev[dev_id].c.mg2410.ack_rcvd = TRUE;
        return FALSE;
    }

    // length error
    else if ((wpan_dev[dev_id].c.mg2410.rf_rx_frame_length < 5) ||
             (wpan_dev[dev_id].c.mg2410.rf_rx_frame_length & 0x80))
    {
        nos_mg2410_flush_rxbuf(dev_id);
        return FALSE;
    }

    HAL_RxFifoPopBurst(dev_id,
                       (UINT8 *) wpan_dev[dev_id].c.mg2410.rf_rx_frame,
                       (UINT8) wpan_dev[dev_id].c.mg2410.rf_rx_frame_length);
    wpan_dev[dev_id].notify(dev_id, 0, &stop_interrupt);
    return TRUE;
}

void nos_mg2410_isr(UINT8 dev_id)
{
    int rf_intr_idx;
    while(1)
    {
        //RF intr idx queue length: 32
        rf_intr_idx = HAL_InterruptGetAndClear(dev_id);

        if (rf_intr_idx == 0)  //0: none or Modem-on
        {
            if (HAL_InterruptGetAndClear(dev_id) == 0)
            {
                // confirm whether it's modem-on intr or not.
                return;
            }
        }
        else if (rf_intr_idx == 1)			//1: TX-end
        {
            wpan_dev[dev_id].c.mg2410.tx_done = TRUE;
        }
        else if (rf_intr_idx == 3)			//3: RX-end
        {
            nos_mg2410_read_rxfifo(dev_id);
        }
    }
}

static const struct wpan_dev_cmdset mg2410_cmd =
{
    nos_mg2410_setup,
    nos_mg2410_read_frame,
    nos_mg2410_write_frame,
    nos_mg2410_rxbuf_is_empty,
    nos_mg2410_flush_rxbuf,
    nos_mg2410_sleep,
    nos_mg2410_wakeup,
    nos_mg2410_tx,
    nos_mg2410_wait_for_ack,
    NULL,
    nos_mg2410_set_pan_id,
    nos_mg2410_set_short_addr,
    nos_mg2410_set_eui64,
};

void nos_mg2410_init(UINT8 dev_id)
{	
    UINT8 status;

    wpan_dev[dev_id].hw_autotx = FALSE;
    wpan_dev[dev_id].sec_support = FALSE;
    wpan_dev[dev_id].addrdec_support = TRUE;
    wpan_dev[dev_id].symbol_duration = SYMBOL_DURATION;
    wpan_dev[dev_id].cmd = &mg2410_cmd;
	
    /* ON and Reset MG2410 */
    CB_Control_DVREG_EN(dev_id, 1);		//ON 
    CB_Control_RESETB(dev_id, 0);		// RESET signal on
    nos_delay_us(40);
    CB_Control_RESETB(dev_id, 1);		// RESET signal off
    nos_delay_us(40);

    /* Check MG2410 CHIP_ID to test SPI comm */
    status = 0;
    status = HAL_RegisterRead(dev_id, xCHIPID);
    if (status == 0xC3 || (status & 0xF0) != 0xC0)
    {
        while(1)
        {
            nos_led_alarm(3);
            nos_led_num_display(1);
            nos_delay_ms(500);
        };
    }

    /* MG2410 Configuration */
    if (HAL_RegisterConfig(dev_id))
    {
        while(1)
        {
            nos_led_alarm(3);
            nos_led_num_display(2);
            nos_delay_ms(500);
        };
    }

    nos_mg2410_sleep(dev_id);
    HAL_DataRateSet(dev_id, MG2410_DataRate_250K);
    HAL_MacCtrlSet(dev_id,
                   MG2410_MacCtrl_PreventAck |
                   MG2410_MacCtrl_AutoAck |
                   MG2410_MacCtrl_AutoCrc |
                   MG2410_MacCtrl_AddrDecode);
    HAL_InterruptEnable(dev_id, MG2410_INT_RxEnd);	//modem-on, tx-end interrupt disable
    HAL_AutoAckFramePendSet(dev_id, 0);
}


#ifdef MG2410_DEBUG
void nos_rf_modem_print_rxfifo_info()
{
    printf("\n\r***RXFIFO RP:%d\n\r***RXFIFO WR:%d\n\r***RXFIFO SIZE:%d",
           HAL_RegisterRead(xMRFCRP),
           HAL_RegisterRead(xMRFCWP),
           HAL_RegisterRead(xMRFCSIZE));
}
void nos_rf_modem_print_txfifo_info()
{
    printf("\n\r***TXFIFO RP:%d\n\r***TXFIFO WR:%d\n\r***TXFIFO SIZE:%d",
           HAL_RegisterRead(xMTFCRP),
           HAL_RegisterRead(xMTFCWP),
           HAL_RegisterRead(xMTFCSIZE));
}
void nos_rf_modem_print_eui64(uint8_t* eui64)
{
    printf("\r\nEUI64: %2x %2x %2x %2x %2x %2x %2x %2x",
           eui64[0],eui64[1],eui64[2],eui64[3],eui64[4],eui64[5],eui64[6],eui64[7]);	
}

#endif
#endif //MG2410_M
