
/*******************************************************************************
	- Chip		: MG2410
	- Vendor		: RadioPulse Inc, 2010.
	- Date		: 2010-07-31
	- Version		: VER 1.0a
	
	[2010-07-31] VER 1.0a
	- Added definitions for several registers.

	[2010-07-10] VER 1.00
	- Initial Version
*******************************************************************************/


#ifndef __MG2410_H__
#define __MG2410_H__


	//	MAC TX FIFO CONTROL when xSECMAP's bit[0] = 0
	#define	xMTFCPUSH		(0x000)
	#define	xMTFCWP		(0x001)
	#define	xMTFCRP		(0x002)
	#define	xMTFCCTL		(0x003)
	#define	xMTFCSTA		(0x004)
	#define	xMTFCSIZE		(0x005)
	#define	xMTFCROOM		(0x006)
	//						(0x007)
	//						(0x008)
	#define	xAACKFC0		(0x009)
	#define	xAACKFC1		(0x00A)
	#define	xAACKDSN		(0x00B)
	#define	xAACKSTA		(0x00C)
	//						(0x00D)
	//						(0x00E)
	//						(0x00F)

	//	MAC RX FIFO CONTROL when xSECMAP's bit[0] = 0
	#define	xMRFCPOP		(0x080)
	#define	xMRFCWP		(0x081)
	#define	xMRFCRP		(0x082)
	#define	xMRFCCTL		(0x083)
	#define	xMRFCSTA		(0x084)
	#define	xMRFCSIZE		(0x085)
	#define	xMRFCROOM		(0x086)
	//						(0x087)
	//						(0x088)
	//						(0x089)
	//						(0x08A)
	//						(0x08B)
	//						(0x08C)
	//						(0x08D)
	//						(0x08E)
	//						(0x08F)

	//	MAC TX FIFO and RX FIFO when xSECMAP's bit[0] = 0
	#define	xMTXFIFO0		(0x300)
	#define	xMRXFIFO0		(0x400)

	//	SECURITY TX FIFO CONTROL when xSECMAP's bit[0] = 1
	#define	xSTFCPUSH		(0x000)
	#define	xSTFCWP		(0x001)
	#define	xSTFCRP		(0x002)
	#define	xSTFCCTL		(0x003)
	#define	xSTFCSTA		(0x004)
	#define	xSMTFCSIZE		(0x005)
	#define	xSTFCROOM		(0x006)
	#define	xSTFCSECBASE	(0x007)
	#define	xSTFCSECLEN	(0x008)
	#define	xSTFDMALEN	(0x009)
	#define	xSTFDMACTL		(0x00A)
	//						(0x00B)
	//						(0x00C)
	//						(0x00D)
	//						(0x00E)
	//						(0x00F)

	//	SECURITY RX FIFO CONTROL when xSECMAP's bit[0] = 1
	#define	xSRFCPOP		(0x080)
	#define	xSRFCWP		(0x081)
	#define	xSRFCRP		(0x082)
	#define	xSRFCCTL		(0x083)
	#define	xSRFCSTA		(0x084)
	#define	xSRFCSIZE		(0x085)
	#define	xSRFCROOM		(0x086)
	#define	xSRFCSECBASE	(0x087)
	#define	xSRFCSECLEN	(0x088)
	#define	xSRFDMALEN	(0x089)
	#define	xSRFDMACTL		(0x08A)
	//						(0x08B)
	//						(0x08C)
	//						(0x08D)
	//						(0x08E)
	//						(0x08F)

	//	SECURITY TX FIFO and RX FIFO when xSECMAP's bit[0] = 1
	#define	xSTXFIFO0		(0x300)
	#define	xSRXFIFO0		(0x400)


	//	MAC CONTROL - SECURITY KEY0
	#define	xKEY0_0	(0x100)
	#define	xKEY0_1	(0x101)
	#define	xKEY0_2	(0x102)
	#define	xKEY0_3	(0x103)
	#define	xKEY0_4	(0x104)
	#define	xKEY0_5	(0x105)
	#define	xKEY0_6	(0x106)
	#define	xKEY0_7	(0x107)
	#define	xKEY0_8	(0x108)
	#define	xKEY0_9	(0x109)
	#define	xKEY0_10	(0x10A)
	#define	xKEY0_11	(0x10B)
	#define	xKEY0_12	(0x10C)
	#define	xKEY0_13	(0x10D)
	#define	xKEY0_14	(0x10E)
	#define	xKEY0_15	(0x10F)

	//	MAC CONTROL - SECURITY RXNONCE
	#define	xRXNONCE_KeySeq		(0x110)
	#define	xRXNONCE_FrameCnt0	(0x111)
	#define	xRXNONCE_FrameCnt1	(0x112)
	#define	xRXNONCE_FrameCnt2	(0x113)
	#define	xRXNONCE_FrameCnt3	(0x114)
	#define	xRXNONCE_ExtAddr0		(0x115)
	#define	xRXNONCE_ExtAddr1		(0x116)
	#define	xRXNONCE_ExtAddr2		(0x117)
	#define	xRXNONCE_ExtAddr3		(0x118)
	#define	xRXNONCE_ExtAddr4		(0x119)
	#define	xRXNONCE_ExtAddr5		(0x11A)
	#define	xRXNONCE_ExtAddr6		(0x11B)
	#define	xRXNONCE_ExtAddr7		(0x11C)
	//								(0x11D)
	//								(0x11E)
	//								(0x11F)

	//	MAC CONTROL - SECURITY SAES BUFFER
	#define	xSAESBUF0	(0x120)
	#define	xSAESBUF1	(0x121)
	#define	xSAESBUF2	(0x122)
	#define	xSAESBUF3	(0x123)
	#define	xSAESBUF4	(0x124)
	#define	xSAESBUF5	(0x125)
	#define	xSAESBUF6	(0x126)
	#define	xSAESBUF7	(0x127)
	#define	xSAESBUF8	(0x128)
	#define	xSAESBUF9	(0x129)
	#define	xSAESBUF10	(0x12A)
	#define	xSAESBUF11	(0x12B)
	#define	xSAESBUF12	(0x12C)
	#define	xSAESBUF13	(0x12D)
	#define	xSAESBUF14	(0x12E)
	#define	xSAESBUF15	(0x12F)

	//	MAC CONTROL - SECURITY KEY1
	#define	xKEY1_0	(0x130)
	#define	xKEY1_1	(0x131)
	#define	xKEY1_2	(0x132)
	#define	xKEY1_3	(0x133)
	#define	xKEY1_4	(0x134)
	#define	xKEY1_5	(0x135)
	#define	xKEY1_6	(0x136)
	#define	xKEY1_7	(0x137)
	#define	xKEY1_8	(0x138)
	#define	xKEY1_9	(0x139)
	#define	xKEY1_10	(0x13A)
	#define	xKEY1_11	(0x13B)
	#define	xKEY1_12	(0x13C)
	#define	xKEY1_13	(0x13D)
	#define	xKEY1_14	(0x13E)
	#define	xKEY1_15	(0x13F)

	//	MAC CONTROL - SECURITY TXNONCE
	#define	xTXNONCE_KeySeq		(0x140)
	#define	xTXNONCE_FrameCnt0	(0x141)
	#define	xTXNONCE_FrameCnt1	(0x142)
	#define	xTXNONCE_FrameCnt2	(0x143)
	#define	xTXNONCE_FrameCnt3	(0x144)
	#define	xTXNONCE_ExtAddr0		(0x145)
	#define	xTXNONCE_ExtAddr1		(0x146)
	#define	xTXNONCE_ExtAddr2		(0x147)
	#define	xTXNONCE_ExtAddr3		(0x148)
	#define	xTXNONCE_ExtAddr4		(0x149)
	#define	xTXNONCE_ExtAddr5		(0x14A)
	#define	xTXNONCE_ExtAddr6		(0x14B)
	#define	xTXNONCE_ExtAddr7		(0x14C)
	//								(0x14D)
	//								(0x14E)
	//								(0x14F)

	//	MAC CONTROL - IEEE ADDRESS
	#define	xIEEEADDR0		(0x150)
	#define	xIEEEADDR1		(0x151)
	#define	xIEEEADDR2		(0x152)
	#define	xIEEEADDR3		(0x153)
	#define	xIEEEADDR4		(0x154)
	#define	xIEEEADDR5		(0x155)
	#define	xIEEEADDR6		(0x156)
	#define	xIEEEADDR7		(0x157)
	#define	xPANID0		(0x158)
	#define	xPANID1		(0x159)
	#define	xSHORTADDR0	(0x15A)
	#define	xSHORTADDR1	(0x15B)
	//						(0x15C)
	//						(0x15D)
	//						(0x15E)
	//						(0x15F)

	//-----------------------------------------------------
	// MAC STATUS
	//-----------------------------------------------------
	// bit[7] : enc/dec			:: default= 0. Active High
	// bit[6] : tx_busy			:: default= 0. Active High
	// bit[5] : rx_busy			:: default= 0. Active High
	// bit[4] : saes_done		:: default= 0. Active High
	// bit[3] : decode_ok		:: default= 1. Active High
	// bit[2] ; enc_done		:: default= 0. Active High
	// bit[1] : dec_done		:: default= 0. Active High
	// bit[0] : crc_ok			:: default= 1. Active High
	#define	xMACSTS	(0x180)
		#define	MACSTS_EncDecBusy	0x80
		#define	MACSTS_TxBusy		0x40
		#define	MACSTS_RxBusy		0x20
		#define	MACSTS_SaesDone	0x10
		#define	MACSTS_DecodeOk	0x08
		#define	MACSTS_EncDone	0x04
		#define	MACSTS_DecDone	0x02
		#define	MACSTS_CrcOk		0x01

	//-----------------------------------------------------

	//-----------------------------------------------------
	// MAC SAES
	// bit[7:1] 	: rsv
	// bit[0]		: SAES		:: Write-Only
	#define	xMACSAES	(0x18E)
	//-----------------------------------------------------

	//-----------------------------------------------------
	// MAC MAIN
	//-----------------------------------------------------
	// bit[7] : rst_fifo				:: default=0. Active High
	// bit[6] : rst_tsm				:: default=0. Active High
	// bit[5] : rst_rsm				:: default=0. Active High
	// bit[4] : rst_aes				:: default=0. Active High
	// bit[3:0] : rsv
	#define	xMACRST	(0x190)
	//-----------------------------------------------------

	//-----------------------------------------------------
	// MAC MODEM
	//-----------------------------------------------------
	// bit[7:5] : rsv
	// bit[4] : prevent_ack_packet	:: default=0. Active High
	// bit[3] : pan_coordinator		:: default=0. Active High
	// bit[2] ; addr_decode			:: default=1. Active High
	// bit[1] : auto_crc				:: default=1. Active High
	// bit[0] : auto_ack			:: default=0. Active High
	#define	xMACCTRL	(0x191)
	//-----------------------------------------------------

	#define	xMACDSN	(0x192)

	//-----------------------------------------------------
	// MAC SEC
	//-----------------------------------------------------
	// bit[7] : sa_keysel
	// bit[6] : tx_keysel
	// bit[5] : rx_keysel
	// bit[4:2] : sec_m[2:0]
	// bit[1:0] : sec_mode[1:0].		0=None, 1=CBC-MAC, 2=CTR, 3=CCM
	#define	xMACSEC	(0x193)
	//-----------------------------------------------------

	#define	xTXL		(0x194)
	#define	xRXL		(0x195)

	//-----------------------------------------------------
	// SEC MAP
	// bit[7:1]	: rsv
	// bit[0]		: 1=MAC FIFO Access, 0=SEC FIFO Access. For registers addressed from 0x000 to 0x4FF.
	//-----------------------------------------------------
	#define	xSECMAP	(0x19F)


	#define	xPCMD0		(0x200)
	#define	xPCMD1		(0x201)
	#define	xPLLPD		(0x202)
	#define	xPLLPU		(0x203)
	#define	xRXRFPD	(0x204)
	#define	xRXRFPU	(0x205)

	#define	xMDMCNF3	(0x211)
	#define	xMDMCNF6	(0x214)
	#define	xMDMCNF9	(0x217)
	#define	xGP_DOUT	(0x219)	
	#define	xGP_OEN	(0x21A)	
	#define	xGP_IE		(0x21B)	
	#define	xGP_PE		(0x21C)	
	#define	xGP_PS		(0x21D)	
	#define	xGP_DS		(0x21E)
	#define	xGP_DIN	(0x21F)	// Read Only		

	#define	xAGCCNF0	(0x220)
	#define	xAGCCNF1	(0x221)
	#define	xAGCCNF2	(0x222)
	#define	xAGCCNF3	(0x223)
	#define	xAGCCNF4	(0x224)
	#define	xAGCCNF5	(0x225)
	#define	xAGCCNF6	(0x226)
	#define	xAGCCNF7	(0x227)
	#define	xAGCCNF8	(0x228)
	#define	xAGCCNF9	(0x229)
	#define	xAGCCNF11	(0x22B)
	#define	xAGCCNF12	(0x22C)	

	#define	xAGCCNF15	(0x230)
	#define	xAGCCNF16	(0x231)
	#define	xAGCCNF17	(0x232)
	#define	xAGCCNF18	(0x233)
	#define	xAGCCNF19	(0x234)
	#define	xAGCCNF20	(0x235)
	#define	xAGCCNF21	(0x236)
	#define	xAGCCNF22	(0x237)
	#define	xAGCCNF23	(0x238)
	#define	xAGCCNF24	(0x239)
	#define	xAGCCNF25	(0x23A)
	#define	xAGCCNF26	(0x23B)
	#define	xAGCCNF28	(0x23D)
	#define	xAGCCNF30	(0x23F)	

	#define	xCORCNF0	(0x240)
	#define	xCORCNF1	(0x241)
	#define	xCORCNF2	(0x242)
	#define	xCORCNF4	(0x244)
	#define	xCORCNF7	(0x247)
	#define	xCORCNF8	(0x248)
	
	#define	xCCA0		(0x24C)
	#define	xCCA1		(0x24D)

	#define	xTST0		(0x260)
	#define	xTST2		(0x262)	
	#define	xEXTCLK		(0x267)	
	#define	xTST8		(0x268)
	#define	xTST11		(0x26B)
	#define	xLQICNF0	(0x26E)	// Read Only bit[7]
	#define	xLQICNF1	(0x26F)	// Read Only

	#define	xPHYSTS	(0x270)	// Read Only
	#define	xAGCSTS2	(0x274)	// Read Only
	#define	xAGCSTS3	(0x275)	// Read Only
	#define	xMONCON0	(0x279)		
	#define	xMONCON3	(0x27C)
	#define	xINTCON	(0x27D)	// Read Only bit[5]
	#define	xINTIDX		(0x27E)	// Read Only bit[3], bit[1:0]

	#define	xPLLCTRL	(0x280)
	#define	xPLLVCO1	(0x287)
	#define	xPLLVCO2	(0x288)
	#define	xPLLAFC		(0x28B)
	#define	xPLLMON	(0x28C)	// Read Only bit[2:0]
	#define	xDMSCALE	(0x28F)

	#define	xDMKDIV1	(0x294)
	#define	xCHNLSEL	(0x296)
	#define	xPLLFREQ	(0x297)
	#define	xRXCUR		(0x298)
	#define	xRXCTRL		(0x299)
	#define	xRXBW1		(0x29A)
	#define	xRXBW2		(0x29B)
	#define	xTSEN		(0x29C)
	#define	xTXPA		(0x29E)
	#define	xTXDA		(0x29F)

	#define	xDCCDAC	(0x2A2)
	#define	xDCCCON	(0x2A3)
	#define	xDCCCNF	(0x2A8)
	#define	xDMCAL1	(0x2A9)
	#define	xRSVD1		(0x2AC)
	#define	xCHIPID		(0x2AF)	// Read Only

	//-	xCLKON0
	//	bit[7]	: AESCLK.	1=Off 	0=On
	//	bit[6]	: MTCLK.		1=Off 	0=On
	//	bit[5]	: MRCLK.		1=Off 	0=On
	//	bit[4]	: SREGCLK.	1=On	0=Off
	//	bit[3]	: RX3CLK.	1=Off	0=On
	//	bit[2]	: RX2CLK.	1=Off	0=On
	//	bit[1]	: RX1CLK.	1=Off	0=On
	//	bit[0]	: ADCCLK.	1=Off	0=On
	#define	xCLKON0	(0x2C0)
	//-	xCLKON1
	//	bit[7:5]	: reserved
	//	bit[4]	: TSTCLK.	1=On	0=Off
	//	bit[3]	: TXCLK.		1=Off	0=On
	//	bit[2]	: MPICLK.	1=On	0=Off
	//	bit[1]	: COR0CLK.	1=Off	0=On
	//	bit[0]	: COR1CLK.	1=Off	0=On
	#define	xCLKON1	(0x2C1)
	#define	xCLKFE0		(0x2C3)
	#define	xCLKFE1		(0x2C4)
	#define	xCLKCFG	(0x2C6)
	#define	xAVREG		(0x2CF)	// Read Only bit[0]

	// - SPI Command
	#define	MG2410_SpiCmd_RegWr_Byte1	0x3F
	#define	MG2410_SpiCmd_RegWr_Byte3	0x00
	#define	MG2410_SpiCmd_RegRd_Byte1	0x3F
	#define	MG2410_SpiCmd_RegRd_Byte3	0x80
	#define	MG2410_SpiCmd_SinglePush		0x3E
	#define	MG2410_SpiCmd_BurstPush		0x7E
	#define	MG2410_SpiCmd_SinglePop		0xBD
	#define	MG2410_SpiCmd_BurstPop		0xFD

	//- Data Rate Mode
	#define	MG2410_DataRate_31K			0
	#define	MG2410_DataRate_62K			1
	#define	MG2410_DataRate_125K			2
	#define	MG2410_DataRate_250K			3
	#define	MG2410_DataRate_500K			4
	#define	MG2410_DataRate_1000K			5
	#define	MG2410_DataRate_1300K			6
	#define	MG2410_DataRate_1500K			7
	#define	MG2410_DataRate_2000K			8
	#define	MG2410_DataRate_2600K			9
	#define	MG2410_DataRate_3000K			10
	#define	MG2410_DataRate_4000K			11


	//-	Interrupt Mask. 
	#define	MG2410_INT_RxEnd		0x08
	#define	MG2410_INT_RxStart		0x04
	#define	MG2410_INT_TxEnd		0x02
	#define	MG2410_INT_ModemOn	0x01

	//-	MAC Control Register Mask. Active High.
	#define	MG2410_MacCtrl_PreventAck	0x10
	#define	MG2410_MacCtrl_PANCoordi	0x08
	#define	MG2410_MacCtrl_AddrDecode	0x04
	#define	MG2410_MacCtrl_AutoCrc		0x02
	#define	MG2410_MacCtrl_AutoAck	0x01

	//-	IEEE 802.15.4 Constants
	#define	maccMaxPHYPacketSize			127
	#define	maccMinPHYPacketSize			5

	//-	IEEE 802.15.4 Status Enumeration
	#define	maccSTA_SUCCESS 				0x00
	#define	maccSTA_CHAN_ACCESS_FAIL 	0xE1  
	#define	maccSTA_FRAME_TOO_LONG 		0xE5
	#define	maccSTA_INVALID_PARAMETER	0xE8
	#define	maccSTA_NO_ACK				0xE9
	#define	maccSTA_FRAME_TOO_SHORT 	0xF8

	//-	IEEE 802.15.4 Frame Structure - FrameControl_Low Field
	#define	maccFCL_FrameType		0x07
	#define	maccFCL_Security		0x08
	#define	maccFCL_FramePend		0x10
	#define	maccFCL_AckReq		0x20
	#define	maccFCL_IntraPan		0x40
	#define	maccFCL_rsv			0x80

	//-	IEEE 802.15.4 Frame Structure - FrameControl_High Field
	#define	maccFCH_rsv01			0x03
	#define	maccFCH_DstMode		0x0C
	#define	maccFCH_rsv45			0x30
	#define	maccFCH_SrcMode		0xC0

	//-	MAC Frame
	#define	maccFrame_Beacon		0
	#define	maccFrame_Data		1
	#define	maccFrame_Ack			2
	#define	maccFrame_Command	3

	//-	MAC Address Mode
	#define	maccAddrMode_None	0
	#define	maccAddrMode_Rsv		1
	#define	maccAddrMode_16bit	2
	#define	maccAddrMode_64bit	3

	//-	MAC Address
	#define	maccFFFE				0xFFFE
	#define	maccBroadcast			0xFFFF

#endif		// #ifndef __MG2410_H__