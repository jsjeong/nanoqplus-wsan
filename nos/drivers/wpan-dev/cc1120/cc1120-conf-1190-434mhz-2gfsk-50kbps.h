// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC1120 Configuration for 434MHz, 2GFSK, 50Kbps with CC1190
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 9. 2.
 */

#ifndef CC1120_CONF_1190_434MHZ_2GFSK_50KBPS_H
#define CC1120_CONF_1190_434MHZ_2GFSK_50KBPS_H

#include "cc1120-const.h"

#define CC1120_CONF_CC1190_434MHZ_2GFSK_50KBPS  \
    {                                           \
        { REG_SYNC_CFG1,         0x08 },        \
        { REG_SYNC_CFG0,         0x17 },        \
        { REG_DEVIATION_M,       0x99 },        \
        { REG_MODCFG_DEV_E,      0x0D },        \
        { REG_DCFILT_CFG,        0x15 },        \
        { REG_PREAMBLE_CFG1,     0x18 },        \
        { REG_PREAMBLE_CFG0,     0x2A },        \
        { REG_FREQ_IF_CFG,       0x3A },        \
        { REG_IQIC,              0x00 },        \
        { REG_CHAN_BW,           0x02 },        \
        { REG_MDMCFG1,           0x46 },        \
        { REG_MDMCFG0,           0x05 },        \
        { REG_SYMBOL_RATE2,      0x99 },        \
        { REG_SYMBOL_RATE1,      0x99 },        \
        { REG_SYMBOL_RATE0,      0x99 },        \
        { REG_AGC_REF,           0x3C },        \
        { REG_AGC_CS_THR,        0xEF },        \
        { REG_AGC_GAIN_ADJUST,   0x00 },        \
        { REG_AGC_CFG3,          0x91 },        \
        { REG_AGC_CFG2,          0x20 },        \
        { REG_AGC_CFG1,          0xA9 },        \
        { REG_AGC_CFG0,          0xC0 },        \
        { REG_FIFO_CFG,          0x00 },        \
        { REG_DEV_ADDR,          0x00 },        \
        { REG_SETTLING_CFG,      0x03 },        \
        { REG_FS_CFG,            0x14 },        \
        { REG_WOR_CFG1,          0x08 },        \
        { REG_WOR_CFG0,          0x21 },        \
        { REG_WOR_EVENT0_MSB,    0x00 },        \
        { REG_WOR_EVENT0_LSB,    0x00 },        \
        { REG_PKT_CFG2,          0x04 },        \
        { REG_PKT_CFG1,          0x05 },        \
        { REG_PKT_CFG0,          0x20 },        \
        { REG_RFEND_CFG1,        0x0F },        \
        { REG_RFEND_CFG0,        0x00 },        \
        { REG_PA_CFG2,           0x7F },        \
        { REG_PA_CFG1,           0x56 },        \
        { REG_PA_CFG0,           0x79 },        \
        { REG_PKT_LEN,           0xFF },        \
        { REG_IF_MIX_CFG,        0x00 },        \
        { REG_FREQOFF_CFG,       0x20 },        \
        { REG_TOC_CFG,           0x0A },        \
        { REG_MARC_SPARE,        0x00 },        \
        { REG_ECG_CFG,           0x00 },        \
        { REG_SOFT_TX_DATA_CFG,  0x00 },        \
        { REG_EXT_CTRL,          0x01 },        \
        { REG_RCCAL_FINE,        0x00 },        \
        { REG_RCCAL_COARSE,      0x00 },        \
        { REG_RCCAL_OFFSET,      0x00 },        \
        { REG_FREQOFF1,          0x00 },        \
        { REG_FREQOFF0,          0x00 },        \
        { REG_FREQ2,             0x6C },        \
        { REG_FREQ1,             0x80 },        \
        { REG_FREQ0,             0x00 },        \
        { REG_IF_ADC2,           0x02 },        \
        { REG_IF_ADC1,           0xA6 },        \
        { REG_IF_ADC0,           0x04 },        \
        { REG_FS_DIG1,           0x00 },        \
        { REG_FS_DIG0,           0x5F },        \
        { REG_FS_CAL3,           0x00 },        \
        { REG_FS_CAL2,           0x20 },        \
        { REG_FS_CAL1,           0x40 },        \
        { REG_FS_CAL0,           0x0E },        \
        { REG_FS_CHP,            0x28 },        \
        { REG_FS_DIVTWO,         0x03 },        \
        { REG_FS_DSM1,           0x00 },        \
        { REG_FS_DSM0,           0x33 },        \
        { REG_FS_DVC1,           0xFF },        \
        { REG_FS_DVC0,           0x17 },        \
        { REG_FS_PFD,            0x50 },        \
        { REG_FS_PRE,            0x6E },        \
        { REG_FS_REG_DIV_CML,    0x14 },        \
        { REG_FS_SPARE,          0xAC },        \
        { REG_FS_VCO4,           0x14 },        \
        { REG_FS_VCO3,           0x00 },        \
        { REG_FS_VCO2,           0x00 },        \
        { REG_FS_VCO1,           0x00 },        \
        { REG_FS_VCO0,           0xB4 },        \
        { REG_GBIAS6,            0x00 },        \
        { REG_GBIAS5,            0x02 },        \
        { REG_GBIAS4,            0x00 },        \
        { REG_GBIAS3,            0x00 },        \
        { REG_GBIAS2,            0x10 },        \
        { REG_GBIAS1,            0x00 },        \
        { REG_GBIAS0,            0x00 },        \
        { REG_IFAMP,             0x01 },        \
        { REG_LNA,               0x01 },        \
        { REG_RXMIX,             0x01 },        \
        { REG_XOSC5,             0x0E },        \
        { REG_XOSC4,             0xA0 },        \
        { REG_XOSC3,             0x03 },        \
        { REG_XOSC2,             0x04 },        \
        { REG_XOSC1,             0x03 },        \
        { REG_ANALOG_SPARE,      0x00 },        \
        { REG_PA_CFG3,           0x00 },        \
        { REG_IRQ0M,             0x00 },        \
        { REG_BIST,              0x00 },        \
        { REG_DCFILTOFFSET_I1,   0x00 },        \
        { REG_DCFILTOFFSET_I0,   0x00 },        \
        { REG_DCFILTOFFSET_Q1,   0x00 },        \
        { REG_DCFILTOFFSET_Q0,   0x00 },        \
        { REG_IQIE_I1,           0x00 },        \
        { REG_IQIE_I0,           0x00 },        \
        { REG_IQIE_Q1,           0x00 },        \
        { REG_IQIE_Q0,           0x00 },        \
        { REG_AGC_GAIN2,         0xD1 },        \
        { REG_AGC_GAIN1,         0x00 },        \
        { REG_AGC_GAIN0,         0x3F },        \
        { REG_SOFT_TX_DATA_IN,   0x00 },        \
        { REG_RNDGEN,            0x7F },        \
        { REG_FSCAL_CTRL,        0x01 },        \
        { REG_SERIAL_STATUS,     0x00 },        \
        { REG_PA_IFAMP_TEST,     0x00 },        \
        { REG_FSRF_TEST,         0x00 },        \
        { REG_PRE_TEST,          0x00 },        \
        { REG_PRE_OVR,           0x00 },        \
        { REG_ADC_TEST,          0x00 },        \
        { REG_DVC_TEST,          0x0B },        \
        { REG_ATEST,             0x40 },        \
        { REG_ATEST_LVDS,        0x00 },        \
        { REG_ATEST_MODE,        0x00 },        \
        { REG_XOSC_TEST1,        0x3C },        \
        { REG_XOSC_TEST0,        0x00 },        \
        { REG_RXFIRST,           0x00 },        \
        { REG_TXFIRST,           0x00 },        \
        { REG_RXLAST,            0x00 },        \
        { REG_TXLAST,            0x00 },        \
        { 0xFFFF,                0x00 }}


#endif //CC1120_CONF_1190_434MHZ_2GFSK_50KBPS_H
