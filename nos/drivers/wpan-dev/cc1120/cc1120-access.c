// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief CC1120 FIFOs, registers access
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 9. 1.
 */

#include "cc1120-access.h"

#ifdef CC1120_M

#include "cc1120-const.h"
#include "cc1120-interface.h"

ERROR_T cc1120_command_strobe(UINT8 dev_id, UINT8 cmd, UINT8 *ret)
{
    ERROR_T err;
    
    err = cc1120_request_cs(dev_id);

    if (err != ERROR_SUCCESS)
        return err;
    
    cc1120_spi(dev_id, cmd, ret);
    cc1120_release_cs(dev_id);
    
    return ERROR_SUCCESS;
}

ERROR_T cc1120_set_register(UINT8 dev_id, UINT16 reg, UINT8 value)
{
    UINT8 ret;
    ERROR_T err;

    err = cc1120_request_cs(dev_id);

    if (err != ERROR_SUCCESS)
        return err;

    if ((reg & 0xff00) == 0x2f00) //extended register
    {
        cc1120_spi(dev_id, 0x40 | 0x2f, NULL);
        cc1120_spi(dev_id, reg & 0x00ff, NULL);
    }
    else
    {
        cc1120_spi(dev_id, 0x40 | (reg & 0x00ff), NULL);
    }
    cc1120_spi(dev_id, value, &ret);
    cc1120_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T cc1120_get_register(UINT8 dev_id, UINT16 reg, UINT8 *value)
{
    ERROR_T err;
    
    if (!value)
        return ERROR_INVALID_ARGS;
    
    err = cc1120_request_cs(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    if ((reg & 0xff00) == 0x2f00)
    {
        cc1120_spi(dev_id, 0xc0 | 0x2f, NULL);
        cc1120_spi(dev_id, reg & 0x00ff, NULL);
    }
    else
    {
        cc1120_spi(dev_id, 0xc0 | (reg & 0x00ff), NULL);
    }
    cc1120_spi(dev_id, 0, value);
    cc1120_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T cc1120_rxfifo_pop_single(UINT8 dev_id, UINT8 *value)
{
    ERROR_T err;
    
    if (!value)
        return ERROR_INVALID_ARGS;
    
    err = cc1120_request_cs(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    cc1120_spi(dev_id, CC1120_SINGLE_RXFIFO, NULL);
    cc1120_spi(dev_id, 0, value);
    cc1120_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T cc1120_rxfifo_pop_burst(UINT8 dev_id, UINT8 *data, UINT8 n)
{
    int i;
    ERROR_T err;
    
    if (!data || n == 0)
        return ERROR_INVALID_ARGS;

    err = cc1120_request_cs(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    cc1120_spi(dev_id, CC1120_BURST_RXFIFO, NULL);
    for (i = 0; i < n; i++)
    {
        cc1120_spi(dev_id, 0, &data[i]);
    }
    cc1120_release_cs(dev_id);

    return ERROR_SUCCESS;
}

ERROR_T cc1120_txfifo_push(UINT8 dev_id, const UINT8 *buf)
{
    UINT8 i;
    ERROR_T err;
    
    err = cc1120_request_cs(dev_id);
    if (err != ERROR_SUCCESS)
        return err;
    
    cc1120_spi(dev_id, CC1120_BURST_TXFIFO, NULL);
    cc1120_spi(dev_id, buf[0] - 2, NULL);
    for (i = 0; i < buf[0] - 2; i++)
    {
        cc1120_spi(dev_id, buf[i + 1], NULL);
    }
    cc1120_release_cs(dev_id);
    return ERROR_SUCCESS;
}

#endif //CC1120_M
