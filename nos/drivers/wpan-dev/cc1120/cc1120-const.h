// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief TI's CC112x Low-power High Performance Sub-1GHz RF Transceiver Driver
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 7. 5.
 */

#ifndef CC1120_CONST_H_
#define CC1120_CONST_H_

#define CC1120_REG_IOCFG3   0x0000
#define CC1120_REG_IOCFG2   0x0001
#define CC1120_REG_IOCFG1   0x0002
#define CC1120_REG_IOCFG0 	0x0003

#define CC1120_GPIO_ATRAN   (1<<7)
#define CC1120_GPIO_INV     (1<<6)
enum
{
    GPIOx_CFG_RXFIFO_THR = 0,
    GPIOx_CFG_RXFIFO_THR_PKT = 1,
    GPIOx_CFG_TXFIFO_THR = 2,
    GPIOx_CFG_TXFIFO_THR_PKT = 3,
    GPIOx_CFG_RXFIFO_OVERFLOW = 4,
    GPIOx_CFG_TXFIFO_UNDERFLOW = 5,
    GPIOx_CFG_PKT_SYNC_RXTX = 6,
    GPIOx_CFG_CRC_OK = 7,
    GPIOx_CFG_SERIAL_CLK = 8,
    GPIOx_CFG_SERIAL_RX = 9,
    GPIOx_CFG_PQT_REACHED = 11,
    GPIOx_CFG_PQT_VALID = 12,
    GPIOx_CFG_RSSI_VALID = 13,
    GPIO0_CFG_AGC_UPDATE = 14,
    GPIO1_CFG_AGC_HOLD = 14,
    GPIO2_CFG_RSSI_UPDATE = 14,
    GPIO3_CFG_RSSI_UPDATE = 14,
    GPIO0_CFG_TXONCCA_FAILED = 15,
    GPIO1_CFG_CCA_STATUS = 15,
    GPIO2_CFG_TXONCCA_DONE = 15,
    GPIO3_CFG_CCA_STATUS = 15,
    GPIOx_CFG_CARRIER_SENSE_VALID = 16,
    GPIOx_CFG_CARRIER_SENSE = 17,
    GPIO0_CFG_DSSS_DATA1 = 18,
    GPIO1_CFG_DSSS_CLK = 18,
    GPIO2_CFG_DSSS_DATA0 = 18,
    GPIO3_CFG_DSSS_CLK = 18,
    GPIOx_CFG_PKT_CRC_OK = 19,
    GPIOx_CFG_MARC_MCU_WAKEUP = 20,
    GPIOx_CFG_SYNC_LOW0_HIGH1 = 21,
    GPIOx_CFG_LNA_PA_REG_PD = 23,
    GPIOx_CFG_LNA_PD = 24,
    GPIOx_CFG_PA_PD = 25,
    GPIOx_CFG_RX0RX1 = 26,
    GPIOx_CFG_IMAGE_FOUND = 28,
    GPIOx_CFG_CLKEN_SOFT = 29,
    GPIOx_CFG_SOFT_TX_DATA_CLK = 30,
    GPIOx_CFG_RSSI_STEP_FOUND = 33,
    GPIOx_CFG_RSSI_STEP_EVENT = 34,
    GPIOx_CFG_ANTENNA_SELECT = 36,
    GPIOx_CFG_MARC_2PIN_STATUS1 = 37,
    GPIOx_CFG_MARC_2PIN_STATUS0 = 38,
    GPIO0_CFG_RXFIFO_UNDERFLOW = 39,
    GPIO2_CFG_TXFIFO_OVERFLOW = 39,
    GPIO0_CFG_CHFILT_STARTUP_VALID = 40,
    GPIO1_CFG_RCC_CAL_VALID = 40,
    GPIO2_CFG_CHFILT_VALID = 40,
    GPIO3_CFG_MAGN_VALID = 40,
    GPIO0_CFG_COLLISION_EVENT = 41,
    GPIO1_CFG_COLLISION_FOUND = 41,
    GPIO2_CFG_SYNC_EVENT = 41,
    GPIO3_CFG_COLLISION_FOUND = 41,
    GPIOx_CFG_PA_RAMP_UP = 42,
    GPIO0_CFG_UART_FRAMING_ERROR = 43,
    GPIO1_CFG_ADDR_FAILED = 43,
    GPIO2_CFG_LENGTH_FAILED = 43,
    GPIO3_CFG_CRC_FAILED = 43,
    GPIOx_CFG_AGC_STABLE_GAIN = 44,
    GPIOx_CFG_AGC_UPDATE = 45,
    GPIO0_CFG_ADC_I_DATA_SAMPLE = 46,
    GPIO1_CFG_ADC_CLOCK = 46,
    GPIO2_CFG_ADC_Q_DATA_SAMPLE = 46,
    GPIO3_CFG_ADC_CLOCK = 46,
    GPIOx_CFG_HIGHZ = 47,
    GPIOx_CFG_EXT_CLOCK = 49,
    GPIOx_CFG_CHIP_RDYn = 50,
    GPIOx_CFG_HW0 = 51,
    GPIOx_CFG_CLOCK_32K = 54,
    GPIOx_CFG_WOR_EVENT0 = 55,
    GPIOx_CFG_WOR_EVENT1 = 56,
    GPIOx_CFG_WOR_EVENT2 = 57,
    GPIOx_CFG_XOSC_STABLE = 59,
    GPIOx_CFG_EXT_OSC_EN = 60,
};

#define REG_SYNC3                    0x0004
#define REG_SYNC2                    0x0005
#define REG_SYNC1                    0x0006
#define REG_SYNC0                    0x0007

#define REG_SYNC_CFG1                0x0008

enum
{
    PQT_GATING_DISABLED = 0x00,
    PQT_GATING_ENABLED = 0x20,
};

#define REG_SYNC_CFG0                0x0009

enum
{
    SYNC_MODE_NO = 0x00,
    SYNC_MODE_11BITS = 0x04,
    SYNC_MODE_16BITS = 0x08,
    SYNC_MODE_18BITS = 0x0c,
    SYNC_MODE_24BITS = 0x10,
    SYNC_MODE_32BITS = 0x14,
    SYNC_MODE_16HBITS = 0x18,
    SYNC_MODE_16DBITS = 0x1c,
};

enum
{
    SYNC_ERROR_0BIT = 0x00,
    SYNC_ERROR_UNDER2BITS = 0x01,
    SYNC_ERROR_NO_CHECK = 0x03,
};

#define REG_DEVIATION_M              0x000A
#define REG_MODCFG_DEV_E             0x000B

enum
{
    MODEM_MODE_NORMAL = (0 << 6),
    MODEM_MODE_DSSS_REPEAT = (1 << 6),
    MODEM_MODE_DSSS_PN = (2 << 6),
};

enum
{
    MODEM_FORMAT_2FSK = (0 << 3),
    MODEM_FORMAT_2GFSK = (1 << 3),
    MODEM_FORMAT_ASK_OOK = (3 << 3),
    MODEM_FORMAT_4FSK = (4 << 3),
    MODEM_FORMAT_4GFSK = (5 << 3),
    MODEM_FORMAT_SCMSK_UNSHAPED = (6 << 3), // CC1125 Tx Only
    MODEM_FORMAT_SCMSK_SHAPED = (7 << 3), // CC1125 Tx Only
};

#define REG_DCFILT_CFG               0x000C

enum
{
    DCFILTER_OVRD_DCERROR = (0 << 6),
    DCFILTER_OVRD_MANUAL_COMPENSATION = (1 << 6),
};

enum
{
    DCFILT_BW_SETTLE_32 = (0 << 3),
    DCFILT_BW_SETTLE_64 = (1 << 3),
    DCFILT_BW_SETTLE_128 = (2 << 3),
    DCFILT_BW_SETTLE_256 = (3 << 3),
    DCFILT_BW_SETTLE_512 = (4 << 3),
};

#define REG_PREAMBLE_CFG1            0x000D

enum
{
    NUM_PREAMBLE_0 = (0 << 2),
    NUM_PREAMBLE_0_5 = (1 << 2),
    NUM_PREAMBLE_1 = (2 << 2),
    NUM_PREAMBLE_1_5 = (3 << 2),
    NUM_PREAMBLE_2 = (4 << 2),
    NUM_PREAMBLE_3 = (5 << 2),
    NUM_PREAMBLE_4 = (6 << 2),
    NUM_PREAMBLE_5 = (7 << 2),
    NUM_PREAMBLE_6 = (8 << 2),
    NUM_PREAMBLE_7 = (9 << 2),
    NUM_PREAMBLE_8 = (10 << 2),
    NUM_PREAMBLE_12 = (11 << 2),
    NUM_PREAMBLE_24 = (12 << 2),
    NUM_PREAMBLE_30 = (13 << 2),
};

enum
{
    PREAMBLE_WORD_1010 = (0 << 0),
    PREAMBLE_WORD_0101 = (1 << 0),
    PREAMBLE_WORD_0011 = (2 << 0),
    PREAMBLE_WORD_1100 = (3 << 0),
};

#define REG_PREAMBLE_CFG0            0x000E

enum
{
    DISABLE_PREAMBLE_DETECTION = (0 << 5),
    ENABLE_PREAMBLE_DETECTION = (1 << 5),
};

enum
{
    PQT_TIMEOUT_16SYM = (0 << 4),
    PQT_TIMEOUT_43SYM = (1 << 4),
};

#define REG_FREQ_IF_CFG              0x000F
#define REG_IQIC                     0x0010

enum
{
    DISABLE_IQIC = (0 << 7),
    ENABLE_IQIC = (1 << 7),
};

enum
{
    DISABLE_IQIC_UPDATE_COEFFICIENTS = (1 << 6),
    ENABLE_IQIC_UPDATE_COEFFICIENTS = (0 << 6),
};

enum
{
    IQIC_BLEN_SETTLE_8 = (0 << 4),
    IQIC_BLEN_SETTLE_32 = (1 << 4),
    IQIC_BLEN_SETTLE_128 = (2 << 4),
    IQIC_BLEN_SETTLE_256 = (3 << 4),
};

enum
{
    IQIC_BLEN_8 = (0 << 2),
    IQIC_BLEN_32 = (1 << 2),
    IQIC_BLEN_128 = (2 << 2),
    IQIC_BLEN_256 = (3 << 2),
};

enum
{
    IQIC_IMGCH_LEVEL_THR_OVER_256 = (0 << 0),
    IQIC_IMGCH_LEVEL_THR_OVER_512 = (1 << 0),
    IQIC_IMGCH_LEVEL_THR_OVER_1024 = (2 << 0),
    IQIC_IMGCH_LEVEL_THR_OVER_2048 = (3 << 0),
};

#define REG_CHAN_BW                  0x0011

enum
{
    ENABLE_CHFILT = (0 << 7),
    DISABLE_CHFILT = (1 << 7),
};

enum
{
    ADC_CIC_DECFACT_20 = (0 << 6),
    ADC_CIC_DECFACT_32 = (1 << 6),
};

#define REG_MDMCFG1                  0x0012

enum
{
    CARRIER_SENSE_GATE__SEARCH_SYNCWORD = (0 << 7),
    CARRIER_SENSE_GATE__DONT_SEARCH_SYNCWORD = (1 << 7),
};

enum
{
    FIFO_EN__THROUGH_SERIAL_PIN = (0 << 6),
    FIFO_EN__THROUGH_FIFO = (1 << 6),
};

enum
{
    MANCHESTER_EN__NRZ = (0 << 5),
    MANCHESTER_EN__MANCHESTER_CODEC = (1 << 5),
};

enum
{
    INVERT_DATA_EN__DIABLE_INVERT_DATA = (0 << 4),
    INVERT_DATA_EN__ENABLE_INVERT_DATA = (1 << 4),
};

enum
{
    COLLISION_DETECT_EN__DISABLE_COL_DETECT = (0 << 3),
    COLLISION_DETECT_EN__ENABLE_COL_DETECT = (1 << 3),
};

enum
{
    DVGA_GAIN__0dB = (0 << 1),
    DVGA_GAIN__3dB = (1 << 1),
    DVGA_GAIN__6dB = (2 << 1),
    DVGA_GAIN__9dB = (3 << 1),
};

enum
{
    SINGLE_ADC_EN__IQ_CHANNELS = (0 << 0),
    SINGLE_ADC_EN__ONLY_I_CHANNEL = (1 << 0),
};

#define REG_MDMCFG0                  0x0013

enum
{
    TRANSPARENT_MODE_EN__DISABLED = (0 << 6),
    TRANSPARENT_MODE_EN__ENABLED = (1 << 6),
};

enum
{
    TRANSPARENT_INTFACT__1x = (0 << 4),
    TRANSPARENT_INTFACT__2x = (1 << 4),
    TRANSPARENT_INTFACT__4x = (2 << 4),
};

enum
{
    DATA_FILTER_EN__DISABLED = (0 << 3),
    DATA_FILTER_EN__ENABLED = (1 << 3),
};

enum
{
    VITERBI_EN__DISABLE_DETECTION = (0 << 2),
    VITERBI_EN__ENABLE_DETECTION = (1 << 2),
};

#define REG_SYMBOL_RATE2             0x0014
#define REG_SYMBOL_RATE1             0x0015
#define REG_SYMBOL_RATE0             0x0016
#define REG_AGC_REF                  0x0017
#define REG_AGC_CS_THR               0x0018
#define REG_AGC_GAIN_ADJUST          0x0019
#define REG_AGC_CFG3                 0x001A

enum
{
    RSSI_STEP_THR__3dB = (0 << 7),
    RSSI_STEP_THR__6dB = (1 << 7),
};

enum
{
    AGC_ASK_BW__NO_FILTER = (0 << 5),
    AGC_ASK_BW__1to64_IIR_FILTER = (1 << 5),
    AGC_ASK_BW__1to256_IIR_FILTER = (2 << 5),
    AGC_ASK_BW__1to1024_IIR_FILTER = (3 << 5),
};

#define REG_AGC_CFG2                 0x001B

enum
{
    START_PREVIOUS_GAIN_EN__RX_STARTS_WITH_MAX_GAIN = (0 << 7),
    START_PREVIOUS_GAIN_EN__RX_STARTS_WITH_PREV_GAIN = (1 << 7),
};

enum
{
    FE_PERFORMANCE_MODE__OPTIMIZED_LINEARITY = (0 << 5),
    FE_PERFORMANCE_MODE__NORMAL_OPERATION = (1 << 5),
    FE_PERFORMANCE_MODE__LOW_POWER = (2 << 5),
};

#define REG_AGC_CFG1                 0x001C

enum
{
    AGC_SYNC_BEHAVIOR__NO_AGC_GAIN_FREEZE = (0 << 5),
    AGC_SYNC_BEHAVIOR__AGC_GAIN_FREEZE = (1 << 5),
    AGC_SYNC_BEHAVIOR__NO_AGC_GAIN_FREEZE_SLOW_MODE = (2 << 5),
    AGC_SYNC_BEHAVIOR__FREEZE_AGC_GAIN_AND_RSSI = (3 << 5),
};

enum
{
    AGC_WIN_SIZE__8_SAMPLES = (0 << 2),
    AGC_WIN_SIZE__16_SAMPLES = (1 << 2),
    AGC_WIN_SIZE__32_SAMPLES = (2 << 2),
    AGC_WIN_SIZE__64_SAMPLES = (3 << 2),
    AGC_WIN_SIZE__128_SAMPLES = (4 << 2),
    AGC_WIN_SIZE__256_SAMPLES = (5 << 2),
};

enum
{
    AGC_SETTLE_WAIT__24_SAMPLES = (0 << 0),
    AGC_SETTLE_WAIT__32_SAMPLES = (1 << 0),
    AGC_SETTLE_WAIT__40_SAMPLES = (2 << 0),
    AGC_SETTLE_WAIT__48_SAMPLES = (3 << 0),
};

#define REG_AGC_CFG0                 0x001D

enum
{
    AGC_HYST_LEVEL__2dB = (0 << 6),
    AGC_HYST_LEVEL__4dB = (1 << 6),
    AGC_HYST_LEVEL__7dB = (2 << 6),
    AGC_HYST_LEVEL__10dB = (3 << 6),
};

enum
{
    AGC_SLEWRATE_LIMIT__60dB = (0 << 4),
    AGC_SLEWRATE_LIMIT__30dB = (1 << 4),
    AGC_SLEWRATE_LIMIT__18dB = (2 << 4),
    AGC_SLEWRATE_LIMIT__9dB = (3 << 4),
};

enum
{
    RSSI_VALID_CNT__2 = (0 << 2),
    RSSI_VALID_CNT__3 = (1 << 2),
    RSSI_VALID_CNT__5 = (2 << 2),
    RSSI_VALID_CNT__9 = (3 << 2),
};

enum
{
    AGC_ASK_DECAY__1to16_IIR_DECAY = (0 << 0),
    AGC_ASK_DECAY__1to32_IIR_DECAY = (1 << 0),
    AGC_ASK_DECAY__1to64_IIR_DECAY = (2 << 0),
    AGC_ASK_DECAY__1to128_IIR_DECAY = (3 << 0),
};

#define REG_FIFO_CFG                 0x001E

enum
{
    CRC_AUTOFLUSH__DISABLED = (0 << 7),
    CRC_AUTOFLUSH__ENABLED = (1 << 7),
};

#define REG_DEV_ADDR                 0x001F
#define REG_SETTLING_CFG             0x0020

enum
{
    FS_AUTOCAL__NEVER = (0 << 3),
    FS_AUTOCAL__WHEN_IDLE_TO_RXTX = (1 << 3),
    FS_AUTOCAL__WHEN_RXTX_TO_IDLE = (2 << 3),
    FS_AUTOCAL__EVERY_4TH_TIME_WHEN_RXTX_TO_IDLE = (3 << 3),
};

enum
{
    LOCK_TIME__50to20us = (0 << 1),
    LOCK_TIME__75to30us = (1 << 1),
    LOCK_TIME__100to40us = (2 << 1),
    LOCK_TIME__150to60us = (3 << 1),
};

enum
{
    FSREG_TIME__30us = (0 << 0),
    FSREG_TIME__60us = (1 << 0),
};

#define REG_FS_CFG                   0x0021

enum
{
    FS_LOCK_EN__DISABLE_OUT_OF_LOCK = (0 << 4),
    FS_LOCK_EN__ENABLE_OUT_OF_LOCK = (1 << 4),
};

enum
{
    FSD_BANDSELECT__820_to_960 = (2 << 0),
    FSD_BANDSELECT__410_to_480 = (4 << 0),
    FSD_BANDSELECT__273_to_320 = (6 << 0),
    FSD_BANDSELECT__205_to_240 = (8 << 0),
    FSD_BANDSELECT__164_to_192 = (10 << 0),
    FSD_BANDSELECT__136_to_160 = (11 << 0),
};

#define REG_WOR_CFG1                 0x0022
#define REG_WOR_CFG0                 0x0023

enum
{
    DIV_256HZ_EN__DISABLE_CLK_DIV = (0 << 5),
    DIV_256HZ_EN__ENABLE_CLK_DIV = (1 << 5),
};

enum
{
    EVENT2_CFG__DISABLED = (0 << 3),
    EVENT2_CFG__15 = (1 << 3),
    EVENT2_CFG__18 = (2 << 3),
    EVENT2_CFG__21 = (3 << 3),
};

enum
{
    RC_MODE__DISABLED = (0 << 1),
    RC_MODE__ENABLED = (3 << 1),
};

enum
{
    RC_PD__RCOSC_RUNNING = (0 << 0),
    RC_PD__RCOSC_POWER_DOWN = (1 << 0),
};

#define REG_WOR_EVENT0_MSB           0x0024
#define REG_WOR_EVENT0_LSB           0x0025
#define REG_PKT_CFG2                 0x0026

enum
{
    CCA_MODE__ALWAYS = (0 << 2),
    CCA_MODE__RSSI_BELOW_THR = (1 << 2),
    CCA_MODE__UNLESS_RXING = (2 << 2),
    CCA_MODE__RSSI_BELOW_THR_UNLESS_NOT_RXING = (3 << 2),
    CCA_MODE__RSSI_BELOW_THR_AND_LBT_REQ_MET = (4 << 2),
};

enum
{
    PKT_FORMAT__NORMAL_FIFO = (0 << 0),
    PKT_FORMAT__SYNC_SERIAL = (1 << 0),
    PKT_FORMAT__RANDOM = (2 << 0),
    PKT_FORMAT__TRANSPARENT_SERIAL = (3 << 0),
};

#define REG_PKT_CFG1                 0x0027

enum
{
    WHITE_DATA__DISABLE = (0 << 6),
    WHITE_DATA__ENABLE = (1 << 6),
};

enum
{
    ADDR_CHECK__NEVER = (0 << 4),
    ADDR_CHECK__NO_BROADCAST = (1 << 4),
    ADDR_CHECK__0x00_BROADCAST = (2 << 4),
    ADDR_CHECK__0x00_0xFF_BROADCAST = (3 << 4),
};

enum
{
    CRC__DISABLED = (0 << 2),
    CRC__ENABLED_CRC16_INIT_ONES = (1 << 2),
    CRC__ENABLED_CRC16_INIT_ZEROS = (2 << 2),
};

enum
{
    BYTE_SWAP_EN__DISABLED = (0 << 1),
    BYTE_SWAP_EN__ENABLED = (1 << 1),
};

enum
{
    APPEND_STATUS__DISABLED = (0 << 0),
    APPEND_STATUS__ENABLED = (1 << 0),
};

#define REG_PKT_CFG0                 0x0028

enum
{
    LENGTH__FIXED = (0 << 5),
    LENGTH__CONFIGURED_BY_FIRST_BYTE_AFTER_SYNCWORD = (1 << 5),
    LENGTH__INFINITE = (2 << 5),
    LENGTH__CONFIGURED_BY_5LSB_OF_FIRST_BYTE_AFTER_SYNCWORD = (3 << 5),
};

enum
{
    UART_MODE_EN__DISABLED = (0 << 1),
    UART_MODE_EN__ENABLED = (1 << 1),
};

enum
{
    UART_SWAP_EN__DISABLED = (0 << 0),
    UART_SWAP_EN__ENABLED = (1 << 0),
};

#define REG_RFEND_CFG1               0x0029

enum
{
    RXOFF_MODE__IDLE = (0 << 4),
    RXOFF_MODE__FSTXON = (1 << 4),
    RXOFF_MODE__TX = (2 << 4),
    RXOFF_MODE__RX = (3 << 4),
};

enum
{
    RX_TIME__DISABLE_TIMEOUT = (7 << 1), // 111b
};

enum
{
    RX_TIME_QUAL__CONTINUE_RX_ON_TIMEOUT_IF_SYNC_FOUND = (0 << 0),
    RX_TIME_QUAL__CONTINUE_RX_ON_TIMEOUT_IF_PKT_REACHED = (1 << 0),
};

#define REG_RFEND_CFG0               0x002A

enum
{
    CAL_END_WAKE_UP_EN__DISABLED = (0 << 6),
    CAL_END_WAKE_UP_EN__ENABLED = (1 << 6),
};

enum
{
    TXOFF_MODE__IDLE = (0 << 4),
    TXOFF_MODE__FSTXON = (1 << 4),
    TXOFF_MODE__TX = (2 << 4),
    TXOFF_MODE__RX = (3 << 4),
};

enum
{
    TERM_ON_BAD_PACKET_EN__DISABLED = (0 << 3),
    TERM_ON_BAD_PACKET_EN__ENABLED = (1 << 3),
};

enum
{
    ANT_DIV_RX_TERM_CFG__DISABLED = (0 << 0),
    ANT_DIV_RX_TERM_CFG__TERM_BASED_ON_CS = (1 << 0),
    ANT_DIV_RX_TERM_CFG__SINGLE_SWITCH_ANT_DIV_ON_CS = (2 << 0),
    ANT_DIV_RX_TERM_CFG__CONT_SWITCH_ANT_DIV_ON_CS = (3 << 0),
    ANT_DIV_RX_TERM_CFG__TERM_BASED_ON_PQT = (4 << 0),
    ANT_DIV_RX_TERM_CFG__SINGLE_SWITCH_ANT_DIV_ON_PQT = (5 << 0),
    ANT_DIV_RX_TERM_CFG__CONT_SWITCH_ANT_DIV_ON_PQT = (6 << 0),
};

#define REG_PA_CFG2                  0x002B
#define REG_PA_CFG1                  0x002C

enum
{
    RAMP_SHAPE__3to8SYM_RAMP_TIME_AND_1to32SYM_LENGTH = (0 << 0),
    RAMP_SHAPE__3to2SYM_RAMP_TIME_AND_1to16SYM_LENGTH = (1 << 0),
    RAMP_SHAPE__3SYM_RAMP_TIME_AND_1to8SYM_LENGTH = (2 << 0),
    RAMP_SHAPE__6SYM_RAMP_TIME_AND_1to4SYM_LENGTH = (3 << 0),
};

#define REG_PA_CFG0                  0x002D

enum
{
    UPSAMPLER_P__1 = (0 << 0),
    UPSAMPLER_P__2 = (1 << 0),
    UPSAMPLER_P__4 = (2 << 0),
    UPSAMPLER_P__8 = (3 << 0),
    UPSAMPLER_P__16 = (4 << 0),
    UPSAMPLER_P__32 = (5 << 0),
    UPSAMPLER_P__64 = (6 << 0),
};

#define REG_PKT_LEN                  0x002E

/* Extended Configuration Registers */
#define REG_IF_MIX_CFG               0x2f00
#define REG_FREQOFF_CFG              0x2f01

///@note The datasheet (SWRU295B) has an error on page 87.

enum
{
    FOC_EN__DISABLED = (0 << 5),
    FOC_EN__ENABLED = (1 << 5),
};

enum
{
    FOC_CFG__AFTER_CHFILT = (0 << 3),
    FOC_CFG__FS_LGF_1to128 = (1 << 3),
    FOC_CFG__FS_LGF_1to256 = (2 << 3),
    FOC_CFG__FS_LGF_1to512 = (3 << 3),
};

enum
{
    FOC_LIMIT__RXFILTBW_TO_4 = (0 << 2),
    FOC_LIMIT__RXFILTBW_TO_8 = (1 << 2),
};

enum
{
    FOC_KI_FACTOR__FOC_DISABLED = (0 << 0),
    FOC_KI_FACTOR__FOC_DURING_RX_WITH_LGF_1to32 = (1 << 0),
    FOC_KI_FACTOR__FOC_DURING_RX_WITH_LGF_1to64 = (2 << 0),
    FOC_KI_FACTOR__FOC_DURING_RX_WITH_LGF_1to128 = (3 << 0),
    FOC_KI_FACTOR__FOC_WITH_LGF_1to128 = (4 << 0),
    FOC_KI_FACTOR__FOC_WITH_LGF_1to256 = (5 << 0),
    FOC_KI_FACTOR__FOC_WITH_LGF_1to512 = (6 << 0),
    FOC_KI_FACTOR__FOC_WITH_LGF_1to1024 = (7 << 0),
};

#define REG_TOC_CFG                  0x2f02

enum
{
    TOC_LIMIT__LESEER_THAN_2000PPM = (0 << 6),
    TOC_LIMIT__LESEER_THAN_20000PPM = (1 << 6),
    TOC_LIMIT__LESEER_THAN_120000PPM = (3 << 6),
};

enum
{
    TOC_PRE_SYNC_BLOCKLEN__8SYMBOLS = (0 << 3),
    TOC_PRE_SYNC_BLOCKLEN__16SYMBOLS = (1 << 3),
    TOC_PRE_SYNC_BLOCKLEN__32SYMBOLS = (2 << 3),
    TOC_PRE_SYNC_BLOCKLEN__64SYMBOLS = (3 << 3),
    TOC_PRE_SYNC_BLOCKLEN__128SYMBOLS = (4 << 3),
    TOC_PRE_SYNC_BLOCKLEN__256SYMBOLS = (5 << 3),
};

enum
{
    PROP_SCALE_FACTOR__8_16 = (0 << 3),
    PROP_SCALE_FACTOR__6_16 = (1 << 3),
    PROP_SCALE_FACTOR__2_16 = (2 << 3),
    PROP_SCALE_FACTOR__1_16 = (3 << 3),
    PROP_SCALE_FACTOR__1_16_AFTER_SYNC_FOUND = (4 << 3),
};

enum
{
    TOC_POST_SYNC_BLOCKLEN__8SYMBOLS = (0 << 0),
    TOC_POST_SYNC_BLOCKLEN__16SYMBOLS = (1 << 0),
    TOC_POST_SYNC_BLOCKLEN__32SYMBOLS = (2 << 0),
    TOC_POST_SYNC_BLOCKLEN__64SYMBOLS = (3 << 0),
    TOC_POST_SYNC_BLOCKLEN__128SYMBOLS = (4 << 0),
    TOC_POST_SYNC_BLOCKLEN__256SYMBOLS = (5 << 0),
};

enum
{
    INTG_SCALE_FACTOR__8_32 = (0 << 0),
    INTG_SCALE_FACTOR__6_32 = (1 << 0),
    INTG_SCALE_FACTOR__2_32 = (2 << 0),
    INTG_SCALE_FACTOR__1_32 = (3 << 0),
    INTG_SCALE_FACTOR__1_32_AFTER_SYNC_FOUND = (4 << 0),
};

#define REG_MARC_SPARE               0x2f03
#define REG_ECG_CFG                  0x2f04
#define REG_SOFT_TX_DATA_CFG         0x2f05

enum
{
    SOFT_TX_DATA_EN__DISABLED = (0 << 0),
    SOFT_TX_DATA_EN__ENABLED = (1 << 0),
};

#define REG_EXT_CTRL                 0x2f06
#define REG_RCCAL_FINE               0x2f07
#define REG_RCCAL_COARSE             0x2f08
#define REG_RCCAL_OFFSET             0x2f09
#define REG_FREQOFF1                 0x2f0A
#define REG_FREQOFF0                 0x2f0B
#define REG_FREQ2                    0x2f0C
#define REG_FREQ1                    0x2f0D
#define REG_FREQ0                    0x2f0E
#define REG_IF_ADC2                  0x2f0F
#define REG_IF_ADC1                  0x2f10
#define REG_IF_ADC0                  0x2f11
#define REG_FS_DIG1                  0x2f12
#define REG_FS_DIG0                  0x2f13

enum
{
    RX_LPF_BW__101_6_kHz = (0 << 2),
    RX_LPF_BW__131_7_kHz = (1 << 2),
    RX_LPF_BW__150_0_kHz = (2 << 2),
    RX_LPF_BW__170_8_kHz = (3 << 2),
};

enum
{
    TX_LPF_BW__101_6_kHz = (0 << 0),
    TX_LPF_BW__131_7_kHz = (1 << 0),
    TX_LPF_BW__150_0_kHz = (2 << 0),
    TX_LPF_BW__170_8_kHz = (3 << 0),
};

#define REG_FS_CAL3                  0x2f14
#define REG_FS_CAL2                  0x2f15
#define REG_FS_CAL1                  0x2f16
#define REG_FS_CAL0                  0x2f17

enum
{
    LOCK_CFG__OUT_OF_LOCK_DETECTOR_TIME__OVER_512_CYCLES = (0 << 2),
    LOCK_CFG__OUT_OF_LOCK_DETECTOR_TIME__OVER_1024_CYCLES = (1 << 2),
    LOCK_CFG__OUT_OF_LOCK_DETECTOR_TIME__OVER_256_CYCLES = (2 << 2),
    LOCK_CFG__OUT_OF_LOCK_DETECTOR_TIME__INFINITE = (3 << 2),
};

#define REG_FS_CHP                   0x2f18
#define REG_FS_DIVTWO                0x2f19
#define REG_FS_DSM1                  0x2f1A
#define REG_FS_DSM0                  0x2f1B
#define REG_FS_DVC1                  0x2f1C
#define REG_FS_DVC0                  0x2f1D
#define REG_FS_LBI                   0x2f1E
#define REG_FS_PFD                   0x2f1F
#define REG_FS_PRE                   0x2f20
#define REG_FS_REG_DIV_CML           0x2f21
#define REG_FS_SPARE                 0x2f22
#define REG_FS_VCO4                  0x2f23
#define REG_FS_VCO3                  0x2f24
#define REG_FS_VCO2                  0x2f25
#define REG_FS_VCO1                  0x2f26
#define REG_FS_VCO0                  0x2f27
#define REG_GBIAS6                   0x2f28
#define REG_GBIAS5                   0x2f29
#define REG_GBIAS4                   0x2f2A
#define REG_GBIAS3                   0x2f2B
#define REG_GBIAS2                   0x2f2C
#define REG_GBIAS1                   0x2f2D
#define REG_GBIAS0                   0x2f2E
#define REG_IFAMP                    0x2f2F
#define REG_LNA                      0x2f30
#define REG_RXMIX                    0x2f31
#define REG_XOSC5                    0x2f32
#define REG_XOSC4                    0x2f33
#define REG_XOSC3                    0x2f34
#define REG_XOSC2                    0x2f35
#define REG_XOSC1                    0x2f36
#define REG_XOSC0                    0x2f37
#define REG_ANALOG_SPARE             0x2f38
#define REG_PA_CFG3                  0x2f39
#define REG_IRQ0M                    0x2f3F
#define REG_IRQ0F                    0x2f40

/* Status Registers */
#define REG_WOR_TIME1                0x2f64
#define REG_WOR_TIME0                0x2f65
#define REG_WOR_CAPTURE1             0x2f66
#define REG_WOR_CAPTURE0             0x2f67
#define REG_BIST                     0x2f68
#define REG_DCFILTOFFSET_I1          0x2f69
#define REG_DCFILTOFFSET_I0          0x2f6A
#define REG_DCFILTOFFSET_Q1          0x2f6B
#define REG_DCFILTOFFSET_Q0          0x2f6C
#define REG_IQIE_I1                  0x2f6D
#define REG_IQIE_I0                  0x2f6E
#define REG_IQIE_Q1                  0x2f6F
#define REG_IQIE_Q0                  0x2f70
#define REG_RSSI1                    0x2f71
#define REG_RSSI0                    0x2f72
#define REG_MARCSTATE                0x2f73
#define REG_LQI_VAL                  0x2f74
#define REG_PQT_SYNC_ERR             0x2f75
#define REG_DEM_STATUS               0x2f76
#define REG_FREQOFF_EST1             0x2f77
#define REG_FREQOFF_EST0             0x2f78
#define REG_AGC_GAIN3                0x2f79
#define REG_AGC_GAIN2                0x2f7A
#define REG_AGC_GAIN1                0x2f7B
#define REG_AGC_GAIN0                0x2f7C
#define REG_SOFT_RX_DATA_OUT         0x2f7D
#define REG_SOFT_TX_DATA_IN          0x2f7E
#define REG_ASK_SOFT_RX_DATA         0x2f7F
#define REG_RNDGEN                   0x2f80
#define REG_MAGN2                    0x2f81
#define REG_MAGN1                    0x2f82
#define REG_MAGN0                    0x2f83
#define REG_ANG1                     0x2f84
#define REG_ANG0                     0x2f85
#define REG_CHFILT_I2                0x2f86
#define REG_CHFILT_I1                0x2f87
#define REG_CHFILT_I0                0x2f88
#define REG_CHFILT_Q2                0x2f89
#define REG_CHFILT_Q1                0x2f8A
#define REG_CHFILT_Q0                0x2f8B
#define REG_GPIO_STATUS              0x2f8C
#define REG_FSCAL_CTRL               0x2f8D
#define REG_PHASE_ADJUST             0x2f8E
#define REG_PARTNUMBER               0x2f8F
#define REG_PARTVERSION              0x2f90
#define REG_SERIAL_STATUS            0x2f91
#define REG_RX_STATUS                0x2f92

enum
{
    RX_STATUS__SYNC_FOUND = (1 << 7),
    RX_STATUS__RXFIFO_FULL = (1 << 6),
    RX_STATUS__RXFIFO_OVER_THR = (1 << 5),
    RX_STATUS__RXFIFO_EMPTY = (1 << 4),
    RX_STATUS__RXFIFO_OVERFLOW = (1 << 3),
    RX_STATUS__RXFIFO_UNDERFLOW = (1 << 2),
    RX_STATUS__PQT_REACHED = (1 << 1),
    RX_STATUS__PQT_VALID = (1 << 0),
};

#define REG_TX_STATUS                0x2f93
#define REG_MARC_STATUS1             0x2f94
#define REG_MARC_STATUS0             0x2f95
#define REG_PA_IFAMP_TEST            0x2f96
#define REG_FSRF_TEST                0x2f97
#define REG_PRE_TEST                 0x2f98
#define REG_PRE_OVR                  0x2f99
#define REG_ADC_TEST                 0x2f9A
#define REG_DVC_TEST                 0x2f9B
#define REG_ATEST                    0x2f9C
#define REG_ATEST_LVDS               0x2f9D
#define REG_ATEST_MODE               0x2f9E
#define REG_XOSC_TEST1               0x2f9F
#define REG_XOSC_TEST0               0x2fA0

#define REG_RXFIRST                  0x2fD2
#define REG_TXFIRST                  0x2fD3
#define REG_RXLAST                   0x2fD4
#define REG_TXLAST                   0x2fD5
#define REG_NUM_TXBYTES              0x2fD6  /* Number of bytes in TXFIFO */
#define REG_NUM_RXBYTES              0x2fD7  /* Number of bytes in RXFIFO */
#define REG_FIFO_NUM_TXBYTES         0x2fD8
#define REG_FIFO_NUM_RXBYTES         0x2fD9

/* DATA FIFO Access */
#define CC1120_DIRECT_FIFO              0x3e
#define CC1120_SINGLE_TXFIFO            0x3F      /*  TXFIFO  - Single access to Transmit FIFO */
#define CC1120_BURST_TXFIFO             0x7F      /*  TXFIFO  - Burst access to Transmit FIFO  */
#define CC1120_SINGLE_RXFIFO            0xBF      /*  RXFIFO  - Single access to Receive FIFO  */
#define CC1120_BURST_RXFIFO             0xFF      /*  RXFIFO  - Burst access to Receive FIFO  */

#define CC1120_LQI_CRC_OK_BM            0x80
#define CC1120_LQI_EST_BM               0x7F

/* Command strobe registers */
#define CC1120_SRES                     0x30      /*  SRES    - Reset chip. */
#define CC1120_SFSTXON                  0x31      /*  SFSTXON - Enable and calibrate frequency synthesizer. */
#define CC1120_SXOFF                    0x32      /*  SXOFF   - Turn off crystal oscillator. */
#define CC1120_SCAL                     0x33      /*  SCAL    - Calibrate frequency synthesizer and turn it off. */
#define CC1120_SRX                      0x34      /*  SRX     - Enable RX. Perform calibration if enabled. */
#define CC1120_STX                      0x35      /*  STX     - Enable TX. If in RX state, only enable TX if CCA passes. */
#define CC1120_SIDLE                    0x36      /*  SIDLE   - Exit RX / TX, turn off frequency synthesizer. */
#define CC1120_AFC                      0x37      /*  AFC     - Automatic Frequency Correction */
#define CC1120_SWOR                     0x38      /*  SWOR    - Start automatic RX polling sequence (Wake-on-Radio) */
#define CC1120_SPWD                     0x39      /*  SPWD    - Enter power down mode when CSn goes high. */
#define CC1120_SFRX                     0x3A      /*  SFRX    - Flush the RX FIFO buffer. */
#define CC1120_SFTX                     0x3B      /*  SFTX    - Flush the TX FIFO buffer. */
#define CC1120_SWORRST                  0x3C      /*  SWORRST - Reset real time clock. */
#define CC1120_SNOP                     0x3D      /*  SNOP    - No operation. Returns status byte. */

/* Chip states returned in status byte */
enum cc1120_state
{
    CC1120_STATE_IDLE             = 0x00,
    CC1120_STATE_RX               = 0x10,
    CC1120_STATE_TX               = 0x20,
    CC1120_STATE_FSTXON           = 0x30,
    CC1120_STATE_CALIBRATE        = 0x40,
    CC1120_STATE_SETTLING         = 0x50,
    CC1120_STATE_RXFIFO_ERROR     = 0x60,
    CC1120_STATE_TXFIFO_ERROR     = 0x70,
};

#define CC1120_REG_ACCESS_READ          0x80
#define CC1120_REG_ACCESS_WRITE         0x00
#define CC1120_REG_ACCESS_BURST         0x40
#define CC1120_REG_ACCESS_SINGLE        0x00

struct cc1120_register_setting
{
    UINT16 reg;
    UINT8 value;
};

enum cc1120_mode
{
    CC1120_MODE_ERROR = 0,
    CC1120_MODE_915MHZ_2GFSK_50KBPS,
    CC1120_MODE_CC1190_434MHZ_2GFSK_50KBPS,
    CC1120_MODE_CC1190_868MHZ_2GFSK_38KBPS,
    CC1120_MODE_CC1190_868MHZ_2GFSK_50KBPS,
    CC1120_MODE_CC1190_868MHZ_4GFSK_200KBPS,
    CC1120_MODE_CC1190_869MHZ_2GFSK_38KBPS,
    CC1120_MODE_CC1190_869MHZ_4GFSK_200KBPS,
    CC1120_MODE_CC1190_915MHZ_2GFSK_38KBPS,
    CC1120_MODE_CC1190_915MHZ_2GFSK_50KBPS,
    CC1120_MODE_CC1190_915MHZ_4GFSK_200KBPS,
    CC1120_MODE_CC1190_950MHZ_2GFSK_50KBPS,
    CC1120_MODE_CC1190_950MHZ_4GFSK_200KBPS,
};

#endif /* CC1120_CONST_H_ */
