// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief TI's CC112x Low-power High Performance Sub-1GHz RF Transceiver Driver
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 2. 17.
 */

#include "cc1120.h"

#ifdef CC1120_M

#include "cc1120-const.h"
#include "cc1120-access.h"
#include "cc1120-interface.h"
#include <stdlib.h>
#include <string.h>
#include "arch.h"
#include "critical_section.h"
#include "platform.h"
#include "wpan-dev.h"
#include "ieee-802-15-4.h"

extern WPAN_DEV wpan_dev[WPAN_DEV_CNT];

static void reset_on_error(UINT8 dev_id)
{
    cc1120_interface_init(dev_id);
    cc1120_init(dev_id);
    cc1120_setup_mode(dev_id, wpan_dev[dev_id].c.cc1120.mode);
}

static void cc1120_wakeup(UINT8 dev_id)
{
    //TODO Wake up and enter the idle state.
}

static void cc1120_sleep(UINT8 dev_id)
{
    //TODO Sleep.
}

ERROR_T cc1120_set_tx_level(UINT8 dev_id, UINT8 l)
{
    /*
     * TODO Tx power control.
     * It seems to be related with PA_CFG2.PA_POWER_RAMP.
     */
    return ERROR_NOT_SUPPORTED;
}

static void cc1120_flush_rxbuf(UINT8 dev_id)
{
    if (cc1120_command_strobe(dev_id, CC1120_SIDLE, NULL) != ERROR_SUCCESS ||
        cc1120_command_strobe(dev_id, CC1120_SFRX, NULL) != ERROR_SUCCESS ||
        cc1120_command_strobe(dev_id, CC1120_SRX, NULL) != ERROR_SUCCESS)
        reset_on_error(dev_id);
}

static void cc1120_flush_txbuf(UINT8 dev_id)
{
    if (cc1120_command_strobe(dev_id, CC1120_SIDLE, NULL) != ERROR_SUCCESS ||
        cc1120_command_strobe(dev_id, CC1120_SFTX, NULL) != ERROR_SUCCESS ||
        cc1120_command_strobe(dev_id, CC1120_SRX, NULL) != ERROR_SUCCESS)
        reset_on_error(dev_id);
}

static ERROR_T cc1120_tx_on_cca(UINT8 dev_id, const UINT8 *buf)
{
    UINT16 foo;
    BOOL pin_is_set;
    
    if (cc1120_set_register(dev_id, REG_TXFIRST, 0) != ERROR_SUCCESS)
        goto error;

    // Wait for packet reception done.
    foo = 0;
    while (foo < 20000)
    {
        foo++;
        cc1120_get_pin_state(dev_id, CC1120_PIN_GPIO0, &pin_is_set);
        if (!pin_is_set)
            break;
    }

    if (foo == 20000)
    {
        return ERROR_FAIL;
    }

    cc1120_enable_intr(dev_id, FALSE);
    
    // Start Tx.
    cc1120_prepare_tx(dev_id);
    if (cc1120_command_strobe(dev_id, CC1120_STX, NULL) != ERROR_SUCCESS)
        goto error;

    foo = 0;
    while (foo < 20000)
    {
        foo++;

        if (cc1120_get_intr(dev_id))
        {
            cc1120_clear_intr(dev_id);
            break;
        }        
    }

    cc1120_enable_intr(dev_id, TRUE);

    cc1120_prepare_rx(dev_id);
    if (cc1120_command_strobe(dev_id, CC1120_SRX, NULL) != ERROR_SUCCESS)
        goto error;

    if (foo < 20000)
    {
        return ERROR_SUCCESS;
    }
    else
    {
        return ERROR_TXFAIL_CH_BUSY;
    }

error:
    reset_on_error(dev_id);
    return ERROR_FAIL;
}

static void cc1120_ack(UINT8 dev_id, UINT8 seq)
{
    UINT8 ack_frame[] = { 0x05, 0x02, 0x00, 0x00 };

    ack_frame[3] = seq;
    
    cc1120_flush_txbuf(dev_id);

    // IDLE to send Ack without CCA.
    
    if (cc1120_txfifo_push(dev_id, ack_frame) != ERROR_SUCCESS ||
        cc1120_command_strobe(dev_id, CC1120_SIDLE, NULL) != ERROR_SUCCESS)
    {
        reset_on_error(dev_id);
        return;
    }
    
    cc1120_set_register(dev_id, REG_PKT_CFG2, 0x00); //Turn off Tx on CCA.
    cc1120_tx_on_cca(dev_id, ack_frame);
    cc1120_set_register(dev_id, REG_PKT_CFG2, 0x04); //Turn on Tx on CCA again.
}

static ERROR_T cc1120_read_frame(UINT8 dev_id, struct wpan_rx_frame *f)
{
    UINT16 fcf, pan, id;
    ERROR_T err;
    
    if (cc1120_rxfifo_pop_single(dev_id, &f->len) != ERROR_SUCCESS)
        goto error;

    if (f->len > 127)
    {
        return ERROR_INVALID_FRAME;
    }

    if (f->len < 14)
        err = cc1120_rxfifo_pop_burst(dev_id, f->buf, f->len);
    else
        err = cc1120_rxfifo_pop_burst(dev_id, f->buf, 14);
    if (err != ERROR_SUCCESS)
        goto error;

    fcf = f->buf[0] + (f->buf[1] << 8);
    pan = f->buf[3] + (f->buf[4] << 8);
    id = f->buf[5] + (f->buf[6] << 8);

    if (FRAME_REQUESTS_ACK(fcf) &&
        (pan == 0xFFFF || pan == wpan_dev[dev_id].pan_id) &&
        ((FRAME_CONTAINS_SHORTDST(fcf) && id == wpan_dev[dev_id].short_id) ||
         (f->len >= 14 &&
          FRAME_CONTAINS_LONGDST(fcf) &&
          f->buf[5] == wpan_dev[dev_id].eui64[7] &&
          f->buf[6] == wpan_dev[dev_id].eui64[6] &&
          f->buf[7] == wpan_dev[dev_id].eui64[5] &&
          f->buf[8] == wpan_dev[dev_id].eui64[4] &&
          f->buf[9] == wpan_dev[dev_id].eui64[3] &&
          f->buf[10] == wpan_dev[dev_id].eui64[2] &&
          f->buf[11] == wpan_dev[dev_id].eui64[1] &&
          f->buf[12] == wpan_dev[dev_id].eui64[0])))
    {
        cc1120_ack(dev_id, f->buf[2]);
    }

    if (f->len > 14)
        err = cc1120_rxfifo_pop_burst(dev_id, &f->buf[14], f->len - 14);

    if (err != ERROR_SUCCESS)
        goto error;

    if (cc1120_rxfifo_pop_single(dev_id, (UINT8 *) &f->rssi) != ERROR_SUCCESS ||
        cc1120_rxfifo_pop_single(dev_id, (UINT8 *) &f->lqi) != ERROR_SUCCESS)
        goto error;

    if (f->lqi & CC1120_LQI_CRC_OK_BM)
    {
        INT8 gain_adjust;
        
        f->lqi &= CC1120_LQI_EST_BM;
        f->crc_ok = TRUE;
        if (cc1120_get_register(dev_id,
                                REG_AGC_GAIN_ADJUST,
                                (UINT8 *) &gain_adjust) != ERROR_SUCCESS)
        {
            goto error;
        }
        f->rssi += gain_adjust - 102;
    }
    else
    {
        f->crc_ok = FALSE;
        return ERROR_CRC_ERROR;
    }

    f->secured = FALSE;

    // Flushing RxFifo is the most safe method from the chip failure.
    cc1120_flush_rxbuf(dev_id);
    return ERROR_SUCCESS;

error:
    reset_on_error(dev_id);
    return ERROR_FAIL;
}

static BOOL cc1120_rxbuf_is_empty(UINT8 dev_id)
{
    UINT8 status;
    BOOL pin_is_set;
    
    if (cc1120_command_strobe(dev_id, CC1120_SNOP, &status) != ERROR_SUCCESS)
        goto error;
    
    status &= 0xf0;

    if (status == CC1120_STATE_RXFIFO_ERROR)
    {
        cc1120_flush_rxbuf(dev_id);
        return TRUE;
    }
    else if (status == CC1120_STATE_IDLE)
    {
        if (cc1120_command_strobe(dev_id, CC1120_SRX, NULL) != ERROR_SUCCESS)
            goto error;
    }

    cc1120_get_pin_state(dev_id, CC1120_PIN_GPIO2, &pin_is_set);

    return (!pin_is_set);

error:
    reset_on_error(dev_id);
    return TRUE;
}

static void cc1120_write_frame(UINT8 dev_id, const UINT8 *buf)
{
    cc1120_flush_txbuf(dev_id);
    if (cc1120_txfifo_push(dev_id, buf) != ERROR_SUCCESS)
        reset_on_error(dev_id);
}

static BOOL cc1120_cca(UINT8 dev_id)
{
    BOOL pin_is_set;
    cc1120_get_pin_state(dev_id, CC1120_PIN_GPIO2, &pin_is_set);
    
    // The GPIO2 pin for CCA does not work. I don't know why. (jsjeong)
    pin_is_set = TRUE;

    return pin_is_set;
}

static ERROR_T cc1120_wait_for_ack(UINT8 dev_id, UINT8 seq)
{
    UINT8 i;
    ERROR_T err = ERROR_FAIL;

    cc1120_enable_intr(dev_id, FALSE);

    cc1120_flush_rxbuf(dev_id);

    //nos_delay_us(wpan_dev[dev_id].symbol_duration * 54);
    nos_delay_us(2000); // to compensate SW ack delay
    if (!cc1120_rxbuf_is_empty(dev_id))
    {
        // When in the critical section currently.
        if (cc1120_read_frame(dev_id, &wpan_dev[dev_id].c.cc1120.tmp_rcvd)
            == ERROR_SUCCESS)
        {
            // Check whether it's an ACK for me or not.
            if (FRAME_IS_ACK(wpan_dev[dev_id].c.cc1120.tmp_rcvd.buf[0] +
                             (wpan_dev[dev_id].c.cc1120.tmp_rcvd.buf[1] << 8)) &&
                wpan_dev[dev_id].c.cc1120.tmp_rcvd.buf[2] == seq)
            {
                err = ERROR_SUCCESS; // valid Ack reception
            }
        }
    }

    cc1120_flush_rxbuf(dev_id);
    cc1120_clear_intr(dev_id);
    cc1120_enable_intr(dev_id, TRUE);

    return err;
}

static const struct wpan_dev_cmdset cc1120_cmd =
{
    NULL,
    cc1120_read_frame,
    cc1120_write_frame,
    cc1120_rxbuf_is_empty,
    cc1120_flush_rxbuf,
    cc1120_sleep,
    cc1120_wakeup,
    cc1120_tx_on_cca,
    cc1120_wait_for_ack,    
    cc1120_cca,
    NULL,
    NULL,
    NULL,
};

void cc1120_init(UINT8 dev_id)
{
    wpan_dev[dev_id].hw_autotx = FALSE;
    wpan_dev[dev_id].sec_support = FALSE;
    wpan_dev[dev_id].addrdec_support = FALSE;
    wpan_dev[dev_id].cmd = &cc1120_cmd;
}

void cc1120_intr_handler(UINT8 dev_id)
{
    if (wpan_dev[dev_id].notify)
        wpan_dev[dev_id].notify(dev_id, 0, NULL);

    cc1120_prepare_rx(dev_id);
    
    if (cc1120_command_strobe(dev_id, CC1120_SRX, NULL) != ERROR_SUCCESS)
        reset_on_error(dev_id);
}

#endif //CC1120_M
