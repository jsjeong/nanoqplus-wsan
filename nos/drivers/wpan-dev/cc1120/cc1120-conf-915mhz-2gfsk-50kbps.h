// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CC1120 Configuration for 915MHz, 2GFSK, 50Kbps
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 9. 2.
 */

#ifndef CC1120_CONF_915MHZ_2GFSK_50KBPS_H
#define CC1120_CONF_915MHZ_2GFSK_50KBPS_H

#include "cc1120-const.h"

#define CC1120_CONF_915MHZ_2GFSK_50KBPS         \
    {                                           \
        {REG_SYNC_CFG1,         0x08},          \
        {REG_DEVIATION_M,       0x48},          \
        {REG_MODCFG_DEV_E,      0x0D},          \
        {REG_DCFILT_CFG,        0x1C},          \
        {REG_IQIC,              0x00},          \
        {REG_CHAN_BW,           0x02},          \
        {REG_MDMCFG0,           0x05},          \
        {REG_SYMBOL_RATE2,      0x93},          \
        {REG_AGC_CS_THR,        0x19},          \
        {REG_AGC_CFG1,          0xA9},          \
        {REG_AGC_CFG0,          0xCF},          \
        {REG_FIFO_CFG,          0x00},          \
        {REG_SETTLING_CFG,      0x03},          \
        {REG_FS_CFG,            0x12},          \
        {REG_PKT_CFG0,          0x20},          \
        {REG_PA_CFG0,           0x7B},          \
        {REG_PKT_LEN,           0xFF},          \
        {REG_IF_MIX_CFG,        0x00},          \
        {REG_FREQOFF_CFG,       0x22},          \
        {REG_FREQ2,             0x72},          \
        {REG_FREQ1,             0x60},          \
        {REG_FS_DIG1,           0x00},          \
        {REG_FS_DIG0,           0x5F},          \
        {REG_FS_CAL1,           0x40},          \
        {REG_FS_CAL0,           0x0E},          \
        {REG_FS_DIVTWO,         0x03},          \
        {REG_FS_DSM0,           0x33},          \
        {REG_FS_DVC0,           0x17},          \
        {REG_FS_PFD,            0x50},          \
        {REG_FS_PRE,            0x6E},          \
        {REG_FS_REG_DIV_CML,    0x14},          \
        {REG_FS_SPARE,          0xAC},          \
        {REG_FS_VCO0,           0xB4},          \
        {REG_XOSC5,             0x0E},          \
        {REG_XOSC1,             0x03},          \
        {0xFFFF,                0x00}}

#endif //REG_CONF_1190_2GFSK_50KBPS_H
