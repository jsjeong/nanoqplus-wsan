// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * TI's CC112x Low-power High Performance Sub-1GHz RF Transceiver Driver
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 7. 5.
 */

#include "cc1120.h"
#ifdef CC1120_M
#include "cc1120-const.h"
#include "cc1120-access.h"
#include "cc1120-interface.h"
#include "cc1120-conf-915mhz-2gfsk-50kbps.h"
#include "cc1120-conf-1190-434mhz-2gfsk-50kbps.h"
#include "cc1120-conf-1190-868mhz-2gfsk-38kbps.h"
#include "cc1120-conf-1190-868mhz-2gfsk-50kbps.h"
#include "cc1120-conf-1190-868mhz-4gfsk-200kbps.h"
#include "cc1120-conf-1190-869.525024mhz-2gfsk-38kbps.h"
#include "cc1120-conf-1190-869.525024mhz-4gfsk-200kbps.h"
#include "cc1120-conf-1190-915mhz-2gfsk-38kbps.h"
#include "cc1120-conf-1190-915mhz-2gfsk-50kbps.h"
#include "cc1120-conf-1190-915mhz-4gfsk-200kbps.h"
#include "cc1120-conf-1190-950mhz-2gfsk-50kbps.h"
#include "cc1120-conf-1190-950mhz-4gfsk-200kbps.h"
#include "wpan-dev.h"

extern WPAN_DEV wpan_dev[];

static void config(UINT8 dev_id, struct cc1120_register_setting *conf)
{
    while (conf->reg != 0xFFFF)
    {
        cc1120_set_register(dev_id, conf->reg, conf->value);
        conf++;
    }
}

void cc1120_setup_mode(UINT8 dev_id, enum cc1120_mode mode)
{
    cc1120_reset(dev_id);

    cc1120_set_register(dev_id, CC1120_REG_IOCFG3, GPIO3_CFG_CCA_STATUS);
    cc1120_set_register(dev_id, CC1120_REG_IOCFG2, GPIOx_CFG_RXFIFO_THR);
    cc1120_set_register(dev_id, CC1120_REG_IOCFG0, GPIOx_CFG_PKT_SYNC_RXTX);

    cc1120_set_register(dev_id, REG_SYNC3, 0x93);
    cc1120_set_register(dev_id, REG_SYNC2, 0x0B);
    cc1120_set_register(dev_id, REG_SYNC1, 0x51);
    cc1120_set_register(dev_id, REG_SYNC0, 0xDE);

    if (mode == CC1120_MODE_ERROR)
    {
        return;
    }
#ifdef CC1120_MODE_915MHZ_2GFSK_50KBPS_ENABLED
    else if (mode == CC1120_MODE_915MHZ_2GFSK_50KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_915MHZ_2GFSK_50KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 20;
    }    
#endif
#ifdef CC1120_MODE_CC1190_434MHZ_2GFSK_50KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_434MHZ_2GFSK_50KBPS)
    {
        struct cc1120_register_setting conf[] = CC1120_CONF_CC1190_434MHZ_2GFSK_50KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 20;
    }
#endif
#ifdef CC1120_MODE_CC1190_868MHZ_2GFSK_38KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_868MHZ_2GFSK_38KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_868MHZ_2GFSK_38KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 26;
    }
#endif
#ifdef CC1120_MODE_CC1190_868MHZ_2GFSK_50KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_868MHZ_2GFSK_50KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_868MHZ_2GFSK_50KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 20;
    }
#endif
#ifdef CC1120_MODE_CC1190_868MHZ_4GFSK_200KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_868MHZ_4GFSK_200KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_868MHZ_4GFSK_200KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 10;
    }
#endif
#ifdef CC1120_MODE_CC1190_869MHZ_2GFSK_38KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_869MHZ_2GFSK_38KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_869MHZ_2GFSK_38KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 26;
    }
#endif
#ifdef CC1120_MODE_CC1190_869MHZ_4GFSK_200KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_869MHZ_4GFSK_200KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_869MHZ_4GFSK_200KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 10;
    }
#endif
#ifdef CC1120_MODE_CC1190_915MHZ_2GFSK_38KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_915MHZ_2GFSK_38KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_915MHZ_2GFSK_38KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 26;
    }
#endif
#ifdef CC1120_MODE_CC1190_915MHZ_2GFSK_50KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_915MHZ_2GFSK_50KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_915MHZ_2GFSK_50KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 20;
    }
#endif
#ifdef CC1120_MODE_CC1190_915MHZ_4GFSK_200KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_915MHZ_4GFSK_200KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_915MHZ_4GFSK_200KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 10;
    }
#endif
#ifdef CC1120_MODE_CC1190_950MHZ_2GFSK_50KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_950MHZ_2GFSK_50KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_915MHZ_2GFSK_50KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 20;
    }
#endif
#ifdef CC1120_MODE_CC1190_950MHZ_4GFSK_200KBPS_ENABLED
    else if (mode == CC1120_MODE_CC1190_950MHZ_4GFSK_200KBPS)
    {
        struct cc1120_register_setting conf[] =
            CC1120_CONF_CC1190_915MHZ_4GFSK_200KBPS;
        config(dev_id, conf);
        wpan_dev[dev_id].c.cc1120.mode = mode;
        wpan_dev[dev_id].symbol_duration = 10;
    }
#endif
    else
    {
        return;
    }

    cc1120_prepare_rx(dev_id);
    cc1120_command_strobe(dev_id, CC1120_SRX, NULL);
    cc1120_enable_intr(dev_id, TRUE);
}
#endif //CC1120_M
