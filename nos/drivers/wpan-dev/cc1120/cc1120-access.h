// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief CC1120 FIFOs, registers access
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 9. 1.
 */

#ifndef CC1120_ACCESS_H
#define CC1120_ACCESS_H

#include "kconf.h"
#ifdef CC1120_M

#include "nos_common.h"
#include "errorcodes.h"

ERROR_T cc1120_command_strobe(UINT8 dev_id, UINT8 cmd, UINT8 *ret);

ERROR_T cc1120_set_register(UINT8 dev_id, UINT16 reg, UINT8 value);

ERROR_T cc1120_get_register(UINT8 dev_id, UINT16 reg, UINT8 *value);

ERROR_T cc1120_rxfifo_pop_single(UINT8 dev_id, UINT8 *value);

ERROR_T cc1120_rxfifo_pop_burst(UINT8 dev_id, UINT8 *data, UINT8 n);

ERROR_T cc1120_txfifo_push(UINT8 dev_id, const UINT8 *buf);

#endif //CC1120_M
#endif //CC1120_ACCESS_H
