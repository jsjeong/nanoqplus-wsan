// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * WPAN device abstraction
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 12. 28.
 */

#ifndef WPAN_DEV_H
#define WPAN_DEV_H

#include "kconf.h"

#ifdef IEEE_802_15_4_M
#include "nos_common.h"
#include "errorcodes.h"
#include "ieee-802-15-4.h"
#include "mac.h"

#ifdef CC1120_M
#include "cc1120.h"
#endif

#ifdef CC2420_M
#include "cc2420.h"
#endif

#ifdef CC2520_M
#include "cc2520.h"
#endif

#ifdef AT86RF212_M
#include "rf212.h"
#endif

#ifdef AT86RF230_M
#include "rf230.h"
#endif

#ifdef ADF7023J_M
#include "adf7023-j-const.h"
#endif

#ifdef MG2410_M
#include "mg2410-nos.h"
#endif

enum wpan_dev_command
{
    WPAN_DEV_CMD_SETUP,
    WPAN_DEV_CMD_FLUSH_RXBUF,
    WPAN_DEV_CMD_READ_FRAME,
    WPAN_DEV_CMD_WRITE_FRAME,
    WPAN_DEV_CMD_RXBUF_IS_EMPTY,
    WPAN_DEV_CMD_SWITCH_RX,
    WPAN_DEV_CMD_TX,
    WPAN_DEV_CMD_WAIT_ACK,
    WPAN_DEV_CMD_CCA,
    WPAN_DEV_CMD_SET_PAN_ID,
    WPAN_DEV_CMD_SET_SHORT_ID,
};

enum wpan_dev_cmd_error_code
{
    ERROR_CRC_ERROR = -11,
    ERROR_INVALID_FRAME = -12,
    ERROR_TXFAIL_CH_BUSY = -11
};

struct wpan_dev_cmdset
{
    /**
     * @return None.
     */
    void (*setup)(UINT8 dev_id);

    /**
     * Readout a frame from a frame reception buffer.
     *
     * @param[in] dev_id  Device ID
     * @param[out] frame  Room to store the read frame.
     *
     * @return ERROR_SUCCESS        Reading is done.
     *         ERROR_INVALID_FRAME  Frame is invalid.
     *         ERROR_CRC_ERROR      Frame CRC is invalid.
     *
     * @note This function should be called by MAC layer with valid @p id and
     *       non-NULL @p frame.
     */
    ERROR_T (*read_frame)(UINT8 dev_id, struct wpan_rx_frame *frame);

    /**
     * Write a frame to a transmission buffer.
     *
     * @param[in] dev_id  Device ID
     * @param[in] tx      Frame to be transmitted
     * @return None
     */
    void (*write_frame)(UINT8 dev_id, const UINT8 *buf);

    /**
     * Check whether frame reception buffer of an IEEE 802.15.4 device is empty
     * or not.
     *
     * @param[in] dev_id  Device ID to be checked.
     *
     * @return TRUE   The buffer is empty.
     *         FALSE  The buffer is not empty.
     */
    BOOL (*rxbuf_is_empty)(UINT8 dev_id);

    /**
     * @return None.
     */
    void (*flush_rxbuf)(UINT8 dev_id);

    /**
     * @return None.
     */
    void (*sleep)(UINT8 dev_id);

    /**
     * @return None.
     */
    void (*wake_up)(UINT8 dev_id);

    /**
     * Transmit a frame stored in a transmission buffer.
     *
     * @param[in] id  Valid device ID
     * @param[in] tx  The frame to be transmitted. It is used when the frame
     *                re-buffering is required for some transceivers that have
     *                single buffer for Tx/Rx.
     *
     * @return ERROR_SUCCESS         Transmission success
     *         ERROR_FAIL            Transmission failure due to unknown reason
     *         ERROR_TXFAIL_CH_BUSY  Transmission failure due to busy channel
     *
     * @note This function should be called by MAC layer with valid @p id and
     *       non-NULL @p tx.
     */
    ERROR_T (*transmit)(UINT8 dev_id, const UINT8 *buf);

    /**
     * Wait for an acknowledgment frame.
     *
     * @param[in] dev_id  Device ID
     * @param[in] seq     Frame sequence number to be acknowledged.
     * @return ERROR_SUCCESS        Acknowledgment frame is received.
     *         ERROR_FAIL           Acknowledgment frame is not received.
     *         ERROR_INVALID_FRAME  Received frame is invalid.
     */
    ERROR_T (*wait_for_ack)(UINT8 dev_id, UINT8 seq);

    BOOL (*cca)(UINT8 dev_id);

    /**
     * @return None.
     */
    void (*set_pan_id)(UINT8 dev_id);

    /**
     * @return None.
     */
    void (*set_short_id)(UINT8 dev_id);

    /**
     * @return None.
     */
    void (*set_eui64)(UINT8 dev_id);

#ifdef GLOSSY_M
    /**
     * Peek a received frame rapidly to support Glossy. This function may be
     * implemented various ways according to the radio chip's characteristics.
     * For example, it can be a function that peeks the chip's buffer directly
     * for single Rx/Tx buffer transceivers such as RF212 & RF230. In contrast,
     * for dual buffers transceivers such as CC2420 & CC1120, it can be a
     * function that reads out data from the Rx buffer and loads into the Tx
     * buffer.
     *
     * @param[in] dev_id  Device ID
     * @param[out] len    Pointer where a frame length will be stored at. If it
     *                    is NULL, storing the value is discarded.
     * @param[out] seq    Pointer where an IEEE 802.15.4 sequnce field will be
     *                    stored at. If it is NULL, storing the value is
     *                    discarded.
     * @param[out] dst    Pointer where an IEEE 802.15.4 short destination
     *                    address field will be stored at. If it is NULL,
     *                    storing the value is discarded.
     * @param[out] src    Pointer where an IEEE 802.15.4 short source address
     *                    field will be stored at. If it is NULL, storing the
     *                    value is discarded.
     *
     * @return TRUE   Received frame's CRC is OK.
     *         FALSE  Received frame's CRC is invalid.
     */
    BOOL (*glossy_peek_received_frame)(UINT8 dev_id,
                                       UINT8 *seq,
                                       UINT16 *dst,
                                       UINT16 *src);

    /**
     * Lock a chip to forward. It is required for single buffer transceivers
     * such as RF212/RF230 to protect a buffer corruption by packets from the
     * air.
     */
    void (*glossy_lock)(UINT8 dev_id);

    /**
     * Release a chip after forward. It should be called if the glossy_lock()
     * has been called.
     */
    void (*glossy_release)(UINT8 dev_id);
#endif //GLOSSY_M
};

struct wpan_device
{
    const struct wpan_dev_cmdset *cmd;
    void (*notify)(UINT8 dev_id, UINT8 event, void *args);

    BOOL hw_autotx:1;
    BOOL rx_on:1;
    BOOL locked:1;
    BOOL eui64_assigned:1;
    BOOL sec_support:1;
    BOOL addrdec_support:1;

    UINT8 symbol_duration;
    UINT8 channel;
    UINT16 pan_id;
    UINT16 short_id;
    UINT8 eui64[8];   /* EUI-64 in MSB first */
    enum nos_mac_type mac_type;

    union
    {
#ifdef ADF7023J_M
        struct adf7023j_chip_status adf7023j;
#endif
        
#ifdef AT86RF212_M
        struct rf212 rf212;
#endif

#ifdef AT86RF230_M
        struct rf230 rf230;
#endif
        
#ifdef CC1120_M
        struct cc1120_device cc1120;
#endif
        
#ifdef CC2420_M
        CC2420 cc2420;
#endif

#ifdef CC2520_M
        struct cc2520 cc2520;
#endif

#ifdef MG2410_M
        MG2410 mg2410;
#endif        
    } c;
};
typedef struct wpan_device WPAN_DEV;

/**
 * Configure an IEEE 802.15.4 device.
 *
 * @param[in] dev_id    Device ID which defined in platform.h.
 * @param[in] pan       16-bit PAN ID
 * @param[in] short_id  16-bit short ID
 * @param[in] eui64     64-bit extended ID
 * @param[in] mac_event_notifier  Function pointer to notify events to MAC layer.
 *
 * @note This function should be called by nos_mac_init() with valid @p id.
 */
void wpan_dev_setup(UINT8 dev_id,
                    UINT16 pan,
                    UINT16 short_id,
                    const UINT8 *eui64,
                    void (*mac_event_notifier)(UINT8, UINT8, void *),
                    enum nos_mac_type mac_type);

/**
 * Lock an IEEE 802.15.4 device before transmission.
 *
 * @param[in] id  Device ID to be locked.
 * @return ERROR_SUCCESS    Locking done.
 *         ERROR_BUSY       Device is busy.
 *         ERROR_NOT_FOUND  @p id is out of range.
 */
ERROR_T wpan_dev_lock(UINT8 id);

/**
 * Release an IEEE 802.15.4 device after transmission.
 *
 * @param[in] id  Device ID to be released.
 * @return ERROR_SUCCESS    Releasing done.
 *         ERROR_NOT_FOUND  @p id is out of range.
 */
ERROR_T wpan_dev_release(UINT8 id);

/**
 * Assess a channel status.
 *
 * @param[in] id  Device ID
 * @param[out] is_clear  Channel state.
 * @return TRUE   Channel is clear.
 *         FALSE  Channel is busy.
 */
BOOL wpan_dev_cca(UINT8 id);

void wpan_dev_set_pan_id(UINT8 dev_id, UINT16 new_pan_id);
void wpan_dev_set_short_id(UINT8 dev_id, UINT16 new_short_id);
void wpan_dev_set_eui64(UINT8 dev_id, const UINT8 *new_eui64);

#define wpan_dev_rxbuf_is_empty(id)   wpan_dev[id].cmd->rxbuf_is_empty(id)
#define wpan_dev_flush_rxbuf(id)      wpan_dev[id].cmd->flush_rxbuf(id)
#define wpan_dev_read_frame(id, f)    wpan_dev[id].cmd->read_frame(id, f)
#define wpan_dev_write_frame(id, buf) wpan_dev[id].cmd->write_frame(id, buf)
#define wpan_dev_sleep(id)            wpan_dev[id].cmd->sleep(id)
#define wpan_dev_wakeup(id)           wpan_dev[id].cmd->wake_up(id)
#define wpan_dev_tx(id, buf)          wpan_dev[id].cmd->transmit(id, buf)
#define wpan_dev_wait_ack(id, seq)    wpan_dev[id].cmd->wait_for_ack(id, seq)

#ifdef GLOSSY_M
#define wpan_dev_glossy_peek_rx_frame(id, seq, dst, src)            \
    wpan_dev[id].cmd->glossy_peek_received_frame(id, seq, dst, src)

#define wpan_dev_glossy_lock(id)                \
    do {                                        \
        if (wpan_dev[id].cmd->glossy_lock)      \
            wpan_dev[id].cmd->glossy_lock(id);  \
    } while(0)

#define wpan_dev_glossy_release(id)               \
    do {                                          \
        if (wpan_dev[id].cmd->glossy_release)     \
            wpan_dev[id].cmd->glossy_release(id); \
    } while(0)
#endif

#endif //IEEE_802_15_4_M
#endif // WPAN_DEV_H
