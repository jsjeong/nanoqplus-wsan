/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 11. 29.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef GENERIC_MIC_H_
#define GENERIC_MIC_H_

#include "kconf.h"
#ifdef SENSOR_MIC_M
#include "platform.h"

#ifdef GENERIC_ADC_MIC_SENSOR_M

#if !defined PORT_MIC_SENSOR_POWER_SW || \
    !defined PIN_MIC_SENSOR_POWER_SW || \
    !defined PORT_ADC_MIC_SENSOR_CHANNEL || \
    !defined PIN_ADC_MIC_SENSOR_CHANNEL
#error "PORT_MIC_SENSOR_POWER_SW, PIN_MIC_SENSOR_POWER_SW, and ADC_MIC_SENSOR_CHANNEL must be defined in platform.h in your mote's platform directory (e.g., $NOS_HOME/nos/platform/xxx) to use a generic ADC mic sensor."
#else

#include "generic_analog_sensor.h"
#define nos_mic_sensor_init() \
    generic_analog_sensor_init(MIC_SENSOR_POWER_SW_PORT, \
            MIC_SENSOR_POWER_SW_PIN, \
            MIC_SENSOR_ADC_CHANNEL_PORT, \
            MIC_SENSOR_ADC_CHANNEL_PIN)

#define nos_mic_sensor_power_on() \
    generic_analog_sensor_on(MIC_SENSOR_POWER_SW_PORT, \
            MIC_SENSOR_POWER_SW_PIN)

#define nos_mic_sensor_power_off() \
    generic_analog_sensor_off(MIC_SENSOR_POWER_SW_PORT, \
            MIC_SENSOR_POWER_SW_PIN)

#define nos_mic_sensor_get_data() \
    generic_analog_sensor_get_data(MIC_SENSOR_ADC_CHANNEL_PIN)
#endif

#endif
#endif
#endif /* GENERIC_MIC_H_ */
