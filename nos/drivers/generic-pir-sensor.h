/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 12. 05.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef PIR_SENSOR_H_
#define PIR_SENSOR_H_

#include "kconf.h"
#ifdef SENSOR_PIR_M

#ifdef GENERIC_ADC_PIR_SENSOR_M
#include "generic_analog_sensor.h"

#if defined PIR_SENSOR_POWER_SW_PORT && defined PIR_SENSOR_POWER_SW_PIN
#define nos_pir_sensor_init() \
    generic_analog_sensor_init(PIR_SENSOR_POWER_SW_PORT, \
            PIR_SENSOR_POWER_SW_PIN, \
            PIR_SENSOR_ADC_CHANNEL_PORT, \
            PIR_SENSOR_ADC_CHANNEL_PIN)
#define nos_pir_sensor_power_on() \
    generic_analog_sensor_on(PIR_SENSOR_POWER_SW_PORT, \
            PIR_SENSOR_POWER_SW_PIN)
#define nos_pir_sensor_power_off() \
    generic_analog_sensor_off(PIR_SENSOR_POWER_SW_PORT, \
            PIR_SENSOR_POWER_SW_PIN)
#else // There is no sensor switch.
#define nos_pir_sensor_init() \
    NOS_GPIO_INIT_PERIPHERAL(PIR_SENSOR_ADC_CHANNEL_PORT,\
            PIR_SENSOR_ADC_CHANNEL_PIN)
#define nos_pir_sensor_power_on()
#define nos_pir_sensor_power_off()
#endif

#define nos_pir_sensor_get_data() \
    generic_analog_sensor_get_data(PIR_SENSOR_ADC_CHANNEL_PIN)

#elif OCTACOMM_PIR_SENSOR_M
#include "octacomm-pir-sensor-module.h"
#define nos_pir_sensor_init()       octacomm_pir_sensor_init()
#define nos_pir_sensor_power_on()   octacomm_pir_sensor_power_on()
#define nos_pir_sensor_power_off()  octacomm_pir_sensor_power_off()
#define nos_pir_sensor_get_data()   // Nothing to do.

#elif defined PLATFORM_SPECIFIC_PIR_SENSOR_M
#endif

#endif /* SENSOR_PIR_M */
#endif /* PIR_SENSOR_H_ */
