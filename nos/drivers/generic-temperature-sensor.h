/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 11. 29.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_

#include "kconf.h"
#ifdef SENSOR_TEMPERATURE_M
#include "nos_common.h"
#include "platform.h"

#ifdef HAVE_DEVICE_IDS

enum TEMPERATURE_SENSORS
{
    TEMPERATURE_SENSOR_SHTXX = 1,
    TEMPERATURE_SENSOR_UNKNOWN = 255,
};

void nos_temperature_sensor_on(UINT8 id);
void nos_temperature_sensor_off(UINT8 id);
UINT16 nos_temperature_sensor_read(UINT8 id);

#elif defined GENERIC_ADC_TEMPERATURE_SENSOR_M

#if !defined PORT_TEMPERATURE_SENSOR_POWER_SW || \
    !defined PIN_TEMPERATURE_SENSOR_POWER_SW || \
    !defined PORT_ADC_TEMPERATURE_SENSOR_CHANNEL || \
    !defined PIN_ADC_TEMPERATURE_SENSOR_CHANNEL
#error "PORT_TEMPERATURE_SENSOR_POWER_SW, PIN_TEMPERATURE_SENSOR_POWER_SW, and ADC_TEMPERATURE_SENSOR_CHANNEL must be defined in platform.h in your mote's platform directory (e.g., $NOS_HOME/nos/platform/xxx) to use a generic ADC temperature sensor."
#else

#include "generic_analog_sensor.h"
#define nos_temperature_sensor_init() \
    generic_analog_sensor_init(PORT_TEMPERATURE_SENSOR_POWER_SW, \
            PIN_TEMPERATURE_SENSOR_POWER_SW, \
            PORT_ADC_TEMPERATURE_SENSOR_CHANNEL, \
            PIN_ADC_TEMPERATURE_SENSOR_CHANNEL)

#define nos_temperature_sensor_power_on() \
    generic_analog_sensor_on(PORT_TEMPERATURE_SENSOR_POWER_SW, \
            PIN_TEMPERATURE_SENSOR_POWER_SW)

#define nos_temperature_sensor_power_off() \
    generic_analog_sensor_off(PORT_TEMPERATURE_SENSOR_POWER_SW, \
            PIN_TEMPERATURE_SENSOR_POWER_SW)

#define nos_temperature_sensor_get_data() \
    generic_analog_sensor_get_data(PIN_ADC_TEMPERATURE_SENSOR_CHANNEL)
#endif

#elif defined SHTXX_M
#include "sensor_shtxx.h"
#define nos_temperature_sensor_init()       shtxx_init()
#define nos_temperature_sensor_power_on()   shtxx_set_temperature_power(TRUE)
#define nos_temperature_sensor_power_off()  shtxx_set_temperature_power(FALSE)
#define nos_temperature_sensor_get_data()   shtxx_get_temperature()

#elif defined MTS300_M
#include "mts300.h"
#define nos_temperature_sensor_init()       mts300_init_temperature_sensor()
#define nos_temperature_sensor_power_on()   mts300_set_temperature_sensor_on()
#define nos_temperature_sensor_power_off()  mts300_set_temperature_sensor_off()
#define nos_temperature_sensor_get_data()   mts300_get_temperature_value()

//TODO Implement MTS310~420 boards drivers.
//#elif defined MTS310_M
//#include "mts310.h"
//#define nos_temperature_sensor_init()       mts310_init_temperature_sensor()
//#define nos_temperature_sensor_power_on()   mts310_set_temperature_sensor_power(TRUE)
//#define nos_temperature_sensor_power_off()  mts310_set_temperature_sensor_power(FALSE)
//#define nos_temperature_sensor_get_data()   mts310_get_temperature_value()
//
//#elif defined MTS400_M
//#include "mts400.h"
//#define nos_temperature_sensor_init()       mts400_init_temperature_sensor()
//#define nos_temperature_sensor_power_on()   mts400_set_temperature_sensor_power(TRUE)
//#define nos_temperature_sensor_power_off()  mts400_set_temperature_sensor_power(FALSE)
//#define nos_temperature_sensor_get_data()   mts400_get_temperature_value()
//
//#elif defined MTS420_M
//#include "mts420.h"
//#define nos_temperature_sensor_init()       mts420_init_temperature_sensor()
//#define nos_temperature_sensor_power_on()   mts420_set_temperature_sensor_power(TRUE)
//#define nos_temperature_sensor_power_off()  mts420_set_temperature_sensor_power(FALSE)
//#define nos_temperature_sensor_get_data()   mts420_get_temperature_value()

#elif defined OCTACOMM_TEMPERATURE_SENSOR_M
#include "octacomm-env-sensor-module.h"
#define nos_temperature_sensor_init()       octacomm_temperature_sensor_init()
#define nos_temperature_sensor_power_on()   octacomm_temperature_sensor_power_on()
#define nos_temperature_sensor_power_off()  octacomm_temperature_sensor_power_off()
#define nos_temperature_sensor_get_data()   octacomm_temperature_sensor_get_data()

#elif defined PLATFORM_SPECIFIC_TEMPERATURE_SENSOR_M
#include "platform_temperature_sensor.h"
#define nos_temperature_sensor_init()       platform_init_temperature_sensor()
#define nos_temperature_sensor_power_on()   platform_temperature_sensor_on()
#define nos_temperature_sensor_power_off()  platform_temperature_sensor_off()
#define nos_temperature_sensor_get_data()   platform_get_temperature_value()
#endif

#endif
#endif /* TEMPERATURE_H_ */
