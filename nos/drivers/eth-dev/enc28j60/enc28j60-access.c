// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Access to ENC28J60 Ethernet controller
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 10.
 */

#include "enc28j60-access.h"

#ifdef ENC28J60_M
#include "enc28j60-interface.h"
#include "enc28j60-const.h"
#include "eth-dev.h"

extern struct eth_device eth_dev[];

UINT8 enc28j60_read_op(UINT8 dev_id, UINT8 op, UINT8 addr)
{
    UINT8 dat = 0;

    enc28j60_request_cs(dev_id);
    enc28j60_spi(dev_id, op | (addr & ADDR_MASK));
    dat = enc28j60_spi(dev_id, 0xFF);

    if(addr & 0x80)
    {
        // Do dummy read if needed (for mac and mii, see datasheet page 29)
        dat = enc28j60_spi(dev_id, 0xFF);
    }

    enc28j60_release_cs(dev_id);

    return dat;
}

void enc28j60_write_op(UINT8 dev_id, UINT8 op, UINT8 addr, UINT8 d)
{
    enc28j60_request_cs(dev_id);
    enc28j60_spi(dev_id, op | (addr & ADDR_MASK));
    enc28j60_spi(dev_id, d);
    enc28j60_release_cs(dev_id);
}

void enc28j60_read_buffer(UINT8 dev_id, UINT8 *d, UINT16 len)
{
    enc28j60_request_cs(dev_id);
    enc28j60_spi(dev_id, ENC28J60_READ_BUF_MEM);
    while (len)
    {
        len--;
        if (d)
        {
            *d = (UINT8) enc28j60_spi(dev_id, 0);
            d++;
        }
        else
        {
            enc28j60_spi(dev_id, 0);
        }
    }
    enc28j60_release_cs(dev_id);
}

void enc28j60_write_buffer(UINT8 dev_id, const PACKET *v)
{
    enc28j60_request_cs(dev_id);
    enc28j60_spi(dev_id, ENC28J60_WRITE_BUF_MEM);
    
    while (v)
    {
        UINT16 i;
        for (i = 0; i < v->buf_len; i++)
        {
            enc28j60_spi(dev_id, ((UINT8 *) v->buf)[i]);
        }

        v = v->next;
    }
    enc28j60_release_cs(dev_id);
}

void enc28j60_set_bank(UINT8 dev_id, UINT8 addr)
{
    struct enc28j60 *chip = (struct enc28j60 *) eth_dev[dev_id].dev_ctrl;
    
    if ((addr & BANK_MASK) != chip->bank)
    {
        enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_CLR, ECON1,
                          (ECON1_BSEL1 | ECON1_BSEL0));
        enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_SET, ECON1,
                          (addr & BANK_MASK) >> 5);
        chip->bank = (addr & BANK_MASK);
    }
}

UINT8 enc28j60_read(UINT8 dev_id, UINT8 addr)
{
    enc28j60_set_bank(dev_id, addr);
    return enc28j60_read_op(dev_id, ENC28J60_READ_CTRL_REG, addr);
}

void enc28j60_write(UINT8 dev_id, UINT8 addr, UINT8 d)
{
    enc28j60_set_bank(dev_id, addr);
    enc28j60_write_op(dev_id, ENC28J60_WRITE_CTRL_REG, addr, d);
}

#endif //ENC28J60_M
