// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ENC28J60 Ethernet controller driver interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 10.
 */

#ifndef ENC28J60_INTERFACE_H
#define ENC28J60_INTERFACE_H

#include "kconf.h"

#ifdef ENC28J60_M
#include "nos_common.h"

/**
 * Set CS pin low.
 *
 * @note Since the ENC28J60 chip's interrupt pin is asserted regardless CS pin's
 *       state, the implementation of this function should include disabling the
 *       interrupt temporarily.
 */
void enc28j60_request_cs(UINT8 dev_id);

/**
 * Set CS pin high.
 *
 * @note Since the ENC28J60 chip's interrupt pin is asserted regardless CS pin's
 *       state, the implementation of this function should include enabling the
 *       interrupt again.
 */
void enc28j60_release_cs(UINT8 dev_id);

UINT8 enc28j60_spi(UINT8 dev_id, UINT8 txd);

void enc28j60_interface_init(UINT8 dev_id);

BOOL enc28j60_intn_pin_is_set(UINT8 dev_id);

#endif //ENC28J60_M
#endif //ENC28J60_INTERFACE_H
