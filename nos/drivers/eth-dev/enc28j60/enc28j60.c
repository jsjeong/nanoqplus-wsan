// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * ENC28J60 Ethernet controller driver
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 10.
 */

#include "enc28j60.h"

#ifdef ENC28J60_M

#include "eth-dev.h"
#include "enc28j60-access.h"
#include "enc28j60-const.h"
#include "enc28j60-interface.h"
#include "heap.h"
#include "arch.h"
#include <stdio.h>
#include "critical_section.h"

extern struct eth_device eth_dev[];

static UINT16 phy_read(UINT8 dev_id, UINT8 addr)
{
    UINT16 d;
    
    NOS_ENTER_CRITICAL_SECTION();
    
    enc28j60_write(dev_id, MIREGADR, addr);
    enc28j60_write(dev_id, MICMD, MICMD_MIIRD);
    while (enc28j60_read(dev_id, MISTAT) & MISTAT_BUSY);
    enc28j60_write(dev_id, MICMD, 0);
    d = enc28j60_read(dev_id, MIRDL);
    d |= enc28j60_read(dev_id, MIRDH) << 8;

    NOS_EXIT_CRITICAL_SECTION();

    return d;
}

static void phy_write(UINT8 dev_id, UINT8 addr, UINT16 d)
{
    NOS_ENTER_CRITICAL_SECTION();
    
    // set the PHY register address
    enc28j60_write(dev_id, MIREGADR, addr);
    // write the PHY data
    enc28j60_write(dev_id, MIWRL, d);
    enc28j60_write(dev_id, MIWRH, d >> 8);
    // wait until the PHY write completes
    while(enc28j60_read(dev_id, MISTAT) & MISTAT_BUSY);

    NOS_EXIT_CRITICAL_SECTION();
}

static void enc28j60_setup(UINT8 dev_id)
{
    if (eth_dev[dev_id].net_type == IPV6_OVER_ETHERNET)
    {
        enc28j60_write(dev_id, ERXFCON,
                       ERXFCON_UCEN |
                       ERXFCON_CRCEN |
                       ERXFCON_MCEN);
    }
    else
    {
        // Not supported upper layer.
        return;
    }

    // bring MAC out of reset
    enc28j60_write(dev_id, MACON2, 0x00);
    
    // enable MAC receive
    enc28j60_write(dev_id, MACON1, MACON1_MARXEN | MACON1_TXPAUS | MACON1_RXPAUS);
    
    // enable automatic padding to 60bytes and CRC operations
    enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_SET, MACON3,
                      MACON3_PADCFG0 | MACON3_TXCRCEN | MACON3_FRMLNEN | MACON3_FULDPX);
    
    // set inter-frame gap (non-back-to-back)
    enc28j60_write(dev_id, MAIPGL, 0x12);
    enc28j60_write(dev_id, MAIPGH, 0x0C);
    // set inter-frame gap (back-to-back)
    enc28j60_write(dev_id, MABBIPG, 0x12);

// Set the maximum packet size which the controller will accept
    // Do not send packets longer than MAX_FRAMELEN:
    enc28j60_write(dev_id, MAMXFLL, MAX_FRAMELEN&0xFF);
    enc28j60_write(dev_id, MAMXFLH, MAX_FRAMELEN>>8);
    
    // write MAC address
    enc28j60_write(dev_id, MAADR5, eth_dev[dev_id].mac48[0]);
    enc28j60_write(dev_id, MAADR4, eth_dev[dev_id].mac48[1]);
    enc28j60_write(dev_id, MAADR3, eth_dev[dev_id].mac48[2]);
    enc28j60_write(dev_id, MAADR2, eth_dev[dev_id].mac48[3]);
    enc28j60_write(dev_id, MAADR1, eth_dev[dev_id].mac48[4]);
    enc28j60_write(dev_id, MAADR0, eth_dev[dev_id].mac48[5]);

    /* printf("MAC48=%02x:%02x:%02x:%02x:%02x:%02x, %02x:%02x:%02x:%02x:%02x:%02x expected\n", */
    /*        enc28j60_read(dev_id, MAADR5), enc28j60_read(dev_id, MAADR4), */
    /*        enc28j60_read(dev_id, MAADR3), enc28j60_read(dev_id, MAADR2), */
    /*        enc28j60_read(dev_id, MAADR1), enc28j60_read(dev_id, MAADR0), */
    /*        eth_dev[dev_id].mac48[0], eth_dev[dev_id].mac48[1], */
    /*        eth_dev[dev_id].mac48[2], eth_dev[dev_id].mac48[3], */
    /*        eth_dev[dev_id].mac48[4], eth_dev[dev_id].mac48[5]); */

    phy_write(dev_id, PHCON1, PHCON1_PDPXMD);
    phy_write(dev_id, PHCON2, PHCON2_HDLDIS); // no loopback of transmitted frames

    // switch to bank 0
    enc28j60_set_bank(dev_id, ECON1);
    // enable interrutps
    enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_SET, EIE,
                      EIE_INTIE | EIE_PKTIE);
    
    // enable packet reception
    enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_SET, ECON1, ECON1_RXEN);
}

static BOOL enc28j60_tx(UINT8 dev_id, const PACKET *v)
{
    UINT16 len;
    BOOL result;

    len = nos_packet_length(v);

    // Set the write pointer to start of transmit buffer area
    enc28j60_write(dev_id, EWRPTL, TXSTART_INIT & 0xFF);
    enc28j60_write(dev_id, EWRPTH, TXSTART_INIT >> 8);

    // Set the TXND pointer to correspond to the packet size given
    enc28j60_write(dev_id, ETXNDL, (TXSTART_INIT + len) & 0xFF);
    enc28j60_write(dev_id, ETXNDH, (TXSTART_INIT + len) >> 8);

    // write per-packet control byte (0x00 means use macon3 settings)
    enc28j60_write_op(dev_id, ENC28J60_WRITE_BUF_MEM, 0, 0x00);

    // copy the packet into the transmit buffer
    enc28j60_write_buffer(dev_id, v);

    // Reset the transmit logic problem. See Rev. B4 Silicon Errata point 12.
    enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRST);
    enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_TXRST);
    
    // send the contents of the transmit buffer onto the network
    enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_SET, ECON1, ECON1_TXRTS);

    while ((enc28j60_read(dev_id, EIR) & EIR_TXIF) == 0);
    enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_CLR, EIR, EIR_TXIF);
    
    if( (enc28j60_read(dev_id, EIR) & EIR_TXERIF) )
    {
        enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_CLR, ECON1, ECON1_TXRTS);
        enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_CLR, EIR, EIR_TXERIF);
    }

    if (enc28j60_read(dev_id, ESTAT) & ESTAT_TXABRT)
    {
        enc28j60_write_op(dev_id, ENC28J60_BIT_FIELD_CLR, ESTAT, ESTAT_TXABRT);
        result = FALSE;
    }
    else
    {
        result = TRUE;
    }

    return result;
}

static BOOL enc28j60_check_link(UINT8 dev_id)
{
    return (phy_read(dev_id, PHSTAT2) & PHSTAT2_LSTAT) ? TRUE : FALSE;
}

static const struct eth_dev_cmdset enc28j60_api =
{
    enc28j60_setup,
    enc28j60_tx,
    enc28j60_check_link,
};

/**
 * @see ENC28J60 Silicon Errata and Data Sheet Clarification, 14.
 */
static void set_erxrdpt_to_odd(UINT8 dev_id)
{
    struct enc28j60 *chip = (struct enc28j60 *) eth_dev[dev_id].dev_ctrl;
    UINT16 new_pt;

    new_pt = chip->next_pkt_ptr;
    if (new_pt - 1 < RXSTART_INIT || new_pt - 1 > RXSTOP_INIT)
        new_pt = RXSTOP_INIT;
    else
        new_pt--;

    enc28j60_write(dev_id, ERXRDPTL, new_pt & 0xFF);
    enc28j60_write(dev_id, ERXRDPTH, new_pt >> 8);
}

void enc28j60_init(UINT8 dev_id)
{
    struct enc28j60 *chip;
    
    eth_dev[dev_id].dev_ctrl = (struct enc28j60 *) nos_malloc(sizeof(struct enc28j60));
    if (!eth_dev[dev_id].dev_ctrl)
        while(1);

    eth_dev[dev_id].cmd = (struct eth_dev_cmdset *) &enc28j60_api;
    chip = (struct enc28j60 *) eth_dev[dev_id].dev_ctrl;
    chip->bank = 0;
    chip->next_pkt_ptr = RXSTART_INIT;
    
    enc28j60_write_op(dev_id, ENC28J60_SOFT_RESET, 0, ENC28J60_SOFT_RESET);
    /*
     * After issuing the Reset command, wait at least 1ms.
     * (Rev. B7 Silicon Errata).
     */
    nos_delay_ms(1);
    
    enc28j60_write(dev_id, ERXSTL, RXSTART_INIT & 0xFF);
    enc28j60_write(dev_id, ERXSTH, RXSTART_INIT >> 8);
    set_erxrdpt_to_odd(dev_id);
    enc28j60_write(dev_id, ERXNDL, RXSTOP_INIT & 0xFF);
    enc28j60_write(dev_id, ERXNDH, RXSTOP_INIT >> 8);
    enc28j60_write(dev_id, ETXSTL, TXSTART_INIT & 0xFF);
    enc28j60_write(dev_id, ETXSTH, TXSTART_INIT >> 8);
    enc28j60_write(dev_id, ETXNDL, TXSTOP_INIT & 0xFF);
    enc28j60_write(dev_id, ETXNDH, TXSTOP_INIT >> 8);
}

void enc28j60_intr_handler(UINT8 dev_id)
{
    struct enc28j60 *chip = (struct enc28j60 *) eth_dev[dev_id].dev_ctrl;
    UINT16 rxstat;
    UINT16 len;
    UINT8 *buf;

    /*
     * Note that checking PKTIF is unreliable. (Rev. B7. Silicon Errata)
     * Also sometimes SPI communication becomes unreliable in our experience. So
     * checking INTn pin is used.
     */
    while (!enc28j60_intn_pin_is_set(dev_id))
    {
        UINT8 tmp[2];

        if (enc28j60_read(dev_id, EPKTCNT) == 0)
            continue;
        
        // Set the read pointer to the start of the received packet
        enc28j60_write(dev_id, ERDPTL, chip->next_pkt_ptr);
        enc28j60_write(dev_id, ERDPTH, chip->next_pkt_ptr >> 8);

        // read the next packet pointer
        enc28j60_read_buffer(dev_id, tmp, 2);
        chip->next_pkt_ptr = (tmp[0] + (tmp[1] << 8));
        
        // read the packet length (see datasheet page 43)
        enc28j60_read_buffer(dev_id, tmp, 2);
        len = tmp[0] + (tmp[1] << 8);

        len -= 4; //remove the CRC count
        // read the receive status (see datasheet page 43)
        enc28j60_read_buffer(dev_id, tmp, 2);
        rxstat = tmp[0] + (tmp[1] << 8);
        
        // Check CRC and symbol errors.
        // (see datasheet page 44, table 7-3):
        // The ERXFCON.CRCEN is set by default. Normally we should not
        // need to check this.
        if ((rxstat & 0x80) == 0)
        {
            // invalid
            len = 0;
        }
        else
        {
            buf = (eth_dev[dev_id].notify) ?
                nos_packet_buffer_alloc(len, FALSE) : NULL;

            if (buf)
            {
                // copy the packet from the receive buffer
                enc28j60_read_buffer(dev_id, buf, len);
                if (!eth_dev[dev_id].notify(dev_id, buf, len))
                {
                    nos_packet_buffer_free(buf);
                }
            }
            else
            {
                // Just read out the frame and discard.
                enc28j60_read_buffer(dev_id, NULL, len);
            }
        }
                
        /*
         * Move the RX read pointer to the start of the next received
         * packet. This frees the memory we just read out.
         */
        set_erxrdpt_to_odd(dev_id);
        
        /*
         * Decrement the packet counter indicate we are done with this
         * packet.
         */
        enc28j60_write_op(dev_id,
                          ENC28J60_BIT_FIELD_SET,
                          ECON2,
                          ECON2_PKTDEC);
    }

    /* printf("%s()-remain %u, ESTAT:0x%X, EIR:0x%X\n", */
    /*        __FUNCTION__, */
    /*        enc28j60_read(dev_id, EPKTCNT), */
    /*        enc28j60_read(dev_id, ESTAT), */
    /*        enc28j60_read(dev_id, EIR)); */
}

#endif //ENC28J60_M
