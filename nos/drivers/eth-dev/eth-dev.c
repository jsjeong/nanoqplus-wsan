// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Ethernet device abstraction
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 9.
 */

#include "kconf.h"
#ifdef ETH_DEV_M

#include "eth-dev.h"
#include "platform.h"
#include <string.h>

struct eth_device eth_dev[ETH_DEV_CNT] = { { 0 } };

void eth_dev_setup(UINT8 dev_id,
                   const UINT8 *mac48,
                   BOOL (*callback)(UINT8 dev_id,
                                    UINT8 *buf,
                                    UINT16 len),
                   enum net_for_ethernet net_type)
{
    memcpy(eth_dev[dev_id].mac48, mac48, 6);
    eth_dev[dev_id].notify = callback;
    eth_dev[dev_id].net_type = net_type;

    if (eth_dev[dev_id].cmd->setup)
        eth_dev[dev_id].cmd->setup(dev_id);
}

BOOL eth_dev_tx(UINT8 dev_id, const PACKET *v)
{
    if (eth_dev[dev_id].cmd->tx)
        return eth_dev[dev_id].cmd->tx(dev_id, v);
    else
        return FALSE;
}

BOOL eth_dev_check_link_status(UINT8 dev_id)
{
    if (eth_dev[dev_id].cmd->check_link)
        return eth_dev[dev_id].cmd->check_link(dev_id);
    else
        return FALSE;
}

#endif //ETH_DEV_M
