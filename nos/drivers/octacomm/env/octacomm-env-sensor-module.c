/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 12. 1.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "octacomm-env-sensor-module.h"
#ifdef OCTACOMM_ENV_SENSOR_M

/**
 * @brief Initialize all sensor's power switch and turn off them to save power.
 */
void octacomm_env_sensor_module_init(void)
{
    static BOOL initialized = FALSE;

    if (initialized == FALSE)
    {
        NOS_GPIO_INIT_OUT(OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PORT,
                OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PIN);
        NOS_GPIO_OFF(OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PORT,
                OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PIN);

        NOS_GPIO_INIT_OUT(OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PORT,
                OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PIN);
        NOS_GPIO_OFF(OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PORT,
                OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PIN);

        NOS_GPIO_INIT_OUT(OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PORT,
                OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PIN);
        NOS_GPIO_OFF(OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PORT,
                OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PIN);

        NOS_GPIO_INIT_OUT(OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PORT,
                OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PIN);
        NOS_GPIO_OFF(OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PORT,
                OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PIN);

        initialized = TRUE;
    }
}

#ifdef OCTACOMM_TEMPERATURE_SENSOR_M
INT16 octacomm_temperature_sensor_get_data(void)
{
    return (generic_analog_sensor_get_data(
            OCTACOMM_ENV_SENSOR_TEMPERATURE_ADC_CHANNEL_PIN) * 25 - 6850);
}
#endif /* OCTACOMM_TEMPERATURE_SENSOR_M */

#ifdef OCTACOMM_HUMIDITY_SENSOR_M
#include "critical_section.h"
#include "arch.h"

const UINT16 rh_table[101] =
{
    7351, 7338, 7326, 7313, 7300, 7288, 7275, 7262, 7249, 7237,
    7224, 7212, 7199, 7187, 7174, 7162, 7150, 7137, 7125, 7112,
    7100, 7088, 7075, 7063, 7050, 7038, 7026, 7013, 7001, 6988,
    6979, 6964, 6951, 6939, 6927, 6915, 6902, 6881, 6878, 6865,
    6853, 6841, 6828, 6816, 6803, 6791, 6778, 6766, 6753, 6741,
    6728, 6715, 6702, 6690, 6677, 6664, 6651, 6638, 6626, 6613,
    6600, 6587, 6574, 6560, 6547, 6534, 6521, 6508, 6494, 6481,
    6468, 6454, 6440, 6427, 6413, 6399, 6385, 6371, 6358, 6344,
    6330, 6316, 6301, 6287, 6272, 6258, 6244, 6229, 6215, 6200,
    6186, 6171, 6155, 6140, 6125, 6109, 6094, 6079, 6064, 6048,
    6033
}; // 7351 (0%), 6033 (100%)

void octacomm_humidity_sensor_init(void)
{
    octacomm_env_sensor_module_init();
    // Input : humidity clock source (frequency), TIMER3
    NOS_GPIO_INIT_IN(OCTACOMM_ENV_SENSOR_HUMIDITY_CLK_PORT,
            OCTACOMM_ENV_SENSOR_HUMIDITY_CLK_PIN);
    TCCR3B |= (1 << CS32) | (1 << CS31) | (0 << CS30);

    octacomm_humidity_sensor_power_on();
}

UINT16 octacomm_humidity_sensor_get_freq(void)
{
    UINT16 humidity_freq = 0;
    NOS_ENTER_CRITICAL_SECTION();
    TCNT3 = 0;
    nos_delay_ms(1000); // delay 1 second
    humidity_freq = TCNT3;
    NOS_EXIT_CRITICAL_SECTION();
    return humidity_freq;
}

UINT16 octacomm_humidity_sensor_get_data(void)
{
    UINT8 i;
    UINT16 hum_freq;
    hum_freq = octacomm_humidity_sensor_get_freq() * 0.9 + 1300; // compensation

    for (i=0; i < 100; ++i)
    {
        if ( hum_freq >= rh_table[i] )
        {
            return i;
        }
    }
    return 100;
}
#endif /* OCTACOMM_HUMIDITY_SENSOR_M */

#endif /* OCTACOMM_ENV_SENSOR_M */
