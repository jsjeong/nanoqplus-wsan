// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file octacomm-env-sensor-module.h
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 12. 1.
 */

#ifndef OCTACOMM_ENV_SENSOR_MODULE_H_
#define OCTACOMM_ENV_SENSOR_MODULE_H_

#include "kconf.h"
#ifdef OCTACOMM_ENV_SENSOR_M
#include "nos_common.h"
#include "platform.h"

void octacomm_env_sensor_module_init(void);

#ifdef OCTACOMM_TEMPERATURE_SENSOR_M
#if !defined OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PORT || \
    !defined OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PIN || \
    !defined OCTACOMM_ENV_SENSOR_TEMPERATURE_ADC_CHANNEL_PORT || \
    !defined OCTACOMM_ENV_SENSOR_TEMPERATURE_ADC_CHANNEL_PIN
#error "OCTACOMM_ENV_SENSOR_TEMPERATURE~_PORT and OCTACOMM_ENV_SENSOR_TEMPERATURE~_PIN must be defined in platform.h."
#endif

#include "generic_analog_sensor.h"
#define octacomm_temperature_sensor_init() \
    do { \
        octacomm_env_sensor_module_init(); \
        generic_analog_sensor_init(OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PORT,\
                OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PIN,\
                OCTACOMM_ENV_SENSOR_TEMPERATURE_ADC_CHANNEL_PORT,\
                OCTACOMM_ENV_SENSOR_TEMPERATURE_ADC_CHANNEL_PIN); \
    } while(0)

#define octacomm_temperature_sensor_power_on() \
	generic_analog_sensor_on(OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PORT,\
			OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PIN)

#define octacomm_temperature_sensor_power_off() \
    generic_analog_sensor_off(OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PORT,\
            OCTACOMM_ENV_SENSOR_TEMPERATURE_POWER_SW_PIN)

INT16 octacomm_temperature_sensor_get_data(void);
#endif /* OCTACOMM_TEMPERATURE_SENSOR_M */

#ifdef OCTACOMM_HUMIDITY_SENSOR_M

#if !defined OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PORT || \
    !defined OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PIN || \
    !defined OCTACOMM_ENV_SENSOR_HUMIDITY_CLK_PORT || \
    !defined OCTACOMM_ENV_SENSOR_HUMIDITY_CLK_PIN
#error "OCTACOMM_ENV_SENSOR_HUMIDITY_~_PORT and OCTACOMM_ENV_SENSOR_HUMIDITY_~_PIN must be defined in platform.h."
#endif

#include "gpio.h"

void octacomm_humidity_sensor_init(void);
#define octacomm_humidity_sensor_power_on() \
    NOS_GPIO_ON(OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PORT,\
            OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PIN)
#define octacomm_humidity_sensor_power_off() \
    NOS_GPIO_OFF(OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PORT,\
            OCTACOMM_ENV_SENSOR_HUMIDITY_POWER_SW_PIN)
UINT16 octacomm_humidity_sensor_get_data(void);
#endif /* OCTACOMM_HUMIDITY_SENSOR_M */

#ifdef OCTACOMM_LIGHT_SENSOR_M
#if !defined OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PORT || \
    !defined OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PIN || \
    !defined OCTACOMM_ENV_SENSOR_LIGHT_ADC_CHANNEL_PORT || \
    !defined OCTACOMM_ENV_SENSOR_LIGHT_ADC_CHANNEL_PIN
#error "PORT_OCTACOMM_ENV_SENSOR_~ and PIN_OCTACOMM_ENV_SENSOR_~ must be defined in platform.h."
#endif

#include "generic_analog_sensor.h"
#define octacomm_light_sensor_init() \
    do { \
        octacomm_env_sensor_module_init(); \
        generic_analog_sensor_init(OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PORT,\
                OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PIN,\
                OCTACOMM_ENV_SENSOR_LIGHT_ADC_CHANNEL_PORT,\
                OCTACOMM_ENV_SENSOR_LIGHT_ADC_CHANNEL_PIN); \
    } while(0)

#define octacomm_light_sensor_power_on() \
    generic_analog_sensor_on(OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PORT,\
            OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PIN)

#define octacomm_light_sensor_power_off() \
    generic_analog_sensor_off(OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PORT,\
            OCTACOMM_ENV_SENSOR_LIGHT_POWER_SW_PIN)

#define octacomm_light_sensor_get_data() \
    generic_analog_sensor_get_data(OCTACOMM_ENV_SENSOR_LIGHT_ADC_CHANNEL_PIN)

#endif /* OCTACOMM_LIGHT_SENSOR_M */

#ifdef OCTACOMM_GAS_SENSOR_M
#if !defined OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PORT || \
    !defined OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PIN || \
    !defined OCTACOMM_ENV_SENSOR_GAS_ADC_CHANNEL_PORT || \
    !defined OCTACOMM_ENV_SENSOR_GAS_ADC_CHANNEL_PIN
#error "PORT_OCTACOMM_ENV_SENSOR_~ and PIN_OCTACOMM_ENV_SENSOR_~ must be defined in platform.h."
#endif

#include "generic_analog_sensor.h"
#define octacomm_gas_sensor_init() \
    do { \
        octacomm_env_sensor_module_init(); \
        generic_analog_sensor_init(OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PORT,\
                OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PIN,\
                PORT_OCTACOMM_ENV_SENSOR_GAS_ADC_CHANNEL,\
                PIN_OCTACOMM_ENV_SENSOR_GAS_ADC_CHANNEL);\
    } while(0)

#define octacomm_gas_sensor_power_on() \
    generic_analog_sensor_on(OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PORT,\
            OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PIN)

#define octacomm_gas_sensor_power_off() \
    generic_analog_sensor_off(OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PORT,\
            OCTACOMM_ENV_SENSOR_GAS_POWER_SW_PIN)

#define octacomm_gas_sensor_get_data() \
    generic_analog_sensor_get_data(OCTACOMM_ENV_SENSOR_GAS_ADC_CHANNEL_PIN)

#endif /* OCTACOMM_GAS_SENSOR_M */

#endif /* OCTACOMM_ENV_SENSOR_M */

#endif /* OCTACOMM_ENV_SENSOR_MODULE_H_ */
