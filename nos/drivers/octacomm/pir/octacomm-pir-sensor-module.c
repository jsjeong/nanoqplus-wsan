/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011.12.05.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "octacomm-pir-sensor-module.h"

#ifdef OCTACOMM_PIR_SENSOR_M

#include <avr/interrupt.h>
#include "critical_section.h"
#include "gpio.h"

static void (*callback_func)(void);

void octacomm_pir_sensor_init()
{
    callback_func = NULL;
    NOS_GPIO_INIT_OUT(OCTACOMM_PIR_SENSOR_POWER_SW_PORT, \
            OCTACOMM_PIR_SENSOR_POWER_SW_PIN);
    //INT7 (PE7) keeps low level for some period after something has beed
    // detected. So, we must generate interrupts on falling edge.
    SET_INT7_IRQ_GENERATED_ON_FALLING_EDGE();
    OCTACOMM_ENABLE_PIR_INTR();
}


void octacomm_pir_set_callback(void (*func)(void))
{
    callback_func = func;
}

ISR(INT7_vect)
{
    NOS_ENTER_ISR();
    if (callback_func)
    {
        callback_func();
    }
    NOS_EXIT_ISR();
}
#endif
