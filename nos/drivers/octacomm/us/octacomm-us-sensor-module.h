/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 12. 05.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef OCTACOMM_US_SENSOR_MODULE_H_
#define OCTACOMM_US_SENSOR_MODULE_H_

#include "kconf.h"
#ifdef OCTACOMM_ULTRASONIC_SENSOR_M
#include "common.h"
#include "intr.h"
#include "platform.h"
#include "gpio.h"

#if !defined OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PORT || \
    !defined OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PIN || \
    !defined OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PORT || \
    !defined OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PIN || \
    !defined OCTACOMM_ULTRASONIC_SENSOR_INTR_PORT || \
    !defined OCTACOMM_ULTRASONIC_SENSOR_INTR_PIN
#error "OCTACOMM_ULTRASONIC_SENSOR_~_PORT and OCTACOMM_ULTRASONIC_SENSOR_~_PIN must be defined in platform.h."
#endif

#define octacomm_clear_us_intr()    CLEAR_INT7_vect()
#define octacomm_enable_us_intr() \
    do { \
        octacomm_clear_us_intr(); \
        ENABLE_INT7_vect(); \
    } while(0)
#define octacomm_disable_us_intr()  DISABLE_INT7_vect()

#define octacomm_ultrasonic_sensor_power_on()   NOS_GPIO_ON(OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PORT, OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PIN)
#define octacomm_ultrasonic_sensor_power_off()  NOS_GPIO_OFF(OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PORT, OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PIN)
#define octacomm_ultrasonic_sensor_tx_on()      NOS_GPIO_ON(OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PORT, OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PIN)
#define octacomm_ultrasonic_sensor_tx_off()     NOS_GPIO_OFF(OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PORT, OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PIN)

void octacomm_ultrasonic_sensor_init(void);
void octacomm_ultrasonic_sensor_set_callback(void (*func)(void));
BOOL octacomm_ultrasonic_sensor_trigger(void);
UINT16 octacomm_ultrasonic_sensor_get_data(void);

#endif /* SENSOR_ULTRASONIC_M */

#endif /* OCTACOMM_US_SENSOR_MODULE_H_ */
