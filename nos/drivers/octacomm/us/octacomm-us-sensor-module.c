/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 12. 05.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "octacomm-us-sensor-module.h"

#ifdef OCTACOMM_ULTRASONIC_SENSOR_M
#include <avr/interrupt.h>
#include "critical_section.h"
#include "arch.h"

static void (*callback_func)(void);
static volatile BOOL us_trigger;
static volatile UINT16 round_trip_counter;

void octacomm_ultrasonic_sensor_init()
{
    callback_func = NULL;
    NOS_GPIO_INIT_OUT(OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PORT, OCTACOMM_ULTRASONIC_SENSOR_POWER_SW_PIN);
    NOS_GPIO_INIT_OUT(OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PORT, OCTACOMM_ULTRASONIC_SENSOR_TX_SW_PIN);
    octacomm_ultrasonic_sensor_power_on();
    octacomm_ultrasonic_sensor_tx_off();

    // US RX interrupt setting
    EICRB |= (1 << ISC71) | (0 << ISC70); // 00 : low level, 01 : any change, 10: falling edge, 11:rising edge
    octacomm_disable_us_intr();

    // Timer3 on for US
    TCCR3B |= (0 << CS32) | (1 << CS31) | (1 << CS30); // 1/64 clk = 800000/64 = 125000 Hz
}

void octacomm_ultrasonic_sensor_set_callback(void (*func)(void))
{
    callback_func = func;
}

BOOL octacomm_ultrasonic_sensor_trigger()
{
    UINT8 waiting_us;
    us_trigger=FALSE;
    octacomm_enable_us_intr();
    octacomm_ultrasonic_sensor_tx_on();
    TCNT3 = 0;
    // Maximum delay 65535tick / (125tick/msec) = 524.28ms
    for ( waiting_us =0; waiting_us < 250; waiting_us++ )
    {
        nos_delay_ms(3);
        if ( us_trigger )
        {
            return TRUE;
        }
    }
    octacomm_disable_us_intr();
    octacomm_ultrasonic_sensor_tx_off();
    return FALSE;
}


ISR(INT7_vect)
{
    if (us_trigger)
    {
        return;
    }
    NOS_ENTER_ISR();

    round_trip_counter = TCNT3;  // read timer
    octacomm_ultrasonic_sensor_tx_off(); // turn off US TX
    octacomm_disable_us_intr();
    us_trigger = TRUE;
    if (callback_func)
    {
        callback_func();
    }

    NOS_EXIT_ISR();
}

UINT16 octacomm_ultrasonic_sensor_get_data()
{
    // Assumption : the speed of ultra-sonic = 340 m/sec (Actually,  331.5 + 0.61 * Celsius)
    // Round trip distance (cm) = (TCNT3 / 125000(Hz) ) * 34000(cm/s)  = TCNT3 * 0.272
    // Maximum 90m (theoretically)
    // returns distance (cm), range : 2cm ~ 8912cm
    return ( (round_trip_counter-11) * 0.136);
}

#endif /* OCTACOMM_ULTRASONIC_SENSOR_M */
