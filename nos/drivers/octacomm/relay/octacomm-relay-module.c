/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 12. 05.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "octacomm-relay-module.h"

#ifdef OCTACOMM_RELAY_M
#include "platform.h"

void octacomm_relay_init(void)
{
#ifdef OCTACOMM_RELAY1_M
    NOS_GPIO_INIT_OUT(OCTACOMM_RELAY1_PORT, OCTACOMM_RELAY1_PIN);
    octacomm_relay_off(1);
#endif /* OCTACOMM_RELAY1_M */

#ifdef OCTACOMM_RELAY2_M
    NOS_GPIO_INIT_OUT(OCTACOMM_RELAY2_PORT, OCTACOMM_RELAY2_PIN);
    octacomm_relay_off(2);
#endif /* OCTACOMM_RELAY2_M */

#ifdef OCTACOMM_RELAY3_M
    NOS_GPIO_INIT_OUT(OCTACOMM_RELAY3_PORT, OCTACOMM_RELAY3_PIN);
    octacomm_relay_off(3);
#endif /* OCTACOMM_RELAY3_M */

#ifdef OCTACOMM_RELAY4_M
    NOS_GPIO_INIT_OUT(OCTACOMM_RELAY4_PORT, OCTACOMM_RELAY4_PIN);
    octacomm_relay_off(4);
#endif /* OCTACOMM_RELAY4_M */
}

void octacomm_relay_on(UINT8 id)
{
    switch (id)
    {
#ifdef OCTACOMM_RELAY1_M
    case 1:
        NOS_GPIO_ON(OCTACOMM_RELAY1_PORT, OCTACOMM_RELAY1_PIN);
        break;
#endif /* OCTACOMM_RELAY1_M */

#ifdef OCTACOMM_RELAY2_M
    case 2:
        NOS_GPIO_ON(OCTACOMM_RELAY2_PORT, OCTACOMM_RELAY2_PIN);
        break;
#endif /* OCTACOMM_RELAY2_M */

#ifdef OCTACOMM_RELAY3_M
    case 3:
        NOS_GPIO_ON(OCTACOMM_RELAY3_PORT, OCTACOMM_RELAY3_PIN);
        break;
#endif /* OCTACOMM_RELAY3_M */

#ifdef OCTACOMM_RELAY4_M
    case 4:
        NOS_GPIO_ON(OCTACOMM_RELAY4_PORT, OCTACOMM_RELAY4_PIN);
        break;
#endif /* OCTACOMM_RELAY4_M */
    }
}

void octacomm_relay_off(UINT8 id)
{
    switch (id)
    {
#ifdef OCTACOMM_RELAY1_M
    case 1:
        NOS_GPIO_OFF(OCTACOMM_RELAY1_PORT, OCTACOMM_RELAY1_PIN);
        break;
#endif /* OCTACOMM_RELAY1_M */

#ifdef OCTACOMM_RELAY2_M
    case 2:
        NOS_GPIO_OFF(OCTACOMM_RELAY2_PORT, OCTACOMM_RELAY2_PIN);
        break;
#endif /* OCTACOMM_RELAY2_M */

#ifdef OCTACOMM_RELAY3_M
    case 3:
        NOS_GPIO_OFF(OCTACOMM_RELAY3_PORT, OCTACOMM_RELAY3_PIN);
        break;
#endif /* OCTACOMM_RELAY3_M */

#ifdef OCTACOMM_RELAY4_M
    case 4:
        NOS_GPIO_OFF(OCTACOMM_RELAY4_PORT, OCTACOMM_RELAY4_PIN);
        break;
#endif /* OCTACOMM_RELAY4_M */
    }
}

void octacomm_relay_toggle(UINT8 id)
{
    switch (id)
    {
#ifdef OCTACOMM_RELAY1_M
    case 1:
        NOS_GPIO_TOGGLE(OCTACOMM_RELAY1_PORT, OCTACOMM_RELAY1_PIN);
        break;
#endif /* OCTACOMM_RELAY1_M */

#ifdef OCTACOMM_RELAY2_M
    case 2:
        NOS_GPIO_TOGGLE(OCTACOMM_RELAY2_PORT, OCTACOMM_RELAY2_PIN);
        break;
#endif /* OCTACOMM_RELAY2_M */

#ifdef OCTACOMM_RELAY3_M
    case 3:
        NOS_GPIO_TOGGLE(OCTACOMM_RELAY3_PORT, OCTACOMM_RELAY3_PIN);
        break;
#endif /* OCTACOMM_RELAY3_M */

#ifdef OCTACOMM_RELAY4_M
    case 4:
        NOS_GPIO_TOGGLE(OCTACOMM_RELAY4_PORT, OCTACOMM_RELAY4_PIN);
        break;
#endif /* OCTACOMM_RELAY0_M */
    }
}

BOOL octacomm_relay_get_state(UINT8 id)
{
    switch (id)
    {
#ifdef OCTACOMM_RELAY1_M
    case 1:
        return NOS_GPIO_READ(OCTACOMM_RELAY1_PORT, OCTACOMM_RELAY1_PIN);
#endif /* OCTACOMM_RELAY1_M */

#ifdef OCTACOMM_RELAY2_M
    case 2:
        return NOS_GPIO_READ(OCTACOMM_RELAY2_PORT, OCTACOMM_RELAY2_PIN);
#endif /* OCTACOMM_RELAY2_M */

#ifdef OCTACOMM_RELAY3_M
    case 3:
        return NOS_GPIO_READ(OCTACOMM_RELAY3_PORT, OCTACOMM_RELAY3_PIN);
#endif /* OCTACOMM_RELAY3_M */

#ifdef OCTACOMM_RELAY4_M
    case 4:
        return NOS_GPIO_READ(OCTACOMM_RELAY4_PORT, OCTACOMM_RELAY4_PIN);
#endif /* OCTACOMM_RELAY4_M */
    }

    return FALSE;
}

#endif /* OCTACOMM_RELAY_M */
