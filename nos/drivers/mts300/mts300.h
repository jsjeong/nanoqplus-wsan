/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011.11.30.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef MTS300_H_
#define MTS300_H_

#include "kconf.h"
#ifdef MTS300_M
#include "common.h"
#include "platform.h"

#ifdef MTS300_TEMPERATURE_M

#if !defined MTS300_TEMPERATURE_POWER_SW_PORT || \
    !defined MTS300_TEMPERATURE_POWER_SW_PIN || \
    !defined MTS300_TEMPERATURE_ADC_CHANNEL_PORT || \
    !defined MTS300_TEMPERATURE_ADC_CHANNEL_PIN
#error "PORTs and PINs related with MTS300 temperature sensor must be defined in platform.h."
#endif

#include "generic_analog_sensor.h"
#define mts300_init_temperature_sensor() \
    generic_analog_sensor_on(\
            MTS300_TEMPERATURE_POWER_SW_PORT,\
            MTS300_TEMPERATURE_POWER_SW_PIN,\
            MTS300_TEMPERATURE_ADC_CHANNEL_PORT,\
            MTS300_TEMPERATURE_ADC_CHANNEL_PIN)
#define mts300_get_temperature_value() \
    generic_analog_sensor_get_data(MTS300_TEMPERATURE_ADC_CHANNEL_PIN)
#define mts300_temperature_sensor_on() \
    generic_analog_sensor_on(\
            MTS300_TEMPERATURE_POWER_SW_PORT,\
            MTS300_TEMPERATURE_POWER_SW_PIN)
#define mts300_temperature_sensor_off() \
    generic_analog_sensor_off(\
            MTS300_TEMPERATURE_POWER_SW_PORT,\
            MTS300_TEMPERATURE_POWER_SW_PIN)
#elif defined MTS300_LIGHT_M

#if !defined MTS300_LIGHT_SENSOR_POWER_SW_PORT || \
    !defined MTS300_LIGHT_SENSOR_POWER_SW_PIN || \
    !defined MTS300_LIGHT_SENSOR_ADC_CHANNEL_PORT || \
    !defined MTS300_LIGHT_SENSOR_ADC_CHANNEL_PIN
#error "PORTs and PINs related with MTS300 light sensor must be defined in platform.h."
#endif

#include "generic_analog_sensor.h"
#define mts300_init_light_sensor() \
    generic_analog_sensor_init(\
            MTS300_LIGHT_SENSOR_POWER_SW_PORT,\
            MTS300_LIGHT_SENSOR_POWER_SW_PIN,\
            MTS300_LIGHT_SENSOR_ADC_CHANNEL_PORT,\
            MTS300_LIGHT_SENSOR_ADC_CHANNEL_PIN)
#define mts300_get_light_intensity() \
    generic_analog_sensor_get_data(MTS300_LIGHT_SENSOR_ADC_CHANNEL_PIN)
#define mts300_set_light_sensor_on() \
    generic_analog_sensor_on(\
            MTS300_TEMPERATURE_POWER_SW_PORT,\
            MTS300_TEMPERATURE_POWER_SW_PIN)
#define mts300_set_light_sensor_off() \
    generic_analog_sensor_off(\
            MTS300_TEMPERATURE_POWER_SW_PORT,\
            MTS300_TEMPERATURE_POWER_SW_PIN)
#endif

//TODO Implement the mic sensor of MTS300 driver.
//#ifdef SENSOR_MIC_M
//#define mts300_init_mic_sensor()
//#define mts300_get_mic_data()
//#define mts300_set_mic_sensor_power(on)
//#endif

#endif
#endif /* MTS300_H_ */
