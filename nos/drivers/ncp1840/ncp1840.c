// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * NCP1840 8-Channel programmable LED driver
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 4. 24.
 */

#include "ncp1840.h"

#ifdef NCP1840_M
#include "ncp1840-interface.h"

enum
{
    I2C_ADDR = (0x1B << 1),
};

enum
{
    CTRL_ALL_LED_ON_OFF = (0 << 5),
    CTRL_SINGLE_REG = (1 << 5),
    CTRL_SINGLE_REG_REFRESH = (2 << 5),
    CTRL_ALL_CURR_LEVEL = (3 << 5),
    CTRL_ALL_PWM_LEVEL = (4 << 5),
};

enum
{
    REG_LED1_CURR = 0x00,
    REG_LED2_CURR = 0x01,
    REG_LED3_CURR = 0x02,
    REG_LED4_CURR = 0x03,
    REG_LED5_CURR = 0x04,
    REG_LED6_CURR = 0x05,
    REG_LED7_CURR = 0x06,
    REG_LED8_CURR = 0x07,
    REG_LED1_PWM = 0x08,
    REG_LED2_PWM = 0x09,
    REG_LED3_PWM = 0x0A,
    REG_LED4_PWM = 0x0B,
    REG_LED5_PWM = 0x0C,
    REG_LED6_PWM = 0x0D,
    REG_LED7_PWM = 0x0E,
    REG_LED8_PWM = 0x0F,
    REG_OUTPUT = 0x10,
    REG_STATUS = 0x11,
};

void ncp1840_init(UINT8 dev)
{
    ncp1840_set_current(dev, 0xFF, NCP1840_CURR_LEVEL0);
    ncp1840_set_pwm(dev, 0xFF, NCP1840_PWM_LEVEL63);
    ncp1840_led_ctrl(dev, 0);
}

ERROR_T ncp1840_set_current(UINT8 dev, UINT8 ledmap, UINT8 current)
{
    ERROR_T err;
    
    if (ledmap == 0xFF)
    {
        err = ncp1840_write_byte(dev, I2C_ADDR, CTRL_ALL_CURR_LEVEL, current);
    }
    else
    {
        if (_IS_SET(ledmap, 0))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED1_CURR,
                                     current);
        }
        
        if (_IS_SET(ledmap, 1))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED2_CURR,
                                     current);
        }
        
        if (_IS_SET(ledmap, 2))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED3_CURR,
                                     current);
        }
        
        if (_IS_SET(ledmap, 3))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED4_CURR,
                                     current);
        }
        
        if (_IS_SET(ledmap, 4))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED5_CURR, current);
        }
        
        if (_IS_SET(ledmap, 5))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED6_CURR, current);
        }
        
        if (_IS_SET(ledmap, 6))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED7_CURR, current);
        }
        
        if (_IS_SET(ledmap, 7))
        {
            err= ncp1840_write_byte(dev, I2C_ADDR,
                                    CTRL_SINGLE_REG_REFRESH | REG_LED8_CURR,
                                    current);
        }
    }

    return err;
}

ERROR_T ncp1840_set_pwm(UINT8 dev, UINT8 ledmap, UINT8 pwm)
{
    ERROR_T err;
    
    if (ledmap == 0xFF)
    {
        err = ncp1840_write_byte(dev, I2C_ADDR, CTRL_ALL_PWM_LEVEL, pwm);
    }
    else
    {
        if (_IS_SET(ledmap, 0))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED1_PWM,
                                     pwm);
        }
        
        if (_IS_SET(ledmap, 1))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED2_PWM,
                                     pwm);
        }

        if (_IS_SET(ledmap, 2))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED3_PWM,
                                     pwm);
        }

        if (_IS_SET(ledmap, 3))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED4_PWM,
                                     pwm);
        }
        
        if (_IS_SET(ledmap, 4))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED5_PWM,
                                     pwm);
        }
        
        if (_IS_SET(ledmap, 5))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED6_PWM,
                                     pwm);
        }
        
        if (_IS_SET(ledmap, 6))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED7_PWM,
                                     pwm);
        }
        
        if (_IS_SET(ledmap, 7))
        {
            err = ncp1840_write_byte(dev, I2C_ADDR,
                                     CTRL_SINGLE_REG_REFRESH | REG_LED8_PWM,
                                     pwm);
        }
    }

    return err;
}

ERROR_T ncp1840_led_ctrl(UINT8 dev, UINT8 ledmap)
{
    return ncp1840_write_byte(0, I2C_ADDR,
                              CTRL_SINGLE_REG_REFRESH | REG_OUTPUT,
                              ledmap);
}

#endif //NCP1840_M
