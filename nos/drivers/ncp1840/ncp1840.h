// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * NCP1840 8-Channel programmable LED driver
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 4. 24.
 */

#ifndef NCP1840_H
#define NCP1840_H

#include "kconf.h"

#ifdef NCP1840_M
#include "nos_common.h"
#include "errorcodes.h"

void ncp1840_init(UINT8 dev);

enum
{
    NCP1840_CURR_LEVEL0 = 0x00,   //2.8333 %
    NCP1840_CURR_LEVEL1 = 0x01,   //3.1667 %
    NCP1840_CURR_LEVEL2 = 0x02,   //3.5333 % (1mA)
    NCP1840_CURR_LEVEL3 = 0x03,   //3.9667 %
    NCP1840_CURR_LEVEL4 = 0x04,   //4.4667 %
    NCP1840_CURR_LEVEL5 = 0x05,   //5.0000 %
    NCP1840_CURR_LEVEL6 = 0x06,   //5.6333 %
    NCP1840_CURR_LEVEL7 = 0x07,   //6.3000 %
    NCP1840_CURR_LEVEL8 = 0x08,   //7.0667 %
    NCP1840_CURR_LEVEL9 = 0x09,   //7.9333 %
    NCP1840_CURR_LEVEL10 = 0x0A,  //8.9000 %
    NCP1840_CURR_LEVEL11 = 0x0B,  //10.0000 % (3mA)
    NCP1840_CURR_LEVEL12 = 0x0C,  //11.2333 %
    NCP1840_CURR_LEVEL13 = 0x0D,  //12.6000 %
    NCP1840_CURR_LEVEL14 = 0x0E,  //14.1333 %
    NCP1840_CURR_LEVEL15 = 0x0F,  //15.8667 % (5mA)
    NCP1840_CURR_LEVEL16 = 0x10,  //17.8000 %
    NCP1840_CURR_LEVEL17 = 0x11,  //19.9667 %
    NCP1840_CURR_LEVEL18 = 0x12,  //22.4000 % (7mA)
    NCP1840_CURR_LEVEL19 = 0x13,  //25.1333 %
    NCP1840_CURR_LEVEL20 = 0x14,  //28.2000 % (9mA)
    NCP1840_CURR_LEVEL21 = 0x15,  //31.6333 %
    NCP1840_CURR_LEVEL22 = 0x16,  //35.4667 %
    NCP1840_CURR_LEVEL23 = 0x17,  //39.8000 %
    NCP1840_CURR_LEVEL24 = 0x18,  //44.6667 %
    NCP1840_CURR_LEVEL25 = 0x19,  //50.2333 %
    NCP1840_CURR_LEVEL26 = 0x1A,  //56.2333 %
    NCP1840_CURR_LEVEL27 = 0x1B,  //63.1000 %
    NCP1840_CURR_LEVEL28 = 0x1C,  //70.8000 %
    NCP1840_CURR_LEVEL29 = 0x1D,  //79.4333 %
    NCP1840_CURR_LEVEL30 = 0x1E,  //89.1333 %
    NCP1840_CURR_LEVEL31 = 0x1F,  //100.0000 %
};

ERROR_T ncp1840_set_current(UINT8 dev, UINT8 ledmap, UINT8 current);

enum
{
    NCP1840_PWM_LEVEL0 = 0x00,     // 0.000%
    NCP1840_PWM_LEVEL1 = 0x01,
    NCP1840_PWM_LEVEL2 = 0x02,
    NCP1840_PWM_LEVEL3 = 0x03,
    NCP1840_PWM_LEVEL4 = 0x04,
    NCP1840_PWM_LEVEL5 = 0x05,
    NCP1840_PWM_LEVEL6 = 0x06,
    NCP1840_PWM_LEVEL7 = 0x07,
    NCP1840_PWM_LEVEL8 = 0x08,
    NCP1840_PWM_LEVEL9 = 0x09,
    NCP1840_PWM_LEVEL10 = 0x0A,
    NCP1840_PWM_LEVEL11 = 0x0B,
    NCP1840_PWM_LEVEL12 = 0x0C,
    NCP1840_PWM_LEVEL13 = 0x0D,
    NCP1840_PWM_LEVEL14 = 0x0E,
    NCP1840_PWM_LEVEL15 = 0x0F,
    NCP1840_PWM_LEVEL16 = 0x10,
    NCP1840_PWM_LEVEL17 = 0x11,
    NCP1840_PWM_LEVEL18 = 0x12,
    NCP1840_PWM_LEVEL19 = 0x13,
    NCP1840_PWM_LEVEL20 = 0x14,
    NCP1840_PWM_LEVEL21 = 0x15,
    NCP1840_PWM_LEVEL22 = 0x16,
    NCP1840_PWM_LEVEL23 = 0x17,
    NCP1840_PWM_LEVEL24 = 0x18,
    NCP1840_PWM_LEVEL25 = 0x19,
    NCP1840_PWM_LEVEL26 = 0x1A,
    NCP1840_PWM_LEVEL27 = 0x1B,
    NCP1840_PWM_LEVEL28 = 0x1C,
    NCP1840_PWM_LEVEL29 = 0x1D,
    NCP1840_PWM_LEVEL30 = 0x1E,
    NCP1840_PWM_LEVEL31 = 0x1F,
    NCP1840_PWM_LEVEL32 = 0x20,
    NCP1840_PWM_LEVEL33 = 0x21,
    NCP1840_PWM_LEVEL34 = 0x22,
    NCP1840_PWM_LEVEL35 = 0x23,
    NCP1840_PWM_LEVEL36 = 0x24,
    NCP1840_PWM_LEVEL37 = 0x25,
    NCP1840_PWM_LEVEL38 = 0x26,
    NCP1840_PWM_LEVEL39 = 0x27,
    NCP1840_PWM_LEVEL40 = 0x28,
    NCP1840_PWM_LEVEL41 = 0x29,
    NCP1840_PWM_LEVEL42 = 0x2A,
    NCP1840_PWM_LEVEL43 = 0x2B,
    NCP1840_PWM_LEVEL44 = 0x2C,
    NCP1840_PWM_LEVEL45 = 0x2D,
    NCP1840_PWM_LEVEL46 = 0x2E,
    NCP1840_PWM_LEVEL47 = 0x2F,
    NCP1840_PWM_LEVEL48 = 0x30,
    NCP1840_PWM_LEVEL49 = 0x31,
    NCP1840_PWM_LEVEL50 = 0x32,
    NCP1840_PWM_LEVEL51 = 0x33,
    NCP1840_PWM_LEVEL52 = 0x34,
    NCP1840_PWM_LEVEL53 = 0x35,
    NCP1840_PWM_LEVEL54 = 0x36,
    NCP1840_PWM_LEVEL55 = 0x37,
    NCP1840_PWM_LEVEL56 = 0x38,
    NCP1840_PWM_LEVEL57 = 0x39,
    NCP1840_PWM_LEVEL58 = 0x3A,
    NCP1840_PWM_LEVEL59 = 0x3B,
    NCP1840_PWM_LEVEL60 = 0x3C,
    NCP1840_PWM_LEVEL61 = 0x3D,
    NCP1840_PWM_LEVEL62 = 0x3E,
    NCP1840_PWM_LEVEL63 = 0x3F,   // 100.000%
};

ERROR_T ncp1840_set_pwm(UINT8 dev, UINT8 ledmap, UINT8 pwm);

ERROR_T ncp1840_led_ctrl(UINT8 dev, UINT8 ledmap);

#endif //NCP1840_M
#endif //NCP1840_H
