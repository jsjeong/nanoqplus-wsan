/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief EnergyMicro LCD abstraction
 * @author Jongsoo Jeong (ETRI)
 * @date 2012. 7. 26.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "emlcd.h"
#ifdef EMLCD_M
#include <string.h>
#include "em_cmu.h"
#include "em_lcd.h"
#include "segmentlcd.h"
#include "pwr_mgr.h"
#include "arch.h"

void nos_emlcd_init(void)
{
    bool useBoost = false;

//    SegmentLCD_Init(false);
    const LCD_Init_TypeDef lcdInit = { true,
                                       lcdMuxOctaplex,
                                       lcdBiasOneFourth,
                                       lcdWaveLowPower,
                                       lcdVLCDSelVDD,
                                       lcdConConfVLCD };

    /* LCD Controller Prescaler (divide LFACLK / 64) */
    CMU_ClockDivSet(cmuClock_LCDpre, cmuClkDiv_64);
    /* LFACLK_LCDpre = 512 Hz */
    CMU_LCDClkFDIVSet(cmuClkDiv_1);
    /* Set FDIV=0, means 512/1 = 512 Hz */
    /* With octaplex mode, 512/16 => 32 Hz Frame Rate */

    /* Enable clock to LCD module */
    CMU_ClockEnable(cmuClock_LCD, true);

    /* Disable interrupts */
    LCD_IntDisable(0xFFFFFFFF);

    /* Initialize and enable LCD controller */
    LCD_Init(&lcdInit);

    /* Enable all display segments */
    LCD_SegmentRangeEnable(lcdSegment12_15, true);
    LCD_SegmentRangeEnable(lcdSegment16_19, true);
    LCD_SegmentRangeEnable(lcdSegment28_31, true);
    LCD_SegmentRangeEnable(lcdSegment32_35, true);
    LCD_SegmentRangeEnable(lcdSegment36_39, true);

    /* Enable boost if necessary */
    if (useBoost)
    {
        LCD_VBoostSet(lcdVBoostLevel3);
        LCD_VLCDSelect(lcdVLCDSelVExtBoost);
        CMU->LCDCTRL |= CMU_LCDCTRL_VBOOSTEN;
    }

    /* Turn all segments off */
    SegmentLCD_AllOff();

    LCD_SyncBusyDelay(0xFFFFFFFF);

    SegmentLCD_Symbol(LCD_SYMBOL_GECKO, 1);
    SegmentLCD_Symbol(LCD_SYMBOL_EFM32, 1);

    // Indicate power level.
    nos_emlcd_print_power_mode(nos_mcu_get_current_power_mode());
}

void nos_emlcd_print_string(char *str)
{
    if (str)
        SegmentLCD_Write(str);
}

void nos_emlcd_print_power_mode(UINT8 pm)
{
    if (pm == NOS_POWER_MODE_IDLE)
    {
        SegmentLCD_EnergyMode(0, 1);
        SegmentLCD_EnergyMode(1, 1);
        SegmentLCD_EnergyMode(2, 1);
        SegmentLCD_EnergyMode(3, 1);
        SegmentLCD_EnergyMode(4, 1);
    }
    else if (pm == NOS_POWER_MODE_SLEEP)
    {
        SegmentLCD_EnergyMode(0, 0);
        SegmentLCD_EnergyMode(1, 1);
        SegmentLCD_EnergyMode(2, 1);
        SegmentLCD_EnergyMode(3, 1);
        SegmentLCD_EnergyMode(4, 1);
    }
    else if (pm == NOS_POWER_MODE_DEEPSLEEP)
    {
        SegmentLCD_EnergyMode(0, 0);
        SegmentLCD_EnergyMode(1, 0);
        SegmentLCD_EnergyMode(2, 1);
        SegmentLCD_EnergyMode(3, 1);
        SegmentLCD_EnergyMode(4, 1);
    }
    else if (pm == NOS_POWER_MODE_STOP)
    {
        SegmentLCD_EnergyMode(0, 0);
        SegmentLCD_EnergyMode(1, 0);
        SegmentLCD_EnergyMode(2, 0);
        SegmentLCD_EnergyMode(3, 1);
        SegmentLCD_EnergyMode(4, 1);
    }
    else if (pm == NOS_POWER_MODE_SHUTOFF)
    {
        SegmentLCD_EnergyMode(0, 0);
        SegmentLCD_EnergyMode(1, 0);
        SegmentLCD_EnergyMode(2, 0);
        SegmentLCD_EnergyMode(3, 0);
        SegmentLCD_EnergyMode(4, 1);
    }
}

#endif /* EMLCD_M */
