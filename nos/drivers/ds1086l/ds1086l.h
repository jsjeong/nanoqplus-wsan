// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * DS1086L Oscillator driver
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 9.
 */

#ifndef DS1086L_H
#define DS1086L_H

#include "kconf.h"
#ifdef DS1086L_M
#include "nos_common.h"
#include "errorcodes.h"

enum
{
    DS1086L_JITTER_RATE_DIV_BY_8192 = 0,
    DS1086L_JITTER_RATE_DIV_BY_4096 = 1,
    DS1086L_JITTER_RATE_DIV_BY_2048 = 2,
};

enum
{
    DS1086L_JITTER_PER_0_5 = 0,
    DS1086L_JITTER_PER_1 = 1,
    DS1086L_JITTER_PER_2 = 2,
    DS1086L_JITTER_PER_4 = 4,
    DS1086L_JITTER_PER_8 = 7,
};

/**
 * Set prescaler register.
 *
 * @param id[in]        Chip ID
 * @param jit_rate[in]  Jitter Rate
 * @param jit_per[in]   Jitter Percentage
 * @param output[in]    Output Low or High-Z
 * @param div[in]       Prescaler Divider
 */
ERROR_T ds1086l_set_prescaler(UINT8 id, UINT8 jit_rate, UINT8 jit_per, BOOL output, UINT8 div);

/**
 * Get prescaler register.
 *
 * @param id[in]         Chip ID
 * @param jit_rate[out]  Jitter Rate
 * @param jit_per[out]   Jitter Percentage
 * @param output[out]    Output Low or High-Z
 * @param div[out]       Prescaler Divider
 */
ERROR_T ds1086l_get_prescaler(UINT8 id, UINT8 *jit_rate, UINT8 *jit_per, BOOL *output, UINT8 *div);

ERROR_T ds1086l_set_dac(UINT8 id, UINT16 value);
ERROR_T ds1086l_get_dac(UINT8 id, UINT16 *value);
ERROR_T ds1086l_set_offset(UINT8 id, UINT8 value);
ERROR_T ds1086l_get_offset(UINT8 id, UINT8 *value);
ERROR_T ds1086l_set_addr(UINT8 id, UINT8 value);
ERROR_T ds1086l_get_addr(UINT8 id, UINT8 *value);
ERROR_T ds1086l_get_range(UINT8 id, UINT8 *value);
ERROR_T ds1086l_write_eeprom(UINT8 id);

#endif //DS1086L_M
#endif //DS1086L_H
