// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * DS1086L Oscillator driver
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 9.
 */

#include "ds1086l.h"
#ifdef DS1086L_M
#include "ds1086l-interface.h"
#include "arch.h"

enum
{
    REG_PRESCALER = 0x02,
    REG_DAC = 0x08,
    REG_OFFSET = 0x0e,
    REG_ADDR = 0x0d,
    REG_RANGE = 0x37,
    CMD_WRITE_EE = 0x3f,
};

ERROR_T ds1086l_set_prescaler(UINT8 id, UINT8 jit_rate, UINT8 jit_per, BOOL output, UINT8 div)
{
    UINT8 buf[2];

    buf[0] = (jit_rate << 6) | (jit_per << 3) | (((output) ? 1 : 0) << 2) | ((div >> 2) & 3);
    buf[1] = (div << 6) & 0xc0;
    
    //printf("%s()-buf:%02X %02X\n", __FUNCTION__, buf[0], buf[1]);
    return ds1086l_write_byte(id, REG_PRESCALER, buf, 2);
}

ERROR_T ds1086l_get_prescaler(UINT8 id, UINT8 *jit_rate, UINT8 *jit_per, BOOL *output, UINT8 *div)
{
    UINT8 buf[2];
    
    ds1086l_read_byte(id, REG_PRESCALER, buf, 2);
    if (jit_rate) *jit_rate = (buf[0] >> 6) & 0x03;
    if (jit_per) *jit_per = (buf[0] >> 3) & 0x07;
    if (output) *output = (buf[0] >> 2) & 0x01;
    if (div) *div = ((buf[0] << 2) & 0x0c) | ((buf[1] >> 6) & 0x03);

    //printf("%s()-buf:%02X %02X\n", __FUNCTION__, buf[0], buf[1]);
    return ERROR_SUCCESS;
}

ERROR_T ds1086l_set_dac(UINT8 id, UINT16 value)
{
    UINT8 buf[2];
    ERROR_T err;

    value <<= 6;
    buf[0] = (value >> 8) & 0xFF; //MSB
    buf[1] = (value >> 0) & 0xFF; //LSB

    //printf("%s()-buf:%02X %02X\n", __FUNCTION__, buf[0], buf[1]);
    err = ds1086l_write_byte(id, REG_DAC, buf, 2);
    if (err == ERROR_SUCCESS)
        nos_delay_ms(1);
    
    return err;
}

ERROR_T ds1086l_get_dac(UINT8 id, UINT16 *value)
{
    ERROR_T err;
    UINT8 buf[2];

    if (value == NULL)
        return ERROR_INVALID_ARGS;
    
    err = ds1086l_read_byte(id, REG_DAC, buf, 2);
    
    if (err == ERROR_SUCCESS)
    {
        *value = ((buf[0] << 8) + buf[1]) >> 6;
    }

    //printf("%s()-buf:%02X %02X\n", __FUNCTION__, buf[0], buf[1]);
    return ERROR_SUCCESS;
}

ERROR_T ds1086l_set_offset(UINT8 id, UINT8 value)
{
    ERROR_T err;

    err = ds1086l_write_byte(id, REG_OFFSET, &value, 1);
    if (err == ERROR_SUCCESS)
        nos_delay_ms(1);

    //printf("%s()-value: 0x%02X\n", __FUNCTION__, value);
    return err;
}

ERROR_T ds1086l_get_offset(UINT8 id, UINT8 *value)
{
    ERROR_T err;
    
    if (value == NULL)
        return ERROR_INVALID_ARGS;

    err = ds1086l_read_byte(id, REG_OFFSET, value, 1);
    //printf("%s()-value:0x%02X\n", __FUNCTION__, *value);

    *value &= 0x1f;
    return err;
}

ERROR_T ds1086l_get_range(UINT8 id, UINT8 *value)
{
    ERROR_T err;
    
    if (value == NULL)
        return ERROR_INVALID_ARGS;
    
    err = ds1086l_read_byte(id, REG_RANGE, value, 1);

    //printf("%s()-value:0x%02X\n", __FUNCTION__, *value);

    *value &= 0x1f;
    return err;
}

ERROR_T ds1086l_write_eeprom(UINT8 id)
{
    return ds1086l_write_byte(id, CMD_WRITE_EE, NULL, 0);
}

#endif //DS1086L_M
