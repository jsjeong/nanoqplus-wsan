// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Apollo sensor driver
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 5. 11.
 */

#ifndef APOLLO_H
#define APOLLO_H

#include "kconf.h"
#ifdef APOLLO_M
#include "nos_common.h"
#include "errorcodes.h"

void apollo_init(UINT8 id);
ERROR_T apollo_set_power(UINT8 id, BOOL on);
ERROR_T apollo_get_integ_time(UINT8 id, UINT16 *t);
ERROR_T apollo_set_integ_time(UINT8 id, UINT16 t);
ERROR_T apollo_get_frame_blanking_time(UINT8 id, UINT16 *t);
ERROR_T apollo_set_frame_blanking_time(UINT8 id, UINT16 t);
ERROR_T apollo_get_start_row_addr(UINT8 id, UINT8 *addr);
ERROR_T apollo_set_start_row_addr(UINT8 id, UINT8 addr);
ERROR_T apollo_get_start_col_addr(UINT8 id, UINT8 *addr);
ERROR_T apollo_set_start_col_addr(UINT8 id, UINT8 addr);
ERROR_T apollo_get_row_count(UINT8 id, UINT8 *n);
ERROR_T apollo_set_row_count(UINT8 id, UINT8 n);
ERROR_T apollo_get_col_count(UINT8 id, UINT8 *n);
ERROR_T apollo_set_col_count(UINT8 id, UINT8 n);
ERROR_T apollo_get_adc(UINT8 id, UINT16 *mv_top, UINT16 *mv_bottom);
ERROR_T apollo_set_adc(UINT8 id, UINT16 mv_top, UINT16 mv_bottom);
ERROR_T apollo_get_gain(UINT8 id, UINT8 *ampgain, UINT8 *reso);
ERROR_T apollo_set_gain(UINT8 id, UINT8 ampgain, UINT8 reso);
ERROR_T apollo_capture_single_frame(UINT8 id, UINT16 *buf, UINT16 size);

#endif //APOLLO_M
#endif //APOLLO_H
