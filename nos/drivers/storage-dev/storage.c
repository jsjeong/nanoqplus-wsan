// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Storage Device Abstraction Layer
 *
 * @date 2012. 10. 30.
 * @author Jongsoo Jeong (ETRI)
 */

#include "storage.h"

#ifdef STORAGE_M
#include "platform.h"

STORAGE storage[STORAGE_DEV_CNT];

ERROR_T nos_storage_get_size(UINT8 id, UINT32 *size)
{
    ERROR_T err;

    if (id >= STORAGE_DEV_CNT)
        return ERROR_NOT_FOUND;
    
    *size = storage[id].size;
    return ERROR_SUCCESS;
}

ERROR_T nos_storage_read(UINT8 id, UINT32 addr, UINT8 *buf, UINT32 len)
{
    struct arg_storage_read arg;
    ERROR_T err;
    
    if (id >= STORAGE_DEV_CNT)
        return ERROR_NOT_FOUND;

    if (!buf || len == 0)
        return ERROR_INVALID_ARGS;
    
    arg.addr = addr;
    arg.buf = buf;
    arg.len = len;
    err = storage[id].command(id, STORAGE_CMD_READ, (void *) &arg);
    return err;
}

ERROR_T nos_storage_write(UINT8 id, UINT32 addr, const UINT8 *buf, UINT32 len)
{
    struct arg_storage_write arg;
    ERROR_T err;

    if (id >= STORAGE_DEV_CNT)
        return ERROR_NOT_FOUND;

    if (!buf || len == 0)
        return ERROR_INVALID_ARGS;
    
    arg.addr = addr;
    arg.buf = buf;
    arg.len = len;
    err = storage[id].command(id, STORAGE_CMD_WRITE, (void *) &arg);
    return err;
}

ERROR_T nos_storage_erase(UINT8 id, UINT32 addr, UINT32 len)
{
    struct arg_storage_erase arg;
    ERROR_T err;

    if (id >= STORAGE_DEV_CNT)
        return ERROR_NOT_FOUND;

    if (len == 0)
        return ERROR_INVALID_ARGS;
    
    arg.addr = addr;
    arg.len = len;
    err = storage[id].command(id, STORAGE_CMD_ERASE, (void *) &arg);
    return err;
}

ERROR_T nos_storage_erase_all(UINT8 id)
{
    ERROR_T err;

    if (id >= STORAGE_DEV_CNT)
        return ERROR_NOT_FOUND;

    err = storage[id].command(id, STORAGE_CMD_ERASE_ALL, NULL);
    return err;
}

#endif /* STORAGE_M */
