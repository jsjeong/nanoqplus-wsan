// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @date 2012. 10. 30.
 * @author Jongsoo Jeong (ETRI)
 * @brief Storage Device Abstraction Layer
 */

#ifndef STORAGE_H
#define STORAGE_H

#include "kconf.h"

#ifdef STORAGE_M
#include "nos_common.h"
#include "errorcodes.h"

enum storage_dev_cmd
{
    STORAGE_CMD_GET_SIZE,
    STORAGE_CMD_READ,
    STORAGE_CMD_WRITE,
    STORAGE_CMD_ERASE,
    STORAGE_CMD_ERASE_ALL,
};

struct storage_device
{
    UINT32 size;
    ERROR_T (*command)(UINT8 id, UINT8 cmd, void *args);
};
typedef struct storage_device STORAGE;

ERROR_T nos_storage_get_size(UINT8 id, UINT32 *size);

ERROR_T nos_storage_read(UINT8 id, UINT32 addr, UINT8 *data, UINT32 len);

struct arg_storage_read
{
    UINT32 addr;
    UINT8 *buf;
    UINT32 len;
};

ERROR_T nos_storage_write(UINT8 id, UINT32 addr, const UINT8 *data, UINT32 len);

struct arg_storage_write
{
    UINT32 addr;
    const UINT8 *buf;
    UINT32 len;
};

ERROR_T nos_storage_erase(UINT8 id, UINT32 addr, UINT32 len);

struct arg_storage_erase
{
    UINT32 addr;
    UINT32 len;
};

ERROR_T nos_storage_erase_all(UINT8 id);

#endif //STORAGE_M
#endif //~STORAGE_H
