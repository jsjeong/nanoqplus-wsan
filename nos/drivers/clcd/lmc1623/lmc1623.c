// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LMC1623 16x2 character LCD driver
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 4.
 */

#include "lmc1623.h"
#ifdef LMC1623_M
#include "nos_common.h"
#include "lmc1623-interface.h"
#include "clcd.h"
#include "arch.h"
#include <string.h>

#define LINE1 0x80
#define LINE2 0xC0

extern struct clcd_cmdset *clcd_cmd[];
extern void *clcd_state[];

static void lmc1623_on(UINT8 dev_id)
{
}

static void lmc1623_off(UINT8 dev_id)
{
}

static void write_control(UINT8 dev_id, char d)
{
    lmc1623_disable(dev_id);
    lmc1623_rs_control(dev_id);
    lmc1623_write(dev_id);
    nos_delay_us(1);
    lmc1623_write_byte(dev_id, d);
    nos_delay_us(1);
    lmc1623_enable(dev_id);
    nos_delay_us(1);
    lmc1623_disable(dev_id);
    nos_delay_us(1);
}

static void write_data(UINT8 dev_id, char d)
{
    lmc1623_disable(dev_id);
    lmc1623_rs_data(dev_id);
    lmc1623_write(dev_id);
    nos_delay_us(1);
    lmc1623_write_byte(dev_id, d);
    nos_delay_us(1);
    lmc1623_enable(dev_id);
    nos_delay_us(1);
    lmc1623_disable(dev_id);
    nos_delay_us(1);
}

static void display_update(UINT8 dev_id, struct lmc1623_state *l)
{
    UINT8 i;

    write_control(dev_id, LINE1);
    for (i = 0; i < 16; i++)
        write_data(dev_id, l->buffer[0][i]);
    
    write_control(dev_id, LINE2);
    for (i = 0; i < 16; i++)
        write_data(dev_id, l->buffer[1][i]);
}

static void lmc1623_set_line(UINT8 dev_id, UINT8 row, const char *str)
{
    struct lmc1623_state *l = (struct lmc1623_state *) clcd_state[dev_id];
    if (row >= 2)
        return;
    
    strncpy(l->buffer[row], str, 16);
    display_update(dev_id, l);
}

static void lmc1623_clear_line(UINT8 dev_id, UINT8 row)
{
    lmc1623_set_line(dev_id, row, "                ");
}

struct clcd_cmdset lmc1623_cmds =
{
    lmc1623_on,
    lmc1623_off,
    lmc1623_set_line,
    lmc1623_clear_line,
};

void lmc1623_init(UINT8 dev_id, struct lmc1623_state *state)
{
    clcd_cmd[dev_id] = (struct clcd_cmdset *) &lmc1623_cmds;
    clcd_state[dev_id] = (void *) state;

    write_control(dev_id, 0x38); //function set
    nos_delay_us(50);
    write_control(dev_id, 0x0C); //display on/off control
    nos_delay_us(50);
    write_control(dev_id, 0x01); //display clear
    nos_delay_us(160);
    write_control(dev_id, 0x06); //entry mode set
    
    lmc1623_clear_line(dev_id, 0);
    lmc1623_clear_line(dev_id, 1);
}

#endif //LMC1623_M
