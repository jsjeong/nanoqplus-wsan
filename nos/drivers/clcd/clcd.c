// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Character LCD abstraction layer
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 4.
 */

#include "clcd.h"
#ifdef CLCD_M
#include "platform.h"

struct clcd_cmdset *clcd_cmd[CLCD_DEV_CNT] = { 0 };
void *clcd_state[CLCD_DEV_CNT] = { 0 };

void clcd_on(UINT8 dev_id)
{
    if (dev_id < CLCD_DEV_CNT &&
        clcd_cmd[dev_id] &&
        clcd_cmd[dev_id]->on)
    {
        clcd_cmd[dev_id]->on(dev_id);
    }
}

void clcd_off(UINT8 dev_id)
{
    if (dev_id < CLCD_DEV_CNT &&
        clcd_cmd[dev_id] &&
        clcd_cmd[dev_id]->off)
    {
        clcd_cmd[dev_id]->off(dev_id);
    }
}

void clcd_set_line(UINT8 dev_id, UINT8 row, const char *str)
{
    if (dev_id < CLCD_DEV_CNT &&
        clcd_cmd[dev_id] &&
        clcd_cmd[dev_id]->set_line)
    {
        clcd_cmd[dev_id]->set_line(dev_id, row, str);
    }
}

void clcd_clear_line(UINT8 dev_id, UINT8 row)
{
    if (dev_id < CLCD_DEV_CNT &&
        clcd_cmd[dev_id] &&
        clcd_cmd[dev_id]->clear_line)
    {
        clcd_cmd[dev_id]->clear_line(dev_id, row);
    }
}
#endif //CLCD_M
