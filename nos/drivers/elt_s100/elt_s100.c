// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief TCC ELT S-100 Carbon Dioxide (CO2) Sensor
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 4.
 */

#include "elt_s100.h"

#ifdef ELT_S100_M
#include "sensor.h"
#include "elt_s100-interface.h"

extern struct sensor_cmdset *sensor_cmd[];

void elt_s100_on(UINT8 dev_id)
{
    elt_s100_interface_switch(dev_id, TRUE);
}

void elt_s100_off(UINT8 dev_id)
{
    elt_s100_interface_switch(dev_id, FALSE);
}

UINT16 elt_s100_read(UINT8 dev_id)
{
    UINT32 mv = elt_s100_interface_read_adc(dev_id);

    //ppm = 1250 * Vco2 - 625
    //ppm = 1.25 * Vco2 (mV) - 625

    return ((float) 1.25 * mv - 625);
}

const struct sensor_cmdset elt_s100_cmds =
{
    elt_s100_on,
    elt_s100_off,
    elt_s100_read,
};

void elt_s100_init(UINT8 dev_id)
{
    sensor_cmd[dev_id] = (struct sensor_cmdset *) &elt_s100_cmds;
}

#endif // ELT_S100_M
