// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * SHTxx integrated digital temperature and humidity sensor.
 *
 * @author Haeyong Kim (ETRI)
 *
 * @description SHTXX sensor is not compatible with I2C  (SHT1x, SHT7x).
 * Humidity is not compensated as temperature.
 * Mimimum sleep period after a measurement: temperature(sleep time>4200ms),
 * Humidity(sleep time>1000ms) (To prohibit sensor-self hitting problem)
 */

#include "sensor_shtxx.h"

#ifdef SHTXX_M
#include "platform.h"
#include "shtxx-interface.h"
#include "critical_section.h"
#include "arch.h"
#include "sensor.h"

extern struct sensor_cmdset *sensor_cmd[];

#define SHTXX_SCL_TICK(d)                       \
    do {                                        \
        nos_delay_us(5);                        \
        shtxx_set_scl(d, TRUE);                 \
        nos_delay_us(5);                        \
        shtxx_set_scl(d, FALSE);                \
    } while(0)

#define SHTXX_WRITE0(d)                         \
    do {                                        \
        shtxx_set_sda(d, FALSE);                \
        SHTXX_SCL_TICK(d);                      \
    } while(0)

#define SHTXX_WRITE1(d)                         \
    do {                                        \
        shtxx_set_sda(d, TRUE);                 \
        SHTXX_SCL_TICK(d);                      \
    } while(0)

#define SHTXX_READ(d,bit)                       \
    do {                                        \
        nos_delay_us(5);                        \
        shtxx_set_scl(d, TRUE);                 \
        bit = shtxx_get_sda(d);                 \
        nos_delay_us(5);                        \
        shtxx_set_scl(d, FALSE);                \
    } while(0)

enum shtxx_cmds
{
    SHTXX_MEASURE_TEMPERATURE = 0x03,
    SHTXX_MEASURE_HUMIDITY = 0x05,
    SHTXX_WRITE_STATUS_REG = 0x06,
    SHTXX_READ_STATUS_REG = 0x07,
    SHTXX_SOFT_RESET = 0x1E,	// must wait 11ms at least before next command
};

enum shtxx_mode
{
    RH8_TE12 = 0x01,   //fast, low-power mode
    RH12_TE14 = 0x00,  //normal mode
};

// Converting output to physical values
// RH: c1 + c2*out + c3 *out*out
#define RH_12BIT_C1          (-2.0468)
#define RH_12BIT_C2          (0.0367)
#define RH_12BIT_C3          (-0.0000015955)
#define RH_8BIT_C1           (-2.0468)
#define RH_8BIT_C2           (0.5872)
#define RH_8BIT_C3           (-0.00040845)

//Temp: d1+d2*out
#define TEMP_12BIT_D1C       (-39.64)
#define TEMP_12BIT_D1F       (-39.35)
#define TEMP_12BIT_D2C       (0.04)
#define TEMP_12BIT_D2F       (0.072)
#define TEMP_14BIT_D1C       (-39.64)
#define TEMP_14BIT_D1F       (-39.35)
#define TEMP_14BIT_D2C       (0.01)
#define TEMP_14BIT_D2F       (0.018)

struct _shtxx_status
{
    BOOL temp_active:1;    // Temperature sensor is set as on.
    BOOL hum_active:1;     // Humidity sensor is set as on.
    BOOL power:1;
    UINT8 temp_sensor_dev_id;
    UINT8 hum_sensor_dev_id;
};

#ifndef SHTXX_DEV_CNT
#error "SHTXX_DEV_CNT MUST be defined in platform.h."
#endif

static struct _shtxx_status shtxx[SHTXX_DEV_CNT] = { { 0 } };

static UINT8 get_chip_id(UINT8 dev_id)
{
    UINT8 i;
    for (i = 0; i < SHTXX_DEV_CNT; i++)
    {
        if (shtxx[i].temp_sensor_dev_id == dev_id ||
            shtxx[i].hum_sensor_dev_id == dev_id)
            return i;
    }
    
    return i;
}

static void shtxx_start(UINT8 chip_id)
{
    shtxx_set_out_sda(chip_id);
    shtxx_set_sda(chip_id, TRUE);
    shtxx_set_scl(chip_id, TRUE);
    shtxx_set_sda(chip_id, FALSE);
    nos_delay_us(5);
    shtxx_set_scl(chip_id, FALSE);
    nos_delay_us(5);
    shtxx_set_scl(chip_id, TRUE);
    shtxx_set_sda(chip_id, TRUE);
    nos_delay_us(5);
    shtxx_set_scl(chip_id, FALSE);
    nos_delay_us(5);	
}

// conncetion reset sequence.
static void shtxx_reset(UINT8 chip_id)
{
    UINT8 i;
    
    shtxx_set_out_sda(chip_id);
    
    for (i=0; i<9; ++i)
        SHTXX_WRITE1(chip_id);
    
    shtxx_start(chip_id);
}

static BOOL shtxx_tx_byte(UINT8 chip_id, UINT8 byte)
{
    UINT8 i;

    for (i = 0x80; i != 0; i >>= 1)
    {
        if ((byte & i) == 0)
            SHTXX_WRITE0(chip_id);
        else
            SHTXX_WRITE1(chip_id);
    }
    
    // read ACK
    shtxx_set_in_sda(chip_id);
    SHTXX_READ(chip_id, i);
    
    if (i)
        return FALSE;
    else
        return TRUE;
}

void shtxx_set_mode(UINT8 chip_id, UINT8 mode)
{
    if (mode == RH8_TE12)
    {
        // high seed, low power mode
        shtxx_start(chip_id);
        shtxx_tx_byte(chip_id, SHTXX_WRITE_STATUS_REG);
        shtxx_tx_byte(chip_id, RH8_TE12);
    }
    else
    {
        // normal
        shtxx_start(chip_id);
        shtxx_tx_byte(chip_id, SHTXX_WRITE_STATUS_REG);
        shtxx_tx_byte(chip_id, RH12_TE14);
    }
}

static BOOL shtxx_tx_command(UINT8 chip_id, UINT8 command)
{
    UINT8 i;
    
    NOS_ENTER_CRITICAL_SECTION();
    
    shtxx_start(chip_id); //SCK low, SDA high

    // 3 address bit (always '000' for now)
    shtxx_set_sda(chip_id, FALSE);
    SHTXX_SCL_TICK(chip_id);
    SHTXX_SCL_TICK(chip_id);
    SHTXX_SCL_TICK(chip_id);

    // 5 command bit
    for (i=0x10; i!=0; i >>= 1)
    {
        if ((command&i) == 0)
            SHTXX_WRITE0(chip_id);
        else
            SHTXX_WRITE1(chip_id);
    }

    // read ACK
    shtxx_set_in_sda(chip_id);
    SHTXX_READ(chip_id, i);

    NOS_EXIT_CRITICAL_SECTION();
    if (i)
        return FALSE;
    else
        return TRUE;
}

static UINT8 shtxx_compute_crc(UINT8 data, UINT8 crc)
{
    UINT16 result, i;
    UINT16 j = 0x9880;
    result = data^crc;
    result <<= 8;
    for (i = 0x8000; i!=0x80; i>>=1)
    {
        if ( (result&i)==i )
            result = result^j;
        j>>=1;
    }
    return (UINT8)result;
}

// return if CRC error
static UINT16 shtxx_read_measurement(UINT8 chip_id, UINT8 command)
{
    UINT8 read_bit;	//temporal use
    UINT8 i, measured_data_h, measured_data_l, crc;

    NOS_ENTER_CRITICAL_SECTION();	
    // read measured data_h
    measured_data_h = 0;
    for (i=0x80; i!=0; i >>= 1)
    {
        SHTXX_READ(chip_id, read_bit);
        if (read_bit)
            measured_data_h |= i;
    }
    shtxx_set_out_sda(chip_id);
    SHTXX_WRITE0(chip_id);	// send ACK
    shtxx_set_in_sda(chip_id);

    // read measured data_l
    measured_data_l = 0;
    for (i=0x80; i!=0; i >>= 1)
    {
        SHTXX_READ(chip_id, read_bit);
        if (read_bit)
            measured_data_l |= i;
    }
    shtxx_set_out_sda(chip_id);
    SHTXX_WRITE0(chip_id);	// send ACK
    shtxx_set_in_sda(chip_id);

    // read crc
    crc = 0;
    for (i=0x80; i!=0; i >>= 1)
    {
        SHTXX_READ(chip_id, read_bit);
        if (read_bit)
            crc |= i;
    }
    shtxx_set_out_sda(chip_id);
    SHTXX_WRITE1(chip_id);	// send end transmission

    NOS_EXIT_CRITICAL_SECTION();

    // check CRC
    read_bit = 0;
    read_bit = shtxx_compute_crc(command,read_bit);
    read_bit = shtxx_compute_crc(measured_data_h,read_bit);
    read_bit = shtxx_compute_crc(measured_data_l,read_bit);
    if( ((read_bit >> 7) & 0x01) == ((crc >> 0) & 0x01) && \
        ((read_bit >> 6) & 0x01) == ((crc >> 1) & 0x01) &&     \
        ((read_bit >> 5) & 0x01) == ((crc >> 2) & 0x01) &&     \
        ((read_bit >> 4) & 0x01) == ((crc >> 3) & 0x01) &&     \
        ((read_bit >> 3) & 0x01) == ((crc >> 4) & 0x01) &&     \
        ((read_bit >> 2) & 0x01) == ((crc >> 5) & 0x01) &&     \
        ((read_bit >> 1) & 0x01) == ((crc >> 6) & 0x01) &&     \
        ((read_bit >> 0) & 0x01) == ((crc >> 7) & 0x01) )
    {
        return( (UINT16)measured_data_l | (UINT16)(measured_data_h << 8));
    }
    else
    {
        return(0xFFFF);
    }
}

// return celsius*10
UINT16 shtxx_get_temperature(UINT8 dev_id)
{
    UINT16 i;
    float cel_temp;
    UINT8 chip_id;

    chip_id = get_chip_id(dev_id);
    if (chip_id >= SHTXX_DEV_CNT)
        return 0xFFFF;
    
    if (shtxx_tx_command(chip_id, SHTXX_MEASURE_TEMPERATURE))
    {
        // wait maximum 416ms (320ms*130%)
        for (i = 0; i < 41600; ++i)
        {
            if (!shtxx_get_sda(chip_id))	// if measurement is finished
            {
                i = shtxx_read_measurement(chip_id, SHTXX_MEASURE_TEMPERATURE);
                cel_temp = TEMP_14BIT_D1C + TEMP_14BIT_D2C * (float) i;
                return (UINT16) (((INT16) cel_temp) * 10);
            }
            nos_delay_us(9);
        }
    }
    
    // Sensor does not response.
    shtxx_reset(chip_id);
    return 0xFFFF;	
}

// return linear RH*10(%)
UINT16 shtxx_get_humidity(UINT8 dev_id)
{
    UINT16 i;
    float t_cal;
    UINT8 chip_id;

    chip_id = get_chip_id(dev_id);
    if (chip_id >= SHTXX_DEV_CNT)
        return 0xFFFF;

    if (shtxx_tx_command(chip_id, SHTXX_MEASURE_HUMIDITY))
    {
        // wait maximum 104ms (80ms*130%)
        for (i = 0; i < 10400; ++i)
        {
            if (!shtxx_get_sda(chip_id))	// if measurement is finished
            {
                // RH_linear = c1 +c2*(sensor_val) +c3(sensor_val^2)
                // RH_true = (temp-25)*(t1+t2*(sensor_val))+RH_linear
                i = shtxx_read_measurement(chip_id, SHTXX_MEASURE_HUMIDITY);
                t_cal = (float) (RH_12BIT_C1 +
                                 RH_12BIT_C2 * i +
                                 RH_12BIT_C3 * i * i); //for V4 (3alphabets) only.
                if (t_cal <= 0)
                    return 0;
                
                i = (UINT16)(t_cal*10);
                return (i >= 1000) ? 1000 : i;
            }
            else
            {
                nos_delay_us(9);
            }
        }
    }

    // Sensor does not response.
    shtxx_reset(chip_id);
    return 0xFFFF;
}

static void shtxx_control_power(UINT8 chip_id)
{
    if (shtxx[chip_id].power == TRUE)
    {
        if (shtxx[chip_id].temp_active == FALSE &&
            shtxx[chip_id].hum_active == FALSE)
        {
            shtxx_switch(chip_id, FALSE);
        }
    }
    else
    {
        if (shtxx[chip_id].temp_active == TRUE ||
            shtxx[chip_id].hum_active == TRUE)
        {
            shtxx_switch(chip_id, TRUE);
        }
    }

}

static void shtxx_temperature_sensor_on(UINT8 dev_id)
{
    UINT8 chip_id;

    chip_id = get_chip_id(dev_id);
    if (chip_id >= SHTXX_DEV_CNT)
        return;
    
    shtxx[chip_id].temp_active = TRUE;
    shtxx_control_power(chip_id);
}

static void shtxx_temperature_sensor_off(UINT8 dev_id)
{
    UINT8 chip_id;

    chip_id = get_chip_id(dev_id);
    if (chip_id >= SHTXX_DEV_CNT)
        return;

    shtxx[chip_id].temp_active = FALSE;
    shtxx_control_power(chip_id);
}

static void shtxx_humidity_sensor_on(UINT8 dev_id)
{
    UINT8 chip_id;

    chip_id = get_chip_id(dev_id);
    if (chip_id >= SHTXX_DEV_CNT)
        return;

    shtxx[chip_id].hum_active = TRUE;
    shtxx_control_power(chip_id);
}

static void shtxx_humidity_sensor_off(UINT8 dev_id)
{
    UINT8 chip_id;

    chip_id = get_chip_id(dev_id);
    if (chip_id >= SHTXX_DEV_CNT)
        return;

    shtxx[chip_id].hum_active = FALSE;
    shtxx_control_power(chip_id);
}

const struct sensor_cmdset shtxx_temp_cmds =
{
    shtxx_temperature_sensor_on,
    shtxx_temperature_sensor_off,
    shtxx_get_temperature,
};

const struct sensor_cmdset shtxx_hum_cmds =
{
    shtxx_humidity_sensor_on,
    shtxx_humidity_sensor_off,
    shtxx_get_humidity,
};

void shtxx_init(UINT8 shtxx_id, UINT8 temp_sensor_id, UINT8 hum_sensor_id)
{
    shtxx_switch(temp_sensor_id, TRUE);   // Turn on.
    nos_delay_ms(20);	//powerup time (at least 11ms)
    shtxx[shtxx_id].power = TRUE;
    shtxx[shtxx_id].hum_active = TRUE;
    shtxx[shtxx_id].temp_active = TRUE;
    shtxx[shtxx_id].hum_sensor_dev_id = hum_sensor_id;
    shtxx[shtxx_id].temp_sensor_dev_id = temp_sensor_id;

    // Sensor init (SCL low, SDA high)
    shtxx_set_scl(shtxx_id, FALSE);
    shtxx_set_sda(shtxx_id, TRUE);	//must be set for pull-up when RX
    shtxx_reset(shtxx_id);

    sensor_cmd[temp_sensor_id] = (struct sensor_cmdset *) &shtxx_temp_cmds;
    sensor_cmd[hum_sensor_id] = (struct sensor_cmdset *) &shtxx_hum_cmds;
}

#endif //SHTXX_M
