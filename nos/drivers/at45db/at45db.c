// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief AT45DB041B external flash memory driver
 * @author Jongsoo Jeong (ETRI)
 */

#include "at45db.h"

#ifdef AT45DB_M
#include "arch.h"
#include <string.h>
#include "heap.h"
#include "critical_section.h"

/** commands */
#define AT45DB_CMD_READ_BUF1        0xd4
#define AT45DB_CMD_READ_BUF2        0xd6
#define AT45DB_CMD_READ_CONT        0xe8
#define AT45DB_CMD_READ_PAGE        0xd2
#define AT45DB_CMD_WRITE_BUF1       0x84
#define AT45DB_CMD_WRITE_BUF2       0x87
#define AT45DB_CMD_WRITE_PAGE_BUF1  0x82
#define AT45DB_CMD_WRITE_PAGE_BUF2  0x85
#define AT45DB_CMD_FILL_BUF1        0x53
#define AT45DB_CMD_FILL_BUF2        0x55
#define AT45DB_CMD_FLUSH_BUF1       0x83
#define AT45DB_CMD_FLUSH_BUF2       0x86
#define AT45DB_CMD_QFLUSH_BUF1      0x88
#define AT45DB_CMD_QFLUSH_BUF2      0x89
#define AT45DB_CMD_COMP_BUF1        0x60
#define AT45DB_CMD_COMP_BUF2        0x61
#define AT45DB_CMD_REQ_STATUS       0xd7
#define AT45DB_CMD_ERASE_PAGE       0x81
#define AT45DB_CMD_ERASE_BLOCK      0x50

/** timings */
#define AT45DB_TIME_TRANSFER_US     300
#define AT45DB_TIME_TRANSFER_MS     1
#define AT45DB_TIME_ERASE_PROG      20
#define AT45DB_TIME_PROG            14
#define AT45DB_TIME_PAGE_ERASE      8
#define AT45DB_TIME_BLOCK_ERASE     12

#pragma pack(1)
struct _at45db_status_reg {
#ifdef BYTE_ORDER_LITTLE_ENDIAN
    UINT8 dontcare:2;   // don't care bits
    UINT8 density:4;    // MUST be '0111'
    UINT8 compare:1;    // Comparison result
    UINT8 ready:1;      // ready / ~busy
#else
    UINT8 ready:1;      // ready / ~busy
    UINT8 compare:1;    // Comparison result
    UINT8 density:4;    // MUST be '0111'
    UINT8 dontcare:2;   // don't care bits
#endif /* BYTE_ORDER_LITTLE_ENDIAN */
};
#pragma pack()
typedef struct _at45db_status_reg AT45DB_STATUS;
#define AT45DB_DENSITY_VALUE    7

INT8 at45db_read_cont(UINT16 page, UINT16 offset, UINT8 *data, UINT16 cnt);
INT8 at45db_read_page(UINT16 page, UINT16 offset, UINT8 *data, UINT16 cnt);
INT8 at45db_transfer_page_to_buf(UINT16 page, AT45DB_BUF_T buf);
INT8 at45db_comp_page_and_buf(UINT16 page, AT45DB_BUF_T buf, AT45DB_STATUS *stat);
INT8 at45db_read_buf(AT45DB_BUF_T buf, UINT16 offset, UINT8 *data, UINT16 cnt);
INT8 at45db_read_status(AT45DB_STATUS *stat);
INT8 at45db_write_buf(AT45DB_BUF_T buf, UINT16 offset, UINT8 *data, UINT16 cnt);
INT8 at45db_flush_buf_to_page(AT45DB_BUF_T buf, UINT16 page, BOOL bie);
INT8 at45db_write_page(UINT16 page, UINT16 offset, UINT8 *data, UINT16 cnt, AT45DB_BUF_T buf);
INT8 at45db_erase_page(UINT16 page);
INT8 at45db_erase_block(UINT8 block);
void at45db_write_address(UINT16 page, UINT16 offset);

INT8 at45db_storage_read(UINT32 addr, UINT8 *data, UINT16 len) {
    return at45db_read_cont(addr / AT45DB_PAGE_SIZE, 
            addr % AT45DB_PAGE_SIZE, (UINT8 *)data, len);
}

INT8 at45db_storage_write(UINT32 addr, UINT8 *data, UINT16 len) {
    if (addr >= AT45DB_SPACE) return AT45DB_ERR_ADDRESS;
    if (len == 0) return AT45DB_ERR_RANGE;
    if (addr + len > AT45DB_SPACE) return AT45DB_ERR_RANGE;

    while (len > 0) {
        UINT16 page = addr / AT45DB_PAGE_SIZE;
        UINT16 offset = addr % AT45DB_PAGE_SIZE;
        INT8 res;

        if ((offset > 0) || (len < AT45DB_PAGE_SIZE)) {
            UINT16 sz;

            res = at45db_transfer_page_to_buf(page, BUF1);
            if (res < 0) return res;

            sz = AT45DB_PAGE_SIZE - offset;
            if (sz >= len) sz = len;
            res = at45db_write_buf(BUF1, offset, data, sz);
            if (res < 0) return res;

            res = at45db_flush_buf_to_page(BUF1, page, TRUE);
            if (res < 0) return res;

            data += sz;
            len -= sz;
            addr += sz;
        }
        else {
            if (len > AT45DB_PAGE_SIZE * 8) {
                UINT8 i;
                res = at45db_erase_block(page / 8);
                if (res < 0) return res;

                for (i = 0; i < 8; i++) {
                    res = at45db_write_buf(BUF1, offset, data, AT45DB_PAGE_SIZE);
                    if (res < 0) return res;
                    res = at45db_flush_buf_to_page(BUF1, page, FALSE);
                    if (res < 0) return res;
                    data += AT45DB_PAGE_SIZE;
                    len -= AT45DB_PAGE_SIZE;
                    addr += AT45DB_PAGE_SIZE;
                }
            }
            else {
                res = at45db_write_page(page, offset, data, AT45DB_PAGE_SIZE, BUF1);
                if (res < 0) return res;
                data += AT45DB_PAGE_SIZE;
                len -= AT45DB_PAGE_SIZE;
                addr += AT45DB_PAGE_SIZE;
            }
        }
    }
    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_storage_erase(UINT32 addr, UINT16 len) {
    if (addr >= AT45DB_SPACE) return AT45DB_ERR_ADDRESS;
    if (len == 0) return AT45DB_ERR_RANGE;
    if (addr + len > AT45DB_SPACE) return AT45DB_ERR_RANGE;

    while (len > 0) {
        UINT16 page = addr / AT45DB_PAGE_SIZE;
        UINT16 offset = addr % AT45DB_PAGE_SIZE;
        INT8 res;

        if ((offset > 0) || (len < AT45DB_PAGE_SIZE)) {
            UINT16 sz;
            UINT8 *temp;

            res = at45db_transfer_page_to_buf(page, BUF1);
            if (res < 0) return res;

            sz = AT45DB_PAGE_SIZE - offset;
            if (sz >= len) sz = len;
            temp = nos_malloc(sz);
            if (!temp) return AT45DB_ERR_MEMORY;
            memset(temp, 0xff, sz);
            res = at45db_write_buf(BUF1, offset, temp, sz);
            if (res < 0) return res;
            nos_free(temp);

            res = at45db_flush_buf_to_page(BUF1, page, TRUE);
            if (res < 0) return res;

            len -= sz;
            addr += sz;
        }
        else {
            if (len > AT45DB_PAGE_SIZE * 8) {
                res = at45db_erase_block(page / 8);
                if (res < 0) return res;
                len -= AT45DB_PAGE_SIZE * 8;
                addr += AT45DB_PAGE_SIZE * 8;
            }
            else {
                res = at45db_erase_page(page);
                if (res < 0) return res;
                len -= AT45DB_PAGE_SIZE;
                addr += AT45DB_PAGE_SIZE;
            }
        }
    }
    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_storage_erase_all(void) {
    UINT8 i;
    for (i = 0; i < 256; i++) {
        INT8 res = at45db_erase_block(i);
        if (res < 0) return res;
    }
    return AT45DB_ERR_SUCCESS;
}

void at45db_write_address(UINT16 page, UINT16 offset) {
    UINT8 addr[3], i;

    addr[0] = (page >> 7) & 0x0f;
    addr[1] = ((page << 1) & 0xfe) | ((offset >> 8) & 0x01);
    addr[2] = offset & 0xff;

    for (i = 0; i < 3; i++)
        FLASH_SPI_TX(addr[i]);
}
        
INT8 at45db_read_cont(UINT16 page, UINT16 offset, UINT8 *data, UINT16 cnt) {
    UINT8 i;

    if (page >= AT45DB_NUMBER_OF_PAGE) return AT45DB_ERR_ADDRESS;
    if (offset >= AT45DB_PAGE_SIZE) return AT45DB_ERR_ADDRESS;
    if ((page * AT45DB_PAGE_SIZE + offset + cnt) > AT45DB_SPACE) 
        return AT45DB_ERR_RANGE;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    FLASH_SPI_TX(AT45DB_CMD_READ_CONT);
    at45db_write_address(page, offset);
    for (i = 0; i < 4; i++) FLASH_SPI_TX(0);
    for (i = 0; i < cnt; i++) FLASH_SPI_RX(&data[i]);
    FLASH_SPI_DISABLE();
    NOS_EXIT_CRITICAL_SECTION();

    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_read_page(UINT16 page, UINT16 offset, UINT8 *data, UINT16 cnt) {
    UINT8 i;

    if (page >= AT45DB_NUMBER_OF_PAGE) return AT45DB_ERR_ADDRESS;
    if (offset >= AT45DB_PAGE_SIZE) return AT45DB_ERR_ADDRESS;
    if (offset + cnt > AT45DB_PAGE_SIZE) return AT45DB_ERR_RANGE;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    FLASH_SPI_TX(AT45DB_CMD_READ_PAGE);
    at45db_write_address(page, offset);
    for (i = 0; i < 4; i++) FLASH_SPI_TX(0);
    for (i = 0; i < cnt; i++) FLASH_SPI_RX(&data[i]);
    FLASH_SPI_DISABLE();
    NOS_EXIT_CRITICAL_SECTION();

    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_transfer_page_to_buf(UINT16 page, AT45DB_BUF_T buf) {
    if (page >= AT45DB_NUMBER_OF_PAGE) return AT45DB_ERR_ADDRESS;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    if (buf == BUF1) FLASH_SPI_TX(AT45DB_CMD_FILL_BUF1);
    else if (buf == BUF2) FLASH_SPI_TX(AT45DB_CMD_FILL_BUF2);
    else {
        FLASH_SPI_DISABLE();
        NOS_EXIT_CRITICAL_SECTION();
        return AT45DB_ERR_GENERAL;
    }
    at45db_write_address(page, 0);
    FLASH_SPI_DISABLE();

    FLASH_WAIT_UNTIL_READY(AT45DB_TIME_TRANSFER_MS);
    NOS_EXIT_CRITICAL_SECTION();

    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_comp_page_and_buf(UINT16 page, AT45DB_BUF_T buf, AT45DB_STATUS *stat) {
    if (page >= AT45DB_NUMBER_OF_PAGE) return AT45DB_ERR_ADDRESS;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    if (buf == BUF1) FLASH_SPI_TX(AT45DB_CMD_COMP_BUF1);
    else if (buf == BUF2) FLASH_SPI_TX(AT45DB_CMD_COMP_BUF2);
    else {
        FLASH_SPI_DISABLE();
        NOS_EXIT_CRITICAL_SECTION();
        return AT45DB_ERR_GENERAL;
    }
    at45db_write_address(page, 0);
    FLASH_SPI_DISABLE();

    nos_delay_us(AT45DB_TIME_TRANSFER_US);
    do {
        if (!at45db_read_status(stat)) {
            NOS_EXIT_CRITICAL_SECTION();
            return AT45DB_ERR_GENERAL;
        }
    } while (!stat->ready);
    NOS_EXIT_CRITICAL_SECTION();

    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_read_buf(AT45DB_BUF_T buf, UINT16 offset, UINT8 *data, UINT16 cnt) {
    UINT8 i;
    if (offset >= AT45DB_PAGE_SIZE) return AT45DB_ERR_ADDRESS;
    if (offset + cnt > AT45DB_PAGE_SIZE) return AT45DB_ERR_RANGE;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    if (buf == BUF1) FLASH_SPI_TX(AT45DB_CMD_READ_BUF1);
    else if (buf == BUF2) FLASH_SPI_TX(AT45DB_CMD_READ_BUF2);
    else {
        FLASH_SPI_DISABLE();
        NOS_EXIT_CRITICAL_SECTION();
        return AT45DB_ERR_GENERAL;
    }
    at45db_write_address(0, offset);
    for (i = 0; i < cnt; i++) FLASH_SPI_RX(&data[i]);
    FLASH_SPI_DISABLE();
    NOS_EXIT_CRITICAL_SECTION();

    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_read_status(AT45DB_STATUS *stat) {
    UINT8 s;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    FLASH_SPI_TX(AT45DB_CMD_REQ_STATUS);
    FLASH_SPI_RX(&s);
    FLASH_SPI_DISABLE();
    NOS_EXIT_CRITICAL_SECTION();
    memcpy((void *)stat, (void *)&s, 1);
    if (stat->density == AT45DB_DENSITY_VALUE) return AT45DB_ERR_SUCCESS;
    else return AT45DB_ERR_GENERAL;
}

INT8 at45db_write_buf(AT45DB_BUF_T buf, UINT16 offset, UINT8 *data, UINT16 cnt) {
    UINT8 i;
    if (offset >= AT45DB_PAGE_SIZE) return AT45DB_ERR_ADDRESS;
    if (offset + cnt > AT45DB_PAGE_SIZE) return AT45DB_ERR_RANGE;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    if (buf == BUF1) FLASH_SPI_TX(AT45DB_CMD_WRITE_BUF1);
    else if (buf == BUF2) FLASH_SPI_TX(AT45DB_CMD_WRITE_BUF2);
    else {
        FLASH_SPI_DISABLE();
        NOS_EXIT_CRITICAL_SECTION();
        return AT45DB_ERR_GENERAL;
    }
    at45db_write_address(0, offset);
    for (i = 0; i < cnt; i++) FLASH_SPI_TX(data[i]);
    FLASH_SPI_DISABLE();
    NOS_EXIT_CRITICAL_SECTION();
    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_flush_buf_to_page(AT45DB_BUF_T buf, UINT16 page, BOOL bie) {
    if (page >= AT45DB_NUMBER_OF_PAGE) return AT45DB_ERR_ADDRESS;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    if (buf == BUF1) {
        if (bie) FLASH_SPI_TX(AT45DB_CMD_FLUSH_BUF1);
        else FLASH_SPI_TX(AT45DB_CMD_QFLUSH_BUF1);
    }
    else if (buf == BUF2) {
        if (bie) FLASH_SPI_TX(AT45DB_CMD_FLUSH_BUF2);
        else FLASH_SPI_TX(AT45DB_CMD_QFLUSH_BUF2);
    }
    else {
        FLASH_SPI_DISABLE();
        NOS_EXIT_CRITICAL_SECTION();
        return AT45DB_ERR_GENERAL;
    }
    at45db_write_address(page, 0);
    
    FLASH_SPI_DISABLE();

    if (bie) FLASH_WAIT_UNTIL_READY(AT45DB_TIME_ERASE_PROG);
    else FLASH_WAIT_UNTIL_READY(AT45DB_TIME_PROG);
    NOS_EXIT_CRITICAL_SECTION();
    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_erase_page(UINT16 page) {
    if (page >= AT45DB_NUMBER_OF_PAGE) return AT45DB_ERR_ADDRESS;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    FLASH_SPI_TX(AT45DB_CMD_ERASE_PAGE);
    at45db_write_address(page, 0);
    FLASH_SPI_DISABLE();

    FLASH_WAIT_UNTIL_READY(AT45DB_TIME_PAGE_ERASE);
    NOS_EXIT_CRITICAL_SECTION();
    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_erase_block(UINT8 block) {

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    FLASH_SPI_TX(AT45DB_CMD_ERASE_BLOCK);
    at45db_write_address(block << 3, 0);
    FLASH_SPI_DISABLE();

    FLASH_WAIT_UNTIL_READY(AT45DB_TIME_BLOCK_ERASE);
    NOS_EXIT_CRITICAL_SECTION();

    return AT45DB_ERR_SUCCESS;
}

INT8 at45db_write_page(UINT16 page, UINT16 offset, UINT8 *data, UINT16 cnt, AT45DB_BUF_T buf) {
    UINT8 i;
    if (page >= AT45DB_NUMBER_OF_PAGE) return AT45DB_ERR_ADDRESS;
    if (offset >= AT45DB_PAGE_SIZE) return AT45DB_ERR_ADDRESS;
    if (offset + cnt > AT45DB_PAGE_SIZE) return AT45DB_ERR_RANGE;

    NOS_ENTER_CRITICAL_SECTION();
    FLASH_SPI_ENABLE();
    if (buf == BUF1) FLASH_SPI_TX(AT45DB_CMD_WRITE_PAGE_BUF1);
    else if (buf == BUF2) FLASH_SPI_TX(AT45DB_CMD_WRITE_PAGE_BUF2);
    else {
        FLASH_SPI_DISABLE();
        NOS_EXIT_CRITICAL_SECTION();
        return AT45DB_ERR_GENERAL;
    }
    at45db_write_address(page, offset);
    for (i = 0; i < cnt; i++) FLASH_SPI_TX(data[i]);
    FLASH_SPI_DISABLE();

    FLASH_WAIT_UNTIL_READY(AT45DB_TIME_ERASE_PROG);
    NOS_EXIT_CRITICAL_SECTION();
    return AT45DB_ERR_SUCCESS;
}

#endif //AT45DB_M
