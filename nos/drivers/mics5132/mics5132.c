// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief MiCS-5132 Automotive Pollution Gas (CO) Sensor
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 4.
 */

#include "mics5132.h"
#ifdef MICS5132_M
#include "sensor.h"
#include "mics5132-interface.h"
#include <math.h>

extern struct sensor_cmdset *sensor_cmd[];

void mics5132_on(UINT8 dev_id)
{
    mics5132_interface_switch(dev_id, TRUE);
}

void mics5132_off(UINT8 dev_id)
{
    mics5132_interface_switch(dev_id, FALSE);
}

UINT16 mics5132_read(UINT8 dev_id)
{
    UINT32 mv = mics5132_interface_read_adc(dev_id);

    //Rs =  20 / Vco * (5 - Vco);
    //Rs_R0 = Rs / 24;
    //ppm = pow(10.0, -3.0709*Rs_R0 + 3.4843);
    return pow(10.0, 6.0433 - (12795.0 / (float) mv));
}

const struct sensor_cmdset mics5132_cmds =
{
    mics5132_on,
    mics5132_off,
    mics5132_read,
};

void mics5132_init(UINT8 dev_id)
{
    sensor_cmd[dev_id] = (struct sensor_cmdset *) &mics5132_cmds;
}

#endif // MICS5132_M
