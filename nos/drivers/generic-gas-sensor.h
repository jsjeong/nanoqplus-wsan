/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 11. 29.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#ifndef GAS_H_
#define GAS_H_

#include "kconf.h"
#ifdef SENSOR_GAS_M
#include "platform.h"

#ifdef GENERIC_ADC_GAS_SENSOR_M

#if !defined PORT_GAS_SENSOR_POWER_SW || \
    !defined PIN_GAS_SENSOR_POWER_SW || \
    !defined PORT_ADC_GAS_SENSOR_CHANNEL || \
    !defined PIN_ADC_GAS_SENSOR_CHANNEL
#error "PORT_GAS_SENSOR_POWER_SW, PIN_GAS_SENSOR_POWER_SW, and ADC_GAS_SENSOR_CHANNEL must be defined in platform.h in your mote's platform directory (e.g., $NOS_HOME/nos/platform/xxx) to use a generic ADC gas sensor."
#endif

#include "generic_analog_sensor.h"
#define nos_gas_sensor_init() \
    generic_analog_sensor_init(PORT_GAS_SENSOR_POWER_SW, \
            PIN_GAS_SENSOR_POWER_SW, \
            PORT_ADC_GAS_SENSOR_CHANNEL, \
            PIN_ADC_GAS_SENSOR_CHANNEL)

#define nos_gas_sensor_power_on() \
    generic_analog_sensor_on(PORT_GAS_SENSOR_POWER_SW, \
            PIN_GAS_SENSOR_POWER_SW)

#define nos_gas_sensor_power_off() \
    generic_analog_sensor_off(PORT_GAS_SENSOR_POWER_SW, \
            PIN_GAS_SENSOR_POWER_SW)

#define nos_gas_sensor_get_data() \
    generic_analog_sensor_get_data(PIN_ADC_GAS_SENSOR_CHANNEL)

#elif defined OCTACOMM_ENV_SENSOR_M
#include "octacomm-env-sensor-module.h"
#define nos_gas_sensor_init()       octacomm_gas_sensor_init()
#define nos_gas_sensor_power_on()   octacomm_gas_sensor_power_on()
#define nos_gas_sensor_power_off()  octacomm_gas_sensor_power_off()
#define nos_gas_sensor_get_data()   octacomm_gas_sensor_get_data()

#elif defined PLATFORM_SPECIFIC_GAS_SENSOR_M
#include "platform_gas_sensor.h"
#define nos_gas_sensor_init()       platform_init_gas_sensor()
#define nos_gas_sensor_power_on()   platform_gas_sensor_on()
#define nos_gas_sensor_power_off()  platform_gas_sensor_off()
#define nos_gas_sensor_get_data()   platform_get_gas_value()
#endif
#endif /* SENSOR_GAS_M */
#endif /* GAS_H_ */
