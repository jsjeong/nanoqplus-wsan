/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 12. 5.
 */

/*
 * $LastChangedDate$
 * $Id$
 */


#ifndef GENERIC_RELAY_H_
#define GENERIC_RELAY_H_

#include "kconf.h"

#ifdef RELAY_M

#ifdef OCTACOMM_RELAY_M
#include "octacomm-relay-module.h"
#define nos_relay_init()        octacomm_relay_init()
#define nos_relay_on(id)        octacomm_relay_on(id)
#define nos_relay_off(id)       octacomm_relay_off(id)
#define nos_relay_toggle(id)    octacomm_relay_toggle(id)
#define nos_relay_get_state(id) octacomm_relay_get_state(id)

#elif defined KEP_M
#include "kep.h"
#define nos_relay_init()		kep_relay_init()
#define nos_relay_on(id)		kep_relay_on(id)
#define nos_relay_off(id)		kep_relay_off(id)
#define nos_relay_toggle(id)	kep_relay_toggle(id)
#define nos_relay_get_state(id)	kep_relay_get_state(id)

#elif defined PLATFORM_SPECIFIC_ACTUATOR_M
#define nos_relay_init()
#define nos_relay_on(id)
#define nos_relay_off(id)
#define nos_relay_toggle(id)
#define nos_relay_get_state(id)

#endif

#endif /* ACTUATOR_M */
#endif /* GENERIC_RELAY_H_ */
