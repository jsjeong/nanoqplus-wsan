// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Generic humidity sensor interface
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 4.
 * @description Link generic API functions to the device driver which is
 * corresponding to the device ID.
 */

#include "generic-humidity-sensor.h"

#ifdef SENSOR_HUMIDITY_M
#include "platform.h"

#ifdef SHTXX_M
#include "sensor_shtxx.h"
#endif //SHTXX_M

void nos_humidity_sensor_on(UINT8 id)
{
#ifdef SHTXX_M
    /*
     * SHTxx sensor driver is not separated into the driver and the
     * interface. So it does not require passing the device ID (id).
     */
    shtxx_set_humidity_power(TRUE);
    return;
#endif //SHTXX_M
}

void nos_humidity_sensor_off(UINT8 id)
{
#ifdef SHTXX_M
    shtxx_set_humidity_power(FALSE);
    return;
#endif //SHTXX_M
}

UINT16 nos_humidity_sensor_read(UINT8 id)
{
#ifdef SHTXX_M
    return shtxx_get_humidity();
#endif //SHTXX_M

    return 0xffff;
}

#endif //SENSOR_HUMIDITY_M
