// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief M25PX16 16Mbit Serial Flash Memory Driver
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 4. 30.
 */

#include "m25px16.h"

#ifdef M25PX16
#include "m25px16-interface.h"
#include "storage.h"
#include "platform.h"
#include "heap.h"
#include <string.h>

extern STORAGE storage[STORAGE_DEV_CNT];

enum m25px16_cmd
{
    CMD_WRITE_ENABLE             = 0x06,
    CMD_WRITE_DISABLE            = 0x04,
    CMD_READ_ID                  = 0x9F,
    CMD_READ_STATUS              = 0x05,
    CMD_WRITE_STATUS             = 0x01,
    CMD_WRITE_LOCK               = 0xE5,
    CMD_READ_LOCK                = 0xE8,
    CMD_READ                     = 0x03,
    CMD_DUAL_OUTPUT_READ         = 0x3B,
    CMD_READ_OTP                 = 0x4B,
    CMD_PROGRAM_OTP              = 0x42,
    CMD_PROGRAM_PAGE             = 0x02,
    CMD_DUAL_INPUT_FAST_PROGRAM  = 0xA2,
    CMD_SUBSECTOR_ERASE          = 0x20,
    CMD_SECTOR_ERASE             = 0xD8,
    CMD_BULK_ERASE               = 0xC7,
    CMD_DEEP_POWER_DOWN          = 0xB9,
    CMD_RELEASE_FROM_DPD         = 0xAB,
};

enum m25px16_addr
{
    ADDR_START = 0x000000L,
    ADDR_END = 0x1fffffL,
};

enum m25px16_status
{
    STATUS_SRWD = 7,
    STATUS_TOPBTM = 5,
    STATUS_BLOCK_PROTECTS = 2,
    STATUS_WEL = 1,
    STATUS_WIP = 0,
};

static void sleep(UINT8 id)
{
    UINT8 st;

    m25px16_request_cs(id);
    m25px16_spi(id, CMD_READ_STATUS, NULL);
    do {
        m25px16_spi(id, 0, &st);
        
        if (st == 0xff)
            break; //already sleeping.
    } while(_IS_SET(st, STATUS_WIP));
    m25px16_release_cs(id);

    if (st != 0xff)
    {
        m25px16_request_cs(id);
        m25px16_spi(id, CMD_DEEP_POWER_DOWN, NULL);
        m25px16_release_cs(id);
    }
}

static void wakeup(UINT8 id)
{
    m25px16_request_cs(id);
    m25px16_spi(id, CMD_RELEASE_FROM_DPD, NULL);
    m25px16_release_cs(id);
}

ERROR_T m25px16_read(UINT8 id, void *args)
{
    struct arg_storage_read *param = (struct arg_storage_read *) args;
    UINT32 i;

    if (param->addr > ADDR_END || param->len == 0)
        return ERROR_INVALID_ARGS;

    wakeup(id);
    
    m25px16_request_cs(id);
    m25px16_spi(id, CMD_READ, NULL);
    m25px16_spi(id, (param->addr >> 16) & 0xff, NULL);
    m25px16_spi(id, (param->addr >> 8) & 0xff, NULL);
    m25px16_spi(id, (param->addr >> 0) & 0xff, NULL);

    for (i = 0; i < param->len && param->addr + i <= ADDR_END; i++)
    {
        m25px16_spi(id, 0, &param->buf[i]);
    }
    m25px16_release_cs(id);

    sleep(id);
    return ERROR_SUCCESS;
}

static UINT8 get_status(UINT8 id)
{
    UINT8 st;
    
    m25px16_request_cs(id);
    m25px16_spi(id, CMD_READ_STATUS, NULL);
    m25px16_spi(id, 0, &st);
    m25px16_release_cs(id);

    return st;
}

static void enable_write(UINT8 id, BOOL enable)
{
    m25px16_request_cs(id);
    m25px16_spi(id, (enable) ? CMD_WRITE_ENABLE : CMD_WRITE_DISABLE, NULL);
    m25px16_release_cs(id);
}

ERROR_T m25px16_write(UINT8 id, void *args)
{
    struct arg_storage_write *param = (struct arg_storage_write *) args;
    UINT8 st;
    UINT32 i, subsector_addr, pos;
    UINT8 *subsector;

    if (param->addr > ADDR_END || param->len == 0)
        return ERROR_INVALID_ARGS;

    subsector = nos_malloc(0x1000);
    if (!subsector)
        return ERROR_NOT_ENOUGH_MEMORY;

    pos = param->addr;
    
    for (subsector_addr = (param->addr / 0x1000) * 0x1000;
         subsector_addr < param->addr + param->len && subsector_addr < ADDR_END;
         subsector_addr += 0x1000)
    {
        //Fetch a subsector.
        struct arg_storage_read read_arg;
        UINT32 next_pos;

        read_arg.addr = subsector_addr;
        read_arg.len = 0x1000;
        read_arg.buf = subsector;
        m25px16_read(id, &read_arg);

        //printf("read done\n");
        
        //TODO ICCARM complains _MIN.
        //next_pos = _MIN(subsector_addr + 0x1000, param->addr + param->len);
        next_pos = (subsector_addr + 0x1000 > param->addr + param->len) ?
            param->addr + param->len : subsector_addr + 0x1000;

        memcpy(&subsector[pos - subsector_addr],
               &param->buf[pos - param->addr],
               next_pos - pos);

        wakeup(id);

        //printf("subsector erase\n");
        
        enable_write(id, TRUE);
        m25px16_request_cs(id);
        m25px16_spi(id, CMD_SUBSECTOR_ERASE, NULL);
        m25px16_spi(id, (pos >> 16) & 0xff, NULL);
        m25px16_spi(id, (pos >> 8) & 0xff, NULL);
        m25px16_spi(id, (pos >> 0) & 0xff, NULL);
        m25px16_release_cs(id);

        pos = subsector_addr;
        while (pos < subsector_addr + 0x1000)
        {
            if (subsector[pos - subsector_addr] == 0xff)
            {
                pos++;
                continue;
            }

            //printf("wait until write done\n");
        
            m25px16_request_cs(id);
            m25px16_spi(id, CMD_READ_STATUS, NULL);
            do {
                m25px16_spi(id, 0, &st);
            } while(_IS_SET(st, STATUS_WIP));
            m25px16_release_cs(id);

            //printf("write done\n");

            enable_write(id, TRUE);            

            m25px16_request_cs(id);
            m25px16_spi(id, CMD_PROGRAM_PAGE, NULL);
            m25px16_spi(id, (pos >> 16) & 0xff, NULL);
            m25px16_spi(id, (pos >> 8) & 0xff, NULL);
            m25px16_spi(id, (pos >> 0) & 0xff, NULL);

            for (i = 0; i < 256 && pos < subsector_addr + 0x1000; i++)
            {
                //printf("[%x] %02X\n", pos, subsector[pos - subsector_addr]);
                
                m25px16_spi(id, subsector[pos - subsector_addr], NULL);
                pos++;
                
                if (subsector[pos - subsector_addr] == 0xff)
                {
                    //printf("skip 0x%x~\n", pos);
                    pos++;
                    break;
                }
            }
            m25px16_release_cs(id);

            m25px16_request_cs(id);
            m25px16_spi(id, CMD_READ_STATUS, NULL);
            do {
                m25px16_spi(id, 0, &st);
            } while(_IS_SET(st, STATUS_WIP));
            m25px16_release_cs(id);

            //printf("%d bytes written\n", i + 1);
        }
    }
    
    nos_free(subsector);
    sleep(id);
    return ERROR_SUCCESS;
}

ERROR_T m25px16_erase(UINT8 id, void *args)
{
    struct arg_storage_erase *param = (struct arg_storage_erase *) args;
    UINT8 st, lock;
    UINT32 pos, next_pos;

    if (param->addr > ADDR_END)
        return ERROR_INVALID_ARGS;

    wakeup(id);
    
    st = get_status(id);
    //printf("st:%02X\n", st);
    if (_IS_SET(st, STATUS_WIP) || _IS_SET(st, STATUS_WEL))
        return ERROR_BUSY;

    
    pos = param->addr;

    while (pos <= ADDR_END &&
           pos < param->addr + param->len)
    {
        UINT32 next_sector, next_subsector;

        m25px16_request_cs(id);
        m25px16_spi(id, CMD_READ_LOCK, NULL);
        m25px16_spi(id, (pos >> 16) & 0xff, NULL);
        m25px16_spi(id, (pos >> 8) & 0xff, NULL);
        m25px16_spi(id, (pos >> 0) & 0xff, NULL);
        m25px16_spi(id, 0, &lock);
        m25px16_release_cs(id);
        
        //printf("pos:%u(l:%02X)", pos, lock);
        
        next_sector = ((pos / 0x10000) + 1) * 0x10000;
        next_subsector = ((pos / 0x1000) + 1) * 0x1000;
        
        if (pos % 0x10000 == 0 &&
            next_sector <= param->addr + param->len)
        {
            next_pos = next_sector;
        }
        else if (next_subsector <= param->addr + param->len)
        {
            next_pos = next_subsector;
        }
        else
        {
            next_pos = param->addr + param->len;
        }

        //printf(",next_pos:%u", next_pos);
        
        if (next_pos - pos == 0x10000)
        {
            //printf(",sector erase");
            
            //Do sector erase.
            enable_write(id, TRUE);

            m25px16_request_cs(id);    
            m25px16_spi(id, CMD_SECTOR_ERASE, NULL);
            m25px16_spi(id, (pos >> 16) & 0xff, NULL);
            m25px16_spi(id, (pos >> 8) & 0xff, NULL);
            m25px16_spi(id, (pos >> 0) & 0xff, NULL);
            m25px16_release_cs(id);
        }
        else if (next_pos - pos == 0x1000)
        {
            //printf(",subsector erase");
            
            //Do subsector erase.
            enable_write(id, TRUE);

            m25px16_request_cs(id);
            m25px16_spi(id, CMD_SUBSECTOR_ERASE, NULL);
            m25px16_spi(id, (pos >> 16) & 0xff, NULL);
            m25px16_spi(id, (pos >> 8) & 0xff, NULL);
            m25px16_spi(id, (pos >> 0) & 0xff, NULL);
            m25px16_release_cs(id);
        }
        else
        {
            UINT32 i;
            struct arg_storage_write write_arg;
            UINT8 *buf;

            write_arg.addr = pos;
            write_arg.len = next_pos - pos;
            buf = nos_malloc(write_arg.len);
            if (!buf)
                return ERROR_NOT_ENOUGH_MEMORY;

            memset(buf, 0xff, write_arg.len);
            write_arg.buf = buf;

            //printf(",erase(st:%02X)", get_status(id));
            
            m25px16_write(id, &write_arg);

            nos_free(buf);
        }
        //printf("\n");
        pos = next_pos;
    }
    
    sleep(id);
    return ERROR_SUCCESS;
}

ERROR_T m25px16_erase_all(UINT8 id)
{
    UINT8 st;

    wakeup(id);
    
    st = get_status(id);
    if (_IS_SET(st, STATUS_WIP) || _IS_SET(st, STATUS_WEL))
        return ERROR_BUSY;

    enable_write(id, TRUE);

    m25px16_request_cs(id);
    m25px16_spi(id, CMD_BULK_ERASE, NULL);
    m25px16_release_cs(id);

    sleep(id);
    return ERROR_SUCCESS;
}

ERROR_T m25px16_cmd(UINT8 id, UINT8 cmd, void *args)
{
    if (cmd == STORAGE_CMD_READ)
        return m25px16_read(id, args);
    else if (cmd == STORAGE_CMD_WRITE)
        return m25px16_write(id, args);
    else if (cmd == STORAGE_CMD_ERASE)
        return m25px16_erase(id, args);
    else if (cmd == STORAGE_CMD_ERASE_ALL)
        return m25px16_erase_all(id);
    else
        return ERROR_INVALID_ARGS;
}

void m25px16_init(UINT8 id)
{
    storage[id].size = ADDR_END + 1;
    storage[id].command = m25px16_cmd;

    wakeup(id);
    enable_write(id, FALSE);
    sleep(id);
}

#endif //M25PX16
