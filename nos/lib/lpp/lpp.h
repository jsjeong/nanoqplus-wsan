// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LPP (Low Power Probing) Core
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 7. 2.
 */

#ifndef LPP_H
#define LPP_H

#include "kconf.h"

#ifdef LIB_LPP_M
#include "nos_common.h"
#include "errorcodes.h"

/**
 * Initialize an LPP to work between a WPAN device and a MAC protocol.
 */
void lpp_init(void);

/**
 * Pass a received packet to the LPP layer.
 */
BOOL lpp_receive_control_packet(UINT8 *pkt, UINT8 pkt_len);

/**
 * Sleep using LPP.
 */
ERROR_T lpp_sleep(void);

/**
 * Listen and start to response probe packets until timeout.
 */
ERROR_T lpp_listen(void);

/**
 * Stop LPP from any LPP states.
 */
ERROR_T lpp_stop(void);

/**
 * Set a LPP listen timeout.
 *
 * @param timeout_sec: Timeout in unit of seconds.
 */
ERROR_T lpp_set_listen_timeout(UINT16 timeout_sec);

/**
 * Set a LPP probe interval.
 *
 * @param probe_interval_sec: Probe interval in unit of seconds.
 */
ERROR_T lpp_set_probe_interval(UINT16 interval_sec);

enum lpp_state
{
    LPP_STOP,
    LPP_SLEEP,
    LPP_PROBE,
    LPP_PROBE_ACKED,
    LPP_LISTEN,
    LPP_LISTEN_TIMEOUT,
};

/**
 * Set an event handler to be notified.
 */
void lpp_set_event_handler(void (*notify_event)(enum lpp_state new_state));

#endif //LIB_LPP_M
#endif //LPP_H
