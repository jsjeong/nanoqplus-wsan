// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LPP (Low Power Probing) Interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 7. 2.
 * @description This is an interface for protocols using LPP. Each protocol using
 *              LPP should have its own implementation of this interface.
 */

#ifndef LPP_INTERFACE_H
#define LPP_INTERFACE_H

#include "kconf.h"
#ifdef LIB_LPP_M
#include "nos_common.h"
#include "errorcodes.h"

enum
{
    LPP_PACKET_MAX_SIZE = 127,
};

/**
 * Get a probe packet.
 *
 * @param[out] pkt: Pointer where the probe is stored at.
 * @return Size of the probe.
 */
UINT8 lpp_get_probe(UINT8 *pkt);

/**
 * Get a probe ack packet.
 *
 * @param[out] pkt: Pointer where the probe ack is stored at.
 * @return Size of the probe ack.
 */
UINT8 lpp_get_probe_ack(UINT8 *pkt);

/**
 * Check a received packet is a probe.
 *
 * @param pkt: Pointer of the received packet.
 * @param size: Size of the received packet.
 */
BOOL lpp_is_probe(const UINT8 *pkt, UINT8 size);

/**
 * Check a received packet is a probe ack.
 *
 * @param pkt: Pointer of the received packet.
 * @param size: Size of the received packet.
 */
BOOL lpp_is_probe_ack(const UINT8 *pkt, UINT8 size);

BOOL lpp_handle_probe(const UINT8 *probe, UINT8 len);

BOOL lpp_handle_probe_ack(const UINT8 *probe_ack, UINT8 len);

#endif //LIB_LPP_M
#endif //LPP_INTERFACE_H
