// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LPP (Low Power Probing) Core
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 7. 2.
 */

#include "lpp.h"

#ifdef LIB_LPP_M
#include "user_timer.h"
#include "arch.h"
#include "lpp-interface.h"
#include "critical_section.h"
#include "rf_modem.h"
#include <stdlib.h>
#include "taskq.h"

struct lpp_control
{
    enum lpp_state state;
    BOOL (*uplayer_rx_handler)(void);
    INT8 timer;
    UINT16 probe_interval;
    UINT16 listen_timeout;
    UINT8 buf[LPP_PACKET_MAX_SIZE];
    void (*notify_event)(enum lpp_state new_state);
};

enum
{
    DEFAULT_PROBE_INTERVAL = 10, //sec
    DEFAULT_LISTEN_TIMEOUT = 60, //sec
    DEFAULT_PROBE_ACK_WAIT = 30, //msec
};

static struct lpp_control lpp;

static void listen_timeout(void *args);
static void probe(void *args);

BOOL lpp_receive_control_packet(UINT8 *pkt, UINT8 pkt_len)
{
    if (lpp_is_probe_ack(pkt, pkt_len))
    {
        if ((lpp.state == LPP_PROBE || lpp.state == LPP_PROBE_ACKED) &&
            lpp_handle_probe_ack(pkt, pkt_len))
        {
            lpp.state = LPP_PROBE_ACKED;
        }
        return TRUE;
    }
    else if (lpp_is_probe(pkt, pkt_len))
    {
        if (lpp.state == LPP_LISTEN && lpp_handle_probe(pkt, pkt_len))
        {
            UINT16 delay, max_delay;
            
            pkt_len = lpp_get_probe_ack(lpp.buf);
            nos_rf_modem_write_txbuf(lpp.buf, pkt_len);
        
            delay = rand();

            //lesser value than probe ack wait time to compensate CCA and propagation delay
            max_delay = DEFAULT_PROBE_ACK_WAIT * 700L;
            delay %= max_delay;
            nos_delay_us(delay);
            if (nos_rf_modem_cca() == TRUE)
            {
                nos_rf_modem_tx(NULL);
            }
        }
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

void lpp_init(void)
{
    lpp.state = LPP_STOP;
    lpp.timer = -1;
    lpp.probe_interval = DEFAULT_PROBE_INTERVAL;
    lpp.listen_timeout = DEFAULT_LISTEN_TIMEOUT;
    lpp.notify_event = NULL;
}

/**
 * Called when listen timeout.
 */
static void listen_timeout(void *args)
{
    struct lpp_control *lpp = (struct lpp_control *) args;

    if (lpp->state == LPP_LISTEN)
    {
        lpp->timer = nos_user_timer_create_sec(probe, lpp, lpp->probe_interval, TRUE);
        if (lpp->timer < 0)
        {
            lpp->state = LPP_STOP;
        }
        else
        {
            if (lpp->notify_event != NULL)
                lpp->notify_event(LPP_LISTEN_TIMEOUT);
            
            NOS_ENTER_CRITICAL_SECTION();
            nos_rf_modem_rx_off();
            lpp->state = LPP_SLEEP;
            NOS_EXIT_CRITICAL_SECTION();

            nos_taskq_reg(probe, lpp);
        }

        // Notify a new state.
        if (lpp->notify_event != NULL)
            lpp->notify_event(lpp->state);
    }
}

static BOOL send_probe(struct lpp_control *lpp)
{
    UINT8 probe_len;
    enum lpp_state old_state;
    BOOL result = FALSE;
    UINT8 i;

    old_state = lpp->state;
    probe_len = lpp_get_probe(lpp->buf);

    NOS_ENTER_CRITICAL_SECTION();
    nos_rf_modem_rx_on();
    lpp->state = LPP_PROBE;
    NOS_EXIT_CRITICAL_SECTION();

    nos_rf_modem_write_txbuf(lpp->buf, probe_len);

    if (nos_rf_modem_cca() == TRUE)
    {
        nos_rf_modem_tx(NULL);
        
        // Wait for a LPP probe response.
        for (i = 0; i < DEFAULT_PROBE_ACK_WAIT; i++)
        {
            nos_delay_ms(1);
            
            if (lpp->state == LPP_PROBE_ACKED)
            {
                result = TRUE;
            }
        }
    }

    lpp->state = old_state;
    return result;
}

static void probe(void *args)
{
    struct lpp_control *lpp = (struct lpp_control *) args;

    if (lpp->state == LPP_SLEEP)
    {
        if (send_probe(lpp))
        {
            // Enter into a LPP_LISTEN state.
            nos_user_timer_destroy(lpp->timer);
            lpp->timer = nos_user_timer_create_sec(listen_timeout, lpp,
                                                   lpp->listen_timeout, FALSE);
            if (lpp->timer < 0)
            {
                lpp->state = LPP_STOP;
            }
            else
            {
                lpp->state = LPP_LISTEN;
            }
        }
        else
        {
            NOS_ENTER_CRITICAL_SECTION();
            nos_rf_modem_rx_off();
            lpp->state = LPP_SLEEP;
            NOS_EXIT_CRITICAL_SECTION();
        }

        // Notify a new state.
        if (lpp->notify_event != NULL)
            lpp->notify_event(lpp->state);
    }
}

ERROR_T lpp_sleep(void)
{
    if (lpp.state == LPP_STOP)
    {
        lpp.timer = nos_user_timer_create_sec(probe, &lpp, lpp.probe_interval, TRUE);
        if (lpp.timer < 0)
            return ERROR_BUSY;

        NOS_ENTER_CRITICAL_SECTION();
        nos_rf_modem_rx_off();
        lpp.state = LPP_SLEEP;
        NOS_EXIT_CRITICAL_SECTION();

        nos_taskq_reg(probe, &lpp);

        return ERROR_SUCCESS;
    }
    else
    {
        return ERROR_GENERAL_FAILURE;
    }
}

ERROR_T lpp_listen(void)
{
    if (lpp.state == LPP_STOP)
    {
        send_probe(&lpp);
        
        lpp.timer = nos_user_timer_create_sec(listen_timeout, &lpp, lpp.listen_timeout, FALSE);
        if (lpp.timer < 0)
            return ERROR_BUSY;

        lpp.state = LPP_LISTEN;

        if (lpp.notify_event != NULL)
            lpp.notify_event(LPP_LISTEN);

        return ERROR_SUCCESS;
    }
    else
    {
        return ERROR_GENERAL_FAILURE;
    }
}

ERROR_T lpp_stop(void)
{
    if (lpp.state != LPP_STOP)
    {
        nos_user_timer_destroy(lpp.timer);
        lpp.timer = -1;

        NOS_ENTER_CRITICAL_SECTION();
        nos_rf_modem_rx_on();
        lpp.state = LPP_STOP;
        NOS_EXIT_CRITICAL_SECTION();

        if (lpp.notify_event != NULL)
            lpp.notify_event(LPP_STOP);
        
        return ERROR_SUCCESS;
    }
    else
    {
        return SUCCESS_NOTHING_HAPPENED;
    }
}

ERROR_T lpp_set_listen_timeout(UINT16 timeout_sec)
{
    if (timeout_sec > nos_user_timer_get_max_sec())
        return ERROR_INVALID_ARGS;

    lpp.listen_timeout = timeout_sec;
    return ERROR_SUCCESS;
}

ERROR_T lpp_set_probe_interval(UINT16 interval_sec)
{
    if (interval_sec > nos_user_timer_get_max_sec())
        return ERROR_INVALID_ARGS;

    lpp.probe_interval = interval_sec;
    return ERROR_SUCCESS;
}

void lpp_set_event_handler(void (*notify_event)(enum lpp_state new_state))
{
    lpp.notify_event = notify_event;
}

#endif //LIB_LPP_M

