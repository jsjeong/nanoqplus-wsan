// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

#include "utc-clock.h"

#ifdef LIB_UTC_CLOCK_M
#include "critical_section.h"

/*
 * Reference UTC time  (base: 1970.01.01 00:00:00, GMT)
 * Korea: GMT+0900,  +32400 seconds
 * 2012.01.01 00:00:00, GMT - 1325376000 sec
 * 2013.01.01 00:00:00, GMT - 1356998400 sec (+31622400, 366days)
 * 2014.01.01 00:00:00, GMT - 1388534400 sec (+31536000)
 * 2015.01.01 00:00:00, GMT - 1420070400 sec (+31536000)
 * 2016.01.01 00:00:00, GMT - 1451606400 sec (+31536000)
 * 2016.01.01 00:00:00, GMT - 1483228800 sec (+31622400, 366days)
 */

volatile UINT32 utc_curr_sec = 1325376000; //2012.01.01 00:00:00 GMT

void nos_utc_set_time(UINT32 sec)
{
    utc_curr_sec = sec;
}

UINT32 nos_utc_get_time(void)
{
    return utc_curr_sec;
}

void nos_utc_increment(void)
{
    utc_curr_sec++;
}

#endif //LIB_UTC_CLOCK_M
