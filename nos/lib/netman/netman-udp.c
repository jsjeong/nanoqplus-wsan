// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2014
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file netman-udp.c
 *
 * @brief Network manager daemon - UDP interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2014. 3. 21.
 */

#include "netman.h"
#include "udp-api.h"
#include "heap.h"
#include <stdio.h>
#include "netman-core.h"

struct netman_udp_ctx
{
    const IP6_ADDRESS *dst_addr;
    UINT16 dst_port;
};

static UINT16 listen_port = 0;

static void netman_udp_send(const char *str, void *ctx)
{
    struct netman_udp_ctx *u = (struct netman_udp_ctx *) ctx;
    printf("%s", str);
    
    udp_sendto(255, listen_port, u->dst_addr, u->dst_port, str, strlen(str));
}

static void netman_on_udp_rcvd(UINT8 ip6_inf,
                               const IP6_ADDRESS *src,
                               UINT16 src_port,
                               UINT16 dst_port,
                               const void *buf,
                               UINT16 len)
{
    UINT16 i;
    char *cmd;
    struct netman_udp_ctx c;

    cmd = nos_malloc(len + 1);
    if (!cmd)
    {
        printf("Not enough memory (%u byte)\n", len + 1);
        return;
    }

    memcpy(cmd, buf, len);
    cmd[len] = 0;
    for (i = 0; i < len; i++)
    {
        if (cmd[i] == '\r' || cmd[i] == '\n')
        {
            cmd[i] = 0;
            len = i;
            break;
        }
    }
    
    printf("Cmd:%s (%u byte)\n", cmd, len);
    c.dst_addr = src;
    c.dst_port = src_port;
    netman_core_cmd(cmd, netman_udp_send, (void *) &c);
    nos_free(cmd);
}

ERROR_T netman_listen_udp(UINT16 port)
{
    ERROR_T err;
    err = udp_set_listen(port, netman_on_udp_rcvd);
    if (err == ERROR_SUCCESS)
        listen_port = port;
    return err;
}
