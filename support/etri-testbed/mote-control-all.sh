#!/bin/sh

cmd=$1
platform=$2
nodelist=`grep $platform $NOS_HOME/support/etri-testbed/map | awk '{print $2}'`

if [ $cmd = 'reprogram' ]
then
    for node in $nodelist
    do
        echo $node
        make testbed-reprogram id=$node
    done
elif [ $cmd = 'reset' ]
then
    for node in $nodelist
    do
        echo $node
        make testbed-reset id=$node
    done
elif [ $cmd = 'erase' ]
then
    for node in $nodelist
    do
        echo $node
        make testbed-erase id=$node
    done
fi

