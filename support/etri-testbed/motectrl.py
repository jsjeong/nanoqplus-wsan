import asynchat, asyncore, logging, socket

class MoteControlClient(asynchat.async_chat):
    def __init__(self, host, port, cmd):
        self.cmd = cmd
        self.received_data = []
        self.logger = logging.getLogger('ReprogramClient')
        self.set_terminator('\n')
        asynchat.async_chat.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.logger.debug('Connecting to %s', (host, port))
        self.connect((host, port))
        return

    def handle_connect(self):
        #self.logger.debug('handle_connect() -> send command "%s"', self.cmd)
        self.push(self.cmd)
        self.push('\n')

    def collect_incoming_data(self, data):
        self.logger.debug('collect_incoming_data() -> (%d)\n """%s"""', len(data), data)
        self.received_data.append(data)

    def found_terminator(self):
        self.logger.debug('found_terminator()')
        received_message = ''.join(self.received_data)
        if received_message == self.received_data:
            self.logger.debug('RECEIVED COPY OF MESSAGE')
        else:
            self.logger.debug('ERROR IN TRANSMISSION')
            self.logger.debug('RECEIVED "%s"', received_message)
        return
