#! /bin/bash

if [ $# -lt 1 ]
then
    echo "Usage: $0 [node ID] [log file description]"
    exit 0
fi

nodeid=$1
desc=$2

if [ $# -gt 1 ]
then
    log="$2_$1_`date +"%Y%m%d-%H%M"`.log"
else
    log="$1_`date +"%Y%m%d-%H%M"`.log"
fi

cmd=`echo "$NOS_HOME/support/etri-testbed/mote-serial.py $nodeid $log; exec bash"`

echo "Terminating an existing screen..."
screen -S "testbed-$nodeid." -X quit 2> /dev/null
echo "Executing '$cmd'..."
screen -dmS "testbed-$nodeid." bash -c "$cmd"
#$cmd