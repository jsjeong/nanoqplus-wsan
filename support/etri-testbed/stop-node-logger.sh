#! /bin/sh

if [ $# -lt 1 ]
then
    echo "Usage: $0 [node ID]"
    exit 0
fi

nodeid=$1

screen -S testbed-$nodeid -X quit 2> /dev/null
