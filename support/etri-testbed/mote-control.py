#!/usr/bin/env python

import asyncore, logging, sys, os
import motectrl

logging.basicConfig(level=logging.DEBUG,format='%(name)s: %(message)s',)

def print_usage():
    print 'Usage: %s [cmd] [node ID] [platform] {image}' % (sys.argv[0])
    print ' - cmd: should be one of \'reprogram\', \'reset\', \'erase\'.'
    print ' - node ID: valid node ID specified in \'map\' file.'
    print ' - platform: valid mote type specified in \'map\' file.'
    print ' - image: only for \'reprogram\' command. valid image file path to reprogram.'

if len(sys.argv) < 3:
    print_usage()
    print 'Mandatory arguments are not specified.'
    sys.exit(-1)

if sys.argv[1] not in ['reprogram', 'reset', 'erase']:
    print_usage()
    print '[cmd] should be one of \'reprogram\', \'reset\', or \'erase\'.'
    sys.exit(-1)

cmd = ""
if sys.argv[1] == 'reprogram':
    if len(sys.argv) < 5:
        print_usage()
        print '{platform} and {image} shoud be also specified for \'reprogram\' command.'
        sys.exit(-1)
    else:
        image_file = file(sys.argv[4])
        for line in image_file.readlines():
            cmd += line + '\n'

target_mac = ""
map_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'map')

f = file(map_path)
for line in f.readlines():
    if line[0] != '#' and len(line) > 1:
        (mac, node, hwtype) = line.split()
        if node == sys.argv[2] and hwtype == sys.argv[3]:
            target_mac = mac
            break
f.close()

if len(target_mac) == 0:
    print_usage()
    print 'The pair of \'%s\' and \'%s\' is not valid.' % (sys.argv[2], sys.argv[3])
    sys.exit(-1)

ip = '129.254.192.26'
port = 16462

cmd += '%s %s' % (sys.argv[1], target_mac)
client = motectrl.MoteControlClient(ip, port, cmd)

asyncore.loop()
