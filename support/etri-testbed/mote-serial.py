#!/usr/bin/env python

import socket, asyncore, asynchat, sys
from datetime import datetime

class MoteSerialListener(asynchat.async_chat):
    def __init__(self, host, nodeid, log=None):
        asynchat.async_chat.__init__(self)
        self.received_data = []
        self.set_terminator('\n')
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect( (host, 17000 + int(nodeid)) )
        self.nodeid = nodeid
        if log != None:
            self.log = open(log, 'w')
        else:
            self.log = None

    def handle_connect(self):
        pass

    def handle_close(self):
        self.close()

    def collect_incoming_data(self, data):
        self.received_data.append(data)

    def found_terminator(self):
        received_message = '[ID:' + self.nodeid + ', ' + str(datetime.now()) + '] ' + ''.join(self.received_data)
        self.received_data = []
        print received_message
        if self.log != None:
            self.log.write(received_message + '\n')
            self.log.flush()

if len(sys.argv) == 2:
    client = MoteSerialListener('129.254.192.26', sys.argv[1])
elif len(sys.argv) == 3:
    client = MoteSerialListener('129.254.192.26', sys.argv[1], sys.argv[2])
else:
    print 'Usage: %s [node ID] {log file}' % (sys.argv[0])
    exit

asyncore.loop()
