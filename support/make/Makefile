# -*- mode:Makefile; -*-

RM	= rm -f
MKDIR = mkdir -p
RD = rm -rf
CP = cp
MV = mv
TOUCH = touch
AR = ar

-include $(PWD)/.config

# Common
NOSDIR = \
	$(NOS_HOME)/nos\
	$(NOS_HOME)/nos/include\
	$(NOS_HOME)/nos/arch\
	$(NOS_HOME)/nos/arch/$(MCU)\
	$(NOS_HOME)/nos/platform\
	$(NOS_HOME)/nos/platform/$(PLATFORM)\
	$(NOS_HOME)/nos/drivers\
	$(NOS_HOME)/lib/include

LIB += cyassl netman nos-trickle nos-util xively

# Modules are sorted by alphabetical order.
ifeq ($(CONFIG_APOLLO_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/apollo
endif

ifeq ($(CONFIG_ADF7023J_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/wpan-dev/adf7023-j
endif

ifeq ($(CONFIG_AT86RF212_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/wpan-dev/rf212
endif

ifeq ($(CONFIG_AT86RF230_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/wpan-dev/rf230
endif

ifeq ($(CONFIG_BH1600_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/bh1600
endif

ifeq ($(CONFIG_AT45DB_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/at45db
endif

ifeq ($(CONFIG_CC1120_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/wpan-dev/cc1120
endif

ifeq ($(CONFIG_CC2420_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/wpan-dev/cc2420
endif

ifeq ($(CONFIG_CC2520_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/wpan-dev/cc2520
endif

ifeq ($(CONFIG_CLCD_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/clcd
endif

ifeq ($(CONFIG_DS1086L_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/ds1086l
endif

ifeq ($(CONFIG_EAP_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/eap
endif

ifeq ($(CONFIG_ELT_S100_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/elt_s100
endif

ifeq ($(CONFIG_EMLCD_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/em-lcd
endif

ifeq ($(CONFIG_ENC28J60_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/eth-dev/enc28j60
endif

ifeq ($(CONFIG_ETH_DEV_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/eth-dev
endif

ifeq ($(CONFIG_ETHERNET6_M),y)
	ifeq ($(CONFIG_ETHER6_DEBUG),y)
		LIB += nos-ethernet6dbg
	else
		LIB += nos-ethernet6
	endif
endif

ifeq ($(CONFIG_GLOSSY_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/mac/glossy
endif

ifeq ($(CONFIG_IEEE_802_15_4_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/802.15.4
	NOSDIR += $(NOS_HOME)/nos/net/mac
	NOSDIR += $(NOS_HOME)/nos/drivers/wpan-dev
endif

ifeq ($(CONFIG_INET_M),y)
	ifeq ($(CONFIG_IP_DEBUG),y)
		LIB += nos-ip6dbg
	else
		LIB += nos-ip6
	endif
endif

ifeq ($(CONFIG_KERNEL_M),y)
	NOSDIR += \
		$(NOS_HOME)/nos/kernel \
		$(NOS_HOME)/nos/kernel/thread
endif

ifeq ($(CONFIG_LESENSOR_LIGHT_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/em-lesensor-light
endif

ifeq ($(CONFIG_LIB_LPP_M),y)
	NOSDIR += $(NOS_HOME)/nos/lib/lpp
endif

ifeq ($(CONFIG_LIB_TINY_RS_M),y)
	NOSDIR += $(NOS_HOME)/nos/lib/tinyrs
endif

ifeq ($(CONFIG_LIB_UTC_CLOCK_M),y)
	NOSDIR += $(NOS_HOME)/nos/lib/utc-clock
endif

ifeq ($(CONFIG_LIBNOS_AES_M),y)
	NOSDIR += $(NOS_HOME)/nos/lib/aes
endif

ifeq ($(CONFIG_LMC1623_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/clcd/lmc1623
endif

ifeq ($(CONFIG_LOWPAN_M),y)
	ifeq ($(CONFIG_LOWPAN_DEBUG),y)
		LIB += nos-6lowpandbg
	else
		LIB += nos-6lowpan
	endif
endif

ifeq ($(CONFIG_LPLL_MAC_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/mac/lpll-mac
endif

ifeq ($(CONFIG_LPP_MAC),y)
	NOSDIR += $(NOS_HOME)/nos/net/mac/lpp-mac
endif

ifeq ($(CONFIG_M25P80_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/m25p80
endif

ifeq ($(CONFIG_M25PX16_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/m25px16
endif

ifeq ($(CONFIG_MESH_ROUTING_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/l2-routing
endif

ifeq ($(CONFIG_MICS5132_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/mics5132
endif

ifeq ($(CONFIG_MG2410_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/wpan-dev/mg2410
endif

ifeq ($(CONFIG_MLE_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/mle
endif

ifeq ($(CONFIG_MTS300_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/mts300
endif

ifeq ($(CONFIG_NANO_MAC_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/mac/nano-mac
endif

ifeq ($(CONFIG_NCP1840_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/ncp1840
endif

ifeq ($(CONFIG_OCTACOMM_ENV_SENSOR_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/octacomm/env
endif

ifeq ($(CONFIG_OCTACOMM_PIR_SENSOR_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/octacomm/pir
endif

ifeq ($(CONFIG_OCTACOMM_RELAY_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/octacomm/relay
endif

ifeq ($(CONFIG_OCTACOMM_ULTRASONIC_SENSOR_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/octacomm/us
endif

ifeq ($(CONFIG_PANA_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/pana
endif

ifeq ($(CONFIG_PPP_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/ppp
endif

ifeq ($(CONFIG_RENO_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/l2-routing/reno
endif

ifeq ($(CONFIG_RPL_M),y)
	ifeq ($(CONFIG_RPL_DEBUG),y)
		LIB += nos-rpldbg
	else
		LIB += nos-rpl
	endif
endif

ifeq ($(CONFIG_SENSOR_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/sensor
endif

ifeq ($(CONFIG_SHTXX_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/shtxx
endif

ifeq ($(CONFIG_STORAGE_M),y)
	NOSDIR += $(NOS_HOME)/nos/drivers/generic-storage
endif

ifeq ($(CONFIG_TCP_M),y)
	ifeq ($(CONFIG_TCP_DEBUG),y)
		LIB += nos-tcpdbg
	else
		LIB += nos-tcp
	endif
endif

ifeq ($(CONFIG_TENO_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/l2-routing/teno
endif

ifeq ($(CONFIG_UDP_M),y)
	ifeq ($(CONFIG_UDP_DEBUG),y)
		LIB += nos-udpdbg
	else
		LIB += nos-udp
	endif
endif

ifeq ($(CONFIG_ZIGBEE_IP_M),y)
	NOSDIR += $(NOS_HOME)/nos/net/zigbee-ip
endif

PREDEFINES=NOS $(MCU)

# CyaSSL definitions
PREDEFINES+=\
	NO_ERROR_STRING SMALL_SESSION_CACHE CYASSL_DER_LOAD CYASSL_DTLS CYASSL_RIPEMD CYASSL_SHA384 CYASSL_SHA512 HAVE_CAMELLIA HAVE_ECC CYASSL_USER_IO NO_FILESYSTEM NO_WRITEV NO_DEV_RANDOM SINGLE_THREADED USER_TIME USE_CERT_BUFFERS_1024 CYASSL_SMALL_STACK

include $(NOS_HOME)/nos/platform/$(PLATFORM)/Makefile.build

pkg_check : toolchain_check
