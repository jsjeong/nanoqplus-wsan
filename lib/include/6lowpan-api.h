// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file 6lowpan-api.h
 *
 * 6LoWPAN API
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2014. 2. 18.
 */

#ifndef LOWPAN_API_H
#define LOWPAN_API_H

#include "nos_common.h"

/**
 * @brief Initialize a LoWPAN interface to be usable by IPv6.
 *
 * It initialize a device whose ID is @p dev_id with @p panid PAN ID, @p sid
 * 2-byte short ID, and @p lid 8-byte EUI-64. The initialized interface will
 * support @p num_neigh link-local neighbors, and @p num_prefix global
 * addresses. It is usally called by user applications.
 *
 * @param[in] dev_id      Device ID
 * @param[in] panid       PAN ID
 * @param[in] sid         16-bit short ID
 * @param[in] lid         64-bit extended ID
 * @param[in] num_neigh   Number of link-local neighbors
 * @param[in] num_prefix  Acceptable number of prefixes from RAs
 *
 * @return If successfully initialized, valid ID less than the maximum number of
 * IPv6 interfaces. Otherwise, value greater than or equal to the maximum number
 * of IPv6 interfaces.
 *
 * @warning It must be called after ip6_init() called.
 *
 * @see http://tools.ietf.org/html/rfc6282#section-3.2.2
 */
UINT8 lowpan_init(UINT8 dev_id,
                  UINT16 panid,
                  UINT16 sid,
                  const UINT8 *lid,
                  UINT8 num_neigh,
                  UINT8 num_prefix);

/**
 * @brief Initialize 6LoWPAN functionality.
 *
 * It tries to attempt to allocate space for control blocks for @p ninf 6LoWPAN
 * interfaces. It should be called by OS when startup.
 *
 * @param[in] ninf  Number of 6LoWPAN interfaces
 *
 * @warning Do not call it twice. It should be only called by OS when system
 *          startup.
 */
void lowpan_core_init(UINT8 ninf);

BOOL lowpan_get_lladdr(UINT8 ip6_inf, UINT8 *eui64, UINT16 *short_addr);

#endif //LOWPAN_API_H
