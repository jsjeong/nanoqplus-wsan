// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * IPv6 Network Interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 7. 30.
 */

#ifndef IP6_INTERFACE_H
#define IP6_INTERFACE_H

#include "nos_common.h"
#include "errorcodes.h"
#include "ip6-types.h"
#include "icmp6-types.h"

enum ip6_interface_type
{
    IP6_INTERFACE_UNKNOWN = 0,
    IP6_INTERFACE_6LOWPAN,
    IP6_INTERFACE_ETHERNET,
    IP6_INTERFACE_PPP,
};

struct ip6_interface_link_api
{
    void    (*get_iid)       (const void *dev, UINT8 *dst);
    BOOL    (*has_iid)       (const void *dev, const UINT8 *iid);
    BOOL    (*is_in_mc_group)(UINT8 inf, const IP6_ADDRESS *mcaddr);
    ERROR_T (*link_send)     (UINT8 out_inf, UINT8 nxhop, PACKET *ippkt);
    void    (*get_neigh_iid) (const void *dev, UINT8 idx, UINT8 *iid);
    BOOL    (*neigh_has_addr)(const void *dev, UINT8 idx, const IP6_ADDRESS *addr);

    ERROR_T (*recv_rs)       (UINT8 in_inf,
                              IP6_HEADER *ip6,
                              struct icmp6_rs *msg,
                              UINT16 len);

    ERROR_T (*recv_ra)       (UINT8 in_inf,
                              IP6_HEADER *ip6,
                              struct icmp6_ra *msg,
                              UINT16 len);
    
    ERROR_T (*recv_ns)       (UINT8 in_inf,
                              IP6_HEADER *ip6,
                              struct icmp6_ns *msg,
                              UINT16 len);
    
    ERROR_T (*recv_na)       (UINT8 in_inf,
                              IP6_HEADER *ip6,
                              struct icmp6_na *msg,
                              UINT16 len);

    ERROR_T (*recv_redirect) (UINT8 in_inf,
                              IP6_HEADER *ip6,
                              struct icmp6_redirect *msg,
                              UINT16 len);

    void    (*confirm_reach) (UINT8 ip6_inf, UINT8 neigh);
    
    void    (*lifetime_tick) (UINT8 ip6_inf);

    void    (*state_changed) (UINT8 ip6_inf);
};

struct ip6_interface_routing_api
{
    void (*start)(UINT8 inf);
    
    ERROR_T (*preforward)(UINT8 inf, PACKET *ippkt, UINT8 nxhop);
    
    void (*feedback)(UINT8 inf, UINT8 nxhop);
    
    void (*init_neigh)(UINT8 inf, UINT8 neigh);
    
    void (*preremove_neigh)(UINT8 inf, UINT8 neigh);
    
    void (*postremove_neigh)(UINT8 inf, UINT8 neigh);
    
    ERROR_T (*recv_rtctrl)(UINT8 inf,
                           IP6_HEADER *ip6,
                           ICMP6_MESSAGE *msg,
                           UINT16 len);
};

struct ip6_interface_stat
{
    UINT32 send_count;
    UINT32 recv_count;
    UINT32 forward_count;
};

struct ip6_prefix
{
    BOOL valid:1;
    BOOL on_link:1;
    BOOL autoconf:1;              /* can be advertised to neighbor hosts for
                                     auto-configuration */
    IP6_ADDRESS addr;
    UINT32 lifetime;              /* for on-link determination */
    UINT32 preferred_lifetime;    /* auto-configured address lifetime in unit of
                                     minute */
    UINT8 len;                    /* prefix length (1~64) */
};

struct ip6_neighbor
{
    BOOL valid:1;
    BOOL is_router:1;

    union
    {
        struct
        {
            INT8 preference:2;
            UINT16 lifetime;
        } router;
    } t;
};

enum ip6_routing_protocol
{
    IP6_ROUTING_NONE = 0,
    IP6_ROUTING_RPL,
};

struct ip6_interface
{
    BOOL valid:1;
    BOOL ll_dad_passed:1;
    UINT8 def_hop_limit;
    UINT16 mtu;
    UINT16 pmtu;
    enum ip6_interface_type dev_type;
    void *dev;

    struct ip6_prefix *prefix;
    UINT8 max_num_prefix;
    UINT8 num_valid_prefix;
    
    struct ip6_neighbor *neighbor;
    UINT8 max_num_neigh;

    IP6_NODE_STATE state;
    IP6_STATE_NOTIFIER notify;

    const struct ip6_interface_link_api *link_api;
#define get_iid            link_api->get_iid
#define has_iid            link_api->has_iid
#define is_in_mc_group     link_api->is_in_mc_group
#define link_send          link_api->link_send
#define get_neigh_iid      link_api->get_neigh_iid
#define neigh_has_addr     link_api->neigh_has_addr

#define recv_rs            link_api->recv_rs
#define recv_ra            link_api->recv_ra
#define recv_ns            link_api->recv_ns
#define recv_na            link_api->recv_na
#define recv_redirect      link_api->recv_redirect
#define confirm_reach      link_api->confirm_reach
#define link_lifetime_tick link_api->lifetime_tick
#define link_state_changed link_api->state_changed

    enum ip6_routing_protocol rt_proto;
    const struct ip6_interface_routing_api *rt_api;
#define rt_start           rt_api->start
#define rt_preforward      rt_api->preforward
#define rt_feedback        rt_api->feedback
#define rt_init_neigh      rt_api->init_neigh
#define rt_prerm_neigh     rt_api->preremove_neigh
#define rt_postrm_neigh    rt_api->postremove_neigh
#define rt_recv_ctrlmsg    rt_api->recv_rtctrl

#define HAVE_RT_START(pinf)         ((pinf)->rt_api && (pinf)->rt_start)
#define HAVE_RT_PREFORWARD(pinf)    ((pinf)->rt_api && (pinf)->rt_preforward)
#define HAVE_RT_FEEDBACK(pinf)      ((pinf)->rt_api && (pinf)->rt_feedback)
#define HAVE_RT_INIT_NEIGH(pinf)    ((pinf)->rt_api && (pinf)->rt_init_neigh)
#define HAVE_RT_PRERM_NEIGH(pinf)   ((pinf)->rt_api && (pinf)->rt_prerm_neigh)
#define HAVE_RT_POSTRM_NEIGH(pinf)  ((pinf)->rt_api && (pinf)->rt_postrm_neigh)

    struct ip6_interface_stat stat;
};

enum
{
    IP6_INTERFACE_ANY = 255,
};

UINT8 nos_ip6_interface_init(const struct ip6_interface_link_api *api,
                             enum ip6_interface_type inf_type,
                             void *inf,
                             struct ip6_neighbor *neigh_list,
                             UINT8 neigh_list_len,
                             struct ip6_prefix *prefix_list,
                             UINT8 prefix_list_len,
                             UINT16 mtu);

ERROR_T nos_ip6_interface_get_neigh_addr(IP6_ADDRTYPE type,
                                         UINT8 inf,
                                         UINT8 neigh,
                                         IP6_ADDRESS *dst);

UINT8 nos_ip6_interface_get_neighbor(UINT8 ip6_inf,
                                     const IP6_ADDRESS *addr);

void nos_ip6_interface_set_routing(UINT8 inf,
                                   const struct ip6_interface_routing_api *api);

enum ip6_interface_stat_type
{
    IP6_STAT_SEND,
    IP6_STAT_RECEIVE,
    IP6_STAT_FORWARD,
};

UINT32 nos_ip6_interface_get_stat(UINT8 inf, enum ip6_interface_stat_type stat);

#endif //IP6_INTERFACE_H
