// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Header for the UDP.
 * @author Jongsoo Jeong (ETRI)
 * @date 2010. 2. 1.
 */

#ifndef UDP_H
#define UDP_H

#include "nos_common.h"
#include "ip6-types.h"
#include "errorcodes.h"

/**
 * Send a user datagram.
 *
 * @param[in] out_inf   Output interface ID (only for link-local send)
 * @param[in] src_port  Source port address.
 * @param[in] dst_addr  Destination IPv6 address.
 * @param[in] dst_port  Destination port address.
 * @param[in] buf The   User datagram to be sent.
 * @param[in] len The   Length of @a buf.
 *
 * @return An error code.
 */
ERROR_T udp_sendto(UINT8 out_inf,
                   UINT16 src_port,
                   const IP6_ADDRESS *dst_addr,
                   UINT16 dst_port,
                   const void *user_data,
                   UINT16 len);

typedef void (*UDP_CALLBACK_ON_RECV)(UINT8 in_inf,
                                     const IP6_ADDRESS *src_addr,
                                     UINT16 src_port,
                                     UINT16 dst_port,
                                     const UINT8 *msg,
                                     UINT16 len);

/**
 * Open an UDP port to listen incoming UDP datagrams.
 *
 * @param[in] port   Port number
 * @param[in] func   Callback function to be called on receipt of UDP datagrams
 *
 * @return Error codes
 */
ERROR_T udp_set_listen(UINT16 port, UDP_CALLBACK_ON_RECV func);

/**
 * Get a random port number that is not listening.
 */
UINT16 udp_get_unused_random_port(void);

/**
 * UDP initialization when startup
 */
void udp_core_init(UINT8 nport);

#endif //UDP_H
