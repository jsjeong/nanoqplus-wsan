// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file ip6-api.h
 *
 * IPv6 Application Programming Interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 11. 28.
 */

#ifndef IP6_API_H
#define IP6_API_H

#include "nos_common.h"
#include "errorcodes.h"
#include "ip6-types.h"

/**
 * @brief Initialize the IPv6 network layer.
 *
 * @param[in] uplink_netif  The ID of uplink (WAN) network interface
 */
void ip6_init(UINT8 uplink_netif);

/**
 * @brief Set a callback function to notify the IPv6 state to user when it is
 * changed.
 *
 * @param[in] func The callback function pointer to be called when the IPv6
 * state is changed.
 */
void ip6_set_state_notifier(UINT8 inf, IP6_STATE_NOTIFIER func);

/**
 * @brief Get the node's state.
 *
 * @return The state of the node.
 */
IP6_NODE_STATE ip6_get_state(UINT8 inf);

enum
{
    IP6_NTOP_BUF_SZ = 40,
};

/**
 * @brief Parse @p src IPv6 address to abbreviated string type, and store it to @p dst.
 *
 * @param[in] src The IPv6 address to be parsed.
 * @param[in] dst The buffer pointer where the output string is stored in.
 *
 * @return Parsed string.
 */
char *ip6_ntop(const IP6_ADDRESS *src, char *dst);

/**
 * @brief Parse @p src string to an IPv6 address, and store it to @p dst.
 *
 * @param[in] src The IPv6 address string to be parsed.
 * @param[in] dst The buffer pointer where the output address is stored in.
 *
 * @return TRUE when success. Otherwise, FALSE.
 */
BOOL ip6_pton(const char *src, IP6_ADDRESS *dst);

#define ip6_is_attached(inf)                      \
    (ip6_get_state(inf) == IP6_NODE_STATE_ROUTER)

/**
 * @brief Set a prefix to an IPv6 interface.
 *
 * @param[in] prefix         Prefix to be registered
 * @param[in] len            Prefix length
 * @param[in] lifetime       Prefix lifetime
 * @param[in] pref_lifetime  Prefix preferred lifetime
 * @param[in] autoconf       Whether the prefix can be advertised for IPv6
 *                           address auto-configuration
 *
 * @return Whether my prefix is created or updated (<255), or not(255).
 */
UINT8 ip6_interface_set_prefix(UINT8 ip6_inf,
                               IP6_ADDRESS *prefix,
                               UINT8 len,
                               UINT32 lifetime,
                               UINT32 pref_lifetime,
                               BOOL autoconf);

/**
 * @brief Get an IPv6 address assigned to the @p inf interface.
 *
 * @param[in] type  Address type
 * @param[in] inf   Network interface
 * @param[out] dst  Pointer where the output address is stored
 *
 * @return ERROR_SUCCESS       Success
 *         ERROR_INVALID_ARGS  Invalid arguments
 */
ERROR_T ip6_interface_get_addr(IP6_ADDRTYPE type,
                               UINT8 inf,
                               IP6_ADDRESS *dst);

/**
 * @brief Add an IPv6 route to the main IPv6 routing table.
 *
 * @param[in] prefix      Destination prefix
 * @param[in] prefix_len  Destination prefix's valid length in bits
 * @param[in] egress_inf  Egress IPv6 interface ID
 * @param[in] nexthop     IPv6 address of nexthop
 *
 * @return ERROR_SUCCESS  Success
 *         ERROR_FAIL     Failure due to table full
 */
ERROR_T ip6_add_route(const IP6_ADDRESS *prefix,
                      UINT8 prefix_len,
                      UINT8 egress_inf,
                      const IP6_ADDRESS *nexthop);

enum
{
    MAX_PING_REQUESTS = 10,
};

/**
 * @brief Send ICMPv6 echo requests. (ping6)
 *
 * @param[in] ip6_inf     IPv6 interface ID where link-local scope requests to
 *                        be sent. If the @a dst has a global scope, it is
 *                        ignored.
 * @param[in] dst         Destination address
 * @param[in] size        Payload size overloaded to the ICMPv6 message
 * @param[in] cnt         Requests count
 * @param[in] on_replied  Callback function pointer to be called when
 *                        corresponding echo replies arrive. When the function
 *                        is called with NULL @a ip6, it means request timed
 *                        out.
 *
 * @return TRUE when sending an echo request is successful. If a number of
 *         waiting for echo replies exceeds @link MAX_PING_REQUESTS @endlink, or
 *         the kernel scheduler denies, it returns FALSE.
 */
BOOL ping6(UINT8 ip6_inf,
           const IP6_ADDRESS *dst,
           UINT16 size,
           UINT8 cnt,
           void (*on_replied)(const IP6_HEADER *ip6,
                              UINT16 seq,
                              UINT16 sz,
                              const void *payload));

/**
 * @brief Initialize IPv6 functionality.
 *
 * @param[in] ninf  Number of IPv6 interfaces
 * @param[in] nrt   Number of main IPv6 routing table
 *
 * @warning Do not call it twice. It should be only called by OS when system
 *          startup.
 */
void ip6_core_init(UINT8 ninf, UINT16 nrt);

/**
 * @brief Parse an IPv6 state to printable string.
 *
 * @param state IPv6 state to be parsed
 * @return State string
 */
const char *ip6_state_string(IP6_NODE_STATE state);

#endif //IP6_API_H
