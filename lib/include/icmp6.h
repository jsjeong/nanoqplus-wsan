// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Header file for the ICMPv6.
 * @author Jongsoo Jeong (ETRI)
 * @date 2010. 2. 1.
 */

#ifndef ICMP_H
#define ICMP_H

#include "nos_common.h"
#include "ip6-types.h"
#include "errorcodes.h"
#include "ip6-interface.h"

void nos_icmp6_recv(UINT8 in_dev,
                    PACKET *head,
                    IP6_HEADER *ip6,
                    PACKET *icmp6);

ERROR_T nos_icmp6_send(IP6_ADDRTYPE src_scope,
                       const IP6_ADDRESS *dst_addr,
                       UINT8 out_dev,
                       void *buf,
                       UINT16 buf_len,
                       BOOL data_free,
                       UINT8 hop_limit);

ICMP6_OPTION *nos_icmp6_get_next_option(ICMP6_OPTION *firstopt,
                                        UINT16 wholelen,
                                        UINT8 opt_type);

ERROR_T nos_icmp6_error_report(PACKET *orig_pkt,
                               UINT8 error_type,
                               UINT8 error_code,
                               UINT32 error_value,
                               UINT8 ip6_inf);
#endif // ~ICMP_H
