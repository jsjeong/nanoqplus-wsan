// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Functions for the IPv6.
 * @author Jongsoo Jeong (ETRI)
 * @date 2010. 2. 1.
 * @see http://tools.ietf.org/html/rfc2460
 */

#ifndef IP6_H
#define IP6_H

#include "nos_common.h"
#include "ip6-types.h"
#include "ip6-interface.h"
#include "errorcodes.h"
#include "ip6-api.h"

#define NOS_IP6_SET_MC_GID_TO_ALL_NODE(p_addr)               \
    do {                                                \
        memset((p_addr)->m.group_id, 0, 13);            \
        (p_addr)->m.group_id[13] = MCGID_ALL_NODE;  \
    } while(0)

#define NOS_IP6_SET_MC_GID_TO_ALL_RT(p_addr)                 \
    do {                                                \
        memset((p_addr)->m.group_id, 0, 13);            \
        (p_addr)->m.group_id[13] = MCGID_ALL_RT;    \
    } while(0)
// Other multicast IDs are not supported currently.

/*
 * IPv6 Core Functions
 */

/**
 * @brief Add a new IPv6 neighbor.
 *
 * @param[in] srcaddr The neighbor's IPv6 address.
 *
 * @return A created IP6_NEIGHBOR index of the @a my_neighbor array. Out of bound
 *         value of the array means adding fail.
 */
UINT8 nos_ip6_add_neighbor(UINT8 inf, const IP6_ADDRESS *srcaddr);

/**
 * @brief Remove a neighbor @p n from @p my_neighbor.
 *
 * @param[in] n An index of the IPv6 neighbor in the IPv6 interface @a inf.
 */
void nos_ip6_remove_neighbor(UINT8 inf, UINT8 n);

/**
 * @brief The directions of the received packet.
 */
enum ip6_packet_direction
{
    DISCARD_THIS_PACKET  = 0, // should be discard
    PACKET_IS_MINE       = 1, // should be consumed by me
    PACKET_IS_NOT_MINE   = 2, // should be forwarded
};

/**
 * @brief Check the direction of the packet whose destination address is @p addr.
 *
 * @param[in] addr The address to be compared.
 *
 * @return @link enum ip6_packet_direction @endlink values.
 */
enum ip6_packet_direction nos_ip6_addr_for_me(UINT8 inf,
                                              const IP6_ADDRESS *addr);

/**
 * @brief Set the node's state.
 *
 * @param[in] s New state value
 *
 * @return The state of the node.
 */
IP6_NODE_STATE nos_ip6_set_state(UINT8 inf, IP6_NODE_STATE s);

/**
 * @brief Calculate a checksum including pseudo header checksum for upper-layer
 * header.
 *
 * @param[in] ip6 The IPv6 header to be referenced as a pseudo header.
 *
 * @param[in] ipnxt The upper-layer PACKET.
 *
 * @see http://tools.ietf.org/html/rfc2460#section-8.1
 *
 * This function is used on both send and receive IPv6 packets. When an upper-
 * layer sends an IPv6 packet, it should clear the checksum field in its header
 * before passing the packet to the IPv6. Then, the checksum will be done at
 * the IPv6 layer by this function. In contrast, when the upper-layer receives
 * an IPv6 packet, it should call this function to validate its checksum field.
 */
UINT16 nos_ip6_upperlayer_checksum(IP6_HEADER *ip6, PACKET *ipnxt);

/*
 * IPv6 inter-layer functions
 */

/**
 * @brief Pass the @a next packet to the IPv6 layer to send an IPv6 packet.
 *
 * @param[in] src_scope  The scope of the source address
 * @param[in] dst_addr   The IPv6 address that the packet should be destined for.
 * @param[in] payload    The PACKET from upper-layers.
 * @param[in] hop_limit  Hop limit. If 0, output interface's default hop limit
 *                       value is used.
 *
 * @return An error code.
 */
ERROR_T nos_ip6_send(IP6_ADDRTYPE src_scope,
                     const IP6_ADDRESS *dst_addr,
                     UINT8 out_inf,
                     PACKET *payload,
                     UINT8 hop_limit);

/**
 * @brief Pass the @a ippkt to process an IPv6 packet.
 *
 * @param[in] head A head of the PACKET list. It is used to free buffers of
 * the PACKET list at the last consumer layer.
 *
 * @param[in] ippkt A PACKET to be passed to the IPv6 layer.
 *
 * @param[in] sender An IP6_NEIGHBOR who sent the packet from the previous hop.
 *
 */
void nos_ip6_recv(PACKET *head,
                  PACKET *ippkt,
                  UINT8 in_dev,
                  UINT8 sender);

/*
 * IPv6 misc. functions
 */

/**
 * @brief Distinguish the type of the @p addr.
 *
 * @param[in] addr The IPv6 address to be distinguished.
 *
 * @return IP6_ADDRTYPE
 */
IP6_ADDRTYPE nos_ip6_get_addr_type(const IP6_ADDRESS *addr);

/**
 * @brief Counts a length of a multicast address's group ID.
 *
 * @param[in] addr The multicast IPv6 address whose GID length should be counted.
 *
 * @return a number of octets of the group ID.
 */
UINT8 nos_ip6_get_multicast_gid_length(IP6_ADDRESS *addr);

TLV_HEADER *nos_ip6_get_tlv_option(TLV_HEADER *opt, UINT16 remain_len,
		UINT8 type);

BOOL nos_ip6_prefix_matched(const IP6_ADDRESS *target,
                            const IP6_ADDRESS *prefix,
                            UINT8 prefix_len);

struct ip6_recent_dst
{
    BOOL valid:1;
    IP6_ADDRESS dst;
    struct ip6_route route;
    UINT8 age;
};

#define MAX_NUM_IP6_RSM  5
#define MAX_NUM_IP6_DC   3

struct ip6_ctrl
{
    UINT8 nnic;                                  /* number of IPv6 interfaces */
    struct ip6_interface *nic;                   /* IPv6 control blocks */
    struct ip6_reassembly rsm[MAX_NUM_IP6_RSM];  /* reassembly control blocks */
    UINT16 nrt;                                  /* number of routing table */
    struct ip6_routing_entry *rt;                /* IPv6 routing table */
    struct ip6_route default_route;              /* default route */
    struct ip6_recent_dst dc[MAX_NUM_IP6_DC];    /* destination cache */

    //RPL function pointers
    void (*rpl_tick)(void);
    BOOL (*rpl_opt_check)(IP6_OPT_RPL *ro, const IP6_HEADER *ip6, UINT8 sender);

    //UDP function pointers
    void (*udp_input)(UINT8 ip6_inf,
                      PACKET *head,
                      PACKET *ippkt,
                      PACKET *udppkt);

    //TCP function pointers
    void (*tcp_input)(UINT8 ip6_inf,
                      PACKET *head,
                      PACKET *ippkt,
                      PACKET *tcppkt,
                      UINT8 sender);
};

#endif // ~IP6_H
