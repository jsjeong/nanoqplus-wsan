// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Variables and message formats of the ICMPv6.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2011. 5. 18.
 */

#ifndef ICMP6_RELATED_H
#define ICMP6_RELATED_H

#include "nos_common.h"
#include "ip6-types.h"

// ICMP header
#pragma pack(1)
struct _icmp6_message
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT32 value;
};
#pragma pack()
typedef struct _icmp6_message ICMP6_MESSAGE;

enum
{
    ICMP_TYPE_DESTINATION_UNREACHABLE = 1,
    ICMP_TYPE_PACKET_TOO_BIG = 2,
    ICMP_TYPE_TIME_EXCEEDED = 3,
    ICMP_TYPE_PARAMETER_PROBLEM = 4,
    ICMP_TYPE_ECHO_REQUEST = 128,
    ICMP_TYPE_ECHO_REPLY = 129,
    ICMP_TYPE_ROUTER_SOLICITATION = 133,
    ICMP_TYPE_ROUTER_ADVERTISEMENT = 134,
    ICMP_TYPE_NEIGHBOR_SOLICITATION = 135,
    ICMP_TYPE_NEIGHBOR_ADVERTISEMENT = 136,
    ICMP_TYPE_REDIRECT = 137,
    ICMP_TYPE_RPL_CONTROL = 155,
};

#pragma pack(1)
struct _icmp6_dest_unreach
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT32 unused;
};
#pragma pack()
typedef struct _icmp6_dest_unreach ICMP6_MSG_DEST_UNREACHABLE;

enum
{
    ICMP_ERROR_CODE_DSTUNREACH_NO_ROUTE = 0,
    ICMP_ERROR_CODE_DSTUNREACH_ADMIN_PROHIBITED = 1,
    ICMP_ERROR_CODE_DSTUNREACH_BEYOND_SRC_SCOPE = 2,
    ICMP_ERROR_CODE_DSTUNREACH_ADDR_UNREACH = 3,
    ICMP_ERROR_CODE_DSTUNREACH_PORT_UNREACH = 4,
    ICMP_ERROR_CODE_DSTUNREACH_SRCADDR_FAIL_BY_POLICY = 5,
    ICMP_ERROR_CODE_DSTUNREACH_REJECT = 6,
    ICMP_ERROR_CODE_DSTUNREACH_STRICT_SR_FAILED = 7,
};

#pragma pack(1)
struct icmp6_pkt_too_big
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT32 mtu;
};
#pragma pack()

#pragma pack(1)
struct _icmp6_time_exceeded
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT32 unused;
};
#pragma pack()
typedef struct _icmp6_time_exceeded ICMP6_MSG_TIME_EXCEEDED;

enum icmp6_time_exceeded_error_codes
{
    ICMP_ERROR_CODE_HOP_LIMIT_EXCEEDED = 0,
    ICMP_ERROR_CODE_FRAG_REASSEM_TIME_EXCEEDED = 1,
};

#pragma pack(1)
struct _icmp6_param_problem
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT32 pointer;
};
#pragma pack()
typedef struct _icmp6_param_problem ICMP6_MSG_PARAM_PROBLEM;

enum icmp6_param_error_codes
{
    ICMP_ERROR_CODE_ERR_HEADER               = 0,
    ICMP_ERROR_CODE_UNRECOGNIZED_NEXT_HEADER = 1,
    ICMP_ERROR_CODE_UNRECOGNIZED_IP6_OPTION  = 2,
};

#pragma pack(1)
struct _icmp6_echo_request
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT16 identifier;
    UINT16 seq_no;
};
#pragma pack()
typedef struct _icmp6_echo_request ICMP6_MSG_ECHO_REQUEST;

#pragma pack(1)
struct _icmp6_echo_reply
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT16 identifier;
    UINT16 seq_no;
};
#pragma pack()
typedef struct _icmp6_echo_reply ICMP6_MSG_ECHO_REPLY;

#pragma pack(1)
struct icmp6_rs
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT32 reserved;
};
#pragma pack()

#pragma pack(1)
struct icmp6_ra
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT8 cur_hop_limit;
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 reserved:3;
    INT8 prf:2;
    UINT8 h_bit:1;
    UINT8 o_bit:1;
    UINT8 m_bit:1;
#else
    UINT8 m_bit:1;
    UINT8 o_bit:1;
    UINT8 h_bit:1;
    INT8 prf:2;
    UINT8 reserved:3;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT16 rt_lifetime;
    UINT32 reachable_time;
    UINT32 retrans_timer;
};
#pragma pack()

enum
{
    ICMP_RA_PRF_HIGH    = 1,
    ICMP_RA_PRF_MEDIUM  = 0,
    ICMP_RA_PRF_LOW     = -1,
};

#pragma pack(1)
struct icmp6_ns
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT32 reserved;
    IP6_ADDRESS target_address;
};
#pragma pack()

#pragma pack(1)
struct icmp6_na
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 res1:5;
    UINT8 o:1;
    UINT8 s:1;
    UINT8 r:1;
#else
    UINT8 r:1;
    UINT8 s:1;
    UINT8 o:1;
    UINT8 res1:5;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT8 res2;
    UINT16 res3;
    IP6_ADDRESS target_address;
};
#pragma pack()

#pragma pack(1)
struct icmp6_redirect
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT32 reserved;
    IP6_ADDRESS target_addr;
    IP6_ADDRESS dest_addr;
};
#pragma pack()

#pragma pack(1)
struct _icmp6_common_option
{
    UINT8 tlv_type;
    UINT8 tlv_length;
    UINT16 res1;
    UINT32 res2;
} ;
#pragma pack()
typedef struct _icmp6_common_option ICMP6_OPTION;

enum
{
    ICMP_OPT_TYPE_SLLA = 1,
    ICMP_OPT_TYPE_TLLA = 2,
    ICMP_OPT_TYPE_PREFIX = 3,
    ICMP_OPT_TYPE_REDIRECTED = 4,
    ICMP_OPT_TYPE_MTU = 5,
    ICMP_OPT_TYPE_ARO = 31,
    ICMP_OPT_TYPE_6CO = 32,
    ICMP_OPT_TYPE_ABRO = 33,
};

#pragma pack(1)
struct _icmp6_prefix_info_option
{
    UINT8 tlv_type;
    UINT8 tlv_length;
    UINT8 prefix_length;

#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 reserved1:5;
    UINT8 r_bit:1;
    UINT8 a_bit:1;
    UINT8 l_bit:1;
#else
    UINT8 l_bit:1;
    UINT8 a_bit:1;
    UINT8 r_bit:1;
    UINT8 reserved1:5;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT32 valid_lifetime;
    UINT32 preferred_lifetime;
    UINT32 reserved2;
    IP6_ADDRESS prefix;
};
#pragma pack()
typedef struct _icmp6_prefix_info_option ICMP6_OPT_PREFIX;

#pragma pack(1)
struct _icmp6_redirected
{
    UINT8 tlv_type;
    UINT8 tlv_length;
    UINT16 reserved1;
    UINT32 reserved2;
};
#pragma pack()
typedef struct _icmp6_redirected ICMP6_OPT_REDIRECTED_HEADER;
// IP header + data must be followed.

#pragma pack(1)
struct _icmp6_mtu
{
    UINT8 tlv_type;
    UINT8 tlv_length;
    UINT16 reserved;
    UINT32 mtu;
};
#pragma pack()
typedef struct _icmp6_mtu ICMP6_OPT_MTU;

typedef UINT8 ARO_STATUS;
enum
{
    ARO_STATUS_SUCCESS = 0,
    ARO_STATUS_DUP_ADDR = 1,
    ARO_STATUS_NBCACHE_FULL = 2,
};

#pragma pack(1)
struct _icmp6_address_registration_option
{
    UINT8 tlv_type;
    UINT8 tlv_length;
    ARO_STATUS status;
    UINT8 res1;
    UINT16 res2;
    UINT16 reg_lifetime;
    UINT8 eui_64[8];
    // Optional registered address (IP_ADDRESS type) field may follow.
};
#pragma pack()
typedef struct _icmp6_address_registration_option ICMP6_OPT_ARO;

#pragma pack(1)
struct _icmp6_6lowpan_context_option
{
    UINT8 tlv_type;
    UINT8 tlv_length;
    UINT8 context_length;
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 cid:4;
    UINT8 c_flag:1;
    UINT8 res1:3;
#else
    UINT8 res1:3;
    UINT8 c_flag:1;
    UINT8 cid:4;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT16 res2;
    UINT16 valid_lifetime;
    // Context Prefix (variable length) field may follow.
};
#pragma pack()
typedef struct _icmp6_6lowpan_context_option ICMP6_OPT_6CO;

#pragma pack(1)
struct _icmp6_authoritative_border_router_option
{
    UINT8 icmp6_opt_type;
    UINT8 icmp6_opt_length;
    UINT16 version;
    UINT32 reserved;
    IP6_ADDRESS lbr_addr;
};
#pragma pack()
typedef struct _icmp6_authoritative_border_router_option ICMP6_OPT_ABRO;

struct icmp6_ping_cb
{
    UINT8 out_inf;
    UINT8 id;
    IP6_ADDRESS dst;
    UINT16 sz;
    UINT8 max;
    UINT8 cnt;
    void (*callback)(const IP6_HEADER *ip6,
                     UINT16 seq,
                     UINT16 sz,
                     const void *payload);
    UINT8 lifetime;
};

#endif //~ICMP6_RELATED_H
