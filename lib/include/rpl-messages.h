// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * RPL Messages
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 6. 27.
 */

#ifndef RPL_MESSAGES_H
#define RPL_MESSAGES_H

#include "nos_common.h"
#include "ip6-address.h"

enum
{
    RPL_GROUP_NEIGHBOR = 0,
    RPL_GROUP_PARENTS = 128,
    RPL_GROUP_PREFERRED_PARENT = 255,
};

// Messages
enum
{
    RPL_MSG_CODE_DIS = 0x00,         // DODAG Information Solicitation
    RPL_MSG_CODE_DIO = 0x01,         // DODAG Information Object
    RPL_MSG_CODE_DAO = 0x02,         // Destination Advertisement Object
    RPL_MSG_CODE_DAOACK = 0x03,      // DAO Acknowledgment
    RPL_MSG_CODE_SEC_DIS = 0x80,     // Secure DIS
    RPL_MSG_CODE_SEC_DIO = 0x81,     // Secure DIO
    RPL_MSG_CODE_SEC_DAO = 0x82,     // Secure DAO
    RPL_MSG_CODE_SEC_DAOACK = 0x83,  // Secure DAO-ACK
    RPL_MSG_CODE_CON_CHK = 0x8A,     // Consistency Check
};

// DIS message
#pragma pack(1)
struct _rpl_msg_dis
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT8 flags;
    UINT8 reserved;
} ;
#pragma pack()
typedef struct _rpl_msg_dis RPL_MESSAGE_DIS;

// DIO message
#pragma pack(1)
struct _rpl_msg_dio
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT8 instance_id;
    UINT8 version;
    UINT16 rank;
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 prf:3; // Preference
    UINT8 mop:3;// Mode of Operation
    UINT8 res1:1;
    UINT8 g_bit:1;// Grounded
#else
    UINT8 g_bit:1; // Grounded
    UINT8 res1:1;
    UINT8 mop:3;// Mode of Operation
    UINT8 prf:3;// Preference
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT8 dtsn; // Destination Advertisement Trigger Sequence Number
    UINT8 flags;
    UINT8 res2;
    UINT8 dodag_id[16];
} ;
#pragma pack()
typedef struct _rpl_msg_dio RPL_MESSAGE_DIO;

enum
{
    DIO_MOP_NO_DOWNWARDS = 0,
    DIO_MOP_NON_STORING = 1,
    DIO_MOP_STORING_WO_MC = 2,
    DIO_MOP_STORING_W_MC = 3,
};

// DAO message
#pragma pack(1)
struct _rpl_msg_dao
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT8 instance_id;
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 flags:6;
    UINT8 d_bit:1; // whether DODAGID fiend is present or not
    UINT8 k_bit:1;// whether the parent is expected to send DAO-ACK back or not
#else
    UINT8 k_bit:1; // whether the parent is expected to send DAO-ACK back or not
    UINT8 d_bit:1;// whether DODAGID fiend is present or not
    UINT8 flags:6;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT8 res2;
    UINT8 sequence;
} ;
#pragma pack()
typedef struct _rpl_msg_dao RPL_MESSAGE_DAO;

// DAO-ACK message
#pragma pack(1)
struct _rpl_msg_dao_ack
{
    UINT8 icmp6_type;
    UINT8 icmp6_code;
    UINT16 icmp6_checksum;
    UINT8 instance_id;
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 reserved:7;
    UINT8 d_bit:1; // whether DODAGID fiend is present or not
#else
    UINT8 d_bit:1; // whether DODAGID fiend is present or not
    UINT8 reserved:7;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT8 sequence;
    UINT8 status;
} ;
#pragma pack()
typedef struct _rpl_msg_dao_ack RPL_MESSAGE_DAO_ACK;

// TODO:Consistency Check message is not supported.

enum
{
    OPT_TYPE_METRIC_CONTAINER = 2,
    OPT_TYPE_ROUTE_INFO = 3,
    OPT_TYPE_DODAG_CONFIG = 4,
    OPT_TYPE_RPL_TARGET = 5,
    OPT_TYPE_TRANSIT_INFO = 6,
    OPT_TYPE_SOLICITED_INFO = 7,
    OPT_TYPE_PREFIX_INFO = 8,
};

#pragma pack(1)
struct _rpl_msg_opt_metric_container
{
    UINT8 header_type;
    UINT8 header_len;
} ;
#pragma pack()
typedef struct _rpl_msg_opt_metric_container RPL_OPT_METRIC_CONTAINER;

#pragma pack(1)
struct _rpl_msg_opt_metric_mc_object
{
    UINT8 routing_mc_type;
    
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 o_bit:1;
    UINT8 c_bit:1;
    UINT8 p_bit:1;
    UINT8 res_flags:5;
#else //BIT_ORDER_BIG_ENDIAN
    UINT8 res_flags:5;
    UINT8 p_bit:1;
    UINT8 c_bit:1;
    UINT8 o_bit:1;
#endif //BIT_ORDER_BIG_ENDIAN

#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 prec:4;
    UINT8 a:3;
    UINT8 r_bit:1;
#else //BIT_ORDER_BIG_ENDIAN
    UINT8 r_bit:1;
    UINT8 a:3;
    UINT8 prec:4;
#endif //BIT_ORDER_BIG_ENDIAN

    UINT8 length;
} ;
#pragma pack()
typedef struct _rpl_msg_opt_metric_mc_object RPL_OPT_METRIC_MC_OBJECT;

enum
{
    RPL_MC_TYPE_NODE_STATE_ATTR = 1,
    RPL_MC_TYPE_NODE_ENERGY = 2,
    RPL_MC_TYPE_HOP_COUNT = 3,
    RPL_MC_TYPE_LINK_THROUGHPUT = 4,
    RPL_MC_TYPE_LINK_LATENCY = 5,
    RPL_MC_TYPE_LINK_QUALITY = 6,
    RPL_MC_TYPE_LINK_ETX = 7,
    RPL_MC_TYPE_LINK_COLOR = 8,
};

#pragma pack(1)
struct _rpl_msg_opt_metric_ne_sub_object
{
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 e_bit:1;
    UINT8 t:2;
    UINT8 i_bit:1;
    UINT8 flags:4;
#else //BIT_ORDER_BIG_ENDIAN
    UINT8 flags:4;
    UINT8 i_bit:1;
    UINT8 t:2;
    UINT8 e_bit:1;
#endif //BIT_ORDER_BIG_ENDIAN

    UINT8 e_e;
} ;
#pragma pack()
typedef struct _rpl_msg_opt_metric_ne_sub_object RPL_OPT_METRIC_NE_SUB_OBJECT;

enum
{
    RPL_MC_NODE_TYPE_MAINS_POWERED = 0,
    RPL_MC_NODE_TYPE_BATTERY_POWERED = 1,
    RPL_MC_NODE_TYPE_ENERGY_SCAVENGER = 2,
    RPL_MC_NODE_TYPE_UNKNOWN = 4, // out of IANA range
};

#pragma pack(1)
struct _rpl_msg_opt_route_info
{
    UINT8 header_type;
    UINT8 header_len;
    
    UINT8 prefix_length;
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 res2:3;
    UINT8 prf:2;
    UINT8 res1:3;
#else
    UINT8 res1:3;
    UINT8 prf:2;
    UINT8 res2:3;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT32 route_lifetime;
} ;
#pragma pack()
typedef struct _rpl_msg_opt_route_info RPL_OPT_ROUTE_INFO;

#pragma pack(1)
struct _rpl_msg_opt_dodag_config
{
    UINT8 header_type;
    UINT8 header_len;
    
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 pcs:3;
    UINT8 a_bit:1;
    UINT8 flags:4;
#else
    UINT8 flags:4;
    UINT8 a_bit:1;
    UINT8 pcs:3;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT8 dio_iv_doub;
    UINT8 dio_iv_min;
    UINT8 dio_redundancy;
    UINT16 max_rank_increase;
    UINT16 min_hop_rank_increase;
    UINT16 ocp;
    UINT8 reserved;
    UINT8 def_lifetime;
    UINT16 lifetime_unit;
} ;
#pragma pack()
typedef struct _rpl_msg_opt_dodag_config RPL_OPT_DODAG_CONFIG;

#pragma pack(1)
struct _rpl_msg_opt_rpl_target
{
    UINT8 header_type;
    UINT8 header_len;

    UINT8 flags;
    UINT8 prefix_length;
} ;
#pragma pack()
typedef struct _rpl_msg_opt_rpl_target RPL_OPT_RPL_TARGET;

#pragma pack(1)
struct _rpl_msg_opt_transit_info
{
    UINT8 header_type;
    UINT8 header_len;
    
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 flags:7;
    UINT8 e_bit:1;
#else
    UINT8 e_bit:1;
    UINT8 flags:7;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT8 path_control;
    UINT8 path_sequence;
    UINT8 path_lifetime;
} ;
#pragma pack()
typedef struct _rpl_msg_opt_transit_info RPL_OPT_TRANSIT_INFO;

#pragma pack(1)
struct _rpl_msg_opt_solicited_info
{
    UINT8 header_type;
    UINT8 header_len;
    
    UINT8 instance_id;
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 flags:5;
    UINT8 d_bit:1;
    UINT8 i_bit:1;
    UINT8 v_bit:1;
#else
    UINT8 v_bit:1;
    UINT8 i_bit:1;
    UINT8 d_bit:1;
    UINT8 flags:5;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT8 dodagid[16];
    UINT8 version;
} ;
#pragma pack()
typedef struct _rpl_msg_opt_solicited_info RPL_OPT_SOLICITED_INFO;

#pragma pack(1)
struct _rpl_msg_opt_prefix_info
{
    UINT8 header_type;
    UINT8 header_len;
    
    UINT8 prefix_length;
#ifdef BIT_ORDER_LITTLE_ENDIAN
    UINT8 res1:5;
    UINT8 r_bit:1;
    UINT8 a_bit:1;
    UINT8 l_bit:1;
#else
    UINT8 l_bit:1;
    UINT8 a_bit:1;
    UINT8 r_bit:1;
    UINT8 res1:5;
#endif /* BIT_ORDER_LITTLE_ENDIAN */
    UINT32 valid_lifetime;
    UINT32 preferred_lifetime;
    UINT32 res2;
    IP6_ADDRESS prefix;
} ;
#pragma pack()
typedef struct _rpl_msg_opt_prefix_info RPL_OPT_PREFIX_INFO;

#endif //RPL_MESSAGES_H
