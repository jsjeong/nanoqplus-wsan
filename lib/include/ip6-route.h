// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * IPv6 Routing Module
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 7. 31.
 */

#ifndef IP6_ROUTE_H
#define IP6_ROUTE_H

#include "nos_common.h"
#include "errorcodes.h"
#include "ip6-types.h"

ERROR_T nos_ip6_add_route(const IP6_ADDRESS *prefix,
                          UINT8 prefix_len,
                          UINT8 egress_inf,
                          UINT8 nxhop);

ERROR_T nos_ip6_remove_route_by_prefix(const IP6_ADDRESS *prefix,
                                       INT8 prefix_len);

void nos_ip6_remove_route_by_neighbor(UINT8 inf, UINT8 neigh);

void nos_ip6_get_route(const IP6_ADDRESS *dst, struct ip6_route *route);

struct ip6_routing_entry *nos_ip6_find_route(const IP6_ADDRESS *prefix,
                                             UINT8 prefix_len);

UINT8 nos_ip6_add_dst_cache(const IP6_ADDRESS *dst, struct ip6_route *route);

/**
 * Called when a link's MTU is changed.
 */
void nos_ip6_update_dst_cache_pmtu(UINT8 inf);

#endif //IP6_ROUTE_H
