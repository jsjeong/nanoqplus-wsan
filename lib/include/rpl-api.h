// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * RPL Application Programming Interfaces
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 11. 7.
 */

#ifndef RPL_API_H
#define RPL_API_H

#include "nos_common.h"
#include "errorcodes.h"
#include "ip6-address.h"

void rpl_core_init(UINT8 ndodag);

/**
 * Initialize the RPL protocol for an IPv6 interface.
 */
UINT8 rpl_init(UINT8 ip6_inf);

enum rpl_ocp
{
    RPL_OCP_OF0 = 0,
    RPL_OCP_MRHOF = 1,
};

enum rpl_mop
{
    RPL_MOP_NO_DOWNWARD = 0,
    RPL_MOP_NON_STORING = 1,
    RPL_MOP_STORING_NO_MC = 2,
    RPL_MOP_STORING_MC = 3,
    RPL_MOP_P2P = 4,
    RPL_MOP_HYBRID = 5,
};

/**
 * Initialize the node as a RPL root.
 *
 * @return < maximum number of RPL interfaces: Success
 *         >= maximum numbrer of RPL interfaces: Fail
 */
UINT8 rpl_root_init(UINT8 ip6_inf,
                    UINT8 inst_id,
                    IP6_ADDRESS *dodag_id,
                    UINT8 ocp,
                    UINT8 mop,
                    UINT16 min_hop_rank_increase,
                    UINT16 lft_unit,
                    UINT8 lft_default,
                    UINT16 src_rt_table_len);

enum rpl_stat_type
{
    RPL_STAT_UPWARD_LOOP,
    RPL_STAT_RANK_ERROR,
    RPL_STAT_DOWNWARD_LOOP,
    RPL_STAT_FWD_ERROR,
    RPL_STAT_POISON,
};
UINT32 rpl_get_stat(UINT8 ip6_inf, enum rpl_stat_type stat);

#define rpl_get_upward_loop_cnt(inf)        rpl_get_stat(inf, RPL_STAT_UPWARD_LOOP)
#define rpl_get_rank_err_cnt(inf)           rpl_get_stat(inf, RPL_STAT_RANK_ERROR)
#define rpl_get_downward_loop_cnt(inf)      rpl_get_stat(inf, RPL_STAT_DOWNWARD_LOOP)
#define rpl_get_fwd_err_cnt(inf)            rpl_get_stat(inf, RPL_STAT_FWD_ERROR)
#define rpl_get_poison_cnt(inf)             rpl_get_stat(inf, RPL_STAT_POISON)

/**
 * @brief Get the DODAG root address.
 *
 * @param[out] a An IP6_ADDRESS where the root address is stored at.
 */
IP6_ADDRESS *rpl_get_root_address(UINT8 ip6_inf, IP6_ADDRESS *a);

/**
 * @brief Get an OCP value.
 *
 * @param[in] ip6_inf  IPv6 interface ID
 */
UINT8 rpl_get_ocp(UINT8 ip6_inf);

/**
 * @brief Get my RPL rank.
 *
 * @param[in] ip6_inf  IPv6 interface ID
 */
UINT16 rpl_get_rank(UINT8 ip6_inf);

/**
 * @brief Set a DAO interval.
 *
 * @param[in] ip6_inf  IPv6 interface ID
 * @param[in] sec      DAO interval in unit of seconds.
 *
 * @return
 *  * ERROR_SUCCESS       Success
 *  * ERROR_INVALID_ARGS  @p ip6_inf is not valid ID.
 *  * ERROR_FAIL          Failure when the node is configured to some modes that
 *                        is not required to send DAOs.
 */
ERROR_T rpl_set_dao_interval(UINT8 ip6_inf, UINT16 sec);

/**
 * @brief Enable OF0.
 */
void rpl_enable_of0(void);

#endif //RPL_API_H
