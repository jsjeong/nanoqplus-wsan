// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file 6lowpan.h
 * @brief Types and functions related with 6LoWPAN
 * @author Jongsoo Jeong (ETRI)
 * @date 2010. 2. 1.
 * @see http://tools.ietf.org/html/rfc4944
 * @see http://tools.ietf.org/html/rfc6282
 */

#ifndef LOWPAN_H
#define LOWPAN_H

#include "nos_common.h"
#include "ip6.h"
#include "ieee-802-15-4.h"

/**
 * @brief Structure that is used when frame reassembly.
 */
struct reassembled
{
    BOOL valid;
    BOOL src_eui64;     // Whether the source ID is long (EUI-64), or not.
    UINT8 src_id[8];    // Source ID. If it is short, most significant 2bytes are used.
    UINT16 tag;         // Datagram Tag
    UINT16 sz;          // buffer size
    UINT8 *buf;         // buffer pointer
    UINT8 bitmap[32];   // Bitmap to mark received blocks.
    UINT8 checked;      // The bitmap index that indicates a block to be checked on next time.
    INT8 discard_timer; // Reassemble timer ID
};


struct lowpan_context
{
    BOOL valid:1;
    IP6_ADDRESS prefix;
    UINT8 prefix_length;
};

struct border_router
{
    BOOL valid;
    UINT16 ver;
    IP6_ADDRESS addr;
};

struct lowpan_neighbor
{
    BOOL eui64_valid:1;
    BOOL short_addr_valid:1;
    UINT16 short_addr;
    UINT8 eui64[8];
    UINT16 link_etx;    // Link Quality (One-hop Expected Tx Count * 128)
    INT8 rssi;
    UINT8 lqi;

    union
    {
        struct
        {
            UINT8 reg_retries;    
            UINT32 reg_lifetime;
        } router; //Used in the host mode

        struct
        {
            //TODO States related with hosts' address registration.
            UINT8 tmp;
        } host;   //Used in the router mode
    } t;
};

enum { LOWPAN_MAX_FRAME_REASSEMBLY = 3 };
enum { LOWPAN_MAX_NUM_CONTEXTS = 16 };

struct lowpan_interface
{
    BOOL valid:1;
    BOOL eui64_assigned:1;
    BOOL short_id_assigned:1;
    BOOL short_id_confirmed_as_unique:1;
    BOOL rxing:1;
    UINT8 dev_id;
    UINT16 short_id;
    UINT8 eui64[8];
    UINT8 rs_retries;
    UINT16 rs_tick;
    UINT8 ra_cnt;
    UINT16 ra_tick;
    IEEE_802_15_4_SEC_MODE sec_mode;
    struct lowpan_context contexts[LOWPAN_MAX_NUM_CONTEXTS];
    struct border_router br;
    struct reassembled rsm_cb[LOWPAN_MAX_FRAME_REASSEMBLY];
    NOS_MAC_RX_INFO frame_buffer;
    struct lowpan_neighbor *neighbor;
};

#define GET_6LOWPAN_INFO(i)  ((struct lowpan_interface *)(ip6_ctrl.nic[i].dev))

/**
 * @brief Set an @p prefix / @p plen to one of context informations.
 *
 * @param[in] id An index of context where @p prefix is stored at.
 *
 * @param[in] prefix An IPv6 prefix to be stored. If it is NULL, the context
 * whose ID is @p id is marked as invalid.
 *
 * @param[in] plen Prefix length of @p prefix.
 *
 * @return Error code.
 */
ERROR_T nos_lowpan_set_context(struct lowpan_interface *linf,
                               UINT8 id,
                               IP6_ADDRESS *prefix,
                               UINT8 plen);

/**
 * @brief Get an @p prefix whose ID is @p id.
 *
 * @param[in] id An index of context.
 *
 * @parma[out] prefix A prefix whose ID is @p id.
 *
 * @return 0 is returned when either @p id is greater than 15, or @p prefix is
 * NULL, or @p id context is marked as invalid. Otherwise, it returns valid
 * prefix length.
 */
UINT8 nos_lowpan_get_context(struct lowpan_interface *linf,
                             UINT8 id,
                             IP6_ADDRESS *prefix);

/**
 * @brief Set IEEE 802.15.4 security (AES-128) mode.
 *
 * @param[in] dev   6LoWPAN interface control block pointer
 * @param[in] mode  Securit mode
 */
void nos_lowpan_set_security_mode(void *dev, IEEE_802_15_4_SEC_MODE mode);

struct lowpan_ctrl
{
    UINT8 nnic;
    struct lowpan_interface *nic;
};

#endif // ~LOWPAN_H
