// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 */

#ifndef RPL_TYPES_H
#define RPL_TYPES_H

#include "nos_common.h"
#include <trickle.h>
#include "rpl-messages.h"
#include "errorcodes.h"

// Constants
enum rpl_rank
{
    RPL_RANK_BASE = 0,
    RPL_RANK_ROOT = 1,
    RPL_RANK_INFINITE = 0xFFFF,
};

typedef enum
{
    RPL_I_AM_INITED,
    RPL_I_AM_JOINED,
    RPL_I_AM_POISONING,
    RPL_I_AM_A_LEAF,
} DODAG_STATE;

struct rpl_stat
{
    UINT32 upward_loop_pkt_count;
    UINT32 rank_error_pkt_count;
    UINT32 downward_loop_pkt_count;
    UINT32 fwd_error_pkt_count;
    UINT32 poison_count;
};

struct rpl_neighbor
{
    UINT16 rank;
    UINT8 path_control;
    UINT8 dtsn;

    BOOL pc_changed:1;
    
#if defined(RPL_OF0_PARENT_SELECT_MIN_ENERGY) || defined(RPL_OF0_PARENT_SELECT_MAX_ENERGY)
    UINT8 node_energy;
    UINT8 node_type;
#endif
};

struct rpl_src_rt_entry
{
    BOOL valid:1;
    IP6_ADDRESS parent;
    UINT8 path_ctrl;
    UINT8 path_seq;
    UINT32 lifetime;
};

struct dodag_info
{
    BOOL valid:1;
    BOOL grounded:1;
    BOOL dao_ack_waiting:1;
    BOOL increment_dtsn:1;
    BOOL help_neighbor:1;

    UINT8 ip6_inf;
    
    DODAG_STATE state;
    UINT8 id[16];
    UINT8 rpl_instance_id;
    UINT8 ver;
    UINT16 my_rank;
    UINT16 ocp;
    UINT8 mop;
    UINT8 preference;
    UINT8 pcs;
    UINT16 min_hop_rank_increase;
    UINT16 max_rank_increase;
    UINT8 dtsn;
    UINT8 dao_seq;
    UINT8 path_seq;
    UINT8 default_lifetime;
    UINT16 lifetime_unit;

    UINT16 instance_lost_timeout;
    UINT16 instance_lost_count_down;
    
    UINT16 dao_interval;
    UINT16 dao_count_down;
    
    TRICKLE dio_timer;
    
    UINT8 dis_retries;
    UINT16 dis_count_down;

    UINT8 pref_parent;
    struct rpl_neighbor *neighbor;

    // Source routing
    ERROR_T (*src_rt_update)(UINT8 ip6_inf,
                             const IP6_ADDRESS *src,
                             const RPL_MESSAGE_DAO *dao,
                             UINT16 dao_len);
    ERROR_T (*src_rt_install)(UINT8 ip6_inf,
                              const IP6_ADDRESS *prefix,
                              UINT8 prefix_len,
                              const IP6_ADDRESS *parent,
                              UINT8 path_control,
                              UINT8 path_sequence,
                              UINT8 path_lifetime);
    void (*src_rt_lifetime_tick)(struct dodag_info *dodag);
    struct rpl_src_rt_entry *src_rt;
    UINT16 src_rt_len;

    // Node Energy Constraints
    RPL_OPT_METRIC_NE_SUB_OBJECT mains_powered_nodes;
    RPL_OPT_METRIC_NE_SUB_OBJECT battery_powered_nodes;
    RPL_OPT_METRIC_NE_SUB_OBJECT energy_scavenging_nodes;

    // Statistics.
    struct rpl_stat stats;
};

#define DODAG_CONSTRAINS_MAINS_POWERED(d)    ((d)->mains_powered_nodes.flags == 0)
#define DODAG_CONSTRAINS_BATT_POWERED(d)     ((d)->battery_powered_nodes.flags == 0)
#define DODAG_CONSTRAINS_EN_SCAVENGER(d)     ((d)->energy_scavenging_nodes.flags == 0)

#define DODAG_RESET_MAINS_POWERED_CONSTR(d)  do { (d)->mains_powered_nodes.flags = 1; } while(0)
#define DODAG_RESET_BATT_POWERED_CONSTR(d)   do { (d)->battery_powered_nodes.flags = 1; } while(0)
#define DODAG_RESET_EN_SCAVENGER_CONSTR(d)   do { (d)->energy_scavenging_nodes.flags = 1; } while(0)

#define SEQ_INCREMENT(a) (a = ((a) == 255 || (a) == 127) ? 0 : a + 1)

struct rpl_ctrl
{
    UINT8 ndodag;
    struct dodag_info *dodag;

    BOOL (*of0_refresh)(struct dodag_info *d);
    BOOL (*mrhof_refresh)(struct dodag_info *d);
};

#endif //~RPL_TYPES_H
