/*
 * Copyright (c) 2006-2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Header for the UDP.
 * @author Jongsoo Jeong (ETRI)
 * @author Junkeun Song (ETRI)
 * @date 2013. 8. 16.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "kconf.h"

#ifndef TCP_H
#define TCP_H

#ifdef TCP_M

#include "nos_common.h"
#include "ip6-types.h"
#include "taskq.h"
#include "errorcodes.h"
#include "tcp-types.h"
#include "sem.h"

#define TCP_MAX_CONNS	10	// max conn
#define TCP_MAX_EVENTS	20


#define TCP_TIMER_INTERVAL	300	// in millisecond

#define TMP_IP_MTU	127	
#define DEFAULT_MSS	100

// to limit using heap memory for buffer
#define TCP_LIMIT_SND_BUF	2048		
#define TCP_LIMIT_RCV_BUF	2048

// defalut window size for send & recv. at least (2*DEFAULT_MSS) 
#define TCP_DEFAULT_SND_WND		(4*DEFAULT_MSS)	
#define TCP_DEFAULT_RCV_WND		(4*DEFAULT_MSS)


#define MSG_DONTWAIT	1

// TCP States
enum _tcp_state {
  TS_CLOSED      = 0,
  TS_LISTEN      = 1,
  TS_SYN_SENT    = 2,
  TS_SYN_RCVD    = 3,
  TS_ESTABLISHED = 4,
  TS_FIN_WAIT_1  = 5,
  TS_FIN_WAIT_2  = 6,
  TS_CLOSE_WAIT  = 7,
  TS_CLOSING     = 8,
  TS_LAST_ACK    = 9,
  TS_TIME_WAIT   = 10
};

//--------------------------------
// TCP segment
//--------------------------------
struct tcp_seg
{
	struct tcp_seg *next;
	PACKET *p;
	UINT16 checksum;

	TCP_HEADER *tcph;	// pointing tcp header in p->buf

};



//--------------------------------
// TCP connection
//--------------------------------
struct tcp_conn
{
	enum _tcp_state state;
	IP6_ADDRESS local_addr;
	IP6_ADDRESS remote_addr;
	UINT16 local_port;
	UINT16 remote_port;

	UINT16 mss;	// currnet maximum segment size
	UINT8  c_state;

	INT8 pto;	// persist time-out count
	INT8 rto_left;	// retransmission time-out count
	INT8 rto;	// rto calculated by rtt
	UINT8	nrtx;	// number of retransmissions
			// use next accpet tc idx for listening mode
    UINT8  dupack_cnt;

	//for sending
	// http://web.cs.wpi.edu/~rek/Undergrad_Nets/C02/TCP_SlidingWindows.pdf
	UINT16	send_ws; // SWS
	UINT32	lastack;	// LAR: higest acked seqno
	UINT32  snd_nxt; // LFS: the seq number that was last sent.

	// for receive
	UINT16 receive_ws;
	UINT32 rcv_nxt;	// LFR. // the seq number expected to be receive next
	UINT32 rcv_max_acceptable;	// LAF.
	UINT32 read_nxt;	// next reading sequence from application
	UINT16 rcv_adv_wnd;	// advertised wnd

	// for RTT
	UINT32	rtt_sent_seq;	// seq no being timed
	UINT32	rtt_sent_tick;	// tick @ segment enqueued to ip.
	UINT16	rtt;	// in tick

	SEMAPHORE op_sem;
	SEMAPHORE recv_sem;
	struct tcp_seg *unsent;
	struct tcp_seg *unacked;

        PACKET *up_pkt;

	UINT8	need_to_send_ack;
	UINT8   pending_close;


};

#define CS_NONE		0
#define CS_OP_BLOCK	1
#define CS_RECV_BLOCK	2

//--------------------------------
// TCP EVENT, event args
//--------------------------------
struct tcp_event_args {
	UINT8	tcp_conn_idx;
	union {
		// for bind, accept, connect
	    struct {
	      IP6_ADDRESS ipaddr;
	      UINT16 port;
	    } addr;	
		// for send, recv
	    struct {
	      void * dataptr;
	      UINT16 len;
		  UINT8 flag;
	    } data;	
// input from ip6 interlayer
	    struct {
			UINT8 in_inf;
				PACKET *head;
				IP6_HEADER *ip6;
				PACKET *tcppkt;	      
				UINT8 sender;
	    } input;				
	} arg;
};


// 
/**
 * @brief Initialize the TCP protocol.
 *
 * Initialize the TCP port list.
 */
void nos_tcp_init(void);

/**
 * @brief Open a TCP connection.
 *
 * Send a segment using established connection @a conn_idx .
 *
 * The segment is at @a buf and has @a len bytes.
 *
 * @param[in] tcp_conn_idx The index of tcp connection.
 *
 * @param[in] buf The segment to be sent.
 *
 * @param[in] len The length of @a buf.
 *
 * @return index of established tcp connection. -1 if  An error code.
 */
int nos_tcp_connect(IP6_ADDRESS ip, UINT16 port);

// 바로 오픈. or 할당만 하고 추후 오픈?
// contiki는 나중에 오픈함.

/**
 * @brief Close a tcp connection.
 *
 * Close the connection and put in a closing state.
 *
 * Resources are freed in tcp timer.
 *
 * @param[in] tcp_conn_idx The index of tcp connection.
 *
 * @return An error code.
 */
int nos_tcp_close(UINT8 tcp_conn_idx);


/**
 * @brief Send a segment.
 *
 * Send a segment using established connection @a conn_idx .
 *
 * The segment is at @a buf and has @a len bytes.
 *
 * @param[in] tcp_conn_idx The index of tcp connection.
 *
 * @param[in] buf The segment to be sent.
 *
 * @param[in] len The length of @a buf.
 *
 * @return An error code.
 */
int nos_tcp_send(UINT8 tcp_conn_idx,void *user_data, UINT16 len);

/**
 * @brief Pass an IPv6 packet to the UDP layer to receive.
 *
 * Pass the UDP packet @a udp whose IPv6 header is @a ip6, and head of PACKET
 * list is @a head to the UDP layer. Then, post @link udp_recv_task() @endlink
 * to the Task Queue.
 *
 * @param[in] head
 *
 * @param[in] ip6
 *
 * @param[in] udp
 *
 * @return
 */

int nos_tcp_recv(UINT8 tc_idx, void *buf, UINT16 len, UINT8 flags);


void tcp_input(UINT8 in_inf, PACKET *head, IP6_HEADER *ip6, PACKET *uptpkt, UINT8 sender);


typedef void (*TCP_CALLBACK_ON_RECV)(UINT16 tcp_conn_idx, const char *msg, UINT16 len);

int nos_tcp_bind(IP6_ADDRESS ip, UINT16 port);

ERROR_T nos_tcp_listen(UINT8 tc_idx);

/**
 * called by ip6-core for every 1 sec
 */
void tcp_tick(void);

int nos_tcp_accept(UINT8 listen_tc_idx, IP6_ADDRESS *cli_ip, UINT16 *cli_port);

#endif

#endif
