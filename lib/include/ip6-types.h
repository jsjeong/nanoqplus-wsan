// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Variables and message formats for IPv6.
 * @author Jongsoo Jeong (ETRI)
 * @date 2010. 2. 1
 *
 * @see http://tools.ietf.org/html/rfc2460
 */

#ifndef IP6_TYPES_H
#define IP6_TYPES_H

#include "nos_common.h"
#include "ip6-address.h"
#include "packet.h"

enum ip6_node_state
{
    IP6_NODE_STATE_NONE = 0,
    IP6_NODE_STATE_BECOMING_HOST = 1,
    IP6_NODE_STATE_DUPLICATE = 2,
    IP6_NODE_STATE_HOST = 3,
    IP6_NODE_STATE_BECOMING_ROUTER = 4,
    IP6_NODE_STATE_ROUTER = 5,
};
typedef enum ip6_node_state IP6_NODE_STATE;

struct ip6_reassembly_buf
{
    PACKET p;
    UINT8 d[2048];
    UINT8 progress[32]; /* 32 * 8 bits = 256 bits
                         * 256 bits * 8 octets/bit = 2048 octets */
};

struct ip6_reassembly
{
    BOOL valid:1;
    BOOL first_rcvd:1;
    BOOL last_rcvd:1;
    UINT8 lifetime;
    UINT8 sender_1hop;
    UINT8 in_inf;
    IP6_ADDRESS src;
    IP6_ADDRESS dst;
    UINT32 id;
    UINT16 total_len;
    struct ip6_reassembly_buf *buf[32]; /* 32 bufs * 2048 octets/buf
                                         * = 65536 octets */
    PACKET unfrag;
};

typedef void (*IP6_STATE_NOTIFIER)(UINT8 ip6_inf, IP6_NODE_STATE state);

/// Router preference values
enum ip6_rt_pref
{
    RT_PREFERENCE_HIGH = 1,
    RT_PREFERENCE_MEDIUM = 0,
    RT_PREFERENCE_LOW = -1,
};

/**
 * @brief IPv6 Header
 * @see http://tools.ietf.org/html/rfc2460#section-3
 */
#pragma pack(1)
struct ip6_header
{
#ifdef BIT_ORDER_BIG_ENDIAN
    UINT8 ver:4;
    UINT8 traffic_class_h:4;
    UINT8 traffic_class_l:4;
    UINT8 flow_label_h:4;
#else
    UINT8 traffic_class_h :4;
    UINT8 ver :4;
    UINT8 flow_label_h :4;
    UINT8 traffic_class_l :4;
#endif /* BIT_ORDER_BIG_ENDIAN */
    UINT16 flow_label;
    UINT16 payload_len;
    UINT8 next_hdr;
    UINT8 hop_limit;
    IP6_ADDRESS src_addr;
    IP6_ADDRESS dst_addr;
} ;
#pragma pack()
typedef struct ip6_header IP6_HEADER;

enum protocol
{
    PROTOCOL_IP6E_HOP_BY_HOP  = 0,   // ext. header
    PROTOCOL_TCP              = 6,   // upper layer
    PROTOCOL_UDP              = 17,  // upper layer
    PROTOCOL_IP6              = 41,  // IPv6 encapsulation
    PROTOCOL_IP6E_ROUTING     = 43,  // ext. header
    PROTOCOL_IP6E_FRAGMENT    = 44,  // ext. header
    PROTOCOL_ICMP6            = 58,  // upper layer
    PROTOCOL_NO_NEXT          = 59,  // no next header
    PROTOCOL_IP6E_DESTINATION = 60,  // ext. header
    PROTOCOL_IP6E_MOBILITY    = 135, // ext. header
    PROTOCOL_UNKNOWN          = 255, // reserved value (IANA)
                                     //.we use it to indicate unknown data.
};

/**
 * @brief Common fields of IPv6 Extension Headers
 * @see http://tools.ietf.org/html/rfc2460#section-4
 */
#pragma pack(1)
struct ip6_ext_header
{
    UINT8 nx_hdr;
    UINT8 hdr_len;
} ;
#pragma pack()
typedef struct ip6_ext_header IP6_EXT_HEADER;

/**
 * @brief IPv6 Routing extension header
 * http://tools.ietf.org/html/rfc2460#section-4.4
 */
#pragma pack(1)
struct ip6_ext_rt
{
    UINT8 nx_hdr;
    UINT8 hdr_len;
    UINT8 rt_type;
    UINT8 seg_lft;
} ;
#pragma pack()
typedef struct ip6_ext_rt IP6_EXT_RT;

/// IPv6 Routing Type values
enum ip6_rt_type
{
    IP6_RT_TYPE_RPL = 3,
};

/**
 * @brief IPv6 Routing Header for Source Routes with RPL
 * @see http://tools.ietf.org/html/draft-ietf-6man-rpl-routing-header-03
 */
#pragma pack(1)
struct ip6_ext_rt_rpl
{
    IP6_EXT_RT common_hdr;
#ifdef BIT_ORDER_BIG_ENDIAN
    UINT8 cmpr_i:4;
    UINT8 cmpr_e:4;
    UINT8 pad:4;
    UINT8 res1:4;
#else
    UINT8 cmpr_e:4;
    UINT8 cmpr_i:4;
    UINT8 res1:4;
    UINT8 pad:4;
#endif /* BIT_ORDER_BIG_ENDIAN */
    UINT16 res2;
} ;
#pragma pack()
typedef struct ip6_ext_rt_rpl IP6_EXT_RT_RPL;

/**
 * @brief IPv6 Fragment Header
 * @see http://tools.ietf.org/html/rfc2460#section-4.5
 */
#pragma pack(1)
struct ip6_ext_frag
{
    UINT8 nx_hdr;
    UINT8 res1;
    UINT8 offset_h;
#ifdef BIT_ORDER_BIG_ENDIAN
    UINT8 offset_l:5;
    UINT8 res2:2;
    UINT8 m:1;
#else
    UINT8 m:1;
    UINT8 res2:2;
    UINT8 offset_l:5;
#endif /* BIT_ORDER_BIG_ENDIAN */
    UINT32 id;
} ;
#pragma pack()
typedef struct ip6_ext_frag IP6_EXT_FRAG;

/**
 * @brief IPv6 Options' TL (Type and Length)
 */
#pragma pack(1)
struct tlv_header
{
    UINT8 tlv_type;
    UINT8 tlv_len;
} ;
#pragma pack()
typedef struct tlv_header TLV_HEADER;

enum
{
    TLV_TYPE_PAD1 = 0,
    TLV_TYPE_PADN = 1,
};

/*
 * [7:6] - action when the option is not recognized.
 * [5]   - data may change en-route
 * [4:0] - rest value: actual type
 */
enum ip6_options
{
    IP6_OPT_TYPE_PAD1            = ((0 << 6) | (0 << 5) | 0),
    IP6_OPT_TYPE_PADN            = ((0 << 6) | (0 << 5) | 1),
    IP6_OPT_TYPE_JUMBO_PAYLOAD   = ((3 << 6) | (0 << 5) | 2),
    IP6_OPT_TYPE_RPL_OPTION      = ((1 << 6) | (1 << 5) | 3),
    IP6_OPT_TYPE_TUN_ENCAP_LIMIT = ((0 << 6) | (0 << 5) | 4),
    IP6_OPT_TYPE_ROUTER_ALERT    = ((0 << 6) | (0 << 5) | 5),
    IP6_OPT_TYPE_QUICK_START     = ((0 << 6) | (1 << 5) | 6),
    IP6_OPT_TYPE_CALIPSO         = ((0 << 6) | (0 << 5) | 7),
    IP6_OPT_TYPE_SMF_DPD         = ((0 << 6) | (0 << 5) | 8),
    IP6_OPT_TYPE_HOME_ADDR       = ((3 << 6) | (0 << 5) | 9),
    IP6_OPT_TYPE_ILNP_NONCE      = ((2 << 6) | (0 << 5) | 11),
    IP6_OPT_TYPE_LINE_ID_OPTION  = ((2 << 6) | (0 << 5) | 12),
    IP6_OPT_TYPE_MPL_OPTION      = ((1 << 6) | (1 << 5) | 13),
    IP6_OPT_TYPE_IP_DFF          = ((3 << 6) | (1 << 5) | 14),
    IP6_OPT_TYPE_EXPERIMENT      = 30, //MS 3-bits are don't care.
};

/**
 * @brief RPL Option for Carrying RPL Information in Data-Plane Datagrams
 * @see http://tools.ietf.org/html/draft-ietf-6man-rpl-option-03
 */
#pragma pack(1)
struct ip6_opt_rpl
{
    TLV_HEADER header;
#ifdef BIT_ORDER_BIG_ENDIAN
    UINT8 o_bit:1;
    UINT8 r_bit:1;
    UINT8 f_bit:1;
    UINT8 res:5;
#else
    UINT8 res:5;
    UINT8 f_bit:1;
    UINT8 r_bit:1;
    UINT8 o_bit:1;
#endif /* BIT_ORDER_BIG_ENDIAN */
    UINT8 rpl_instance_id;
    UINT16 sender_rank;
} ;
#pragma pack()
typedef struct ip6_opt_rpl IP6_OPT_RPL;

struct ip6_route
{
    UINT8 inf;
    UINT8 nxhop;
    UINT16 pmtu;
};

struct ip6_routing_entry
{
    BOOL valid:1;
    IP6_ADDRESS prefix;
    UINT8 prefix_len;
    UINT8 netif;
    UINT8 nexthop;
    INT16 rt_opt;
};

#endif //~IP6_TYPES_H
