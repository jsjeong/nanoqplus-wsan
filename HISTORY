######################################################################

 		     NanoQplus OS 2 (Nano OS)
		    	   H I S T O R Y 

   ETRI (Electronics and Telecommunications of Research Institute)

######################################################################

Ver 2.5.5 (2014. 3. 6.)
=======================

 1. The IPv6 stack is now IPv6 Ready Logo approved!
   - Logo ID: 02-C-001149 (Phase-2, Core Protocols, Host)
 2. IETF protocol libraries for the Internet of Things were added.
   - RPL: IPv6 Routing Protocol for Low-power and Lossy Networks
   - CoAP: Constrained Application Protocol
   - TCP: Transport Control Protocol
 2. Various platforms were added.
   - Kmote, IRIS, iSN-903N, Mango-EToI, Mango-Z1, Z1
 3. Various device drivers are added.
   - Wireless PAN devices: ADF7023-J, CC1120, CC2520, MG2410, AT86RF212, AT86RF230
   - Ethernet controller: ENC28J60
   - Flash memory: M25PX16
   - Sensors: Apollo, BH1600, ELT-S100, MICS5132
 4. Glossy MAC was added.


Ver 2.5.0 (2012. 9. 18.)
========================

 1. 6LoWPAN & IPv6
   - newly added.
 2. TENO routing
   - minor change


Ver 2.4.1 (2010. 2. 4.)
=======================

 1. NanoMAC
   - mac_rx_on() and mac_rx_off() has been added.
     CC2420 oscillator will be disabled by mac_rx_off.
     TX command can be used even if mac_rx_off() was called.

 2. yp128(YoungPoong corp.), a new platform was added.
   - with Ultrasonic sensor


Ver 2.4.0 (2009. 3. 12.)
========================

 1. Configuration of NanoQplus
   - Need to export $CYGWIN_HOME (bash ex: export $CYGWIN_HOME=/cygdrive/c/cygwin)
   - You can install cygwin anywhere now.

 2. UART module for MSP430
   - The periodic fast calibration was removed. It makes a problem when many interrupts are occurred.
   - It was added at v2.3.6 

 3. NanoMAC for ZigbeX
   - There was a bug that ZigbeX was halted when mac_init() is called because RX interrupt configuration was wrong. Fixed now.

 4. RENO routing protocol
   - Bug fixed.

 5. UBee430(Hanback corp.), a new platform was added.
   - with luminance, temperature, humidity, human detect and door open check sensors.

 6. Lots of sensor modules were added.


Ver 2.3.6b (2009. 1. 30.)
=========================

 1.  Kernel module
   1.1 thread.h, thread_sleep.c, user_timer.h and user_timer.c
     - Fixed bug: "thread create_ms"(or sec) and "timer_create_ms" (or sec) could be run on wrong time because of UINT16 overflow.
     - The maximum value for time parameter of "thread_create_sec" or "timer_create_sec" is defined as THREAD_SLEEP_MAX_SEC or TIMER_MAX_SEC.
   1.2 User Timer module
     - Now, user timer module can be added or removed as user wishes.
     - Fixed bug: timer callback function might be executed right after creation because timer_destroy has a bug.
 
 2. NanoMAC
   - mac_rx_on() and mac_rx_off() was removed. 
     Frequent CC2420 command (SRXON, SRFOFF) through SPI sometimes makes an error for ATmega128 platforms.
     Probably the reason is related to running CC2420_SRXON right after CC2420_SRFOFF.
     There is no way to solve this problem. We guess It's a hardware problem.

 3. ZigbeX(Hanback corp.) new interface is added. (HBE-USN-ISP)
   - New interface uses usb-to-serial port for fusing. 


Ver 2.3.6 (2009. 1. 12.)
========================

 1.  EEPROM module for ATMEGA128
   - This module was substituted by WinAVR library.

 2. Semaphore
  - A bug was fixed.

 3. Message Queue
   3.1 Non-blocking TX/RX API names are changed.
     - msgq_isend() --> msgq_send_nb()
     - msgq_irecv() --> msgq_recv_nb()

 4.  Automatic stack size configuring module (BETA) was removed.

 5. Kconfigs and Makefiles were changed.

 6. Non-commercial platforms was removed.
   - ETRI-SSN, ISN-400n, SKY-z200

 7. "Manual numbering for thread ID (Deprecated)" option will not be supported anymore.
   - The old thread_create() function must be changed.

 8. Routing module (TENO, RENO)
   - init() arguments were changed. Callback function must be assigned when init() is called.
   8.1 RENO routing module
     - Whole new code.
     - Queue for RX msg has been removed. Must use callback function.
     - TX FIFO queue has been added.
     - This module will use one of USER THREAD. Therefore user can use threads up to 4. ( 5(max) - 1 = 4 )
     - Support 16bit address
 
 9. Nano MAC
   - nmac_tx() will be substituted by nmac_tx_noack() if the destination is 0xFFFF.
   9.1 STATIC MAC INFO Module for ATMEGA128 has been added
     - Only for Nano24 or Ziebex platform
     - You can give different mac info (short addr, pan addr, channel) with the same apllication (rom file).
     - Try "$make mac_info ch=[channel] pan=[pan_address] short=[short_address] port=[your_platform_port]" after fusing a rom file.
     - MAC infomation will be deleted if a new rom file is written on a FLASH.
   9.2 nmac_csma_ca_tx_trail() has chaged into nmac_unslotted_csma_ca()
     - stabler version.

 10. ADC module for MSP430
   - The sampling time duration has been shortened. (clock source was changed.)

 11. UART module for MSP430
   - tolerant of temperature change. Very fast calibration will be run every 31.25ms


Ver 2.3.4 (2008. 2. 20.)
========================

 - Known bug : nos_eeprom_write_byte() function (eeprom module) for ATmega128 has a bug if compiled with -O or -O1 switch. 

 1.TENO routing module.
  - little performance update

2. Makefile
  - Useless memory usage by '__clz_tab' has been removed using '-lm' switch. You can save about 260 bytes RAM now!

3. UART module
 - uart_printf32 has been added. You can print out 32 bit variables.
 - uart_puti, uart_putu also supports 32 bit variables.

4. General Library
 - lltoa(), ultoa() has been added. These are used by uart_put..() functions.

5. ADC module for MSP430
 - There is a bug that NanoOS does not support multi-channel usage for ADC.
  Now you can use several ADC channel at the same time.


Ver 2.3.3 (2008. 1. 14.)
========================

 1. Nano MAC
  - Bug that RX is not executed forever after TX has been fixed.

2. Tree-based ETRI NanoQplus OS (TENO) routing module is newly added.
  - Tree-based routing for tree networks.
  - support 16bit address. (0xFFFF is reserved for protocol)
 	

Ver 2.3.2 (2007. 11. 23.)
=========================

 1. All modules have been rearranged.
  - Unnecessarily included header files are removed.
  - Global variables are defined in the C code instead of the header.

 2. Clock architecture for MSP430 has been changed.
  - clock.h and clock.c handle hardware clocks for MSP430 system.
  - MCLK for CPU is 4,194,304Hz (4096kHz) from DCO oscillator.
  - SMCLK is 2,097,152Hz (2048kHz) from DCO oscillator.
  - ACLK is 32,768Hz (32kHz) from LF mode of LFXT1 oscillator.

 3. Timer architerture has been changed.
  3.1 MSP430 
   - timer.h and timer.c manage timers for MSP430 system.
   - TimerA uses SMCLK for kernel, 802.15.4 and UART. (Do not change!)
   - TimerB uses ACLK for UART (32768KHz. It used for DCO calibration). 
   - TimerA CCR0 interrupt is used for kernel scheduling timer. The maximum period is 32ms
   - TimerA CCR1 interrupt is used for 802.15.4MAC scheduling timer. The period is 320us
   - TimerA overflow interrupt is used to make periodic DCO calibration for high-speed UART communication.
  3.2 ATmega128
   - Timer0 is used for "Time check" module (32.768KHz)
   - Timer2 is used for 802.15.4 scheduling (320us)
   - Timer3 is used for kernel scheduling (5, 10, 32ms)

 4. UART module for MSP430 has been updated
  - The periodic calibration for DCO oscillator makes UART module endurable for the change of temparature.
  - Baudrate for MSP430 supports up to 921600.
  - Baudrate for ATmega128L (Nano24, ETRI-SSN, Sky-z200) suppors up to 1M.

 5. Kernel module has been changed.
   5.1 Scheduling time (time slice) that are now supported has been changed to
    - 5ms, 10ms, 32ms
   5.2. thread_create() has been modified, so that they return a created id number after called.
    - If you want to use the old function, please select "Manual numbering for thread ID (Deprecated)" in menuconfig.
   5.3. thread_kill() function has been added.
   5.4. thread_suspend(), thread_exit bug has been fixed.
   5.5. timer_create() has been modified.
    - Old timer_create() is not supported in this version. Argument *ptmid should be removed. timer_create() will return TIMER_ID.
    - The file names are changed to user_timer.h, user_timer.c to distinguish user timer from hardware timer.
   5.6. Semaphore module has been updated.
   5.7. Message queue module has been updated.
   5.8. Automatic stack size configuring module (BETA) has been removed from menuconfig.
   5.9. Small changes in codes for performance optimization.

 6. Time check module has been changed.
  - Now, MSP430 is also supported.
  - It uses 32.768KHz crystal oscillator.
  - The resolution is from 30.52us to 2 sec.
  - ATmega128 uses Timer/Counter0 ISR for time check.
     . Check that interrupt is enabled if ATmega128 CPU is used. 
     . If interrupt is disabled, the maximum time that can be measured is 7.8125ms
  - You can check the local time (age) of each node with this module.

 7. Nano-MAC has been updated
  - You can refer to RSSI and correlation values from MAC-RX queue.


Ver 2.3.1 (2007. 10. 8.)
========================

 1. Kernel module supports up to 15 threads.
  - Extended multi-thread module is included as an option. This module may affect the performance quite a little.

 2. User timer module has been updated.
  - 'nos_timer_create_xx' function has been changed
    .Timer numbers are auto-generated by kernel
      ('tmid' argument should be a pointer to store timer id which is generated automatically by kernel.)
    .No need to pass arguments in the function called by timer
      ('args_data' argument has been removed. You can use "void function" only.)
  - Some minor errors have been fixed
  
 3. The non-blocking mode in message queue module is now supported.

 4. Nano-MAC has been changed.
  - RX overflow handling bugs have been fixed.
  - APIs for TX power level control has been added. ( vold mac_set_tx_power(UINT8 level), level:1~31 )


Ver 2.3.0 (2007. 9. 17.)
========================

 1. First release version of Nano Qplus OS 2
