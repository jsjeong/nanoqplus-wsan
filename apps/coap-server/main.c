// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @date 2012. 1. 25.
 * @author Jongsoo Jeong (ETRI)
 */

#include "nos.h"

enum resource_tag
{
    TAG_SENSOR_TEMPERATURE = 0,
    TAG_SENSOR_LIGHT = 1,
    TAG_SENSOR_HUMIDITY = 2,
    TAG_SENSOR_GAS = 3,
    TAG_SENSOR_MIC = 4,
    TAG_SENSOR_POWER = 5,
    TAG_ACTUATOR_RELAY = 6,
    TAG_SENSOR_PIR = 7,
};

UINT8 node_iid[] = {0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x0B};

#ifdef RELAY_M
void relay_action(const COAP_RESOURCE *resource,
        const COAP_QUERY *request, COAP_ANSWER *response)
{
    char *msg;
    msg = malloc(50);

    if (request->code == COAP_REQUEST_GET)
    {

        if (msg)
        {
            if (relay_get_state(4))
            {
                sprintf(msg, "on");
            }
            else
            {
                sprintf(msg, "off");
            }

            if (coap_put_answer(response, msg, strlen(msg)))
            {
                response->code = COAP_RESPONSE_205_CONTENT;
                response->media_type = COAP_MEDIA_TEXT_PLAIN;
            }
            else
            {
                response->code = COAP_RESPONSE_500_INTERNAL_SERVER_ERROR;
            }
        }
        else
        {
            response->code = COAP_RESPONSE_500_INTERNAL_SERVER_ERROR;
        }
    }
    else if (request->code == COAP_REQUEST_POST)
    {
        //printf("%s\r\n", (char *) request->data);
        if (!memcmp(request->data, "on", 2))
        {
            relay_on(4);
            led_on(2);
            sprintf(msg, "turned on");
            if (coap_put_answer(response, msg, strlen(msg)))
            {
                response->code = COAP_RESPONSE_204_CHANGED;
                response->media_type = COAP_MEDIA_TEXT_PLAIN;
            }
            else
            {
                response->code = COAP_RESPONSE_500_INTERNAL_SERVER_ERROR;
            }
        }
        else if (!memcmp(request->data, "off", 3))
        {
            relay_off(4);
            led_off(2);
            sprintf(msg, "turned off");
            if (coap_put_answer(response, msg, strlen(msg)))
            {
                response->code = COAP_RESPONSE_204_CHANGED;
                response->media_type = COAP_MEDIA_TEXT_PLAIN;
            }
            else
            {
                response->code = COAP_RESPONSE_500_INTERNAL_SERVER_ERROR;
            }
        }
        else if (!memcmp(request->data, "toggle", 6))
        {
            relay_toggle(4);
            led_toggle(2);
            if (relay_get_state(4))
            {
                sprintf(msg, "turned on");
            }
            else
            {
                sprintf(msg, "turned off");
            }
            if (coap_put_answer(response, msg, strlen(msg)))
            {
                response->code = COAP_RESPONSE_204_CHANGED;
                response->media_type = COAP_MEDIA_TEXT_PLAIN;
            }
            else
            {
                response->code = COAP_RESPONSE_500_INTERNAL_SERVER_ERROR;
            }
        }
        else
        {
            response->code = COAP_RESPONSE_400_BAD_REQUEST;
        }
    }
    free(msg);
}
#endif

void sensor_action(const COAP_RESOURCE *resource,
        const COAP_QUERY *request, COAP_ANSWER *response)
{
    char *msg;

    msg = malloc(20);
    if (msg)
    {
        UINT16 value;

#ifdef SENSOR_HUMIDITY_M
        if (resource->tag == TAG_SENSOR_HUMIDITY)
        {
            value = humidity_sensor_get_data();
            sprintf(msg, "%u.%02u %%", value / 100, value % 100);
        }
#endif /* SENSOR_HUMIDITY_M */

#ifdef SENSOR_TEMPERATURE_M
        if (resource->tag == TAG_SENSOR_TEMPERATURE)
        {
            value = temperature_sensor_get_data();
            sprintf(msg, "%u.%02u (C)", value / 100, value % 100);
            printf("%s\n", msg);
        }
#endif /* SENSOR_TEMPERATURE_M */

#ifdef SENSOR_GAS_M
        if (resource->tag == TAG_SENSOR_GAS)
        {
            value = gas_sensor_get_data();
            sprintf(msg, "%u ppm", value);
        }
#endif /* SENSOR_GAS_M */

#ifdef SENSOR_LIGHT_M
        if (resource->tag == TAG_SENSOR_LIGHT)
        {
            value = light_sensor_get_data();
            sprintf(msg, "%u lux", value);
        }
#endif /* SENSOR_LIGHT_M */

#ifdef POWERMETER_M
        if (resource->tag == TAG_SENSOR_POWER)
        {
            value = powermeter_get_state();
            sprintf(msg, "%u mA", value);
        }
#endif /* SENSOR_LIGHT_M */

#ifdef SENSOR_PIR_M
        if (resource->tag == TAG_SENSOR_PIR)
        {
            value = pir_sensor_get_data();

            if (value > 20)
            {
                sprintf(msg, "Detected");
            }
            else
            {
                sprintf(msg, "Nothing...");
            }
        }
#endif /* SENSOR_PIR_M */
        if (coap_put_answer(response, msg, strlen(msg)))
        {
            response->code = COAP_RESPONSE_205_CONTENT;
            response->media_type = COAP_MEDIA_TEXT_PLAIN;
        }
        else
        {
            response->code = COAP_RESPONSE_500_INTERNAL_SERVER_ERROR;
        }
        free(msg);
    }
    else
    {
        response->code = COAP_RESPONSE_500_INTERNAL_SERVER_ERROR;
    }
}

int main(void)
{
    COAP_RESOURCE *sensor, *act, *r;

    nos_init();
    led_on(1);
    printf("\n*** Nano OS ***\n");

    // Initialize the LoWPAN interface.
    lowpan_init(0, 1, 0xffff, node_iid, 10, 1);
    ip6_init(0);

    coap_init(COAP_DEFAULT_PORT_NUMBER);

    sensor = coap_make_resource(&root, "sensor", 0);
    if (!sensor)
    {
        printf("Making a 'sensor' resource failed.\n");
        return -1;
    }

    act = coap_make_resource(&root, "act", 0);
    if (!act)
    {
    	printf("Making an 'act' resource failed.\n");
    	return -1;
    }

#ifdef SENSOR_TEMPERATURE_M
    if ((r = coap_make_resource(sensor, "temperature", TAG_SENSOR_TEMPERATURE)) != NULL)
    {
        coap_set_action_on_request(r, ALLOW_GET | IMMEDIATE_RESPONSE, sensor_action);
    }
    else
    {
        printf("Making a 'temperature' resource failed.\n");
    }
#endif

#ifdef SENSOR_LIGHT_M
    if ((r = coap_make_resource(sensor, "light", TAG_SENSOR_LIGHT)) != NULL)
    {
        coap_set_action_on_request(r, ALLOW_GET | IMMEDIATE_RESPONSE, sensor_action);
    }
    else
    {
        printf("Making a 'light' resource failed.\n");
    }
#endif

#ifdef SENSOR_HUMIDITY_M
    if ((r = coap_make_resource(sensor, "humidity", TAG_SENSOR_HUMIDITY)) != NULL)
    {
        coap_set_action_on_request(r, ALLOW_GET | IMMEDIATE_RESPONSE, sensor_action);
    }
    else
    {
        printf("Making a 'humidity' resource failed.\n");
    }
#endif

#ifdef SENSOR_GAS_M
    if ((r = coap_make_resource(sensor, "gas", TAG_SENSOR_GAS)) != NULL)
    {
        coap_set_action_on_request(r, ALLOW_GET | IMMEDIATE_RESPONSE, sensor_action);
    }
    else
    {
        printf("Making a 'gas' resource failed.\n");
    }
#endif

#ifdef SENSOR_MIC_M
    if ((r = coap_make_resource(sensor, "mic", TAG_SENSOR_MIC)) != NULL)
    {
        coap_set_action_on_request(r, ALLOW_GET | IMMEDIATE_RESPONSE, sensor_action);
    }
    else
    {
        printf("Making a 'mic' resource failed.\n");
    }
#endif

#ifdef POWERMETER_M
    if ((r = coap_make_resource(sensor, "power", TAG_SENSOR_POWER)) != NULL)
    {
        coap_set_action_on_request(r, ALLOW_GET | IMMEDIATE_RESPONSE, sensor_action);
    }
    else
    {
        printf("Making a 'power' resource failed.\n");
    }
#endif

#ifdef RELAY_M
    if ((r = coap_make_resource(act, "relay", TAG_ACTUATOR_RELAY)) != NULL)
    {
        coap_set_action_on_request(r, ALLOW_GET | ALLOW_POST | IMMEDIATE_RESPONSE, relay_action);
    }
    else
    {
        printf("Making a 'relay' resource failed.\n");
    }
#endif

#ifdef SENSOR_PIR_M
    if ((r = coap_make_resource(sensor, "pir", TAG_SENSOR_PIR)) != NULL)
    {
        coap_set_action_on_request(r, ALLOW_GET | IMMEDIATE_RESPONSE, sensor_action);
    }
    else
    {
        printf("Making a 'PIR' resource failed.\n");
    }
#endif /* SENSOR_PIR_M */
    sched_start();
    return 0;
}
