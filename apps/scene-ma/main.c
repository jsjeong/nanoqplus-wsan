// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * RPL node test application to monitor the RPL network
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2010. 10. 1.
 */

#include "nos.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef BATTERY_MONITOR_M
#include "bat-monitor.h"
#endif //BATTERY_MONITOR_M

#include "rf212.h"  // JJ: add for set channel
#include "heap.h"
// JJ: WSAN profile
// Master Actuator
////////////////////////////////////////////
#include "platform.h"
#include "uart.h"
#include "led.h"
#include "taskq.h"
#include "heap.h"
#include <string.h>

#define wsan_port 1236
#define wsan_port_send 1235
#define MAX_UDP_LENGTH  200 // maximum UDP packet length
#define MAX_STRING_LENGTH 30  // string to define
#define MAX_NUM_SENSOR  9  // maximum number of kinds of sensors (e.g., temp, hum, light, co, co2, pir)
#define MAX_NUM_SN 10
#define MAX_NUM_ACT 10
#define MAX_NUM_SCENE 13
#define MAX_NUM_MA_PROFILE 1 // for master actuator only
#define MAX_NUM_SEN_PROFILE 10 // for sensor only


#define MAX_RX_QUEUE 10

enum{         // sType bitmap
  DOPPLER_SENSOR = 0x0100,
  TEMP_SENSOR = 0x0080,
  HUM_SENSOR = 0x0040,
  LIGHT_SENSOR = 0x0020,
  CO_SENSOR = 0x0010,
  CO2_SENSOR = 0x0008,
  PIR_SENSOR = 0x0004,
  MIC_SENSOR = 0x0002,
  ACC_SENSOR = 0x0001
};

enum {        // dType bitmap
    TYPE_SENSOR = 0x00,
    TYPE_ACT = 0x02,
    TYPE_MASTER_ACT = 0x02
};

enum {        // Condition bitmap
    CON_OCCUPANCY = 0x0001,
    CON_MIC_ONE = 0x0002,
    CON_MIC_TWO = 0x0004,
    CON_PROJECTOR = 0x0008,
    CON_WB = 0x0010
};
    

enum {        // msgID
    ID_RPT_JOIN = 0x01,
    ID_DEPLOY_MA_PROFILE = 0x02,
    ID_DEPLOY_ACT_PROFILE = 0x03,
    ID_SCENE_INFO = 0x04,
    ID_DEPLOY_SEN_PROFILE = 0x05,
    ID_RPT_SENSING = 0x06,
    ID_RPT_ACT = 0x07,
    ID_MA_CTRL = 0x08,
    ID_ACT_CTRL = 0x09,
    ID_REQ_ACT_STAT = 0x0a,
    ID_RSP_ACT_STAT = 0x0b,
    ID_REQ_SEN_VAL = 0x0c,
    ID_RSP_SEN_VAL = 0x0d,
    
    ID_RPT_ACT_INFO = 0x21,
    ID_RPT_ACT_INFO_MA = 0x22,  // TBD
 
    ID_RPT_JOIN_ACK = 0x31,
    ID_DEPLOY_MA_PROFILE_ACK = 0x32,
    ID_DEPLOY_ACT_PROFILE_ACK = 0x33,

    ID_DEPLOY_SEN_PROFILE_ACK = 0x35,
    ID_RPT_SENSING_ACK = 0x36,
    ID_RPT_ACT_ACK = 0x37,
    ID_MA_CTRL_ACK = 0x38
};

////////////////////////////////////
// UDP RX Queue 
typedef struct rx_data {
    IP6_ADDRESS src_addr;
    UINT16 src_port;
    UINT16 dst_port;
    char msg[MAX_UDP_LENGTH];
    UINT16 len;
    UINT8 msgID;
} RX_DATA;
    
RX_DATA rx_queue[MAX_RX_QUEUE];
int front = 0;
int rear = -1;
int queue_size = 0;

// Linked list for sending data (with ack)
typedef struct tx_packet {
    long expired_time;
    UINT16 dest;
    UINT8 msgID;
    UINT16 port;
    char msg[MAX_UDP_LENGTH];
    UINT8 msg_len;
    long ack_time;
} TX_PACKET;

typedef struct tx_list {
    TX_PACKET val;
    struct tx_list *next;
} tx_list;

struct tx_list *head = NULL;
struct tx_list *curr = NULL;
///////////////////////////////

typedef struct REPORT_JOIN_PKT {
    UINT8 msgID;
  IP6_ADDRESS did;
  UINT8 dType; // IPv6 addr
  char locL[10]; // semantic location
  UINT16 locPx; // location x
  UINT16 locPy; // location y
  UINT8 aType; // actuation type (sensor = 0)
  UINT16 sType; // sensor type (bitmap)
} REPORT_JOIN_PKT;

typedef struct SCENE {
//  UINT8 msgID;
  UINT8 sceneID;
  UINT16 condition; // bitmap (0b10000: WB, 0b01000: proj., 0b0110: MIC, 0b0001: PIR)
} SCENE;


typedef struct MA_CONTROL_PKT {
  UINT8 msgID;
  UINT8 controlMode;
  UINT8 sceneID;
  UINT32 timestamp;
} MA_CONTROL_PKT;

typedef struct SCENE_INFO_PKT {
  UINT8 msgID;
  UINT8 sceneID;
    //UINT16 condition; // bitmap (0b10000: WB, 0b01000: proj., 0b0110: MIC, 0b0001: PIR)
} SCENE_INFO_PKT;

typedef struct SENSORS {
  UINT16 sen_addr;
  UINT16 stype;
} SENSORS;


typedef struct MASTER_ACT_PROFILE {
    UINT8 msgID;
  UINT8 num_scene;
  UINT8 num_act;
  UINT16 act_addr[MAX_NUM_ACT];
  UINT8 num_sen;
  SENSORS sensors[MAX_NUM_SN]; // htw modify 140203  
    //UINT8 pir_cnt;
  //UINT16 sen_addr[MAX_NUM_SN];
    //UINT16 stype[MAX_NUM_SN];  // htw modify 140203
  SCENE scene[MAX_NUM_SCENE];
  UINT8 groupID;
} MASTER_ACT_PROFILE;


typedef struct DECISION {
    UINT16 sen_addr[MAX_NUM_SN];
    UINT16 stype[MAX_NUM_SN];
    UINT16 scondition[MAX_NUM_SN];
    UINT8 WB;
    UINT8 WB_Stat;
    UINT8 Proj;
    UINT8 Proj_Stat;
    UINT8 Speaker;
    UINT8 Occupancy;
    UINT8 Occupancy_Stat;
} DECISION;


/*
typedef struct ACT_PROFILE {
    UINT8 msgID;
  UINT8 num_scene;
  UINT8 sceneID[MAX_NUM_SCENE];
  UINT8 status[MAX_NUM_SCENE]; // dimming value;
} ACT_PROFILE;
*/

/*
typedef struct SENSOR_PROFILE {
  UINT8  msgID;
  UINT16 act_addr;
  UINT16 sType; // bitmap
  UINT16 mode[MAX_NUM_SENSOR];
} SENSOR_PROFILE;
*/

typedef struct SCENE_RPT_PKT {
  UINT8 msgID;
  UINT8 sceneID;
} SCENE_RPT_PKT;

typedef struct ACT_STATUS_RPT_PKT {
  UINT8 msgID;
  UINT8 scene_status;
  UINT32 timestamp;
} ACT_STATUS_RPT_PKT;

// ACT
typedef struct ACTUATION_INFO_RPT_PKT {
  UINT8 msgID;
  UINT8 sceneID;
  UINT8 status;
} ACTUATION_INFO_RPT_PKT; // to MA

typedef struct SENSOR_INFO_RPT_PKT {
  UINT8 msgID;
//  UINT16 src_addr;
  UINT16 sType;
  UINT16 condition;
  UINT32 timestamp;
  UINT32 reserved;
} SENSOR_INFO_RPT_PKT;

typedef struct SENSING_RESP_PKT {
  UINT8 msgID;
  UINT16 sType;
  UINT16 sensingValues[MAX_NUM_SENSOR];
  UINT32 timestamp;
} SENSING_RESP_PKT;


////////////////////////////////////////////
// Global Variables
char locL[10];
UINT16 locPx;
UINT16 locPy;
UINT8 aType;

int current_ma_profile = 0;
MASTER_ACT_PROFILE my_ma_profile[MAX_NUM_MA_PROFILE];
//ACT_PROFILE my_act_profile;
//int current_sen_profile = -1;
//SENSOR_PROFILE my_sen_profile[MAX_NUM_SEN_PROFILE];
DECISION my_decision;
UINT8 my_scene;
UINT8 my_control = 0x01; // 0x01 self mode, 0x02 control mode
UINT16 rx_src_addr; 
UINT16 my_act_bitmap = 0x0000;
UINT16 act_bitmap = 0x0000;	

/////////////////////////////////////////////
enum
{
    COMMAND_READY = 1,
    COMMAND_INCOMING = 2,
    COMMAND_STARTED = 3,
    COMMAND_INCOMING_0 = 4,
    COMMAND_INCOMING_1 = 5,
};

static UINT8 isConnected = 0;
static unsigned char serialBuffer[100];
static unsigned char serialIndex = 0;
static unsigned char serialLength = 0;
static unsigned char commandStatus = COMMAND_READY;
static unsigned char msgID = 0;  // JJ: add for msgID check
///////////////////////////////////////////

INT8 send_timer_id = -1;
#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 5;
#endif
UINT8 node_ext_id[] = { 0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x00};

IP6_ADDRESS base_addr = {{
        0xfd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x0e, 'T', 'R', 0xff, 0xfe, 0x00, 0x00, 0x05 }};

IP6_ADDRESS root_addr;
IP6_ADDRESS server_addr = {{
        0xaa, 0xaa, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11 }};
       
IP6_ADDRESS broadcast_addr= {{
        0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02 }};

UINT8 def_inf; //IPv6 default interface

UINT8 sec_key[] = {
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
        0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };

///////////////////////////////////////////////////////////////////////////
// Functions
void init_profile(void);
void report_join(void);

////////////////////////////////////////////////////////////////
// JJ: Add for WSAN profile (MA)

// for tx_list (linked list)
struct tx_list* create_list(TX_PACKET val)
{
    struct tx_list *ptr = (tx_list*)nos_malloc(sizeof(tx_list));
    if (ptr == NULL) {
        printf("Fail to create tx_list\n");
        return NULL;
    }
    ptr->val = val;
    ptr->next = NULL;

    head = curr = ptr;
    return ptr;
}

struct tx_list* add_to_list(TX_PACKET val) 
{
    printf("add: %d, %d\n", val.dest, val.msgID);
    if (NULL == head)
        return (create_list(val));
    
    struct tx_list *ptr = (tx_list*)nos_malloc(sizeof(tx_list));
    if (ptr == NULL) {
        printf("Fail to create tx_list\n");
        return NULL;
    }
    ptr->val = val;
    ptr->next = NULL;
    curr->next = ptr;
    curr = ptr;

    return ptr;
}

struct tx_list* search_in_list(UINT16 addr, UINT8 msgID, struct tx_list **prev)
{
    struct tx_list *ptr = head;
    struct tx_list *tmp = NULL;
    
    while(ptr !=NULL) {
        //printf("search: %d, %d\n", addr, msgID);
        //printf("from: %d, %d\n", ptr->val.dest, ptr->val.msgID);
        if ((ptr->val.dest == addr) && (ptr->val.msgID == msgID) ) {
            if (prev) 
                *prev = tmp;
            return ptr;
        }
        else {
            tmp = ptr;
            ptr = ptr->next;
        }
    }
    return NULL;
}

int delete_from_list(UINT16 addr, UINT8 msgID)
{
    struct tx_list *prev = NULL;
    struct tx_list *del = NULL;
    //print_list();
    
    del = search_in_list(addr, msgID, &prev);
    if (del == NULL)
        return -1;
    else {
        printf("del: %d, %d\n", del->val.dest, del->val.msgID);
        if (prev !=NULL)
            prev->next = del->next;
        if (del == curr)
            curr = prev;
        if (del == head)
            head = del->next;
    }
    nos_free(del);
    del = NULL;

    if (head == NULL)
        printf("clear head\n");
    if (curr == NULL)
        printf("clear curr\n");
    return 1;
}

void print_list(void)
{
    struct tx_list *ptr = head;
    printf(" ----------------------------\n");
    while( ptr != NULL) {
        printf("addr = %d, msgId = %d\n", ptr->val.dest, ptr->val.msgID);
        ptr = ptr->next;
    }

    return;
}

// for rx queue
UINT8 enqueue_rx(RX_DATA in)
{
    if (queue_size == MAX_RX_QUEUE) {
        printf("Full of rx_queue\n");
        return 0;
    }
    
    rx_queue[++rear] = in; // TBC: check the struct copy
    queue_size++;
    // debug
    if (queue_size > MAX_RX_QUEUE)
        printf("Enqueue error\n");
    if (rear == MAX_RX_QUEUE) 
        rear = -1;

    return 1;
}

int dequeue_rx(void)
{
    int temp;
    if (queue_size == 0) {
        printf("Queue empty\n");
        return -1;
    }
    temp = front;
    front++;
    queue_size--;
    // debug
    if (queue_size < 0)
        printf("Dequeue error\n");

    if (front == MAX_RX_QUEUE)
        front = 0;
    return temp;
}

// TODO: Send process here
void process_tx(void) {
    IP6_ADDRESS dst_addr;
    struct tx_list *temp = head;
    UINT8 isSend = 1;
    char text_addr[IP6_NTOP_BUF_SZ];

    if (ip6_is_attached(def_inf)) {

        while( (temp !=NULL) && isSend ) {
            temp->val.expired_time -= 40;
            if (temp->val.expired_time < 0) {
                if (temp->val.dest == 0x0001) {
                    memcpy(&dst_addr, &server_addr, 16);
                    printf("Server [%s]: %d, %d\n",
                           ip6_ntop(&dst_addr, text_addr), temp->val.port, temp->val.msg_len);
                }
                else {
                    memcpy(&dst_addr, &base_addr, 16);
                    memcpy(&dst_addr.s6_addr[14], &temp->val.dest, 2);
                    printf("To [%s]: %d, %d\n",
                       ip6_ntop(&dst_addr, text_addr), temp->val.port, temp->val.msg_len);
                }
                // printf("send packet %ld, %ld\n", temp->val.expired_time, temp->val.ack_time);
                udp_sendto(def_inf, wsan_port, &dst_addr, temp->val.port, temp->val.msg, temp->val.msg_len);
                led_toggle(2);
                isSend = 0;
                if (temp->val.ack_time > 0)
                    temp->val.expired_time = temp->val.ack_time; 

                return;
            }
            temp = temp->next;
        }        
    }
}

void send_Ack(int ind, UINT8 msgid, UINT16 dst_port)
{
    UINT8 msg[2];
    msg[0] = msgid;
    msg[1] = 0x00;
    printf("Send Ack: %d\n", msgid);
    udp_sendto(def_inf, rx_queue[ind].dst_port, &rx_queue[ind].src_addr, dst_port/* rx_queue[ind].src_port */, msg, 2);
}


UINT8 findProfileID(UINT8 groupID)
{
    UINT8 ind;
    UINT8 i;    
//groupID = 1; // htw modify
    printf("curr ma = %d\n", current_ma_profile);
    if (current_ma_profile > 0) {
        ind = current_ma_profile;
        for(i=0; i< current_ma_profile; i++) {
            if (my_ma_profile[i].groupID == groupID) {
                // change MA profile here
                ind = i;
                printf("Find! grpId = %d\n", groupID);
                return ind;
            }
        }        
    }
    else
        ind = current_ma_profile;
    return ind;

}

// Support changing the whole MA profile only
void save_ma_profile(UINT8* ptr) 
{
    UINT8 i;
    UINT8 ind;
    UINT8 ma_cnt=0;
    UINT8 pir_cnt =0;
    MASTER_ACT_PROFILE temp_ma;
    temp_ma.msgID = ptr[ma_cnt++];
    temp_ma.num_scene = ptr[ma_cnt++];
    temp_ma.num_act = ptr[ma_cnt++]; 
    memcpy(temp_ma.act_addr, &ptr[ma_cnt], temp_ma.num_act *2);
    ma_cnt += temp_ma.num_act *2;
    temp_ma.num_sen = ptr[ma_cnt++];

    memcpy(temp_ma.sensors, &ptr[ma_cnt], temp_ma.num_sen * sizeof(SENSORS));  // htw modification
    ma_cnt += temp_ma.num_sen* sizeof(SENSORS);

    // store sensor information...
    for (i=0;i<temp_ma.num_sen;i++) {
        my_decision.sen_addr[i] = temp_ma.sensors[i].sen_addr;
        my_decision.stype[i] = temp_ma.sensors[i].stype;
        if(my_decision.stype[i] & ACC_SENSOR)
            my_decision.WB_Stat |= (0x01<<i);
        if(my_decision.stype[i] & LIGHT_SENSOR)
            my_decision.Proj_Stat |= (0x01<<i);
        if(my_decision.stype[i] & PIR_SENSOR)
            my_decision.Occupancy_Stat |= (0x01<<i);
    }
    //temp_ma.pir_cnt = pir_ct;
    //     ma_cnt += temp_ma.num_sen *2;

    memcpy(temp_ma.scene, &ptr[ma_cnt], temp_ma.num_scene * sizeof(SCENE));
    ma_cnt += temp_ma.num_scene * sizeof(SCENE);
    temp_ma.groupID = ptr[ma_cnt++];

    ind = findProfileID(temp_ma.groupID);
    
    printf("master[%d]\n", ind);
    
    // memcpy(&my_ma_profile[current_ma_profile], rx_data[ret].msg, rx_data[ret].len);    
    my_ma_profile[ind].num_scene = temp_ma.num_scene;
    my_ma_profile[ind].num_act = temp_ma.num_act;
    memcpy(my_ma_profile[ind].act_addr, temp_ma.act_addr, temp_ma.num_act * 2); // htw ??1
    my_ma_profile[ind].num_sen = temp_ma.num_sen;
    memcpy(my_ma_profile[ind].sensors, temp_ma.sensors, temp_ma.num_sen * sizeof(SENSORS)); // htw ??2
    //my_ma_profile[ind].pir_cnt = temp_ma.pir_cnt; // htw modification
    memcpy(my_ma_profile[ind].scene, temp_ma.scene, temp_ma.num_scene * sizeof(SCENE));
    my_ma_profile[ind].groupID = temp_ma.groupID;

    printf("MA: num_scene: %d, num_act: %d, num_sen: %d, grpID: %d\n", temp_ma.num_scene, temp_ma.num_act, temp_ma.num_sen, temp_ma.groupID);

    if (ind == current_ma_profile)
        current_ma_profile++; 
    return;
}

BOOL ischangedScene(UINT8* ptr, UINT8 ind)
{
    //return false;

    int index=0;
    UINT8 WB, Proj, PIR, Speaker;
    UINT8 temp_scene;
    SENSOR_INFO_RPT_PKT *sensor_rpt;
    UINT16 temp_condition = 0x0000;
    memcpy(sensor_rpt, &ptr[0], sizeof(SENSOR_INFO_RPT_PKT));   
    for( int i=0; i < my_ma_profile[ind].num_sen;i++){
        if(my_decision.sen_addr[i] == rx_src_addr){
		index = i;
		break;
	}
    }
    WB = (sensor_rpt->condition & 0x10)>>4;
    my_decision.WB = (my_decision.WB &~(0x01<<index))| (WB<<index);
    Proj = (sensor_rpt->condition & 0x08)>>3;
    my_decision.Proj = (my_decision.Proj &~(0x01<<index))| (Proj<<index);
    PIR = (sensor_rpt->condition & 0x01);
    my_decision.Occupancy = (my_decision.Occupancy &~(0x01<<index))| (PIR<<index);

    Speaker = (sensor_rpt->condition & 0x06);
    my_decision.Speaker = Speaker;
	
    if ( my_decision.WB == my_decision.WB_Stat)
	temp_condition |= 0x0010; 
    
    if ( my_decision.Proj == my_decision.Proj_Stat)
	temp_condition |= 0x0008; 
    //if ( my_decision.Occupancy == my_decision.Occupancy_Stat)
    if ( my_decision.Occupancy == 0x00)// if all are 0, then condition 0x0001 field is 0
	temp_condition &= 0xFFFE;   
    temp_condition |= (my_decision.Speaker & 0x0006);
 
    //if (my_scene   
    for (int i=0; i< my_ma_profile[ind].num_scene;i++){
         if(my_ma_profile[ind].scene[i].condition == temp_condition){
	     temp_scene = my_ma_profile[ind].scene[i].sceneID;
	     if (my_scene != temp_scene){
		my_scene = temp_scene;
		return TRUE;
		}
		return FALSE;
	}

    }  
    return FALSE;
}




void report_Actuation(void){
    TX_PACKET join_tx;
    ACT_STATUS_RPT_PKT act_status_rpt_pkt;
    act_status_rpt_pkt.msgID = ID_RPT_ACT;
    act_status_rpt_pkt.scene_status = my_scene;
    act_status_rpt_pkt.timestamp = 0x00000001; // current time

    join_tx.expired_time = 0;
    join_tx.dest = 0x0001;
    join_tx.msgID = ID_RPT_ACT;
    join_tx.port = wsan_port_send;
    memcpy(join_tx.msg, &act_status_rpt_pkt, sizeof(ACT_STATUS_RPT_PKT)); // htw
    join_tx.msg_len = sizeof(ACT_STATUS_RPT_PKT); // htw
    join_tx.ack_time = 1000; // msec
    add_to_list(join_tx);
}


void do_decision_making(UINT8* ptr)
{
    
    UINT8 i;
    UINT8 ind;
    //UINT8 ma_cnt=0;
    //UINT8 sceneid;
    UINT8 groupID=1; // htw setting
    //MASTER_ACT_PROFILE temp_ma;
    //save_sensor_value()
    ind = findProfileID(groupID);    
    if (!ischangedScene(ptr, ind)) return NULL;
    //    sceneid = 0; //select_scene();
    // find groupid
   
    //char *msg;
    TX_PACKET join_tx;
    SCENE_INFO_PKT scene_info_pkt;

    scene_info_pkt.msgID = ID_SCENE_INFO;
    scene_info_pkt.sceneID = my_scene; 
        
    join_tx.expired_time = 0;
    join_tx.msgID = ID_SCENE_INFO;
    join_tx.port = wsan_port;
    memcpy(join_tx.msg, &scene_info_pkt, sizeof(SCENE_INFO_PKT)); // htw
    join_tx.msg_len = sizeof(SCENE_INFO_PKT); // htw
    join_tx.ack_time = 1000; // msec

    /* printf("To : %d, %s, %s, %d\n", */
    /*        join_tx.msg_len, report_join_pkt.locL, locL, strlen(locL)); */
    /* printf("port = %d\n", join_tx.port); */
    
    for (i=0;i<my_ma_profile[ind].num_act;i++)
    {
       join_tx.dest = my_ma_profile[ind].act_addr[i]; // Send SceneInfo Packet
       add_to_list(join_tx);
    }

    // Report ActuationInfo Packet 
    report_Actuation();  // after it's done fully, then send report_actuation
    /*
    ACT_STATUS_RPT_PKT act_status_rpt_pkt;

    act_status_rpt_pkt.msgID = ID_RPT_ACT;
    act_status_rpt_pkt.scene_status = my_scene;
    act_status_rpt_pkt.timestamp = 0x00000001; // current time

    join_tx.expired_time = 0;
    join_tx.dest = 0x0001;
    join_tx.msgID = ID_RPT_ACT;
    join_tx.port = wsan_port_send;
    memcpy(join_tx.msg, &act_status_rpt_pkt, sizeof(ACT_STATUS_RPT_PKT)); // htw
    join_tx.msg_len = sizeof(ACT_STATUS_RPT_PKT); // htw
    join_tx.ack_time = 1000; // msec
    add_to_list(join_tx);
    */
}
/*
void save_act_profile(UINT8* ptr)
{
    UINT8 ind, found;
    UINT8 i, j;
    ind = 1; // clear msgID
    if (my_act_profile.num_scene == 0) {
        my_act_profile.num_scene = ptr[ind++];
        for (i=0; i< my_act_profile.num_scene; i++) {
            my_act_profile.sceneID[i] = ptr[ind++];
            my_act_profile.status[i] = ptr[ind++];
        }
        return;
    }
    // TEST: if ACT have a same scene, change the status
    for (j=2; j< ptr[1]; j+=2) {
        found = 0;
        for (i=0; i<my_act_profile.num_scene; i++) {
            if (my_act_profile.sceneID[i] == ptr[j]) {
                my_act_profile.status[i] = ptr[j+1];
                found = 1;
            }
        }
        if (!found) {
            my_act_profile.sceneID[my_act_profile.num_scene] = ptr[j];
            my_act_profile.sceneID[my_act_profile.num_scene] = ptr[j+1];
            my_act_profile.num_scene++;
        }
    }
    return;
}
*/
/*
void do_actuation(UINT8 sceneID, UINT8 ind)
{
    UINT8 i;
    UINT8 msg[3];
    for (i=1; i<my_act_profile.num_scene; i++) {
        if (my_act_profile.sceneID[i] == sceneID) {
            // TODO: do actuation based on my_act_profile.status[i]
            // Respond ID_RPT_ACT_INFO
            // TODO: Do dimming here
            msg[0] = ID_RPT_ACT_INFO;
            msg[1] = sceneID;
            udp_sendto(rx_queue[ind].dst_port, &rx_queue[ind].src_addr, rx_queue[ind].src_port, msg, 3);    
            break;
        }
    }
}
*/
 /*
void save_sen_profile(UINT8* ptr) 
{
    UINT8 i;
    UINT8 ind, cnt;
    UINT16 recv_addr;
    cnt = 0;
    recv_addr = (UINT16)(ptr[1] << 8) + (UINT16)ptr[2];
    if (current_sen_profile >= 0) {
        ind = current_sen_profile;
        for(i=0; i< current_sen_profile+1; i++) {
            if (my_sen_profile[i].act_addr == recv_addr) {
                // change MA profile here
                ind = i;
                break;
            }
        }        
    }
    // add here
    // memcpy(&my_ma_profile[current_ma_profile], rx_data[ret].msg, rx_data[ret].len);    
    my_sen_profile[ind].act_addr = recv_addr;
    my_sen_profile[ind].sType = (UINT16)(ptr[3] << 8) + (UINT16)ptr[4];
    // TODO: Need to check the 'mode' before testing
    for (i=0; i<16; i++) {
        if ((my_sen_profile[ind].sType >> i) & 0x0001) cnt++;
    }
    memcpy(my_sen_profile[ind].mode, &ptr[5], 2*cnt);
    
    if (ind == current_sen_profile)
        current_sen_profile++; 
    return;
}
 */

void do_ma_control(UINT8* ptr){
    MA_CONTROL_PKT *ma_control;
    UINT16 temp_condition = 0x0000;
    memcpy(ma_control, &ptr[0], sizeof(MA_CONTROL_PKT));   
    my_control = ma_control->controlMode;
    my_scene = ma_control->sceneID;
    printf("MA Control Mode = %d  ", my_control, "My SceneID = %d", my_scene);
    // timer setting TODO // htw
    my_act_bitmap = 0x0000;
    act_bitmap = 0x0000;	
}

void checkAck(UINT8* ptr) {
    UINT8 index =0;
    UINT8 cnt=0;
    UINT8 ind;
    UINT8 groupID=1; // htw setting /considering
    //MASTER_ACT_PROFILE temp_ma;
    //save_sensor_value()
    ind = findProfileID(groupID);    
    UINT8 num_act = my_ma_profile[ind].num_act;
    if (act_bitmap == 0x0000)
	act_bitmap = act_bitmap | (0xFFFF& (0x0001FFFF << num_act)>>16); 
    for( UINT8 i=0; i < num_act;i++){
        if(my_ma_profile[ind].act_addr[i] == rx_src_addr){
		index = i;
		break;
	}
    }
    ACTUATION_INFO_RPT_PKT act_info_rpt; // to MA
    act_info_rpt.msgID = ptr[cnt++];
    act_info_rpt.sceneID = ptr[cnt++];
    act_info_rpt.status = ptr[cnt++];
    if(act_info_rpt.sceneID == my_scene)
	if(act_info_rpt.status == 0x00)
	    my_act_bitmap |=   ((0x01) << index);
    
    if(my_act_bitmap == act_bitmap) 
         report_Actuation();  // after it's done fully, then send report_actuation
}

void process_rx(void *args) 
{
    int ret;
    //char msg[10];
    ret = 1;
    while (ret >=0) {
        ret = dequeue_rx();
        if (ret <0)
            break;
        printf("rxQ[%d]: msgID=%d\n", ret, rx_queue[ret].msgID);
        // TODO: process received msg with sending ACK
        // MA only
        //rx_src_addr = (rx_queue[ret].src_addr[14]);
                    // rx_queue[ret].src_addr[16]&0xff;
        if (rx_queue[ret].msgID == ID_DEPLOY_MA_PROFILE) {
            // send Ack
            send_Ack(ret, ID_DEPLOY_MA_PROFILE_ACK, wsan_port_send);
            // TEST: save MA profile here
            // MASTER_ACT_PROFILE* ma_ptr = (MASTER_ACT_PROFILE*)rx_queue[ret].msg;
            save_ma_profile((UINT8*)rx_queue[ret].msg);
        }
/* 
       // actuator only
        else if (rx_queue[ret].msgID == ID_DEPLOY_ACT_PROFILE) {
            // send Ack
            send_Ack(ret, ID_DEPLOY_ACT_PROFILE_ACK, wsan_port_send);
            // TEST: save Act profile here            
            save_act_profile(rx_queue[ret].msg);
        }        
        else if (rx_queue[ret].msgID == ID_SCENE_INFO) {
            // TEST: Do actuation based on ACT_PROFILE
            do_actuation((UINT8)rx_queue[ret].msg[1], ret);
        }
        else if (rx_queue[ret].msgID == ID_DEPLOY_SEN_PROFILE) {
            // send Ack
            send_Ack(ret, ID_DEPLOY_SEN_PROFILE_ACK, wsan_port_send);
            // TEST: save Sen profile here
            // SENSOR_PROFILE* sen_ptr = (SENSOR_PROFILE*)rx_queue[ret].msg;
            save_sen_profile((UINT8*)rx_queue[ret].msg);
        }
*/
        // MA only
        else if (rx_queue[ret].msgID == ID_RPT_SENSING) {
            // send Ack
            send_Ack(ret, ID_RPT_SENSING_ACK, wsan_port);
            // TODO: process sensing data here
            do_decision_making((UINT8*)rx_queue[ret].msg);
        }
        /* else if (rx_queue[ret].msgID == ID_RPT_ACT) { */
        /*     // send Ack */
        /*     send_Ack(ret, ID_RPT_ACT_ACK); */
        /*     // TODO: process actuation data here */
        /* } */
        else if (rx_queue[ret].msgID == ID_MA_CTRL) {
            // send Ack
            send_Ack(ret, ID_MA_CTRL_ACK, wsan_port_send);
            do_ma_control((UINT8*)rx_queue[ret].msg);
            
            // TODO: process actuation control (sending ID_SCENE_INFO to the ACTs)
        }
        else if (rx_queue[ret].msgID = ID_ACT_CTRL) {
            // TODO: send ID_RPT_ACT here and process the contorl
        }
        else if (rx_queue[ret].msgID == ID_REQ_ACT_STAT) {
            // TODO: respond ID_RSP_ACT_STAT here
        }
        else if (rx_queue[ret].msgID == ID_REQ_SEN_VAL) {
            // TODO: respond ID_RSP_ACT_STAT here
        }
        else if (rx_queue[ret].msgID == ID_RPT_ACT_INFO_MA){ // TBD
	    // 
            checkAck((UINT8*)rx_queue[ret].msg);
        }
    }
}
///////////////////////////////////////////////////////////




void init_profile()
{
    memcpy(locL, "12-509", 6);
    locPx = 10;
    locPy = 20;
    aType = 1; // Master actuator (TBD)

//    my_act_profile.num_scene = 0;
//    my_decision.SceneID=0;

}

void report_join()
{
    char *msg;
    ERROR_T rsp_error;
    char text_addr[IP6_NTOP_BUF_SZ];
    REPORT_JOIN_PKT report_join_pkt;
    TX_PACKET join_tx;

    //memcpy(locL, "12-509", 6);
    base_addr.s6_addr[14] = (UINT8)(node_id >>8);
    base_addr.s6_addr[15] = (UINT8)node_id;
    report_join_pkt.msgID = ID_RPT_JOIN;
    memcpy(&report_join_pkt.did, &base_addr, 16);
    report_join_pkt.dType = TYPE_MASTER_ACT; // actuator
    memcpy(report_join_pkt.locL, locL, 10);
    report_join_pkt.locPx = locPx;
    report_join_pkt.locPy = locPy;
    report_join_pkt.aType = aType;
    report_join_pkt.sType = 0x0000;
        
    join_tx.expired_time = 0;
    join_tx.dest = 0x0001; // for server (reserved)
    join_tx.msgID = ID_RPT_JOIN;
    join_tx.port = wsan_port_send;
    memcpy(join_tx.msg, &report_join_pkt, sizeof(REPORT_JOIN_PKT));
    join_tx.msg_len = sizeof(REPORT_JOIN_PKT);
    join_tx.ack_time = 1000; // msec

    /* printf("To : %d, %s, %s, %d\n", */
    /*        join_tx.msg_len, report_join_pkt.locL, locL, strlen(locL)); */
    /* printf("port = %d\n", join_tx.port); */
    add_to_list(join_tx);
     
}
////////////////////////////////////////////////////////////////
// JJ: 

void nara_uart_recv(UINT8 port, char ct)
{
    UINT8 c = (UINT8)ct;
    //printf("rcv: %x, ", c);
    if (serialIndex > 100)
    {
        commandStatus = COMMAND_READY;
        serialIndex = 0;
        return;
    }

    if (c == 0x40 && commandStatus == COMMAND_READY)
    {
        commandStatus = COMMAND_INCOMING_0;
    }
    // check msgID here
    else if (commandStatus == COMMAND_INCOMING_0)
    {
        msgID = c;
        commandStatus = COMMAND_INCOMING_1;
    }
    else if (commandStatus == COMMAND_INCOMING_1)
    {
        serialLength = c;
        commandStatus = COMMAND_INCOMING;
    }
/*
    else if (c == 0xE1 && commandStatus == COMMAND_INCOMING)
    {
        if (serialIndex < serialLength)
        {
            serialBuffer[serialIndex] = c;
            serialIndex++;
        }
        else
        {
            commandStatus = COMMAND_STARTED;
            nos_taskq_reg(nara_uart2rf, NULL);
        }
    }
*/
    else if (commandStatus == COMMAND_INCOMING)
    {
        // TODO: checksum check here?
        serialBuffer[serialIndex] = c;
        serialIndex++;
        //printf("index and len = %x %d\n", serialIndex, serialLength);
        if (serialIndex == serialLength + 1)
        {
            commandStatus = COMMAND_STARTED;
            //nos_taskq_reg(nara_uart2rf, NULL);
        }
        if (serialIndex > serialLength + 1)
        {
            commandStatus = COMMAND_READY;
            serialIndex = 0;
        }
    }
    else
    {
        // COMMAND_READY or COMMAND_STARTED and serial data is not C0.
        printf("err %x\n", c);
    }
}


////////////////////////////////////////////////////////////////

void rpl_status_report(void *args)
{
    char msg[200];
    static UINT16 sent = 0;

    if (ip6_is_attached(def_inf))
    {
        sent++;
        memset(msg, 0, 200);

        msg[0] = (sent >> 8);
        msg[1] = (sent & 0xff);
        rpl_get_root_address(def_inf, &root_addr);
        udp_sendto(def_inf, 1234, &root_addr, 1234, msg, 15);
        printf("Send! (%u)\n", sent);
    }
    else
    {
        printf("Network detached.\n");
    }
}

void listener(UINT8 in_inf,
              const IP6_ADDRESS *src_addr,
              UINT16 src_port,
              UINT16 dst_port,
              const UINT8 *msg, UINT16 len)
{
    char text_addr[IP6_NTOP_BUF_SZ];
    UINT16 addr;
    
    printf("Recv from [%s]:%u (%u byte) \n",
            ip6_ntop(src_addr, text_addr), src_port, len);
    
    // TODO: Enqueue and call the nos_taskq_reg
    RX_DATA recv;
    memcpy(&recv.src_addr, src_addr, 16);
    recv.src_port = src_port;
    recv.dst_port = dst_port;
    memcpy(recv.msg, msg, len);
    recv.len = len;
    recv.msgID = msg[0];
    rx_src_addr = (UINT16)(src_addr->s6_addr[14] << 8) + (UINT16)src_addr->s6_addr[15];

    // if received msg is an ack
    if (recv.msgID > 0x30) {
        // TODO: delete the matched tx_list 
        //memcpy(&addr, &src_addr->s6_addr[14], 2);
        addr = (UINT16)(src_addr->s6_addr[14] << 8) + (UINT16)src_addr->s6_addr[15];
        printf("isAck? %d, from %d\n", recv.msgID, addr);
        if (!memcmp(src_addr, &server_addr, 16)) {
            addr = 0x0001;
        }
        
        delete_from_list(addr, recv.msgID - 0x30); // Do we need to check the return value?
        //print_list();
    }
    else {
        if (!enqueue_rx(recv))
            printf("Error to process the received msg\n");

        nos_taskq_reg(process_rx, NULL);
    }
}

void ip6_state_changed(UINT8 ip6_inf, IP6_NODE_STATE state)
{
    printf("State changed to %d\n", state);
    if (ip6_is_attached(def_inf))
    {
        isConnected = 1;
        printf("RPL network connection established.\n");

        // send report_join here
        report_join();

        rpl_get_root_address(def_inf, &root_addr);
        led_on(1);
        if (send_timer_id < 0)
        {
            send_timer_id = user_timer_create_sec(rpl_status_report, NULL, 50,
                                                  USER_TIMER_PERIODIC);
            printf("send_timer_id:%d\n", send_timer_id);
        }
    }
}

UINT16 keep_cnt=0;
void keep_alive(void)
{
    printf("keep: %d\n", keep_cnt++);
}

int main(void)
{
    nos_init();
    led_on(0);

    printf("\n*** NanoQplus ***\n");

    // JJ: set channel and modulation
    rf212_set_mode(0, RF212_OQPSK_250KBPS_SIN);
    rf212_set_channel(0, 1);

    // Initialize the LoWPAN interface.
    node_ext_id[6] = node_id >> 8;
    node_ext_id[7] = node_id & 0xff;
    def_inf = lowpan_init(0, 1, 0xffff, node_ext_id,30, 1);
    ip6_set_state_notifier(def_inf, ip6_state_changed);
    rpl_init(def_inf);
    ip6_init(def_inf);
    
    printf("Node Id = %d\n", node_id);
    init_profile();

// test
    
    /* struct tx_list *ptr = NULL; */
    /* TX_PACKET temp_list[10]; */
    /* int i, ret; */
    /* /\* for (i=0; i< 10; i++) { *\/ */
    /* /\*     temp_list[i].expired_time = 1; *\/ */
    /* /\*     temp_list[i].dest = i+1; *\/ */
    /* /\*     temp_list[i].msgID = i+2; *\/ */
    /* /\*     temp_list[i].port = 11; *\/ */
    /* /\*     temp_list[i].msg[5] = "test"; *\/ */
    /* /\*     temp_list[i].msg_len = 5; *\/ */
    /* /\*     temp_list[i].ack_time = 10 + 10*i; *\/ */
    /* /\*     add_to_list(temp_list[i]); *\/ */
    /* /\* } *\/ */
    /* temp_list[0].expired_time = 10; */
    /* temp_list[0].dest = 0x0001; */
    /* temp_list[0].msgID = 0x01; */
    /* temp_list[0].port = 1235; */
    /* temp_list[0].msg[5] = "test"; */
    /* temp_list[0].msg_len = 5; */
    /* temp_list[0].ack_time = 1000; */
    /* add_to_list(temp_list[i]); */

    /* print_list(); */

    /* ptr = search_in_list(0x0001, 0x01, NULL); */
    /* if (NULL == ptr) */
    /*     printf("search failed\n"); */
    /* else */
    /*     printf("searched %d and %d\n", ptr->val.dest, ptr->val.msgID); */

    /* ret = delete_from_list(0x0001, 0x01); */

    
    /* print_list(); */

    
    udp_set_listen(wsan_port, listener);

    // process_tx start here
    user_timer_create_ms(process_tx, NULL, 40, USER_TIMER_PERIODIC);
    user_timer_create_sec(keep_alive, NULL, 2, USER_TIMER_PERIODIC);

    sched_start();
    return 0;
}
