Author: Jongsoo Jeong (Jongsoo Jeong (ETRI))
Date: Aug. 10. 2011.

- $LastChangedDate: 2012-10-11 11:12:57 +0900 (Thu, 11 Oct 2012) $
- $LastChangedBy: jsjeong $
- $Rev: 993 $
- $Id: README 993 2012-10-11 02:12:57Z jsjeong $

This application is for monitoring and observing behavior of RPL 
(IPv6 Routing Protocol for Low-power and Lossy Networks). It consists 
of a mote-side and a PC-side applications. The mote-side application 
makes motes to act as RPL nodes, and report properties related with 
RPL every minute. The properties are followings.

* Seq: message sequence number
* OCP: Objective code point
* InstanceID: RPL instance ID
* MOP: Mode of operation
* PCS: Path control size
* Ver: DODAG Version
* Interval: DIO interval level
* Consistencies: number of received consistent DIO messages
* Rank: rank (depth) of the node
* Parent: short address of the most preferred parent
* DIO Resets: number of DIO timer resets
* DIS: number of sending DIS messages
* DAO Seq: sequence number of the DAO message has to be sent next
* NC size: number of neighbors
* Loop: number of loops (inconsistent events) occurred
* Fail: number of send fails
* Fwd fail: number of forward fails
* NS: number of sending NS message
* NA: number of sending NA message
* RS: number of sending RS message
* RA: number of sending RA message
* Malloc fail: number of MALLOC fails
* Taskq fail: number of registering task to taskq fails

The PC-side application opens an UDP port to receive nodes' report
messages, and waits for them. Whenever it receives reports, it prints 
them for you.

Installation
------------

In order to make a RPL node, you should change CHANNEL, PAN_ADDR, 
and SHORT_ADDR before compilation. Note that all nodes including 
border router within a network should share same CHANNEL and PAN_ADDR 
while each of them has an unique SHORT_ADDR.

Then, compile and download application.

$ make
$ make burn port={port name}
 
Setting up the IP border router
-------------------------------

The IP border router for LLNs is required to connect a RPL network 
with the Internet. We provide Linux and Click based router in 
'$NOS_HOME/apps/ip-base-station'.

Monitor
-------

Since the PC-side application was written in Java, you need JRE or 
JDK to execute it.

$ cd rpl-network-monitor-java
$ javac *.java
$ java RplNetworkMonitor [{listening port}]

The argument 'listening port' can be elided. If you run without 
specifying listening port, it will use default port number '65363'.

Note that this application uses an UDP port. So you have to make sure 
that it is an empty port. If your machine already uses it, you can 
change this port number by both modifying 'RPL_MONITOR_PORT' of 
'rpl-node.c' and specifying 'listening port' when you execute
'RplNetworkMonitor.java' on command line.