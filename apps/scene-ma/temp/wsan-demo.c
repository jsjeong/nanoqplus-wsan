/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Jongsoo Jeong (ETRI)
 * @author JeongGil Ko
 * @date 2012. 11. 21.
 * @brief Modifications to rpl-node.c to initiate skeleton code for WSAN 3rd year demo
 */

/*
 * $LastChangedDate: 2012-11-02 10:14:28 +0900 (Fri, 02 Nov 2012) $
 * $Id: rpl-node.c 1029 2012-11-02 01:14:28Z jsjeong $
 */

#include "nos.h"
#include "noslib.h"
#include <stdlib.h>
#include <string.h>

#include "wsan-profile-1.h" // JJ added for WSAN profile
#include "rf212.h"  // JJ: add for set channel
// #define DEBUG_TIMER    // enable if you want to see the timer_debug msg
 #define DEBUG_PROFILE  // enable if you want to see the profile_debug msg

UINT8 lowpan_inf = 255;

char udp_buf[MAX_UDP_LENGTH]; // udp buffer length

UINT16 node_id = NODE_ID;  // actuator id = 5
UINT8 SEND_MSG_TIMER = 0xFF;

// TIMER VARIABLES
UINT16 PACKET_INTERVAL = 5000UL;
UINT16 MINIMUM_PACKET_INTERVAL = 2000UL;
UINT16 MAX_PACKET_INTERVAL = 2000UL;//0xFFFF;

// JJ: add global variables
// TODO: Initial value here!
DEVICE my_device;
LOCATION my_location;

// added for ACK check
UINT8 report_timer_id;
UINT8 report_state;

BOOL KEEP_NG_ACT_FLAG = TRUE;
IP6_ADDRESS DATA_REPORT_ADDR;

// JJ: add global variables
GENERIC_SENSOR my_sensors;
SENSOR_PROFILE my_sen_profile[MAX_SENSOR_PROFILE];
UINT8 cur_sen_profile =0;
UINT8 EVENT_SITUATION = FALSE;
//UINT8 my_sen_type = 0xfc;   // 6 sensors 
INT16 prev_temp = 0, prev_hum = 0, prev_illum = 0, prev_co = 0, prev_co2 = 0, prev_pir = -1;
// JJ: need to check the initial THRs
INT16 temp_THR = 40;
INT16 hum_THR = 4;
INT16 illum_THR = 4;
INT16 co_THR = 4;
INT16 co2_THR = 4;
INT16 pir_THR = 1;
UINT8 ZEROVALUE = 0;
UINT8 profile_st_flag =0;
UINT8 COOP_SENSOR_TYPE;
UINT8 COOP_P_ID;

// adaptive sensing and report
UINT8 TRICKLE_TIMER_ID = 0;
BOOL trickleIsRunning = FALSE;
UINT8 CURRENT_TARGET_SENSOR = 0;
UINT8 EVENT_DETECTED[100]; // this array keeps track of the nodes that have beed determined to have events

action_noti_pkt ans_pkt;
// TODO: TYPE initial value here!!
void init_profile(void) {
  memcpy(my_device.mfg,"ETRI",4);
  memcpy(my_device.mdl,"WSAN",4);
  my_location.x_int = 10;
  my_location.x_point = 11;
  my_location.y_int = 9;
  my_location.y_point = 12;
  memcpy(my_location.sem,"12-501",6);
  // TODO: Init. and save the data from the real sensors
  // each saved data indicates previous sent data by adaptive sampling
  my_sensors.sensor_type = 0xfc;
  if (my_sensors.sensor_type & 0x80) {
    //my_sensors.temp_int = 18;
    //my_sensors.temp_point = 1;
    my_sensors.temp = 0;
  }
  if (my_sensors.sensor_type & 0x40)
    my_sensors.hum = 30;
  if (my_sensors.sensor_type & 0x20)
    my_sensors.light = 100;
  if (my_sensors.sensor_type & 0x10)
    my_sensors.co = 0;
  if (my_sensors.sensor_type & 0x08)
    my_sensors.co2 = 0;
  if (my_sensors.sensor_type & 0x04)
    my_sensors.pir = FALSE;
    
}

// clear all profiles!!
void reset_profile(void) {
  UINT8 i, j;

  prev_temp = 0, prev_hum = 0, prev_illum = 0, prev_co = 0, prev_co2 = 0, prev_pir = -1;
  // TODO: stop co-op reporting 
  if (EVENT_SITUATION) {
    if(SEND_MSG_TIMER != 0xFF) {
      user_timer_destroy(SEND_MSG_TIMER);
    }
  }

  // kill trickle timer
  if (trickleIsRunning) {
    user_timer_destroy(TRICKLE_TIMER_ID);
  }

  EVENT_SITUATION = FALSE;
  profile_st_flag =0;

  if (cur_sen_profile >0) {
    for (i=0; i< cur_sen_profile; i++) {
      my_sen_profile[i].P_ID = 255;
      my_sen_profile[i].A_ID = 65535;
      my_sen_profile[i].at = 0;
      my_sen_profile[i].st = 0;
      my_sen_profile[i].st2 = 0;
      for (j=0; j< my_sen_profile[i].num_sensor; j++) {
	my_sen_profile[i].diff[j] = 65535;
	my_sen_profile[i].tmin[j] = 100;
	my_sen_profile[i].tmax[j] = 65535;
      }
      my_sen_profile[i].num_sensor = 0;
    }
  }
  cur_sen_profile = 0;
}
/////////////////////////////////////////////////////////////////////////////////////
// from JK

BOOL checkIsRedundant(void)
{
   
//  if (cur_sen_profile ==0)
  //  return FALSE;

  for (UINT8 i=0; i < cur_sen_profile; i++){ // JK: I wonder if this keeps track of the total number of profiles that I am dealing with
    my_sen_profile[i].st2 = my_sen_profile[i].st;
    profile_st_flag |= my_sen_profile[i].st;
    for(UINT8 k=0; k < i; k++){
      if(my_sen_profile[i].A_ID == my_sen_profile[k].A_ID){  // profile dstinationa address is redundant?
        my_sen_profile[i].st2 &= ~my_sen_profile[k].st;
//        printf("k = %d i = %d st=%d\n ", k,i, my_sen_profile[i].st2);
      }
    }
    printf("i=%d  st2=%d st=%d\n ", i, my_sen_profile[i].st2, my_sen_profile[i].st);  
  } 
  return TRUE;   
}

UINT16 SERVER_SEND_COUNT = 0;

void send_messageto(UINT8* SENSOR_TYPE_IN)
{


  IP6_ADDRESS lserver_addr = {{
      0xaa, 0xaa, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11 }};

  // this function is used to send periodic or event based packets. Cancel out the last timer line when using it for event-based algorithms
  //UINT16 msg[100];  
  struct sensor_report_pkt srp;
  char msg_get[50];
  memset(msg_get, 0, 50);  // set all zero for init.

  SENSOR_PROFILE mysen;
  IP6_ADDRESS temp_actuator_addr = actuator_addr;
  UINT16 targetaddr;
  
  SEND_MSG_TIMER = 0xFF;
  
  UINT8 SENSOR_TYPE = 0;
  if(SENSOR_TYPE_IN != NULL)
    SENSOR_TYPE = *SENSOR_TYPE_IN;

  printf("ENTER SMT \n");
  SERVER_SEND_COUNT ++;

  BOOL FLAG_TEMP = TRUE;
  BOOL FLAG_HUM = TRUE;
  BOOL FLAG_CO = TRUE;
  BOOL FLAG_CO2 = TRUE;
  BOOL FLAG_LIGHT = TRUE;
  BOOL FLAG_PIR = TRUE;

  //sprintf(msg, "Hello! [%u]", seq++);
  //srp = (struct sensor_report_pkt*) msg;
  //udp_sendto(1234, &server_addr, 1234, msg, strlen(msg) + 1);
  //for (UINT8 i=0; i < MAX_SENSOR_PROFILE; i++){


  if(SENSOR_TYPE != 0){
    // in this case, we are looking for a specific sensor not the entire thing...

    FLAG_TEMP = FALSE;
    FLAG_HUM = FALSE;
    FLAG_CO = FALSE;
    FLAG_CO2 = FALSE;
    FLAG_LIGHT = FALSE;
    FLAG_PIR = FALSE;

    if(SENSOR_TYPE == TEMP_SENSOR){
      FLAG_TEMP = TRUE;
    }else if(SENSOR_TYPE == HUM_SENSOR){
      FLAG_HUM = TRUE;
    }else if(SENSOR_TYPE == LIGHT_SENSOR){
      FLAG_LIGHT = TRUE;
    }else if(SENSOR_TYPE == CO_SENSOR){
      FLAG_CO = TRUE;
    }else if(SENSOR_TYPE == CO2_SENSOR){
      FLAG_CO2 = TRUE;
    }else if(SENSOR_TYPE == PIR_SENSOR){
      FLAG_PIR = TRUE;
    }
 
  }

  //if(SENSOR_TYPE == TEMP_SENSOR){
  //}else if(SENSOR_TYPE == HUM_SENSOR){
  //}

  //}else{

    if(EVENT_SITUATION && !KEEP_NG_ACT_FLAG) {

    // TODO:TW  make the packet!!!!
    // TODO:TW make the packet distinguished as co-op reporting!!
    srp.coop_report_flag=TRUE;
      if(SENSOR_TYPE == TEMP_SENSOR) {
        srp.Sensor_type = TEMP_SENSOR;
	//srp.Value = temperature_sensor_read(DEFAULT);
        srp.Value = sensor_read(DEFAULT_TEMP); // JJ modify 2013.11.25
	sprintf(msg_get, "RPT /%s/%s %d", sen_name, temp_name, srp.Value);  
      }else if (SENSOR_TYPE == HUM_SENSOR) {
        srp.Sensor_type = HUM_SENSOR;
	// srp.Value = humidity_sensor_read(DEFAULT);
	srp.Value = sensor_read(DEFAULT_HUM); // JJ modify 2013.11.25
        sprintf(msg_get, "RPT /%s/%s %d", sen_name, hum_name, srp.Value);  
      }else if(SENSOR_TYPE == LIGHT_SENSOR) {
        srp.Sensor_type = LIGHT_SENSOR;
	// srp.Value = light_sensor_read(DEFAULT);
        srp.Value = sensor_read(DEFAULT_LIGHT); // JJ modify 2013.11.25
	sprintf(msg_get, "RPT /%s/%s %d", sen_name, light_name, srp.Value);  
      }else if(SENSOR_TYPE == CO_SENSOR) {
        srp.Sensor_type = CO_SENSOR;
	// srp.Value = co_sensor_read(DEFAULT);
        srp.Value = sensor_read(DEFAULT_CO); // JJ modify 2013.11.25
	sprintf(msg_get, "RPT /%s/%s %d", sen_name, co_name, srp.Value);  
      }else if(SENSOR_TYPE == CO2_SENSOR) {
        srp.Sensor_type = CO2_SENSOR;
	// srp.Value = co2_sensor_read(DEFAULT);
        srp.Value = sensor_read(DEFAULT_CO2); // JJ modify 2013.11.25
	sprintf(msg_get, "RPT /%s/%s %d", sen_name, co2_name, srp.Value);  
      }else if(SENSOR_TYPE == PIR_SENSOR) {
        srp.Sensor_type = PIR_SENSOR;
	srp.Value = pir_sensor_read(); // htw modify 2013.01.07
        //pir_sensor_on(); // JJ modify 2013.11.25
	//srp.Value = pir_sensor_get_data();
	//pir_sensor_off();
	sprintf(msg_get, "RPT /%s/%s %d", sen_name, pir_name, srp.Value);  
      }else {
      printf("type is not matched!!!!  type = %u\n",COOP_SENSOR_TYPE);
      if(SEND_MSG_TIMER != 0xFF) {
	user_timer_destroy(SEND_MSG_TIMER);
      }
      SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, (void*)&COOP_SENSOR_TYPE, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
      return;
    }
      
    srp.coop_report_flag=TRUE;
    srp.N_ID = node_id;
    srp.P_ID = COOP_P_ID;
    targetaddr=DATA_REPORT_ADDR.s6_addr[14];
    targetaddr=(targetaddr<<8)+DATA_REPORT_ADDR.s6_addr[15];
    printf("#Co-op EVENT SITUATION To = %u, ap_id=%u, st = %u, val = %u\n", DATA_REPORT_ADDR.s6_addr16[7], COOP_P_ID, COOP_SENSOR_TYPE, srp.Value);

    IP6_ADDRESS lDATA_REPORT_ADDR;
    memcpy(&lDATA_REPORT_ADDR, &DATA_REPORT_ADDR, 16);
    udp_sendto(1234, &DATA_REPORT_ADDR, 1234, &srp, sizeof(srp)); // actuator addresss can be changed //
//    delay_ms(100);  // udp packet delay // htw modify

    if(!(SERVER_SEND_COUNT % 5) && EVENT_SITUATION){
      udp_sendto(1235, &lserver_addr, 1235, &msg_get, sizeof(msg_get));
    }    // htw modify periodic report
    

    led_toggle(1);
    if(SEND_MSG_TIMER != 0xFF) {
      user_timer_destroy(SEND_MSG_TIMER);
    }
    SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, (void*)&COOP_SENSOR_TYPE, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
    //user_timer_create_ms((void*)send_messageto, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
    return;
  }


  for (UINT8 i=0; i < cur_sen_profile; i++){ // JK: I wonder if this keeps track of the total number of profiles that I am dealing with
    mysen = my_sen_profile[i];
//    printf("cur_sen_profile i= %d st=%d\n " ,i ,mysen.st2);
    //  BOOL isSensing=TRUE;
    while(mysen.st2){
      srp.coop_report_flag=FALSE;
      srp.N_ID = node_id;
      srp.P_ID = mysen.P_ID;
      //  srp.Sensor_type = TEMP_SENSOR;  
      //          save_ip6_addr(profile_value, val_len, dst_addr); // htw I wonder if it is correc
      //          mysen.dst = dst_addr[7];
      // configure actuator_addr here!!
      
      temp_actuator_addr.s6_addr[14] = (UINT8)(mysen.A_ID >> 8);
      temp_actuator_addr.s6_addr[15] = (UINT8)(mysen.A_ID);
      
      if(mysen.st2 & TEMP_SENSOR && FLAG_TEMP){
	srp.Sensor_type = TEMP_SENSOR; 
	// srp.Value = temperature_sensor_read(DEFAULT);
	srp.Value = sensor_read(DEFAULT_TEMP); // JJ modify 2013.11.25
	mysen.st2 = ~TEMP_SENSOR & mysen.st2;
        sprintf(msg_get, "RPT /%s/%s %d", sen_name, temp_name, srp.Value);  
      }
      else if(mysen.st2 & HUM_SENSOR && FLAG_HUM){
	srp.Sensor_type = HUM_SENSOR; 
	// srp.Value = humidity_sensor_read(DEFAULT);
	srp.Value = sensor_read(DEFAULT_HUM); // JJ modify 2013.11.25
	mysen.st2 = ~HUM_SENSOR & mysen.st2;
        sprintf(msg_get, "RPT /%s/%s %d", sen_name, hum_name, srp.Value);  
	
      }
      else if(mysen.st2 & LIGHT_SENSOR && FLAG_LIGHT){
	srp.Sensor_type = LIGHT_SENSOR;  
	// srp.Value = light_sensor_read(DEFAULT);
	srp.Value = sensor_read(DEFAULT_LIGHT); // JJ modify 2013.11.25
	mysen.st2 = ~LIGHT_SENSOR & mysen.st2;
        sprintf(msg_get, "RPT /%s/%s %d", sen_name, light_name, srp.Value);  
      }
      else if(mysen.st2 & CO_SENSOR && FLAG_CO){
	srp.Sensor_type = CO_SENSOR;  
	// srp.Value = co_sensor_read(DEFAULT);
	srp.Value = sensor_read(DEFAULT_CO); // JJ modify 2013.11.25
	mysen.st2 = ~CO_SENSOR & mysen.st2;
        sprintf(msg_get, "RPT /%s/%s %d", sen_name, co_name, srp.Value);  
      }
      else if(mysen.st2 & CO2_SENSOR && FLAG_CO2){
	srp.Sensor_type = CO2_SENSOR;
	// srp.Value = co2_sensor_read(DEFAULT);
	srp.Value = sensor_read(DEFAULT_CO2); // JJ modify 2013.11.25
	mysen.st2 = ~CO2_SENSOR & mysen.st2;
        sprintf(msg_get, "RPT /%s/%s %d", sen_name, co2_name, srp.Value);  
      }
      else if(mysen.st2 & PIR_SENSOR && FLAG_PIR){
	srp.Sensor_type = PIR_SENSOR;  
	srp.Value = pir_sensor_read(); // htw modify 2013.01.07
	//pir_sensor_on(); // JJ modify 2013.11.25
	//srp.Value = pir_sensor_get_data();
	//pir_sensor_off();
	mysen.st2 = ~PIR_SENSOR & mysen.st2;
        sprintf(msg_get, "RPT /%s/%s %d", sen_name, pir_name, srp.Value);  
      }
      else {
	mysen.st2=0;
	break;
      }
            
     if(!EVENT_SITUATION)
       printf("Reporting st = %d, val = %d Addr = %u\n", srp.Sensor_type, srp.Value, mysen.A_ID);
     
     udp_sendto(1234, &temp_actuator_addr, 1234, &srp, sizeof(srp)); // actuator addresss can be changed //
//      printf("#l = %d\n", mysen.A_ID, srp.Sensor_type, srp.Value, PACKET_INTERVAL);


     if(!(SERVER_SEND_COUNT % 5) && EVENT_SITUATION){
       udp_sendto(1235, &lserver_addr, 1235, &msg_get, sizeof(msg_get));
       printf("msg = %s, count = %u\n",msg_get, SERVER_SEND_COUNT);
     }

      //delay_ms(100);  // udp packet delay // htw modify
      
      led_toggle(1);
      
      if(EVENT_SITUATION){
        printf("#Normal Event Situation to= %u, st = %d, val = %d packet interval = %d\n", mysen.A_ID, srp.Sensor_type, srp.Value, PACKET_INTERVAL);
	if(SEND_MSG_TIMER != 0xFF){
	  user_timer_destroy(SEND_MSG_TIMER);
	}
	SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
	printf("send msg timer id = %u\n", SEND_MSG_TIMER);
	
        return;
      }
    } 
  }

  //print_routes();
  //user_timer_create_ms(send_messageto, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
  //return;
  // if this is not an event situation perform adaptive sampling... if this is an event situation perform periodic sampling...
/*
  if(EVENT_SITUATION){
  }else{
    // we are going to use the trickle interval to compute our target time...???????
  }
*/
}

void trickle_timer(void *args) {
  if(EVENT_SITUATION)
    return;

  trickleIsRunning = FALSE;

  UINT16 RAND_TIME = rand() % PACKET_INTERVAL + 1;
#ifdef DEBUG_TIMER
  printf("RT = %d, PI = %d\n", RAND_TIME, PACKET_INTERVAL);
#endif  // DEBUG_TIMER
  if(SEND_MSG_TIMER != 0xFF){
    user_timer_destroy(SEND_MSG_TIMER);
  }
  SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, NULL, RAND_TIME, USER_TIMER_ONE_SHOT);

  
  if(PACKET_INTERVAL < MAX_PACKET_INTERVAL) 
    PACKET_INTERVAL *= 2;

  //user_timer_deactivate(TRICKLE_TIMER_ID);
  if(!trickleIsRunning){
    TRICKLE_TIMER_ID = user_timer_create_ms(trickle_timer, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT);
    trickleIsRunning = TRUE;
    //printf(STDIO, "tid = %d\n", TRICKLE_TIMER_ID);
  }
  /*
  while(1) {
    TRICKLE_TIMER_ID = user_timer_create_ms(trickle_timer, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT);
    if (TRICKLE_TIMER_ID < 254) {
      break;
    }
  }
  */
}

void adaptive_sampling_triggered(UINT8 SENSOR_TYPE) {
  printf("Threshold Passed %d\n", SENSOR_TYPE);
  CURRENT_TARGET_SENSOR = SENSOR_TYPE; // we need to keep this value since this function will terminate...
  if(SEND_MSG_TIMER != 0xFF){
    user_timer_destroy(SEND_MSG_TIMER);
  }
  SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, (void*)&CURRENT_TARGET_SENSOR, 2, USER_TIMER_ONE_SHOT);
  printf("Adaptive Sampling Triggered %d\n", SENSOR_TYPE);
  // Implementation of trickle timer is required at this point... 
  PACKET_INTERVAL = MINIMUM_PACKET_INTERVAL;
  //if (TRICKLE_TIMER_ID != 0) {
  if(trickleIsRunning) {
#ifdef DEBUG_TIMER
    printf("tid = %d\n", TRICKLE_TIMER_ID);
#endif  // DEBUG_TIMER
    //user_timer_deactivate(TRICKLE_TIMER_ID); changed to destory hyunhak 2013.1.8
    user_timer_destroy(TRICKLE_TIMER_ID);
    trickleIsRunning = FALSE;
  }
  if(!trickleIsRunning){
    TRICKLE_TIMER_ID = user_timer_create_ms(trickle_timer, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT);
    trickleIsRunning = TRUE;
#ifdef DEBUG_TIMER
    printf("tid = %d\n", TRICKLE_TIMER_ID);
#endif  // DEBUG_TIMER
  }
  /*
    while(1) {
    TRICKLE_TIMER_ID = user_timer_create_ms(trickle_timer, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT);
    if (TRICKLE_TIMER_ID < 254) {
    break;
    }
    }
  */
  ////user_timer_create_ms(send_message, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT);
}

void check_sensor(void *args) {
  // We are going to check the sensors and see if adaptive reporting needs to happen...
  INT16 temp=0, hum=0, illum=0, co=0, co2=0, pir=0;//, diff_temp, diff_hum, diff_illum, diff_co, diff_co2;
  //UINT8 i;  // JJ: added
  INT16 cur_temp=0, cur_hum=0, cur_illum=0, cur_co=0, cur_co2=0, cur_pir=0;

//  printf("Profile SensorType Flag = %d \n", profile_st_flag);

  if(EVENT_SITUATION)
    return;

  if (profile_st_flag & TEMP_SENSOR){
    // cur_temp = temperature_sensor_read(DEFAULT);
    cur_temp = sensor_read(DEFAULT_TEMP); // JJ modify 2013.11.25
    temp = abs(prev_temp -cur_temp );
  }
  else if (profile_st_flag & HUM_SENSOR){
    // cur_hum = humidity_sensor_read(DEFAULT);
    cur_hum = sensor_read(DEFAULT_HUM); // JJ modify 2013.11.25
    hum = abs(prev_hum - cur_hum);
  }
  else if (profile_st_flag & LIGHT_SENSOR){
    // cur_illum = light_sensor_read(DEFAULT);
    cur_illum = sensor_read(DEFAULT_LIGHT); // JJ modify 2013.11.25
    illum = abs(prev_illum - cur_illum);
  }
  else if (profile_st_flag & CO_SENSOR){
    // cur_co = co_sensor_read(DEFAULT);
    cur_co = sensor_read(DEFAULT_CO); // JJ modify 2013.11.25
    co = abs(prev_co - cur_co);
  }
  else if (profile_st_flag & CO2_SENSOR){
    // cur_co2 = co2_sensor_read(DEFAULT);
    cur_co2 = sensor_read(DEFAULT_CO2); // JJ modify 2013.11.25
    co2 = abs(co2 - cur_co2);    
  }
  else if (profile_st_flag & PIR_SENSOR){
    cur_pir = pir_sensor_read();
    // pir_sensor_on();
    // cur_pir = pir_sensor_get_data(); // JJ modify 2013.11.25
    //pir_sensor_off();
    pir = abs(pir - cur_pir);
  }
  else 
    return;

  if(temp > temp_THR){
    prev_temp = cur_temp;
    adaptive_sampling_triggered(TEMP_SENSOR);
  }else if(hum > hum_THR){
    prev_hum = cur_hum;
    adaptive_sampling_triggered(HUM_SENSOR);
  }else if(illum > illum_THR){
    prev_illum = cur_illum;
    adaptive_sampling_triggered(LIGHT_SENSOR);
  }else if(co > co_THR){
    prev_co = cur_co;
    adaptive_sampling_triggered(CO_SENSOR);
  }else if(hum > co2_THR){
    prev_co2 = cur_co2;
    adaptive_sampling_triggered(CO2_SENSOR);
  }
  /*
  else if(hum > pir_THR){
    prev_pir = cur_pir;
    adaptive_sampling_triggered(PIR_SENSOR);
  }
  */
}

BOOL checkIsMe(const IP6_ADDRESS *addr) {
  if(addr->s6_addr16[7] == node_id)
    return TRUE;
  else
    return FALSE;
}

BOOL checkIsMe16(UINT16 addr) {
  if(addr == node_id)
    return TRUE;
  else
    return FALSE;
}


IP6_ADDRESS REPLY_ACTUATOR_ADDR; // we need to get this information from the data packet.
void send_reply_actuator() {

  udp_sendto(8765, &REPLY_ACTUATOR_ADDR, 8765, &ans_pkt, sizeof(action_noti_pkt));//
  printf("Sent by timer\n");
}

void listener(UINT8 in_inf,
              const IP6_ADDRESS *src_addr,
              UINT16 src_port,
              UINT16 dst_port,
              const UINT8 *m, UINT16 len)
{
  const char *msg = (const char *) m;
  //char text_addr[NTOP_BUF_SZ];
  //UINT16 value, value_type;
  UINT16 src_id;
  struct event_help_pkt ehp;
  struct action_noti_pkt anp;

  //moved up from above, hyunhak 2013.1.8
  struct event_help_pkt* ehp_rx;
  ehp_rx = (struct event_help_pkt*)msg;

   //added hyunhak 2013.1/14
  struct event_noti_pkt* enp_rx;
  enp_rx = (struct event_noti_pkt*)msg;
    //IP6_ADDRESS ACTUATOR_ID;
/*
  printf("[%s]:%u (%u byte) - %s\n",
	      ip6_ntop(src_addr, text_addr), src_port, len, msg);
*/
  //src_id = src_addr -> s6_addr16[7];
  
  src_id = (UINT16)(src_addr->s6_addr[14] <<8) + src_addr->s6_addr[15];
  //printf("src id=%d, %d\n", src_addr->s6_addr[15], src_id);
  
  if(dst_port == 1234){
    // If I am an actuator, this is where all the data from the sensors come to.
    // Filter out the sensor ID and the value that the sensor is reporting.
    // we will use these as input values to trigger an event back to the sensor node if needed.

  }else if(dst_port == 4321){
    // this packet should be from an actuator to the sensor node indicating that an event has taken place
    // The sensor node should make a broadcast at this point looking for other nodes using port 5678 this packet should include this node's associated actuator's address

    ehp.N_ID = node_id;
    ehp.P_ID = enp_rx->P_ID; //hyunhak 2013.1.8
    ehp.Sensor_type = enp_rx->Sensor_type; //hyunhak 2013.1.8 changed from TEMP_SENSOR;
    ehp.Actuator_type = enp_rx->Actuator_type;
    ehp.Event_status = TRUE;
    ehp.A_ID = src_id; // only the normal group's actuator can send this message... //ACTUATOR_ID.s6_addr16[7]; // this is the address of the sensor's normal group's actuator node
    ehp.av = enp_rx->av; // htw modify 2013.01.11
    EVENT_SITUATION = TRUE;
    SERVER_SEND_COUNT = 0;
    KEEP_NG_ACT_FLAG = TRUE;
    PACKET_INTERVAL=4000;
    COOP_SENSOR_TYPE = ehp.Sensor_type;
    if(SEND_MSG_TIMER != 0xFF){
      user_timer_destroy(SEND_MSG_TIMER);
    }
    SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, (void*)&COOP_SENSOR_TYPE, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
    // JJ: change BCAST_ADDR to broadcast_addr
    //udp_sendto(5678, &BCAST_ADDR, 5678, &ehp, sizeof(ehp) + 1);
    udp_sendto(5678, &broadcast_addr, 5678, &ehp, sizeof(ehp));
    //delay_ms(20);
    //udp_sendto(5678, &broadcast_addr, 5678, &ehp, sizeof(ehp));
    //delay_ms(20);

    printf("#Rcv1 gather_coop_group from=%u, P_ID=%u, st=%u\n\r#Broadcast done!\n\r", enp_rx->N_ID, enp_rx->P_ID,enp_rx->Sensor_type); //hyunhak added 2013.1.8
   

  }else if(dst_port == 5678){
    // the node has received a message that a node in its reach has triggered an event. 
    // check the packet and if I am not the node in the group_actuator field, then send a packet to the group actuator (if I am an actuator)
    BOOL /*isActuator = FALSE,*/ isMe = FALSE;
    //struct event_help_pkt* ehp_rx;  //commented by hyunhak 2013.1.8
    //ehp_rx = (struct event_help_pkt*)msg;  //commented by hyunhak 2013.1.8
    isMe = checkIsMe16(ehp_rx->A_ID); // check if the associated acutator is myself.

    // check if I am an actuator (this is just a quick check using the node id -- this needs to be changed to some other algorithm later on... TODO)

    printf("#Rcv2 Bcast gather_coop_group from=%u, P_ID=%u, st=%u\n\r",ehp_rx->N_ID, ehp_rx->P_ID,ehp_rx->Sensor_type); //hyunhak added 2013.1.8

    if(!isMe){ // JK:We are going to receive this at all nodes including sensor nodes... not only actuators...
      // TODO: Do we need to store the fact that this sensor node is currently experiencing an event????????
      //ehp_rx = (struct event_help_pkt*)msg;  //commented by hyunhak 2013.1.8

      //hyunhak changed 2013.1.8
     // JJ: comment out 2013.3.13 
//IP6_ADDRESS REPLY_ACTUATOR_ADDR; // we need to get this information from the data packet.
      memcpy(&REPLY_ACTUATOR_ADDR, &actuator_addr, 16);
      REPLY_ACTUATOR_ADDR.s6_addr[14] = (UINT8)((ehp_rx -> A_ID) >>8);
      REPLY_ACTUATOR_ADDR.s6_addr[15] = (UINT8)(ehp_rx -> A_ID);
      //REPLY_ACTUATOR_ADDR.s6_addr16[7] = ehp_rx -> A_ID;

      //delay_ms(rand()%20+1);

      anp.N_ID = node_id;
      anp.A_ID = ehp_rx -> A_ID;
      anp.P_ID = ehp_rx -> P_ID;
      anp.Sensor_ID = ehp_rx -> N_ID;
      anp.Sensor_type = ehp_rx -> Sensor_type;
      anp.Event_status = TRUE;
      //anp.av = ehp_rx -> av;
      anp.av = 255;
      COOP_P_ID=enp_rx->P_ID;
      printf("#I will join to event as grpCO group to=%u, P_ID=%u, st=%u\n\r",ehp_rx->A_ID,ehp_rx->P_ID,ehp_rx->Sensor_type); //hyunhak added 2013.1.8

      if(EVENT_SITUATION)
        return;   // htw modify TODO:TW actuation profile check  

      //prev_actuator_flag = cur_actuator_flag;
      //cur_actuator_flag = TRUE;

      SERVER_SEND_COUNT = 0;
      EVENT_SITUATION = TRUE;
      PACKET_INTERVAL=4000;
      // Check what actuator type my normal group is....
      if (cur_sen_profile == 0){
        // send to a1
        printf("#I've no an original Actuator, so start to report to ACT. %u as Co-op. group\r\n", ehp_rx->A_ID);
        memcpy(&DATA_REPORT_ADDR, &REPLY_ACTUATOR_ADDR, 16);
        KEEP_NG_ACT_FLAG = FALSE;
        COOP_SENSOR_TYPE = ehp_rx ->Sensor_type;
        COOP_P_ID=ehp_rx->P_ID;
	if(SEND_MSG_TIMER != 0xFF){
	  user_timer_destroy(SEND_MSG_TIMER);
	}
	SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, (void*)&COOP_SENSOR_TYPE, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.

      }else if(ehp_rx->Actuator_type == (UINT8)my_sen_profile[cur_sen_profile-1].at){

        // send to a2 (original)
        printf("#I've an original Act.(same actuator_type), so keep reporting to ACT. %u as NO group\r\n", my_sen_profile[cur_sen_profile-1].A_ID);        
        KEEP_NG_ACT_FLAG = TRUE;
	if(SEND_MSG_TIMER != 0xFF){
	  user_timer_destroy(SEND_MSG_TIMER);
	}
        SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
        
      }else { 
        // send to a1 (new event's actuators)
        printf("#I've an original Act.(different actuator_type), so change reporting to ACT. %u as Co-op. group\r\n", ehp_rx->A_ID);
        memcpy(&DATA_REPORT_ADDR, &REPLY_ACTUATOR_ADDR, 16);
        KEEP_NG_ACT_FLAG = FALSE;
        COOP_SENSOR_TYPE = ehp_rx ->Sensor_type;
        COOP_P_ID=ehp.P_ID;
//      user_timer_create_ms((void*)send_messageto, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
	if(SEND_MSG_TIMER != 0xFF){
	  user_timer_destroy(SEND_MSG_TIMER);
	}
        SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, (void*)&COOP_SENSOR_TYPE, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
      }
     
      //////////////////////////////////////////////////////////////////
      delay_ms(rand()%20 +5);
	printf("reply addr=%d\n", ehp_rx -> A_ID);
	udp_sendto(8765, &REPLY_ACTUATOR_ADDR, 8765, &anp, sizeof(anp));
	memcpy(&ans_pkt, &anp, sizeof(anp));
	user_timer_create_ms((void*)send_reply_actuator, (void*)&anp, rand()%20 +5, USER_TIMER_ONE_SHOT);
      //////////////////////////////////////////////////////////////////
    }
  }else if(dst_port == 8765){
    // this case comes in two cases. 1) a new actuator was activated due to my own (actuator) event trigger 2) the event triggering actuator has requested that the event has stopped.
    // In any case, if A_ID is myself, then record the status of the other actuator, if not follow the status of the message
    struct action_noti_pkt* anp_rx;
    anp_rx = (struct action_noti_pkt*)msg;

    BOOL isMe = FALSE;
    isMe = checkIsMe16(anp_rx->A_ID); // check if the associated acutator is myself.

    printf("#Rcv4 Reply-gather_coop_group from=%u, A_ID=%u, P_ID=%u, st=%u, isMe=%u\n\r",anp_rx->N_ID,anp_rx->A_ID,anp_rx->P_ID,anp_rx->Sensor_type, isMe); //hyunhak added 2013.1.8

    if(isMe){
    // TODO: record the status of the other actuator and the associated eventa
    
      //hyunhak grp info fill 2013.1.11
      //mygrp_update(lookupAPIdxByAp(anp_rx->A_ID, grpAC, src_id);
      //printf("#grp AC updated Ap_id=%u, src_id=%u\n", anp_rx->A_ID, src_id);  

    }else{
      // TODO: follow the status of the message

      printf("#Co-op reporting ended..\r\n");
      EVENT_SITUATION = FALSE; // htw modify 
      trickleIsRunning = FALSE;
      if(!trickleIsRunning){
        TRICKLE_TIMER_ID = user_timer_create_ms(trickle_timer, NULL, 20, USER_TIMER_ONE_SHOT);
        trickleIsRunning = TRUE;
        printf("tid = %d\n", TRICKLE_TIMER_ID);
      }
    }
  }
}

/////////////////////////////////////////////////////////////////////////////////////
// JJ: Belows are added by juny 
// This function will be called when the button is pressed
// 
UINT8 atox(char asc) {
  UINT8 hex;
  if( asc >= '0' && asc <= '9')
	{
	        hex = asc - 0x30;
	}
	else if( asc >= 'A' && asc <= 'Z' )
	{
	        hex = asc - 0x37;
	}
	else 
	{
	        hex = asc - 0x57;
	}
  return hex;
}

// save ip6 address from string(eg., fd00:0:0:0:0:0:0:5)
// UINT16* dst_addr is a return value! (last 16-bit of IPv6 Address)
void save_ip6_addr(char* str ,UINT8 val_len, UINT16* dst_addr) {
  UINT8 head, tail, cnt,j;
  UINT8 tmp_addr[32];
  head = 0;
  tail = 0;
  cnt = 0;
  memset(tmp_addr, 0, 32);  // set all zero for init.
  
  while(tail<val_len) {
    // split by ':' for checking 1~4 letter interger
    if (str[tail] == ':') {
      tmp_addr[cnt*4+3] = atox(str[tail-1]);
      if (tail-head > 1) {
        tmp_addr[cnt*4+2] = atox(str[tail-2]);
      }
      if (tail-head >2) {
        tmp_addr[cnt*4+1] = atox(str[tail-3]);
      }
      if (tail-head >3) {
        tmp_addr[cnt*4] = atox(str[tail-4]);
      }
      cnt++;
      
      head = tail+1;
    }

    // actually the pointer cannot reached below if statements (just for error check)
    if (cnt>=8) {
      for (j=0; j<8; j++) {
        dst_addr[j] = (UINT16)(tmp_addr[j*4] << 12) + (UINT16)(tmp_addr[j*4+1] << 8) + (UINT16)(tmp_addr[j*4+2] << 4) + (UINT16)(tmp_addr[j*4+3]);
      }
      return ;
    }
    tail++;
  }
  tmp_addr[cnt*4+3] = atox(str[tail-1]);
  if (tail-head > 1) {
    tmp_addr[cnt*4+2] = atox(str[tail-2]);
  }
  if (tail-head >2) {
    tmp_addr[cnt*4+1] = atox(str[tail-3]);
  }
  if (tail-head >3) {
    tmp_addr[cnt*4] = atox(str[tail-4]);
  }
  for (j=0; j<8; j++) {
    // save dst_addr (16-bit last IPv6 address)
    dst_addr[j] = (UINT16)(tmp_addr[j*4] << 12) + (UINT16)(tmp_addr[j*4+1] << 8) + (UINT16)(tmp_addr[j*4+2] << 4) + (UINT16)(tmp_addr[j*4+3]);
  }
  return ;
}

// This function is not coded yet.
// TODO: for testing, check the button 
void button_cb(UINT8 id)
{

  IP6_ADDRESS lserver_addr = {{
      0xaa, 0xaa, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11 }};
            

    char msg[100];
    printf("\n\rButton (ID: %u) is pressed!\n\r", id);
    switch(id) {
      case 1:
      // Event emul.
      sprintf(msg, "Button");
      printf("%s\n", msg);
      udp_sendto(wsan_port, &lserver_addr, wsan_port, msg, strlen(msg) + 1);

      led_toggle(3);
      
        break;
      case 2:
      
        break;
      case 3:
	// printf("\n\rTemperature(celsius*10): %u \n\rHumidity(RH_linear*10 %% ): %u", temperature_sensor_read(DEFAULT), humidity_sensor_read(DEFAULT) );
	printf("\n\rTemperature(celsius*10): %u \n\rHumidity(RH_linear*10 %% ): %u", sensor_read(DEFAULT_TEMP), sensor_read(DEFAULT_HUM) ); // JJ 2013.11.25
          delay_ms(500);
        break;
      case 4:

         my_sen_profile[cur_sen_profile].P_ID=1;
         if (cur_sen_profile > 1)
           my_sen_profile[cur_sen_profile].A_ID=20;
         else 
           my_sen_profile[cur_sen_profile].A_ID=22;

         if (cur_sen_profile == 2)
           my_sen_profile[cur_sen_profile].st=192;
         else if (cur_sen_profile == 3)
           my_sen_profile[cur_sen_profile].st=224;
         else if (cur_sen_profile == 4)
           my_sen_profile[cur_sen_profile].st=240;

         my_sen_profile[cur_sen_profile].num_sensor=2;
//         my_sen_profile[cur_sen_profile].diff1[0]=20;
//         my_sen_profile[cur_sen_profile].diff2[0]=20;
          my_sen_profile[cur_sen_profile].diff[0] = 1; // JJ: add
         my_sen_profile[cur_sen_profile].tmin[0]=1000;
         my_sen_profile[cur_sen_profile].tmax[0]=5000;
//         my_sen_profile[cur_sen_profile].diff1[1]=20;
//         my_sen_profile[cur_sen_profile].diff2[1]=20;
        my_sen_profile[cur_sen_profile].diff[1] = 1; // JJ: add
         my_sen_profile[cur_sen_profile].tmin[1]=1000;
         my_sen_profile[cur_sen_profile].tmax[1]=5000;

         printf("\n\r# of profile %u\n\r",cur_sen_profile);
         printf("\n\rdst=%u num=%u st=%u\n\r", my_sen_profile[cur_sen_profile].A_ID, my_sen_profile[cur_sen_profile].num_sensor, my_sen_profile[cur_sen_profile].st);
         if(cur_sen_profile > MAX_SENSOR_PROFILE){
           printf("This profile is more than MAX_SENSOR_PROFILE profileidx=0 " );
         }
         else{
           cur_sen_profile++;
         }

  	if (checkIsRedundant())
	    return;

        break;
      default:
        break;
    }
}

// for check ACK packet from the server
BOOL isRcvAck(void) {
  if (report_state ==1 )
    return TRUE;
  else
    return FALSE;
}

void report_join(void *args)
{

  IP6_ADDRESS lserver_addr = {{
      0xaa, 0xaa, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11 }};
            

    char msg[100];
    ERROR_T rsp_error;
    //IP6_ADDRESS temp_root_addr;
    char text_addr[NTOP_BUF_SZ];
    if (isRcvAck()) {
      //report_state=0;
      return;
    }
    else {
      if (ip6_is_attached(lowpan_inf)) {
        base_addr.s6_addr[14] = (UINT8)(node_id >>8);
        base_addr.s6_addr[15] = (UINT8)node_id;
        sprintf(msg, "POST /sen addr=%s,type=0,st=%u,sem=%s", ip6_ntop(&base_addr, text_addr), my_sensors.sensor_type, my_location.sem);
      //printf("data=%s, len = %u\n", msg, strlen(msg));        
        rsp_error = udp_sendto(wsan_port, &lserver_addr, wsan_port, msg, strlen(msg) + 1);
        if (rsp_error != ERROR_SUCCESS) {
          rsp_error =udp_sendto(wsan_port, &lserver_addr, wsan_port, msg, strlen(msg) + 1);
          printf("err = %u\n", rsp_error);
        }
        else {
          printf("Send RPT Join\n");
        }
        led_toggle(2);
      }
      report_state = 0;
      report_timer_id = user_timer_create_sec(report_join, NULL, 1, USER_TIMER_ONE_SHOT);
      
    }
}

// The actuator profile have to be texted with fixed series and order of profile value.
// JJ: Exceptional case is not coded yet.
UINT8 save_profile(char *profile_name, char *profile_value, UINT8 val_len, UINT8 id, UINT8 code) {
  UINT8 i = 0,j = 0;
  UINT16 dst_addr[8], k = 0; 
  if (code == SET_SP) {
    if (!memcmp(profile_name, aid_name,2)) {
	for (UINT8 k=0; k <cur_sen_profile; k++) {
		if(my_sen_profile[k].P_ID == id) {
			printf("Same Id with %u\n", id);
			return 255;
		}
	}
      my_sen_profile[cur_sen_profile].P_ID = id;    
      cur_sen_profile++;
      return i;
    }
    for (i=0; i<cur_sen_profile; i++) {
      // only save when the id is same
      if (my_sen_profile[i].P_ID == id) {
        
        if (!memcmp(profile_name, dst_name, 3)) {
          save_ip6_addr(profile_value, val_len, dst_addr);
          my_sen_profile[i].A_ID = dst_addr[7];
          // configure actuator_addr here!!
          actuator_addr.s6_addr[14] = (UINT8)(dst_addr[7] >> 8);
          actuator_addr.s6_addr[15] = (UINT8)dst_addr[7];
          //printf("dst=%u\n", my_sen_profile[i].dst);
          return i;
        }
        // JJ: save actuator type added
        else if (!memcmp(profile_name, at_name, 2)) {
          my_sen_profile[i].at = (UINT8)atoi(profile_value);
          //printf("save at= %u\n", my_sen_profile[i].at);
          return i;
        }
        else if (!memcmp(profile_name, st_name, 2)) {
          my_sen_profile[i].st = (UINT8)atoi(profile_value);
          //printf("save st= %u\n", my_sen_profile[i].st);
          return i;
        }
        else if (!memcmp(profile_name, diff_name, 4)) {
          /*
          // JJ: find '.'
          for (j=0; j<val_len; j++) {
            if (profile_value[j] == '.') {
              my_sen_profile[i].diff1[my_sen_profile[i].num_sensor] = (UINT8)atoi(&profile_value[0]);
              my_sen_profile[i].diff2[my_sen_profile[i].num_sensor] = (UINT8)atoi(&profile_value[j+1]);
              break;
            }
          }
          */  // JJ: changed (UINT8 diff1 & diff2 => UINT16 diff)
          
          my_sen_profile[i].diff[my_sen_profile[i].num_sensor] = (UINT16)atoi(profile_value);         
          
          //printf("save diff= %u.%u\n", my_sen_profile[i].diff1[my_sen_profile[i].num_sensor], my_sen_profile[i].diff2[my_sen_profile[i].num_sensor]);
          return i;
        }
        else if (!memcmp(profile_name, tmin_name, 4)) {
          my_sen_profile[i].tmin[my_sen_profile[i].num_sensor] = (UINT16)atoi(profile_value);
          //printf("save tmin= %u\n", my_sen_profile[i].tmin[my_sen_profile[i].num_sensor]);
          return i;
        }
        else if (!memcmp(profile_name, tmax_name, 4)) {
          my_sen_profile[i].tmax[my_sen_profile[i].num_sensor] = (UINT16)atoi(profile_value);
          //printf("save tmax= %u\n", my_sen_profile[i].tmax[my_sen_profile[i].num_sensor]);
          my_sen_profile[i].num_sensor++;
          // JJ: set up the received diff value 2013.1.8
          for (j=0; j<my_sen_profile[i].num_sensor; j++) {
            UINT8 bit=0x80;
            for (k=0; k< 8; k++) {
              if (my_sen_profile[i].st & (UINT8)(bit >>k)) {
                MAX_PACKET_INTERVAL = my_sen_profile[i].tmax[j]*1000;
                MINIMUM_PACKET_INTERVAL = my_sen_profile[i].tmin[j] *1000;
                // JJ: add for test. To be deleted
                if (MAX_PACKET_INTERVAL > 30000)
                  MAX_PACKET_INTERVAL = 30000;
                if (k==0) temp_THR = my_sen_profile[i].diff[j];
                else if (k==1) hum_THR = my_sen_profile[i].diff[j];
                else if (k==2) illum_THR = my_sen_profile[i].diff[j];
                else if (k==3) co_THR = my_sen_profile[i].diff[j];
                else if (k==4) co2_THR = my_sen_profile[i].diff[j];
                else if (k==5) pir_THR = my_sen_profile[i].diff[j];
                break;  
              }
            }
          }
  	if (checkIsRedundant())
	    return i;

          return i;
        }
      }
    }
  } // SET SP
  
  // GET SP
  else if (code == GET_SP) {
    if(!memcmp(profile_name, aid_name,2)) {
      // add aid
      /*
      my_sen_profile[cur_sen_profile].aid = id;    
      cur_sen_profile++;
      return cur_sen_profile;
      */
      // JJ: revise it (save sen_profile only SET_SP)
      return 0;
    }
    else {
      // discard
      return 0;
    }
  }
  return 0;
}
// JJ: Exceptional case is not coded yet.

// JJ: text parser for actuator profile
// code: unique process code (see in wsan-profile-1.h)
UINT8 process_data_parser(char* msg, UINT8 total_len, UINT8 code) {
  UINT8 tail, head, return_type, id = 0;
  char profile_name[5];
  char profile_value[50];
  head = 0;
  tail = 0;
  return_type = 255;
  while(tail < total_len) {
    if( msg[tail] == '=') {
      memset(profile_name, 0, 5); // clear profile_name;
      memcpy(profile_name, &msg[head],tail-head);
      tail++; 
      head = tail;
      while(1) {
        if ( msg[tail] == ',') {
          //memset(profile_value, NULL, 50);   // clear profile_value;
          memset(profile_value, 0, 50);   // clear profile_value;
          memcpy(profile_value, &msg[head], tail-head);
          // process
          if (code == SET_AP) {
            if (!memcmp(profile_name, "id", 2)) {
              id = (UINT8)atoi(profile_value);
            }
            return_type = save_profile(profile_name, profile_value, tail-head, id, code); 
            //printf("return_type= %u\n", return_type);
            if (return_type == 255) {
              printf("Error, no such type of data\n");
              return return_type;
            }
          }
          else {
            if (!memcmp(profile_name, "aid",3)) {
              id = atoi(profile_value);
              //printf("aid = %u\n", id);
            }
            return_type = save_profile(profile_name, profile_value, tail-head, id, code);
            if (return_type == 255) {
              printf("Error, no such type of data\n");
              return return_type;
            }
          }
          tail++;
          head = tail;
          break;
        }
        tail++;
        if (tail >= total_len) {
          //memset(profile_value, NULL, 50);   // clear profile_value;
          memset(profile_value, 0, 50);   // clear profile_value;
          memcpy(profile_value, &msg[head], tail-head);
          if (code == GET_SP) {
            if (!memcmp(profile_name, "aid", 3)){
              id = atoi(profile_value);
            }
          }
          return_type = save_profile(profile_name, profile_value, tail-head, id, code);
          break;
        }
      }
    }
    tail++;    
  }
  return return_type;
}

// The function will be called when a node received a wsan message (port check)
// listener_profile function 
void listener_profile(UINT8 in_inf,
                      const IP6_ADDRESS *src_addr,
                      UINT16 src_port,
                      UINT16 dst_port,
                      const UINT8 *m, UINT16 len)
{
  const char *msg = (const char *) m;
    UINT8 cur, ack, parser_return;    
    char text_addr[NTOP_BUF_SZ];
    char msg_get[50];
    cur = 0;
    /*
    printf("[%s]:%u (%u byte) - %s\n",
            ip6_ntop(src_addr, text_addr), src_port, len, msg);
    */

    UINT8 i, id=0;

    if (len > MAX_UDP_LENGTH) {
      printf("UDP packet overflow!\n");
      return 0;
    }
    memset(udp_buf, 0, MAX_UDP_LENGTH);
    memcpy(udp_buf, msg, len);  // copy packet!!
    
    switch (udp_buf[0])
    {
      case 'P': // POST
      cur = 6;  // skip "POST /"
        while(1) {
          if (cur >= len) {
            // printf("end\n");
            break;
          }
          if (!memcmp(&udp_buf[cur], "sp",2)) {
            // Need data saving            
            cur += 2;
            // JJ: only process the msg such as 'POST /sp {data} here.
            if (udp_buf[cur] == ' ') {
              cur++;
#ifdef DEBUG_PROFILE 
              printf("sp: %s\n", &udp_buf[cur]);
#endif // DEBUG_PROFILE
              printf("RCV SP\n");
              parser_return = process_data_parser((char*)&udp_buf[cur], len-cur, SET_SP);
              if (parser_return !=255) {
#ifdef DEBUG_PROFILE 
                printf("START!!\n");
#endif // DEBUG_PROFILE 
		if(SEND_MSG_TIMER != 0xFF){
		  user_timer_destroy(SEND_MSG_TIMER);
		}
                SEND_MSG_TIMER = user_timer_create_ms((void*)send_messageto, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT);
              }
              else {
                printf("Error, process_data_parser cannot work\n");
                return;
              }
              //sprintf(msg_get, "ACK /sp");
              //udp_sendto(wsan_port, src_addr, wsan_port, msg_get, strlen(msg_get) + 1);
            }
            else if (udp_buf[cur] == '/') {
              // process specific sensor profile here 
              // Not configured yet
            }
          }
	  // JJ: clear all profiles!
	  else if (!memcmp(&udp_buf[cur], "clr", 3)) {
	    if (cur_sen_profile !=0) {
	      // send broadcast again!
              printf("CLR RX\n");
	      sprintf(msg_get, "POST /clr");
	      udp_sendto(wsan_port, &broadcast_addr, wsan_port, msg_get, strlen(msg_get)+1);

	      // clear my profiles
	      reset_profile();
	    }
	      return;
	  }
          cur++;
        }
        break;
        
      case 'R': // RESPONSE

        // response of GET /sp
        //printf("%s\n", &msg[cur]);
        cur =4;  // skip RSP
        ack = (UINT8)atoi(&udp_buf[cur]);
        if (ack == COAP_RESPONSE_205_CONTENT) {
          cur +=4;  // skip '{ack}  /'
          // TODO: another rsp of each cases
        }
        else {
          // TODO: if response is not a content, then process here
          // discard the other ack type (errors)
        }
        break;
        
      case 'G': // GET, but not used for stand alone emul.
      // Rsp to Actuator with /sp
        cur =5; // skip 'GET /'
        // TODO: Need to code here for another GET operation
        // Device profile 
        if (!memcmp(&udp_buf[cur], dev_name,3)) {
          cur +=4;
          if (!memcmp(&udp_buf[cur], mfg_name,3)) {
            sprintf(msg_get, "RSP %u /%s/%s %s",COAP_RESPONSE_205_CONTENT, dev_name, mfg_name, my_device.mfg);              
          }
          else if (!memcmp(&udp_buf[cur], mdl_name,3)) {
            sprintf(msg_get, "RSP %u /%s/%s %s",COAP_RESPONSE_205_CONTENT, dev_name, mdl_name, my_device.mdl);
          }
          else if (!memcmp(&udp_buf[cur], type_name,4)) {
            sprintf(msg_get, "RSP %u /%s/%s %u",COAP_RESPONSE_205_CONTENT, dev_name, type_name, my_device.type);
          }
          // error return 
          else {
            sprintf(msg_get, "RSP %u",COAP_RESPONSE_404_NOT_FOUND);
          }
#ifdef DEBUG_PROFILE 
          printf("%s, %u\n", msg_get, strlen(msg_get));
#endif // DEBUG_PROFILE
          udp_sendto(wsan_port, src_addr, wsan_port, msg_get, strlen(msg_get) + 1);
          printf("Send RSP of dev\n");
          break;
        }
        // Location profile
        else if (!memcmp(&udp_buf[cur], loc_name,3)) {
          cur +=4;
          if (!memcmp(&udp_buf[cur], x_name,1)) {
            sprintf(msg_get, "RSP %u /%s/%s %u.%02u",COAP_RESPONSE_205_CONTENT, loc_name, x_name, my_location.x_int, my_location.x_point);              
          }
          else if (!memcmp(&udp_buf[cur], y_name,1)) {
            sprintf(msg_get, "RSP %u /%s/%s %u.%02u",COAP_RESPONSE_205_CONTENT, loc_name, y_name, my_location.y_int, my_location.y_point);
          }
          else if (!memcmp(&udp_buf[cur], sem_name,3)) {
            sprintf(msg_get, "RSP %u /%s/%s %s",COAP_RESPONSE_205_CONTENT, loc_name, sem_name, my_location.sem);
          }
          // error return 
          else {
            sprintf(msg_get, "RSP %u",COAP_RESPONSE_404_NOT_FOUND);
          }
#ifdef DEBUG_PROFILE 
          printf("%s, %u\n", msg_get, strlen(msg_get));
#endif // DEBUG_PROFILE 
          udp_sendto(wsan_port, src_addr, wsan_port, msg_get, strlen(msg_get) + 1);
          printf("Send RSP of loc\n");
          break;
        }
        else if (!memcmp(&udp_buf[cur], saddr_name,5)) {
          //cur +=6;
          sprintf(msg_get, "RSP %u /%s %s",COAP_RESPONSE_205_CONTENT, saddr_name, ip6_ntop(&server_addr, text_addr));              
#ifdef DEBUG_PROFILE               
          printf("%s, %u\n", msg_get, strlen(msg_get));
#endif // DEBUG_PROFILE
          udp_sendto(wsan_port, src_addr, wsan_port, msg_get, strlen(msg_get) + 1);
          printf("Send RSP of server addr\n");
          break;
        }

        else if (!memcmp(&udp_buf[cur], sen_name,3)) {
          cur +=4;
          // TODO: check my_sensors.sensor_type
          if (!memcmp(&udp_buf[cur], temp_name,4)) {
            if (my_sensors.sensor_type & 0x80) {
              //sprintf(msg_get, "RSP %u /%s/%s %u.%02u",COAP_RESPONSE_205_CONTENT, sen_name, temp_name, my_sensors.temp_int, my_sensors.temp_point);              
              // my_sensors.temp = temperature_sensor_read(DEFAULT);
	      my_sensors.temp = sensor_read(DEFAULT_TEMP); // JJ: 2013.11.25
              sprintf(msg_get, "RSP %u /%s/%s %d",COAP_RESPONSE_205_CONTENT, sen_name, temp_name, my_sensors.temp);  
            }            
            else
              sprintf(msg_get, "RSP %u",COAP_RESPONSE_400_BAD_REQUEST);
          }
          else if (!memcmp(&udp_buf[cur], hum_name,3)) {
            if (my_sensors.sensor_type & 0x40) {
              // my_sensors.hum = humidity_sensor_read(DEFAULT);
	      my_sensors.hum = sensor_read(DEFAULT_HUM); // JJ: 2013.11.25
              sprintf(msg_get, "RSP %u /%s/%s %u",COAP_RESPONSE_205_CONTENT, sen_name, hum_name, my_sensors.hum);
            }
            else
              sprintf(msg_get, "RSP %u",COAP_RESPONSE_400_BAD_REQUEST);
          }
          else if (!memcmp(&udp_buf[cur], light_name,5)) {
            if (my_sensors.sensor_type & 0x20) {
              // my_sensors.light = light_sensor_read(DEFAULT);
	      my_sensors.light = sensor_read(DEFAULT_LIGHT); // JJ: 2013.11.25
              sprintf(msg_get, "RSP %u /%s/%s %u",COAP_RESPONSE_205_CONTENT, sen_name, light_name, my_sensors.light);
            }
            else
              sprintf(msg_get, "RSP %u",COAP_RESPONSE_400_BAD_REQUEST);
          }
          // JJ: changed the order (co2 first)
          else if (!memcmp(&udp_buf[cur], co2_name,3)) {
            if (my_sensors.sensor_type & 0x08) {
              // my_sensors.co2 = co2_sensor_read(DEFAULT);
	      my_sensors.co2 = sensor_read(DEFAULT_CO2); // JJ: 2013.11.25
              sprintf(msg_get, "RSP %u /%s/%s %u",COAP_RESPONSE_205_CONTENT, sen_name, co2_name, my_sensors.co2);
            }
            else
              sprintf(msg_get, "RSP %u",COAP_RESPONSE_400_BAD_REQUEST);
          }  
          else if (!memcmp(&udp_buf[cur], co_name,2)) {
            if (my_sensors.sensor_type & 0x10) {
              // my_sensors.co = co_sensor_read(DEFAULT);
	      my_sensors.co = sensor_read(DEFAULT_CO); // JJ: 2013.11.25
              sprintf(msg_get, "RSP %u /%s/%s %u",COAP_RESPONSE_205_CONTENT, sen_name, co_name, my_sensors.co);
            }
            else
              sprintf(msg_get, "RSP %u",COAP_RESPONSE_400_BAD_REQUEST);
          }                     
          else if (!memcmp(&udp_buf[cur], pir_name,3)) {
            if (my_sensors.sensor_type & 0x40) {
              //my_sensors.pir = nos_pir_sensor_get_value();
              sprintf(msg_get, "RSP %u /%s/%s %u",COAP_RESPONSE_205_CONTENT, sen_name, pir_name, my_sensors.pir);
            }
            else
              sprintf(msg_get, "RSP %u",COAP_RESPONSE_400_BAD_REQUEST);
          }
          // error return 
          else {
            sprintf(msg_get, "RSP %u",COAP_RESPONSE_404_NOT_FOUND);

          }
#ifdef DEBUG_PROFILE
          printf("%s, %u\n", msg_get, strlen(msg_get));
#endif // DEBUG_PROFILE
          udp_sendto(wsan_port, src_addr, wsan_port, msg_get, strlen(msg_get) + 1);
          printf("Send RSP of sensors\n");
          break;
        }
        else if (!memcmp(&udp_buf[cur], "sp",2)) {
          // Need data saving            
          cur += 2;
          // JJ: only process the msg such as 'GET /sp {data} here.
          if (udp_buf[cur] == ' ') {
            cur++;
            if (!memcmp(&udp_buf[cur], "aid",3)) {
              // find aid in my_sen_profile
              id = atoi(&udp_buf[cur+4]);
              for (i=0; i< cur_sen_profile; i++) {
                // only send RSP to the actuator which has same aid
                // if this sensor cannot receive SENSOR PROFILE yet, send RSP again
                if ((my_sen_profile[i].P_ID == id) && (my_sen_profile[i].num_sensor >0)) {
		              // Same aid, discard it
#ifdef DEBUG_PROFILE
                  printf("check out\n");
#endif // DEBUG_PROFILE
                  return;
                }
              }
              ////////////////////////////////////////////////
              // JJ: emulation, 
              // if aid ==1 => only sensor who has a node id between 11~20
              // if aid ==2 => only sensor who has a node id between 21~30
              if (id == 1) {
                if (node_id >20) {
#ifdef DEBUG_PROFILE
                  printf("aid =%d, node_id =%d\n", id, node_id);
#endif // DEBUG_PROFILE
                  return;
                }
              }
              else if (id ==2) {
                if (node_id <21 || node_id >30) {
#ifdef DEBUG_PROFILE
                  printf("aid =%d, node_id=%d\n", id, node_id);
#endif // DEBUG_PROFILE
                  //break;
		return;
                }
              }
            }
            parser_return = process_data_parser((char *)&udp_buf[cur], len-cur, GET_SP);
            if (parser_return == 255) {
              printf("Error, process_data_parser cannot work\n");
              return;
            }
              
            // response of GET /sp here
            base_addr.s6_addr[14] = (UINT8)(node_id >>8);
            base_addr.s6_addr[15] = (UINT8)node_id;
            sprintf(msg_get, "RSP %u /sp aid=%u,src=%s,st=%u", COAP_RESPONSE_205_CONTENT, id, ip6_ntop(&base_addr, text_addr),my_sensors.sensor_type);
#ifdef DEBUG_PROFILE
            printf("%s, %u\n", msg_get, strlen(msg_get));
#endif // DEBUG_PROFILE
            udp_sendto(wsan_port, src_addr, wsan_port, msg_get, strlen(msg_get) + 1);
            printf("Send RSP to Act.\n");
            break;
          
          }
        }
          // error return 
        else {
            //sprintf(msg_get, "RSP %u",COAP_RESPONSE_404_NOT_FOUND);
            //printf("error 8\n");
            //udp_sendto(wsan_port, src_addr, wsan_port, msg_get, strlen(msg_get) + 1);
        }

        break;
        
      case 'A' :  // receive the ACK packet
#ifdef DEBUG_PROFILE
        printf("%s\n", &udp_buf[cur]);
#endif // DEBUG_PROFILE
        cur = 4;
        if (udp_buf[cur] == 'R') {  // check ACK RPT
          // change state of report here
	  printf("Report State Changed...\n");
          report_state = 1;
        }
        break;
      default:
        break;
    }
}

// JJ: added 130502
void send_done(BOOL success,
               UINT16 seq,
               UINT16 src_port,
               const IP6_ADDRESS *dst_addr,
               UINT16 dst_port,
               const void *msg,
               UINT16 len)
{
	UINT16 i;
    if (success)
    {
        printf("UDP send success:");
	
    }
    else
    {
	
        printf("UDP send fail(%d):", udp_sendto(src_port, dst_addr, dst_port, msg, len));
    }
    for (i = 0; i < len; i++)
      {
	printf("%c", ((const char *) msg)[i]);
      }
    printf("\n");
}

void ip6_state_changed(IP6_NODE_STATE state)
{
    printf("State changed to %d\n", state);
    if (ip6_is_attached(lowpan_inf))
    {
        
        printf("RPL network connection established.\n");

        rpl_get_root_address(lowpan_inf, &server_addr);
        led_on(1);
	/*
        if (send_timer_id < 0)
        {
            send_timer_id = user_timer_create_sec(rpl_status_report, NULL, 50,
                                                  USER_TIMER_PERIODIC);
            printf("send_timer_id:%d\n", send_timer_id);
        }
	*/
    }
}

int main(void)
{
    //COAP_RESOURCE *r, *sensor;

    nos_init();
    led_on(0);
    //node_id = NODEID;
    init_profile();
    uart_puts(STDIO, "\n\r*** Nano OS -Sensor mode ***\n\r");

    // Initialize the LoWPAN interface.
    node_ext_id[6] = node_id >> 8;
    node_ext_id[7] = node_id & 0xff;
    lowpan_inf = lowpan_init(0, 1, 0xffff, node_ext_id);
 

    // JJ: set channel and modulation
    rf212_set_mode(0, RF212_OQPSK_250KBPS_SIN);
    rf212_set_channel(0, 1);

    ip6_set_state_notifier(lowpan_inf, ip6_state_changed);
    rpl_init(lowpan_inf);
    ip6_init(0);
    //UINT8 aa=sqrt(4);

    /* JK: This Part of the code will initiate the CoAP operations and
       also presents some sample sensor resources */
    //coap_init(COAP_DEFAULT_PORT_NUMBER);

    udp_set_listen(wsan_port, listener_profile);
    button_set_callback(button_cb); 

    // Initial report    
    report_timer_id = user_timer_create_sec(report_join, NULL, 2, USER_TIMER_ONE_SHOT);
    //////////////////////////////////////////////////////////////////////
    // JK
    // TODO: Need to figure out to start (after initial profile configuration)
    //user_timer_create_ms(send_messageto, NULL, PACKET_INTERVAL, USER_TIMER_ONE_SHOT); // JK: Enabling this will force periodic reports.
    // user_timer_create_ms(check_sensor, NULL, 2000, USER_TIMER_PERIODIC);
    udp_set_listen(1234, listener);
    udp_set_listen(4321, listener);
    udp_set_listen(5678, listener);
    udp_set_listen(8765, listener);
    //////////////////////////////////////////////////////////////////////
    sched_start();
    led_on(2);
    return 0;
}
