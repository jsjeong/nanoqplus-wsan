
// UDP PORT for WSAN
#define wsan_port 1235

#define MAX_STRING_LENGTH 30  // string to define

#define MAX_SENSOR_PROFILE 5  // maximum number of sensor profiles


#define MAX_UDP_LENGTH  200 // maximum UDP packet length


/////////////////////////////////////////////////
// JJ: New msg format for 2013 WSAN

#define MAX_NUM_SENSOR  9  // maximum number of kinds of sensors (e.g., temp, hum, light, co, co2, pir)
#define MAX_NUM_SN 10
#define MAX_NUM_ACT 10
#define MAX_NUM_SCENE 13

enum{         // htw modify 2012.12.21
  DOPPLER_SENSOR = 0x0100,
  TEMP_SENSOR = 0x0080,
  HUM_SENSOR = 0x0040,
  LIGHT_SENSOR = 0x0020,
  CO_SENSOR = 0x0010,
  CO2_SENSOR = 0x0008,
  PIR_SENSOR = 0x0004,
  MIC_SENSOR = 0x0002,
  ACC_SENSOR = 0x0001
};

typedef struct REPORT_JOIN_PKT {
  IP6_ADDRESS did;
  UINT8 dType; // IPv6 addr
  char locL[10]; // semantic location
  UINT16 locPx; // location x
  UINT16 locPy; // location y
  UINT8 aType; // actuation type (sensor = 0)
  UINT16 sType; // sensor type (bitmap)
} REPORT_JOIN_PKT;

typedef struct SCENE {
  UINT8 sceneID;
  UINT16 condition; // bitmap (0b10000: WB, 0b01000: proj., 0b0110: MIC, 0b0001: PIR)
} SCENE;

typedef struct MASTER_ACT_PROFILE {
  UINT8 msgID;
  UINT8 num_scene;
  UINT8 num_act;
  UINT16 act_addr[MAX_NUM_ACT];
  UINT8 num_sen;
  UINT16 sen_addr[MAX_NUM_SN];
  SCENE scene[MAX_NUM_SCENE];
  UINT8 groupID;
} master_act_profile;

typedef struct ACT_PROFILE {
  UINT8 msgID;
  UINT8 num_scene;
  UINT8 sceneID[MAX_NUM_SCENE];
  UINT8 status[MAX_NUM_SCENE]; // dimming value;
} ACT_PROFILE;

typedef struct SCENE_INFO_PKT {
  UINT8 msgID;
  UINT8 sceneID;
} SCENE_INFO_PKT;

typedef struct SENSOR_PROFILE_PKT {
  UINT8 msgID;
  UINT16 act_addr;
  UINT16 sType; // bitmap
  UINT16 mode[MAX_NUM_SENSOR];
} SENSOR_PROFILE_PKT;

typedef struct SCENE_RPT_PKT {
  UINT8 msgID;
  UINT8 sceneID;
} SCENE_RPT_PKT;

typedef struct ACT_STATUS_RPT_PKT {
  UINT8 msgID;
  UINT8 scene_status;
  UINT32 timestamp;
} ACT_STATUS_RPT_PKT;

// ACT
typedef struct ACTUATION_INFO_RPT_PKT {
  UINT8 msgID;
  UINT8 sceneID;
  UINT8 status;
} ACTUATION_INFO_RPT_PKT; // to MA

typedef struct SENSOR_INFO_RPT_PKT {
  UINT8 msgID;
  UINT16 sType;
  UINT16 condition;
  UINT32 timestamp;
  UINT32 reserved;
} SENSOR_INFO_RPT_PKT;

typedef struct SENSING_RESP_PKT {
  UINT8 msgID;
  UINT16 sType;
  UINT16 sensingValues[MAX_NUM_SENSOR];
  UINT32 timestamp;
} SENSING_RESP_PKT;

/*
typedef struct LOGICAL_VALUE {
  UINT8 logicalValue; // lvalue (NOT=0, AND=1, OR=2, None=3 (TBC))
  UINT8 caType; // sensor type
  UINT8 cType; // condition type (between=0, less than=1, greater than=2, outer=3 (TBC))
  UINT16 cValue1;
  UINT16 cValue2;
  UINT16 mode; // ??
} logical_value;

typedef struct NOMRAL_CONTROL_VALUE {
  UINT8 numCtrlDev; // number of control device
  UINT16 act_addr[MAX_NUM_CTRL_DEV]; // actuation address
  UINT16 control[MAX_NUM_CTRL_DEV]; // on/off or dimmer
} normal_control_value;

typedef struct ACTUATION_PROFILE {
  UINT8 aID; // actuation profile ID
  UINT8 defaultValue;
  normal_control_value normalCtrlValue;
  UINT8 num_lvalue; // number of lValue
  logical_value lValue[MAX_LVALUE];
} actuation_profile; // from the server to master actuator

typedef struct SENSOR_DISCOVERY_PKT {
  // UINT8 aID;
  UINT16 sType; // sensor type (bitmap)
  char locL[10]; // semantic location
  UINT16 locPx; 
  UINT16 locPy;
  UINT16 round; // (x, y) radius
} sensor_discovery_pkt; // none

typedef struct SENSOR_PROFILE {
  UINT16 sType; // sensor type (bitmap)
  UINT8 mode[MAX_NUM_SENSOR]; // 0: periodic reporting, 1: event triggered, 2: CM, 3: ADPS
  UINT16 diff[MAX_NUM_SENSOR];
} sensor_profile;

typedef struct RCV_SENSOR_PROFILE {
  UINT16 act_addr;
  UINT8 num_sn;
  UINT16 sen_addr[MAX_NUM_SN];
  sensor_profile sProfile[MAX_NUM_SN];
} rcv_sensor_profile; // master actuator only (for receiving sensor profile from the server)

typedef struct DEPLOY_SENSOR_PROFILE_PKT {
  UINT16 act_addr;
  sensor_profile sProfile;  
} deploy_sensor_profile_pkt; // from master actuator to sensor

typedef struct SENSOR_RPT_PKT {
  UINT16 sType;
  UINT16 sValue_over1[MAX_NUM_SENSOR];
  UINT16 sValue_under1[MAX_NUM_SENSOR];
  UINT32 timeStamp[MAX_NUM_SENSOR];
  UINT16 weightingFactor[MAX_NUM_SENSOR];
  UINT16 reserved[MAX_NUM_SENSOR];
} sensor_rpt_pkt;

typedef struct RPT_ACTUATION_PKT {
  UINT8 aID;
  UINT8 status;
} rpt_actuation_pkt;

typedef struct RCV_CTRL {
  UINT8 mode;
  UINT8 aID;
  UINT16 interval;
} rcv_ctrl;
*/

//////////////////////////////////////////////////////



enum process_code {
  SET_AP = 1,   // for ACTUATOR, set-up actuator profile
  RSP_SP = 2,   // for ACTUATOR, receiving sensor address, type
  GET_SP = 3,   // for SENSOR, receiving GET /sp
  SET_SP = 4,   // for SENSOR, set-up sensor profile
};

enum group_type {
  grpNO = 1,   
  grpEV = 2,  
  grpAC = 3,   
  grpCO = 4,   
};

// from JK
enum{         // htw modify 2012.12.21
  TEMP_SENSOR = 0x80,
  HUM_SENSOR = 0x40,
  LIGHT_SENSOR = 0x20,
  CO_SENSOR = 0x10,
  CO2_SENSOR = 0x08,
  PIR_SENSOR = 0x04
  
};

// JJ: Device names
const char dev_name[] = "dev";
const char mfg_name[] = "mfg";
const char mdl_name[] = "mdl";
//const char ser_name[] = "ser";
const char type_name[] = "type";
//const char bat_name[] = "bat";

// location name
const char loc_name[] = "loc";
const char x_name[] = "x";
const char y_name[] = "y";
const char sem_name[] = "sem";

const char saddr_name[] = "saddr";

// actuator profile name
const char ap_name[] = "ap";
const char id_name[] = "id";
const char dv_name[] = "dv";
const char av_name[] = "av";
const char lv_name[] = "lv";
const char st_name[] = "st";
const char cv1_name[] = "cv1";
const char cv2_name[] = "cv2";
const char tr_name[] = "tr";

// sensor profile name
const char sp_name[] = "sp";
const char dst_name[] = "dst";
const char at_name[] = "at";
const char aid_name[] = "aid";
const char src_name[] = "src";
const char diff_name[] = "diff";
const char tmin_name[] = "tmin";
const char tmax_name[] = "tmax";

// generic sensor name
const char sen_name[] = "sen";
const char temp_name[] = "temp";
const char hum_name[] = "hum";
const char co_name[] = "co";
const char co2_name[] = "co2";
const char light_name[] = "light";
const char pir_name[] = "pir";

// TODO: need to re-configurate each link-local or global ID
UINT8 node_ext_id[] = { 0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x00};

UINT8 sec_key[] = {
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
        0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };
        
IP6_ADDRESS base_addr = {{
        0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x0e, 'T', 'R', 0xff, 0xfe, 0x00, 0x00, 0x05 }};

IP6_ADDRESS server_addr = {{
        0xaa, 0xaa, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11 }};
/*

IP6_ADDRESS server_addr = {{
        0xfd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x0e, 'T', 'R', 0xff, 0xfe, 0x00, 0x00, 0x01 }};
*/          
IP6_ADDRESS broadcast_addr= {{
        0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02 }};
       
IP6_ADDRESS actuator_addr = {{
        0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x0e, 'T', 'R', 0xff, 0xfe, 0x00, 0x00, 0x05 }};
        

struct device
{
  char mfg[MAX_STRING_LENGTH];
  char mdl[MAX_STRING_LENGTH];
  //char ser[MAX_STRING_LENGTH];
  UINT8 type; // only for actuator (sensor = 0);
  //UINT8 bat;  // 
};
typedef struct device DEVICE;

struct location
{
  // x and y are float number. x = x_int.x_point, y = y_int.y_point
  UINT16 x_int;
  UINT8 x_point;
  UINT16 y_int;
  UINT8 y_point;
  char sem[MAX_STRING_LENGTH];
};
typedef struct location LOCATION;

struct generic_sensor
{
  // temp = temp_int.temp_point
  UINT8 sensor_type;  // bitmap sensor type here: below order from MSB
  //UINT8 temp_int;
  //UINT8 temp_point;
  INT16 temp;  // JJ: changed 2013.1.8
  UINT8 hum;    // %
  UINT8 light;  // lux
  UINT16 co;
  UINT16 co2; // ppm
  BOOL pir;   // on/off
};
typedef struct generic_sensor GENERIC_SENSOR;

struct sensor_profile
{
  UINT8 P_ID;  // actuator profile id
//  IP6_ADDRESS src;  // sensor ipv6 addr
  UINT16 A_ID;  // actuator ipv6 addr  
  UINT8 at;   // actuator type (/dev/type)  JJ: added
  UINT8 st; // sensor type, 8-bit bitmap, temp, hum, co, co2, light, PIR
  UINT8 num_sensor;   // number of sensors (calculated from st)
  //UINT8 diff1[MAX_NUM_SENSOR]; // delta threshold
  //UINT8 diff2[MAX_NUM_SENSOR];  // delta threshold, under point
  UINT16 diff[MAX_NUM_SENSOR]; // JJ: 2013.1.8 
  UINT16  tmin[MAX_NUM_SENSOR]; // minimum time bound
  UINT16  tmax[MAX_NUM_SENSOR]; // maximum time bound
  UINT8 st2;
};
typedef struct sensor_profile SENSOR_PROFILE;

///////////////////////////////////////////////////////////////////////////
// From JK
typedef struct sensor_report_pkt{
  UINT16 N_ID;
  UINT16 P_ID;
  UINT8 Sensor_type;
  UINT16 Value;
  BOOL coop_report_flag;
}sensor_report_pkt;

typedef struct event_noti_pkt{
  UINT16 N_ID;
  UINT16 P_ID;
  UINT8 Sensor_type;
  UINT8 Actuator_type;
  BOOL Event_status;
  UINT8  av; // actuation level of actuation control, htw added
}event_noti_pkt;

typedef struct event_help_pkt{
  UINT16 N_ID;
  UINT16 P_ID;
  UINT16 A_ID;
  UINT8 Sensor_type;
  UINT8 Actuator_type;
  BOOL Event_status;
  UINT8  av; // actuation level of actuation control, htw added
}event_help_pkt;

typedef struct action_noti_pkt{
  UINT16 N_ID;
  UINT16 P_ID;
  UINT16 A_ID;
  UINT16 Sensor_ID;
  UINT8 Sensor_type;
  BOOL Event_status;
  UINT8  av; // actuation level of actuation control, htw added
}action_noti_pkt;
///////////////////////////////////////////////////////////////////////////
// Functions
void init_profile(void);
BOOL checkIsMe(const IP6_ADDRESS *addr);
BOOL checkIsMe16(UINT16 addr);
UINT8 atox(char asc);
void button_cb(UINT8 id);

// Functions for sensing
BOOL checkIsRedundant(void);
void send_messageto(UINT8* SENSOR_TYPE_IN);
void trickle_timer(void *args);
void adaptive_sampling_triggered(UINT8 SENSOR_TYPE);
void check_sensor(void *args);

// Functions for Profile
void save_ip6_addr(char* str ,UINT8 val_len, UINT16* dst_addr);
BOOL isRcvAck(void);
void report_join(void *args);
UINT8 save_profile(char *profile_name, char *profile_value, UINT8 val_len, UINT8 id, UINT8 code);
UINT8 process_data_parser(char* msg, UINT8 total_len, UINT8 code);
void reset_profile(void);
