deps_config := \
	nos/net/eap/Kconfig \
	nos/net/pana/Kconfig \
	nos/net/mle/Kconfig \
	nos/net/coap/Kconfig \
	nos/net/tcp/Kconfig \
	nos/net/udp/Kconfig \
	nos/net/rpl/Kconfig \
	nos/net/ip6/Kconfig \
	nos/net/ppp/Kconfig \
	nos/net/6lowpan/Kconfig \
	nos/net/ethernet6/Kconfig \
	nos/net/zigbee-ip/Kconfig \
	nos/net/l2-routing/Kconfig \
	nos/net/mac/glossy/Kconfig \
	nos/net/mac/nano-mac/Kconfig \
	nos/net/mac/Kconfig \
	nos/net/802.15.4/Kconfig \
	nos/net/Kconfig \
	nos/lib/Kconfig \
	nos/kernel/Kconfig \
	nos/drivers/eth-dev/Kconfig \
	nos/drivers/wpan-dev/Kconfig \
	nos/drivers/storage-dev/Kconfig \
	nos/drivers/sensor/Kconfig \
	nos/drivers/clcd/Kconfig \
	nos/drivers/Kconfig \
	nos/platform/isn-903n/Kconfig.power \
	nos/drivers/shtxx/Kconfig \
	nos/drivers/mics5132/Kconfig \
	nos/drivers/elt_s100/Kconfig \
	nos/drivers/bh1600/Kconfig \
	nos/drivers/wpan-dev/rf212/Kconfig \
	nos/arch/atmega1284p/Kconfig.usart1 \
	nos/arch/atmega1284p/Kconfig.usart0 \
	nos/arch/atmega1284p/Kconfig \
	/home/nos/workspace/wsan-2014/nanoqplus-wsan/nos/platform/isn-903n/Kconfig

.config include/linux/autoconf.h: $(deps_config)

$(deps_config):
