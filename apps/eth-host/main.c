// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * IPv6 host application with an Ethernet interface
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 11.
 */

#include "nos.h"

UINT8 def_inf = 255;

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 10;
#endif

void ip6_state_changed(UINT8 ip6_inf, IP6_NODE_STATE state)
{
    printf("IPv6 iface %u: State changed to %d\n", ip6_inf, state);
}

void heartbeat(void *args)
{
    led_toggle(1);
}

void ping_replied(const IP6_HEADER *ip6,
                  UINT16 seq,
                  UINT16 sz,
                  const void *payload)
{
    char srctext[IP6_NTOP_BUF_SZ];

    if (ip6)
    {
        printf("%u bytes from %s: icmp_seq=%u ttl=%u\n",
               sz, ip6_ntop(&ip6->src_addr, srctext), seq, ip6->hop_limit);
    }
    else
    {
        printf("Request timeout\n");
    }
}

void uart_rx_handler(UINT8 port, char c)
{
    IP6_ADDRESS dst;
    char addrtext[IP6_NTOP_BUF_SZ];

    if (c == '1')
    {
        memcpy(&dst,
               "\xFF\x1E\x00\x00\x00\x00\x00\x00"
               "\x00\x00\x00\x00\x00\x01\x00\x02",
               16);
        ping6(0, &dst, 1452, 1, ping_replied);
        printf(">> ping6 s=1452 c=1 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == '2')
    {
        memcpy(&dst,
               "\xFF\x1E\x00\x00\x00\x00\x00\x00"
               "\x00\x00\x00\x00\x00\x01\x00\x02",
               16);
        ping6(0, &dst, 1352, 1, ping_replied);
        printf(">> ping6 s=1352 c=1 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == '3')
    {
        memcpy(&dst,
               "\xFE\x80\x00\x00\x00\x00\x00\x00"
               "\x02\x00\x00\xFF\xFE\x00\x01\x00",
               16);
        ping6(0, &dst, 2, 1, ping_replied);
        printf(">> ping6 s=2 c=1 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == '4')
    {
        memcpy(&dst,
               "\xFE\x80\x00\x00\x00\x00\x00\x00"
               "\x7E\xC3\xA1\xFF\xFE\x87\x2B\x69",
               16);
        ping6(0, &dst, 64, 4, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == '5')
    {
        memcpy(&dst,
               "\x20\x01\x0D\xB8\xFF\xFF\x00\x01"
               "\x7E\xC3\xA1\xFF\xFE\x87\x2B\x69",
               16);
        ping6(0, &dst, 64, 4, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == '6')
    {
        memcpy(&dst,
               "\xFE\x80\x00\x00\x00\x00\x00\x00"
               "\xF2\xDE\xF1\xFF\xFE\x11\xE6\x1B",
               16);
        ping6(0, &dst, 64, 4, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == '7')
    {
        memcpy(&dst,
               "\x20\x01\x0D\xB8\xFF\xFF\x00\x01"
               "\xF2\xDE\xF1\xFF\xFE\x11\xE6\x1B",
               16);
        ping6(0, &dst, 64, 4, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == '8')
    {
        memcpy(&dst,
               "\xFE\x80\x00\x00\x00\x00\x00\x00"
               "\x4E\xE6\x76\xFF\xFE\xC2\x62\xD4",
               16);
        ping6(0, &dst, 64, 4, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == '9')
    {
        memcpy(&dst,
               "\x20\x01\x0D\xB8\xFF\xFF\x00\x01"
               "\x4E\xE6\x76\xFF\xFE\xC2\x62\xD4",
               16);
        ping6(0, &dst, 64, 4, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'a' || c == 'A')
    {
        memcpy(&dst,
               "\xFE\x80\x00\x00\x00\x00\x00\x00"
               "\x02\x1E\x8C\xFF\xFE\x25\xC2\xEF",
               16);
        ping6(0, &dst, 64, 4, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'b' || c == 'B')
    {
        memcpy(&dst,
               "\x20\x01\x0D\xB8\xFF\xFF\x00\x01"
               "\x02\x1E\x8C\xFF\xFE\x25\xC2\xEF",
               16);
        ping6(0, &dst, 64, 4, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'c' || c == 'C')
    {
        memcpy(&dst,
               "\xFF\x02\x00\x00\x00\x00\x00\x00"
               "\x00\x00\x00\x00\x00\x00\x00\x01",
               16);
        ping6(0, &dst, 64, 4, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'd' || c == 'D')
    {
        memcpy(&dst,
               "\xFF\x02\x00\x00\x00\x00\x00\x00"
               "\x00\x00\x00\x00\x00\x00\x00\x02",
               16);
        ping6(0, &dst, 64, c, ping_replied);
        printf(">> ping6 s=64 c=4 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'e' || c == 'E')
    {
        memcpy(&dst,
               "\xFE\x80\x00\x00\x00\x00\x00\x00"
               "\x7E\xC3\xA1\xFF\xFE\x87\x2B\x69",
               16);
        ping6(0, &dst, 1452, 10, ping_replied);
        printf(">> ping6 s=1452 c=10 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'f' || c == 'F')
    {
        memcpy(&dst,
               "\x20\x01\x0D\xB8\xFF\xFF\x00\x01"
               "\x7E\xC3\xA1\xFF\xFE\x87\x2B\x69",
               16);
        ping6(0, &dst, 1452, 10, ping_replied);
        printf(">> ping6 s=1452 c=10 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'g' || c == 'G')
    {
        memcpy(&dst,
               "\xFE\x80\x00\x00\x00\x00\x00\x00"
               "\xF2\xDE\xF1\xFF\xFE\x11\xE6\x1B",
               16);
        ping6(0, &dst, 1452, 10, ping_replied);
        printf(">> ping6 s=1452 c=10 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'h' || c == 'H')
    {
        memcpy(&dst,
               "\x20\x01\x0D\xB8\xFF\xFF\x00\x01"
               "\xF2\xDE\xF1\xFF\xFE\x11\xE6\x1B",
               16);
        ping6(0, &dst, 1452, 10, ping_replied);
        printf(">> ping6 s=1452 c=10 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'i' || c == 'I')
    {
        memcpy(&dst,
               "\xFE\x80\x00\x00\x00\x00\x00\x00"
               "\x4E\xE6\x76\xFF\xFE\xC2\x62\xD4",
               16);
        ping6(0, &dst, 1452, 10, ping_replied);
        printf(">> ping6 s=1452 c=10 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'j' || c == 'J')
    {
        memcpy(&dst,
               "\x20\x01\x0D\xB8\xFF\xFF\x00\x01"
               "\x4E\xE6\x76\xFF\xFE\xC2\x62\xD4",
               16);
        ping6(0, &dst, 1452, 10, ping_replied);
        printf(">> ping6 s=1452 c=10 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'k' || c == 'K')
    {
        memcpy(&dst,
               "\xFE\x80\x00\x00\x00\x00\x00\x00"
               "\x02\x1E\x8C\xFF\xFE\x25\xC2\xEF",
               16);
        ping6(0, &dst, 1452, 10, ping_replied);
        printf(">> ping6 s=1452 c=10 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else if (c == 'l' || c == 'L')
    {
        memcpy(&dst,
               "\x20\x01\x0D\xB8\xFF\xFF\x00\x01"
               "\x02\x1E\x8C\xFF\xFE\x25\xC2\xEF",
               16);
        ping6(0, &dst, 1452, 10, ping_replied);
        printf(">> ping6 s=1452 c=10 i=0 %s\n", ip6_ntop(&dst, addrtext));
    }
    else
    {
        printf("=== MENU ===\n");
        printf("(1) 'ping6 s=1452 c=1 i=0 ff1e::1:2'"
               " (for v6LC.4.1.10 and v6LC.4.1.11)\n");
        printf("(2) 'ping6 s=1352 c=1 i=0 ff1e::1:2'"
               " (for v6LC.4.1.10)\n");
        printf("(3) 'ping6 s=2 c=1 i=0 fe80::0200:00ff:fe00:0100'"
               " (for v6LC.5.1.1)\n");
        printf("(4) 'ping6 s=64 c=4 i=0 fe80::7ec3:a1ff:fe87:2b69'"
               " (for IP6Interop - ping to VendorA LL)\n");
        printf("(5) 'ping6 s=64 c=4 i=0 2001:db8:ffff:1:7ec3:a1ff:fe87:2b69'"
               " (for IP6Interop - ping to VendorA GL)\n");
        printf("(6) 'ping6 s=64 c=4 i=0 fe80::f2de:f1ff:fe11:e61b'"
               " (for IP6Interop - ping to VendorB LL)\n");
        printf("(7) 'ping6 s=64 c=4 i=0 2001:db8:ffff:1:f2de:f1ff:fe11:e61b'"
               " (for IP6Interop - ping to VendorB GL)\n");
        printf("(8) 'ping6 s=64 c=4 i=0 fe80::4ee6:76ff:fec2:62d4'"
               " (for IP6Interop - ping to VendorC LL)\n");
        printf("(9) 'ping6 s=64 c=4 i=0 2001:db8:ffff:1:4ee6:76ff:fec2:62d4'"
               " (for IP6Interop - ping to VendorC GL)\n");
        printf("(A) 'ping6 s=64 c=4 i=0 fe80::21e:8cff:fe25:c2ef'"
               " (for IP6Interop - ping to VendorD LL)\n");
        printf("(B) 'ping6 s=64 c=4 i=0 2001:db8:ffff:1:21e:8cff:fe25:c2ef'"
               " (for IP6Interop - ping to VendorD GL)\n");
        printf("(C) 'ping6 s=64 c=4 i=0 ff02::1'"
               " (for IP6Interop - ping to all nodes)\n");
        printf("(D) 'ping6 s=64 c=4 i=0 ff02::2'"
               " (for IP6Interop - ping to all routers)\n");
        printf("(E) 'ping6 s=1452 c=10 i=0 fe80::7ec3:a1ff:fe87:2b69'"
               " (for IP6Interop - ping 1500 byte to VendorA LL)\n");
        printf("(F) 'ping6 s=1452 c=10 i=0 2001:db8:ffff:1:7ec3:a1ff:fe87:2b69'"
               " (for IP6Interop - ping 1500 byte to VendorA GL)\n");
        printf("(G) 'ping6 s=1452 c=10 i=0 fe80::f2de:f1ff:fe11:e61b'"
               " (for IP6Interop - ping 1500 byte to VendorB LL)\n");
        printf("(H) 'ping6 s=1452 c=10 i=0 2001:db8:ffff:1:f2de:f1ff:fe11:e61b'"
               " (for IP6Interop - ping 1500 byte to VendorB GL)\n");
        printf("(I) 'ping6 s=1452 c=10 i=0 fe80::4ee6:76ff:fec2:62d4'"
               " (for IP6Interop - ping 1500 byte to VendorC LL)\n");
        printf("(J) 'ping6 s=1452 c=10 i=0 2001:db8:ffff:1:4ee6:76ff:fec2:62d4'"
               " (for IP6Interop - ping 1500 byte to VendorC GL)\n");
        printf("(K) 'ping6 s=1452 c=10 i=0 fe80::21e:8cff:fe25:c2ef'"
               " (for IP6Interop - ping 1500 byte to VendorD LL)\n");
        printf("(L) 'ping6 s=1452 c=10 i=0 2001:db8:ffff:1:21e:8cff:fe25:c2ef'"
               " (for IP6Interop - ping 1500 byte to VendorD GL)\n");
        printf("\n");
    }
}

int main(void)
{
    UINT8 mac48[] = { 'E', 'T', 'R', 'I', 0, 0 };
    
    nos_init();
    led_on(0);

    printf("\n*** NanoQplus ***\n");

    uart_set_getc_callback(STDIO, uart_rx_handler);
    enable_uart_rx_intr(STDIO);

    // Initialize an Ethernet interface.
    mac48[0] &= ~(1 << 0); //unicast
    mac48[0] |= (1 << 1);  //local
    mac48[4] = node_id >> 8;
    mac48[5] = node_id & 0xff;
    def_inf = ether6_init(0, mac48, 10, 1);
    ip6_set_state_notifier(def_inf, ip6_state_changed);
    ip6_init(def_inf);

    //thread_create(cli, NULL, 100, PRIORITY_NORMAL);
    user_timer_create_ms(heartbeat, NULL, 500, TRUE);
    sched_start();
    
    return 0;
}
