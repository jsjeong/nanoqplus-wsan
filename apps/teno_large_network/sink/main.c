/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "nos.h"

#define MY_SHORT_ADDR	1
#define PAN_ADDR	312

UINT8 rx_data[NWK_MAX_PAYLOAD_SIZE];	//103 byte long
UINT8 tx_data[NWK_MAX_PAYLOAD_SIZE];
INT8 input_str[6];

//Receives string and toggles LEDs.
void rx_callback(void)
{
    UINT16 rx_src_id, parent_id, dest_id;
    UINT8 rx_data_length, rx_port, loop;
    led_toggle(3);
    teno_recv_from_nwk(&rx_src_id, &rx_port, &rx_data_length, rx_data, &parent_id, &dest_id);
    uart_puts(STDIO, "\n\r");
    loop = rx_src_id/10;
    while(--loop)
    {
        uart_puts(STDIO, "\t\t");	
    }
    printf("RX from %u: %u (parent:%u)", rx_src_id, rx_data[0], parent_id);
}


void task1(void* args)
{
    UINT8 i;
    UINT16 dest_addr;
    for (i =0; i<NWK_MAX_PAYLOAD_SIZE; ++i)
    {
        tx_data[i]=i;		// dummy data
    }

    while (TRUE)
    {
        ++tx_data[0];
        printf("\nInput destination address : ");
        uart_gets(STDIO, input_str, sizeof(input_str));
        dest_addr = atoi(input_str);
        teno_send_to_node(dest_addr, 2, 1, tx_data);
        led_toggle(2);		
    }
}

int main(void) 
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    nwk_init(PAN_ADDR, MY_SHORT_ADDR, rx_callback);
    printf("PAN address: %u\nShort address : %u\n", PAN_ADDR, MY_SHORT_ADDR);
    teno_role_as_sink();

    thread_create(task1, NULL, 0, PRIORITY_NORMAL);
    sched_start();

    return 0;
}
