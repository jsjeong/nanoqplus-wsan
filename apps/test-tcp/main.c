/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * UDP echo server application
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2010.02.01
 */

#include "nos.h"

UINT8 eui64[] = {0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x0a};
UINT8 eui64dst[] = {0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x01};
	UINT8 pre[]={0xFD,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

void task_client(void *args)
{
	int k;
	int tc_idx;

	IP6_ADDRESS dst6;
	memcpy(dst6.s6_addr,pre,8);
	memcpy(&dst6.s6_addr[8],eui64dst,8); 


	thread_sleep_sec(20);
	while(1)
	{
		thread_sleep(60);

		tc_idx=tcp_connect(dst6,1010);	// block
		printf("\n tc idx=%d\n",tc_idx);
	}

}

void task_server(void *args)
{
	int k;
	int tc_idx;
	int tc_cli;
	UINT16 cli_port;
	char *buf;

	IP6_ADDRESS src6,dst6;
	memcpy(src6.s6_addr,pre,8);
	memcpy(&src6.s6_addr[8],eui64,8); 


	buf=(char*)malloc(200);

	thread_sleep_sec(20);

	tc_idx=tcp_bind(src6,1010);	
	if(tc_idx==ERROR_FAIL) printf("\n no more connection %d\n",tc_idx);
	else

	{

			printf("\n tc idx=%d\n",tc_idx);
			tcp_listen(tc_idx);

			tc_cli=tcp_accept(tc_idx,&dst6, &cli_port);	// block

			if(tc_cli==FALSE) printf("\n accept failed\n");
			else printf("\n accepted cliport=%u\n",cli_port);
	while(1)
	{
		thread_sleep(60);
		k=tcp_recv(tc_cli,buf,200,0);
		buf[k]='\0';
		printf(" %d bytes received. buf:%s\n",k,buf);

		tcp_send(tc_cli,buf,k);
		

	}
	}

}

int main(void) 
{
    UINT8 task1_id;

    uint32_t  u32_var;
    int tmp;

    nos_init();
    led_on(1);
    printf("\n*** Nano OS ***\n");


    u32_var = 0xa;
    tmp = 16 << (u32_var & 0x07);
    printf("tmp: %d\n", tmp); // "tmp: 16" is printed instead of "tmp: 64"


    // Init lowpan
    lowpan_init(0,0x0001, 0xffff, eui64, 10, 1);
    ip6_init(0);
    
    tcp_init();

    printf("tcp init\n");


    task1_id = thread_create(task_server, NULL, 500, PRIORITY_NORMAL);  
   

    sched_start();
    return 0;
} // main
