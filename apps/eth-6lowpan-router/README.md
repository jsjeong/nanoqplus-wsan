# Ethernet-6LoWPAN router application

This application makes a target board to act as a border router that routes
packets between an Ethernet network and a 6LoWPAN.

## Requirements

The target board consists of an Ethernet transceiver and an IEEE 802.15.4
radio transceiver is needed. Supported boards are below.

* Mango-EToI

## Configuration

![Network Topology](https://docs.google.com/drawings/d/1KQZ6xQEXo-Uc8Pyf0FU9819YR_H6K7dW7-NSPPuuBYk/pub?w=961&h=421)

This application assumes that the Ethernet link (a) side is a super network and
the 6LoWPAN (b) is a sub network. In order to forward packets to the 6LoWPAN, (f)
a routing rule for packets destined for the 6LoWPAN prefix (c) should be applied
to the Ethernet side router (d) manually. It can be worked automatically by
auto-configuration protocols such as DHCP-PD. But the DHCP-PD agent on (e) the
target board is not implemented yet.

The target board will act as a RPL root to make the 6LoWPAN as an IPv6 multi-hop
routing domain. The `rpl_root_init()` function in the `main.c` initializes the
RPL root functionality.

[TBD]

## Authors

- Jongsoo Jeong (ETRI)
