/*
 * Automatically generated C config: don't edit
 * Nano OS kernel version: 
 * Thu Mar  6 11:59:30 2014
 */
#define AUTOCONF_INCLUDED

/*
 * Platform: Mango-EToI
 */
#define CONFIG_MCU_NAME "stm32f10x"
#define STM32F10X_BUILD_GCC 1
#undef STM32F10X_BUILD_IAR
#undef STM32F10X_BUILD_KEIL
#undef HEAP_DEBUG
#define CONFIG_PLATFORM_NAME "mango-etoi"
#define PLATFORM_DEP 1
#define LED_M 1
#undef BUTTON_M
#undef UART_M
#undef CC2520_M
#undef AT86RF212_M
#undef CC1120_M
#undef ENC28J60_M
#undef MANGO_ETOI_NET1_CC2520
#undef MANGO_ETOI_NET1_RF212
#undef MANGO_ETOI_NET1_CC1120
#undef MANGO_ETOI_NET1_ENC28J60
#define MANGO_ETOI_NET1_NONE 1
#undef MANGO_ETOI_NET2_CC2520
#undef MANGO_ETOI_NET2_RF212
#undef MANGO_ETOI_NET2_CC1120
#undef MANGO_ETOI_NET2_ENC28J60
#define MANGO_ETOI_NET2_NONE 1
#undef LMC1623_M
#undef GENERIC_ADC_SENSOR_M
#undef GENERIC_ADC_GAS_SENSOR_M
#undef GENERIC_ADC_LIGHT_SENSOR_M
#undef GENERIC_ADC_MIC_SENSOR_M
#undef GENERIC_ADC_PIR_SENSOR_M
#undef PLATFORM_SPECIFIC_TEMPERATURE_SENSOR_M
#undef PLATFORM_SPECIFIC_LIGHT_SENSOR_M
#undef STORAGE_M

/*
 * Kernel
 */
#undef KERNEL_M

/*
 * Library
 */
#define LIB_UTC_CLOCK_M 1
#undef LIB_LPP_M

/*
 * Network Protocol
 */
#undef ZIGBEE_IP_M
#undef EAP_M
