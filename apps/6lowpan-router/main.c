// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * 6LoWPAN Router
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2010. 10. 1.
 */

#include "nos.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef BATTERY_MONITOR_M
#include "bat-monitor.h"
#endif //BATTERY_MONITOR_M

INT8 send_timer_id = -1;

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 5;
#endif

UINT8 node_ext_id[] = { 0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x00};

IP6_ADDRESS server_addr;

UINT8 def_inf; //IPv6 default interface

UINT8 sec_key[] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
                    0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };

struct rpl_status_report
{
    UINT16 sent;
    UINT16 rank;
    UINT32 fwderr;
    UINT32 rankerr;
    UINT32 poison;
    UINT32 uploop;
    UINT32 downloop;
};

void rpl_report(void *args)
{
    struct rpl_status_report rep;
    static UINT16 sent = 0;

    if (ip6_is_attached(def_inf))
    {
        sent++;
        memset((void *) &rep, 0, sizeof(struct rpl_status_report));

        rep.sent = htons(sent);
        rep.rank = htons(rpl_get_rank(def_inf));
        rep.fwderr = htonl(rpl_get_fwd_err_cnt(def_inf));
        rep.rankerr = htonl(rpl_get_rank_err_cnt(def_inf));
        rep.poison = htonl(rpl_get_poison_cnt(def_inf));
        rep.uploop = htonl(rpl_get_upward_loop_cnt(def_inf));
        rep.downloop = htonl(rpl_get_downward_loop_cnt(def_inf));
        
        rpl_get_root_address(def_inf, &server_addr);
        udp_sendto(def_inf, 1234,
                   &server_addr, 1234,
                   &rep, sizeof(struct rpl_status_report));
        printf("Send! (%u)\n", sent);
    }
    else
    {
        printf("Network detached.\n");
    }
}

void listener(UINT8 in_inf,
              const IP6_ADDRESS *src_addr,
              UINT16 src_port,
              UINT16 dst_port,
              const UINT8 *msg, UINT16 len)
{
    char text_addr[IP6_NTOP_BUF_SZ];
    printf("Recv from [%s]:%u (%u byte) - %s\n",
            ip6_ntop(src_addr, text_addr), src_port, len, msg);
}

void ip6_state_changed(UINT8 ip6_inf, IP6_NODE_STATE state)
{
    printf("State changed to %d\n", state);
    if (ip6_is_attached(def_inf))
    {
        printf("RPL network connection established.\n");

        rpl_get_root_address(def_inf, &server_addr);

        if (send_timer_id < 0)
        {
            send_timer_id = user_timer_create_sec(rpl_report, NULL, 50,
                                                  USER_TIMER_PERIODIC);
            printf("send_timer_id:%d\n", send_timer_id);
        }
    }
}

int main(void)
{
    nos_init();
    led_on(0);

    printf("\n*** NanoQplus ***\n");

    // Initialize the LoWPAN interface.
    node_ext_id[6] = node_id >> 8;
    node_ext_id[7] = node_id & 0xff;
    def_inf = lowpan_init(0, 1, 0xffff, node_ext_id, 10, 1);
    ip6_set_state_notifier(def_inf, ip6_state_changed);
    rpl_init(def_inf);
    ip6_init(def_inf);

    udp_set_listen(1234, listener);

    sched_start();
    return 0;
}
