// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * RPL node test application to monitor the RPL network
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2010. 10. 1.
 */

#include "nos.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef BATTERY_MONITOR_M
#include "bat-monitor.h"
#endif //BATTERY_MONITOR_M

#include "rf212.h"  // JJ: add for set channel
#include "heap.h" // HH add 20140204

// JJ: WSAN profile
// Master Actuator
////////////////////////////////////////////
#include "platform.h"
#include "uart.h"
#include "led.h"
#include "taskq.h"
#include <string.h>
#include "irlcd.h" // HH add 20140204

#define wsan_port 1236
#define wsan_port_send 1235
#define MAX_UDP_LENGTH  200 // maximum UDP packet length
#define MAX_STRING_LENGTH 30  // string to define
#define MAX_NUM_SENSOR  9  // maximum number of kinds of sensors (e.g., temp, hum, light, co, co2, pir)
#define MAX_NUM_SN 10
#define MAX_NUM_ACT 10
#define MAX_NUM_SCENE 13
#define MAX_NUM_MA_PROFILE 1 // for master actuator only
#define MAX_NUM_SEN_PROFILE 10 // for sensor only


#define MAX_RX_QUEUE 10

enum{         // sType bitmap
  DOPPLER_SENSOR = 0x0100,
  TEMP_SENSOR = 0x0080,
  HUM_SENSOR = 0x0040,
  LIGHT_SENSOR = 0x0020,
  CO_SENSOR = 0x0010,
  CO2_SENSOR = 0x0008,
  PIR_SENSOR = 0x0004,
  MIC_SENSOR = 0x0002,
  ACC_SENSOR = 0x0001
};

enum {        // dType bitmap
    TYPE_SENSOR = 0x00,
    TYPE_ACT = 0x01,  //HH fix from 2, 20140204
    TYPE_MASTER_ACT = 0x02,
    TYPE_HYBRID = 0x03  //HH add, 20140204
};

enum {        // Condition bitmap
    CON_OCCUPANCY = 0x0001,
    CON_MIC_ONE = 0x0002,
    CON_MIC_TWO = 0x0004,
    CON_PROJECTOR = 0x0008,
    CON_WB = 0x0010
};
    

enum {        // msgID
    ID_RPT_JOIN = 0x01,
    ID_DEPLOY_MA_PROFILE = 0x02,
    ID_DEPLOY_ACT_PROFILE = 0x03,
    ID_SCENE_INFO = 0x04,
    ID_DEPLOY_SEN_PROFILE = 0x05,
    ID_RPT_SENSING = 0x06,
    ID_RPT_ACT = 0x07,
    ID_MA_CTRL = 0x08,
    ID_ACT_CTRL = 0x09,
    ID_REQ_ACT_STAT = 0x0a,
    ID_RSP_ACT_STAT = 0x0b,
    ID_REQ_SEN_VAL = 0x0c,
    ID_RSP_SEN_VAL = 0x0d,
    
    ID_RPT_ACT_INFO = 0x21,
    // ID_RPT_ACT_INFO_MA = 0x22,  // TBD
 
    ID_RPT_JOIN_ACK = 0x31,
    ID_DEPLOY_MA_PROFILE_ACK = 0x32,
    ID_DEPLOY_ACT_PROFILE_ACK = 0x33,

    ID_DEPLOY_SEN_PROFILE_ACK = 0x35,
    ID_RPT_SENSING_ACK = 0x36,
    ID_RPT_ACT_ACK = 0x37,
    ID_MA_CTRL_ACK = 0x38
};

enum{         // ON OFF
    ON = 1,
    OFF =0
};

////////////////////////////////////
// UDP RX Queue 
typedef struct rx_data {
    IP6_ADDRESS src_addr;
    UINT16 src_port;
    UINT16 dst_port;
    char msg[MAX_UDP_LENGTH];
    UINT16 len;
    UINT8 msgID;
} RX_DATA;
    
RX_DATA rx_queue[MAX_RX_QUEUE];
int front = 0;
int rear = -1;
int queue_size = 0;

// Linked list for sending data (with ack)
typedef struct tx_packet {
    long expired_time;
    UINT16 dest;
    UINT8 msgID;
    UINT16 port;
    char msg[MAX_UDP_LENGTH];
    UINT8 msg_len;
    long ack_time;
} TX_PACKET;

typedef struct tx_list {
    TX_PACKET val;
    struct tx_list *next;
} tx_list;

struct tx_list *head = NULL;
struct tx_list *curr = NULL;
///////////////////////////////

typedef struct REPORT_JOIN_PKT {
    UINT8 msgID;
  IP6_ADDRESS did;
  UINT8 dType; // IPv6 addr
  char locL[10]; // semantic location
  UINT16 locPx; // location x
  UINT16 locPy; // location y
  UINT8 aType; // actuation type (sensor = 0)
  UINT16 sType; // sensor type (bitmap)
} REPORT_JOIN_PKT;

typedef struct SCENE {
  UINT8 sceneID;
  UINT16 condition; // bitmap (0b10000: WB, 0b01000: proj., 0b0110: MIC, 0b0001: PIR)
} SCENE;

typedef struct MASTER_ACT_PROFILE {
    UINT8 msgID;
  UINT8 num_scene;
  UINT8 num_act;
  UINT16 act_addr[MAX_NUM_ACT];
  UINT8 num_sen;
  UINT16 sen_addr[MAX_NUM_SN];
  SCENE scene[MAX_NUM_SCENE];
  UINT8 groupID;
} MASTER_ACT_PROFILE;

typedef struct ACT_PROFILE {
    UINT8 msgID;
  UINT8 num_scene;
  UINT8 sceneID[MAX_NUM_SCENE];
  UINT8 control[MAX_NUM_SCENE]; // dimming value; //name changed acc. to profile ppt by HH 20140204
} ACT_PROFILE;


typedef struct SENSOR_PROFILE {
    UINT8 msgID;
  UINT16 act_addr;
  UINT16 sType; // bitmap
  UINT16 mode[MAX_NUM_SENSOR];
} SENSOR_PROFILE;

typedef struct SCENE_RPT_PKT {
  UINT8 msgID;
  UINT8 sceneID;
} SCENE_RPT_PKT;

typedef struct ACT_STATUS_RPT_PKT {
  UINT8 msgID;
  UINT8 scene_status;
  UINT32 timestamp;
} ACT_STATUS_RPT_PKT;

// ACT
typedef struct ACTUATION_INFO_RPT_PKT {
  UINT8 msgID;
  UINT8 sceneID;
  UINT8 status;
} ACTUATION_INFO_RPT_PKT; // to MA

typedef struct SENSOR_INFO_RPT_PKT {
  UINT8 msgID;
  UINT16 sType;
  UINT16 condition;
  UINT32 timestamp;
  UINT32 reserved;
} SENSOR_INFO_RPT_PKT;

typedef struct SENSING_RESP_PKT {
  UINT8 msgID;
  UINT16 sType;
  UINT16 sensingValues[MAX_NUM_SENSOR];
  UINT32 timestamp;
} SENSING_RESP_PKT;


////////////////////////////////////////////
// Global Variables
char locL[10];
UINT16 locPx;
UINT16 locPy;
UINT8 aType;

int current_ma_profile = 0;
MASTER_ACT_PROFILE my_ma_profile[MAX_NUM_MA_PROFILE];
ACT_PROFILE my_act_profile;
int current_sen_profile = -1;
SENSOR_PROFILE my_sen_profile[MAX_NUM_SEN_PROFILE];

// For Actuation
UINT8 Act_IR_Con[5]; // actuator control value HH add 20140204
UINT8 BLO_ONOFF_STATUS = ON;
/////////////////////////////////////////////
enum
{
    COMMAND_READY = 1,
    COMMAND_INCOMING = 2,
    COMMAND_STARTED = 3,
    COMMAND_INCOMING_0 = 4,
    COMMAND_INCOMING_1 = 5,
};

static UINT8 isConnected = 0;
static unsigned char serialBuffer[100];
static unsigned char serialIndex = 0;
static unsigned char serialLength = 0;
static unsigned char commandStatus = COMMAND_READY;
static unsigned char msgID = 0;  // JJ: add for msgID check
///////////////////////////////////////////

INT8 send_timer_id = -1;
#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 10;
#endif
UINT8 node_ext_id[] = { 0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x00};

IP6_ADDRESS base_addr = {{
        0xfd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x0e, 'T', 'R', 0xff, 0xfe, 0x00, 0x00, 0x05 }};

IP6_ADDRESS root_addr;
IP6_ADDRESS server_addr = {{
        0xaa, 0xaa, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11 }};
       
IP6_ADDRESS broadcast_addr= {{
        0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02 }};

UINT8 def_inf; //IPv6 default interface

UINT8 sec_key[] = {
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
        0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff };

///////////////////////////////////////////////////////////////////////////
// Functions
void init_profile(void);
void report_join(void);

////////////////////////////////////////////////////////////////
// JJ: Add for WSAN profile (MA)

// for tx_list (linked list)
struct tx_list* create_list(TX_PACKET val)
{
    struct tx_list *ptr = (tx_list*)nos_malloc(sizeof(tx_list));
    if (ptr == NULL) {
        printf("#Fail to create tx_list\n");
        return NULL;
    }
    ptr->val = val;
    ptr->next = NULL;

    head = curr = ptr;
    return ptr;
}

struct tx_list* add_to_list(TX_PACKET val) 
{
    printf("#add: DST=%d, MSGID=%d\n", val.dest, val.msgID); //HH mod. 20140204
    if (NULL == head)
        return (create_list(val));
    
    struct tx_list *ptr = (tx_list*)nos_malloc(sizeof(tx_list));
    if (ptr == NULL) {
        printf("#Fail to create tx_list\n");
        return NULL;
    }
    ptr->val = val;
    ptr->next = NULL;
    curr->next = ptr;
    curr = ptr;

    return ptr;
}

struct tx_list* search_in_list(UINT16 addr, UINT8 msgID, struct tx_list **prev)
{
    struct tx_list *ptr = head;
    struct tx_list *tmp = NULL;
    
    while(ptr !=NULL) {
        //printf("search: %d, %d\n", addr, msgID);
        //printf("from: %d, %d\n", ptr->val.dest, ptr->val.msgID);
        if ((ptr->val.dest == addr) && (ptr->val.msgID == msgID) ) {
            if (prev) 
                *prev = tmp;
            return ptr;
        }
        else {
            tmp = ptr;
            ptr = ptr->next;
        }
    }
    return NULL;
}

int delete_from_list(UINT16 addr, UINT8 msgID)
{
    struct tx_list *prev = NULL;
    struct tx_list *del = NULL;
    //print_list();
    
    del = search_in_list(addr, msgID, &prev);
    if (del == NULL)
        return -1;
    else {
        printf("#del: DST=%d, MSGID=%d\n", del->val.dest, del->val.msgID);  //HH mod. 20140204
        if (prev !=NULL)
            prev->next = del->next;
        if (del == curr)
            curr = prev;
        if (del == head)
            head = del->next;
    }
    nos_free(del);
    del = NULL;

    if (head == NULL)
        printf("#clear head\n");
    if (curr == NULL)
        printf("#clear curr\n");
    return 1;
}

void print_list(void)
{
    struct tx_list *ptr = head;
    printf(" #----------------------------\n");
    while( ptr != NULL) {
        printf("#DST = %d, MSGID = %d\n", ptr->val.dest, ptr->val.msgID);  //HH mod. 20140204
        ptr = ptr->next;
    }

    return;
}

// for rx queue
UINT8 enqueue_rx(RX_DATA in)
{
    if (queue_size == MAX_RX_QUEUE) {
        printf("#Full of rx_queue\n");
        return 0;
    }
    
    rx_queue[++rear] = in; // TBC: check the struct copy
    queue_size++;
    // debug
    if (queue_size > MAX_RX_QUEUE)
        printf("#Enqueue error\n");
    if (rear == MAX_RX_QUEUE) 
        rear = -1;

    return 1;
}

int dequeue_rx(void)
{
    int temp;
    if (queue_size == 0) {
        printf("#Queue empty\n");
        return -1;
    }
    temp = front;
    front++;
    queue_size--;
    // debug
    if (queue_size < 0)
        printf("#Dequeue error\n");

    if (front == MAX_RX_QUEUE)
        front = 0;
    return temp;
}

// TODO: Send process here
void process_tx(void) {
    IP6_ADDRESS dst_addr;
    struct tx_list *temp = head;
    UINT8 isSend = 1;
    char text_addr[IP6_NTOP_BUF_SZ];

    if (ip6_is_attached(def_inf)) {

        while( (temp !=NULL) && isSend ) {
            temp->val.expired_time -= 40;
            if (temp->val.expired_time < 0) {
                if (temp->val.dest == 0x0001) {
                    memcpy(&dst_addr, &server_addr, 16);
                    printf("#Server [%s]: PORT=%d, LEN=%d\n",  //HH mod. 20140204
                           ip6_ntop(&dst_addr, text_addr), temp->val.port, temp->val.msg_len);
                }
                else {
                    memcpy(&dst_addr, &base_addr, 16);
                    memcpy(&dst_addr.s6_addr[14], &temp->val.dest, 2);
                    printf("#To [%s]: PORT=%d, LEN=%d\n",  //HH mod. 20140204
                       ip6_ntop(&dst_addr, text_addr), temp->val.port, temp->val.msg_len);
                }
                // printf("send packet %ld, %ld\n", temp->val.expired_time, temp->val.ack_time);
                udp_sendto(def_inf, wsan_port, &dst_addr, temp->val.port, temp->val.msg, temp->val.msg_len);
                led_toggle(2);
                isSend = 0;
                if (temp->val.ack_time > 0)
                    temp->val.expired_time = temp->val.ack_time; 

                return;
            }
            temp = temp->next;
        }        
    }
}

void send_Ack(int ind, UINT8 id, UINT16 dst_port)
{
    UINT8 msg[2];
    msg[0] = id;
    msg[1] = 0x00;
    printf("#Send Ack: ID=%d\n", id);  //HH mod. 20140204
    udp_sendto(def_inf, rx_queue[ind].dst_port, &rx_queue[ind].src_addr, dst_port/* rx_queue[ind].src_port */, msg, 2);
}

// Support changing the whole MA profile only
void save_ma_profile(UINT8* ptr) 
{
    UINT8 i;
    UINT8 ind;
    UINT8 ma_cnt=0;
    MASTER_ACT_PROFILE temp_ma;
    temp_ma.msgID = ptr[ma_cnt++];
    temp_ma.num_scene = ptr[ma_cnt++];
    temp_ma.num_act = ptr[ma_cnt++]; 
    memcpy(temp_ma.act_addr, &ptr[ma_cnt], temp_ma.num_act *2);
    ma_cnt += temp_ma.num_act *2;
    temp_ma.num_sen = ptr[ma_cnt++];
    memcpy(temp_ma.sen_addr, &ptr[ma_cnt], temp_ma.num_sen *2);
    ma_cnt += temp_ma.num_sen *2;
    memcpy(temp_ma.scene, &ptr[ma_cnt], temp_ma.num_scene * sizeof(SCENE));
    ma_cnt += temp_ma.num_scene * sizeof(SCENE);
    temp_ma.groupID = ptr[ma_cnt++];
    
 
    printf("curr ma = %d\n", current_ma_profile);
    if (current_ma_profile > 0) {
        ind = current_ma_profile;
        for(i=0; i< current_ma_profile; i++) {
            if (my_ma_profile[i].groupID == temp_ma.groupID) {
                // change MA profile here
                ind = i;
                printf("Find! grpId = %d\n", temp_ma.groupID);
                break;
            }
        }        
    }
    else
        ind = current_ma_profile;

    printf("master[%d]\n", ind);
    
    // memcpy(&my_ma_profile[current_ma_profile], rx_data[ret].msg, rx_data[ret].len);    
    my_ma_profile[ind].num_scene = temp_ma.num_scene;
    my_ma_profile[ind].num_act = temp_ma.num_act;
    memcpy(my_ma_profile[ind].act_addr, temp_ma.act_addr, temp_ma.num_act * 2); // htw ??1
    my_ma_profile[ind].num_sen = temp_ma.num_sen;
    memcpy(my_ma_profile[ind].sen_addr, temp_ma.sen_addr, temp_ma.num_sen * 2); // htw ??2
    memcpy(my_ma_profile[ind].scene, temp_ma.scene, temp_ma.num_scene * sizeof(SCENE));
    my_ma_profile[ind].groupID = temp_ma.groupID;

    printf("MA: num_scene: %d, num_act: %d, num_sen: %d, grpID: %d\n", temp_ma.num_scene, temp_ma.num_act, temp_ma.num_sen, temp_ma.groupID);

    if (ind == current_ma_profile)
        current_ma_profile++; 
    return;
}


void do_decision_making(UINT8* ptr) //for master act
{
    
    UINT8 i;
    UINT8 ind;
    UINT8 ma_cnt=0;
    UINT8 sceneid;
    //MASTER_ACT_PROFILE temp_ma;
    //save_sensor_value()
    //sceneid = select_scene();
    // find groupid
   
    UINT8 groupID = 1;
    char *msg;
    ERROR_T rsp_error;
    char text_addr[IP6_NTOP_BUF_SZ];
    TX_PACKET join_tx;
    
        
    join_tx.expired_time = 0;
    join_tx.msgID = ID_SCENE_INFO;
    join_tx.port = wsan_port;
    //memcpy(join_tx.msg, &scene_info_pkt, sizeof(REPORT_JOIN_PKT)); // htw
    join_tx.msg_len = sizeof(REPORT_JOIN_PKT); // htw
    join_tx.ack_time = 1000; // msec

    /* printf("To : %d, %s, %s, %d\n", */
    /*        join_tx.msg_len, report_join_pkt.locL, locL, strlen(locL)); */
    /* printf("port = %d\n", join_tx.port); */
    
    printf("curr ma = %d\n", current_ma_profile);
    if (current_ma_profile > 0) {
        ind = current_ma_profile;
        for(i=0; i< current_ma_profile; i++) {
            if (my_ma_profile[i].groupID == groupID) {
                // change MA profile here
                ind = i;
                printf("Find! grpId = %d\n", groupID);
                break;
            }
        }        
    }
    else
        ind = current_ma_profile;
    
    for (i=0;i<my_ma_profile[ind].num_act;i++)
    {
       join_tx.dest = my_ma_profile[ind].act_addr[i]; // htw 3
       add_to_list(join_tx);
    }


}


void save_act_profile(UINT8* ptr)
{
    UINT8 ind, found;
    UINT8 i, j;
    // TODO: ?? clear? index pointer
    ind = 1; // clear msgID 
    printf("[SAVE_ACT_Profile] curr#scene=[%d]", my_act_profile.num_scene);      //HH add 20140204        

    //initial
    if (my_act_profile.num_scene == 0) {
        my_act_profile.num_scene = ptr[ind++]; //scene Number, ex. #1->save [0] //HH mod 20140204
        printf(" --> initial [%d] scene adding\n", my_act_profile.num_scene);
        for (i=0; i< my_act_profile.num_scene; i++) {
            my_act_profile.sceneID[i] = ptr[ind++]; //scene ID //HH mod 20140204
            my_act_profile.control[i] = ptr[ind++]; // scene Control //HH mod 20140204
            printf(" Init add ID[%d],CON[%x]\n", my_act_profile.sceneID[i], my_act_profile.control[i]);  //HH add 20140204        
        }
        return;
    }

    printf(" --> not initial [%d] scene adding\n", ptr[1]);        //HH add 20140204        
    // TEST: if ACT have a same scene, change the control //HH mod 20140204
    for (j=2; j< ptr[1]; j+=2) { //ptr[2]=sceneID
        found = 0;
        for (i=0; i<my_act_profile.num_scene; i++) {
            if (my_act_profile.sceneID[i] == ptr[j]) {
                printf(" Change ID[%d],CON[%x->%x]\n", my_act_profile.sceneID[i], my_act_profile.control[i],ptr[j+1]);  //HH add 20140204
                my_act_profile.control[i] = ptr[j+1]; //HH mod 20140204
                found = 1;
            }
        }
        if (!found) {
            my_act_profile.sceneID[my_act_profile.num_scene] = ptr[j];
            my_act_profile.sceneID[my_act_profile.num_scene] = ptr[j+1];
            my_act_profile.num_scene++;
            printf(" Add New ID[%d],CON[%x]\n", ptr[i], ptr[j+1]);  //HH add 20140204            
        }
    }
    return;
}

/*
void doActuation(UINT8 P_ID, BOOL onoff, UINT8 av){
  
  IP6_ADDRESS lserver_addr = {{
      0xaa, 0xaa, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11 }};

  //printf("P_ID = %d, mycontrol = %d, onoff= %d av = %d!\n", P_ID, my_control.on, onoff, av);
  char msg_get[100];
  
  if(my_control.on){
    if(onoff){
    //  printf("Actuation status is still on !!!!\n");
      //return;
    }
    else {
      //TODO Actuation on 
    //  printf("Actuation status is changed off!!!!\n");
    }
  }
  else {
    if(onoff){
      //TODO Actuation off
   //   printf("Actuation status is changed on !!!!\n");
    }
    else {
   //   printf("Actuation status is still off !!!!\n");
      //return;
    }
  }
//  printf("do actuation ON!!!!\n");
  if(my_device.type == BLO){
    // Actuation onoff flag
    if(onoff) 
      irlcd_on_off_request(1);
    else 
      irlcd_on_off_request(0);
    //led_toggle(0);  // htw modify
  }
  else if(my_device.type == IR_LCD){
    // Actuation av valu
    Act_IR_Con[0] = 0x01;
    Act_IR_Con[1] = 0x8D;
    Act_IR_Con[3] = 0xC8;
    Act_IR_Con[4] = 0x00;
    
    if(av ==0){   
      Act_IR_Con[2] = 0x7C;
      Act_IR_Con[3] = 0x48;
    }
    else if(av == 1)
      Act_IR_Con[2] = 0x7C;
    else if(av ==15)
      Act_IR_Con[2] = 0xF4;
    else if(av ==16)
      Act_IR_Con[2] = 0x0C;
    else if(av ==17)
      Act_IR_Con[2] = 0x8C;
    else if(av ==18)
      Act_IR_Con[2] = 0xCC;
    else if(av ==19)
      Act_IR_Con[2] = 0xCC;
    else if(av ==20)
      Act_IR_Con[2] = 0x2C;
    else if(av ==21)
      Act_IR_Con[2] = 0xAC;
    else if(av ==22)
      Act_IR_Con[2] = 0x6C;
    else if(av ==23)
      Act_IR_Con[2] = 0xEC;
    else if(av ==24)
      Act_IR_Con[2] = 0x1C;
    else if(av ==25)
      Act_IR_Con[2] = 0x9C;
    else if(av ==26)
      Act_IR_Con[2] = 0x5C;
    else if(av ==27)
      Act_IR_Con[2] = 0xDC;
    else if(av ==28)
      Act_IR_Con[2] = 0x3C;
    else if(av ==29)
      Act_IR_Con[2] = 0xBC;
    else if(av ==30)
      Act_IR_Con[2] = 0x7C;
    else
      Act_IR_Con[3] = 0x7C;
    
    irlcd_ir_request(Act_IR_Con);
    printf("IR Control is%x %x %x %x %x !!!!\n", Act_IR_Con[0], Act_IR_Con[1], Act_IR_Con[2], Act_IR_Con[3], Act_IR_Con[4]);
  }

  my_control.on = onoff;
  //delay_ms(rand()%20+1);
  if (P_ID !=255) { 
    sprintf(msg_get, "POST /on aid=%u, ct=%u", P_ID, onoff);
    udp_sendto(wsan_port, &lserver_addr, wsan_port, msg_get, strlen(msg_get) + 1);
    printf("send to Server onoff = %u\n", onoff);
  }
}
*/

void do_actuation(UINT8 sceneID, UINT8 ind)
{

    // TODO: ind??? HH 20140204
    UINT8 i;
    UINT8 msg[3];
    for (i=0; i<my_act_profile.num_scene; i++) { //HH mod 20140204
        if (my_act_profile.sceneID[i] == sceneID) { 
            // TODO: do actuation based on my_act_profile.control[i] //HH mod 20140204
            printf("[DO_Actuation] ID[%d],CON[%x]\n", my_act_profile.sceneID[i], my_act_profile.control[i]);  //HH add 20140204            
            // TODO:check actuation type and control it!  //HH add 20140204            

		// TODO: temporally test
	    if(BLO_ONOFF_STATUS) 
	      irlcd_on_off_request(OFF);
	    else 
	      irlcd_on_off_request(ON);
	    led_toggle(2); 

			
            // TODO: Do dimming here            
            // Respond ID_RPT_ACT_INFO
            msg[0] = ID_RPT_ACT_INFO;
            msg[1] = sceneID;
            udp_sendto(def_inf, rx_queue[ind].dst_port, &rx_queue[ind].src_addr, rx_queue[ind].src_port, msg, 3);    
            break;
        }
    }
}

void save_sen_profile(UINT8* ptr) 
{
    UINT8 i;
    UINT8 ind, cnt;
    UINT16 recv_addr;
    cnt = 0;
    recv_addr = (UINT16)(ptr[1] << 8) + (UINT16)ptr[2];
    if (current_sen_profile >= 0) {
        ind = current_sen_profile;
        for(i=0; i< current_sen_profile+1; i++) {
            if (my_sen_profile[i].act_addr == recv_addr) {
                // change MA profile here
                ind = i;
                break;
            }
        }        
    }
    // add here
    // memcpy(&my_ma_profile[current_ma_profile], rx_data[ret].msg, rx_data[ret].len);    
    my_sen_profile[ind].act_addr = recv_addr;
    my_sen_profile[ind].sType = (UINT16)(ptr[3] << 8) + (UINT16)ptr[4];
    // TODO: Need to check the 'mode' before testing
    for (i=0; i<16; i++) {
        if ((my_sen_profile[ind].sType >> i) & 0x0001) cnt++;
    }
    memcpy(my_sen_profile[ind].mode, &ptr[5], 2*cnt);
    
    if (ind == current_sen_profile)
        current_sen_profile++; 
    return;
}

void process_rx(void *args) 
{
    int ret;
    //char msg[10];
    ret = 1;
    while (ret >=0) {
        ret = dequeue_rx();
        if (ret <0)
            break;
        printf("rxQ[%d]: msgID=%d\n", ret, rx_queue[ret].msgID);
        // TODO: process received msg with sending ACK
        // MA only
        /* 
        if (rx_queue[ret].msgID == ID_DEPLOY_MA_PROFILE) {
            // send Ack
            send_Ack(ret, ID_DEPLOY_MA_PROFILE_ACK, wsan_port_send);
            // TEST: save MA profile here
            // MASTER_ACT_PROFILE* ma_ptr = (MASTER_ACT_PROFILE*)rx_queue[ret].msg;
            save_ma_profile((UINT8*)rx_queue[ret].msg);
        }
       // Actuator only
        else 
        */
        if (rx_queue[ret].msgID == ID_DEPLOY_ACT_PROFILE) {
            // send Ack
            // TODO:TEST01             
            send_Ack(ret, ID_DEPLOY_ACT_PROFILE_ACK, wsan_port_send);
            // TODO:TEST02 TEST: save Act profile here            
            save_act_profile(rx_queue[ret].msg);
        }        
        else if (rx_queue[ret].msgID == ID_SCENE_INFO) {
            // TEST: Do actuation based on ACT_PROFILE
            // TODO:TEST03             
            do_actuation((UINT8)rx_queue[ret].msg[1], ret);
        }
        /* Sensors
        else if (rx_queue[ret].msgID == ID_DEPLOY_SEN_PROFILE) {
            // send Ack
            send_Ack(ret, ID_DEPLOY_SEN_PROFILE_ACK, wsan_port_send);
            // TEST: save Sen profile here
            // SENSOR_PROFILE* sen_ptr = (SENSOR_PROFILE*)rx_queue[ret].msg;
            save_sen_profile((UINT8*)rx_queue[ret].msg);
        }
        */
        /*
        // MA only
        else if (rx_queue[ret].msgID == ID_RPT_SENSING) {
            // send Ack
            send_Ack(ret, ID_RPT_SENSING_ACK, wsan_port);
            // TODO: process sensing data here
            do_decision_making((UINT8*)rx_queue[ret].msg);
        }
        // else if (rx_queue[ret].msgID == ID_RPT_ACT) {
        //     // send Ack 
        //     send_Ack(ret, ID_RPT_ACT_ACK); 
        //     // TODO: process actuation data here 
        // } 
        else if (rx_queue[ret].msgID == ID_MA_CTRL) {
            // send Ack
            send_Ack(ret, ID_MA_CTRL_ACK, wsan_port_send);
            // TODO: process actuation control (sending ID_SCENE_INFO to the ACTs)
        }
        */
        else if (rx_queue[ret].msgID = ID_ACT_CTRL) {
            // TODO: send ID_RPT_ACT here and process the contorl
        }
        else if (rx_queue[ret].msgID == ID_REQ_ACT_STAT) {
            // TODO: respond ID_RSP_ACT_STAT here
        }
        else if (rx_queue[ret].msgID == ID_REQ_SEN_VAL) {
            // TODO: respond ID_RSP_ACT_STAT here
        }
    }
}
///////////////////////////////////////////////////////////




void init_profile()
{
    memcpy(locL, "12-509", 6);
    locPx = 0x200;
    locPy = 0x200;
    aType = 0x11; // BLS/O HH fix. 20140204

    my_act_profile.num_scene = 0; //initialize ACT profile HH 20140204
}

void report_join()
{
    char *msg;
    ERROR_T rsp_error;
    char text_addr[IP6_NTOP_BUF_SZ];
    REPORT_JOIN_PKT report_join_pkt;
    TX_PACKET join_tx;

    //memcpy(locL, "12-509", 6);
    base_addr.s6_addr[14] = (UINT8)(node_id >>8);
    base_addr.s6_addr[15] = (UINT8)node_id;
    report_join_pkt.msgID = ID_RPT_JOIN;
    memcpy(&report_join_pkt.did, &base_addr, 16);
    //report_join_pkt.dType = TYPE_MASTER_ACT; // actuator
    report_join_pkt.dType = TYPE_ACT; // actuator HH 20140203    
    memcpy(report_join_pkt.locL, locL, 10);
    report_join_pkt.locPx = locPx;
    report_join_pkt.locPy = locPy;
    report_join_pkt.aType = aType;
    report_join_pkt.sType = 0x0000;
        
    join_tx.expired_time = 0;
    join_tx.dest = 0x0001; // for server (reserved)
    join_tx.msgID = ID_RPT_JOIN;
    join_tx.port = wsan_port_send;
    memcpy(join_tx.msg, &report_join_pkt, sizeof(REPORT_JOIN_PKT));
    join_tx.msg_len = sizeof(REPORT_JOIN_PKT);
    join_tx.ack_time = 1000; // msec

    /* printf("To : %d, %s, %s, %d\n", */
    /*        join_tx.msg_len, report_join_pkt.locL, locL, strlen(locL)); */
    /* printf("port = %d\n", join_tx.port); */
    add_to_list(join_tx);
     
}
////////////////////////////////////////////////////////////////
// JJ: 

void nara_uart_recv(UINT8 port, char ct)
{
    UINT8 c = (UINT8)ct;
    //printf("rcv: %x, ", c);
    if (serialIndex > 100)
    {
        commandStatus = COMMAND_READY;
        serialIndex = 0;
        return;
    }

    if (c == 0x40 && commandStatus == COMMAND_READY)
    {
        commandStatus = COMMAND_INCOMING_0;
    }
    // check msgID here
    else if (commandStatus == COMMAND_INCOMING_0)
    {
        msgID = c;
        commandStatus = COMMAND_INCOMING_1;
    }
    else if (commandStatus == COMMAND_INCOMING_1)
    {
        serialLength = c;
        commandStatus = COMMAND_INCOMING;
    }
/*
    else if (c == 0xE1 && commandStatus == COMMAND_INCOMING)
    {
        if (serialIndex < serialLength)
        {
            serialBuffer[serialIndex] = c;
            serialIndex++;
        }
        else
        {
            commandStatus = COMMAND_STARTED;
            nos_taskq_reg(nara_uart2rf, NULL);
        }
    }
*/
    else if (commandStatus == COMMAND_INCOMING)
    {
        // TODO: checksum check here?
        serialBuffer[serialIndex] = c;
        serialIndex++;
        //printf("index and len = %x %d\n", serialIndex, serialLength);
        if (serialIndex == serialLength + 1)
        {
            commandStatus = COMMAND_STARTED;
            //nos_taskq_reg(nara_uart2rf, NULL);
        }
        if (serialIndex > serialLength + 1)
        {
            commandStatus = COMMAND_READY;
            serialIndex = 0;
        }
    }
    else
    {
        // COMMAND_READY or COMMAND_STARTED and serial data is not C0.
        printf("err %x\n", c);
    }
}


////////////////////////////////////////////////////////////////

void rpl_status_report(void *args)
{
    char msg[200];
    static UINT16 sent = 0;

    if (ip6_is_attached(def_inf))
    {
        sent++;
        memset(msg, 0, 200);

        msg[0] = (sent >> 8);
        msg[1] = (sent & 0xff);
        rpl_get_root_address(def_inf, &root_addr);
        udp_sendto(def_inf, 1234, &root_addr, 1234, msg, 15);
        printf("Send! (%u)\n", sent);
    }
    else
    {
        printf("Network detached.\n");
    }
}

void listener(UINT8 in_inf,
              const IP6_ADDRESS *src_addr,
              UINT16 src_port,
              UINT16 dst_port,
              const UINT8 *msg, UINT16 len)
{
    char text_addr[IP6_NTOP_BUF_SZ];
    UINT16 addr;
    
    printf("Recv from [%s]:%u (%u byte) \n",
            ip6_ntop(src_addr, text_addr), src_port, len);
    
    // TODO: Enqueue and call the nos_taskq_reg
    RX_DATA recv;
    memcpy(&recv.src_addr, src_addr, 16);
    recv.src_port = src_port;
    recv.dst_port = dst_port;
    memcpy(recv.msg, msg, len);
    recv.len = len;
    recv.msgID = msg[0];

    // if received msg is an ack
    if (recv.msgID > 0x30) {
        // TODO: delete the matched tx_list 
        //memcpy(&addr, &src_addr->s6_addr[14], 2);
        addr = (UINT16)(src_addr->s6_addr[14] << 8) + (UINT16)src_addr->s6_addr[15];
        printf("isAck? %d, from %d\n", recv.msgID, addr);
        if (!memcmp(src_addr, &server_addr, 16)) {
            addr = 0x0001;
        }
        
        delete_from_list(addr, recv.msgID - 0x30); // Do we need to check the return value?
        //print_list();
    }
    else {
        if (!enqueue_rx(recv))
            printf("Error to process the received msg\n");

        nos_taskq_reg(process_rx, NULL);
    }
}

void ip6_state_changed(UINT8 ip6_inf, IP6_NODE_STATE state)
{
    printf("State changed to %d\n", state);
    if (ip6_is_attached(def_inf))
    {
        isConnected = 1;
        printf("RPL network connection established.\n");

        // send report_join here
        report_join();

        rpl_get_root_address(def_inf, &root_addr);
        led_on(1);
        if (send_timer_id < 0)
        {
            send_timer_id = user_timer_create_sec(rpl_status_report, NULL, 50,
                                                  USER_TIMER_PERIODIC);
            printf("send_timer_id:%d\n", send_timer_id);
        }
    }
}

UINT16 keep_cnt=0;
void keep_alive(void)
{
    printf("keep: %d\n", keep_cnt++);
}

void button_cb(UINT8 id)
{
    char msg[100];
    printf("\n\rButton (ID: %u) is pressed!\n\r", id);
    if(BLO_ONOFF_STATUS) 
      irlcd_on_off_request(OFF);
    else 
      irlcd_on_off_request(ON);
    led_toggle(2); 

    
    switch(id) {
      case 1:
/*      // Event emul.
      sprintf(msg, "Button 1");
      //printf("%s\n", msg);
      udp_sendto(wsan_port, &server_addr, wsan_port, msg, strlen(msg) + 1);

      led_toggle(3);
*/      
        break;
      case 2:
      
        break;
      case 3:
/*         my_act_profile[cur_act_profile].num_lval=1;
         my_act_profile[cur_act_profile].st[0]=4;
         my_act_profile[cur_act_profile].ct[0]=0;
         my_act_profile[cur_act_profile].cv1[0]=26;
         my_act_profile[cur_act_profile].cv2[0]=28;
         my_act_profile[cur_act_profile].src[cur_act_profile]=cur_act_profile+9;
         my_act_profile[cur_act_profile].st2[0]=4;
         my_act_profile[cur_act_profile].sv[0]=25;
         my_act_profile[cur_act_profile].cf[0]=0;
         my_act_profile[cur_act_profile].tr[0]=50;
         my_act_profile[cur_act_profile].num_neighbor=cur_act_profile+1;

         printf("\n\r# of profile %u\n\r",cur_act_profile);
         printf("\n\rst=%u ct=%u cv1=%u\n\r", my_act_profile[cur_act_profile].st[0], my_act_profile[cur_act_profile].ct[0], my_act_profile[cur_act_profile].cv1[0]);
         printf("\n\rcv2=%u \n\r",my_act_profile[cur_act_profile].cv2[0]); 
         printf("\n\rcf=%u nn=%u \n\r", my_act_profile[cur_act_profile].cf[0], my_act_profile[cur_act_profile].num_neighbor);
         if (cur_act_profile >= MAX_ACTUATOR_PROFILE-1){
          printf("This profile is more than MAX_ACTUATOR_PROFILE profileidx=0 " );
           cur_act_profile =0;   
         }
         else {
           cur_act_profile++;
         }
*/         
        break;
      case 4:
/*         printf("\n\rcur act profile %u", 0);
         printf("\n\rcur temp %u", my_act_profile[0].sv[0]);
         printf("up to %u =\n\r", (my_act_profile[0].sv[0])+1);
         store_sensing_data(9, 4, (my_act_profile[0].sv[0])+1);
*/
        break;
      default:
        break;
    }
}

int main(void)
{
    nos_init();
    led_on(0);

    printf("\n*** NanoQplus ***\n");

    // JJ: set channel and modulation
    rf212_set_mode(0, RF212_OQPSK_250KBPS_SIN);
    rf212_set_channel(0, 1);

    // Initialize the LoWPAN interface.
    node_ext_id[6] = node_id >> 8;
    node_ext_id[7] = node_id & 0xff;
    def_inf = lowpan_init(0, 1, 0xffff, node_ext_id, 30, 1);
    ip6_set_state_notifier(def_inf, ip6_state_changed);
    rpl_init(def_inf);
    ip6_init(def_inf);
    
    printf("ACT. Node Id = %d\n", node_id);
    init_profile();
    //irlcd_init();  // HH add 20140204 irlcd_init for metering, already in the nos_init 20140205

#ifdef BUTTON_M // HH add 20140205
    button_set_callback(button_cb); //for test use ONLY!
    printf("Button Enabled.\n");
#endif

    
// test
    
    /* struct tx_list *ptr = NULL; */
    /* TX_PACKET temp_list[10]; */
    /* int i, ret; */
    /* /\* for (i=0; i< 10; i++) { *\/ */
    /* /\*     temp_list[i].expired_time = 1; *\/ */
    /* /\*     temp_list[i].dest = i+1; *\/ */
    /* /\*     temp_list[i].msgID = i+2; *\/ */
    /* /\*     temp_list[i].port = 11; *\/ */
    /* /\*     temp_list[i].msg[5] = "test"; *\/ */
    /* /\*     temp_list[i].msg_len = 5; *\/ */
    /* /\*     temp_list[i].ack_time = 10 + 10*i; *\/ */
    /* /\*     add_to_list(temp_list[i]); *\/ */
    /* /\* } *\/ */
    /* temp_list[0].expired_time = 10; */
    /* temp_list[0].dest = 0x0001; */
    /* temp_list[0].msgID = 0x01; */
    /* temp_list[0].port = 1235; */
    /* temp_list[0].msg[5] = "test"; */
    /* temp_list[0].msg_len = 5; */
    /* temp_list[0].ack_time = 1000; */
    /* add_to_list(temp_list[i]); */

    /* print_list(); */

    /* ptr = search_in_list(0x0001, 0x01, NULL); */
    /* if (NULL == ptr) */
    /*     printf("search failed\n"); */
    /* else */
    /*     printf("searched %d and %d\n", ptr->val.dest, ptr->val.msgID); */

    /* ret = delete_from_list(0x0001, 0x01); */

    
    /* print_list(); */

    
    udp_set_listen(wsan_port, listener);

    // process_tx start here
    user_timer_create_ms(process_tx, NULL, 40, USER_TIMER_PERIODIC);
    user_timer_create_sec(keep_alive, NULL, 2, USER_TIMER_PERIODIC);

    sched_start();
    return 0;
}
