// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * HTTPS server based on CyaSSL library.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2010.02.01
 */

#include "nos.h"

UINT8 eui64[] = {0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x0a};
UINT8 eui64dst[] = {0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x01};
UINT8 pre[]={0xFD,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

const static char html_hdr[]="HTTP/1.1 200 OK\r\nContent-type: text/html\r\n\r\n";
const static char http_index_head[] = "<html><head><title>Congrats!</title></head>";
const static char http_index_title[] = "<body><h1>Welcome to nanoQplus HTTP server!</h1>";
const static char http_index_main[] = "<p>This is a test page, served by NanoQplus IPv6";
const static char http_index_end[] ="</body></html>";

UINT8 task_conn_id[8];
UINT8 g_cli[8];

UINT8 pp;

void task_conn(void *args)
{
	
    char buf[130];
    UINT8 i;
    UINT8 k;

    i=pp;

    printf(" !!!!!!!! thread k=%d \n\n",i);

    while(1)
    {
        k = tcp_recv(g_cli[i], buf, 130, 0);

        if (k >= 5 &&
            memcmp(buf, "GET /", 5) == 0)
				{
            tcp_send(g_cli[i], (void *) html_hdr, sizeof(html_hdr) - 1);
            tcp_send(g_cli[i], (void *) http_index_head, sizeof(http_index_head) - 1);
            tcp_send(g_cli[i], (void *) http_index_title, sizeof(http_index_title) - 1);
            tcp_send(g_cli[i], (void *) http_index_main, sizeof(http_index_main) - 1);
            tcp_send(g_cli[i], (void *) http_index_end, sizeof(http_index_end) - 1);
            
            thread_sleep_sec(1);
            tcp_close(g_cli[i]);

            thread_sleep_sec(4);
            task_conn_id[i]=255;

            return;			
				}
    }
}

void task_server(void *args)
{
    UINT8 k;
    int tc_idx;
    int tc_cli;
    UINT16 cli_port;

    IP6_ADDRESS src6,dst6;
    memcpy(src6.s6_addr,pre,8);
    memcpy(&src6.s6_addr[8],eui64,8); 

    for(k=0;k<8;k++) task_conn_id[k]=255;

    thread_sleep_sec(20);

    tc_idx = tcp_bind(src6,80);	
    if(tc_idx==ERROR_FAIL) printf("\n no more connection %d\n",tc_idx);
    else
    {
        tcp_listen(tc_idx);

        while(1)
        {
            for(k=0;k<8;k++) {if(task_conn_id[k]==255) break;}
            if(k!=8)
            {
                printf(" ---------------ww\n");
                g_cli[k]=tcp_accept(tc_idx,&dst6, &cli_port);	// block

                if(g_cli[k]==FALSE) printf("\n accept failed\n");
                else 
                {
                    pp=k;
                    printf("\n accepted k=%d tc idx=%d cliport=%u\n",k,g_cli[k],cli_port);
                    task_conn_id[k]=thread_create(task_conn, NULL, 400, PRIORITY_NORMAL);  
                    printf(" ccc = %d\n", task_conn_id[k]);
                }

            }
            else thread_sleep_sec(2);
        }
    }

}

int main(void) 
{
    UINT8 task1_id;

    uint32_t  u32_var;
    int tmp;

    nos_init();
    led_on(1);
    printf("\n*** Nano OS ***\n");


    u32_var = 0xa;
    tmp = 16 << (u32_var & 0x07);
    printf("tmp: %d\n", tmp); // "tmp: 16" is printed instead of "tmp: 64"


    // Init lowpan
    lowpan_init(0,0x0001, 0xffff, eui64, 10, 1);
    ip6_init(0);

    tcp_init();

    printf("tcp init\n");


    task1_id = thread_create(task_server, NULL, 300, PRIORITY_NORMAL);  
   

    sched_start();
    return 0;
} // main
