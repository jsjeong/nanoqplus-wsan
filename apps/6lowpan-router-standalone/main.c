// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * 6LoWPAN router (standalone RPL root)
 * 
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 12. 23.
 */

#include "nos.h"

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 1;
#endif

UINT8 lowpan_inf = 255;

UINT16 rxcount = 0;

extern struct ip6_interface ip6_nic[];

void listener(UINT8 in_inf,
              const IP6_ADDRESS *src_addr,
              UINT16 src_port,
              UINT16 dst_port,
              const UINT8 *msg,
              UINT16 len)
{
    UINT16 i;
    char src_addr_text[IP6_NTOP_BUF_SZ];
    
    printf("[%s:%u] ", ip6_ntop(src_addr, src_addr_text), src_port);

    // Just print the message in hexadecimal format.
    for (i = 0; i < len; i++)
    {
        printf("%02X ", msg[i]);
    }
    printf("\n");
}

int main(void)
{
    IP6_ADDRESS my_addr = {{0xfd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0x00, 0x00, 0x00, 0xff, 0xfe, 0x00, 0x00, 0x01 }};
    nos_init();
    led_on(0);
    printf("\n*** NanoQplus ***\n");

    my_addr.s6_addr[14] = node_id >> 8;
    my_addr.s6_addr[15] = node_id & 0xff;
    
    lowpan_inf = lowpan_init(DEFAULT, 0x01, node_id, &my_addr.s6_addr[8], 10, 1);
    ip6_init(lowpan_inf);
    rpl_root_init(lowpan_inf,
                  1,
                  &my_addr,
                  RPL_OCP_OF0,
                  RPL_MOP_NON_STORING,
                  1,
                  0xFFFF,
                  0xFF,
                  50);
    udp_set_listen(1234, listener);
    
    sched_start();
    return 0;
}
