/*
 * Automatically generated C config: don't edit
 * Nano OS kernel version: 
 * Tue Feb  4 11:31:48 2014
 */
#define AUTOCONF_INCLUDED

/*
 * Platform: Ubee430
 */
#define CONFIG_MCU_NAME "msp430f1611"
#undef MSP430F1611_UART0_M
#undef MSP430F1611_UART1_BR_500000
#define MSP430F1611_UART1_M 1
#undef MSP430F1611_UART1_BR_9600
#undef MSP430F1611_UART1_BR_19200
#undef MSP430F1611_UART1_BR_38400
#undef MSP430F1611_UART1_BR_57600
#define MSP430F1611_UART1_BR_115200 1
#undef MSP430F1611_UART1_BR_230400
#undef MSP430F1611_UART1_BR_380400
#undef MSP430F1611_UART1_BR_460800
#undef MSP430F1611_UART1_BR_921600
#undef MSP430F1611_SPI0_M
#undef TEMPERATURE_DIODE_M
#define CONFIG_PLATFORM_NAME "ubee430"
#define UART_M 1
#undef UART_INPUT_M
#define DEFAULT_STDIO 1
#define UBEE430_STDIO_UART1 1
#define UBEE430_UART1_M 1
#undef UBEE430_UART1_BR_9600
#undef UBEE430_UART1_BR_19200
#undef UBEE430_UART1_BR_38400
#undef UBEE430_UART1_BR_57600
#define UBEE430_UART1_BR_115200 1
#undef UBEE430_UART1_BR_230400
#undef UBEE430_UART1_BR_380400
#undef UBEE430_UART1_BR_460800
#undef UBEE430_UART1_BR_500000
#undef UBEE430_UART1_BR_921600
#define LED_M 1
#undef USER_SWITCH_M
#define CC2420_M 1
#define CC2420_INIT_CHANNEL_11 1
#undef CC2420_INIT_CHANNEL_12
#undef CC2420_INIT_CHANNEL_13
#undef CC2420_INIT_CHANNEL_14
#undef CC2420_INIT_CHANNEL_15
#undef CC2420_INIT_CHANNEL_16
#undef CC2420_INIT_CHANNEL_17
#undef CC2420_INIT_CHANNEL_18
#undef CC2420_INIT_CHANNEL_19
#undef CC2420_INIT_CHANNEL_20
#undef CC2420_INIT_CHANNEL_21
#undef CC2420_INIT_CHANNEL_22
#undef CC2420_INIT_CHANNEL_23
#undef CC2420_INIT_CHANNEL_24
#undef CC2420_INIT_CHANNEL_25
#undef CC2420_INIT_CHANNEL_26
#undef CC2420_INLINE_SECURITY_M
#undef SHTXX_M
#undef SENSOR_LIGHT_M
#undef SENSOR_PIR_M
#undef SENSOR_DOOR_OPEN_CHECK_M
#undef UBEE430_M25P80
#undef GENERIC_ADC_SENSOR_M
#undef GENERIC_ADC_GAS_SENSOR_M
#undef GENERIC_ADC_LIGHT_SENSOR_M
#undef GENERIC_ADC_MIC_SENSOR_M
#undef GENERIC_ADC_PIR_SENSOR_M
#undef PLATFORM_SPECIFIC_TEMPERATURE_SENSOR_M
#undef PLATFORM_SPECIFIC_LIGHT_SENSOR_M
#undef STORAGE_M
#undef IEEE_802_15_4_DEV_SECURITY_ACCELERATOR_M
#undef IEEE_802_15_4_DEV_FAST_READ

/*
 * Kernel
 */
#define KERNEL_M 1
#define ENABLE_SCHEDULING 1
#undef SCHED_PERIOD_5
#define SCHED_PERIOD_10 1
#undef SCHED_PERIOD_32
#undef SCHED_PERIOD_100
#undef USER_TIMER_M
#undef MSGQ_M
#define TASKQ_LEN_8 1
#undef TASKQ_LEN_16
#undef TASKQ_LEN_32
#undef TASKQ_LEN_64
#undef TASKQ_LEN_128
#define THREAD_M 1
#undef THREAD_EXT_M
#undef SEM_M

/*
 * Library
 */
#undef DEBUG_M
#undef LIB_UTC_CLOCK
#undef LIB_LPP

/*
 * Network Protocol
 */
#define IEEE_802_15_4 1

/*
 * IEEE 802.15.4 options
 */
#undef IEEE_802_15_4_FFD
#undef IEEE_802_15_4_INDIRECT
#undef IEEE_802_15_4_RXADDR_LIMIT_M
#define NANO_MAC_M 1

/*
 * NanoMAC Options
 */
#define CONFIG_NANO_MAC_TX_RETRIAL 3
#undef NANO_MAC_LBT
#undef LPP_MAC
#define MESH_ROUTING_M 1
#define RENO_M 1
#undef TENO_M
#undef ZIGBEE_IP
#undef LOWPAN_M
#undef PPP_M
#undef EAP
