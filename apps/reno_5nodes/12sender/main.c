/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "nos.h"

//====== Change these  ==========
#define SHORT_ADDR	12
#define PAN_ADDR	312
#define TX_SLEEP_PERIOD	200	//msec
//==========================

UINT16 src_addr;
UINT8 length;
UINT8 port;
UINT8 rx_data[NWK_MAX_PAYLOAD_SIZE];	// we need just 1byte for this app.

void led_control(UINT8 rx_char)
{
    switch (rx_char)
    {
        case '1':
            led_toggle(1);
            break;
        case '2':
            led_toggle(2);
            break;
        case '3':
            led_toggle(3);
            break;
        default:
            led_off(1);
            led_off(2);
            led_off(3);
            break;
    }
}


// TX thread : sends "3" to node 31 periodically
void task1(void* args)
{
    UINT8 tx_data;
    tx_data='3';

    while (TRUE)
    {
        nwk_tx(31, 1, 1, &tx_data);	// dest, port, length, data_ptr
        thread_sleep_ms(TX_SLEEP_PERIOD);
    }
}

//Receives string and toggles LEDs.
void rx_callback(void)
{
    nwk_rx(&src_addr, &port, &length, rx_data);
    printf("RX from %u: %u\n",src_addr, rx_data[0]);
    led_control(rx_data[0]);
}

int main(void) 
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    nwk_init(PAN_ADDR, SHORT_ADDR, rx_callback);
    printf("PAN address: %u\nShort address : %u\n", PAN_ADDR, SHORT_ADDR);

    thread_create(task1, NULL, 0, PRIORITY_NORMAL);
    sched_start();
    return 0;
} // main
