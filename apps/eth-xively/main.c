// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Xively Connected App
 *
 * @author Junkeun Song (ETRI)
 * @date 2013. .
 */

#include "nos.h"

// Fill the api_key and feed_id
char* api_key="";
unsigned int feed_id=0;

// datastream ID
char* ds1_id="light";


UINT8 pre[]={0x20,0x01,0x0d,0xb8,0xff,0xff,0x00,0x02};
UINT8 mac48[] = { 'E', 'T', 'R', 'I', 0, 0 };


UINT8 task_feed_update_id;

UINT8 def_inf = 255;

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 10;
#endif

void ip6_state_changed(UINT8 ip6_inf, IP6_NODE_STATE state)
{
    printf("IPv6 iface %u: State changed to %d\n", ip6_inf, state);
}

void heartbeat(void *args)
{
    led_toggle(1);
}

void uart_rx_handler(UINT8 port, char c)
{
    IP6_ADDRESS ipv6_addr;
    char addrtext[IP6_NTOP_BUF_SZ];

    if (c == '1')
    {
	    ip6_interface_get_addr(IP6_ADDRTYPE_GLOBAL, def_inf, &ipv6_addr);

		printf("\n----------------------------------\n");
		printf("Interface index = %d\n",def_inf);
		printf(" >> ip address= %s\n", ip6_ntop(&ipv6_addr, addrtext));
		printf(" >> mac address = %x:%x:%x:%x:%x:%x\n",mac48[0],mac48[1],mac48[2],mac48[3],mac48[4],mac48[5]);
		printf("----------------------------------\n\n");
    }
    else if (c == '2')
    {
    }
    else
    {
        printf("=== MENU ===\n");
        printf("(1) 'print interface info(mac/ ip address)\n");
        printf("(2) 'print tx/rx status\n");
        printf("\n");
    }
}



void task_feed_update(void *args)
{
    // create the xi library context
    xi_context_t* xi_context
        = xi_create_context(
                  XI_HTTP, api_key, feed_id );

	if(xi_context ==0)
	{
		printf(" XI_CREATE_CONTEXT FAILED\n Stopped\n");
		return;
	}
	printf("xi_context=%p\n", xi_context);

	// create feed
    xi_feed_t *f;
    f=(xi_feed_t*)malloc(sizeof( xi_feed_t ));
    memset( f, 0, sizeof( xi_feed_t ) );
	
    // set datastream count
    f->feed_id           = feed_id;
    f->datastream_count  = 1;	// update 1 pairs

    // get the datastream pointer
    xi_datastream_t* d = &f->datastreams[ 0 ];
	d->datapoint_count=1;

    int size = sizeof( d->datastream_id );
    int s = xi_str_copy_untiln( d->datastream_id, size, ds1_id, '\0' );

    if( s >= size )
    {
         printf( "datastream id too long: %s\n", ds1_id);
    }
		

	while(1)
	{
		 // get the datpoint counter
	     xi_datapoint_t* p = &d->datapoints[ 0 ];

		// set the datapoint
	     xi_set_value_i32( p, 1000 );	// sensing value


		 // update feed
		 xi_feed_update( xi_context, f);
	
		thread_sleep_sec(60);
	}


}


int main(void)
{
   

	nos_init();
    led_on(0);

    printf("\n*** NanoQplus ***\n");

    uart_set_getc_callback(STDIO, uart_rx_handler);
    enable_uart_rx_intr(STDIO);

    // Initialize an Ethernet interface.
    mac48[0] &= ~(1 << 0); //unicast
    mac48[0] |= (1 << 1);  //local
    mac48[4] = node_id >> 8;
    mac48[5] = node_id & 0xff;
    def_inf = ether6_init(0, mac48, 10, 1);
    ip6_set_state_notifier(def_inf, ip6_state_changed);
    ip6_init(def_inf);



	//thread_create(cli, NULL, 100, PRIORITY_NORMAL);
	user_timer_create_ms(heartbeat, NULL, 500, TRUE);


	
    task_feed_update_id=thread_create(task_feed_update, 0, 800, PRIORITY_NORMAL);
	
	sched_start();
    
    return 0;
}
