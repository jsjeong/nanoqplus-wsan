// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Web Sense Application
 *
 * @author Junkeun Song (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. .
 */

#include "nos.h"
#include "light-sense-response.h"

UINT8 eui64[] = {0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x0a};
UINT8 eui64dst[] = {0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x01};
UINT8 pre[]={0xFD,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

static char html_hdr[]="HTTP/1.1 200 OK\r\nContent-type: text/html\r\n\r\n";
const static char http_index_head[] = "<html><head><title>Congrats!</title></head>";
const static char http_index_title[] = "<body><h1>NanoQplus WebService!</h1><p>";
const static char http_index_main[] = "This page is served by NanoQplus IPv6";
const static char http_index_end[] ="</body></html>";

const static char http_index_main_ac1[] = "LED ON";
const static char http_index_main_ac0[] = "LED OFF";
const static char http_index_main_ac_err[] = "Invalid value!";

const static char http_index_main_help[] = "/ - TestPage<br>/?ac=[v] - 1:led on / 0:led off";

const static char html_404[]="HTTP/1.0 404 Not Found";
	
char sense_string[100];
extern const char index_html[];

UINT8 task_conn_id;
UINT8 g_cli[8];

char rxbuf[130];
UINT8 lowpan_inf = 255;

void task_conn(void *args)
{	
    UINT8 close_wait;
    UINT8 i;
    UINT8 k;

    close_wait = 0;
    while(1)
    {
        for(i = 0; i < 8; i++)
        {
            if (g_cli[i] < 100)
            {
                k = tcp_recv(g_cli[i], rxbuf, 130, MSG_DONTWAIT);
				
                // tcp쪽에서 연결이 끊긴 경우 -1이 넘어옴. 
                // 이런 경우 해당 소켓이 닫혔다고 생각하고 더이상 처리안함.
                if(k == 255) 
                {
                    g_cli[i] = 255;
                    continue;
                }

                rxbuf [k] = '\0';
                
                //printf(":::::::::  l=%d %s\n", k, rxbuf);

                if (k >= 5 &&
                    memcmp(&rxbuf[0], "GET /", 5) == 0)
                {
                    if (rxbuf[5] == ' ') //default: index.html
                    {
                        tcp_send(g_cli[i], (void*) html_hdr, sizeof(html_hdr) - 1);
                        tcp_send(g_cli[i], (void*) index_html, strlen(index_html));
                        
/*								tcp_send(g_cli[i],(void*)http_index_head,sizeof(http_index_head)-1);
                  tcp_send(g_cli[i],(void*)http_index_title,sizeof(http_index_title)-1);

                  if(rxbuf[5]==' ') tcp_send(g_cli[i],(void*)http_index_main,sizeof(http_index_main)-1);
                  if(rxbuf[5]=='/' && rxbuf[6]=='a' && rxbuf[7]=='c' && rxbuf[8]=='=')
                  {
									if (rxbuf[9]=='1') tcp_send(g_cli[i],(void*)http_index_main_ac1,sizeof(http_index_main_ac1)-1);
									else if(rxbuf[0]=='0') tcp_send(g_cli[i],(void*)http_index_main_ac0,sizeof(http_index_main_ac0)-1);
									else tcp_send(g_cli[i],(void*)http_index_main_ac_err,sizeof(http_index_main_ac_err)-1);
                  }
                  else tcp_send(g_cli[i],(void*)http_index_main_help,sizeof(http_index_main_help)-1);

                  tcp_send(g_cli[i],(void*)http_index_end,sizeof(http_index_end)-1);
*/
                    }
                    else if (memcmp(&rxbuf[5], "light", 5) == 0)		// light_sense.txt
                    {
                        get_light_sense_response(sense_string, sizeof(sense_string));
                        tcp_send(g_cli[i], (void*) sense_string, strlen(sense_string));
                    }
                    else
                    {
                        tcp_send(g_cli[i],(void*) html_404, strlen(html_404));
                    }
                    
                    g_cli[i] += 100;

                    if(close_wait==0) close_wait=30;		
						
                    printf("######### send finish k=%d close_wait=%d\n", i, close_wait);
                }
            }
            else if (g_cli[i] != 255 && close_wait % 10 == 1)
            {
                // flush the data recved.
				
                printf("## flush data recv from tcidx=%d\n", g_cli[i]-100);
                g_cli[i]-=100;
                while((k = tcp_recv(g_cli[i],
                                    (void*) rxbuf,
                                    130,
                                    MSG_DONTWAIT)));
                g_cli[i]+=100;
            }
        }

        thread_sleep_ms(20);

        if(close_wait > 0)
            close_wait--;

        if(close_wait == 1)
        {
            for(i = 0; i < 8; i++)
            {
                printf("++++ g_cli[%d]=%d\n", i, g_cli[i]);
                if (g_cli[i] >= 100 && g_cli[i] != 255)
                {
                    printf("## close tcidx=%d\n", g_cli[i] - 100);
                    g_cli[i] -= 100;
                    tcp_close(g_cli[i]);
                    g_cli[i] = 255;
                }
            }

        }
    }
}

void task_server(void *args)
{
    UINT8 k;
    int tc_idx;
    UINT16 cli_port;

    IP6_ADDRESS src6,dst6;
    memcpy(src6.s6_addr,pre,8);
    memcpy(&src6.s6_addr[8],eui64,8); 

    thread_sleep_sec(10);

    tc_idx=tcp_bind(src6,80);	
    if(tc_idx==ERROR_FAIL) printf("\n no more connection %d\n",tc_idx);
    else
    {
        tcp_listen(tc_idx);

        while(1)
        {
            k=0;
				  
            do
            {
                while(g_cli[k]!=255 && k<8) k++;
                if(k>=8)  thread_sleep_sec(1);
						 
                printf("\n\n########----------- ");
                printf("k=%d\n",k);
						 
            }while(k>=8);
				
//				printf(" ---------------ww\n");
            g_cli[k]=tcp_accept(tc_idx,&dst6, &cli_port);	// block

            if(g_cli[k]==FALSE) printf("\n accept failed\n");
            else 
            {
                printf("\n accepted k=%d tc idx=%d cliport=%u\n",k,g_cli[k],cli_port);
            }
				
            //thread_sleep_sec(1);
        }
    }

}

int main(void) 
{
    UINT8 task1_id;
    UINT8 k;
    uint32_t  u32_var;
    int tmp;

    nos_init();
    led_on(1);
    printf("\n*** Nano OS ***\n");

    // Init lowpan
    lowpan_inf = lowpan_init(0, 0x0001, 0xffff, eui64, 10, 1);
#ifdef RPL_M
    rpl_init(lowpan_inf);
#endif
    ip6_init(lowpan_inf);

    for (k = 0; k < 8; k++)
        g_cli[k]=255;

    task1_id = thread_create(task_server, NULL, 800, PRIORITY_NORMAL);
    task_conn_id=thread_create(task_conn, NULL, 800, PRIORITY_NORMAL);
   
    sched_start();
    return 0;
}
