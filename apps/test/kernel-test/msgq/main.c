/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 * @date 2006. 6. 1.
 * @brief Message queue test program.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "nos.h"

MSGQ mq;

void task1(void *args)
{
    UINT16 data = 0;

    while (1)
    {
        ++data;
        printf("\nTask1 : TX trying.....\n");

        if ( msgq_send_nb(mq, &data) )
        {
            printf("Task1 : TX %d succeeded\n", data);
        }
        else
        {
            printf("Task1 : TX failed because message queue is full. I'll sleep for 8 sec waiting for available message queue.\n");
            thread_sleep_sec(8); 
        }
        thread_sleep_ms(500);
    }
}

void task2(void *args)
{
    UINT16 data;

    while (1)
    {
        printf("\nTask2 : \tRX trying.....\n");
        if ( msgq_recv_nb(mq, &data) )
            printf("Task2 : \tRX %d succeeded\n", data);
        else
            printf("Task2 : \tRX failed because message queue is empty.\n");
        thread_sleep_ms(1200);
    }
}

int main (void)
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    printf("\n=== Message queue test program (non-blocking mode) ===\n");

    mq = nos_msgq_create(MSGQ_UINT16, 4); // create a msgq with an array of 4 integers (total 10 bytes)

    thread_create(task1, NULL, 0, PRIORITY_NORMAL);
    thread_create(task2, NULL, 0, PRIORITY_NORMAL);

    sched_start();

    return 0;
}
