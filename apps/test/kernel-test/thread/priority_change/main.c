/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file priority_change.c
 * @author Sangcheol Kim (ETRI)
 * @date 2006.09.25
 * @brief Thread Priority Change Test Program
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "nos.h"

UINT8 pri_jump_tid;

void do_loop(UINT8 id)
{
    UINT8 i,j,my_prior;
    for (i=1; i<15; ++i)
    {
        ENTER_CRITICAL();
        my_prior = get_thread_prior(get_thread_id());
        if (my_prior == PRIORITY_LOW)
        {
            printf("\n(LOW)Task%d is now working. - ", id);
        }
        else
        {
            printf("\n(NORMAL)Task%d is now working. - ", id);
        }
        for (j=0; j<i; ++j)
        {
            uart_puts(STDIO, "*");
        }
        EXIT_CRITICAL();
        delay_ms(100); // To slow down uart printf speed
    }
}


void task1(void *args)
{
    UINT8 my_id = get_thread_id();
    while(1)
    {
        do_loop(my_id);

        // change the priority of task3 into "NORMAL"
        ENTER_CRITICAL();
        printf("\n===Task%d : I will change the the priority of Task%d to \"PRIORITY_NORMAL\".\n", my_id, pri_jump_tid);
        thread_priority_change(pri_jump_tid, PRIORITY_NORMAL);
        EXIT_CRITICAL();
    }
}

void task2(void *args)
{
    UINT8 my_id = get_thread_id();
    while(1)
    {
        do_loop(my_id);
        // change the priority of task3 into "LOW"

        ENTER_CRITICAL();
        printf("\n===Task%d : I will change the the priority of Task%d to \"PRIORITY_LOW\".\n", my_id, pri_jump_tid);
        thread_priority_change(pri_jump_tid, PRIORITY_LOW);
        EXIT_CRITICAL();

        do_loop(my_id);

        ENTER_CRITICAL();
        printf("\n===Task%d : I'll sleep for a while.\n", my_id);
        thread_sleep_sec(5);
        EXIT_CRITICAL();

        printf("\n\n===Task%d : I've just awaken.", my_id);
    }
}

void task3(void *args)
{
    UINT8 my_id = get_thread_id();
    while(1)
    {
        do_loop(my_id);
    }
}

int main (void)
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    thread_create(task1, NULL, 0, PRIORITY_LOW);  
    thread_create(task2, NULL, 0, PRIORITY_NORMAL); 
    pri_jump_tid = thread_create(task3, NULL, 0, PRIORITY_NORMAL);

    sched_start();

    return 0;
}
