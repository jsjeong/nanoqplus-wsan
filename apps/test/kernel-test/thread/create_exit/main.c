// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 * @date 2007. 11. 14.
 * @brief "thread_create" and "thread_exit" test program.
 *   "thread_exit" is called by kernel automatically when
 *   it returns. User thread can be created up to 5 threads
 *   or 15 threads with thread_ext module.
 */

#include "nos.h"

void do_loop(UINT8 id)
{
    UINT8 i,j;
    for (i=1; i<10; ++i)
    {
        ENTER_CRITICAL();
        printf("\r\nTask%d is now working. - ", id);
        for (j=0; j<i; ++j)
        {
            putchar('*');
        }

        EXIT_CRITICAL();
        thread_sleep_ms(100);
    }
}

void new_task(void *args)
{
    UINT8 my_id = get_thread_id();
    UINT8 my_value = *((UINT8 *)args);

    ENTER_CRITICAL();
    printf("\r\n===A new Task%d has been created by Task%d.===", my_id, my_value);
    EXIT_CRITICAL();

    do_loop(my_id);

    ENTER_CRITICAL();
    printf("\r\n===Task%d has exited.===\n", my_id);
    EXIT_CRITICAL();
}

void task1(void *args)
{
    UINT8 my_id = get_thread_id();
    while (1)
    {
        do_loop(my_id);
        if (thread_create(new_task, &my_id, 0, PRIORITY_NORMAL) < 0)
        {
            ENTER_CRITICAL();
            printf("\r\n===Task%d has failed to create a new thread.===",my_id);
            EXIT_CRITICAL();
        }
    }
}

void task2(void *args)
{
    UINT8 my_id = get_thread_id();
    while (1)
    {
        do_loop(my_id);
        if (thread_create(new_task, &my_id, 0, PRIORITY_NORMAL) < 0)
        {
            ENTER_CRITICAL();
            printf("\r\n===Task%d has failed to create a new thread.===",my_id);
            EXIT_CRITICAL();
        }
    }
}

void task3(void *args)
{
    UINT8 my_id = get_thread_id();
    while (1)
    {
        do_loop(my_id);
        if (thread_create(new_task, &my_id, 0, PRIORITY_NORMAL) < 0)
        {
            ENTER_CRITICAL();
            printf("\r\n===Task%d has failed to create a new thread.===",my_id);
            EXIT_CRITICAL();
        }
    }
}

int main (void)
{
    nos_init();
    led_on(1);
    puts("\n\r*** Nano OS ***\n\r");

    thread_create(task1, NULL, 0, PRIORITY_NORMAL);  
    thread_create(task2, NULL, 0, PRIORITY_NORMAL); 
    thread_create(task3, NULL, 0, PRIORITY_NORMAL);

    sched_start();

    return 0;
}
