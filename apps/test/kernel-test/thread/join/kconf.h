/*
 * Automatically generated C config: don't edit
 * Nano OS kernel version: 
 * Fri Sep  7 19:45:30 2012
 */
#define AUTOCONF_INCLUDED

/*
 * Platforms
 */
#undef ATMEGA128
#undef ATMEGA1284P
#undef MSP430F1611
#undef S3FN41F
#undef EFM32GG330F1024
#define EFM32GG990F1024 1
#define CONFIG_MCU_NAME "efm32gg"
#undef MICAZ
#undef NANO24_XC
#undef NANO24
#undef OCX_Z2
#undef OCX_Z
#undef ZIGBEX
#undef YP128
#define EFM32GG_STK3700 1
#define CONFIG_PLATFORM_NAME "efm32gg-stk3700"

/*
 * Basic Functions
 */
#define UART_M 1
#define ADC_M 1
#undef TIMECHK_M
#undef ATMEGA128_UART_BR_9600
#undef ATMEGA128_UART_BR_19200
#undef ATMEGA128_UART_BR_38400
#undef ATMEGA128_UART_BR_57600
#undef ATMEGA128_UART_BR_115200
#undef ATMEGA128_UART_BR_230400
#undef ATMEGA128_UART_BR_250000
#undef ATMEGA128_UART_BR_500000
#undef ATMEGA128_UART_BR_1000000
#undef EFM32GG_USART0_FOR_UART_M
#undef EFM32GG_USART0_BR_9600
#undef EFM32GG_USART0_BR_19200
#undef EFM32GG_USART0_BR_38400
#undef EFM32GG_USART0_BR_57600
#undef EFM32GG_USART0_BR_115200
#undef EFM32GG_USART0_BR_230400
#undef EFM32GG_USART0_BR_380400
#undef EFM32GG_USART0_BR_460800
#undef EFM32GG_USART0_BR_921600
#undef EFM32GG_USART1_FOR_UART_M
#undef EFM32GG_USART2_FOR_UART_M
#undef EFM32GG_UART0_FOR_UART_M
#undef EFM32GG_UART1_FOR_UART_M
#define EFM32GG_LEUART0_FOR_UART_M 1
#define EFM32GG_LEUART0_BR_9600 1
#undef EFM32GG_LEUART0_BR_19200
#undef EFM32GG_LEUART0_BR_38400
#undef EFM32GG_LEUART0_BR_57600
#undef EFM32GG_LEUART0_BR_115200
#undef EFM32GG_LEUART0_BR_230400
#undef EFM32GG_LEUART0_BR_380400
#undef EFM32GG_LEUART0_BR_460800
#undef EFM32GG_LEUART0_BR_921600
#undef EFM32GG_LEUART1_FOR_UART_M
#define RTC_M 1
#define DMA_M 1
#define MCU_AUTO_SLEEP_M 1

/*
 * Platform Specific Devices and Functions
 */
#define LED_M 1
#undef NO_SENSOR
#undef MTS300_M
#define CONFIG_PART_NAME "EFM32GG990F1024"
#define CONFIG_EFM32_HFXO_FREQ 48000000
#define CONFIG_EFM32_LFXO_FREQ 32768
#undef STDIO_EFM32GG_STK3700_USART0
#undef STDIO_EFM32GG_STK3700_USART1
#undef STDIO_EFM32GG_STK3700_USART2
#define STDIO_EFM32GG_STK3700_LEUART0 1
#undef STDIO_EFM32GG_STK3700_LEUART1
#undef BUTTON_M
#undef EMLCD_M
#undef GENERIC_ADC_SENSOR_M
#undef GENERIC_ADC_GAS_SENSOR_M
#undef GENERIC_ADC_LIGHT_SENSOR_M
#undef GENERIC_ADC_MIC_SENSOR_M
#undef GENERIC_ADC_PIR_SENSOR_M
#undef PLATFORM_SPECIFIC_TEMPERATURE_SENSOR_M
#undef PLATFORM_SPECIFIC_LIGHT_SENSOR_M
#undef CC2420_M
#undef RF212_M
#undef CC112X_M
#undef MG2410_M
#undef SENSOR_SHTXX_M

/*
 * Kernel
 */
#define KERNEL_M 1
#define ENABLE_SCHEDULING 1
#undef SCHED_PERIOD_5
#define SCHED_PERIOD_10 1
#undef SCHED_PERIOD_32
#undef SCHED_PERIOD_100
#define THREAD_M 1
#undef THREAD_EXT_M
#undef SEM_M
#undef USER_TIMER_M
#undef MSGQ_M

/*
 * Library
 */
#undef DEBUG_M
#undef AES_M

/*
 * Network Protocol
 */
#undef L2_LAYER_M

/*
 * Storage System
 */
#undef STORAGE_M

/*
 * Statistics modules
 */
#undef HEAP_STATISTICS_M
#undef KERNEL_STATISTICS_M
