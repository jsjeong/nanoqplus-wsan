/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file thread_join.c
 * @author Sangcheol Kim (ETRI)
 * @date 2006.09.24
 * @brief Thread Joining Test Program
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "nos.h"

void do_loop(UINT8 id, UINT16 cnt)
{
    UINT8 i,j;
    for (i=1; i<cnt; ++i)
    {
        ENTER_CRITICAL();
        printf("\nTask%d is now working. - ", id);
        for (j=0; j<i; ++j)
        {
            uart_puts(STDIO, "*");
        }
        EXIT_CRITICAL();
        delay_ms(100); // To slow down uart printf speed
    }
}

void task2(void *args)
{
    do_loop(get_thread_id(), 40);
    printf("\n===Task%d : I am done. Bye~===", get_thread_id());
}

void task1(void *args)
{
    UINT8 child_thread_id;
    UINT8 my_id = get_thread_id();
    while (1)
    {
        printf("\n\n===Task%d : I'll make a child thread===", my_id);
        child_thread_id = thread_create(task2, NULL, 0, PRIORITY_NORMAL);
        do_loop(my_id, 10);
        printf("\n===Task%d : My work has been done. But I'll wait until Task%d is completed===" , my_id, child_thread_id);
        // wait until child thread completes.
        nos_thread_join(child_thread_id);
        printf("\n===Task%d : OK. Child task has been done.===", my_id);
        thread_sleep_sec(3);
    }
}

int main (void)
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***");

    thread_create(task1, NULL, 0, PRIORITY_NORMAL); 

    sched_start();

    return 0;
}
