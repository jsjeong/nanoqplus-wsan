/**
 * @file sleep_wakeup.c
 * @author Junkeun Song (ETRI), Sangcheol Kim (ETRI),
 *   Haeyong Kim (ETRI)
 * @date 2007.07.29
 * @brief "thread_sleep" and "thread_wakeup" test program.
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "nos.h"

void print_myid(UINT8 id, UINT8 sleep_sec)
{
    ENTER_CRITICAL();
    printf("Thread %d : I will sleep for %d sec(s).\n", id, sleep_sec);
    EXIT_CRITICAL();
}


void task2(void *args)
{
    while (1)
    {
        print_myid(2,5);
        thread_sleep_ms(5000);
    }
}

void task3(void *args)
{
    while (1)
    {
        print_myid(3,10);
        thread_sleep_sec(10);
    }
}

void task4(void *args)
{
    while (1)
    {
        print_myid(4,20);
        thread_sleep_sec(20);
    }
}

void task1(void *args)
{
    UINT8 i, thread2_id, thread3_id, thread4_id;
    thread2_id = thread_create(task2, NULL, 0, PRIORITY_NORMAL);
    thread3_id = thread_create(task3, NULL, 0, PRIORITY_NORMAL);
    thread4_id = thread_create(task4, NULL, 0, PRIORITY_NORMAL);
    while (1)
    {
        printf("\n==Thread sleep test for 20 secs==\n");
        for (i=0; i<20; ++i)
        {
            print_myid(1,1);
            thread_sleep_sec(1);
        }
        thread_sleep_sec(1);

        ENTER_CRITICAL();
        printf("\n==Thread wake up test==\n");
        printf("I am thread 1. I will wake up the other threads.\n");
        EXIT_CRITICAL();
        thread_sleep_sec(1);

        ENTER_CRITICAL();
        printf("Thread 1 : wake up thread 2.\n");
        EXIT_CRITICAL();
        thread_wakeup(thread2_id);
        thread_sleep_sec(1);

        ENTER_CRITICAL();
        printf("Thread 1 : wake up thread 3.\n");
        EXIT_CRITICAL();
        thread_wakeup(thread3_id);
        thread_sleep_sec(1);

        ENTER_CRITICAL();
        printf("Thread 1 : wake thread 4 up.\n");
        EXIT_CRITICAL();
        thread_wakeup(thread4_id);
        thread_sleep_sec(1);
    }
}


int main (void)
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    thread_create(task1, NULL, 0, PRIORITY_NORMAL);  

    sched_start();

    return 0;
}
