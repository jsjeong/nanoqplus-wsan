// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Task queue test program
 *
 * @author Sangcheol Kim (ETRI)
 * @date 2007. 11. 15.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "nos.h"

UINT8 cnt;

void print_hello(void *args)
{
    UINT8 i,j;
    printf("\nRegistered Task%d : I am running from TASK QUEUE, so I am the HIGHEST priority task.", cnt);
    for (i=0; i<10; i++) {
        ENTER_CRITICAL();
        printf("\nRegistered Task%d is now working. - ", cnt);
        for (j = 0; j < i; ++j)
        {
            puts("*");
        }
        EXIT_CRITICAL();
        delay_ms(100); // To slow down uart printf speed
    }
}

void do_loop(UINT8 id)
{
    UINT8 i,j;
    for (i = 1; i < 10; ++i)
    {
        ENTER_CRITICAL();
        printf("\nTask%d is now working. - ", id);
        for (j = 0; j < i; ++j)
        {
            puts("*");
        }
        EXIT_CRITICAL();
        delay_ms(100); // To slow down uart printf speed
    }
}


void task1(void *args)
{
    while (1)
    {
        printf("\nTASK1 : I am a thread with LOW priority.");
        do_loop(1);
        taskq_reg(print_hello, NULL);
        ++cnt;
    }
}

void task2(void *args)
{
    while (1)
    {
        printf("\nTASK2 : I am a thread with NORMAL priority.");
        do_loop(2);
        taskq_reg(print_hello, NULL);
        ++cnt;
        printf("\nTASK2 : I'll sleep for a while for LOW priority thread.");
        thread_sleep_sec(2);
    }
}

void task3(void *args)
{
    while (1)
    {
        printf("\nTASK3 : I am a thread with LOW priority.\r\n");
        do_loop(3);
        taskq_reg(print_hello, NULL);
        ++cnt;
    }
}

int main (void)
{
    nos_init();
    led_on(1);
    puts("\n\r*** Nano OS ***\n\r");
    cnt = 0;

    thread_create(task1, NULL, 0, PRIORITY_LOW);  
    thread_create(task2, NULL, 0, PRIORITY_NORMAL); 
    thread_create(task3, NULL, 0, PRIORITY_LOW); 

    sched_start();

    return 0;
}
