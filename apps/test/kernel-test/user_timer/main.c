// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * An application to test user timer functions.
 *
 * @author Sangcheol Kim (ETRI)
 * @date 2007. 7. 29.
 */

#include "nos.h"

UINT8 led_id = 1;

void oneshot2(void *arg)
{
    printf("\n\n===One-shot Timer2 : Hello.\n");
}

void oneshot1(void *arg)
{
    ENTER_CRITICAL();
    user_timer_create_sec(oneshot2, NULL, 5, USER_TIMER_ONE_SHOT); // after 5 sec, one time
    printf("\n\n===One-shot Timer1 : I am running now and \"One-shot Timer2\" has been created. It will runs after 5sec.\n");
    EXIT_CRITICAL();
}

void periodic_print(void *arg)
{
    printf("\n===This message will be shown every 5sec by periodic timer.");
}

void led_blink(void *arg)
{
    led_id++;
    led_off(led_id % 3);
    led_on((led_id + 1) % 3);
}

// Disable below codes when thread module is disabled in the kernel configuration.
#ifdef THREAD_M
void running(UINT8 id)
{
    ENTER_CRITICAL();
    printf("\nTask%d is now running.", id);
    EXIT_CRITICAL();
    thread_sleep_sec(1);
}

void task1(void *args)
{
    int i;
    while (1)
    {
        ENTER_CRITICAL();
        user_timer_create_sec(oneshot1, NULL, 2, USER_TIMER_ONE_SHOT); // after 2 sec, one time
        printf("\n\nTask1 : \"One-shot Timer1\" has been created. It will runs after 2sec.\n");
        EXIT_CRITICAL();
        for (i = 0; i < 12; ++i)
        {
            running(1);
        }
    }
}

void task2(void *args)
{
    while (1)
    {
        running(2);
    }
}
#endif //THREAD_M

int main(void)
{
    nos_init();
    led_on(1);
    printf("\n*** Nano OS ***\n");

#ifdef THREAD_M
    thread_create(task1, NULL, 0, PRIORITY_NORMAL);  
    thread_create(task2, NULL, 0, PRIORITY_NORMAL);
#endif

    user_timer_create_sec(periodic_print, NULL, 5, USER_TIMER_PERIODIC); // every 5 sec
    user_timer_create_ms(led_blink, NULL, 100, USER_TIMER_PERIODIC); // every 200 msec

    sched_start();

    return 0;
}
