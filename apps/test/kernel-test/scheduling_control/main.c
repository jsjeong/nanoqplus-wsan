/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file scheduling_control.c
 * @author Haeyong Kim (ETRI)
 * @date 2006.10.26
 * @brief An application to test various methods to
 * control scheduling.
 *
 * 1. "ENTER_CRITICAL" delays scheduling.
 * (A scheduling timer interrupt will be called right after
 * critical section.)
 * 2. "disable_sched" ignores scheduling.
 * (A scheduling interrupt is executed, but does not switch
 * context.)
 * 3. You can also use binary semaphore.
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "nos.h"

void do_loop(UINT8 id)
{
    UINT8 i,j;
    for (i=1; i<10; ++i)
    {
        ENTER_CRITICAL();
        printf("\nTask%d is now working. - ", id);
        for (j=0; j<i; ++j)
        {
            uart_puts(STDIO, "*");
        }
        EXIT_CRITICAL();
        delay_ms(100); // To slow down uart printf speed
    }
}

void task1(void *args)
{
    while (1)
    {
        disable_sched();
        do_loop(1);
        enable_sched();
        context_sw(); // force to context switch
    }
}

void task2(void *args)
{
    while (1)
    {
        disable_sched();
        do_loop(2);
        enable_sched();
        thread_sleep(1); // force to context switch
    }
}

void task3(void *args)
{
    while (1)
    {
        ENTER_CRITICAL();
        do_loop(3);
        EXIT_CRITICAL();
    }
}

void task4(void *args)
{
    while (1)
    {
        disable_sched();
        do_loop(4);
        enable_sched();
        thread_sleep_ms(100); // give a short time to be context switched swiftly before disabling context switch() again.
    }
}

int main (void)
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    thread_create(task1, NULL, 0, PRIORITY_NORMAL);  
    thread_create(task2, NULL, 0, PRIORITY_NORMAL); 
    thread_create(task3, NULL, 0, PRIORITY_NORMAL);
    thread_create(task4, NULL, 0, PRIORITY_NORMAL);

    sched_start();

    return 0;
}
