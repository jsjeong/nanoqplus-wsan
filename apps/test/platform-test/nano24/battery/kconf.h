/*
 * Automatically generated C config: don't edit
 * Nano OS kernel version: 
 * Mon Dec  5 16:33:24 2011
 */
#define AUTOCONF_INCLUDED

/*
 * Platforms
 */
#define ATMEGA128 1
#undef MSP430X1611
#define CONFIG_MCU_NAME "atmega128"
#undef MICAZ
#undef NANO24_XC
#define NANO24 1
#undef OCX_Z2
#undef OCX_Z
#undef ZIGBEX
#undef YP128
#define CONFIG_PLATFORM_NAME "nano24"
#undef UART_BR_9600
#undef UART_BR_19200
#undef UART_BR_38400
#undef UART_BR_57600
#define UART_BR_115200 1
#undef UART_BR_230400
#undef UART_BR_250000
#undef UART_BR_500000
#undef UART_BR_1000000
#undef UART_BR_380400
#undef UART_BR_460800
#undef UART_BR_921600

/*
 * Basic Functions
 */
#define LED_M 1
#define UART_M 1
#define ADC_M 1
#undef TIMECHK_M
#undef EEPROM_M

/*
 * Platform Specific Devices and Functions
 */
#undef GENERIC_ADC_SENSOR_M
#undef GENERIC_ADC_GAS_SENSOR_M
#undef GENERIC_ADC_LIGHT_SENSOR_M
#undef GENERIC_ADC_MIC_SENSOR_M
#undef PLATFORM_SPECIFIC_TEMPERATURE_SENSOR_M
#undef PLATFORM_SPECIFIC_LIGHT_SENSOR_M
#undef CC2420_M
#undef SENSOR_SHTXX_M
#undef SENSOR_TEMPERATURE_M
#undef SENSOR_GAS_M
#undef SENSOR_HUMIDITY_M
#undef SENSOR_LIGHT_M
#undef IEEE_802_15_4_DEV_M
#define BATTERY_VOLTAGE_MEASUREMENT_M 1
#define SENSOR_INTERNAL_VOLTAGE_M 1
#define OCTACOMM_ENV_SENSOR_M 1
#undef OCTACOMM_ULTRASONIC_SENSOR_M
#undef OCTACOMM_PIR_SENSOR_M
#undef OCTACOMM_RELAY_M
#undef OCTACOMM_TEMPERATURE_SENSOR_M
#undef OCTACOMM_GAS_SENSOR_M
#undef OCTACOMM_HUMIDITY_SENSOR_M
#undef OCTACOMM_LIGHT_SENSOR_M
#undef SENSOR_ULTRASONIC_M
#undef SENSOR_PIR_M
#undef RELAY_M

/*
 * Kernel
 */
#undef KERNEL_M

/*
 * Library
 */
#undef TRICKLE_M
#undef DEBUG_M

/*
 * Network Protocol
 */
#undef IEEE_802_15_4_M
#undef INET_M

/*
 * Storage System
 */
#undef STORAGE_M

/*
 * Statistics modules
 */
#undef HEAP_STATISTICS_M
