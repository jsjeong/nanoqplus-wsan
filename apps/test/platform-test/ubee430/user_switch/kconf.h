/*
 * Automatically generated C config: don't edit
 * Nano OS kernel version: 
 * Mon Feb 23 17:11:25 2009
 */
#define AUTOCONF_INCLUDED

/*
 * Platforms
 */
#undef ATMEGA128
#define MSP430X1611 1
#define CONFIG_MCU_NAME "msp430x1611"
#define UBEE430 1
#undef UBI_MSP430
#undef UBI_COIN
#undef TMOTE_SKY
#undef HMOTE2420
#define CONFIG_PLATFORM_NAME "ubee430"
#undef UART_BR_9600
#undef UART_BR_19200
#undef UART_BR_38400
#undef UART_BR_57600
#define UART_BR_115200 1
#undef UART_BR_230400
#undef UART_BR_250000
#undef UART_BR_500000
#undef UART_BR_1000000
#undef UART_BR_380400
#undef UART_BR_460800
#undef UART_BR_921600

/*
 * Basic Functions
 */
#define LED_M 1
#define UART_M 1
#undef ADC_M
#undef TIMECHK_M
#undef TEMPERATURE_DIODE_M

/*
 * Platform Specific Devices and Functions
 */
#undef SENSOR_LIGHT_M
#undef SENSOR_SHTXX_M
#undef SENSOR_HUMAN_DETECT_M
#undef SENSOR_DOOR_OPEN_CHECK_M
#define USER_SWITCH_M 1

/*
 * Kernel
 */
#undef KERNEL_M

/*
 * Network Protocol
 */
#undef MAC_M
#undef RF_M
#undef CC2420_M
#undef ROUTING_M
