// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * KEP UDP Sender
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 3. 24.
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "nos.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

INT8 send_timer_id = -1;
#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 5;
#endif
UINT8 node_ext_id[] = { 0x0E, 'T', 'R', 0xFF, 0xFE, 0x00, 0x00, 0x00};

IP6_ADDRESS server_addr;

struct meter_report_message
{
    UINT16 seq;
    UINT16 mA;
} NOS_PACKED;
typedef struct meter_report_message METER_REPORT;

void meter_report(void *args)
{
    METER_REPORT msg;
    static UINT16 seq = 0;

    led_on(1);
    msg.seq = htons(++seq);
    msg.mA = htons(powermeter_get_state());
    //udp_sendto(1234, &server_addr, 1234, msg, strlen(msg) + 1);

    led_off(1);
    {
        UINT8 i;
        for (i = 0; i < sizeof(METER_REPORT); i++)
        {
            printf("%02X ", ((UINT8 *) &msg)[i]);
        }
        printf("\n");
    }

}

int main(void)
{
    nos_init();
    led_on(0);

    printf("\n*** Nano OS ***\n");

    // Initialize the LoWPAN interface.
    node_ext_id[6] = node_id >> 8;
    node_ext_id[7] = node_id & 0xff;
    lowpan_init(0, node_id, node_ext_id, 10, 1);
    ip6_init(0);

    printf("PAN address: %u \nShort address : %u\r\n",
            lowpan_get_pan_id(), lowpan_get_short_id());

    user_timer_create_sec(meter_report, NULL, 1, USER_TIMER_PERIODIC);

    sched_start();
    return 0;
}
