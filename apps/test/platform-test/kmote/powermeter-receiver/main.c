/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file kep-client.c
 * @author Jeehoon Lee (UST, ETRI)
 * @date 2012.01.17
 * @brief Simple test client program for KEP
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "nos.h"

#define MAX_LENGTH 2

MAC_RX_INFO rx_info;

void read_frame(void)
{
    UINT8 i;
    UINT16 val;

    while ( mac_rx(&rx_info) )
    {
        led_toggle(3);

        uart_puts(STDIO, "0x");
        for (i = 0; i < rx_info.payload_length; i++) {
            val = val << 8;
            val |= ((UINT8 *)rx_info.payload)[i];
            printf("%x%x", rx_info.payload[i] >> 4,
                    rx_info.payload[i] & 0x0f);
        }
        printf("\tval(mA): %u receive\r\n",val);
    }
}

int main (void)
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    mac_init(0, 1, NULL);
    mac_set_rx_cb( read_frame );

    while (TRUE)
    {
        delay_us(100);
    }
    return 0;
}
