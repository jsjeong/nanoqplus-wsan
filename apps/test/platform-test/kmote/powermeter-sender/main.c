/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file kep-server.c
 * @author Jeehoon Lee (UST, ETRI)
 * @date 2012.01.17
 * @brief Simple test server program for KEP
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "nos.h"

#define MAX_LENGTH 2

MAC_TX_INFO tx_info;

int main (void)
{
    UINT8 n;
    UINT16 val = 0;

    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    tx_info.dest_addr = 1;
    tx_info.payload_length  = MAX_LENGTH;

    mac_init(0x0000, 0x1234, NULL);

    while (TRUE)
    {
        led_on(2);
        relay_on(0);
        printf("\r\nPower on...");

        for(n = 0; n < 5; n++) {
            val = powermeter_get_state();
            printf("\r\n0x%x\tval(mA): %u send", val, val);
            tx_info.payload[0] = val >> 8;
            tx_info.payload[1] = val;
            if (mac_tx(&tx_info) != ERROR_SUCCESS)
            {
                printf("\r\nTX fail");
            }
            delay_ms(1000);
        }
        printf("\r\nPower off...");
        relay_off(0);
        led_off(2);
        delay_ms(3000);
    }
    return 0;
}
