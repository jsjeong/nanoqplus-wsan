/*
 * Automatically generated C config: don't edit
 * Nano OS kernel version: 
 * Wed Jan 18 22:30:55 2012
 */
#define AUTOCONF_INCLUDED

/*
 * Platforms
 */
#undef ATMEGA128
#define MSP430X1611 1
#undef S3FN41F
#define CONFIG_MCU_NAME "msp430x1611"
#undef UBI_MSP430
#undef UBI_COIN
#undef UBEE430
#undef HMOTE2420
#undef TMOTE_SKY
#define KMOTE 1
#define CONFIG_PLATFORM_NAME "kmote"
#undef UART_BR_9600
#undef UART_BR_19200
#undef UART_BR_38400
#undef UART_BR_57600
#define UART_BR_115200 1
#undef UART_BR_230400
#undef UART_BR_250000
#undef UART_BR_500000
#undef UART_BR_1000000
#undef UART_BR_380400
#undef UART_BR_460800
#undef UART_BR_921600

/*
 * Basic Functions
 */
#define LED_M 1
#define UART_M 1
#define ADC_M 1
#undef TIMECHK_M
#undef TEMPERATURE_DIODE_M

/*
 * Platform Specific Devices and Functions
 */
#undef GENERIC_ADC_SENSOR_M
#undef GENERIC_ADC_GAS_SENSOR_M
#undef GENERIC_ADC_LIGHT_SENSOR_M
#undef GENERIC_ADC_MIC_SENSOR_M
#undef PLATFORM_SPECIFIC_TEMPERATURE_SENSOR_M
#undef PLATFORM_SPECIFIC_LIGHT_SENSOR_M
#define CC2420_M 1
#undef MG2410_M
#undef SENSOR_SHTXX_M
#define IEEE_802_15_4_DEV_M 1
#define RELAY_M 1
#define KEP_M 1
#define POWERMETER_M 1

/*
 * Kernel
 */
#undef KERNEL_M

/*
 * Library
 */
#undef TRICKLE_M
#undef DEBUG_M

/*
 * Network Protocol
 */
#define IEEE_802_15_4_M 1
#define NANO_MAC_M 1
#define CONFIG_MAC_CHANNEL 13
#define CONFIG_MAC_PAN_ID 0x0000
#define CONFIG_MAC_SHORT_ID 1234
#undef L2_LAYER_M
#undef MESH_ROUTING_M
#undef LOWPAN_M

/*
 * Storage System
 */
#undef STORAGE_M

/*
 * Statistics modules
 */
#undef HEAP_STATISTICS_M
