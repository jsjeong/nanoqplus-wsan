// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Receive string from the keyboard.
 *
 * @author Haeyong Kim (ETRI)
 * @date 2006. 6. 1.
 */

#include "nos.h"

#define MAX_STRLEN 10 // You can change this value as you want.

char str[MAX_STRLEN];

void uart_rx_handler (UINT8 port, char rx_char)
{
    if (rx_char == '0')
    {
        led_toggle(0);
        printf("\nLED0 has toggled.\n");
    }
    else if (rx_char == '1')
    {
        led_toggle(1);
        printf("\nLED1 has toggled.\n");
    }
    else if (rx_char == '2')
    {
        led_toggle(2);
        printf("\nLED2 has toggled.\n");
    }
    else
    {
        led_off(0);
        led_off(1);
        led_off(2);
    }
}

int main (void)
{
    nos_init();
    led_on(1);
    printf("\n*** Nano OS ***\n");

    uart_set_getc_callback(STDIO, uart_rx_handler);
    enable_uart_rx_intr(STDIO);

    while (1)
    {
        uart_puts(STDIO, "\n\n\rType anything (up to 9characters), and then press 'Enter'.\n\r");
        uart_gets(STDIO, str, sizeof(str));

        uart_puts(STDIO, "You typed : ");
        uart_puts(STDIO, str);
        uart_puts(STDIO, "\n\r");

        // You can change LED state  for 10sec by calling "uart_rx_handler" 
        uart_puts(STDIO, "\r\nType '0', '1', '2' to change LED state.");
        delay_ms(10000);
    }
}
