// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Heap test
 *
 * @author Junkeun Song (ETRI)
 * @date 2013. 10. 1.
 */

#include "nos.h"

void task1(void *args)
{
    void *a, *b, *c, *d;

    while(1)
    {
        thread_sleep_sec(1);

        a = malloc(10);
        b = malloc(20);
        c = malloc(30);
        d = malloc(20);

        printf("\n\n -1- %p %p %p %p\n",a,b,c,d);
    
        free(a);
        free(b);
        free(c);
        free(d);
    }

}


void task2(void *args)
{
    void *a, *b, *c, *d;

    while(1)
    {
        thread_sleep_sec(1);

        a = malloc(10);
        b = malloc(20);
        c = malloc(30);
        d = malloc(50);

        printf("\n\n -2- %p %p %p %p\n",a,b,c,d);
        free(a);
        free(b);
        free(c);
        free(d);
    }

}

int main(void) 
{
    nos_init();
    led_on(1);
  
    printf("\n*** Nano OS ***\n");
    printf("Heap memory test\n");
  
    thread_create(task1, NULL, 0x0, PRIORITY_NORMAL);  
    thread_create(task2, NULL, 0x0, PRIORITY_NORMAL);   

    sched_start();
    return 0;
}
