// -*- c-basic-offset:4; c-file-style:"bsd"; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Print charater on the terminal (STDIO).
 *
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2006. 6. 1.
 */

/**
 * Print charater on the terminal (STDIO).
 * Terminal setting MUST be '8-N-1'.
 *
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @date 2006. 6. 1.
 */

#include "nos.h"

int main (void)
{
    UINT8 port = STDIO;

    UINT8 i;
    UINT8 name[] = "NanoQplus OS";
    UINT32 u32min = 0x00000000; // 0
    UINT32 u32max = 0xFFFFFFFF; // 4294967295
    INT32 s32min = 0x80000000;  // -2147483648
    INT32 s32max = 0x7FFFFFFF;  // 2147483647
    UINT16 u16min = 0x0000;     // 0
    UINT16 u16max = 0xFFFF;     // 65535
    INT16 s16min = 0x8000;      //-32768
    INT16 s16max = 0x7FFF;      // 32767
    UINT8 u8min = 0x00;         // 0
    UINT8 u8max = 0xFF;         // 255
    INT8 s8min = 0x80;          //-128
    INT8 s8max = 0x7F;          // 127

    nos_init();
    led_on(0);
    
    printf("\n*** %s ***\n", name);

    // "uart_putu" test
    uart_puts(port, "\n\r'uart_putu()' supports 8bit, 16bit, 32bit unsigned integer");
    uart_puts(port, "\n\r8bit unsigned integer (min~max) : ");
    uart_putu(port, u8min);    
    uart_putc(port, '~');
    uart_putu(port, u8max);
    uart_puts(port, "\n\r16bit unsigned integer (min~max) : ");
    uart_putu(port, u16min);
    uart_putc(port, '~');
    uart_putu(port, u16max);
    uart_puts(port, "\n\r32bit unsigned integer (min~max) : ");
    uart_putu(port, u32min);
    uart_putc(port, '~');
    uart_putu(port, u32max);


    // "uart_puti" test
    uart_puts(port, "\n\n\r'uart_puti()' supports 8bit, 16bit, 32bit signed integer and 8bit, 16bit unsigend integer.");
    uart_puts(port, "\n\r8bit signed integer (min~max) : ");
    uart_puti(port, s8min);
    uart_putc(port, '~');
    uart_puti(port, s8max);
    uart_puts(port, "\n\r16bit signed integer (min~max) : ");
    uart_puti(port, s16min);
    uart_putc(port, '~');
    uart_puti(port, s16max);
    uart_puts(port, "\n\r32bit signed integer (min~max) : ");
    uart_puti(port, s32min);
    uart_putc(port, '~');
    uart_puti(port, s32max);

    uart_puts(port, "\n\r8bit unsigned integer (min~max) : ");
    uart_puti(port, u8min);
    uart_putc(port, '~');
    uart_puti(port, u8max);
    uart_puts(port, "\n\r16bit unsigned integer (min~max) : ");
    uart_puti(port, u16min);
    uart_putc(port, '~');
    uart_puti(port, u16max);

    // "printf" test
    printf("\n\n'printf()' supports 8, 16, 32bit signed and unsinged integer.\n");
    printf("8bit unsigned integer (min~max) : "
           "%u~%u (%%u), 0%o~0%o (%%o), 0x%x~0x%x (%%x)\n",
           u8min, u8max, u8min, u8max, u8min, u8max);
           
    printf("16bit unsigned integer (min~max) : "
           "%u~%u (%%u), 0%o~0%o (%%o), 0x%x~0x%x (%%x)\n",
           u16min, u16max, u16min, u16max, u16min, u16max);
           
    printf("32bit unsigned integer (min~max) : "
           "%lu~%lu (%%lu), 0%lo~0%lo (%%lo), 0x%lx~0x%lx (%%lx)\n",
           u32min, u32max, u32min, u32max, u32min, u32max);
    
    printf("8bit signed integer (min~max) : %d~%d (%%d)\n", s8min, s8max);
    printf("16bit signed integer (min~max) : %d~%d (%%d)\n", s16min, s16max);
    printf("32bit signed integer (min~max) : %ld~%ld (%%ld)\n", s32min, s32max);

    while (1)
    {
        // print character
        for (i=0; i<5; i++)
        {
            uart_putc(port, '-');
            delay_ms(200); // wait for 200 ms
        }
        for (i=0; i<5; i++)
        {
            // Backspace key handling
            uart_putc(port, _BS); // Backspace character '_BS' = 0x08
            uart_putc(port, ' ');
            uart_putc(port, _BS);
            delay_ms(200); // wait for 200 ms
        }
        mcu_reboot();
    }
}
