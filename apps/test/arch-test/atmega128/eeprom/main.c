/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file eeprom_app.c
 * @author Haeyong Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "nos.h"
#define MAX_STRLEN 100
char str[MAX_STRLEN];

int main(void)
{
    UINT16 write_addr = 0;
    UINT16 read_addr = 0;
    UINT8 read_len = 0;

    nos_init();
    led_on(1);
    delay_ms(100);
    led_on(2);
    delay_ms(100);
    led_on(3);
    delay_ms(100);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");
    printf("\nEEPROM write/read test.");

    while (1)
    {

        uart_puts(STDIO, "\n\rWrite? (1) or Read? (2) : ");
        uart_gets(STDIO, str, 2);

        if (str[0] == '1')
        {
            printf("Input a string to write : ");
            uart_gets(STDIO, str, MAX_STRLEN);
            printf("Writing \"%s\" into address 0x(%x) in EEPROM ...", str, write_addr);
            eeprom_write_block(str, (void*)write_addr, strlen(str));
            write_addr+= strlen(str);
            printf("\t%d byte has been written.\n", write_addr);
        }
        else if (str[0] == '2')
        {
            printf("Input EEPROM address : ");
            uart_gets(STDIO, str, 6);
            read_addr = atoi(str);
            printf("Input a string length to read : ");
            uart_gets(STDIO, str, 4);
            read_len = atoi(str);
            printf("Reading %d byte from address 0x(%x) in EEPROM ...\n", read_len, read_addr);
            eeprom_read_block(str, (void*)read_addr, read_len);   
            str[read_len] = '\0';
            printf("%s\nRead done.\n", str);
        }

    }
    return 0;
}
