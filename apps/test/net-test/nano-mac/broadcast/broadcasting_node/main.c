//===================================================================
//
// broadcasting_node.c (@haekim)
//
//===================================================================
// Copyright 2004-2010, ETRI
//===================================================================

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Id$
 * $Rev$
 */

#include "nos.h"

NOS_MAC_TX_INFO tx_info;
NOS_MAC_RX_INFO rx_info;

void mac_rx_callback(void)
{
    while (	mac_rx(&rx_info) )
    {
        uart_puts(STDIO, "\n\rRX(");
        uart_putu(STDIO, rx_info.src_addr);
        uart_puts(STDIO, "): ");
        uart_putu(STDIO, rx_info.payload[0]);
    }
}

int main (void)
{
    UINT8 n;

    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    tx_info.dest_addr = 0xffff;	// broadcasting
    tx_info.payload_length = 1;

    // Write dummy data to tx_payload[1]~[115].
    for (n = 0; n < MAC_MAX_SAFE_TX_PAYLOAD_SIZE; n++)
    {
        tx_info.payload[n] = n;
    }

    mac_init(0x2420, 0x1111, NULL);	// channel=26, PAN id = 0x2420, source node id = 0x1111
    mac_set_rx_cb(mac_rx_callback); // default : NULL. RX interrupt callback function

    while (TRUE) 
    {
        ++tx_info.payload[0];
        // Sends packet. Requests autoack
        ENTER_CRITICAL();
        mac_tx(&tx_info);
        printf("\nTX: %u", tx_info.payload[0]);
        delay_ms(6);
        EXIT_CRITICAL();
    }

    return 0;
}
