//===================================================================
//
// response_node.c (@haekim)
//
//===================================================================
// Copyright 2004-2010, ETRI
//===================================================================

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Id$
 * $Rev$
 */

#include "nos.h"

NOS_MAC_TX_INFO tx_info;
NOS_MAC_RX_INFO rx_info;

void mac_rx_callback(void)
{
    while( mac_rx(&rx_info) )
    {
        tx_info.payload[0] = rx_info.payload[0];
        // Sends a packet. Requests autoack
        if (mac_tx(&tx_info) != ERROR_SUCCESS)
        {
            led_toggle(2);
        }
    }
}

int main (void)
{
    UINT8 n;

    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    tx_info.dest_addr = 0x1111;
    tx_info.payload_length = 1;

    // Write dummy data to tx_payload[1]~[115].
    for (n = 0; n < MAC_MAX_SAFE_TX_PAYLOAD_SIZE; n++)
    {
        tx_info.payload[n] = n;
    }

    mac_init(0x2420, 0x0001, NULL); // PAN id = 0x2420, source node id = 0x1111
    mac_set_rx_cb(mac_rx_callback); // default : NULL. RX interrupt callback function

    while (TRUE) 
    {
        delay_ms(1000);
    }
    return 0;
}
