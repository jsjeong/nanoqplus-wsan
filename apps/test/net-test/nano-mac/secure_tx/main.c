/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Security-enabled Tx test
 * @file mac_secure_tx_test.c
 * @date 2012.05.24
 * @author Jongsoo Jeong (ETRI)
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "nos.h"

MAC_TX_INFO tx_info;                            // Simple MAC Tx information.

UINT8 node_ext_id[] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77};
UINT8 sec_key[] = {
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };

UINT16 dest_id = 1;
UINT8 dest_ext_id[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef};

void print_security_mode(MAC_TX_INFO *tx)
{
    switch (tx->sec)
    {
    case IEEE_802_15_4_SEC_NONE:
        uart_puts(STDIO, "without securing");   break;
    case IEEE_802_15_4_SEC_CBC_MAC_4:
        uart_puts(STDIO, "with CBC-MAC-32");    break;
    case IEEE_802_15_4_SEC_CBC_MAC_8:
        uart_puts(STDIO, "with CBC-MAC-64");    break;
    case IEEE_802_15_4_SEC_CBC_MAC_16:
        uart_puts(STDIO, "with CBC-MAC-128");   break;
    case IEEE_802_15_4_SEC_CTR:
        uart_puts(STDIO, "with CTR");           break;
    case IEEE_802_15_4_SEC_CCM_4:
        uart_puts(STDIO, "with CCM-32");        break;
    case IEEE_802_15_4_SEC_CCM_8:
        uart_puts(STDIO, "with CCM-64");        break;
    case IEEE_802_15_4_SEC_CCM_16:
        uart_puts(STDIO, "with CCM-128");       break;
    }
}

int main(void)
{
    UINT8 seq = 0, n;

    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    tx_info.dest_addr_eui64 = NULL;
    tx_info.dest_addr = dest_id;

    tx_info.sec = IEEE_802_15_4_SEC_CCM_16;

    mac_init(0x0104, 0x1234, node_ext_id);
    mac_set_sec_key(0, sec_key);

    // Fill dummy data.
    for (n = 0; n < IEEE_802_15_4_MAX_TX_PAYLOAD_SIZE; n++)
        tx_info.payload[n] = n;

    while (TRUE) 
    {
        delay_ms(1000);
        led_toggle(2);

        switch (tx_info.sec)
        {
        case IEEE_802_15_4_SEC_NONE:
            tx_info.sec = IEEE_802_15_4_SEC_CBC_MAC_4;
            break;
        case IEEE_802_15_4_SEC_CBC_MAC_4:
            tx_info.sec = IEEE_802_15_4_SEC_CBC_MAC_8;
            break;
        case IEEE_802_15_4_SEC_CBC_MAC_8:
            tx_info.sec = IEEE_802_15_4_SEC_CBC_MAC_16;
            break;
        case IEEE_802_15_4_SEC_CBC_MAC_16:
            tx_info.sec = IEEE_802_15_4_SEC_CTR;
            break;
        case IEEE_802_15_4_SEC_CTR:
            tx_info.sec = IEEE_802_15_4_SEC_CCM_4;
            break;
        case IEEE_802_15_4_SEC_CCM_4:
            tx_info.sec = IEEE_802_15_4_SEC_CCM_8;
            break;
        case IEEE_802_15_4_SEC_CCM_8:
            tx_info.sec = IEEE_802_15_4_SEC_CCM_16;
            break;
        case IEEE_802_15_4_SEC_CCM_16:
            tx_info.sec = IEEE_802_15_4_SEC_NONE;
            // Change destination addressing mode when it starts new cycle.
            tx_info.dest_addr_eui64 = (tx_info.dest_addr_eui64) ?
                    NULL : dest_ext_id;
            printf("\n");
            break;
        }

        tx_info.payload_length = mac_get_tx_payload_len(
                ((tx_info.dest_addr_eui64 == NULL) ? FALSE : TRUE),
                tx_info.sec);
        tx_info.payload[0] = seq++;
        uart_puts(STDIO, "\nTX ");
        print_security_mode(&tx_info);

        if (mac_tx(&tx_info) == ERROR_SUCCESS)
        {
            printf(" (success): %u (%u bytes)",
                    tx_info.payload[0], tx_info.payload_length);
        }
        else
        {
            printf(" (fail) : %u (%u bytes)",
                    tx_info.payload[0], tx_info.payload_length);
        }

        tx_info.payload[0] = seq++;
        uart_puts(STDIO, "\nTX not ACK requested ");
        print_security_mode(&tx_info);

        mac_tx_noack(&tx_info);
        printf(" :%u (%u bytes)",
                tx_info.payload[0], tx_info.payload_length);
    }

    return 0;
}
