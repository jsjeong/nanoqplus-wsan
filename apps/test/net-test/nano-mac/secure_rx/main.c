/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief Security enabled Rx test
 * @file mac_secure_rx_test.c
 * @date 2012.05.24
 * @author Jongsoo Jeong (ETRI)
 */

/*
 * $LastChangedDate$
 * $LastChangedBy$
 * $Rev$
 * $Id$
 */

#include "nos.h"

MAC_RX_INFO rx_info;
BOOL processing = FALSE;

UINT8 node_ext_id[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef};
UINT8 sec_key[] = {
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };

void read_frame(void *args)
{
    while( mac_rx(&rx_info) ) // MUST use "while" instead of "if" to make RX QUEUE empty.
    {
        processing = TRUE;
        led_toggle(3);  
        if (!rx_info.use_eui64)
            printf("\nRX : %x, \t", rx_info.src_addr);
        else
            printf("\nRX : %x-%x-%x-%x-%x-%x-%x-%x, \t",
                    rx_info.src_addr_eui64[0],
                    rx_info.src_addr_eui64[1],
                    rx_info.src_addr_eui64[2],
                    rx_info.src_addr_eui64[3],
                    rx_info.src_addr_eui64[4],
                    rx_info.src_addr_eui64[5],
                    rx_info.src_addr_eui64[6],
                    rx_info.src_addr_eui64[7]);

        printf("RSSI(%d),\tCorr(%d)", rx_info.rssi, rx_info.corr);
        printf("\t%x~ (length:%u)",
                rx_info.payload[0], rx_info.payload_length);
        processing = FALSE;
    }
}

void mac_rx_callback(void)
{
    if (!processing)
        nos_taskq_reg(read_frame, NULL);
}

void task1(void *args)
{
    while (TRUE)
    {
        led_toggle(2);
        thread_sleep_ms(300);
    }
}

int main (void)
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    mac_init(0x0104, 1, node_ext_id);
    mac_set_sec_key(0, sec_key);
    mac_set_rx_cb(mac_rx_callback); // default : NULL, sets a callback function for RX interrupt

    thread_create(task1, NULL, 0, PRIORITY_NORMAL);
    sched_start();

    return 0;
}
