// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "nos.h"

UINT8 dev_id = 0;
MAC_RX_INFO rx_info;
BOOL processing = FALSE;

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 2;
#endif

UINT8 node_ext_id[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0, 0};

void read_frame(void *args)
{
    UINT8 i;
    
    while(mac_rx(dev_id, &rx_info) == ERROR_SUCCESS)
    {
        processing = TRUE;
        led_toggle(3);  
        if (rx_info.short_src)
        {
            printf("RX : %x, \t", rx_info.src_addr);
        }
        else
        {
            printf("RX : %02x-%02x-%02x-%02x-%02x-%02x-%02x-%02x, \t",
                    rx_info.src_addr_eui64[0],
                    rx_info.src_addr_eui64[1],
                    rx_info.src_addr_eui64[2],
                    rx_info.src_addr_eui64[3],
                    rx_info.src_addr_eui64[4],
                    rx_info.src_addr_eui64[5],
                    rx_info.src_addr_eui64[6],
                    rx_info.src_addr_eui64[7]);
        }
        printf("RSSI(%d),\tCorr(%d)\t%x~ (length:%u)",
                rx_info.rssi, rx_info.corr, rx_info.payload[0],
                rx_info.payload_length);

        for (i = 0; i < rx_info.payload_length; i++)
        {
            printf(" %02X", rx_info.payload[i]);
        }
        printf("\n");
        processing = FALSE;
    }
}

void mac_rx_callback(UINT8 dev_id)
{
    if (!processing)
        nos_taskq_reg(read_frame, NULL);
}

void blink(void *args)
{
    led_toggle(1);
}

int main (void)
{
    nos_init();
    led_on(0);
    printf("\n*** Nano OS ***\n");

    node_ext_id[6] = (node_id >> 8);
    node_ext_id[7] = node_id & 0xff;
    mac_init(dev_id, 0x1234, node_id, node_ext_id, MAC_TYPE_NANO_MAC, mac_rx_callback);

    user_timer_create_ms(blink, NULL, 100, TRUE);
    
    sched_start();

    return 0;
}
