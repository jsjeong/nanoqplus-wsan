// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "nos.h"

void read_frame(void)
{
    MAC_RX_INFO rx_info;

    while (mac_rx(&rx_info))
    {
        led_toggle(2);
        printf("\nRX : %u\n", rx_info.payload[0]);
    }
}


int main (void)
{
    UINT8 n;
    MAC_TX_INFO tx_info;

    nos_init();
    led_on(0);
    printf("\n*** Nano OS ***\n");

    // Write dummy data to payload room.
    for (n = 0; n < IEEE_802_15_4_MAX_SAFE_TX_PAYLOAD_SIZE; n++)
    {
        tx_info.payload[n] = n;
    }
	
    tx_info.dest_addr = 1;
    tx_info.payload_length = IEEE_802_15_4_MAX_SAFE_TX_PAYLOAD_SIZE;

    mac_init(12, 1, NULL);
    mac_set_rx_cb(read_frame);
	
    while (TRUE) 
    {
        ++tx_info.payload[0];
        if (mac_tx(&tx_info) == ERROR_SUCCESS)
        {
            printf("TX (success): %u (tx count:%u)\n", tx_info.payload[0], tx_info.tx_count);
        }
        else
        {
            printf("\tTX (fail) : %u (tx count:%u)\n", tx_info.payload[0], tx_info.tx_count);
        }
    }
    return 0;
}
