// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Sangcheol Kim (ETRI)
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 */

#include "nos.h"

#ifdef BENCHMARK_M
#include "benchmark.h"
#endif

MAC_TX_INFO tx_info;

UINT16 node_id = 1;
UINT8 node_ext_id[] = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0, 0};
UINT16 dest_id = 2;
UINT8 dest_ext_id[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0, 0};

UINT8 dev_id = 0; //default radio device

void heartbeat(void *args)
{
    led_toggle(2);
}

void tx(void *args)
{
    static UINT16 sent = 0;
    static UINT16 success = 0;
    ERROR_T err;
    
    led_toggle(1);
    ++tx_info.payload[0];

#ifdef BENCHMARK_M
    benchmark_stop();
    benchmark_reset();
    benchmark_start();
#endif

    err = mac_tx(dev_id, &tx_info);
    if (err == ERROR_BUSY)
    {
#ifdef BENCHMARK_M
        benchmark_stop();
#endif

        printf("Resource is busy.\n");
        return;
    }
    else
    {
        if (sent == 1000)
        {
            success = 0;
            sent = 0;
            //while (1);
        }
        
        sent++;
    }

    if (err == ERROR_SUCCESS)
    {
        success++;
        
#ifdef BENCHMARK_M
        benchmark_stop();
        printf("TX (success: %u %% (%u/%u)) - Payload length: %u, Tx count: %u, %u ms\n",
               (UINT16) ((UINT32) success * 100 / sent),
               success, sent,
               tx_info.payload_length,
               tx_info.tx_count,
               benchmark_get_msec());
        
#else
        printf("TX (success: %u %% (%u/%u)) - Payload length: %u, Tx count: %u\n",
               (UINT16) ((UINT32) success * 100 / sent),
               success, sent,
               tx_info.payload_length,
               tx_info.tx_count);
        
#endif      
    }
    else
    {        
#ifdef BENCHMARK_M
        benchmark_stop();
        printf("\tTX (fail: %u %% (%u/%u)) - Payload length: %u, Tx count: %u, %u ms\n",
               (UINT16) ((UINT32) success * 100 / sent),
               success, sent,
               tx_info.payload_length,
               tx_info.tx_count,
               benchmark_get_msec());

#else
        printf("\tTX (fail: %u %% (%u/%u)) - Payload length: %u, Tx count: %u\n",
               (UINT16) ((UINT32) success * 100 / sent),
               success, sent,
               tx_info.payload_length,
               tx_info.tx_count);
#endif
    }
}

int main (void)
{
    UINT8 n;

    nos_init();
    led_on(0);
    
    printf("\n*** Nano OS ***\n");

    dest_ext_id[6] = dest_id >> 8;
    dest_ext_id[7] = dest_id & 0xff;

    tx_info.frame_type = IEEE_802_15_4_FRAME_DATA;
    tx_info.sec = IEEE_802_15_4_SEC_NONE;
    tx_info.dest_addr_eui64 = dest_ext_id;
    tx_info.dest_addr = dest_id;
    /* tx_info.dest_addr_eui64 = NULL; */
    /* tx_info.dest_addr = 0xFFFF; */
    tx_info.payload_length  = MAC_MAX_SAFE_TX_PAYLOAD_SIZE;

    for (n = 0; n < MAC_MAX_SAFE_TX_PAYLOAD_SIZE; n++)
    {
        tx_info.payload[n] = n;
    }

    node_ext_id[6] = node_id >> 8;
    node_ext_id[7] = node_id & 0xff;
    mac_init(dev_id, 0x1234, node_id, node_ext_id, MAC_TYPE_NANO_MAC, NULL);

    user_timer_create_ms(heartbeat, NULL, 200, USER_TIMER_PERIODIC);
    user_timer_create_ms(tx, NULL, 1000, USER_TIMER_PERIODIC);

    sched_start();

    return 0;
}
