/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "nos.h"

UINT16 src_addr, parent_addr;
UINT8 length;
UINT8 port;
UINT8 data[NWK_MAX_PAYLOAD_SIZE]; //108

// RX callback function
void read_packet(void)
{
    UINT8 i;
    led_toggle(2);

    reno_recv_from_nwk(&src_addr, &port, &length, data);
    printf("RX from %u: %u\n",src_addr, data[0]);
    uart_putc(STDIO, '\t');

    for (i = 0; i < length; i++) {
        printf("%x ", data[i]);
    }
    uart_putc(STDIO, '\n');
}

void task1(void* args)
{
    while (TRUE)
    {
        thread_sleep_ms(1000); // do nothing
        led_toggle(3);
    }
}

int main(void) 
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    nwk_init(0, 1, read_packet);
    printf("RF Channel : %u \nPAN address: %u \nShort address : %u\n",
            nwk_get_opmode(), nwk_get_pan_id(), nwk_get_short_id());
    thread_create(task1, NULL, 0, PRIORITY_NORMAL);
    sched_start();
    return 0;
} // main
