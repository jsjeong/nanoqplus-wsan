/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @author Haeyong Kim (ETRI)
 */

/*
 * $LastChangedDate$
 * $Id$
 */

#include "nos.h"

#define DEST_ADDR 1

enum { WORKLOAD_SIZE = 38 };
enum { TX_SLEEP_PERIOD = 60 }; //sec

// TX thread 
void task1(void* args)
{
    UINT8 i, result;
    UINT8 data[WORKLOAD_SIZE];

    // dummy data
    for (i=0; i< WORKLOAD_SIZE; ++i)
    {
        data[i]=i + 0x88;
    }

    while (TRUE)
    {
        led_toggle(2);  
        data[0] = ++i;
        result = reno_send_to_nwk(DEST_ADDR, 1, WORKLOAD_SIZE, data); // dest_addr, port, length, data_ptr
        if (result == RENO_TX_SUCCEEDED)
        {
            printf("\nTX : %u", i);
        }
        else if (result == RENO_TX_DELAYED)
        {
            printf("\nTX Delayed: %u", i);
        }
        else
        {
            printf("\nTX failed %u", i);
        }
        thread_sleep_sec(TX_SLEEP_PERIOD);
    }
}

int main(void) 
{
    nos_init();
    led_on(1);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    nwk_init(0, 2, NULL);
    printf("RF Channel : %u \nPAN address: %u \nShort address : %u\n",
            nwk_get_my_channel(), nwk_get_my_pan_id(), nwk_get_my_short_id());
    thread_create(task1, NULL, 108, PRIORITY_NORMAL); // stack size is 108 not 0 because of data[] in task1.
    sched_start();
    return 0;
} // main
