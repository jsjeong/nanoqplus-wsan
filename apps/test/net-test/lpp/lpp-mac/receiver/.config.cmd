deps_config := \
	nos/net/application/Kconfig \
	nos/net/transport/Kconfig \
	nos/net/rpl/Kconfig \
	nos/net/inet/Kconfig \
	nos/net/6lowpan/Kconfig \
	nos/net/l2-routing/Kconfig \
	nos/net/mac/glossy/Kconfig \
	nos/net/mac/nano-mac/Kconfig \
	nos/net/mac/Kconfig \
	nos/net/Kconfig \
	nos/lib/tinyrs/Kconfig \
	nos/lib/Kconfig \
	nos/kernel/Kconfig \
	nos/drivers/shtxx/Kconfig \
	nos/drivers/rf_modem/mg2410/Kconfig \
	nos/drivers/rf_modem/cc2420/Kconfig \
	nos/drivers/rf_modem/cc112x/Kconfig \
	nos/drivers/rf_modem/at86rf212/Kconfig \
	nos/drivers/rf_modem/adf7023-j/Kconfig \
	nos/drivers/rf_modem/Kconfig \
	nos/drivers/ncp1840/Kconfig \
	nos/drivers/mics5132/Kconfig \
	nos/drivers/m25px16/Kconfig \
	nos/drivers/elt_s100/Kconfig \
	nos/drivers/ds1086l/Kconfig \
	nos/drivers/bh1600/Kconfig \
	nos/drivers/apollo/Kconfig \
	nos/drivers/generic-storage/Kconfig \
	nos/drivers/Kconfig \
	nos/platform/apollo-netk/Kconfig.usart \
	nos/platform/apollo-netk/Kconfig \
	nos/platform/ez-stm32w108cb/Kconfig \
	nos/platform/mct-mmu800i/Kconfig.uca3 \
	nos/platform/mct-mmu800i/Kconfig.uca2 \
	nos/platform/mct-mmu800i/Kconfig.uca1 \
	nos/platform/mct-mmu800i/Kconfig.uca0 \
	nos/platform/mct-mmu800i/Kconfig.uart \
	nos/platform/mct-mmu800i/Kconfig \
	nos/platform/mct-msm210/Kconfig.uart4 \
	nos/platform/mct-msm210/Kconfig.usart3 \
	nos/platform/mct-msm210/Kconfig.usart2 \
	nos/platform/mct-msm210/Kconfig.usart1 \
	nos/platform/mct-msm210/Kconfig.usart \
	nos/platform/mct-msm210/Kconfig \
	nos/arch/stm32f2xx/Kconfig.usart6 \
	nos/arch/stm32f2xx/Kconfig.usart3 \
	nos/arch/stm32f2xx/Kconfig.usart2 \
	nos/arch/stm32f2xx/Kconfig.usart1 \
	nos/platform/mct-sun/Kconfig \
	nos/platform/ek-z005sr/Kconfig \
	nos/platform/mg2410-s3fn41f/Kconfig \
	nos/platform/efm32gg-stk3700/Kconfig.usart \
	nos/platform/efm32gg-stk3700/Kconfig \
	nos/platform/chronos/Kconfig.uca0 \
	nos/platform/chronos/Kconfig.uart \
	nos/platform/chronos/Kconfig \
	nos/platform/z1/Kconfig.usart \
	nos/platform/z1/Kconfig \
	nos/platform/kmote/Kconfig.usart \
	nos/platform/kmote/Kconfig \
	nos/platform/tmote-sky/Kconfig.usart \
	nos/platform/tmote-sky/Kconfig \
	nos/platform/hmote2420/Kconfig.usart \
	nos/platform/hmote2420/Kconfig \
	nos/platform/ubi-coin/Kconfig.usart \
	nos/platform/ubi-coin/Kconfig \
	nos/platform/ubi-msp430/Kconfig.usart \
	nos/platform/ubi-msp430/Kconfig \
	nos/platform/ubee430/Kconfig.usart \
	nos/platform/ubee430/Kconfig \
	nos/platform/isn-903n/Kconfig.usart1 \
	nos/platform/isn-903n/Kconfig.usart0 \
	nos/platform/isn-903n/Kconfig \
	nos/platform/iris/Kconfig \
	nos/platform/yp128/Kconfig \
	nos/platform/zigbex/Kconfig \
	nos/platform/ocx-z/Kconfig \
	nos/platform/nano24/Kconfig \
	nos/platform/micaz/Kconfig \
	nos/platform/Kconfig \
	nos/arch/stm32w108xx/Kconfig \
	nos/arch/stm32f2xx/Kconfig \
	nos/arch/stm32f10x/Kconfig.uart5 \
	nos/arch/stm32f10x/Kconfig.uart4 \
	nos/arch/stm32f10x/Kconfig.usart3 \
	nos/arch/stm32f10x/Kconfig.usart2 \
	nos/arch/stm32f10x/Kconfig.usart1 \
	nos/arch/stm32f10x/Kconfig \
	nos/arch/s3fn41f/Kconfig \
	nos/arch/cc430/Kconfig.uca0 \
	nos/arch/cc430/Kconfig \
	nos/arch/msp430f5438/Kconfig.uca3 \
	nos/arch/msp430f5438/Kconfig.uca2 \
	nos/arch/msp430f5438/Kconfig.uca1 \
	nos/arch/msp430f5438/Kconfig.uca0 \
	nos/arch/msp430f5438/Kconfig \
	nos/arch/msp430f2617/Kconfig \
	nos/arch/msp430f1611/Kconfig \
	nos/arch/efm32gg/Kconfig \
	nos/arch/atmega1284p/Kconfig.usart1 \
	nos/arch/atmega1284p/Kconfig.usart0 \
	nos/arch/atmega1284p/Kconfig \
	nos/arch/atmega1281/Kconfig \
	nos/arch/atmega128/Kconfig \
	nos/arch/Kconfig \
	nos/arch/stm32w108xx/Kconfig.platform \
	nos/arch/stm32f2xx/Kconfig.platform \
	nos/arch/stm32f10x/Kconfig.platform \
	nos/arch/s3fn41f/Kconfig.platform \
	nos/arch/cc430/Kconfig.platform \
	nos/arch/msp430f5438/Kconfig.platform \
	nos/arch/msp430f2617/Kconfig.platform \
	nos/arch/msp430f1611/Kconfig.platform \
	nos/arch/efm32gg/Kconfig.platform \
	nos/arch/atmega1284p/Kconfig.platform \
	nos/arch/atmega1281/Kconfig.platform \
	nos/arch/atmega128/Kconfig.platform \
	/Users/jsjeong/Documents/workspace/NanoQplus/Kconfig

.config include/linux/autoconf.h: $(deps_config)

$(deps_config):
