// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LPP MAC receiver
 *
 * @date Jul 23 2013
 * @author Jongsoo Jeong (ETRI)
 */

#include "nos.h"
#include "rf_modem.h"

MAC_RX_INFO rx_info;
BOOL processing = FALSE;

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 2;
#endif

UINT16 received = 0;

void heartbeat(void *args)
{
    led_toggle(2);
}

void read_frame(void *args)
{
    UINT8 i;
    
    while( mac_rx(&rx_info) ) // MUST use "while" instead of "if" to make RX QUEUE empty.
    {
        processing = TRUE;
        led_toggle(3);
        received++;
        if (!rx_info.use_eui64)
        {
            printf("(%d) RX : %x, \t", received, rx_info.src_addr);
        }
        else
        {
            printf("(%d) RX : %02x-%02x-%02x-%02x-%02x-%02x-%02x-%02x, \t",
                   received,
                   rx_info.src_addr_eui64[0],
                   rx_info.src_addr_eui64[1],
                   rx_info.src_addr_eui64[2],
                   rx_info.src_addr_eui64[3],
                   rx_info.src_addr_eui64[4],
                   rx_info.src_addr_eui64[5],
                   rx_info.src_addr_eui64[6],
                   rx_info.src_addr_eui64[7]);
        }
        printf("RSSI(%d),\tCorr(%d)\t%x~ (length:%u)",
                rx_info.rssi, rx_info.corr, rx_info.payload[0],
                rx_info.payload_length);

        for (i = 0; i < rx_info.payload_length; i++)
        {
            printf(" %02X", rx_info.payload[i]);
        }
        printf("\n");
        processing = FALSE;
    }
}

void receive_done(void)
{
    if (!processing)
        taskq_reg(read_frame, NULL);
}

int main (void)
{
    UINT8 n;

    nos_init();
    led_on(0);
    
    printf("\n*** Nano OS ***\n");

    mac_init(0x1234, node_id, NULL);
    mac_set_rx_cb(receive_done);    

    user_timer_create_ms(heartbeat, NULL, 100, USER_TIMER_PERIODIC);

    sched_start();

    return 0;
}
