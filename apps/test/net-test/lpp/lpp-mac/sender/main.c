// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LPP MAC sender
 *
 * @date Jul 23 2013
 * @author Jongsoo Jeong (ETRI)
 */

#include "nos.h"
#include "rf_modem.h"

MAC_TX_INFO tx_info;

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 1;
#endif

UINT16 dest_id = 2;

UINT16 sent = 0;
UINT16 success = 0;

void heartbeat(void *args)
{
    led_toggle(2);
}

void send_done(MAC_TX_INFO *f, ERROR_T result)
{
    if (result == ERROR_SUCCESS)
    {
        success++;
        printf("Tx success (%u/%u)\n", success, sent);
    }
    else
    {
        printf("Tx fail (%u/%u)\n", success, sent);
    }
    
}

void tx(void *args)
{
    led_toggle(1);
    ++tx_info.payload[0];

    if (tx_info.dest_addr == 0xffff)
        tx_info.dest_addr = dest_id;
    else
        tx_info.dest_addr = 0xffff;
    
    if (mac_tx(&tx_info) >= 0)
    {
        sent++;
    }
}

int main (void)
{
    UINT8 n;

    nos_init();
    led_on(0);
    
    printf("\n*** Nano OS ***\n");

    tx_info.sec = IEEE_802_15_4_SEC_NONE;
    tx_info.dest_addr = dest_id;
    //tx_info.dest_addr = 0xFFFF;
    tx_info.payload_length  = MAC_MAX_SAFE_TX_PAYLOAD_SIZE;

    for (n = 0; n < MAC_MAX_SAFE_TX_PAYLOAD_SIZE; n++)
    {
        tx_info.payload[n] = n;
    }

    mac_init(0x1234, node_id, NULL);
    mac_set_tx_cb(send_done);

    user_timer_create_ms(heartbeat, NULL, 100, USER_TIMER_PERIODIC);
    user_timer_create_ms(tx, NULL, 10000, USER_TIMER_PERIODIC);

    sched_start();

    return 0;
}
