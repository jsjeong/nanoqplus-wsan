// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * LPP sleeping node
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 7. 3.
 */

#include "nos.h"
#include "lpp.h"

char probe_pkt[] = { '\x01', 'P', 'r', 'o', 'b', 'e' };
char probe_ack_pkt[] = { '\x02', 'A', 'c', 'k' };

UINT16 node_id = 2;

UINT8 lpp_get_probe(UINT8 *pkt)
{
    memcpy(pkt, probe_pkt, sizeof(probe_pkt));
    return sizeof(probe_pkt);
}

UINT8 lpp_get_probe_ack(UINT8 *pkt)
{
    memcpy(pkt, probe_ack_pkt, sizeof(probe_ack_pkt));
    return sizeof(probe_ack_pkt);
}

BOOL lpp_is_probe(const UINT8 *pkt, UINT8 size)
{
    return (size == sizeof(probe_pkt) &&
            memcmp(pkt, probe_pkt, sizeof(probe_pkt)) == 0);
}

BOOL lpp_is_probe_ack(const UINT8 *pkt, UINT8 size)
{
    return (size == sizeof(probe_ack_pkt) &&
            memcmp(pkt, probe_ack_pkt, sizeof(probe_ack_pkt)) == 0);
}

void on_new_lpp_state(enum lpp_state state)
{
    if (state == LPP_STOP)
    {
        printf("LPP is stopped.\n");
        led_off(1);
        led_on(2);
    }
    else if (state == LPP_SLEEP)
    {
        printf("Sleeping...\n");
        led_off(1);
        led_off(2);
    }
    else if (state == LPP_LISTEN)
    {
        printf("Listening...\n");
        led_on(1);
        led_off(2);
    }
    else
    {
        printf("Unknown state!!\n");
    }
}

int main(void)
{
    nos_init();
    
    srand(node_id);
    lpp_init(NULL);
    
    led_on(0);
    printf("*** NanoQplus ***\n");
    
    lpp_set_probe_interval(5);
    lpp_set_listen_timeout(30);
    lpp_set_event_handler(on_new_lpp_state);
    lpp_sleep();

    sched_start();
    return 0;
}
