// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Glossy Test Application - 0. Interrupt generator
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 15.
 */

/*
 * $LastChangedDate: 2013-04-22 17:29:08 +0900 (Mon, 22 Apr 2013) $
 * $Id: main.c 1310 2013-04-22 08:29:08Z juny $
 */

#include "nos.h"
#include <avr/interrupt.h>
#include <avr/io.h>

#ifndef ATMEGA1284P
#error "This application is only for ATmega1284P."
#endif //ATMEGA1284P

UINT16 delta = 0x0000;
UINT16 cnt = 0;//1000;
UINT8 check = 0;

void button_pushed(UINT8 sw_id)
{
    if (sw_id == SW3)
    {
        if (delta < 0xff00) delta += 2; //delta += 0x100;
        printf("+:%04X\n", delta);
    }
    else if (sw_id == SW4)
    {
        if (delta < 0xffff) delta += 40; //delta++;
        printf("+:%04X\n", delta);
    }
    else if (sw_id == SW5)
    {
       // if (delta > 0) delta += 200; //delta--;
	if (delta < 0xffff) delta+= 200;
        printf("-:%04X\n", delta);
    }
    else if (sw_id == SW6)
    {
        //if (delta > 0x0100) delta -= 0x100;
        //printf("-:%04X\n", delta);
        cnt = 0;
        check = 0;
    }

    OCR1BH = (65535 - delta) >> 8;
    OCR1BL = (65535 - delta) & 0xff;
    printf("%02x%02x %02x%02x", OCR1AH, OCR1AL, OCR1BH, OCR1BL);
}

void disconnect_oc1x_pins(void)
{
    _BIT_CLR(TCCR1A, COM1A1);
    _BIT_CLR(TCCR1A, COM1A0);
    _BIT_CLR(TCCR1A, COM1B1);
    _BIT_CLR(TCCR1A, COM1B0);
}

void connect_oc1x_pins(void)
{
    // Set OC1A, OC1B pins when interrupt.
    _BIT_SET(TCCR1A, COM1A1);
    _BIT_SET(TCCR1A, COM1A0);
    _BIT_SET(TCCR1A, COM1B1);
    _BIT_SET(TCCR1A, COM1B0);
}

int main(void)
{
    nos_init();
    NOS_GPIO_INIT_OUT(D, 4); //OC1B
    NOS_GPIO_INIT_OUT(D, 5); //OC1A

    led_on(LED_D1);

    printf("*** NanoQplus ***\n");
    button_set_callback(button_pushed);

    CLEAR_TIMER1_CAPT_vect();
    CLEAR_TIMER1_COMPA_vect();
    CLEAR_TIMER1_COMPB_vect();

    DISABLE_TIMER1_CAPT_vect();
    ENABLE_TIMER1_COMPA_vect();
    ENABLE_TIMER1_COMPB_vect();

    OCR1AH = 0xff; OCR1AL = 0xff;
    OCR1BH = (65535 - delta) >> 8;
    OCR1BL = (65535 - delta) & 0xff;

    //TCCR1B |= (1 << CS00);
    //while(1);

    // Toggle OC1A, OC1B pins when interrupt. 
    _BIT_CLR(TCCR1A, COM1A1);
    _BIT_SET(TCCR1A, COM1A0);
    _BIT_CLR(TCCR1A, COM1B1);
    _BIT_SET(TCCR1A, COM1B0);

    while(1)
    {
        delay_ms(100);
        led_toggle(LED_D1);
        if(cnt < 1000){
            TCCR1B |= (1 << CS00);
            cnt ++;
        }
        if (cnt == 1000 && check ==0) {
            printf("\nsend done\n");
            check = 1;
        }
    }

    /*
    while(1)
    {
        TCCR1B &= ~((1 << CS02) + (1 << CS01) + (1 << CS00));
        led_off(LED_D2);
        led_off(LED_D3);
        //printf("Stopped\n");

        disconnect_oc1x_pins();
        NOS_GPIO_OFF(D, 5);
        NOS_GPIO_OFF(D, 4);

        delay_ms(500);
        
        connect_oc1x_pins();
        TCCR1B |= (1 << CS00);
        delay_ms(500);
    }
    */
    return 0;
}

ISR(TIMER1_COMPA_vect, ISR_BLOCK)
{
    NOS_ENTER_ISR();
    
    TCCR1B &= ~((1 << CS02) + (1 << CS01) + (1 << CS00));
    //TCCR1B &= ~((1 << CS02) + (1 << CS01) + (1 << CS00)); // Stop the timer.
    led_on(LED_D2);
    //printf("a");
    NOS_EXIT_ISR();
}

ISR(TIMER1_COMPB_vect, ISR_BLOCK)
{
    NOS_ENTER_ISR();
    
    led_on(LED_D3);
    //printf("b");
    NOS_EXIT_ISR();
}
