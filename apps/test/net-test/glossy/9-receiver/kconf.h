/*
 * Automatically generated C config: don't edit
 * Nano OS kernel version: 
 * Fri Nov  8 17:50:45 2013
 */
#define AUTOCONF_INCLUDED
#undef ATMEGA128
#undef ATMEGA1281
#define ATMEGA1284P 1
#undef MSP430F1611
#undef MSP430F2617
#undef MSP430F5438
#undef CC430F6137
#undef S3FN41F
#undef EFM32GG330F1024
#undef EFM32GG990F1024
#undef STM32F103CB
#undef STM32F103RB
#undef STM32F103RF
#undef STM32F217VE
#undef STM32W108CB
#define CONFIG_MCU_NAME "atmega1284p"
#define CONFIG_PLATFORM_NAME "isn-903n"
#undef IRIS
#define ISN_903N 1

/*
 * ---
 */

/*
 * Basic Functions
 */
#undef ADC_M
#undef EEPROM_M
#undef ATMEGA1284P_CORECLK_16MHZ
#define ATMEGA1284P_CORECLK_8MHZ 1
#undef ATMEGA1284P_ASYNC_USART0
#undef ATMEGA1284P_ASYNC_USART0_BR_9600
#undef ATMEGA1284P_ASYNC_USART0_BR_19200
#undef ATMEGA1284P_ASYNC_USART0_BR_38400
#undef ATMEGA1284P_ASYNC_USART0_BR_57600
#undef ATMEGA1284P_ASYNC_USART0_BR_76800
#undef ATMEGA1284P_ASYNC_USART0_BR_115200
#undef ATMEGA1284P_ASYNC_USART0_BR_230400
#undef ATMEGA1284P_ASYNC_USART0_BR_250000
#undef ATMEGA1284P_ASYNC_USART0_BR_500000
#undef ATMEGA1284P_ASYNC_USART0_BR_1000000
#define ATMEGA1284P_ASYNC_USART1 1
#undef ATMEGA1284P_ASYNC_USART1_BR_9600
#undef ATMEGA1284P_ASYNC_USART1_BR_19200
#undef ATMEGA1284P_ASYNC_USART1_BR_38400
#undef ATMEGA1284P_ASYNC_USART1_BR_57600
#undef ATMEGA1284P_ASYNC_USART1_BR_76800
#define ATMEGA1284P_ASYNC_USART1_BR_115200 1
#undef ATMEGA1284P_ASYNC_USART1_BR_230400
#undef ATMEGA1284P_ASYNC_USART1_BR_250000
#undef ATMEGA1284P_ASYNC_USART1_BR_500000
#undef ATMEGA1284P_ASYNC_USART1_BR_1000000

/*
 * Platform Specific Devices and Functions
 */
#undef BATTERY_MONITOR_M
#undef POWER_SOURCE_MAINS
#undef POWER_SOURCE_BATTERY
#undef POWER_SOURCE_ENERGY_SCAVENGER
#define LED_M 1
#define UART_M 1
#undef UART_INPUT_M
#undef SENSOR_TEMPERATURE_M
#undef SENSOR_LIGHT_M
#undef SENSOR_HUMIDITY_M
#undef IRIS_STDIO_UART0
#undef IRIS_UART0_BR_9600
#undef IRIS_UART0_BR_19200
#undef IRIS_UART0_BR_38400
#undef IRIS_UART0_BR_57600
#undef IRIS_UART0_BR_115200
#undef IRIS_UART0_BR_230400
#define ISN_903N_CLOCK_ATMEGA1284P_RCOSC_8MHZ 1
#undef ISN_903N_CLOCK_AT86RF212_CLKM_16MHZ
#undef STDIO_ISN_903N_USART0
#define STDIO_ISN_903N_USART1 1
#undef ISN_903N_ASYNC_USART0
#define ISN_903N_ASYNC_USART1 1
#undef ISN_903N_ASYNC_USART1_BR_9600
#undef ISN_903N_ASYNC_USART1_BR_19200
#undef ISN_903N_ASYNC_USART1_BR_38400
#undef ISN_903N_ASYNC_USART1_BR_57600
#undef ISN_903N_ASYNC_USART1_BR_76800
#define ISN_903N_ASYNC_USART1_BR_115200 1
#undef ISN_903N_ASYNC_USART1_BR_230400
#undef ISN_903N_ASYNC_USART1_BR_250000
#undef ISN_903N_ASYNC_USART1_BR_500000
#undef ISN_903N_ASYNC_USART1_BR_1000000
#define BUTTON_M 1
#undef SENSOR_CO_M
#undef SENSOR_CO2_M
#undef PIR_SENSOR_M
#undef IR_LCD_M
#define ISN_903N_RF212 1

/*
 * Driver Options
 */
#undef GENERIC_ADC_SENSOR_M
#undef GENERIC_ADC_GAS_SENSOR_M
#undef GENERIC_ADC_LIGHT_SENSOR_M
#undef GENERIC_ADC_MIC_SENSOR_M
#undef GENERIC_ADC_PIR_SENSOR_M
#undef PLATFORM_SPECIFIC_TEMPERATURE_SENSOR_M
#undef PLATFORM_SPECIFIC_LIGHT_SENSOR_M
#undef STORAGE_M
#undef ADF7023J_M
#define AT86RF212_M 1
#undef AT86RF230_M
#undef CC1120
#undef CC2420_M
#undef CC2520_M
#undef MG2410_M
#undef IEEE_802_15_4_DEV_FAST_READ
#undef APOLLO_M
#undef BH1600_M
#undef DS1086L_M
#undef ELT_S100_M
#undef M25PX16
#undef MICS5132_M
#undef MTS300
#undef NCP1840_M
#undef SHTXX_M

/*
 * Kernel
 */
#define KERNEL_M 1
#define ENABLE_SCHEDULING 1
#undef SCHED_PERIOD_5
#define SCHED_PERIOD_10 1
#undef SCHED_PERIOD_32
#undef SCHED_PERIOD_100
#undef USER_TIMER_M
#undef MSGQ_M
#define TASKQ_LEN_8 1
#undef TASKQ_LEN_16
#undef TASKQ_LEN_32
#undef TASKQ_LEN_64
#undef TASKQ_LEN_128
#undef THREAD_M

/*
 * Library
 */
#undef DEBUG_M
#undef LIB_UTC_CLOCK
#undef LIB_LPP

/*
 * Network Protocol
 */
#define IEEE_802_15_4 1

/*
 * IEEE 802.15.4 options
 */
#undef IEEE_802_15_4_FFD
#undef IEEE_802_15_4_INDIRECT
#undef IEEE_802_15_4_RXADDR_LIMIT_M
#undef NANO_MAC_M
#undef LPP_MAC
#define GLOSSY_M 1

/*
 * Glossy Options
 */
#undef GLOSSY_RS
#undef GLOSSY_LPP
#undef MESH_ROUTING_M
#undef ZIGBEE_IP
#undef LOWPAN_M
#undef INET_M
#undef EAP
