// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Glossy Reception Test Program
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 18.
 */

/*
 * $LastChangedDate: 2013-03-16 23:57:27 +0900 (Sat, 16 Mar 2013) $
 * $Id: main.c 1275 2013-03-16 14:57:27Z jsjeong $
 */

#include "nos.h"
#include "glossy.h"

extern UINT8 rx_status;
extern UINT8 ed;
extern UINT8 lqi;

// JJ: for reset seq
UINT16 seq;
UINT16 success;
void received(const struct glossy_frame *frame)
{
//    static UINT16 seq = 0;
//    static UINT16 success = 0;
    UINT8 i;

    printf("%u. Received (%u bytes): ", seq++, frame->len);

    for (i = 0; i < frame->len; i++)
    {
        printf("%02X ", frame->buf[i]);
    }
    
    if (frame->crc_ok)
    {
        printf("CRC OK(%d) ", ++success);
    }
    else
    {
        printf("CRC FAIL(%d) ", success);
    }
    
    printf("RSSI:%d, LQI:%d\n\n", frame->rssi, frame->lqi);
}

void tick(void *args)
{
    led_toggle(0);
}

UINT16 num_ex;

void uart_rx_handler (char rx_char)
{
	if (rx_char == '0')
	{
		led_toggle(2);
		seq = 0; success = 0;
		printf("%u Experiment ended\n", num_ex++);
	}
}

int main(void)
{
    nos_init();
    seq = 0;	num_ex = 0; 	success = 0; 	// JJ: reset seq
    glossy_init(0, 2, NULL);
    glossy_set_rx_cb(received);
    led_on(0);
    printf("*** NanoQplus ***\n");
    user_timer_create_ms(tick, NULL, 500, TRUE);
    
	uart_set_getc_callback(STDIO, uart_rx_handler);
	enable_uart_rx_intr(STDIO);

    sched_start();
    return 0;
}
