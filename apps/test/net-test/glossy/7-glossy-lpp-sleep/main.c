// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Glossy-LPP Reception Test Program
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 18.
 */

#include "nos.h"
#include "glossy.h"

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 2;
#endif

void received(UINT16 src, const UINT8 *payload, UINT8 size, BOOL crc_ok)
{
    if (crc_ok)
    {
        UINT16 sent = (payload[0] << 8) + payload[1];
        UINT16 not_initiated = (payload[2] << 8) + payload[3];
        UINT16 success = (payload[4] << 8) + payload[5];
        UINT32 prr = success * 100 / sent;
    
        printf("[%u] (%u bytes) - sent:%u, not initiated:%u, success:%u, PRR:%u%%\n",
               src, size,
               sent,
               not_initiated,
               success,
               (UINT16) prr);
    }
}

int main(void)
{
    nos_init();
    glossy_init(0, node_id, NULL);
    glossy_set_rx_cb(received);
    printf("*** NanoQplus ***\n");
    sched_start();
    return 0;
}
