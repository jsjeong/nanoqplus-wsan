// -*- c-basic-offset:4; tab-width:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributsrs in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Glossy Test Application - 1. Tx by interrupt
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 12.
 */

/*
 * $LastChangedDate: 2013-04-22 17:29:08 +0900 (Mon, 22 Apr 2013) $
 * $Id: main.c 1310 2013-04-22 08:29:08Z juny $
 */

#include "nos.h"
#include "glossy.h"
#include "rf_modem.h"
#include "at86rf212_registermap.h"
#ifdef GLOSSY_RS
#include "TinyRS.h"
#endif
MAC_TX_INFO f;

//#define RS_MATLAB
//#define rs_matlab_parity_len  20
#define CONV_MATLAB
#define conv_msg_len  60

void button_pressed(UINT8 sw_id)
{
    //if (sw_id == SW3)
    {
        //led_on(1);

        //glossy_tx(&f);
        //nos_rf_modem_tx(&f);

        // Just send a frame stored in the Frame Buffer.
        AT86RF212_SLP_TR_SET();
        nos_delay_us(0);
        AT86RF212_SLP_TR_CLEAR();

        //led_on(1);
        // Wait for TX done.
        while (at86rf212_subreg_read(SR_TRX_STATUS) != TRX_STATUS_BUSY_TX);
        while (at86rf212_subreg_read(SR_TRX_STATUS) != TRX_STATUS_PLL_ON);


        //printf("Send!\n");
        //led_off(1);
    }
}

int main(void)
{
    UINT8 i;
    
    nos_init();
    led_on(0);
    
    glossy_init(0, 1, NULL);
    button_set_callback(button_pressed);
    
    printf("*** Nanoqplus ***\n");

    f.dest_addr = 2;
    f.dest_addr_eui64 = NULL;

#ifndef GLOSSY_RS
    f.payload_length = IEEE_802_15_4_MAX_SAFE_TX_PAYLOAD_SIZE;
#else
    f.payload_length = (CONFIG_LIB_TINY_RS_DATA_LEN - 9); //9 is size of the MAC header.
#endif
    
    for (i = 0; i < f.payload_length; i++)
    {
        f.payload[i] = i;
    }

#ifdef RS_MATLAB
	// ## for RS-code test with MATLAB code
	// header_size = 9; 
	// header + payload length = 40;
	// RS parity length = 20;
	// add on 20 byte RS parity code in f.payload[31]
	{
#if (rs_matlab_parity_len == 20)
		// RS-code with parity length = 20 case
    		UINT8 rs_parity[rs_matlab_parity_len] = {166,  30,  57,  71, 174, 116, 161,  16, 225, 58,
							  92,  27, 165,  43, 229, 129, 140, 252,  16, 99};
#endif
#if (rs_matlab_parity_len == 40)		
		// RS-code with parity length = 40 case
		UINT8 rs_parity[rs_matlab_parity_len] = {236, 185, 133,   4,  60,  17, 175, 176,  24,  71, 
							 194,   5,  76, 216,  32,  65,  45, 162, 214,  77,  
							 143, 143, 133,  50,  77,  83, 126, 216, 247, 217,  
							  64,  48, 197, 173, 219, 192, 211,  69, 112, 161};
#endif
#if (rs_matlab_parity_len == 8)
		// RS-code with parity length = 8 case
		UINT8 rs_parity[rs_matlab_parity_len] = {202, 194, 214, 80, 193, 69, 83, 204};
#endif
		memcpy(&f.payload[31], rs_parity, rs_matlab_parity_len);
		f.payload_length = 31+rs_matlab_parity_len;
    		ieee_802154_make_header(&f, 0, 0, NULL, 1, FALSE);
	}
#endif	// RS_MATLAB

#ifdef CONV_MATLAB
	// ## for convenc/vitdec test with MATLAB
	// trel = poly2trellis(7, [171, 133[); // NASA, DVB system Inner Coding
	{
#if (conv_msg_len == 80)
		// code rate = 1/2 case
		UINT8 tmp[conv_msg_len] = { 59, 196,  80,  63, 108,   0,   0,   0,   0,   0,
					     0,  14, 241, 192,   0,   3, 188, 112,   0,   0, 
					     0,   3, 188, 126, 241, 205,  77, 139, 199,  56,
					   123,  69,  54, 246, 138,  95,  28, 236, 160, 145,
					   237,  34,  81, 100, 219, 215, 103, 170,  42,  25,
					   149,  12, 115, 191, 207, 194, 130, 113,  62,  55,
					   180, 132,   8, 249,  69,  74, 249, 227, 111,  80,
					   211,  45, 158, 158,  34, 216, 168, 107,  20,  22};
#endif
#if (conv_msg_len == 60)		
		// code rate = 2/3 case
		UINT8 tmp[conv_msg_len] = {126,   2,  31,  80,   0,   0,   0,   0,   6, 230, 
					     0,   3, 241, 128,   0,   0,  63,  30, 230,  81, 
					   103, 141, 199, 193, 107, 169, 143,  51,  76,  41, 
					   213,  34,  80, 190, 180, 246,  88, 218,  68, 111, 
					   249, 226, 137, 151, 155, 226,   1,  61,   4, 111,
					   115,  92, 138, 213, 186, 228, 172, 209, 114,  10};
#endif
#if (conv_msg_len == 48)
		// code rate = 5/6 case
		UINT8 tmp[conv_msg_len] = {108,   0, 234,   0,   0,   0,   1, 179,   0,  15,
					    16,   0,   0, 249, 252, 177, 111,  46, 104,  87, 
					   179,  57,  58, 148, 242, 129, 151, 154, 252, 230, 
					   145,  22, 243, 140, 144, 237, 212,  11, 192,  47,
					   117, 228, 139, 110, 105, 167,  28,   5};
#endif
		UINT8 len;
		f.payload_length = conv_msg_len - f.header_length;
		len = f.header_length + f.payload_length;
		
		// encoded val
		memcpy(f.header, &tmp[0], f.header_length);
		memcpy(f.payload, &tmp[f.header_length], f.payload_length);
	}
#endif
#ifdef GLOSSY_RS
    {
        UINT8 *tmp;
        UINT8 len;
        
        //Encode with TinyRS.
        len = f.header_length + f.payload_length;
        tmp = malloc(DATA_LEN + PARITY_LEN);
        if (tmp == NULL)
        {
            return FALSE;
        }

        memcpy(&tmp[0], f.header, f.header_length);
        memcpy(&tmp[f.header_length], f.payload, f.payload_length);
        if (tinyrs_encode(tmp, len) != ERROR_SUCCESS)
        {
            free(tmp);
            return FALSE;
        }

        memcpy(&f.payload[f.payload_length], &tmp[DATA_LEN], PARITY_LEN);
        f.payload_length += PARITY_LEN;
        free(tmp);
    }
#endif
    f.payload[f.payload_length++] = 0; //pseudo relay counter
    
    nos_rf_modem_write_frame(&f);

    // Set the radio to PLL_ON state as soon as RX_AACK is completed.
    at86rf212_subreg_write(SR_TRX_CMD, CMD_PLL_ON);
    //while (at86rf212_subreg_read(SR_TRX_STATUS) == TRX_STATUS_TRANSITION_IN_PROGRESS);
    while (at86rf212_subreg_read(SR_TRX_STATUS) != TRX_STATUS_PLL_ON); // from/to PLL state: 1us

    DISABLE_RF_INTR();

    sched_start();
    return 0;
}
