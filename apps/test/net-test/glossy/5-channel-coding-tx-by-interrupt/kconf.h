/*
 * Automatically generated C config: don't edit
 * Nano OS kernel version: 
 * Fri Apr 12 16:21:14 2013
 */
#define AUTOCONF_INCLUDED
#undef ATMEGA128
#undef ATMEGA1281
#define ATMEGA1284P 1
#undef MSP430F1611
#undef MSP430F2617
#undef S3FN41F
#undef EFM32GG330F1024
#undef EFM32GG990F1024
#undef STM32F103CB
#undef STM32F217VE
#undef STM32W108CB
#define CONFIG_MCU_NAME "atmega1284p"
#undef MICAZ
#undef NANO24
#undef OCX_Z2
#undef OCX_Z
#undef ZIGBEX
#undef YP128
#define ISN_903N 1
#define CONFIG_PLATFORM_NAME "isn-903n"

/*
 * ---
 */

/*
 * Basic Functions
 */
#undef ADC_M
#undef UTC_CLOCK_M
#undef ATMEGA128_UART_BR_9600
#undef ATMEGA128_UART_BR_19200
#undef ATMEGA128_UART_BR_38400
#undef ATMEGA128_UART_BR_57600
#undef ATMEGA128_UART_BR_115200
#undef ATMEGA128_UART_BR_230400
#undef ATMEGA128_UART_BR_250000
#undef ATMEGA128_UART_BR_500000
#undef ATMEGA128_UART_BR_1000000
#undef EEPROM_M
#undef ATMEGA1284P_CORECLK_16MHZ
#define ATMEGA1284P_CORECLK_8MHZ 1
#undef ATMEGA1284P_ASYNC_USART0_M
#define ATMEGA1284P_ASYNC_USART1_M 1
#undef ATMEGA1284P_ASYNC_USART1_BR_9600
#undef ATMEGA1284P_ASYNC_USART1_BR_19200
#undef ATMEGA1284P_ASYNC_USART1_BR_38400
#undef ATMEGA1284P_ASYNC_USART1_BR_57600
#undef ATMEGA1284P_ASYNC_USART1_BR_76800
#define ATMEGA1284P_ASYNC_USART1_BR_115200 1
#undef ATMEGA1284P_ASYNC_USART1_BR_230400
#undef ATMEGA1284P_ASYNC_USART1_BR_250000
#undef ATMEGA1284P_ASYNC_USART1_BR_500000
#undef ATMEGA1284P_ASYNC_USART1_BR_1000000

/*
 * Platform Specific Devices and Functions
 */
#define LED_M 1
#define UART_M 1
#undef UART_INPUT_M
#undef NO_SENSOR
#undef MTS300_M
#undef SENSOR_TEMPERATURE_M
#undef SENSOR_LIGHT_M
#define IEEE_802_15_4_DEV_M 1
#undef SENSOR_HUMIDITY_M
#define ISN_903N_CLOCK_ATMEGA1284P_RCOSC_8MHZ 1
#undef ISN_903N_CLOCK_AT86RF212_CLKM_16MHZ
#undef STDIO_ISN_903N_USART0
#define STDIO_ISN_903N_USART1 1
#define BUTTON_M 1
#undef BATTERY_MONITOR_M
#undef SENSOR_CO_M
#undef SENSOR_CO2_M
#undef PIR_SENSOR_M
#undef IR_LCD_M
#undef GENERIC_ADC_SENSOR_M
#undef GENERIC_ADC_GAS_SENSOR_M
#undef GENERIC_ADC_LIGHT_SENSOR_M
#undef GENERIC_ADC_MIC_SENSOR_M
#undef GENERIC_ADC_PIR_SENSOR_M
#undef PLATFORM_SPECIFIC_TEMPERATURE_SENSOR_M
#undef PLATFORM_SPECIFIC_LIGHT_SENSOR_M
#undef BH1600_M
#undef ELT_S100_M
#undef MICS5132_M
#undef ADF7023J_M
#define AT86RF212_M 1
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_0
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_1
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_2
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_3
#define AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_4 1
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_5
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_6
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_7
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_8
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_9
#undef AT86RF212_IEEE802_15_4_2003_2006_CHANNEL_10
#undef AT86RF212_CLKM_1MHZ
#undef AT86RF212_CLKM_2MHZ
#undef AT86RF212_CLKM_4MHZ
#undef AT86RF212_CLKM_8MHZ
#undef AT86RF212_CLKM_16MHZ
#undef AT86RF212_PHY_BPSK_300KCPS
#define AT86RF212_PHY_BPSK_600KCPS 1
#undef AT86RF212_PHY_OQPSK_400KCPS
#undef AT86RF212_PHY_OQPSK_1000KCPS_SIN
#undef AT86RF212_PHY_OQPSK_1000KCPS_RC_0_8
#undef CC112X_M
#undef CC2420_M
#undef MG2410_M
#undef IEEE_802_15_4_DEV_SECURITY_ACCELERATOR_M
#undef IEEE_802_15_4_DEV_FAST_READ
#undef SHTXX_M

/*
 * Kernel
 */
#define KERNEL_M 1
#define ENABLE_SCHEDULING 1
#undef SCHED_PERIOD_5
#define SCHED_PERIOD_10 1
#undef SCHED_PERIOD_32
#undef SCHED_PERIOD_100
#undef USER_TIMER_M
#undef MSGQ_M
#define TASKQ_LEN_8 1
#undef TASKQ_LEN_16
#undef TASKQ_LEN_32
#undef TASKQ_LEN_64
#undef TASKQ_LEN_128
#undef THREAD_M

/*
 * Library
 */
#define DEBUG_M 1
#undef AES_M
#undef LIB_TINY_RS_M

/*
 * Network Protocol
 */
#define IEEE_802_15_4_M 1
#undef IEEE_802_15_4_RXADDR_LIMIT_M
#undef NANO_MAC_M
#undef LPLL_MAC_M
#define GLOSSY_M 1
#undef NO_MAC_M
#undef GLOSSY_LPL
#undef GLOSSY_RS
#undef L2_LAYER_M
#undef MESH_ROUTING_M
#undef LOWPAN_M

/*
 * Storage System
 */
#undef STORAGE_M
