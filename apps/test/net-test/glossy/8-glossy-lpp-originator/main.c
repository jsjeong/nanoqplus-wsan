// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Glossy-LPP Reception Test Program
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 1. 18.
 */

#include "nos.h"
#include "glossy.h"

void periodic_send(void *args);

MAC_TX_INFO f;

UINT8 payload[100];

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 1;
#endif

UINT16 sent = 1;
UINT16 success = 0;
UINT16 not_initiated = 0;

UINT16 PACKET_INTERVAL = 300;

void send_done(MAC_TX_INFO *f, BOOL result)
{
    UINT32 prr;

    if (result)
        success++;

    prr = success * 100 / sent;
    printf("Send done-%u %s (%u/%u=%u%%)\n", sent, (result) ? "SUCCESS" : "FAIL", success, sent, (UINT16) prr);
}

UINT16 counter = 0;

void periodic_send(void *args)
{
    UINT8 i;

    if(counter%9 != 0){
        counter ++; 
        user_timer_create_sec(periodic_send, NULL, PACKET_INTERVAL+rand()%5, FALSE);
        return;
    }

    counter ++;

    for (i = 0; i < sizeof(payload); i++)
    {
        payload[i] = i;
    }

    payload[0] = (sent >> 8);
    payload[1] = (sent & 0xff);

    payload[2] = (not_initiated >> 8);
    payload[3] = (not_initiated & 0xff);

    payload[4] = (success >> 8);
    payload[5] = (success & 0xff);

    if (glossy_lpp_send(3, payload, 15, send_done) == TRUE)
    {
        printf("Glossy-LPP initiated! (%u)\n", sent);
        sent++;
    }
    else
    {
        printf("Glossy-LPP not initiated! (%u)\n", ++not_initiated);
    }

    user_timer_create_sec(periodic_send, NULL, PACKET_INTERVAL+rand()%5, FALSE);
    
}

int main(void)
{
    nos_init();
    glossy_init(0, node_id, NULL);

    printf("*** NanoQplus ***\n");
    
    srand(node_id);
    user_timer_create_sec(periodic_send, NULL, 50+rand()%10, FALSE);
 
    delay_ms(rand()*2);
    sched_start();
    return 0;
}
