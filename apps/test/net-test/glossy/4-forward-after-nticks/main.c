// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Glossy Forward after adjustable n ticks.
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 2. 15.
 */

#include "nos.h"
#include "glossy.h"

extern struct glossy_control glossy;

#ifdef NODE_ID
UINT16 node_id = NODE_ID;
#else
UINT16 node_id = 1;
#endif

/* void button_pushed(UINT8 sw_id) */
/* { */
/*     if (sw_id == SW3) */
/*     { */
/* //        if (glossy_delay_ticks < 0xff00) glossy_delay_ticks += 0x100;		// JJ: comment out */
/*         printf("\t+ 0x0100"); */
/*     } */
/*     else if (sw_id == SW4) */
/*     { */
/* //        if (glossy_delay_ticks < 0xffff) glossy_delay_ticks++;		// JJ: comment out */
/*         printf("\t+ 0x0001"); */
/*     } */
/*     else if (sw_id == SW5) */
/*     { */
/* //        if (glossy_delay_ticks > 0) glossy_delay_ticks--; */
/*         printf("\t- 0x0001"); */
/*     } */
/*     else if (sw_id == SW6) */
/*     { */
/* //        if (glossy_delay_ticks > 0x0100) glossy_delay_ticks -= 0x100; */
/*         printf("\t- 0x0100"); */
/*     } */

/* //    printf(" = 0x%04X (%u)\n", glossy_delay_ticks, glossy_delay_ticks); */
/* } */

void received(UINT8 dev_id, UINT16 src, const UINT8 *payload, UINT8 size)
{
    UINT8 i;

    printf("From src:%u ", src);
    for (i = 0; i < size; i++)
    {
        printf("%02X ", payload[i]);
    }
    printf("\n");
}

int main(void)
{
    nos_init();

    // Transceiver-specific configurations.
#ifdef AT86RF212_M
    rf212_set_mode(DEFAULT, RF212_BPSK_40KBPS);
#endif

    /* button_set_callback(button_pushed); */

    glossy_init(DEFAULT, 0, node_id, NULL);
    glossy_set_rx_cb(DEFAULT, received);
    led_on(0);
    
    printf("*** NanoQplus ***\n");

    sched_start();
    return 0;
}
