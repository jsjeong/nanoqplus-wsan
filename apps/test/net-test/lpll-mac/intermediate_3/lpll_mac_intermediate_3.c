/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file lpll_mac_tx.c
 * @author Jeehoon Lee (ETRI)
 */

#include "nos.h"

/*
#ifdef IEEE_802_15_4_DEV_M
#include "rf_modem.h"
#endif

typedef struct _lpll_mac_timer {
    void (*func)(void *);
    void *arg;
    UINT16 interval;
    volatile UINT16 next_task;
} LPLL_MAC_TIMER;

volatile LPLL_MAC_TIMER lpll_mac_timer;
*/


#define MY_ADDR     5
#define UP_DEST_ADDR   (MY_ADDR + 1)
#define DOWN_DEST_ADDR   (MY_ADDR - 1)

MAC_TX_INFO tx_info;
MAC_RX_INFO rx_info;
BOOL processing = FALSE;

void tx(void)
{
    //led_toggle(2);
    //tx_info.payload[0] -= 2;
    mac_tx(&tx_info);

    //led_toggle(2);
    //++tx_info.payload[0];
    //mac_tx(&tx_info);

    //led_toggle(2);
    //++tx_info.payload[0];
    //mac_tx(&tx_info);
}

void read_frame(void)
{
    while ( mac_rx(&rx_info) ) // MUST use "while" instead of "if" to make RX QUEUE empty.
    {
        processing = TRUE;
        //led_toggle(1);
        //uart_putu(STDIO, rx_info.payload[0]);
        //uart_putc(STDIO, 'R');
        //uart_putc(STDIO, '\n');

        tx_info.payload_length = rx_info.payload_length;
        
        if (rx_info.src_addr == DOWN_DEST_ADDR)
        {
            tx_info.dest_addr = UP_DEST_ADDR;
        }
        else
        {
            tx_info.dest_addr = DOWN_DEST_ADDR;
        }
        
        memcpy(tx_info.payload, rx_info.payload, rx_info.payload_length);

        //if (tx_info.payload[0]%3 == 0)
        //{
            tx();
        //}

        processing = FALSE;
    }
}

void mac_rx_callback(void)
{
    if (!processing)
        read_frame();
}
int main (void)
{
    nos_init();
    //led_on(0);
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    tx_info.sec = IEEE_802_15_4_SEC_NONE;
    tx_info.dest_addr_eui64 = NULL;

    mac_init(0x0104, MY_ADDR, NULL);
    mac_set_rx_cb( mac_rx_callback ); // default : NULL, sets a callback function for RX interrupt

    while(1)
    {
        delay_ms(3000);
    }

    return 0;
}

