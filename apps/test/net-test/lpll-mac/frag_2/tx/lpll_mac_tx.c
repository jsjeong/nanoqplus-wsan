/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @file nano_mac_tx_test.c
 * @author Sangcheol Kim (ETRI)
 * @author Haeyong Kim (ETRI)
 * @author Jongsoo Jeong (ETRI)
 * @author Jeehoon Lee (UST-ETRI)
 */

/*
 * $LastChangedDate: 2012-08-10 12:53:53 +0900 (Fri, 10 Aug 2012) $
 * $LastChangedBy: jsjeong $
 * $Rev: 844 $
 * $Id: nano_mac_tx_test.c 844 2012-08-10 03:53:53Z jsjeong $
 */

#include "nos.h"
#define PAYLOAD_LENGTH MAC_MAX_SAFE_TX_PAYLOAD_SIZE+22

MAC_TX_INFO tx_info;
MAC_RX_INFO rx_info;
BOOL processing = FALSE;

UINT16 my_addr_16 = 1;
UINT16 dest_addr_16 = 2;

void tx(void)
{
    ++tx_info.payload[0];
    if (tx_info.payload[0] == 0)
        ++tx_info.payload[0];

    // Use 16-bit short address and 64-bit extended address as the default
    // address alternately.
    // tx_info.dest_addr_eui64 = (tx_info.dest_addr_eui64) ? NULL : dest_addr_64;
    // tx_info.dest_addr_eui64 = NULL;

    //uart_putu(STDIO, tx_info.payload[0]);
    //uart_putc(STDIO, 'T');
    //uart_putc(STDIO, '\n');

    mac_tx(&tx_info);

    ++tx_info.payload[0];
    mac_tx(&tx_info);
    

/*
    if (mac_tx(&tx_info))
    {
        printf("\nTX (success): %u", tx_info.payload[0]);
    }
    else
    {
        printf("\n\tTX (fail) : %u", tx_info.payload[0]);
    }
*/

    //++tx_info.payload[0];
    //mac_tx_noack(&tx_info);

    //printf("\n\t\tTX without ACK : %u", tx_info.payload[0]);
}

void read_frame(void)
{
    while( mac_rx(&rx_info) ) // MUST use "while" instead of "if" to make RX QUEUE empty.
    {
        processing = TRUE;
        
        //uart_putc(STDIO, 'R');
        //uart_putc(STDIO, '\n');
        
        processing = FALSE;
    }
}

void mac_rx_callback(void)
{
    if (!processing)
        read_frame();
}

int main (void)
{
    UINT8 n;

    nos_init();
    uart_puts(STDIO, "\n\r*** Nano OS ***\n\r");

    tx_info.sec = IEEE_802_15_4_SEC_NONE;
    tx_info.dest_addr_eui64 = NULL;
    tx_info.dest_addr = dest_addr_16;
    tx_info.payload_length  = PAYLOAD_LENGTH;

    // Write dummy data to tx_info.payload[1]~[115].
    for (n = 0; n < PAYLOAD_LENGTH; n++)
    {
        tx_info.payload[n] = n;
    }

    mac_init(0x0104, my_addr_16, NULL);

    mac_set_rx_cb( mac_rx_callback ); // default : NULL, sets a callback function for RX interrupt

    delay_ms(1000);

    while(1)
    {
        tx();
        delay_ms(10000);
    }

    return 0;
}
