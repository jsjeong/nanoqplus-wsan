deps_config := \
	nos/stats/Kconfig \
	nos/storage/Kconfig \
	nos/net/application/Kconfig \
	nos/net/transport/Kconfig \
	nos/net/inet/Kconfig \
	nos/net/6lowpan/Kconfig \
	nos/net/l2-routing/Kconfig \
	nos/net/mac/Kconfig \
	nos/net/Kconfig \
	nos/lib/Kconfig \
	nos/kernel/Kconfig \
	nos/drivers/rf_modem/mg2410/Kconfig \
	nos/drivers/rf_modem/cc112x/Kconfig \
	nos/drivers/rf_modem/rf212/Kconfig \
	nos/drivers/rf_modem/cc2420/Kconfig \
	nos/drivers/rf_modem/Kconfig \
	nos/drivers/Kconfig \
	nos/platform/ek-z005sr/Kconfig \
	nos/platform/mg2410-s3fn41f/Kconfig \
	nos/platform/efm32gg-stk3700/Kconfig \
	nos/platform/kmote/Kconfig \
	nos/platform/tmote-sky/Kconfig \
	nos/platform/hmote2420/Kconfig \
	nos/platform/ubi-coin/Kconfig \
	nos/platform/ubi-msp430/Kconfig \
	nos/platform/ubee430/Kconfig \
	nos/platform/isn-901n/Kconfig \
	nos/platform/yp128/Kconfig \
	nos/platform/zigbex/Kconfig \
	nos/platform/ocx-z/Kconfig \
	nos/platform/nano24/Kconfig \
	nos/platform/micaz/Kconfig \
	nos/platform/Kconfig \
	nos/arch/efm32gg/Kconfig \
	nos/arch/s3fn41f/Kconfig \
	nos/arch/msp430f1611/Kconfig \
	nos/arch/atmega1284p/Kconfig \
	nos/arch/atmega128/Kconfig \
	nos/arch/Kconfig \
	/home/user/nos//Kconfig

.config include/linux/autoconf.h: $(deps_config)

$(deps_config):
