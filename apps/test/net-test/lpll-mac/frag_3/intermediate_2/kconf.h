/*
 * Automatically generated C config: don't edit
 * Nano OS kernel version: 
 * Wed Sep 12 23:52:21 2012
 */
#define AUTOCONF_INCLUDED

/*
 * Platforms
 */
#undef ATMEGA128
#undef ATMEGA1284P
#define MSP430F1611 1
#undef S3FN41F
#undef EFM32GG330F1024
#undef EFM32GG990F1024
#define CONFIG_MCU_NAME "msp430f1611"
#undef UBI_MSP430
#undef UBI_COIN
#undef UBEE430
#undef HMOTE2420
#define TMOTE_SKY 1
#undef KMOTE
#define CONFIG_PLATFORM_NAME "tmote-sky"

/*
 * Basic Functions
 */
#define LED_M 1
#define UART_M 1
#undef ADC_M
#undef TIMECHK_M
#undef MSP430F1611_UART_BR_9600
#undef MSP430F1611_UART_BR_19200
#undef MSP430F1611_UART_BR_38400
#undef MSP430F1611_UART_BR_57600
#define MSP430F1611_UART_BR_115200 1
#undef MSP430F1611_UART_BR_230400
#undef MSP430F1611_UART_BR_380400
#undef MSP430F1611_UART_BR_460800
#undef MSP430F1611_UART_BR_921600
#undef TEMPERATURE_DIODE_M

/*
 * Platform Specific Devices and Functions
 */
#undef SENSOR_TEMPERATURE_M
#define IEEE_802_15_4_DEV_M 1
#undef SENSOR_HUMIDITY_M
#undef GENERIC_ADC_SENSOR_M
#undef GENERIC_ADC_GAS_SENSOR_M
#undef GENERIC_ADC_LIGHT_SENSOR_M
#undef GENERIC_ADC_MIC_SENSOR_M
#undef GENERIC_ADC_PIR_SENSOR_M
#undef PLATFORM_SPECIFIC_TEMPERATURE_SENSOR_M
#undef PLATFORM_SPECIFIC_LIGHT_SENSOR_M
#define CC2420_M 1
#define CC2420_CHANNEL_11 1
#undef CC2420_CHANNEL_12
#undef CC2420_CHANNEL_13
#undef CC2420_CHANNEL_14
#undef CC2420_CHANNEL_15
#undef CC2420_CHANNEL_16
#undef CC2420_CHANNEL_17
#undef CC2420_CHANNEL_18
#undef CC2420_CHANNEL_19
#undef CC2420_CHANNEL_20
#undef CC2420_CHANNEL_21
#undef CC2420_CHANNEL_22
#undef CC2420_CHANNEL_23
#undef CC2420_CHANNEL_24
#undef CC2420_CHANNEL_25
#undef CC2420_CHANNEL_26
#undef RF212_M
#undef CC112X_M
#undef MG2410_M
#undef IEEE_802_15_4_DEV_SECURITY_ACCELERATOR_M
#undef SENSOR_SHTXX_M

/*
 * Kernel
 */
#undef KERNEL_M

/*
 * Library
 */
#define DEBUG_M 1
#undef AES_M

/*
 * Network Protocol
 */
#define IEEE_802_15_4_M 1
#undef IEEE_802_15_4_RXADDR_LIMIT_M
#undef IEEE_802_15_4_SECURITY_M
#undef NANO_MAC_M
#define LPLL_MAC_M 1
#undef L2_LAYER_M
#undef MESH_ROUTING_M
#undef LOWPAN_M

/*
 * Storage System
 */
#undef STORAGE_M

/*
 * Statistics modules
 */
#undef HEAP_STATISTICS_M
