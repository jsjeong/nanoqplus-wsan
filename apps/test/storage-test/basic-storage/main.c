// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * @brief External flash memory test application
 * @author Jongsoo Jeong (ETRI)
 * @date 2010.05.10
 */

#include "nos.h"
#include <stdlib.h>

UINT32 input_address(void);
UINT8 input_length(void);
UINT8 input_data(void);

UINT8 buf[200];

#define CHIP_ID 0

UINT32 input_address(void)
{
    UINT32 size = 0;

    storage_get_size(CHIP_ID, &size);
    printf("Start address in decimal (0~%u):", size);
    uart_gets(STDIO, (char *)buf, sizeof(buf));
    return strtoul((char *) buf, NULL, 10);
}

UINT8 input_length(void)
{
    printf("Length in decimal (0~%u bytes):", sizeof(buf));
    uart_gets(STDIO, (char *)buf, sizeof(buf));
    return strtoul((char *) buf, NULL, 10);
}

UINT8 input_data(void)
{
    printf("Data (0~%u bytes):", sizeof(buf));
    uart_gets(STDIO, (INT8 *)buf, sizeof(buf));
    return strlen((INT8 *) buf);
}

int main(void)
{
    UINT8 c;
    UINT32 addr;
    UINT8 len;
    INT8 res;

    nos_init();
    led_on(1);
    printf("\n*** Nano OS ***\n");

    enable_uart_rx_intr(STDIO);

    while (1)
    {
        printf("\n\n1. Read\n2. Write\n3. Erase\n");
        uart_puts(STDIO, "\n\rChoose command to run, and then press 'Enter'.\n\r");
        uart_gets(STDIO, (char *)buf, sizeof(buf));
        c = strtoul((char *) buf, NULL, 10);

        if (c == 1)
        {
            addr = input_address();
            len = input_length();
            res = storage_read(CHIP_ID, addr, buf, len);
            if (res < 0)
            {
                printf("\nError:%d\n", res);
            }
            else
            {
                UINT8 i;
                for (i = 0; i < len; i += 8)
                {
                    UINT8 display_buf[8];

                    if (i + 8 < len)
                    {
                        memcpy(display_buf, &buf[i], 8);
                    }
                    else
                    {
                        memcpy(display_buf, &buf[i], len - i);
                        memset(&display_buf[len - i], 0xff, i + 8 - len);
                    }
                    
                    printf("%u | %02X %02X %02X %02X  %02X %02X %02X %02X | %c%c%c%c %c%c%c%c\n",
                           addr + i,
                           display_buf[0], display_buf[1], display_buf[2], display_buf[3],
                           display_buf[4], display_buf[5], display_buf[6], display_buf[7],
                           display_buf[0], display_buf[1], display_buf[2], display_buf[3],
                           display_buf[4], display_buf[5], display_buf[6], display_buf[7]);
                }
                printf("\nReading is completed.\n");
            }
        }
        else if (c == 2)
        {
            addr = input_address();
            len = input_data();
            res = storage_write(CHIP_ID, addr, buf, len);
            printf("\nData to be written: %s", (char *) buf);
            if (res < 0)
            {
                printf("\nError:%d\n", res);
            }
            else
            {
                printf("\nWriting is completed.\n");
            }
        }
        else if (c == 3)
        {
            addr = input_address();
            len = input_length();
            res = storage_erase(CHIP_ID, addr, len);
            if (res < 0)
            {
                printf("\nError:%d\n", res);
            }
            else
            {
                printf("\nErasing is completed.\n");
            }
        }
    }

    return 0;
}
