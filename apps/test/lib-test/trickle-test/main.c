// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2006-2012
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * Trickle timer test application
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2010. 6. 1.
 */

#include "nos.h"

TRICKLE t;

void print_trickle(void)
{
    printf("Trickle(state:%u, tid:%d, interval:%u, next_t:%lu, elapsed:%lu, "
           "imin:%u, doublings:%u, k:%u, counter:%u)\n",
           t.state,
           t.tid,
           t.interval,
           t.next_t,
           t.elapsed,
           t.imin,
           t.doublings,
           t.k,
           t.counter);
}

void trickle_timer_expires(void *args)
{
    ENTER_CRITICAL();
    led_on(2);
    printf("Trickle timer expires!!\n");
    print_trickle();
    led_off(2);
    EXIT_CRITICAL();
}

void uart_rx_handler(UINT8 port, char rx_char)
{
    switch(rx_char)
    {
        case '1':
            printf("'Consistent' event occurs.\n");
            trickle_increment(&t);
            print_trickle();
            break;

        case '2':
            printf("'Inconsistent' event occurs.\n");
            trickle_reset(&t);
            print_trickle();
            break;

        case '3':
            printf("Destroy the timer.\n");
            trickle_stop(&t);
            print_trickle();
            break;

        case '4':
            printf("Create the timer.\n");
            trickle_create(&t, 3, 20, 10, trickle_timer_expires, NULL);
            print_trickle();
            break;
    }
}

void led_blinker(void *arg)
{
    while(1)
    {
        thread_sleep_ms(500);
        led_toggle(1);
        ENTER_CRITICAL();
        print_trickle();
        EXIT_CRITICAL();
    }
}

int main (void)
{
    nos_init();
    led_on(1);

    printf("\n*** Nano OS ***\n");

    uart_set_getc_callback(STDIO, uart_rx_handler);
    enable_uart_rx_intr(STDIO);
    
    printf("Press '1' to make a consistent event, "
           "or '2' to make an inconsistent event\n");
    trickle_create(&t, 3, 20, 10, trickle_timer_expires, NULL);
    print_trickle();

    thread_create(led_blinker, NULL, 0, PRIORITY_NORMAL);
    sched_start();

    return 0;
}
