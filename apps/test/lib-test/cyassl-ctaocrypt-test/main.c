// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * CTaoCrypt Test (imported from CyaSSL's testsuite.)
 *
 * @author Jongsoo Jeong (ETRI)
 * @date 2013. 10. 18.
 */

#include "nos.h"
#include "test-vector.h"

extern int md5_test(void);
extern int md4_test(void);
extern int sha_test(void);
extern int sha256_test(void);
extern int sha512_test(void);
extern int ripemd_test(void);
extern int blake2b_test(void);
extern int hmac_md5_test(void);
extern int hmac_sha_test(void);
extern int hmac_sha256_test(void);
extern int hmac_sha384_test(void);
extern int hmac_sha512_test(void);
extern int arc4_test(void);
extern int hc128_test(void);
extern int rabbit_test(void);
extern int des_test(void);
extern int des3_test(void);
extern int aes_test(void);
extern int aesgcm_test(void);
extern int aesccm_test(void);
extern int camellia_test(void);
extern int random_test(void);
extern int rsa_test(void);
extern int dh_test(void);
extern int dsa_test(void);
extern int pwdbased_test(void);
extern int ecc_test(void);

void test_thread(void *args)
{
    int ret = 0;

    if ( (ret = md5_test()) != 0) 
        printf("(%d) MD5      test failed!\n", ret);
    else
        printf( "MD5      test passed!\n");

#if 0
    if ( (ret = md4_test()) != 0)
        printf("(%d) MD4      test failed!\n", ret);
    else
        printf( "MD4      test passed!\n");
#endif

    if ( (ret = sha_test()) != 0)
        printf("(%d) SHA      test failed!\n", ret);
    else
        printf( "SHA      test passed!\n");

    if ( (ret = sha256_test()) != 0)
        printf("(%d) SHA-256  test failed!\n", ret);
    else
        printf( "SHA-256  test passed!\n");

    if ( (ret = sha512_test()) != 0) 
        printf("(%d) SHA-512  test failed!\n", ret);
    else
        printf( "SHA-512  test passed!\n");

#if 0
    if ( (ret = ripemd_test()) != 0)
        printf("(%d) RIPEMD   test failed!\n", ret);
    else
        printf( "RIPEMD   test passed!\n");
#endif

#if 0
    if ( (ret = blake2b_test()) != 0)
        printf("(%d) BLAKE2b  test failed!\n", ret);
    else
        printf( "BLAKE2b  test passed!\n");
#endif

    if ( (ret = hmac_md5_test()) != 0)
        printf("(%d) HMAC-MD5 test failed!\n", ret);
    else
        printf( "HMAC-MD5 test passed!\n");
    
    if ( (ret = hmac_sha_test()) != 0)
        printf("(%d) HMAC-SHA test failed!\n", ret);
    else
        printf( "HMAC-SHA test passed!\n");

    if ( (ret = hmac_sha256_test()) != 0)
        printf("(%d) HMAC-SHA256 test failed!\n", ret);
    else
        printf( "HMAC-SHA256 test passed!\n");

    if ( (ret = hmac_sha384_test()) != 0)
        printf("(%d) HMAC-SHA384 test failed!\n", ret);
    else
        printf( "HMAC-SHA384 test passed!\n");

    if ( (ret = hmac_sha512_test()) != 0)
        printf("(%d) HMAC-SHA512 test failed!\n", ret);
    else
        printf( "HMAC-SHA512 test passed!\n");

#if 0
    if ( (ret = arc4_test()) != 0)
        printf("(%d) ARC4     test failed!\n", ret);
    else
        printf( "ARC4     test passed!\n");
#endif

#if 0
    if ( (ret = hc128_test()) != 0)
        printf("(%d) HC-128   test failed!\n", ret);
    else
        printf( "HC-128   test passed!\n");
#endif

#if 0
    if ( (ret = rabbit_test()) != 0)
        printf("(%d) Rabbit   test failed!\n", ret);
    else
        printf( "Rabbit   test passed!\n");
#endif

#if 0
    if ( (ret = des_test()) != 0)
        printf("(%d) DES      test failed!\n", ret);
    else
        printf( "DES      test passed!\n");
#endif

#if 0
    if ( (ret = des3_test()) != 0)
        printf("(%d) DES3     test failed!\n", ret);
    else
        printf( "DES3     test passed!\n");
#endif

#if 0
    if ( (ret = aes_test()) != 0)
        printf("(%d) AES      test failed!\n", ret);
    else
        printf( "AES      test passed!\n");
#endif

#if 0
    if ( (ret = aesgcm_test()) != 0)
        printf("(%d) AES-GCM  test failed!\n", ret);
    else
        printf( "AES-GCM  test passed!\n");
#endif

#if 0
    if ( (ret = aesccm_test()) != 0)
        printf("(%d) AES-CCM  test failed!\n", ret);
    else
        printf( "AES-CCM  test passed!\n");
#endif

#if 0
    if ( (ret = camellia_test()) != 0)
        printf("(%d) CAMELLIA test failed!\n", ret);
    else
        printf( "CAMELLIA test passed!\n");
#endif

#if 0
    if ( (ret = random_test()) != 0)
        printf("(%d) RANDOM   test failed!\n", ret);
    else
        printf( "RANDOM   test passed!\n");
#endif

#if 0
    if ( (ret = rsa_test()) != 0)
        printf("(%d) RSA      test failed!\n", ret);
    else
        printf( "RSA      test passed!\n");
#endif

#if 0
    if ( (ret = dh_test()) != 0)
        printf("(%d) DH       test failed!\n", ret);
    else
        printf( "DH       test passed!\n");
#endif

#if 0
    if ( (ret = dsa_test()) != 0)
        printf("(%d) DSA      test failed!\n", ret);
    else
        printf( "DSA      test passed!\n");
#endif

#if 0
    if ( (ret = pwdbased_test()) != 0)
        printf("(%d) PWDBASED test failed!\n", ret);
    else
        printf( "PWDBASED test passed!\n");
#endif

    if ( (ret = ecc_test()) != 0)
        printf("(%d) ECC      test failed!\n", ret);
    else
        printf( "ECC      test passed!\n");
}

int main(void)
{
    nos_init();
    led_on(0);

    printf("\n*** NanoQplus ***\n");
    thread_create(test_thread, NULL, 1000, PRIORITY_NORMAL);

    sched_start();
    return 0;
}

