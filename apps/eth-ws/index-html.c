// -*- c-file-style:"bsd"; c-basic-offset:4; indent-tabs-mode:nil; -*-
/*
 * Copyright (c) 2013
 * Electronics and Telecommunications Research Institute (ETRI)
 * All Rights Reserved.
 *
 * Following acts are STRICTLY PROHIBITED except when a specific prior written
 * permission is obtained from ETRI or a separate written agreement with ETRI
 * stipulates such permission specifically:
 *
 * a) Selling, distributing, sublicensing, renting, leasing, transmitting,
 * redistributing or otherwise transferring this software to a third party;
 *
 * b) Copying, transforming, modifying, creating any derivatives of, reverse
 * engineering, decompiling, disassembling, translating, making any attempt to
 * discover the source code of, the whole or part of this software in source or
 * binary form;
 *
 * c) Making any copy of the whole or part of this software other than one copy
 * for backup purposes only; and
 *
 * d) Using the name, trademark or logo of ETRI or the names of contributors in
 * order to endorse or promote products derived from this software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS," WITHOUT A WARRANTY OF ANY KIND. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT WILL ETRI (OR ITS
 * LICENSORS, IF ANY) BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR
 * DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES,
 * HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN
 * IF ETRI HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * Any permitted redistribution of this software must retain the copyright
 * notice, conditions, and disclaimer as specified above.
 */

/**
 * index.html for the WebSense application.
 */

const char index_html_http_hdr[] =
    "HTTP/1.1 200 OK\r\nContent-type: text/html\r\n\r\n";

const char index_html[] =
    "<!DOCTYPE html>"
    "<html>"
    "<head>"
    "<script>"
    "var x;"
    "function onRecv()"
    "{"
    "if (x.readyState==4 && x.status==200)"
    "{"
    "var d=new Date();"
    "var t=d.toLocaleTimeString();"
    "document.getElementById(\"led\").innerHTML=\"LED: \"+x.responseText+\" (\"+t+\")\";"
    "}"
    "}"
    "function sense()"
    "{"
    "x=new XMLHttpRequest();"
    "x.onreadystatechange=onRecv;"
    "x.open(\"GET\",\"led.txt\",true);"
    "x.send();"
    "}"
    "</script>"
    "</head>"
    "<body>"
    "<img src=\"http://www.etri.re.kr/etri_2012/kor/images/pro/pro070000_img01.gif\">"
    "<h1>NanoQplus TCP&HTTP Web Server</h1>"
    "<h2><div id=\"led\">.</div></h2>"
    "<button type=\"button\" onclick=\"sense()\">Sense!</button>"
    "</body>"
    "</html>";
